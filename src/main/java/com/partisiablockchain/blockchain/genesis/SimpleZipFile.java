package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ThrowingFunction;
import com.secata.tools.coverage.ThrowingSupplier;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/** Simplified ZIP handling. */
final class SimpleZipFile {
  private final ZipFile zip;

  SimpleZipFile(File file) {
    this.zip = ExceptionConverter.call(() -> new ZipFile(file));
  }

  /**
   * Return a filtered list of entries.
   *
   * @param filter the filter
   * @return the entries matching the filter
   */
  List<Entry> filterEntries(Predicate<ZipEntry> filter) {
    return zip.stream().filter(filter).map(entry -> new Entry(zip, entry)).toList();
  }

  /**
   * Get an entry by name.
   *
   * @param name the name of the entry
   * @return the entry, null if the entry does not exist
   */
  Entry getEntry(String name) {
    ZipEntry zipEntry = zip.getEntry(name);
    if (zipEntry == null) {
      return null;
    } else {
      return new Entry(zip, zipEntry);
    }
  }

  static final class Entry {
    private final ZipFile zipFile;
    private final ZipEntry zipEntry;

    private Entry(ZipFile zipFile, ZipEntry zipEntry) {
      this.zipFile = zipFile;
      this.zipEntry = zipEntry;
    }

    /**
     * Get the entry name.
     *
     * @return the name of the entry
     */
    String getName() {
      return zipEntry.getName();
    }

    /**
     * Read the raw bytes from the entry.
     *
     * @return the bytes
     */
    byte[] readBytes() {
      return WithResource.apply(
          () -> zipFile.getInputStream(zipEntry),
          InputStream::readAllBytes,
          "unable to read zip entry");
    }

    /**
     * Read a zip entry, transforming it in the process.
     *
     * @param transform the transform function
     * @param errorMessage the error message if the read fails
     * @return the read object
     * @param <ResultT> the result type
     */
    <ResultT> ResultT read(
        ThrowingFunction<SafeDataInputStream, ResultT> transform, String errorMessage) {
      ThrowingSupplier<InputStream> getEntry = () -> zipFile.getInputStream(zipEntry);

      ThrowingFunction<InputStream, ResultT> read =
          rawStream -> {
            SafeDataInputStream dataInputStream = new SafeDataInputStream(rawStream);
            return transform.apply(dataInputStream);
          };

      return WithResource.apply(getEntry, read, errorMessage);
    }
  }
}
