package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/**
 * The state of the account plugin.
 *
 * @param <ContextFreeT> type of the part of the state without context
 * @param <AccountT> type of the state associated with each account
 * @param <ContractT> type of the state associated with each contract
 */
@Immutable
public final class AccountPluginState<
        ContextFreeT extends StateSerializable,
        AccountT extends StateSerializable,
        ContractT extends StateSerializable>
    implements StateSerializable {

  private final ContextFreeT contextFree;
  private final AvlTree<BlockchainAddress, AccountT> accounts;
  private final AvlTree<BlockchainAddress, ContractT> contracts;

  private AccountPluginState() {
    this.contextFree = null;
    this.accounts = null;
    this.contracts = null;
  }

  AccountPluginState(
      ContextFreeT contextFree,
      AvlTree<BlockchainAddress, AccountT> accounts,
      AvlTree<BlockchainAddress, ContractT> contracts) {
    this.contextFree = contextFree;
    this.accounts = accounts;
    this.contracts = contracts;
  }

  AccountPluginState<ContextFreeT, AccountT, ContractT> setAccount(
      BlockchainAddress address, AccountT account) {
    if (account == null) {
      return new AccountPluginState<>(contextFree, accounts.remove(address), contracts);
    } else {
      return new AccountPluginState<>(contextFree, accounts.set(address, account), contracts);
    }
  }

  AccountPluginState<ContextFreeT, AccountT, ContractT> setContract(
      BlockchainAddress address, ContractT contract) {
    if (contract == null) {
      return new AccountPluginState<>(contextFree, accounts, contracts.remove(address));
    } else {
      return new AccountPluginState<>(contextFree, accounts, contracts.set(address, contract));
    }
  }

  AccountPluginState<ContextFreeT, AccountT, ContractT> setContextFree(ContextFreeT contextFree) {
    return new AccountPluginState<>(contextFree, accounts, contracts);
  }

  /**
   * Get the part of the state without context.
   *
   * @return part of the state without context
   */
  public ContextFreeT getContextFree() {
    return contextFree;
  }

  /**
   * Get account state from address.
   *
   * @param address account address
   * @return account state
   */
  public AccountT getAccount(BlockchainAddress address) {
    return accounts.getValue(address);
  }

  /**
   * Get contract state from address.
   *
   * @param address contract address
   * @return contract state
   */
  public ContractT getContract(BlockchainAddress address) {
    return contracts.getValue(address);
  }

  BlockchainAccountPlugin.AccountState<ContextFreeT, AccountT> getAccountState(
      BlockchainAddress address) {
    return new BlockchainAccountPlugin.AccountState<>(contextFree, getAccount(address));
  }

  BlockchainAccountPlugin.ContractState<ContextFreeT, ContractT> getContractState(
      BlockchainAddress address) {
    return new BlockchainAccountPlugin.ContractState<>(contextFree, getContract(address));
  }

  AvlTree<BlockchainAddress, AccountT> getAccounts() {
    return accounts;
  }

  AvlTree<BlockchainAddress, ContractT> getContracts() {
    return contracts;
  }
}
