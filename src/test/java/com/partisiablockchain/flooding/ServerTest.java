package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.CloseableTest;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ServerTest extends CloseableTest {

  @Test
  public void closeTwice() {
    Server server =
        new Server(
            ObjectCreator.port(),
            ObjectCreator.noOpBiConsumer(),
            ExecutionFactoryThreaded.create());
    assertThat(server.isClosed()).isFalse();
    server.close();
    assertThat(server.isClosed()).isTrue();
    server.close();
  }

  @Test
  public void closeOnSocketError() throws Exception {
    ServerSocket serverSocket = new ServerSocket();
    ExecutionFactoryFake executionFactory = new ExecutionFactoryFake();
    Server server = new Server(serverSocket, ObjectCreator.noOpBiConsumer(), executionFactory);

    executionFactory.getExecutionQueue(Server.SERVER_SOCKET_THREAD_NAME).runStep();
    assertThat(server.isClosed()).isTrue();
    assertThat(serverSocket.isClosed()).isTrue();
  }

  @Test
  public void acceptAnySocket() throws Exception {
    List<Address> accepted = new ArrayList<>();
    List<Socket> sockets = new ArrayList<>();
    Socket expectedSocket =
        ObjectCreator.mockedSocket(
            ObjectCreator.blockingInput(this), ObjectCreator.blockingOutput(this));
    ExecutionFactoryFake executionFactory = new ExecutionFactoryFake();
    Server server =
        new Server(
            ObjectCreator.serverSocket(this, expectedSocket),
            (address, socket) -> {
              accepted.add(address);
              sockets.add(socket);
            },
            executionFactory);
    register(server);
    executionFactory.getExecutionQueue(Server.SERVER_SOCKET_THREAD_NAME).runStep();
    assertThat(sockets.size()).isEqualTo(0);

    executionFactory.getExecutionQueue(Server.FLOODING_SERVER_THREAD_NAME).runStep();
    assertThat(sockets.size()).isEqualTo(1);
    assertThat(sockets.get(0)).isEqualTo(expectedSocket);
    assertThat(accepted.get(0))
        .isEqualTo(
            new Address(
                expectedSocket.getInetAddress().getHostAddress(), expectedSocket.getPort()));
  }
}
