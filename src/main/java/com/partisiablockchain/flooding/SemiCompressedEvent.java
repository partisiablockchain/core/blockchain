package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;

/**
 * Semi compressed executable event. If the event is a sync event it is compressed otherwise not.
 */
public final class SemiCompressedEvent implements DataStreamSerializable {
  private final boolean isCompressed;
  private final DataStreamSerializable executableEventOrIdentifierAndShard;

  private SemiCompressedEvent(
      boolean isCompressed, DataStreamSerializable executableEventOrIdentifierAndShard) {
    this.isCompressed = isCompressed;
    this.executableEventOrIdentifierAndShard = executableEventOrIdentifierAndShard;
  }

  /**
   * Creates a semi compressed event from an identifier and shard.
   *
   * @param identifierAndShard identifier and shard.
   */
  public SemiCompressedEvent(IdentifierAndShard identifierAndShard) {
    this.isCompressed = true;
    this.executableEventOrIdentifierAndShard = identifierAndShard;
  }

  /**
   * Creates a semi compressed event from an executable event.
   *
   * @param executableEvent executable event.
   */
  public SemiCompressedEvent(ExecutableEvent executableEvent) {
    this.isCompressed = false;
    this.executableEventOrIdentifierAndShard = executableEvent;
  }

  /**
   * Returns if the event is compressed.
   *
   * @return is compressed or not.
   */
  public boolean isCompressed() {
    return isCompressed;
  }

  /**
   * Returns the identifier and shard if the event is compressed else throws an error.
   *
   * @return identifier and shard.
   */
  public IdentifierAndShard getIdentifierAndShard() {
    if (!isCompressed) {
      throw new RuntimeException("Event is not compressed");
    }
    return (IdentifierAndShard) executableEventOrIdentifierAndShard;
  }

  /**
   * Returns the executable event if the event is not compressed else throws an error.
   *
   * @return identifier and shard.
   */
  public ExecutableEvent getExecutableEvent() {
    if (isCompressed) {
      throw new RuntimeException("Event is compressed");
    }
    return (ExecutableEvent) executableEventOrIdentifierAndShard;
  }

  /** Utility for serializing and deserializing a list of semi compressed events. */
  public static SafeListStream<SemiCompressedEvent> LIST_SERIALIZER =
      SafeListStream.create(SemiCompressedEvent::read, SemiCompressedEvent::write);

  /**
   * Read a semi compressed event from the stream.
   *
   * @param stream the stream to read from
   * @return the read event
   */
  public static SemiCompressedEvent read(SafeDataInputStream stream) {
    boolean isSync = stream.readBoolean();
    if (isSync) {
      return new SemiCompressedEvent(isSync, IdentifierAndShard.read(stream));
    } else {
      return new SemiCompressedEvent(isSync, ExecutableEvent.read(stream));
    }
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeBoolean(isCompressed);
    executableEventOrIdentifierAndShard.write(stream);
  }

  /** Record holding an identifier for an executable event and its origin shard. */
  public record IdentifierAndShard(Hash identifier, String shard)
      implements DataStreamSerializable {

    @Override
    public void write(SafeDataOutputStream stream) {
      identifier.write(stream);
      stream.writeString(shard);
    }

    /**
     * Reads an IdentifierAndShard from the specified stream.
     *
     * @param inputStream the stream to read from.
     * @return the read IdentifierAndShard
     */
    public static IdentifierAndShard read(SafeDataInputStream inputStream) {
      return new IdentifierAndShard(Hash.read(inputStream), inputStream.readString());
    }
  }
}
