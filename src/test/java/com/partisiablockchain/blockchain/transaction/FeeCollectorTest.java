package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.AccountPluginAccessor;
import com.partisiablockchain.crypto.Hash;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test for {@link FeeCollector}. */
public final class FeeCollectorTest {

  private final AccountPluginAccessor feePlugin = new AccountPluginForTest(3);
  private final Hash identifier = TestObjects.hashNumber(1);

  @Test
  public void updateAndCommit() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerCpuFee(1);
    collector.registerNetworkFee(identifier, 1);
    collector.registerAllocatedCost(1);
    assertToString(collector, 4, 0, 1, Map.of(identifier, 1L), 1);
  }

  @Test
  public void boundary_cpu() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerCpuFee(4);
    assertToString(collector, 4, 0, 4, Map.of(), 0);
  }

  @Test
  public void boundary_network() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerNetworkFee(identifier, 4);
    assertToString(collector, 4, 0, 0, Map.of(identifier, 4L), 0);
  }

  @Test
  public void boundary_allocatedCost() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerAllocatedCost(4);
    assertToString(collector, 4, 0, 0, Map.of(), 4);
  }

  @Test
  public void gasFromContract() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerPaymentFromContract(4);
    collector.registerAllocatedCost(8);
    assertToString(collector, 4, 4, 0, Map.of(), 8);
  }

  @Test
  public void failUpdate() {
    FeeCollector collector = FeeCollector.create(feePlugin, 1);
    assertThatThrownBy(() -> collector.registerCpuFee(2))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ran out of gas: " + referenceToString(1, 0, 2, 0, 0));
  }

  @Test
  public void failUpdate_networkFee() {
    FeeCollector collector = FeeCollector.create(feePlugin, 1);
    assertThatThrownBy(() -> collector.registerNetworkFee(identifier, 2))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ran out of gas: " + referenceToString(1, 0, 0, 2, 0));
  }

  @Test
  public void failGasFromContract() {
    FeeCollector collector = FeeCollector.create(feePlugin, 1);
    collector.registerPaymentFromContract(1);
    assertThatThrownBy(() -> collector.registerCpuFee(3))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ran out of gas: " + referenceToString(1, 1, 3, 0, 0));
  }

  @Test
  public void failAllocatedCost() {
    FeeCollector collector = FeeCollector.create(feePlugin, 1);
    assertThatThrownBy(() -> collector.registerAllocatedCost(2))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ran out of gas: " + referenceToString(1, 0, 0, 0, 2));
  }

  @Test
  public void successGasFromContract() {
    FeeCollector collector = FeeCollector.create(feePlugin, 4);
    collector.registerPaymentFromContract(1);
    collector.registerCpuFee(4);
    assertThat(collector.sum()).isEqualTo(4);
  }

  @Test
  public void accumulate() {
    FeeCollector collector = FeeCollector.create(feePlugin, 100);
    collector.registerPaymentFromContract(1);
    collector.registerPaymentFromContract(1);
    collector.registerCpuFee(1);
    collector.registerCpuFee(1);
    collector.registerAllocatedCost(1);
    collector.registerAllocatedCost(1);
    collector.registerNetworkFee(identifier, 1);
    collector.registerNetworkFee(identifier, 1);
    assertToString(collector, 100, 2, 2, Map.of(identifier, 2L), 2);
  }

  /** Tests collector values. */
  private static void assertToString(
      FeeCollector collector,
      long gasPaidByUser,
      long gasToBePaidByContract,
      long feeCpu,
      Map<Hash, Long> feeNetwork,
      long allocatedForEvents) {

    long networkSum = 0L;
    for (Long value : feeNetwork.values()) {
      networkSum += value;
    }

    assertThat(collector.toString())
        .isEqualTo(
            referenceToString(
                gasPaidByUser, gasToBePaidByContract, feeCpu, networkSum, allocatedForEvents));
  }

  private static String referenceToString(
      long gasPaidByUser,
      long gasToBePaidByContract,
      long feeCpu,
      long feeNetwork,
      long allocatedForEvents) {
    long consumedInTotal = feeCpu + feeNetwork + allocatedForEvents;
    return "FeeCollector{"
        + "gasPaidByUser="
        + gasPaidByUser
        + ", gasToBePaidByContract="
        + gasToBePaidByContract
        + ", consumedInTotal="
        + consumedInTotal
        + ", feeCpu="
        + feeCpu
        + ", feeNetwork="
        + feeNetwork
        + ", allocatedForEvents="
        + allocatedForEvents
        + "}";
  }
}
