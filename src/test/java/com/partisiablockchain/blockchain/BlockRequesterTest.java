package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.flooding.BlockRequest;
import com.partisiablockchain.flooding.Packet;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockRequesterTest {

  private static final long firstBlockTime = 15;
  private final List<Packet<?>> packets;
  private final BlockRequester rpc;

  /** Creates a new test. */
  public BlockRequesterTest() {
    packets = new ArrayList<>();
    rpc = new BlockRequester(500);
    rpc.init(packets::add);
  }

  @Test
  public void requestFirst() {
    rpc.requestBlock(firstBlockTime);

    assertThat(packets).hasSize(1);
    assertPacketHash(0, firstBlockTime);
  }

  @Test
  public void requestSameWithinBackoff() {
    rpc.requestBlock(firstBlockTime);
    rpc.requestBlock(firstBlockTime);

    assertThat(packets).hasSize(1);
    assertPacketHash(0, firstBlockTime);
  }

  @Test
  public void requestOtherWithinBackoff() {
    rpc.requestBlock(firstBlockTime);

    long secondBlockTime = firstBlockTime + NetworkRequestHandler.MAX_BLOCKS;
    rpc.requestBlock(secondBlockTime);

    assertThat(packets).hasSize(2);
    assertPacketHash(0, firstBlockTime);
    assertPacketHash(1, secondBlockTime);
  }

  @Test
  public void requestSameAfterBackoff() throws InterruptedException {
    rpc.requestBlock(firstBlockTime);
    Thread.sleep(600);
    rpc.requestBlock(firstBlockTime);

    assertThat(packets).hasSize(2);
    assertPacketHash(0, firstBlockTime);
    assertPacketHash(1, firstBlockTime);
  }

  @Test
  public void allowRequestAgainAfterReceivingResponse() {
    rpc.requestBlock(firstBlockTime);
    rpc.responseReceived(firstBlockTime);
    rpc.requestBlock(firstBlockTime);

    assertThat(packets).hasSize(2);
    assertPacketHash(0, firstBlockTime);
    assertPacketHash(1, firstBlockTime);
  }

  @Test
  public void isAfter() {
    assertThat(BlockRequester.isAfter(() -> 10, 10)).isFalse();
    assertThat(BlockRequester.isAfter(() -> 10, 11)).isFalse();
    assertThat(BlockRequester.isAfter(() -> 11, 10)).isTrue();
  }

  private void assertPacketHash(int packetIndex, long blockTime) {
    Object payload = packets.get(packetIndex).getPayload();
    assertThat(payload).isInstanceOf(BlockRequest.class);
    assertThat(((BlockRequest) payload).getBlockTime()).isEqualTo(blockTime);
  }
}
