package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Request for a block send via the flooding network. */
public final class BlockRequest implements DataStreamSerializable {

  private final long blockTime;

  /**
   * Default constructor.
   *
   * @param blockTime time of block to be sent
   */
  public BlockRequest(long blockTime) {
    this.blockTime = blockTime;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeLong(blockTime);
  }

  /**
   * Read {@link BlockRequest} from a stream.
   *
   * @param stream to read from
   * @return read {@link BlockRequest}
   */
  public static BlockRequest read(SafeDataInputStream stream) {
    return new BlockRequest(stream.readLong());
  }

  /**
   * Get block time.
   *
   * @return block time
   */
  public long getBlockTime() {
    return blockTime;
  }
}
