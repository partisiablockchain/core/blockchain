package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;

final class ContractVariableStorageImpl implements ContractVariableStorage {

  private final Storage storage;

  ContractVariableStorageImpl(Storage storage) {
    this.storage = storage;
  }

  @Override
  public void write(BlockchainAddress address, int variableId, byte[] value) {
    storage.write(key(address, variableId), stream -> stream.writeDynamicBytes(value));
  }

  @Override
  public void remove(BlockchainAddress address, int variableId) {
    storage.remove(key(address, variableId));
  }

  @Override
  public byte[] read(BlockchainAddress address, int variableId) {
    return storage.read(key(address, variableId), SafeDataInputStream::readDynamicBytes);
  }

  @Override
  public boolean containsVariable(BlockchainAddress address, int variableId) {
    return storage.has(key(address, variableId));
  }

  static Hash key(BlockchainAddress address, int variableId) {
    return Hash.create(
        stream -> {
          address.write(stream);
          stream.writeInt(variableId);
        });
  }
}
