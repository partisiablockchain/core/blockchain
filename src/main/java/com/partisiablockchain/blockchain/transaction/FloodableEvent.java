package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** An event that is valid on the flooding network. */
public final class FloodableEvent implements NetworkFloodable {

  private final FinalBlock block;
  private final EventTransaction eventTransaction;
  private final EventProof proof;

  private final EventProof.ChainIds chainIds;

  FloodableEvent(FinalBlock block, EventTransaction eventTransaction, EventProof proof) {
    this.block = block;
    this.eventTransaction = eventTransaction;
    this.proof = proof;
    this.chainIds = this.proof.verify(block.getBlock().getState(), eventTransaction);
    if (eventTransaction.getCommitteeId() > getCommitteeForFinalizingBlock()) {
      throw new IllegalStateException("Block finalizing event has smaller committeeId than event");
    }
  }

  FloodableEvent(
      FinalBlock block,
      EventTransaction eventTransaction,
      EventProof proof,
      EventProof.ChainIds chainIds) {
    this.block = block;
    this.eventTransaction = eventTransaction;
    this.proof = proof;
    this.chainIds = chainIds;
  }

  /**
   * Create a new floodable event.
   *
   * @param event the event to make floodable
   * @param block the block finalizing the event
   * @param storage the blockchain storage to fetch state parts from
   * @return the created event
   */
  public static FloodableEvent create(
      ExecutableEvent event, FinalBlock block, StateStorageRaw storage) {
    Hash stateHash = block.getBlock().getState();
    EventProof eventProof = EventProof.constructEventProof(storage, stateHash, event.identifier());
    return new FloodableEvent(block, event.getEvent(), eventProof);
  }

  /**
   * Compresses a floodable event.
   *
   * @return compressed floodable event
   */
  public FloodableEventCompressed compress() {
    return FloodableEventCompressed.create(getExecutableEvent().identifier(), block, proof);
  }

  /**
   * Read the event from stream.
   *
   * @param stream the stream to read from
   * @return the read stream
   */
  public static FloodableEvent read(SafeDataInputStream stream) {
    FinalBlock block = FinalBlock.read(stream);
    EventTransaction transaction = EventTransaction.read(stream);
    EventProof proof = EventProof.read(stream);
    return new FloodableEvent(block, transaction, proof);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    block.write(stream);
    eventTransaction.write(stream);
    proof.write(stream);
  }

  /**
   * Get executable event.
   *
   * @return created executable event
   */
  public ExecutableEvent getExecutableEvent() {
    return new ExecutableEvent(chainIds.subChainId(), eventTransaction);
  }

  /**
   * Get the chain id of the sending chain.
   *
   * @return chain id
   */
  public String getChainId() {
    return chainIds.chainId();
  }

  /**
   * Get committee that will be finalizing the block.
   *
   * @return committee id
   */
  public long getCommitteeForFinalizingBlock() {
    return getBlock().getBlock().getCommitteeId();
  }

  /**
   * Get block finalizing the event.
   *
   * @return block finalizing the event
   */
  public FinalBlock getBlock() {
    return block;
  }
}
