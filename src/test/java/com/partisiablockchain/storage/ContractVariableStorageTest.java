package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractVariableStorageTest {

  @Test
  public void testRemove() {
    final ContractVariableStorage storage =
        new ContractVariableStorage() {
          private byte[] value;

          @Override
          public void write(BlockchainAddress address, int variableId, byte[] value) {
            this.value = value;
          }

          @Override
          public byte[] read(BlockchainAddress address, int variableId) {
            return value;
          }

          @Override
          public boolean containsVariable(BlockchainAddress address, int variableId) {
            return false;
          }
        };

    byte[] value = new byte[] {1};
    storage.write(null, 0, value);

    assertThat(storage.read(null, 0)).isEqualTo(value);

    storage.remove(null, 0);

    assertThat(storage.read(null, 0)).isEqualTo(new byte[0]);
  }
}
