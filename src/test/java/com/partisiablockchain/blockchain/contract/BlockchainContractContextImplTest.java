package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContextImpl;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainContractContextImplTest {

  @Test
  public void values() {
    BlockchainContractContextImpl impl =
        new BlockchainContractContextImpl(TestObjects.CONTRACT_SYS);
    assertThat(impl.getContractAddress()).isEqualTo(TestObjects.CONTRACT_SYS);
  }
}
