package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.ContractState;
import com.partisiablockchain.blockchain.ExecutedTransactionEvents;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.Routing;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.EventResult;
import com.partisiablockchain.blockchain.transaction.EventTransaction.EventType;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerEvent;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** The context for executing a transaction. */
public final class ExecutionContext {

  private static final Logger logger = LoggerFactory.getLogger(ExecutionContext.class);

  private final String chainId;
  private final String subChainId;
  private final long committeeId;
  private final long governanceVersion;
  private final long blockTime;
  private final long blockProductionTime;

  private ExecutionContext(
      String chainId,
      String subChainId,
      long committeeId,
      long governanceVersion,
      long blockTime,
      long blockProductionTime) {
    this.chainId = chainId;
    this.subChainId = subChainId;
    this.committeeId = committeeId;
    this.governanceVersion = governanceVersion;
    this.blockTime = blockTime;
    this.blockProductionTime = blockProductionTime;
  }

  /**
   * Initialize the execution context.
   *
   * @param chainId id of the chain
   * @param subChainId id of the current shard
   * @param committeeId the committee id of the current block
   * @param governanceVersion the governance version for this execution
   * @param blockTime block time for the current block
   * @param blockProductionTime production time for the current block
   * @return the initialized context
   */
  static ExecutionContext init(
      String chainId,
      String subChainId,
      long committeeId,
      long governanceVersion,
      long blockTime,
      long blockProductionTime) {
    return new ExecutionContext(
        chainId, subChainId, committeeId, governanceVersion, blockTime, blockProductionTime);
  }

  /**
   * Initialize the execution context.
   *
   * @param chainId id of the chain
   * @param subChainId id of the current shard
   * @param block current block for the execution
   * @param governanceVersion the governance version for this execution
   * @return the initialized context
   */
  public static ExecutionContext init(
      String chainId, String subChainId, Block block, long governanceVersion) {
    return init(
        chainId,
        subChainId,
        block.getCommitteeId(),
        governanceVersion,
        block.getBlockTime(),
        block.getProductionTime());
  }

  /**
   * Get block time for the current block.
   *
   * @return block time for the current block
   */
  public long getBlockTime() {
    return blockTime;
  }

  /**
   * Get production time for the current block.
   *
   * @return production time for the current block
   */
  long getBlockProductionTime() {
    return blockProductionTime;
  }

  /**
   * Execute this signed transaction modifying the chain state.
   *
   * @param nextState current state of the blockchain before execution of this transaction
   * @param transaction the signed transaction to execute
   * @return list of created events if success, null otherwise
   */
  public ExecutedTransactionEvents execute(
      MutableChainState nextState, SignedTransaction transaction) {
    BlockchainAddress sender = transaction.getSender();
    nextState.bumpNonce(sender);
    long remaining = nextState.payFee(getBlockProductionTime(), sender, transaction);
    MutableChainState activeState = nextState.copy();

    try {
      Transaction innerTransaction = transaction.getTransaction();
      FeeCollector feeCollector =
          FeeCollector.create(activeState.getAccountPlugin(), transaction.getCost());
      long networkFeeForSignedTransaction = transaction.getCost() - remaining;
      feeCollector.registerNetworkFee(transaction.identifier(), networkFeeForSignedTransaction);

      EventCollector eventManager =
          new EventCollector(
              subChainId,
              activeState,
              committeeId,
              governanceVersion,
              transaction.identifier(),
              0,
              null,
              feeCollector);
      eventManager.createEventTransaction(
          sender, innerTransaction, feeCollector.remaining(), false);
      feeCollector.handleCollectedFees(
          (surplus, usage) -> {
            // We subtract the network cost for the signed transaction because it is included in the
            // network fee map
            long gas = surplus + usage - networkFeeForSignedTransaction;
            activeState.registerBlockchainUsage(getBlockProductionTime(), gas);
          });
      nextState.apply(activeState);
      return ExecutedTransactionEvents.success(
          transaction, eventManager.getTransactions(), feeCollector.getTransactionCost());
    } catch (Exception e) {
      if (nextState.features().hasRegisterGasForFailingSignedTransaction()) {
        nextState.registerBlockchainUsage(blockProductionTime, remaining);
      }
      return ExecutedTransactionEvents.fail(transaction, e);
    }
  }

  /**
   * Execute this event transaction modifying the chain state for a contract that has not been iced.
   *
   * @param nextState current state of the blockchain before execution of this transaction
   * @param event the event transaction to execute
   * @return list of created events if success, null otherwise
   */
  public ExecutedTransactionEvents execute(MutableChainState nextState, ExecutableEvent event) {
    return execute(nextState, event, false);
  }

  /**
   * Execute this event transaction modifying the chain state.
   *
   * @param nextState current state of the blockchain before execution of this transaction
   * @param event the event transaction to execute
   * @param iced if the contract to execute the event is iced
   * @return list of created events if success, null otherwise
   */
  public ExecutedTransactionEvents execute(
      MutableChainState nextState, ExecutableEvent event, boolean iced) {
    EventTransaction eventTransaction = event.getEvent();
    nextState.bumpInboundShardNonce(event.getOriginShard(), eventTransaction.getCommitteeId());
    InnerEvent inner = eventTransaction.getInner();
    try {
      if (inner.getEventType() == EventType.CALLBACK) {
        CallbackToContract callbackToContract = (CallbackToContract) inner;
        return handleCallback(event, nextState, callbackToContract);
      } else if (inner.getEventType() == EventType.TRANSACTION) {
        if (iced) {
          nextState.updateContractBalance(
              getBlockProductionTime(), inner.target(), ((InnerTransaction) inner).getCost());
          return handleEventError(
              event, nextState, eventTransaction, new Throwable("Contract is iced"));
        } else {
          return handleTransaction(event, nextState, (InnerTransaction) inner);
        }
      } else /* if(inner.getType() == EventType.SYSTEM) */ {
        return handleSystemEvent(event, nextState, eventTransaction, (InnerSystemEvent) inner);
      }
    } catch (VirtualMachineError e) {
      throw e;
    } catch (Throwable e) {
      if (inner.getEventType() == EventType.CALLBACK) {
        CallbackToContract callbackToContract = (CallbackToContract) inner;
        return handleCallBackError(event, nextState, callbackToContract, eventTransaction, e);
      } else if (inner.getEventType() == EventType.TRANSACTION) {
        return handleTransactionError(event, nextState, eventTransaction, e);
      } else /* if(inner.getType() == EventType.SYSTEM) */ {
        return handleEventError(event, nextState, eventTransaction, e);
      }
    }
  }

  private ExecutedTransactionEvents handleEventError(
      ExecutableEvent event,
      MutableChainState nextState,
      EventTransaction eventTransaction,
      Throwable throwable) {
    EventCollector eventManager =
        createSimpleEventManager(
            eventTransaction.getOriginatingTransaction(), nextState, eventTransaction.getHeight());
    eventManager.checkCallbackFailing(event.identifier(), eventTransaction.getReturnEnvelope());
    return ExecutedTransactionEvents.fail(event, eventManager.getTransactions(), throwable);
  }

  private ExecutedTransactionEvents handleTransactionError(
      ExecutableEvent event,
      MutableChainState nextState,
      EventTransaction eventTransaction,
      Throwable throwable) {
    InnerEvent inner = eventTransaction.getInner();
    nextState.registerBlockchainUsage(
        getBlockProductionTime(), ((InnerTransaction) inner).getCost());
    return handleEventError(event, nextState, eventTransaction, throwable);
  }

  private ExecutedTransactionEvents handleCallBackError(
      ExecutableEvent event,
      MutableChainState nextState,
      CallbackToContract callbackToContract,
      EventTransaction eventTransaction,
      Throwable throwable) {
    nextState.registerBlockchainUsage(getBlockProductionTime(), callbackToContract.getCost());
    EventCollector eventManager =
        createSimpleEventManager(
            eventTransaction.getOriginatingTransaction(), nextState, eventTransaction.getHeight());
    nextState.setCallbackResult(
        callbackToContract.target(), callbackToContract.callbackIdentifier(), false);
    nextState.callbackExecutionComplete(
        callbackToContract.target(),
        callbackToContract.callbackIdentifier(),
        eventManager::createSystemEvent,
        new byte[0]);
    return ExecutedTransactionEvents.fail(event, eventManager.getTransactions(), throwable);
  }

  private ExecutedTransactionEvents handleCallback(
      ExecutableEvent event, MutableChainState nextState, CallbackToContract callbackToContract) {
    MutableChainState activeState = nextState.copy();
    final EventExecutor eventExecutor =
        new EventExecutor(
            subChainId,
            event,
            callbackToContract,
            activeState,
            committeeId,
            feeConsumer(activeState, callbackToContract.target()));
    ContractState.CallbackInfo callbackInfo = eventExecutor.execute(this);
    if (callbackInfo == null) {
      EventCollector manager = eventExecutor.eventManager.createCollector();
      activeState.callbackExecutionComplete(
          callbackToContract.target(),
          callbackToContract.callbackIdentifier(),
          manager::createSystemEvent,
          Objects.requireNonNullElse(eventExecutor.returnValue, new byte[0]));
    } else {
      activeState.callbackExecutionUpdated(
          callbackToContract.target(), callbackToContract.callbackIdentifier(), callbackInfo);
    }

    List<ExecutableEvent> result = eventExecutor.eventManager.getTransactions();
    nextState.apply(activeState);
    return ExecutedTransactionEvents.success(
        event, result, eventExecutor.feeCollector.getTransactionCost());
  }

  private ExecutedTransactionEvents handleSystemEvent(
      ExecutableEvent event,
      MutableChainState nextState,
      EventTransaction eventTransaction,
      InnerSystemEvent innerSystemEvent) {
    MutableChainState activeState = nextState.copy();
    EventCollector eventManager =
        createSimpleEventManager(
            eventTransaction.getOriginatingTransaction(),
            activeState,
            event.getEvent().getHeight());
    if (innerSystemEvent.getSystemEventType().governanceEvent) {
      if (subChainId == null) {
        for (String activeShard : activeState.getActiveShards()) {
          governanceSender(activeState, eventManager).accept(activeShard, innerSystemEvent);
        }
      }
      activeState.bumpGovernanceVersion();
    }

    byte[] returnValue =
        Objects.requireNonNullElse(
            innerSystemEvent.execute(
                subChainId,
                governanceSender(activeState, eventManager),
                eventManager::createCallback,
                activeState,
                blockProductionTime),
            InnerSystemEvent.EMPTY_RETURN_VALUE);

    ReturnEnvelope returnEnvelope = eventTransaction.getReturnEnvelope();
    createCallbackEvent(eventManager, returnValue, returnEnvelope, event.identifier());

    nextState.apply(activeState);
    return ExecutedTransactionEvents.success(event, eventManager.getTransactions(), null);
  }

  private void createCallbackEvent(
      EventCollector eventManager,
      byte[] returnValue,
      ReturnEnvelope returnEnvelope,
      Hash identifier) {
    if (returnEnvelope != null) {
      InnerSystemEvent.CallbackEvent callbackEvent =
          new InnerSystemEvent.CallbackEvent(
              returnEnvelope,
              identifier,
              true,
              Objects.requireNonNullElse(returnValue, new byte[0]));
      eventManager.createSystemEvent(callbackEvent);
    }
  }

  private <T extends InnerEvent> BiConsumer<String, T> governanceSender(
      MutableChainState nextState, EventCollector eventManager) {
    return (shard, event) -> {
      ShardRoute shardRoute = nextState.routeToShard(shard);
      eventManager.createGovernanceEvent(shardRoute, event);
    };
  }

  private ExecutedTransactionEvents handleTransaction(
      ExecutableEvent event, MutableChainState nextState, InnerTransaction innerTransaction) {
    MutableChainState activeState = nextState.copy();
    final EventExecutor eventExecutor =
        new EventExecutor(
            subChainId,
            event,
            innerTransaction,
            activeState,
            committeeId,
            feeConsumer(activeState, innerTransaction.target()));
    ContractState.CallbackInfo callbackInfo = eventExecutor.execute(this);
    Hash currentTransaction = event.identifier();
    ReturnEnvelope returnEnvelope = event.getEvent().getReturnEnvelope();
    List<ExecutableEvent> eventTransactions = eventExecutor.eventManager.getTransactions();
    if (callbackInfo != null) {
      activeState.addCallbacks(
          innerTransaction.getSender(),
          innerTransaction.target(),
          returnEnvelope,
          currentTransaction,
          callbackInfo);
    } else {
      EventCollector eventManager =
          createSimpleEventManager(
              event.getEvent().getOriginatingTransaction(),
              activeState,
              event.getEvent().getHeight());
      createCallbackEvent(
          eventManager, eventExecutor.returnValue, returnEnvelope, event.identifier());
      eventTransactions.addAll(eventManager.getTransactions());
    }
    nextState.apply(activeState);
    return ExecutedTransactionEvents.success(
        event, eventTransactions, eventExecutor.feeCollector.getTransactionCost());
  }

  private BiConsumer<Long, Long> feeConsumer(
      MutableChainState activeState, BlockchainAddress contract) {
    return (contractBalanceUpdate, usage) -> {
      activeState.updateContractBalance(getBlockProductionTime(), contract, contractBalanceUpdate);
      activeState.registerBlockchainUsage(getBlockProductionTime(), usage);
    };
  }

  private EventCollector createSimpleEventManager(
      Hash originatingTransaction, Routing state, int currentHeight) {
    return new EventCollector(
        subChainId, state, committeeId, governanceVersion, originatingTransaction, currentHeight);
  }

  /**
   * Execute an event while the chain is in sync state.
   *
   * @param nextState current state of the blockchain before execution of this transaction
   * @param executableEvent the event to execute
   * @return execution status with created events
   */
  public ExecutedTransactionEvents executeInSync(
      MutableChainState nextState, ExecutableEvent executableEvent) {
    EventTransaction event = executableEvent.getEvent();
    nextState.bumpInboundShardNonce(executableEvent.getOriginShard(), event.getCommitteeId());

    InnerEvent inner = event.getInner();
    EventType eventType = inner.getEventType();
    String originShard = executableEvent.getOriginShard();
    if (eventType == EventType.SYNC) {
      SyncEvent sync = (SyncEvent) inner;
      nextState.incomingSync(originShard, sync);
      return ExecutedTransactionEvents.success(executableEvent, List.of(), null);
    }
    return ExecutedTransactionEvents.success(
        executableEvent, List.of(reRouteEvent(nextState, event, inner)), null);
  }

  private ExecutableEvent reRouteEvent(Routing routing, EventTransaction event, InnerEvent inner) {
    ShardRoute shardRoute = determineRerouteShard(inner, routing);
    return new ExecutableEvent(
        subChainId,
        new EventTransaction(
            event.getOriginatingTransaction(),
            shardRoute,
            committeeId,
            governanceVersion,
            event.getHeight(),
            event.getReturnEnvelope(),
            inner));
  }

  private ShardRoute determineRerouteShard(InnerEvent inner, Routing routing) {
    boolean withoutTarget = inner.target() == null;
    if (withoutTarget) {
      return routing.routeToShard(subChainId);
    } else {
      return routing.routeToShard(inner.target());
    }
  }

  /**
   * Get the id of the chain.
   *
   * @return the chain id
   */
  public String getChainId() {
    return chainId;
  }

  private static final class EventExecutor {

    private final ContractExecution contractExecution;
    private final Hash currentTransactionHash;
    private final Hash originatingTransaction;
    private final BlockchainAddress sender;
    private final ExecutionEventManager eventManager;
    private final MutableChainState activeState;
    private final FeeCollector feeCollector;
    private final BiConsumer<Long, Long> feeConsumer;
    private final BlockchainAddress serviceFeesPayer;
    private byte[] returnValue;

    private EventExecutor(
        String subChainId,
        ExecutableEvent event,
        ContractExecution contractExecution,
        BlockchainAddress targetContract,
        BlockchainAddress sender,
        long maximumCost,
        MutableChainState activeState,
        long committeeId,
        BiConsumer<Long, Long> feeConsumer) {
      this.originatingTransaction = event.getEvent().getOriginatingTransaction();
      this.contractExecution = contractExecution;
      this.sender = sender;
      this.currentTransactionHash = event.identifier();
      this.activeState = activeState;
      this.feeCollector = FeeCollector.create(activeState.getAccountPlugin(), maximumCost);
      this.feeConsumer = feeConsumer;
      this.serviceFeesPayer = targetContract;
      Supplier<EventCollector> eventCollectorCreator =
          () ->
              new EventCollector(
                  subChainId,
                  activeState,
                  committeeId,
                  activeState.getGovernanceVersion(),
                  originatingTransaction,
                  event.getEvent().getHeight(),
                  targetContract,
                  feeCollector);
      this.eventManager = new ExecutionEventManager(eventCollectorCreator);
    }

    EventExecutor(
        String subChainId,
        ExecutableEvent event,
        InnerTransaction innerTransaction,
        MutableChainState activeState,
        long committeeId,
        BiConsumer<Long, Long> feeConsumer) {
      this(
          subChainId,
          event,
          innerTransaction.getTransaction(),
          innerTransaction.getTransaction().getTargetContract(),
          innerTransaction.getSender(),
          innerTransaction.getCost(),
          activeState,
          committeeId,
          feeConsumer);
    }

    EventExecutor(
        String subChainId,
        ExecutableEvent event,
        CallbackToContract callbackToContract,
        MutableChainState activeState,
        long committeeId,
        BiConsumer<Long, Long> feeConsumer) {
      this(
          subChainId,
          event,
          callbackToContract,
          callbackToContract.target(),
          callbackToContract.from(),
          callbackToContract.getCost(),
          activeState,
          committeeId,
          feeConsumer);
    }

    ContractState.CallbackInfo execute(ExecutionContext context) {
      ExecutionContextTransaction executionContext =
          ExecutionContextTransaction.create(
              context,
              activeState,
              currentTransactionHash,
              originatingTransaction,
              sender,
              eventManager,
              feeCollector);
      EventResult<? extends BinderEvent> execute = contractExecution.execute(executionContext);
      final ContractState.CallbackInfo callbacks = processEvents(executionContext, execute);
      feeCollector.handleCollectedFees(feeConsumer);
      activeState.payServiceFees(
          context.getBlockProductionTime(),
          executionContext.getPendingServiceFees(),
          serviceFeesPayer);
      activeState.payInfrastructureFees(
          context.getBlockProductionTime(), executionContext.getPendingInfrastructureFees());
      activeState.payByocServiceFees(
          context.getBlockProductionTime(), executionContext.getPendingByocFees());
      logger.debug("Transaction ({}) cost details: {}", currentTransactionHash, feeCollector);
      return callbacks;
    }

    ContractState.CallbackInfo processEvents(
        ExecutionContextTransaction executionContext,
        EventResult<? extends BinderEvent> groupList) {
      ExecutionEventManager executionEventManager = executionContext.getExecutionEventManager();

      EventCollector collector = executionEventManager.createCollector();
      collectEvents(collector, executionContext.getFrom(), groupList.invocations);
      if (groupList.result instanceof CallReturnValue callReturn) {
        this.returnValue = callReturn.getResult();
      } else if (groupList.result instanceof BinderEventGroup<?> eventGroup) {
        collector = executionEventManager.createCollector();
        collector.setCallback(
            eventGroup.getCallbackCost(), eventGroup.isCallbackCostFromContract());
        collectEvents(collector, executionContext.getFrom(), eventGroup.getEvents());
        List<Hash> events =
            collector.getTransactions().stream().map(ExecutableEvent::identifier).toList();
        if (events.isEmpty()) {
          throw new IllegalStateException("Unable to create callbacks");
        }
        ContractState.CallbackInfo info =
            ContractState.CallbackInfo.create(
                events,
                eventGroup.getCallbackCost(),
                new LargeByteArray(eventGroup.getCallbackRpc()));
        return info;
      }
      return null;
    }

    private void collectEvents(
        EventCollector collector, BlockchainAddress from, List<? extends BinderEvent> invocations) {
      EventSender eventSender = new EventSender(from, collector.getCurrentContract());
      for (BinderEvent executableEvent : invocations) {
        collector.process(executableEvent, eventSender);
      }
    }
  }
}
