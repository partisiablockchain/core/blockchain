package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class HashStorageImplTest {

  private final Map<MemoryStorage.PathAndHash, byte[]> storageMap = new HashMap<>();
  private final MemoryStorage memoryStorage = new MemoryStorage(storageMap, "", true);

  private final HashStorageImpl<?> hashStorage = new HashStorageImpl<>(memoryStorage, null);

  /** The result of querying isEmpty() depends on the underlying storage. */
  @Test
  void isEmpty() {
    Assertions.assertThat(hashStorage.isEmpty()).isTrue();

    storageMap.put(
        new MemoryStorage.PathAndHash("", Hash.create(s -> s.writeLong(0L))), new byte[0]);

    Assertions.assertThat(hashStorage.isEmpty()).isFalse();
  }
}
