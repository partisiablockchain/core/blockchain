package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.PayServiceFeesResult;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.util.MathUtil;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/** Test version of account plugin. */
@Immutable
public final class AccountHolderAccountPlugin
    extends BlockchainAccountPlugin<
        GasAndCoinFeePluginGlobal, AccumulatedFees, FeeState, ContractStorage> {

  @Override
  public long convertNetworkFee(GasAndCoinFeePluginGlobal globalState, long bytes) {
    return bytes;
  }

  @Override
  public PayCostResult<AccumulatedFees, FeeState> payCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    long fees = transaction.getCost();
    ensurePositiveFee(fees);
    FeeState account = localState.account;
    if (fees == 0) {
      // Free transaction
      FeeState feeState = account.incrementFreeTransactions(pluginContext.blockProductionTime());
      return new PayCostResult<>(new AccountState<>(localState.local, feeState), 0);
    } else if (account == null || account.getBalance() < fees) {
      throw new IllegalStateException("Calls to this function should be guarded with canCoverFee");
    } else {
      long networkFee = convertNetworkFee(globalState, transaction.computeNetworkByteCount());
      AccumulatedFees newLocal =
          localState
              .local
              .addFees(fees)
              .registerBlockchainUsage(networkFee, pluginContext.blockProductionTime());
      long remainingGas = fees - networkFee;
      if (remainingGas < 0) {
        throw new RuntimeException(
            "Transaction cannot cover network fee ("
                + networkFee
                + "). Should be guarded by 'canCoverCost'");
      }
      return new PayCostResult<>(
          new AccountState<>(newLocal, account.updatedBalance(-fees)), remainingGas);
    }
  }

  private void ensurePositiveFee(long fees) {
    if (fees < 0) {
      throw new RuntimeException("Fee cannot be negative.");
    }
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> contractCreated(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      BlockchainAddress address) {
    long latestStorageFeeTime = latestStorageFeeTime(address, pluginContext.blockProductionTime());
    ContractStorage contractStorage = new ContractStorage(latestStorageFeeTime, 0L);
    return new ContractState<>(contextFreeState, contractStorage);
  }

  private long latestStorageFeeTime(BlockchainAddress address, long blockProductionTime) {
    if (address.getType() == BlockchainAddress.Type.CONTRACT_GOV) {
      return -1L;
    } else {
      return blockProductionTime;
    }
  }

  @Override
  public AccumulatedFees removeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState) {
    ContractStorage account =
        Objects.requireNonNullElseGet(localState.contract, ContractStorage::new);
    return localState.local.registerBlockchainUsage(
        account.getBalance(), pluginContext.blockProductionTime());
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateContractGasBalance(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState,
      BlockchainAddress contract,
      long gas) {
    ContractStorage storage =
        Objects.requireNonNullElseGet(localState.contract, ContractStorage::new);
    final long balance = storage.getBalance();
    if (balance + gas < 0) {
      throw new RuntimeException("Contract balance too low");
    }
    return new ContractState<>(localState.local, storage.updateBalance(gas));
  }

  @Override
  public AccumulatedFees registerBlockchainUsage(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      long gas) {
    long epoch = convertBlockProductionTimeToEpoch(pluginContext.blockProductionTime());
    return contextFreeState.registerBlockchainUsage(gas, epoch);
  }

  @Override
  public PayServiceFeesResult<ContractState<AccumulatedFees, ContractStorage>> payServiceFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> contractState,
      List<PendingFee> pendingFees) {
    if (pendingFees.isEmpty()) {
      throw new RuntimeException("Pending fees must not be empty at this time!");
    }
    long sum = pendingFees.stream().mapToLong(PendingFee::getFee).sum();

    ContractStorage contractStorage = contractState.contract;
    AccumulatedFees updatedFees = contractState.local;

    long contractBalance = contractStorage.getBalance();
    if (contractBalance >= sum) {
      ContractStorage updatedStorage = contractStorage.updateBalance(-sum);
      for (PendingFee pendingFee : pendingFees) {
        updatedFees = updatedFees.payServiceFees(pendingFee.getNode(), pendingFee.getFee());
      }
      return PayServiceFeesResult.coveredFee(new ContractState<>(updatedFees, updatedStorage));
    } else {
      List<Long> weights =
          pendingFees.stream().map(PendingFee::getFee).collect(Collectors.toList());
      List<Long> equalShares = distributeContractGas(contractBalance, weights);
      for (int i = 0; i < pendingFees.size(); i++) {
        BlockchainAddress node = pendingFees.get(i).getNode();
        updatedFees = updatedFees.payServiceFees(node, equalShares.get(i));
      }
      return PayServiceFeesResult.failedToCoverFees(
          new ContractState<>(updatedFees, contractStorage.updateBalance(-contractBalance)));
    }
  }

  @Override
  public AccumulatedFees payInfrastructureFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFree,
      List<PendingFee> pendingFees) {
    if (pendingFees.isEmpty()) {
      throw new RuntimeException("Pending fees must not be empty at this time!");
    }
    long sum = pendingFees.stream().mapToLong(PendingFee::getFee).sum();
    AccumulatedFees updatedFees = contextFree;
    for (PendingFee pendingFee : pendingFees) {
      updatedFees = updatedFees.payServiceFees(pendingFee.getNode(), pendingFee.getFee());
    }
    return updatedFees.addInfrastructureFees(sum);
  }

  private List<Long> distributeContractGas(long gas, List<Long> weights) {
    List<Long> result = new ArrayList<>();
    BigInteger available = BigInteger.valueOf(gas);
    BigInteger weightsSum = BigInteger.valueOf(weights.stream().mapToLong(Long::longValue).sum());

    for (Long w : weights) {
      BigInteger weight = BigInteger.valueOf(w);
      BigInteger scaledGas = weight.multiply(available);
      long distribution = scaledGas.divide(weightsSum).longValue();

      result.add(distribution);
      weightsSum = weightsSum.subtract(weight);
      available = available.subtract(BigInteger.valueOf(distribution));
    }

    return result;
  }

  @Override
  public boolean canCoverCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    long cost = transaction.getCost();
    if (cost == 0) {
      return pluginContext.blockProductionTime() > 0;
    }
    if (cost < transaction.computeNetworkByteCount()) {
      return false;
    } else {
      FeeState account = Objects.requireNonNullElseGet(localState.account, FeeState::create);
      return account.getBalance() >= cost && !account.hasDebt();
    }
  }

  @Override
  public AccumulatedFees updateForBlock(
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      BlockContext<ContractStorage> context) {
    for (BlockchainAddress blockchainAddress : context.contracts().keySet()) {
      if (context.currentBlock().getBlockTime() % 256 != 0) {
        context.registerContractUpdate(blockchainAddress);
      }
    }
    return localState;
  }

  private long convertBlockProductionTimeToEpoch(long blockProductionTime) {
    return (long) (blockProductionTime / Math.pow(2, 21));
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateActiveContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> currentState,
      BlockchainAddress contract,
      long size) {
    try {
      ContractStorage contractStorage = currentState.contract;
      if (contractStorage != null
          && contractStorage.getTimestampForLatestStorageFeePayment() != -1) {
        long interval =
            pluginContext.blockProductionTime()
                - contractStorage.getTimestampForLatestStorageFeePayment();
        long fee = computeStorageGasPrice(interval, size);
        long balance = contractStorage.getBalance();
        // We have some unittests that assumes failure due to a negative fee calculation
        if (fee > balance || fee < 0) {
          return null;
        } else {
          return new ContractState<>(
              currentState.local.addFees(fee), contractStorage.updateBalance(-fee));
        }
      }
    } catch (Exception e) {
      return null;
    }
    return currentState;
  }

  @Override
  public GasAndCoinFeePluginGlobal migrateGlobal(
      StateAccessor currentGlobal, SafeDataInputStream rpc) {
    return new GasAndCoinFeePluginGlobal(null, null, false, false);
  }

  @Override
  protected AccumulatedFees migrateContextFree(StateAccessor current) {
    return AccumulatedFees.create();
  }

  @Override
  protected ContractStorage migrateContract(BlockchainAddress address, StateAccessor current) {
    return null;
  }

  @Override
  protected FeeState migrateAccount(StateAccessor current) {
    return null;
  }

  @Override
  protected InvokeResult<ContractState<AccumulatedFees, ContractStorage>> invokeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      ContractState<AccumulatedFees, ContractStorage> current,
      BlockchainAddress address,
      byte[] invocationBytes) {
    SafeDataInputStream rpc = SafeDataInputStream.createFromBytes(invocationBytes);
    if (rpc.readBoolean()) {
      ContractStorage contractStorage =
          Objects.requireNonNullElseGet(current.contract, ContractStorage::new);
      return new InvokeResult<>(
          new ContractState<>(current.local, contractStorage.updateBalance(rpc.readLong())), null);
    } else {
      long balance = current.contract == null ? 0 : current.contract.getBalance();
      return new InvokeResult<>(
          new ContractState<>(
              current.local, new ContractStorage(rpc.readLong(), balance + 10_000L)),
          null);
    }
  }

  @Override
  protected InvokeResult<AccountState<AccumulatedFees, FeeState>> invokeAccount(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccountState<AccumulatedFees, FeeState> current,
      byte[] invocationBytes) {
    SafeDataInputStream rpc = SafeDataInputStream.createFromBytes(invocationBytes);
    byte invocation = rpc.readSignedByte();
    if (invocation == 1) {
      long gasToCreate = rpc.readLong();
      return new InvokeResult<>(
          new AccountState<>(current.local, updateGasDistribution(current.account, gasToCreate)),
          null);
    } else if (invocation == 2) {
      return new InvokeResult<>(
          current,
          SafeDataOutputStream.serialize(
              returnValue -> returnValue.writeLong(getGasBalance(current.account))));
    } else if (invocation == 3) {
      return new InvokeResult<>(
          current,
          SafeDataOutputStream.serialize(
              returnValue -> returnValue.writeLong(current.account.getFreeTransactions())));
    } else if (invocation == 4) {
      return new InvokeResult<>(
          current,
          SafeDataOutputStream.serialize(
              returnValue -> returnValue.writeBoolean(current.account.getFreeTransactions() == 1)));
    } else {
      throw new IllegalArgumentException("Unable to enable storage for account");
    }
  }

  @Override
  protected InvokeResult<AccumulatedFees> invokeContextFree(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccumulatedFees contextFree,
      byte[] rpc) {
    boolean distribute = SafeDataInputStream.createFromBytes(rpc).readBoolean();
    byte[] returnValue = null;
    if (!distribute) {
      long registeredBlockchainUsage =
          contextFree.getRegisteredBlockchainUsage(pluginContext.blockProductionTime());
      returnValue = SafeDataOutputStream.serialize(s -> s.writeLong(registeredBlockchainUsage));
    }
    // fake distribution
    return new InvokeResult<>(
        contextFree.registerBlockchainUsage(
            -contextFree.getRegisteredBlockchainUsage(), pluginContext.blockProductionTime()),
        returnValue);
  }

  @Override
  protected AccumulatedFees payByocFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      Unsigned256 amount,
      String symbol,
      FixedList<BlockchainAddress> nodes) {
    return localState.registerByocFee(PendingByocFee.create(nodes, amount, symbol));
  }

  private FeeState updateGasDistribution(FeeState acc, long amount) {
    acc = Objects.requireNonNullElseGet(acc, FeeState::create);
    if (amount < 0 && -amount > acc.getBalance()) {
      long newDebt = -amount + acc.getBalance();
      return acc.setDebt(newDebt + acc.getDebt());
    } else {
      return acc.updatedBalance(amount);
    }
  }

  private long getGasBalance(FeeState acc) {
    acc = Objects.requireNonNullElseGet(acc, FeeState::create);
    return acc.getBalance();
  }

  @Override
  public InvokeResult<GasAndCoinFeePluginGlobal> invokeGlobal(
      PluginContext pluginContext, GasAndCoinFeePluginGlobal state, byte[] invocationBytes) {
    SafeDataInputStream rpc = SafeDataInputStream.createFromBytes(invocationBytes);
    byte invocation = rpc.readSignedByte();
    if (invocation == 1) {
      BlockchainAddress next = BlockchainAddress.read(rpc);
      return new InvokeResult<>(state.setZkFeeAccount(next), new byte[0]);
    } else if (invocation == 2) {
      long dummyValue = rpc.readLong();
      return new InvokeResult<>(
          state, SafeDataOutputStream.serialize(s -> s.writeLong(dummyValue)));
    } else {
      BlockchainAddress next = BlockchainAddress.read(rpc);
      return new InvokeResult<>(state.setFeeAccount(next), new byte[0]);
    }
  }

  private long computeStorageGasPrice(long interval, long count) {
    long millisPerHour = 1000L * 60 * 60;
    return MathUtil.ceilDiv(interval * count, millisPerHour);
  }

  @Override
  public List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of(AccumulatedFees.class, FeeState.class, ContractStorage.class);
  }

  @Override
  public Class<GasAndCoinFeePluginGlobal> getGlobalStateClass() {
    return GasAndCoinFeePluginGlobal.class;
  }
}
