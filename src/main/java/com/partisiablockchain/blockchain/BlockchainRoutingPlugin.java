package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** The blockchain routing plugin which handles routing to shards. */
@Immutable
public abstract class BlockchainRoutingPlugin implements BlockchainPlugin<StateVoid, StateVoid> {

  /**
   * Determine which shard the target address belongs to.
   *
   * @param shards list of known shards
   * @param target the address to determine location of
   * @return the id of the shard where target is located or null if located on governance shard
   */
  public abstract String route(FixedList<String> shards, BlockchainAddress target);

  @Override
  public final Class<StateVoid> getGlobalStateClass() {
    return StateVoid.class;
  }

  @Override
  public final Class<StateVoid> getLocalStateClass() {
    return StateVoid.class;
  }

  @Override
  public final List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of();
  }

  @Override
  public final InvokeResult<StateVoid> invokeLocal(
      PluginContext pluginContext,
      StateVoid globalState,
      StateVoid state,
      BlockchainAddress invocationContext,
      byte[] rpc) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final InvokeResult<StateVoid> invokeGlobal(
      PluginContext pluginContext, StateVoid state, byte[] rpc) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final StateVoid migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
    return null;
  }

  @Override
  public final StateVoid migrateLocal(StateAccessor currentLocal) {
    return null;
  }
}
