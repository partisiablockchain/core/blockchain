package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.thread.ThreadedLoop;
import java.util.function.LongSupplier;

/** Monitor for liveness, based on sleeping followed by notify. */
@SuppressWarnings("WeakerAccess")
public final class LivenessMonitor implements AutoCloseable {

  private long lastNotify;
  private final ThreadedLoop loop;

  /**
   * Create a new liveness monitor. If nothing has happened for timeout millis the onTimeOut is
   * invoked. Will continue to notify with an interval of timeout as long as no new event has
   * happened.
   *
   * @param onTimeOut the runnable to invoke when latest event is more than timeout ago
   * @param latestEvent timestamp for latest event
   * @param timeout the time to wait after an event before notifying
   */
  public LivenessMonitor(Runnable onTimeOut, LongSupplier latestEvent, long timeout) {
    this(onTimeOut, latestEvent, timeout, timeout);
  }

  /**
   * Create a new liveness monitor. If nothing has happened for timeout millis the onTimeOut is
   * invoked. Will continue to notify with an interval of timeout as long as no new event has
   * happened.
   *
   * @param onTimeOut the runnable to invoke when latest event is more than timeout ago
   * @param latestEvent timestamp for latest event
   * @param initialTimeout the minimum amount of time to wait before triggering first notify
   * @param timeout the time to wait after an event before notifying
   */
  public LivenessMonitor(
      Runnable onTimeOut, LongSupplier latestEvent, long initialTimeout, long timeout) {
    lastNotify = calculateInitialNotify(System.currentTimeMillis(), initialTimeout, timeout);
    this.loop =
        ThreadedLoop.create(
                () -> {
                  long latest = Math.max(lastNotify, latestEvent.getAsLong());
                  long now = System.currentTimeMillis();
                  long untilTimeout = computeRemaining(timeout, latest, now);
                  if (!sleep(untilTimeout)) {
                    lastNotify = now;
                    onTimeOut.run();
                  }
                },
                "LivenessMonitor")
            .start();
  }

  static long calculateInitialNotify(long now, long initialTimeout, long timeout) {
    long initialNotifyOffset = initialTimeout - timeout;
    return now + initialNotifyOffset;
  }

  static long computeRemaining(long timeout, long latest, long now) {
    long elapsed = now - latest;
    return timeout - elapsed;
  }

  static boolean sleep(long untilTimeout) throws InterruptedException {
    if (untilTimeout >= 1) {
      Thread.sleep(untilTimeout);
      return true;
    } else {
      return false;
    }
  }

  boolean isRunning() {
    return loop.isRunning();
  }

  @Override
  public void close() {
    loop.close();
  }
}
