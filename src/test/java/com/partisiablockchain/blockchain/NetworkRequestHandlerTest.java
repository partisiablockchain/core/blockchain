package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.BlockRequest;
import com.partisiablockchain.flooding.BlockResponse;
import com.partisiablockchain.flooding.BlockResponse.BlockPair;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed.BlockPairSemiCompressed;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.NetworkNode.IncomingPacket;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.flooding.SyncRequest;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.TestExecutionQueue;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/** Test. */
public final class NetworkRequestHandlerTest extends CloseableTest {

  @Captor private ArgumentCaptor<Packet<?>> captor;

  private final KeyPair producer = new KeyPair(BigInteger.ONE);
  private final Connection mockedConnection = Mockito.mock(Connection.class);
  private BlockchainLedger blockchainLedger;
  private NetworkRequestHandler requestHandler;
  private Block latestBlockBefore;
  private BlockchainTestHelper helper;
  private String chainId;
  private BlockRequester requester;
  private int requestCount = 0;
  private ExecutionFactoryFake executionFactory;

  /** Setup default things. */
  @BeforeEach
  public void setUp() {
    executionFactory = ExecutionFactoryFake.create();
    BlockchainTestHelper helper = new BlockchainTestHelper(temporaryFolder, executionFactory);
    helper.appendBlock(producer);
    setupForHelper(helper);

    register(MockitoAnnotations.openMocks(this));
  }

  private void setupForHelper(BlockchainTestHelper helper) {
    this.helper = helper;
    this.blockchainLedger = register(helper.blockchainLedger);
    this.latestBlockBefore = blockchainLedger.getLatestBlock();
    this.chainId = helper.blockchainLedger.getChainId();
    this.requester = new BlockRequester(100_000);
    requester.init(p -> requestCount++);
    this.requestHandler =
        new NetworkRequestHandler(requester, blockchainLedger, Runnable::run, chainId);
  }

  @Test
  public void finalizationData() {
    AtomicReference<FinalizationData> receivedData = new AtomicReference<>();
    blockchainLedger.attachFinalizationListener(
        (connection, data) -> {
          Assertions.assertThat(connection).isEqualTo(mockedConnection);
          receivedData.set(data);
        });

    byte[] expectedData = new byte[22];
    FinalizationData finalizationData = FinalizationData.create(expectedData);
    add(new Packet<>(Packet.Type.FINALIZATION, finalizationData));

    TestExecutionQueue listenerQueue =
        executionFactory.getExecutionQueue(BlockchainLedger.ledgerFinalizationThreadName("Gov"));
    Assertions.assertThat(listenerQueue.runStep()).isTrue();
    Assertions.assertThat(receivedData.get()).isNotNull();

    FinalizationData actual = receivedData.get();
    Assertions.assertThat(actual.getData()).isEqualTo(expectedData);
  }

  @Test
  public void getBlock() {
    add(
        new Packet<>(
            Packet.Type.BLOCK_REQUEST, new BlockRequest(latestBlockBefore.getBlockTime())));

    verifyBlockResponse();
  }

  private void verifyBlockResponse() {
    Mockito.verify(mockedConnection).send(captor.capture());
    Packet<?> packet = captor.getValue();
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_RESPONSE);
    BlockResponse response = (BlockResponse) packet.getPayload();
    Assertions.assertThat(response.getSenderBlockTime())
        .isEqualTo(latestBlockBefore.getBlockTime());
    Assertions.assertThat(response.getBlocks()).hasSize(1);
    Assertions.assertThat(response.getBlocks().get(0).getBlock().getBlock())
        .isEqualTo(latestBlockBefore);
    Assertions.assertThat(response.getBlocks().get(0).getTransactions()).isEmpty();
  }

  @Test
  public void blockResponse() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, latestBlock.getProductionTime() + 100_000, 10_000),
                createTransaction())
            .sign(producer, chainId);
    FinalBlock finalBlock = helper.createFinalBlock(producer, List.of(transaction));

    // Make sure we have an outstanding request for the response block time
    requester.requestBlock(finalBlock.getBlock().getBlockTime());
    add(
        new Packet<>(
            Packet.Type.BLOCK_RESPONSE,
            new BlockResponse(
                finalBlock.getBlock().getBlockTime() + 1,
                List.of(new BlockPair(finalBlock, List.of(transaction), List.of(), chainId)),
                chainId)));

    assertBlockRequest(finalBlock.getBlock());
  }

  private void assertBlockRequest(Block block) {

    Assertions.assertThat(blockchainLedger.getLatestBlock()).isEqualTo(block);
    Mockito.verify(mockedConnection).send(captor.capture());
    Packet<?> packet = captor.getValue();
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_REQUEST);
    BlockRequest request = (BlockRequest) packet.getPayload();
    Assertions.assertThat(request.getBlockTime()).isEqualTo(block.getBlockTime() + 1);
  }

  @Test
  public void blockResponseCompressed() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, latestBlock.getProductionTime() + 100_000, 10_000),
                createTransaction())
            .sign(producer, chainId);
    FinalBlock finalBlock = helper.createFinalBlock(producer, List.of(transaction));

    // Make sure we have an outstanding request for the response block time
    requester.requestBlock(finalBlock.getBlock().getBlockTime());
    add(
        new Packet<>(
            Packet.Type.COMPRESSED_BLOCK_RESPONSE,
            new BlockResponseSemiCompressed(
                finalBlock.getBlock().getBlockTime() + 1,
                List.of(
                    new BlockPairSemiCompressed(
                        finalBlock, List.of(transaction), List.of(), chainId)),
                chainId)));

    assertBlockRequest(finalBlock.getBlock());
  }

  @Test
  public void shouldNotClearBackoffWhenOnlyRequestingDataFromSelfWithBlockProposal() {
    BlockchainTestHelper helper =
        new BlockchainTestHelper(
            temporaryFolder,
            stateBuilder -> {
              StateHelper.initial(stateBuilder);
              BlockchainLedgerConsensusTest.configureConsensusWithRollback(stateBuilder);
            },
            ExecutionFactoryFake.create());
    BlockchainLedger blockchainLedger = helper.blockchainLedger;
    setupForHelper(helper);
    Block expectedBlock = helper.createBlock(List.of());

    requester.requestBlock(expectedBlock.getBlockTime());

    blockchainLedger.appendBlock(new FinalBlock(expectedBlock, new byte[1]));

    long requestBlockTime = blockchainLedger.getBlockTime() + 1;
    Assertions.assertThat(blockchainLedger.getProposals()).hasSize(1);

    add(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(requestBlockTime)));
    Mockito.verify(mockedConnection).send(captor.capture());

    Packet<?> packet = captor.getValue();
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_RESPONSE);

    Assertions.assertThat(requestCount).isEqualTo(1);
    add(packet);
    Assertions.assertThat(requestCount).isEqualTo(1);
    requester.requestBlock(requestBlockTime);
    Assertions.assertThat(requestCount).isEqualTo(1);
    BlockResponse blockResponse = (BlockResponse) packet.getPayload();
    Assertions.assertThat(requestCount).isEqualTo(1);

    Packet<BlockResponseSemiCompressed> semiCompressedPacket =
        new Packet<>(
            Packet.Type.COMPRESSED_BLOCK_RESPONSE,
            new BlockResponseSemiCompressed(
                blockResponse.getBlocks().get(0).getBlock().getBlock().getBlockTime(),
                List.of(
                    new BlockPairSemiCompressed(
                        blockResponse.getBlocks().get(0).getBlock(),
                        blockResponse.getBlocks().get(0).getTransactions(),
                        List.of(),
                        chainId)),
                chainId));
    add(semiCompressedPacket);
    Assertions.assertThat(requestCount).isEqualTo(1);
    requester.requestBlock(requestBlockTime);
    Assertions.assertThat(requestCount).isEqualTo(1);

    BlockchainLedger.BlockAndState blockAndState = blockchainLedger.getProposals().get(0);
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockAndState.getBlockTime() + 1,
            blockAndState.getBlockTime(),
            blockAndState.getBlock().identifier(),
            blockAndState.getState().getHash(),
            List.of(),
            List.of());
    blockchainLedger.appendBlock(new FinalBlock(block, new byte[1]));
    add(
        new Packet<>(
            Packet.Type.BLOCK_REQUEST, new BlockRequest(blockchainLedger.getBlockTime() + 1)));

    Assertions.assertThat(requestCount).isEqualTo(1);
    add(packet);
    add(semiCompressedPacket);
    Assertions.assertThat(requestCount).isEqualTo(1);
    requester.requestBlock(blockchainLedger.getBlockTime() + 1);
    Assertions.assertThat(requestCount).isEqualTo(2);
  }

  @Test
  public void emptyBlockResponse() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    add(
        new Packet<>(
            Packet.Type.BLOCK_RESPONSE,
            new BlockResponse(latestBlock.getBlockTime() + 1, List.of(), chainId)));

    assertBlockRequest(latestBlock);
  }

  @Test
  void onlyKnownProposalsCompressed() {
    Assertions.assertThat(requestHandler.onlyKnownProposalsCompressed(List.of())).isTrue();
  }

  @Test
  public void emptyBlockResponseCompressed() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    add(
        new Packet<>(
            Packet.Type.COMPRESSED_BLOCK_RESPONSE,
            new BlockResponseSemiCompressed(latestBlock.getBlockTime() + 1, List.of(), chainId)));
    assertBlockRequest(latestBlock);
  }

  @Test
  public void getBlockWithLargeBlockTime() {
    add(
        new Packet<>(
            Packet.Type.BLOCK_REQUEST, new BlockRequest(latestBlockBefore.getBlockTime() + 1)));

    Mockito.verify(mockedConnection).send(captor.capture());
    Packet<?> packet = captor.getValue();
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_RESPONSE);
    BlockResponse response = (BlockResponse) packet.getPayload();
    Assertions.assertThat(response.getSenderBlockTime())
        .isEqualTo(latestBlockBefore.getBlockTime());
    Assertions.assertThat(response.getBlocks()).hasSize(0);
  }

  @Test
  public void syncRequest() {
    add(SyncRequest.PACKET);

    verifyBlockResponse();
  }

  @Test
  public void shouldGetProposals() {
    BlockchainTestHelper helper =
        new BlockchainTestHelper(
            temporaryFolder,
            stateBuilder -> {
              StateHelper.initial(stateBuilder);
              BlockchainLedgerConsensusTest.configureConsensusWithRollback(stateBuilder);
            },
            ExecutionFactoryFake.create());
    BlockchainLedger blockchainLedger = helper.blockchainLedger;
    Block expectedBlock = helper.createBlock(List.of());
    blockchainLedger.appendBlock(new FinalBlock(expectedBlock, new byte[1]));
    Assertions.assertThat(blockchainLedger.getProposals()).hasSize(1);
    setupForHelper(helper);
    add(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(blockchainLedger.getBlockTime())));

    Mockito.verify(mockedConnection).send(captor.capture());
    Packet<?> packet = captor.getValue();
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_RESPONSE);
    BlockResponse response = (BlockResponse) packet.getPayload();
    Assertions.assertThat(response.getSenderBlockTime())
        .isEqualTo(latestBlockBefore.getBlockTime());
    Assertions.assertThat(response.getBlocks()).hasSize(2);
    Assertions.assertThat(response.getBlocks().get(0).getBlock().getBlock())
        .isEqualTo(latestBlockBefore);
    Assertions.assertThat(response.getBlocks().get(0).getTransactions()).isEmpty();
    Assertions.assertThat(response.getBlocks().get(1).getBlock().getBlock())
        .isEqualTo(expectedBlock);
  }

  private InteractWithContractTransaction createTransaction() {
    return InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[32]);
  }

  @Test
  public void blockResponseBoundary() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, latestBlock.getProductionTime() + 100_000, 10_000),
                createTransaction())
            .sign(producer, chainId);
    FinalBlock block = helper.createFinalBlock(producer, List.of(transaction));
    add(
        new Packet<>(
            Packet.Type.BLOCK_RESPONSE,
            new BlockResponse(
                block.getBlock().getBlockTime(),
                List.of(new BlockPair(block, List.of(transaction), List.of(), chainId)),
                chainId)));

    Assertions.assertThat(blockchainLedger.getLatestBlock()).isEqualTo(block.getBlock());

    Mockito.verifyNoMoreInteractions(mockedConnection);
  }

  @Test
  public void blockResponseCompressedBoundary() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, latestBlock.getProductionTime() + 100_000, 10_000),
                createTransaction())
            .sign(producer, chainId);
    FinalBlock block = helper.createFinalBlock(producer, List.of(transaction));
    add(
        new Packet<>(
            Packet.Type.COMPRESSED_BLOCK_RESPONSE,
            new BlockResponseSemiCompressed(
                block.getBlock().getBlockTime(),
                List.of(
                    new BlockPairSemiCompressed(block, List.of(transaction), List.of(), chainId)),
                chainId)));

    Assertions.assertThat(blockchainLedger.getLatestBlock()).isEqualTo(block.getBlock());

    Mockito.verifyNoMoreInteractions(mockedConnection);
  }

  private void add(Packet<?> packet) {
    requestHandler.newPacket(new IncomingPacket(mockedConnection, packet));
  }

  @Test
  public void block() {
    FinalBlock block = helper.createFinalBlock(producer, List.of());
    add(new Packet<>(Packet.Type.BLOCK, block));

    Assertions.assertThat(blockchainLedger.getLatestBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void transaction() {
    Block latestBlock = blockchainLedger.getLatestBlock();
    AccountState accountState =
        blockchainLedger.getAccountState(producer.getPublic().createAddress());
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    accountState.getNonce(), latestBlock.getProductionTime() + 1000, 1000L),
                InteractWithContractTransaction.create(
                    BlockchainAddress.fromString("010000000000000000000000000000000006666666"),
                    new byte[0]))
            .sign(producer, chainId);
    add(new Packet<>(Packet.Type.TRANSACTION, transaction));

    Assertions.assertThat(blockchainLedger.getPending())
        .containsEntry(transaction.identifier(), transaction);
  }

  @Test
  public void event() {
    ExecutableEvent executableEvent =
        new ExecutableEvent(
            "ShardId",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                TestObjects.ACCOUNT_ONE,
                10L,
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
                new ShardRoute(null, 27),
                0,
                1L));
    StateStorageRaw stateStorage = blockchainLedger.getStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            blockchainLedger.getChainId(),
            "ShardId",
            executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    Block block =
        new Block(10L, 1L, 0L, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of());
    FloodableEvent event =
        FloodableEvent.create(
            executableEvent,
            new FinalBlock(
                block, SafeDataOutputStream.serialize(producer.sign(block.identifier()))),
            blockchainLedger.getStateStorage());

    Assertions.assertThat(helper.network.queued).hasSize(1);
    add(new Packet<>(Packet.Type.EVENT, event));

    Assertions.assertThat(blockchainLedger.getPendingEvents()).hasSize(1);
    Assertions.assertThat(helper.network.queued).hasSize(2);

    helper.addEventToFinder(event.getExecutableEvent());
    add(new Packet<>(Packet.Type.COMPRESSED_EVENT, event.compress()));
    Assertions.assertThat(blockchainLedger.getPendingEvents()).hasSize(1);
    Assertions.assertThat(helper.network.queued).hasSize(2);
  }
}
