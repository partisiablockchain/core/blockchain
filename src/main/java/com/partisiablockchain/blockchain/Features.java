package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.tree.AvlTree;
import java.util.List;

/** Access to information about enabled features on the blockchain. */
public final class Features {

  private final AvlTree<String, String> features;

  /**
   * Construct a new instance.
   *
   * @param features the currently enabled features
   */
  Features(AvlTree<String, String> features) {
    this.features = features;
  }

  /** If enable accounts are automatically created when interacted with. */
  public static final String FEATURE_OPEN_ACCOUNT_CREATION = "OPEN_ACCOUNT_CREATION";

  /** If enabled we check that the blockchain address is an account address in getAccount. */
  public static final String FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT =
      "CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT";

  /** If enabled the remaining gas for a signed transaction. */
  public static final String REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION =
      "REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION";

  /** The list of known features in the current version of the blockchain. */
  static final List<String> KNOWN_FEATURES =
      List.of(
          FEATURE_OPEN_ACCOUNT_CREATION,
          FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT,
          REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION);

  /**
   * Is accounts automatically created when used.
   *
   * @return true if accounts should be automatically created
   */
  public boolean hasOpenAccountCreation() {
    return features.containsKey(FEATURE_OPEN_ACCOUNT_CREATION);
  }

  /**
   * Blocks account from being created if address is a contract address and has open account feature
   * is set.
   *
   * @return true if we should check that the address was a contract address before creating account
   *     if open account feature is set
   */
  public boolean hasCheckExistenceForContractWithOpenAccount() {
    return features.containsKey(FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT);
  }

  /**
   * Should the blockchain register remaining gas from a signed transaction as blockchain usage when
   * it fails to spawn event transaction.
   *
   * @return true if gas should be registered as blockchain usage
   */
  public boolean hasRegisterGasForFailingSignedTransaction() {
    return features.containsKey(REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION);
  }
}
