package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PendingByocFeeTest {

  @Test
  public void getters() {
    FixedList<BlockchainAddress> nodes =
        FixedList.create(List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO));
    PendingByocFee pending = PendingByocFee.create(nodes, Unsigned256.create(10L), "symbol");
    Assertions.assertThat(pending.getSymbol()).isEqualTo("symbol");
    Assertions.assertThat(pending.getAmount()).isEqualTo(Unsigned256.create(10L));
    Assertions.assertThat(pending.getNodes()).isEqualTo(nodes);
  }
}
