package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainState.PluginSerialization;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Immutable
final class ChainPlugin<
    GlobalT extends StateSerializable,
    LocalT extends StateSerializable,
    PluginT extends BlockchainPlugin<GlobalT, LocalT>> {

  private final PluginT plugin;
  private final GlobalT globalState;
  private final LocalT localState;

  private final PluginIdentifier pluginIdentifier;
  private final Hash pluginIdentifierHash;
  private final Hash globalStateHash;

  static <
          GlobalT extends StateSerializable,
          LocalT extends StateSerializable,
          PluginT extends BlockchainPlugin<GlobalT, LocalT>>
      ChainPlugin<GlobalT, LocalT, PluginT> create(
          PluginLoader<GlobalT, LocalT, PluginT> loader,
          ChainPlugin<?, ?, ?> currentPlugin,
          Hash jarHash,
          byte[] rpc) {
    StateAccessor currentGlobal;
    StateAccessor currentLocal;
    Hash currentPluginIdentifier;
    if (currentPlugin != null) {
      currentGlobal = StateAccessor.create(currentPlugin.getGlobalState());
      currentLocal = StateAccessor.create(currentPlugin.getLocalState());
      currentPluginIdentifier = currentPlugin.pluginIdentifierHash;
    } else {
      currentGlobal = null;
      currentLocal = null;
      currentPluginIdentifier = null;
    }
    PluginIdentifier pluginIdentifier = new PluginIdentifier(currentPluginIdentifier, jarHash);
    PluginSerialization<GlobalT, LocalT, PluginT> pluginSerialization = loader.load(jarHash);
    PluginT pluginT = pluginSerialization.instantiatePlugin();
    GlobalT global = pluginT.migrateGlobal(currentGlobal, SafeDataInputStream.createFromBytes(rpc));
    LocalT local = pluginT.migrateLocal(currentLocal);
    Hash pluginIdentifierHash = loader.saveId(pluginIdentifier);
    return new ChainPlugin<>(pluginT, local, global, pluginIdentifier, pluginIdentifierHash, null);
  }

  private ChainPlugin(
      PluginT plugin,
      LocalT localState,
      GlobalT globalState,
      PluginIdentifier pluginIdentifier,
      Hash pluginIdentifierHash,
      Hash globalStateHash) {
    this.plugin = plugin;
    this.localState = localState;
    this.globalState = globalState;
    this.pluginIdentifier = pluginIdentifier;
    this.pluginIdentifierHash = pluginIdentifierHash;
    this.globalStateHash = globalStateHash;
  }

  PluginT getPlugin() {
    return plugin;
  }

  LocalT getLocalState() {
    return localState;
  }

  ChainPlugin<GlobalT, LocalT, PluginT> setLocalState(LocalT localState) {
    return new ChainPlugin<>(
        plugin, localState, globalState, pluginIdentifier, pluginIdentifierHash, globalStateHash);
  }

  GlobalT getGlobalState() {
    return globalState;
  }

  @SuppressWarnings("unchecked")
  ChainPlugin<GlobalT, LocalT, PluginT> setGlobalState(StateSerializable globalState) {
    return new ChainPlugin<>(
        plugin, localState, (GlobalT) globalState, pluginIdentifier, pluginIdentifierHash, null);
  }

  SerializedPlugin serialize(PluginLoader<GlobalT, LocalT, PluginT> loader) {
    Hash globalStateHash = this.globalStateHash;
    PluginSerialization<GlobalT, LocalT, PluginT> serialization =
        loader.load(pluginIdentifier.getJarHash());
    if (globalStateHash == null) {
      globalStateHash = serialization.saveGlobalValue(globalState);
    }
    return new SerializedPlugin(globalState, pluginIdentifierHash, globalStateHash);
  }

  public LocalPlugin serializeLocal(PluginLoader<GlobalT, LocalT, PluginT> loader) {
    PluginSerialization<GlobalT, LocalT, PluginT> serialization =
        loader.load(pluginIdentifier.getJarHash());
    return new LocalPlugin(serialization, pluginIdentifierHash, localState);
  }

  BlockchainPlugin.InvokeResult<ChainPlugin<GlobalT, LocalT, PluginT>> invokeLocal(
      PluginContext pluginContext, BlockchainAddress context, byte[] invocation) {
    BlockchainPlugin.InvokeResult<LocalT> nextLocal =
        getPlugin()
            .invokeLocal(pluginContext, getGlobalState(), getLocalState(), context, invocation);
    return new BlockchainPlugin.InvokeResult<>(
        setLocalState(nextLocal.updatedState()), nextLocal.result());
  }

  public BlockchainPlugin.InvokeResult<ChainPlugin<GlobalT, LocalT, PluginT>> invokeGlobal(
      PluginContext pluginContext, byte[] invocation) {
    BlockchainPlugin.InvokeResult<GlobalT> next =
        getPlugin().invokeGlobal(pluginContext, globalState, invocation);
    ChainPlugin<GlobalT, LocalT, PluginT> chainPlugin = setGlobalState(next.updatedState());
    return new BlockchainPlugin.InvokeResult<>(chainPlugin, next.result());
  }

  PluginIdentifier getIdentifier() {
    return pluginIdentifier;
  }

  Hash getPluginIdentifierHash() {
    return pluginIdentifierHash;
  }

  @Immutable
  public static final class SerializedPlugin implements StateSerializable {

    private static final Logger logger = LoggerFactory.getLogger(SerializedPlugin.class);

    // We cache the state to prevent getting from storage between each block
    @SuppressWarnings("Immutable")
    private transient BlockchainPlugin<StateSerializable, StateSerializable> plugin;

    @SuppressWarnings("Immutable")
    private transient StateSerializable globalState;

    private final Hash globalStateHash;
    private final Hash pluginIdentifier;

    @SuppressWarnings("unused")
    public SerializedPlugin() {
      globalStateHash = null;
      plugin = null;
      globalState = null;
      pluginIdentifier = null;
    }

    <GlobalT extends StateSerializable> SerializedPlugin(
        GlobalT globalState, Hash pluginIdentifier, Hash globalStateHash) {
      this.globalState = globalState;
      this.globalStateHash = globalStateHash;
      this.pluginIdentifier = pluginIdentifier;
    }

    <
            LocalT extends StateSerializable,
            GlobalT extends StateSerializable,
            PluginT extends BlockchainPlugin<GlobalT, LocalT>>
        ChainPlugin<GlobalT, LocalT, PluginT> asPlugin(
            PluginLoader<GlobalT, LocalT, PluginT> loader, LocalPlugin value) {
      final PluginIdentifier pluginIdentifier = loader.loadId(this.pluginIdentifier);
      final PluginT currentPlugin = plugin(loader);

      Hash parentToFind = value == null ? null : value.pluginIdentifier;
      LocalT loadedLocal;
      if (!Objects.equals(parentToFind, this.pluginIdentifier)) {
        logger.info(
            "Migrating local state from plugin {} to plugin {}",
            parentToFind,
            this.pluginIdentifier);

        PluginIdentifier migrateTo = pluginIdentifier;
        List<PluginIdentifier> migrations = new ArrayList<>();
        while (migrateTo.getParentId() != null && !migrateTo.getParentId().equals(parentToFind)) {
          migrateTo = loader.loadId(migrateTo.getParentId());
          migrations.add(migrateTo);
        }

        StateSerializable currentValue =
            migrateTo.getParentId() != null
                ? loader
                    .load(loader.loadId(parentToFind).getJarHash())
                    .loadLocalValue(value.identifier)
                : null;
        for (int i = migrations.size() - 1; i >= 0; i--) {
          PluginIdentifier migrationIdentifier = migrations.get(i);
          BlockchainPlugin<?, ?> pluginT =
              loader.load(migrationIdentifier.getJarHash()).instantiatePlugin();
          StateAccessor currentLocal = StateAccessor.create(currentValue);
          currentValue = pluginT.migrateLocal(currentValue == null ? null : currentLocal);
        }
        StateAccessor currentLocal = StateAccessor.create(currentValue);
        loadedLocal = currentPlugin.migrateLocal(currentValue == null ? null : currentLocal);
      } else {
        loadedLocal = localState(loader, value);
      }

      return new ChainPlugin<>(
          currentPlugin,
          loadedLocal,
          globalState(loader),
          pluginIdentifier,
          this.pluginIdentifier,
          globalStateHash);
    }

    @SuppressWarnings("unchecked")
    <
            LocalT extends StateSerializable,
            GlobalT extends StateSerializable,
            PluginT extends BlockchainPlugin<GlobalT, LocalT>>
        PluginT plugin(PluginLoader<GlobalT, LocalT, PluginT> loader) {
      if (this.plugin == null) {
        plugin =
            (BlockchainPlugin<StateSerializable, StateSerializable>)
                loader.load(getJarHash(loader)).instantiatePlugin();
      }
      return (PluginT) plugin;
    }

    @SuppressWarnings("unchecked")
    <
            LocalT extends StateSerializable,
            GlobalT extends StateSerializable,
            PluginT extends BlockchainPlugin<GlobalT, LocalT>>
        LocalT localState(PluginLoader<GlobalT, LocalT, PluginT> loader, LocalPlugin localState) {
      if (localState.value == null) {
        localState.value = loader.load(getJarHash(loader)).loadLocalValue(localState.identifier);
      }
      return (LocalT) localState.value;
    }

    private <
            LocalT extends StateSerializable,
            GlobalT extends StateSerializable,
            PluginT extends BlockchainPlugin<GlobalT, LocalT>>
        Hash getJarHash(PluginLoader<GlobalT, LocalT, PluginT> loader) {
      PluginIdentifier pluginIdentifier = loader.loadId(this.pluginIdentifier);
      Hash jarHash = pluginIdentifier.getJarHash();
      return jarHash;
    }

    @SuppressWarnings("unchecked")
    <
            LocalT extends StateSerializable,
            GlobalT extends StateSerializable,
            PluginT extends BlockchainPlugin<GlobalT, LocalT>>
        GlobalT globalState(PluginLoader<GlobalT, LocalT, PluginT> loader) {
      if (this.globalState == null) {
        this.globalState = loader.load(getJarHash(loader)).loadGlobalValue(globalStateHash);
      }
      return (GlobalT) globalState;
    }

    Hash getIdentifierHash() {
      return pluginIdentifier;
    }
  }

  @Immutable
  public static final class LocalPlugin implements StateSerializable {

    @SuppressWarnings("Immutable")
    private transient StateSerializable value;

    private final Hash identifier;
    private final Hash pluginIdentifier;

    @SuppressWarnings("unused")
    private LocalPlugin() {
      this.identifier = null;
      this.pluginIdentifier = null;
    }

    <LocalT extends StateSerializable> LocalPlugin(
        PluginSerialization<?, LocalT, ?> serialization, Hash pluginIdentifier, LocalT state) {
      this.pluginIdentifier = pluginIdentifier;
      this.value = state;
      this.identifier = serialization.saveLocalValue(state);
    }
  }

  @Immutable
  public static final class PluginIdentifier implements StateSerializable {

    private final Hash parentId;
    private final Hash jarHash;

    @SuppressWarnings("unused")
    private PluginIdentifier() {
      parentId = null;
      jarHash = null;
    }

    public PluginIdentifier(Hash parentId, Hash jarHash) {
      this.parentId = parentId;
      this.jarHash = jarHash;
    }

    public Hash getParentId() {
      return parentId;
    }

    public Hash getJarHash() {
      return jarHash;
    }
  }

  public interface PluginLoader<
      GlobalT extends StateSerializable,
      LocalT extends StateSerializable,
      PluginT extends BlockchainPlugin<GlobalT, LocalT>> {

    PluginSerialization<GlobalT, LocalT, PluginT> load(Hash jar);

    PluginIdentifier loadId(Hash identifier);

    Hash saveId(PluginIdentifier pluginIdentifier);
  }
}
