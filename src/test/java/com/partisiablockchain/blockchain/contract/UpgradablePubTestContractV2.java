package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateString;
import com.secata.stream.SafeDataInputStream;

/** Upgraded pub contract for test purposes. */
public final class UpgradablePubTestContractV2 extends PubContract<StateString>
    implements UpgradableContract<PubContractContext, StateString> {

  @Override
  public StateString onCreate(PubContractContext context, SafeDataInputStream rpc) {
    return new StateString(rpc.readString());
  }

  @Override
  public StateString onUpgradeState(
      PubContractContext context,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    byte[] actualRpc = removeContractIdentifierHeader(rpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    input.readBoolean();
    long toAdd = input.readLong();
    return new StateString(Long.toString(oldState.get("value").longValue() + toAdd));
  }
}
