package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.thread.ExecutionFactory;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** Test. */
public final class BlockchainTestHelper {

  public final BlockchainLedger blockchainLedger;
  public final BlockChainTestNetwork network;
  private final DummyExecutableEventFinder finder = new DummyExecutableEventFinder();

  /**
   * Construct a new helper.
   *
   * @param folder the folder to use as data directory
   * @param executionFactory execution factory for this test
   */
  public BlockchainTestHelper(Path folder, ExecutionFactory executionFactory) {
    this(MemoryStorage.createRootDirectory(folder.toFile()), executionFactory);
  }

  /**
   * Construct a new helper.
   *
   * @param root the folder to use as data directory
   * @param initialState callback to populate initial state
   * @param executionFactory execution factory for this test
   */
  public BlockchainTestHelper(
      Path root, Consumer<MutableChainState> initialState, ExecutionFactory executionFactory) {
    this(MemoryStorage.createRootDirectory(root.toFile()), initialState, executionFactory);
  }

  /**
   * Creates a helper with the supplied folder.
   *
   * @param rootDirectory the folder to use as data directory
   * @param executionFactory execution factory for this test
   */
  private BlockchainTestHelper(RootDirectory rootDirectory, ExecutionFactory executionFactory) {
    this(rootDirectory, StateHelper::initial, executionFactory);
  }

  /**
   * Creates a helper with the supplied folder and initial state.
   *
   * @param rootDirectory the folder to use as data directory
   * @param initialState the state to initialize the blockchain with
   * @param executionFactory execution factory for this test
   */
  private BlockchainTestHelper(
      RootDirectory rootDirectory,
      Consumer<MutableChainState> initialState,
      ExecutionFactory executionFactory) {
    this.network = new BlockChainTestNetwork();
    this.blockchainLedger =
        BlockchainLedger.createForTest(
            rootDirectory,
            mutableChainState -> {
              initialState.accept(mutableChainState);
              FeePluginHelper.setFeeAccount(mutableChainState, TestObjects.ACCOUNT_THREE);
              FeePluginHelper.setZkFeeAccount(mutableChainState, TestObjects.ACCOUNT_THREE);
              mutableChainState.createAccount(TestObjects.ACCOUNT_FOUR);
              mutableChainState.createAccount(TestObjects.ACCOUNT_THREE);
            },
            network,
            finder,
            executionFactory);
  }

  /** Adds event to executable event finder. */
  void addEventToFinder(ExecutableEvent event) {
    finder.setEvent(event);
  }

  /** Append a new block to the contained blockchain. */
  void appendBlock(KeyPair keyPair) {
    appendBlock(keyPair, blockchainLedger);
  }

  /** Append a new block to the contained blockchain. */
  private static void appendBlock(KeyPair keyPair, BlockchainLedger blockchainLedger) {

    FinalBlock block = createFinalBlock(keyPair, List.of(), blockchainLedger);
    blockchainLedger.appendBlock(block);
  }

  private static FinalBlock createFinalBlock(
      KeyPair keyPair,
      List<ExecutableTransaction> transactions,
      BlockchainLedger blockchainLedger) {
    Block producedBlock = createBlock(transactions, blockchainLedger);
    return new FinalBlock(
        producedBlock, SafeDataOutputStream.serialize(keyPair.sign(producedBlock.identifier())));
  }

  /**
   * Create a final block.
   *
   * @param keyPair producer
   * @param transactions transaction to include
   * @return the created block
   */
  public FinalBlock createFinalBlock(KeyPair keyPair, List<ExecutableTransaction> transactions) {
    return createFinalBlock(keyPair, transactions, blockchainLedger);
  }

  /** Create a block. */
  public Block createBlock(List<ExecutableTransaction> transactions) {
    return createBlock(transactions, blockchainLedger);
  }

  /** Create a block. */
  private static Block createBlock(
      List<ExecutableTransaction> transactions, BlockchainLedger blockchainLedger) {
    List<Hash> hashes =
        transactions.stream().map(ExecutableTransaction::identifier).collect(Collectors.toList());

    Hash previousStateHash = blockchainLedger.latest().getState().getHash();
    Block previousBlock = blockchainLedger.getLatestBlock();
    long pastBlockTime = previousBlock.getBlockTime();
    return new Block(
        previousBlock.getProductionTime() + 1,
        pastBlockTime + 1,
        pastBlockTime,
        previousBlock.identifier(),
        previousStateHash,
        List.of(),
        hashes);
  }
}
