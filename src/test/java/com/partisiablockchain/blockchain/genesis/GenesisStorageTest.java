package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class GenesisStorageTest {

  @TempDir Path tempDir;
  private static final String CHAIN_ID = "ChainId";
  private static final KeyPair signer = new KeyPair(BigInteger.ONE);

  @Test
  public void readWrite() {
    String value = "My string";
    DataStreamSerializable serialize = s -> s.writeString(value);
    Hash id = Hash.create(serialize);
    GenesisStorage genesisStorage = new GenesisStorage();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString)).isNull();
    Assertions.assertThat(genesisStorage.write(id, serialize)).isTrue();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString))
        .isEqualTo(value);
    Assertions.assertThat(genesisStorage.write(id, FunctionUtility.noOpConsumer())).isFalse();
    Assertions.assertThat(genesisStorage.read(id, SafeDataInputStream::readString))
        .isEqualTo(value);
  }

  @Test
  void onlyIncludeTransactionsPresentInBlock() {
    SignedTransaction transaction0 = createTransaction(0);
    SignedTransaction transaction1 = createTransaction(1);

    FinalBlock block =
        new FinalBlock(createBlock(List.of(transaction0.identifier()), List.of()), new byte[0]);
    GenesisStorage genesisStorage = new GenesisStorage();
    File file = createGenesisPathLocation();

    genesisStorage.writeGenesisFile(file, block, List.of(transaction0, transaction1), List.of());

    GenesisFile genesisFile = new GenesisFile(file);

    List<SignedTransaction> transactions =
        genesisFile.readTransactionsFromGenesisZip("ChainId", block.getBlock().getTransactions());

    Assertions.assertThat(transactions).containsOnly(transaction0);
  }

  @Test
  void onlyIncludeEventsPresentInBlock() {
    ExecutableEvent event0 = createEvent(0);
    ExecutableEvent event1 = createEvent(1);

    FinalBlock block =
        new FinalBlock(createBlock(List.of(), List.of(event0.identifier())), new byte[0]);
    GenesisStorage genesisStorage = new GenesisStorage();
    File file = createGenesisPathLocation();

    genesisStorage.writeGenesisFile(file, block, List.of(), List.of(event0, event1));

    GenesisFile genesisFile = new GenesisFile(file);
    List<ExecutableEvent> events =
        genesisFile.readEventsFromGenesisZip(block.getBlock().getEventTransactions());

    Assertions.assertThat(events.size()).isEqualTo(1);
    Assertions.assertThat(events.get(0)).usingRecursiveComparison().isEqualTo(event0);
  }

  @Test
  void expectStoredVersionNumber() {
    FinalBlock block = new FinalBlock(createBlock(List.of(), List.of()), new byte[0]);
    GenesisStorage genesisStorage = new GenesisStorage();
    File file = createGenesisPathLocation();

    genesisStorage.writeGenesisFile(file, block, List.of(), List.of());

    GenesisFile genesisFile = new GenesisFile(file);
    Assertions.assertThat(genesisFile.getVersion()).isEqualTo(GenesisFile.VERSION_1);
  }

  private static ExecutableEvent createEvent(long nonce) {
    return new ExecutableEvent(
        null,
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            signer.getPublic().createAddress(),
            0,
            createInteractWithContract(),
            new ShardRoute(null, nonce),
            2L,
            1L));
  }

  private static SignedTransaction createTransaction(long nonce) {
    return SignedTransaction.create(
            new CoreTransactionPart(nonce, 0, 0), createInteractWithContract())
        .sign(signer, CHAIN_ID);
  }

  private static InteractWithContractTransaction createInteractWithContract() {
    return InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
  }

  private File createGenesisPathLocation() {
    return tempDir.resolve("genesisFile.zip").toFile();
  }

  private static Block createBlock(List<Hash> txs, List<Hash> events) {
    return new Block(0, 0, 0, Block.GENESIS_PARENT, TestObjects.EMPTY_HASH, events, txs);
  }
}
