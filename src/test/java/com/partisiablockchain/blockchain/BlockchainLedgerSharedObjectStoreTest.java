package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainLedgerTest.produce;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainLedgerSharedObjectStoreTest extends CloseableTest {

  public static final byte[] JAR_PLUGIN_STORAGE =
      JarBuilder.buildJar(InMemoryGovernanceStoragePlugin.class, StorageState.class);

  private static final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private static final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private BlockchainLedger blockchainLedger;
  private String chainId;

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    DummyExecutableEventFinder finder = new DummyExecutableEventFinder();
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  bootstrapDeployment(mutable);
                  FeePluginHelper.mintGas(mutable, producerAccount, 100_000);
                  CoreContractStateTest.createContract(
                      mutable,
                      JarBuilder.buildJar(UpdateGovernanceStorageInTest.class),
                      BlockchainAddress.fromString("01A000000000000000000000000000000000000001"),
                      TestContracts.CONTRACT_BINDER_SYSTEM);
                  CoreContractStateTest.createContract(
                      mutable,
                      JarBuilder.buildJar(UpdateGlobalGovernanceStorageStateInTest.class),
                      BlockchainAddress.fromString("01A000000000000000000000000000000000000002"),
                      TestContracts.CONTRACT_BINDER_SYSTEM);
                  assertThat(mutable.getSharedObjectStorePlugin())
                      .isInstanceOf(EmptySharedObjectStorePlugin.class);
                  StateSerializable globalGovernanceStorage =
                      mutable.getGlobalPluginState(ChainPluginType.SHARED_OBJECT_STORE);
                  assertThat(globalGovernanceStorage).isNull();
                },
                network,
                finder));
    chainId = blockchainLedger.getChainId();
  }

  @Test
  void updateGovernanceStoragePlugin() {
    updateGovernanceStorage();
    byte[] rawBinder = TestContracts.CONTRACT_BINDER_PUBLIC;
    updateGlobalGovernanceStorageState(rawBinder);
    Hash identifier = new LargeByteArray(rawBinder).getIdentifier();
    testPubDeploy(identifier);
  }

  private void updateGovernanceStorage() {
    InteractWithContractTransaction transaction =
        UpdateGovernanceStorageInTest.create(JAR_PLUGIN_STORAGE);
    SignedTransaction signed = signedTransaction(transaction);
    blockchainLedger.addPendingTransaction(signed);
    produce(blockchainLedger, signed);
    produce(blockchainLedger);
    produce(blockchainLedger);
    assertThat(blockchainLedger.getChainState().getSharedObjectStorePlugin())
        .isInstanceOf(SharedObjectStorePluginWrapper.class);
  }

  private void updateGlobalGovernanceStorageState(byte[] binder) {
    Hash hash = new LargeByteArray(binder).getIdentifier();
    assertThat(blockchainLedger.getChainState().getSharedObjectStorePlugin().exists(hash))
        .isFalse();
    InteractWithContractTransaction transaction =
        UpdateGlobalGovernanceStorageStateInTest.create(binder);
    SignedTransaction signed = signedTransaction(transaction);
    blockchainLedger.addPendingTransaction(signed);
    produce(blockchainLedger, signed);
    produce(blockchainLedger);
    produce(blockchainLedger);

    StateSerializable storageState =
        blockchainLedger.getChainState().getGlobalPluginState(ChainPluginType.SHARED_OBJECT_STORE);
    assertThat(storageState).isNotNull();
    assertThat(blockchainLedger.getChainState().getSharedObjectStorePlugin().exists(hash)).isTrue();
  }

  private void testPubDeploy(Hash binderIdentifier) {
    SignedTransaction deploy = createDeployPubWithBinderHash(new byte[0], binderIdentifier);
    blockchainLedger.addPendingTransaction(deploy);
    produce(blockchainLedger, deploy);

    produce(blockchainLedger);
    produce(blockchainLedger);

    ImmutableChainState state = blockchainLedger.getChainState();
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;
    assertThat(state.getContractState(contractAddress)).hasFieldOrPropertyWithValue("state", 37);
  }

  private SignedTransaction signedTransaction(InteractWithContractTransaction transaction) {
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount),
                blockchainLedger.getLatestBlock().getBlockTime() + 10,
                50_000),
            transaction)
        .sign(producerKey, chainId);
  }

  private void bootstrapDeployment(MutableChainState chainState) {
    StateHelper.initial(chainState);
    byte[] jar = JarBuilder.buildJar(BlockchainLedgerTest.DeployContractInTest.class);

    BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
    CoreContractStateTest.createContract(
        chainState, jar, deployPublicContract, TestContracts.CONTRACT_BINDER_SYSTEM);
  }

  private SignedTransaction createDeployPubWithBinderHash(byte[] rpc, Hash binderIdentifier) {
    return createDeploy(binderIdentifier, rpc);
  }

  private SignedTransaction createDeploy(Hash binderHash, byte[] rpc) {
    byte[] jar = JarBuilder.buildJar(CreateContractTransactionTest.PubTestContract.class);
    InteractWithContractTransaction deployTransaction =
        BlockchainLedgerTest.DeployContractInTest.create(
            TestObjects.CONTRACT_PUB1, binderHash.getBytes(), jar, rpc);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 100_000),
            deployTransaction)
        .sign(producerKey, chainId);
  }

  private long getNonce(BlockchainAddress account) {
    return blockchainLedger.latest().getState().getAccount(account).getNonce();
  }

  /** Test. */
  public static final class UpdateGovernanceStorageInTest extends SysContract<StateVoid> {

    static InteractWithContractTransaction create(byte[] jar) {
      return InteractWithContractTransaction.create(
          BlockchainAddress.fromString("01A000000000000000000000000000000000000001"),
          SafeDataOutputStream.serialize(stream -> stream.writeDynamicBytes(jar)));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      context.getInvocationCreator().updateSharedObjectStorePlugin(jar, new byte[0]);
      return null;
    }
  }

  /** Test. */
  public static final class UpdateGlobalGovernanceStorageStateInTest
      extends SysContract<StateVoid> {

    static InteractWithContractTransaction create(byte[] binder) {
      return InteractWithContractTransaction.create(
          BlockchainAddress.fromString("01A000000000000000000000000000000000000002"),
          SafeDataOutputStream.serialize(stream -> stream.writeDynamicBytes(binder)));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      context
          .getInvocationCreator()
          .updateGlobalSharedObjectStorePluginState(GlobalPluginStateUpdate.create(jar));
      return null;
    }
  }

  /** Storage for test plugin {@link InMemoryGovernanceStoragePlugin}. */
  public record StorageState(AvlTree<Hash, LargeByteArray> map) implements StateSerializable {}

  /** Test. */
  public static final class InMemoryGovernanceStoragePlugin
      extends BlockchainSharedObjectStorePlugin<StorageState> {

    @Override
    public boolean exists(StorageState globalState, Hash hash) {
      if (globalState != null) {
        return globalState.map().containsKey(hash);
      } else {
        return false;
      }
    }

    @Override
    public Class<StorageState> getGlobalStateClass() {
      return StorageState.class;
    }

    @Override
    public InvokeResult<StorageState> invokeGlobal(
        PluginContext pluginContext, StorageState state, byte[] binderJar) {
      LargeByteArray binder = new LargeByteArray(binderJar);
      AvlTree<Hash, LargeByteArray> updated;
      if (state == null) {
        updated = AvlTree.create(Map.of(binder.getIdentifier(), binder));
      } else {
        updated = state.map().set(binder.getIdentifier(), binder);
      }
      return new InvokeResult<>(new StorageState(updated), new byte[0]);
    }

    @Override
    public StorageState migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }
  }
}
