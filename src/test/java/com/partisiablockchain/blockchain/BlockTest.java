package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockTest {

  @Test
  public void equality() {
    EqualsVerifier.forClass(Block.class)
        .withNonnullFields("identifier")
        .withOnlyTheseFields("identifier")
        .verify();
  }

  @Test
  public void hashShouldNotChange() {
    Block block = createBlock();
    assertThat(block.getProductionTime()).isEqualTo(123L);
    assertThat(block.identifier().toString())
        .isEqualTo("93d6bb7a7ff2e427753af6e84687e191d5d030a39ba800f35f9e45dfa14d8d82");
  }

  @Test
  public void producerIndex() {
    Block block = createBlock();
    assertThat(block.getProducerIndex()).isEqualTo((short) -1);
  }

  private Block createBlock() {
    List<Hash> transactionHashes = List.of(TestObjects.hashNumber(1L), TestObjects.hashNumber(2L));

    return new Block(
        123L,
        1L,
        0L,
        TestObjects.hashNumber(44556L),
        TestObjects.hashNumber(222L),
        List.of(),
        transactionHashes);
  }

  @Test
  public void testToString() {
    long blockTime = 117L;
    Hash parentHash = TestObjects.hashNumber(blockTime);
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockTime,
            blockTime,
            parentHash,
            TestObjects.EMPTY_HASH,
            List.of(),
            List.of(TestObjects.hashNumber(111L)));
    assertThat(block.toString())
        .contains("blockTime=" + blockTime)
        .contains("events=")
        .contains("transactions=")
        .contains("hash=")
        .contains("producerIndex=")
        .contains(block.identifier().toString());
  }
}
