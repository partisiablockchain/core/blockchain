package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.function.Consumer;

final class RefreshBucketsTask implements Runnable {

  private final KademliaKey id;
  private final List<KademliaBucket> buckets;
  private final Consumer<KademliaKey> lookup;

  RefreshBucketsTask(KademliaKey id, List<KademliaBucket> buckets, Consumer<KademliaKey> lookup) {
    this.id = id;
    this.buckets = buckets;
    this.lookup = lookup;
  }

  @Override
  public void run() {
    boolean firstNonEmptyFound = false;
    for (int i = buckets.size() - 1; i >= 0; i--) {
      KademliaBucket kademliaBucket = buckets.get(i);
      firstNonEmptyFound |= kademliaBucket.hasNodes();
      if (firstNonEmptyFound) {
        lookup.accept(id.createForDistance(i));
      }
    }
  }
}
