package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.blockchain.ImmutableChainState.SerializableChainState;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.Collection;

/** The state after executing a list of transaction, each with an execution status. */
@SuppressWarnings("ConstantConditions")
@Immutable
public final class ExecutedState implements StateSerializable {

  private final SerializableChainState chainState;
  private final String chainId;
  private final String subChainId;
  private final AvlTree<Hash, Hash> spawnedEvents;
  private final AvlTree<Hash, Boolean> executionStatus;

  @SuppressWarnings({"unused"})
  ExecutedState() {
    chainState = null;
    subChainId = null;
    spawnedEvents = null;
    executionStatus = null;
    chainId = null;
  }

  ExecutedState(
      SerializableChainState chainState,
      String chainId,
      String subChainId,
      AvlTree<Hash, Hash> spawnedEvents,
      AvlTree<Hash, Boolean> executionStatus) {
    this.chainState = chainState;
    this.chainId = chainId;
    this.subChainId = subChainId;
    this.spawnedEvents = spawnedEvents;
    this.executionStatus = executionStatus;
  }

  /**
   * Get all events that was spawned as part of this block execution. Maps from spawned event id to
   * the id of the spawning transaction.
   *
   * @return the spawned events
   */
  public AvlTree<Hash, Hash> getSpawnedEvents() {
    return spawnedEvents;
  }

  /**
   * Get the execution status of transactions and events that has been executed as part of this
   * block execution. Maps from transaction id to boolean execution status.
   *
   * @return the execution statuses
   */
  public AvlTree<Hash, Boolean> getExecutionStatus() {
    return executionStatus;
  }

  SerializableChainState getChainState() {
    return chainState;
  }

  /**
   * Get list of all hashes of event transactions created by the executed transactions.
   *
   * @return the list of event transaction hashes
   */
  public Collection<Hash> getEventTransactions() {
    return spawnedEvents.keySet();
  }

  /**
   * Get id of chain.
   *
   * @return id of chain
   */
  public String getChainId() {
    return chainId;
  }

  /**
   * Get id of sub chain.
   *
   * @return id of sub chain
   */
  public String getSubChainId() {
    return subChainId;
  }
}
