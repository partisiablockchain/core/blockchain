package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.sys.BinderDeploy;
import com.partisiablockchain.binder.sys.BinderUpgrade;
import com.partisiablockchain.binder.sys.SystemInteraction;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.Routing;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.UpgradeContractTransaction;
import com.partisiablockchain.crypto.Hash;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Class for handling the collection of events. */
public final class EventCollector {

  private static final Logger logger = LoggerFactory.getLogger(EventCollector.class);

  private final List<ExecutableEvent> transactions = new ArrayList<>();

  private final String subChainId;
  private final long committeeId;
  private final long governanceVersion;
  private final Hash originatingTransaction;
  private final Routing routing;
  private final int currentHeight;
  private final BlockchainAddress currentContract;
  private final FeeCollector feeCollector;
  private boolean hasCallback;

  /**
   * Create a new event manager.
   *
   * @param subChainId id of the current shard
   * @param routing the routing for selecting shards
   * @param committeeId the committee id that created the active block
   * @param governanceVersion the governance version for this execution
   * @param originatingTransaction the originating signed transaction
   * @param currentHeight current call height in the event stack
   */
  public EventCollector(
      String subChainId,
      Routing routing,
      long committeeId,
      long governanceVersion,
      Hash originatingTransaction,
      int currentHeight) {
    this(
        subChainId,
        committeeId,
        governanceVersion,
        originatingTransaction,
        routing,
        currentHeight,
        null,
        null);
  }

  /**
   * Create a new event manager.
   *
   * @param subChainId id of the current shard
   * @param state the active state handling routing for selecting shards and network fee conversion
   * @param committeeId the committee id that created the active block
   * @param governanceVersion the governance version for this execution
   * @param originatingTransaction the originating signed transaction
   * @param currentHeight current call height in the event stack
   * @param currentContract the current contract for receiving callbacks
   * @param feeCollector fee collector to be used for creation of events
   */
  public EventCollector(
      String subChainId,
      MutableChainState state,
      long committeeId,
      long governanceVersion,
      Hash originatingTransaction,
      int currentHeight,
      BlockchainAddress currentContract,
      FeeCollector feeCollector) {
    this(
        subChainId,
        committeeId,
        governanceVersion,
        originatingTransaction,
        state,
        currentHeight,
        currentContract,
        feeCollector);
  }

  EventCollector(
      String subChainId,
      long committeeId,
      long governanceVersion,
      Hash originatingTransaction,
      Routing routing,
      int currentHeight,
      BlockchainAddress currentContract,
      FeeCollector feeCollector) {
    this.subChainId = subChainId;
    this.committeeId = committeeId;
    this.governanceVersion = governanceVersion;
    this.originatingTransaction = originatingTransaction;
    this.routing = routing;
    this.currentHeight = currentHeight;
    this.currentContract = currentContract;
    this.feeCollector = feeCollector;
  }

  /**
   * Creates an event transaction with a unique hash.
   *
   * @param contract the target contract
   * @param callback the callback to handle
   */
  public void createCallback(BlockchainAddress contract, CallbackCreator.Callback callback) {
    BlockchainAddress target =
        Objects.requireNonNull(contract, "Unable to create events without target");

    ShardRoute shardRoute = routing.routeToShard(target);

    CallbackToContract callbackToContract = new CallbackToContract(contract, callback);
    EventTransaction event =
        EventTransaction.create(
            originatingTransaction,
            callbackToContract,
            shardRoute,
            committeeId,
            governanceVersion,
            nextHeightCapped(false));
    ExecutableEvent executableEvent = new ExecutableEvent(subChainId, event);
    transactions.add(executableEvent);
  }

  private int nextHeightCapped(boolean enforceHeight) {
    int nextHeight = currentHeight + 1;
    if (enforceHeight && nextHeight > 128) {
      throw new IllegalStateException("Cannot add another level");
    }
    return nextHeight;
  }

  /**
   * Creates an event transaction with a unique hash.
   *
   * @param sender the sender
   * @param rawTransaction the raw transaction
   * @param cost the cost of the transaction
   * @param costFromContract the allocated cost will be paid by the current contract
   */
  public void createEventTransaction(
      BlockchainAddress sender, Transaction rawTransaction, long cost, boolean costFromContract) {
    if (costFromContract) {
      feeCollector.registerPaymentFromContract(cost);
    }

    boolean governanceContract =
        rawTransaction.getTargetContract().getType() == BlockchainAddress.Type.CONTRACT_GOV;
    long networkByteCount = rawTransaction.computeNetworkByteCount();
    long networkFee = feeCollector.convertNetworkFee(networkByteCount);

    long remainingCost = cost;
    if (!governanceContract) {
      remainingCost = cost - networkFee;
      if (remainingCost < 0) {
        throw new RuntimeException(
            "Ran out of gas. Allocated cost %d is less than the network fee %d. %s"
                .formatted(cost, networkFee, feeCollector));
      }
      logger.debug(
          "Deducting network fee {} for transaction to {} - remaining: {}",
          networkFee,
          rawTransaction.getTargetContract(),
          remainingCost);
    }

    BlockchainAddress target =
        Objects.requireNonNull(
            rawTransaction.getTargetContract(), "Unable to create events without target");

    ShardRoute shardRoute = routing.routeToShard(target);
    ReturnEnvelope returnEnvelope = createReturnEnvelope();
    EventTransaction event =
        EventTransaction.create(
            originatingTransaction,
            sender,
            remainingCost,
            rawTransaction,
            shardRoute,
            committeeId,
            governanceVersion,
            nextHeightCapped(true),
            returnEnvelope);
    ExecutableEvent executableEvent = new ExecutableEvent(subChainId, event);
    if (!governanceContract) {
      feeCollector.registerNetworkFee(executableEvent.identifier(), networkFee);
    }

    feeCollector.registerAllocatedCost(remainingCost);
    transactions.add(executableEvent);
  }

  /**
   * Create a system event without target.
   *
   * @param event the actual executable event
   */
  public void createSystemEvent(InnerSystemEvent event) {
    createSystemEvent(null, event);
  }

  /**
   * Create a system event.
   *
   * @param shardId shard to route to if event is without target
   * @param event the actual executable event
   */
  public void createSystemEvent(String shardId, InnerSystemEvent event) {
    boolean isCallBackType =
        event.getSystemEventType() == InnerSystemEvent.SystemEventType.CALLBACK;
    ShardRoute shardRoute;
    BlockchainAddress target = event.target();
    if (target == null) {
      shardRoute = routing.routeToShard(shardId);
    } else {
      shardRoute = routing.routeToShard(event.target());
    }
    ReturnEnvelope returnEnvelope = createReturnEnvelope();
    EventTransaction eventTransaction =
        new EventTransaction(
            originatingTransaction,
            shardRoute,
            committeeId,
            governanceVersion,
            nextHeightCapped(!isCallBackType),
            returnEnvelope,
            event);
    ExecutableEvent executableEvent = new ExecutableEvent(subChainId, eventTransaction);
    transactions.add(executableEvent);
  }

  void createGovernanceEvent(ShardRoute shardRoute, EventTransaction.InnerEvent systemEvent) {
    EventTransaction eventTransaction =
        new EventTransaction(
            originatingTransaction,
            shardRoute,
            committeeId,
            governanceVersion,
            currentHeight,
            null,
            systemEvent);
    ExecutableEvent executableEvent = new ExecutableEvent(subChainId, eventTransaction);
    transactions.add(executableEvent);
  }

  private ReturnEnvelope createReturnEnvelope() {
    if (shouldCreateCallback()) {
      return new ReturnEnvelope(currentContract);
    } else {
      return null;
    }
  }

  /**
   * Get executable events.
   *
   * @return list of executable events
   */
  public List<ExecutableEvent> getTransactions() {
    return Collections.unmodifiableList(transactions);
  }

  void checkCallbackFailing(Hash eventIdentifier, ReturnEnvelope returnEnvelope) {
    if (returnEnvelope != null) {
      InnerSystemEvent.CallbackEvent callbackEvent =
          new InnerSystemEvent.CallbackEvent(returnEnvelope, eventIdentifier, false, new byte[0]);
      createSystemEvent(callbackEvent);
    }
  }

  /**
   * Check if event should create callback.
   *
   * @return true if event should create callback, false otherwise
   */
  boolean shouldCreateCallback() {
    return hasCallback;
  }

  /**
   * Sets the callback rpc data.
   *
   * @param callbackCost the allocated cost for the callback
   * @param callbackCostFromContract the allocated callback cost will be paid by the current
   */
  public void setCallback(long callbackCost, boolean callbackCostFromContract) {
    this.hasCallback = true;

    if (callbackCostFromContract) {
      feeCollector.registerPaymentFromContract(callbackCost);
    }
    feeCollector.registerAllocatedCost(callbackCost);
  }

  /**
   * Get address of current contract for receiving callbacks.
   *
   * @return contract address
   */
  public BlockchainAddress getCurrentContract() {
    return currentContract;
  }

  void interaction(
      BlockchainAddress contract,
      byte[] contractRpc,
      boolean originalSender,
      long cost,
      boolean costFromContract,
      EventSender eventHandler) {
    InteractWithContractTransaction rawTransaction =
        InteractWithContractTransaction.create(contract, contractRpc);
    createEventTransaction(
        eventHandler.selectSender(originalSender), rawTransaction, cost, costFromContract);
  }

  void deploy(
      BlockchainAddress contract,
      byte[] binderJar,
      byte[] contractJar,
      byte[] abi,
      byte[] rpc,
      boolean originalSender,
      long cost,
      EventSender eventHandler) {
    CreateContractTransaction createContract =
        CreateContractTransaction.create(contract, binderJar, contractJar, abi, rpc);
    createEventTransaction(eventHandler.selectSender(originalSender), createContract, cost, false);
  }

  void upgrade(
      BlockchainAddress contractAddress,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      byte[] rpc,
      boolean originalSender,
      long cost,
      EventSender eventHandler) {
    UpgradeContractTransaction upgradeContract =
        UpgradeContractTransaction.create(
            contractAddress, newBinderJar, newContractJar, newAbi, rpc);
    createEventTransaction(eventHandler.selectSender(originalSender), upgradeContract, cost, false);
  }

  void handle(SystemInteraction event) {
    if (event instanceof SystemInteraction.CreateAccount createAccount) {
      createSystemEvent(new InnerSystemEvent.CreateAccountEvent(createAccount.address));
    } else if (event instanceof SystemInteraction.CreateShard createShard) {
      createSystemEvent(new InnerSystemEvent.CreateShardEvent(createShard.shardId));
    } else if (event instanceof SystemInteraction.RemoveShard removeShard) {
      createSystemEvent(new InnerSystemEvent.RemoveShardEvent(removeShard.shardId));
    } else if (event instanceof SystemInteraction.UpdateLocalAccountPluginState update) {
      createSystemEvent(
          new InnerSystemEvent.UpdateLocalPluginStateEvent(ChainPluginType.ACCOUNT, update.update));
    } else if (event instanceof SystemInteraction.UpdateContextFreeAccountPluginState update) {
      createSystemEvent(
          update.shardId,
          new InnerSystemEvent.UpdateContextFreePluginState(ChainPluginType.ACCOUNT, update.rpc));
    } else if (event instanceof SystemInteraction.UpdateGlobalAccountPluginState update) {
      createSystemEvent(
          new InnerSystemEvent.UpdateGlobalPluginStateEvent(
              ChainPluginType.ACCOUNT, update.update));
    } else if (event instanceof SystemInteraction.UpdateAccountPlugin updateAccountPlugin) {
      createSystemEvent(
          new InnerSystemEvent.UpdatePluginEvent(
              ChainPluginType.ACCOUNT, updateAccountPlugin.pluginJar, updateAccountPlugin.rpc));
    } else if (event instanceof SystemInteraction.UpdateLocalConsensusPluginState update) {
      createSystemEvent(
          new InnerSystemEvent.UpdateLocalPluginStateEvent(
              ChainPluginType.CONSENSUS, update.update));
    } else if (event instanceof SystemInteraction.UpdateGlobalConsensusPluginState update) {
      createSystemEvent(
          new InnerSystemEvent.UpdateGlobalPluginStateEvent(
              ChainPluginType.CONSENSUS, update.update));
    } else if (event instanceof SystemInteraction.UpdateConsensusPlugin update) {
      createSystemEvent(
          new InnerSystemEvent.UpdatePluginEvent(
              ChainPluginType.CONSENSUS, update.pluginJar, update.rpc));
    } else if (event instanceof SystemInteraction.UpdateRoutingPlugin update) {
      createSystemEvent(
          new InnerSystemEvent.UpdatePluginEvent(
              ChainPluginType.ROUTING, update.pluginJar, update.rpc));
    } else if (event instanceof SystemInteraction.UpdateGlobalSharedObjectStorePluginState update) {
      createSystemEvent(
          new InnerSystemEvent.UpdateGlobalPluginStateEvent(
              ChainPluginType.SHARED_OBJECT_STORE, update.update));
    } else if (event instanceof SystemInteraction.UpdateSharedObjectStorePlugin update) {
      createSystemEvent(
          new InnerSystemEvent.UpdatePluginEvent(
              ChainPluginType.SHARED_OBJECT_STORE, update.pluginJar, update.rpc));
    } else if (event instanceof SystemInteraction.RemoveContract update) {
      createSystemEvent(new InnerSystemEvent.RemoveContract(update.contract));
    } else if (event instanceof SystemInteraction.Exists update) {
      createSystemEvent(new InnerSystemEvent.CheckExistenceEvent(update.address));
    } else if (event instanceof SystemInteraction.SetFeature update) {
      createSystemEvent(new InnerSystemEvent.SetFeatureEvent(update.key, update.value));
    } else /* if (event instanceof SystemInteraction.UpgradeSystemContract update) */ {
      SystemInteraction.UpgradeSystemContract update =
          (SystemInteraction.UpgradeSystemContract) event;
      createSystemEvent(
          new InnerSystemEvent.UpgradeSystemContractEvent(
              update.contractJar,
              update.binderJar,
              update.abi,
              update.rpc,
              update.contractAddress));
    }
  }

  void process(BinderEvent executableEvent, EventSender eventHandler) {
    if (executableEvent instanceof SystemInteraction system) {
      handle(system);
    } else if (executableEvent instanceof BinderDeploy deploy) {
      deploy(
          deploy.contract,
          deploy.binderJar,
          deploy.contractJar,
          deploy.abi,
          deploy.rpc,
          deploy.originalSender,
          deploy.effectiveCost,
          eventHandler);
    } else if (executableEvent instanceof BinderUpgrade upgrade) {
      upgrade(
          upgrade.contract,
          upgrade.newBinderJar,
          upgrade.newContractJar,
          upgrade.newAbi,
          upgrade.rpc,
          upgrade.originalSender,
          upgrade.allocatedCost,
          eventHandler);
    } else /*if (event instanceof BinderInteraction interaction)*/ {
      BinderInteraction interaction = (BinderInteraction) executableEvent;
      interaction(
          interaction.contract,
          interaction.rpc,
          interaction.originalSender,
          interaction.effectiveCost,
          interaction.costFromContract,
          eventHandler);
    }
  }
}
