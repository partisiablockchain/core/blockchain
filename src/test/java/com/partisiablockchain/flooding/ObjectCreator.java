package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.mockito.Mockito.when;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.thread.ExecutionFactoryFake;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import org.mockito.Mockito;

/** Testing utility. */
public final class ObjectCreator {

  private static final AtomicInteger port = new AtomicInteger(6001);

  /**
   * Allocate the next port.
   *
   * @return the allocated port
   */
  public static int port() {
    return port.getAndIncrement();
  }

  private static final InetAddress local;

  static {
    try {
      local = InetAddress.getByAddress(new byte[] {127, 0, 0, 1});
    } catch (UnknownHostException e) {
      throw new RuntimeException();
    }
  }

  static Client noOpClient() {
    return new Client(
        fixedSupplier(null),
        null,
        null,
        fixedSupplier(List.of()),
        noOpBiConsumer(),
        new ExecutionFactoryFake());
  }

  static Server noOpServer(CloseableTest registration) throws IOException {
    return new Server(serverSocket(registration), noOpBiConsumer(), new ExecutionFactoryFake());
  }

  static ServerSocket serverSocket(CloseableTest registration, Socket... accepts)
      throws IOException {
    Object semaphore = new Object();
    AtomicInteger accepted = new AtomicInteger(0);
    AtomicBoolean done = new AtomicBoolean(false);
    ServerSocket mock = Mockito.mock(ServerSocket.class);
    when(mock.accept())
        .thenAnswer(
            invocation -> {
              int index = accepted.get();
              if (index < accepts.length) {
                accepted.getAndIncrement();
                return accepts[index];
              }
              synchronized (semaphore) {
                while (!done.get()) {
                  semaphore.wait();
                }
              }
              return null;
            });
    registration.register(
        () -> {
          synchronized (semaphore) {
            done.set(true);
            semaphore.notifyAll();
          }
        });
    return mock;
  }

  static Socket mockedSocket(InputStream in, OutputStream out) throws IOException {
    Socket socket = Mockito.mock(Socket.class);
    when(socket.getInputStream()).thenReturn(in);
    when(socket.getOutputStream()).thenReturn(out);
    when(socket.getInetAddress()).thenReturn(local);
    when(socket.getPort()).thenReturn(1234);
    return socket;
  }

  static BlockingInput blockingInput(CloseableTest registration) {
    BlockingInput input = new BlockingInput();
    registration.register(input);
    return input;
  }

  static BlockingOutput blockingOutput(CloseableTest registration) {
    BlockingOutput output = new BlockingOutput();
    registration.register(output);
    return output;
  }

  static <T, S> BiConsumer<T, S> noOpBiConsumer() {
    return (a, b) -> {};
  }

  private static <T> Supplier<T> fixedSupplier(T result) {
    return () -> result;
  }

  /** Create a network config without remote nodes and a fresh local port. */
  public static NetworkConfig networkConfig() {
    return new NetworkConfig(new Address("localhost", 0), FunctionUtility.nullSupplier(), null);
  }

  /**
   * Create a new State serializer that only persists the data in a memory map.
   *
   * @return a state serializer
   */
  public static StateSerializer createMemoryStoredSerializer() {
    return new StateSerializer(createMemoryStateStorage(), true);
  }

  /** Create memory state storage. */
  public static StateStorageRaw createMemoryStateStorage() {
    return new StateStorageRaw() {

      private final Map<Hash, byte[]> serialized = new HashMap<>();

      @Override
      public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
        if (serialized.containsKey(hash)) {
          return false;
        }
        serialized.put(hash, SafeDataOutputStream.serialize(writer));
        return true;
      }

      @Override
      public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
        if (serialized.containsKey(hash)) {
          SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(serialized.get(hash));
          return reader.apply(bytes);
        } else {
          return null;
        }
      }

      @Override
      public synchronized byte[] read(Hash hash) {
        return serialized.get(hash);
      }
    };
  }

  @SuppressWarnings("InputStreamSlowMultibyteRead")
  static final class BlockingInput extends InputStream {

    private final byte[] data;
    private int index = 0;
    private boolean closed = false;

    BlockingInput(byte[] dataBeforeBlock) {
      this.data = dataBeforeBlock;
    }

    BlockingInput() {
      this(new byte[0]);
    }

    @Override
    public synchronized int read() throws IOException {
      if (index < data.length) {
        int result = Byte.toUnsignedInt(data[index]);
        index++;
        return result;
      }
      while (!closed) {
        ExceptionConverter.run(this::wait, "Interrupt");
      }
      throw new EOFException("expected");
    }

    @Override
    public synchronized void close() {
      closed = true;
      this.notifyAll();
    }
  }

  private static final class BlockingOutput extends OutputStream {

    private final byte[] dataBeforeBlock;
    private int index = 0;
    private boolean closed = false;

    BlockingOutput(byte[] dataBeforeBlock) {
      this.dataBeforeBlock = dataBeforeBlock;
    }

    BlockingOutput() {
      this(new byte[0]);
    }

    @Override
    public synchronized void write(int b) throws IOException {
      if (index < dataBeforeBlock.length) {
        dataBeforeBlock[index] = (byte) b;
        index++;
      } else {
        while (!closed) {
          ExceptionConverter.run(this::wait, "Interrupt");
        }
        throw new EOFException("expected");
      }
    }

    @Override
    public synchronized void close() {
      closed = true;
      this.notifyAll();
    }
  }
}
