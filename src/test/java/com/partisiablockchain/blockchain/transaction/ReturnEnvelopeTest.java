package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ReturnEnvelopeTest {

  @Test
  public void readAndWriteSerializer() {
    ReturnEnvelope returnEnvelope = new ReturnEnvelope(TestObjects.CONTRACT_PUB1);
    StateSerializer stateSerializer =
        new StateSerializer(ObjectCreator.createMemoryStateStorage(), true);
    SerializationResult write = stateSerializer.write(returnEnvelope);
    ReturnEnvelope read = stateSerializer.read(write.hash(), ReturnEnvelope.class);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(returnEnvelope);
  }

  @Test
  public void readAndWriteDatastream() {
    ReturnEnvelope returnEnvelope = new ReturnEnvelope(TestObjects.CONTRACT_PUB1);
    byte[] write = SafeDataOutputStream.serialize(returnEnvelope::write);
    ReturnEnvelope read = SafeDataInputStream.deserialize(ReturnEnvelope::read, write);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(returnEnvelope);
  }
}
