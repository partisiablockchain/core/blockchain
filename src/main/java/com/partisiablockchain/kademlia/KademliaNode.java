package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.net.InetAddress;
import java.util.Objects;

/** A (external) node in Kademlia. */
public final class KademliaNode {

  private final KademliaKey key;
  private final InetAddress address;
  private final int port;

  /**
   * Construct a new KademliaNode.
   *
   * @param key the key of the new now
   * @param address address of the new now
   * @param port port of the new now
   */
  public KademliaNode(KademliaKey key, InetAddress address, int port) {
    this.key = key;
    this.address = address;
    this.port = port;
  }

  KademliaKey getKey() {
    return key;
  }

  InetAddress getAddress() {
    return address;
  }

  int getPort() {
    return port;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KademliaNode that = (KademliaNode) o;
    return port == that.port
        && Objects.equals(key, that.key)
        && Objects.equals(address, that.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, address, port);
  }
}
