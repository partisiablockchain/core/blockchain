package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.fee.AccumulatedFees;
import com.partisiablockchain.blockchain.fee.ContractStorage;
import com.partisiablockchain.blockchain.fee.FeeState;
import com.partisiablockchain.blockchain.fee.GasAndCoinFeePluginGlobal;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** Null implementation of BlockchainAccountPlugin for tests. */
public abstract class NullBlockchainAccountPlugin
    extends BlockchainAccountPlugin<
        GasAndCoinFeePluginGlobal, AccumulatedFees, FeeState, ContractStorage> {
  @Override
  public long convertNetworkFee(GasAndCoinFeePluginGlobal globalState, long bytes) {
    return 0;
  }

  @Override
  public boolean canCoverCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    return false;
  }

  @Override
  public PayCostResult<AccumulatedFees, FeeState> payCost(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccountState<AccumulatedFees, FeeState> localState,
      SignedTransaction transaction) {
    return null;
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> contractCreated(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      BlockchainAddress address) {
    return null;
  }

  @Override
  public AccumulatedFees removeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState) {
    return null;
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateContractGasBalance(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> localState,
      BlockchainAddress contract,
      long gas) {
    return null;
  }

  @Override
  public AccumulatedFees registerBlockchainUsage(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees contextFreeState,
      long gas) {
    return null;
  }

  @Override
  public PayServiceFeesResult<ContractState<AccumulatedFees, ContractStorage>> payServiceFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> contractState,
      List<PendingFee> pendingFees) {
    return null;
  }

  @Override
  public AccumulatedFees payInfrastructureFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      List<PendingFee> pendingFees) {
    return null;
  }

  @Override
  public AccumulatedFees updateForBlock(
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      BlockContext<ContractStorage> context) {
    return null;
  }

  @Override
  public ContractState<AccumulatedFees, ContractStorage> updateActiveContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      ContractState<AccumulatedFees, ContractStorage> currentState,
      BlockchainAddress contract,
      long size) {
    return null;
  }

  @Override
  protected AccumulatedFees migrateContextFree(StateAccessor current) {
    return null;
  }

  @Override
  protected ContractStorage migrateContract(BlockchainAddress address, StateAccessor current) {
    return null;
  }

  @Override
  protected FeeState migrateAccount(StateAccessor current) {
    return null;
  }

  @Override
  protected InvokeResult<ContractState<AccumulatedFees, ContractStorage>> invokeContract(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      ContractState<AccumulatedFees, ContractStorage> current,
      BlockchainAddress address,
      byte[] rpc) {
    return null;
  }

  @Override
  protected InvokeResult<AccountState<AccumulatedFees, FeeState>> invokeAccount(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccountState<AccumulatedFees, FeeState> current,
      byte[] rpc) {
    return null;
  }

  @Override
  protected InvokeResult<AccumulatedFees> invokeContextFree(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal global,
      AccumulatedFees contextFree,
      byte[] rpc) {
    return null;
  }

  @Override
  protected AccumulatedFees payByocFees(
      PluginContext pluginContext,
      GasAndCoinFeePluginGlobal globalState,
      AccumulatedFees localState,
      Unsigned256 amount,
      String symbol,
      FixedList<BlockchainAddress> nodes) {
    return null;
  }

  @Override
  public Class<GasAndCoinFeePluginGlobal> getGlobalStateClass() {
    return null;
  }

  @Override
  public List<Class<?>> getLocalStateClassTypeParameters() {
    return null;
  }

  @Override
  public InvokeResult<GasAndCoinFeePluginGlobal> invokeGlobal(
      PluginContext pluginContext, GasAndCoinFeePluginGlobal state, byte[] rpc) {
    return null;
  }

  @Override
  public GasAndCoinFeePluginGlobal migrateGlobal(
      StateAccessor currentGlobal, SafeDataInputStream rpc) {
    return null;
  }
}
