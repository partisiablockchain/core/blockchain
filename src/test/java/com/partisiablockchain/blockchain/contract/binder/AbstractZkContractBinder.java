package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventCreatorImpl;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.EventManagerImpl;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.contract.zk.ZkContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/** Super class for ZK contract binder, shared for upgradable and non-upgradable ZK contracts. */
public abstract class AbstractZkContractBinder<OpenT extends StateSerializable>
    implements ZkBinderContract<OpenT> {

  protected final ZkContract<OpenT, ?, ?, ?> contract;

  /**
   * Default constructor.
   *
   * @param contract ZK contract open state
   */
  public AbstractZkContractBinder(ZkContract<OpenT, ?, ?, ?> contract) {
    this.contract = contract;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<OpenT> getStateClass() {
    ParameterizedType genericSuperClass =
        (ParameterizedType) contract.getClass().getGenericSuperclass();
    Type type = genericSuperClass.getActualTypeArguments()[0];
    return (Class<OpenT>) ((ParameterizedType) type).getRawType();
  }

  @Override
  public List<Class<?>> getStateClassTypeParameters() {
    return List.of(Integer.class, String.class, Long.class);
  }

  @Override
  public ZkContract<?, ?, ?, ?> getContract() {
    return contract;
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> create(ZkBinderContext context, byte[] rpc) {
    return result(contract.onCreate(null, null, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> invoke(
      ZkBinderContext context, OpenT state, byte[] rpc) {
    return result(
        contract.onOpenInput(null, null, state, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> callback(
      ZkBinderContext context, OpenT state, CallbackContext callbackContext, byte[] rpc) {
    return result(
        contract.onCallback(
            null, null, state, callbackContext, SafeDataInputStream.createFromBytes(rpc)));
  }

  private BinderResult<OpenT, BinderInteraction> result(OpenT state) {
    return new CommonBinderResult<>(state);
  }

  static final class CommonBinderResult<StateT> implements BinderResult<StateT, BinderInteraction> {

    private final StateT state;

    CommonBinderResult(StateT state) {
      this.state = state;
    }

    @Override
    public StateT getState() {
      return state;
    }

    @Override
    public List<BinderInteraction> getInvocations() {
      return List.of();
    }

    @Override
    public CallResult getCallResult() {
      return null;
    }
  }

  /** Implements a ZK contract context for test purposes. */
  protected static final class ZkContext implements ZkContractContext {

    private final ZkBinderContext context;
    private final List<ContractEvent> invocations;
    private ContractEventGroup remoteCalls;

    public ZkContext(ZkBinderContext context) {
      this.context = context;
      this.invocations = new ArrayList<>();
    }

    @Override
    public BlockchainAddress getContractAddress() {
      return context.getContractAddress();
    }

    @Override
    public EventCreator getInvocationCreator() {
      return new EventCreatorImpl(invocations::add);
    }

    @Override
    public EventManager getRemoteCallsCreator() {
      if (remoteCalls == null) {
        remoteCalls = new ContractEventGroup();
      }
      return new EventManagerImpl(remoteCalls);
    }

    @Override
    public void setResult(DataStreamSerializable result) {}

    @Override
    public long getBlockTime() {
      return context.getBlockTime();
    }

    @Override
    public long getBlockProductionTime() {
      return context.getBlockProductionTime();
    }

    @Override
    public BlockchainAddress getFrom() {
      return context.getFrom();
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return context.getCurrentTransactionHash();
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return context.getOriginalTransactionHash();
    }
  }
}
