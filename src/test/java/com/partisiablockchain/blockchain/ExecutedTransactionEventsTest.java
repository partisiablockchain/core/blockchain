package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.crypto.Hash;
import java.util.LinkedHashMap;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ExecutedTransactionEventsTest {

  @Test
  public void ownEvents() {
    ExecutableEvent e1 = new ExecutableEvent("This", create("This", 123));
    ExecutableEvent e2 = new ExecutableEvent("This", create("This", 124));
    ExecutedTransactionEvents success =
        ExecutedTransactionEvents.success(
            new ExecutableEvent("This", null),
            List.of(e1, e2, new ExecutableEvent("This", create("Other", 123))),
            new TransactionCost(0, 0, 0, 0, new LinkedHashMap<>()));
    Assertions.assertThat(success.events()).hasSize(3);
    Assertions.assertThat(success.ownEvents()).hasSize(2).containsSequence(e1, e2);
  }

  @Test
  public void testToString() {
    Assertions.assertThat(create("Something", 123).toString())
        .contains("Something")
        .contains(TestObjects.CONTRACT_BOOTSTRAP.toString());
  }

  /**
   * Executed transaction events can fail with errors that have no message.
   *
   * <p>Such errors can occur if HotSpot has optimized the message away.
   */
  @Test
  void failureWithoutMessage() {
    EventTransaction eventTransaction = createTestEventTransactions();
    ExecutedTransactionEvents failed =
        ExecutedTransactionEvents.fail(
            new ExecutableEvent("this", eventTransaction), new NullPointerException());
    Assertions.assertThat(failed.exception().getErrorMessage()).isEmpty();
    Assertions.assertThat(failed.exception().getStackTrace()).isNotEmpty();
  }

  @Test
  void failureWithMessage() {
    EventTransaction eventTransaction = createTestEventTransactions();
    ExecutedTransactionEvents failed =
        ExecutedTransactionEvents.fail(
            new ExecutableEvent("this", eventTransaction), new NullPointerException("My Error!"));
    Assertions.assertThat(failed.exception().getErrorMessage()).isEqualTo("My Error!");
    Assertions.assertThat(failed.exception().getStackTrace()).isNotEmpty();
  }

  private static EventTransaction createTestEventTransactions() {
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_PUB2, new byte[0], new byte[0], new byte[0], new byte[0]);
    Hash originalTx = Hash.create(s -> s.writeString("originalTx"));
    return EventTransaction.createStandalone(
        originalTx, TestObjects.ACCOUNT_ONE, 42L, transaction, new ShardRoute("this", 0), 0, 0);
  }

  private EventTransaction create(String shard, int nonce) {
    ReturnEnvelope returnEnvelope = new ReturnEnvelope(TestObjects.CONTRACT_BOOTSTRAP);
    return new EventTransaction(
        TestObjects.EMPTY_HASH,
        new ShardRoute(shard, nonce),
        123,
        1,
        123,
        returnEnvelope,
        new InnerSystemEvent.CallbackEvent(
            returnEnvelope, TestObjects.EMPTY_HASH, false, new byte[0]));
  }
}
