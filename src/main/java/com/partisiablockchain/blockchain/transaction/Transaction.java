package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.UpgradeContractTransaction;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** A transaction in pbc. */
public interface Transaction extends ContractExecution, DataStreamSerializable {

  /**
   * Compute network usage as byte count.
   *
   * @return network usage as byte count
   */
  default long computeNetworkByteCount() {
    return SafeDataOutputStream.serialize(this::write).length;
  }

  /**
   * Gets the address this transaction is interacting with. Most interactions is with a contract,
   * the address is looked up here.
   *
   * @return the address
   */
  BlockchainAddress getTargetContract();

  /**
   * Gets the transaction type.
   *
   * @return the transaction type
   */
  Type getType();

  /**
   * Write instance to the supplied output stream.
   *
   * @param safeDataOutputStream the destination stream for the instance
   */
  void writeInner(SafeDataOutputStream safeDataOutputStream);

  @Override
  default void write(SafeDataOutputStream safeDataOutputStream) {
    safeDataOutputStream.writeEnum(getType());
    writeInner(safeDataOutputStream);
  }

  /**
   * Read a transaction from stream.
   *
   * @param safeDataInputStream the stream to read from
   * @return the read transaction
   */
  static Transaction read(SafeDataInputStream safeDataInputStream) {
    Type type = safeDataInputStream.readEnum(Type.values());
    return type.read(safeDataInputStream);
  }

  /** The type of transactions. */
  enum Type {
    /** Deploy contract. */
    DEPLOY_CONTRACT {
      @Override
      Transaction read(SafeDataInputStream safeDataInputStream) {
        return CreateContractTransaction.read(safeDataInputStream);
      }
    },
    /** Interact contract. */
    INTERACT_CONTRACT {
      @Override
      Transaction read(SafeDataInputStream safeDataInputStream) {
        return InteractWithContractTransaction.read(safeDataInputStream);
      }
    },
    /** Upgrade contract. */
    UPGRADE_CONTRACT {
      @Override
      Transaction read(SafeDataInputStream safeDataInputStream) {
        return UpgradeContractTransaction.read(safeDataInputStream);
      }
    };

    abstract Transaction read(SafeDataInputStream safeDataInputStream);
  }
}
