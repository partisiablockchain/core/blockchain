package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** Information about who requires a callback about the execution of a transaction. */
@Immutable
public final class ReturnEnvelope implements DataStreamSerializable, StateSerializable {

  private final BlockchainAddress contract;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public ReturnEnvelope() {
    contract = null;
  }

  /**
   * Default constructor.
   *
   * @param contract address of contract
   */
  public ReturnEnvelope(BlockchainAddress contract) {
    this.contract = Objects.requireNonNull(contract);
  }

  /**
   * Reads from the stream.
   *
   * @param stream source
   * @return the read object
   */
  public static ReturnEnvelope read(SafeDataInputStream stream) {
    return new ReturnEnvelope(BlockchainAddress.read(stream));
  }

  @Override
  @SuppressWarnings("ConstantConditions")
  public void write(SafeDataOutputStream stream) {
    contract.write(stream);
  }

  /**
   * Get address of contract.
   *
   * @return contract address
   */
  public BlockchainAddress contract() {
    return contract;
  }

  @Override
  public String toString() {
    return "ReturnEnvelope{" + ", contract=" + contract + '}';
  }
}
