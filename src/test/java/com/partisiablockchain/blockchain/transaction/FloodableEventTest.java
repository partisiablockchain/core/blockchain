package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateSerializer;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FloodableEventTest {

  @Test
  public void shouldVerifyOnCreate() {
    ExecutableEvent executableEvent = createEvent(1);
    StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            "ChainId",
            "ShardId",
            executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    Assertions.assertThatThrownBy(
            () ->
                FloodableEvent.create(
                    createEvent(2),
                    new FinalBlock(
                        new Block(
                            10L,
                            1L,
                            1L,
                            Block.GENESIS_PARENT,
                            state.getHash(),
                            List.of(),
                            List.of()),
                        new byte[0]),
                    stateStorage))
        .hasMessageContaining("Unable to produce proof");
    Assertions.assertThatThrownBy(
            () ->
                FloodableEvent.create(
                    executableEvent,
                    new FinalBlock(
                        new Block(
                            10L,
                            1L,
                            0,
                            Block.GENESIS_PARENT,
                            state.getHash(),
                            List.of(),
                            List.of()),
                        new byte[0]),
                    stateStorage))
        .hasMessageContaining("Block finalizing event has smaller committeeId than event");
  }

  static ExecutableEvent createEvent(int committeeId) {
    return new ExecutableEvent(
        "ShardId",
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_THREE,
            10L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("DestinationShard", 27),
            committeeId,
            1L));
  }
}
