package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateLong;
import com.secata.stream.SafeDataInputStream;

/** Upgradable sys contract for testing new upgrade flow. */
public final class UpgradableSysContract extends SysContract<StateLong>
    implements UpgradableContract<SysContractContext, StateLong> {

  @Override
  public StateLong onCreate(SysContractContext context, SafeDataInputStream rpc) {
    return new StateLong(rpc.readLong());
  }

  @Override
  public StateLong onUpgradeState(
      SysContractContext context,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    if (permit == null) {
      throw new RuntimeException("Permit must be present!");
    }
    byte[] actualRpc = removeContractIdentifierHeader(rpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    input.readBoolean(); // Skip isAllowedFlag
    long toAdd = input.readLong();
    long oldValue = oldState.get("value").longValue();
    return new StateLong(oldValue + toAdd);
  }
}
