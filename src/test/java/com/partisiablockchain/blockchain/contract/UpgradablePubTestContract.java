package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateLong;
import com.secata.stream.SafeDataInputStream;

/** Upgradable pub contract for test purposes. */
public final class UpgradablePubTestContract extends PubContract<StateLong>
    implements UpgradableContract<PubContractContext, StateLong> {

  @Override
  public StateLong onCreate(PubContractContext context, SafeDataInputStream rpc) {
    return new StateLong(rpc.readLong());
  }

  @Override
  public StateLong onUpgradeState(
      PubContractContext context,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    byte[] actualRpc = removeContractIdentifierHeader(rpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    input.readBoolean(); // Skip isAllowedFlag
    long toAdd = input.readLong();
    long oldValue = oldState.get("value").longValue();
    return new StateLong(oldValue + toAdd);
  }
}
