package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.ChainPlugin.PluginIdentifier;
import com.partisiablockchain.blockchain.ChainPlugin.PluginLoader;
import com.partisiablockchain.blockchain.ChainState.PluginSerialization;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateStorage;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Component for lazy loading of contract and binder bytecode.
 *
 * <p>Contract bytecode is stored in the underlying {@link StateStorage}, and is loaded up and
 * accessible as {@link BlockchainContract}s.
 *
 * <p>Contracts are initializing depending upon the binder's constructor:
 *
 * <ul>
 *   <li>Raw binders: Accept a byte array in the binder constructor ({@code MyBinder(byte[]
 *       contractBytes)}}) Binder decides how to interpret the given bytes. WASM binders are an
 *       example of an raw binder.
 *   <li>Java-contract binders: Binder constructor expects a Java-based contract ({@code
 *       MyPubBinder(PubContract contract)}}) Binder can only interact with contract through the
 *       contract's interface. Most governance contracts are Java-contract binder.
 * </ul>
 *
 * <h2>Caching</h2>
 *
 * <p>{@link ChainStateCache} loads contracts lazily, and does not automatically remove them.
 * Contracts are only removed from the cache if they are deleted from the chain state, and marked
 * for deletion ({@link #markContractsForRemoval}). Removals are effectuated in a delayed fashion,
 * as a consequence of the consensus model.
 *
 * <p>When a contract is removed from the chain state and marked for removal ({@link
 * markContractsForRemoval}) it is added to {@link #numBlocksUntilContractRemoval} (with {@code
 * count=} {@link #CONTRACT_REMOVAL_DELAY_IN_BLOCKS}), and is removed from the cache if not used for
 * the next {@link #CONTRACT_REMOVAL_DELAY_IN_BLOCKS} blocks.
 *
 * <h2>Terminology</h2>
 *
 * <p>When this class refers to {@link Hash}es as JARs, its because the {@link Hash} is the {@link
 * Hash} of the given JAR, and is used to look it up.
 */
@SuppressWarnings("unchecked")
final class ChainStateCache {

  /**
   * The maximum number of deletion counters a contract cache entry can have before being removed
   * from the cache.
   *
   * <p>Reasoning for current value ({@code 10}): The consesus model allows contracts to possibly be
   * read after they have been deleted. {@code 2} should be enough, but an additional buffer of
   * {@code 8} has been added.
   */
  static final int CONTRACT_REMOVAL_DELAY_IN_BLOCKS = 10;

  /** Storage for loading and writing contract and binder bytecode. */
  private final StateStorage storage;

  /** Stores loaded plugins. */
  private final Map<Hash, PluginSerialization<?, ?, ?>> plugins;

  /** Stores loaded blockchain contracts. */
  private final Map<BlockchainAddress, BlockchainContractWithJars> blockchainContracts;

  /**
   * Tracks how many blocks are remaining before tracked contracts are removed from the cache.
   *
   * <p>Keys is a subset of the keys of {@link #blockchainContracts}.
   *
   * @see ChainStateCache for more documentation.
   */
  private final Map<BlockchainAddress, Integer> numBlocksUntilContractRemoval;

  /**
   * Cache of {@link ClassLoader}s for initialized binders. {@link ClassLoader}s are reused between
   * contracts using the same binder.
   */
  private final Map<Hash, JarClassLoader> binderClassLoaders;

  /**
   * Cache of {@link ClassLoader}s for initialized Java contracts. {@link ClassLoader}s are reused
   * between contracts using the same contract and binder Jars.
   */
  private final Map<ContractAndBinder, JarClassLoader> contractClassLoaders;

  /**
   * Create cache for given {@link StateStorage} instance.
   *
   * @param storage Storage to load contract and binder bytecode from. Not nullable.
   */
  ChainStateCache(StateStorage storage) {
    this.storage = Objects.requireNonNull(storage);
    this.blockchainContracts = new ConcurrentHashMap<>();
    this.numBlocksUntilContractRemoval = new ConcurrentHashMap<>();
    this.plugins = new ConcurrentHashMap<>();
    this.binderClassLoaders = new ConcurrentHashMap<>();
    this.contractClassLoaders = new ConcurrentHashMap<>();
  }

  /**
   * Cache entry of a contract along with its Binder JAR and contract definition.
   *
   * @param blockchainContract Loaded contract. Not nullable.
   * @param binderJar Binder JAR. Not nullable.
   * @param contractJar Contract definition. Possibly a JAR, but depending upon the binder it might
   *     be in a different format. Not nullable.
   */
  record BlockchainContractWithJars(
      BlockchainContract<?, ?> blockchainContract, Hash binderJar, Hash contractJar) {}

  /**
   * Get (and possibly initialize) the contract at the given address with the given binder and
   * contract JARs.
   *
   * @param address Address of the contract. Not nullable.
   * @param binderJar Binder JAR of the contract. Not nullable.
   * @param contractJar Contract definition bytes. Not nullable.
   * @return Cached or newly initialized {@link BlockchainContract}. Never null.
   */
  <T extends StateSerializable, U extends BinderEvent> BlockchainContract<T, U> get(
      BlockchainAddress address, Hash binderJar, Hash contractJar) {
    return (BlockchainContract<T, U>)
        blockchainContracts.compute(
                address,
                (contractAddress, existingContractWithJar) -> {
                  if (existingContractWithJar == null
                      || !hasSameJars(existingContractWithJar, binderJar, contractJar)) {
                    final Object instantiatedBinder = instantiateBinder(binderJar, contractJar);
                    return new BlockchainContractWithJars(
                        ChainState.createContract(
                            ContractType.fromAddress(address), instantiatedBinder),
                        binderJar,
                        contractJar);
                  } else {
                    numBlocksUntilContractRemoval.remove(contractAddress);
                    return blockchainContracts.get(contractAddress);
                  }
                })
            .blockchainContract;
  }

  /**
   * Instantiates the binder-contract combination for the given JARs.
   *
   * <p>Supports both raw binders and Java-contract binders.
   *
   * @param binderJar Hash of binder JAR of the contract. Not nullable.
   * @param contractJar Hash of contract definition bytes. Not nullable.
   * @return Newly instantiated binder. Not nullable.
   */
  private Object instantiateBinder(Hash binderJar, Hash contractJar) {
    final JarClassLoader binderClassLoader = getBinderClassLoader(binderJar);

    if (isRawBinder(binderClassLoader.getMainClass())) {
      final LargeByteArray contractData =
          createStateSerializer().read(contractJar, LargeByteArray.class);
      final Object contract = contractData.getData();
      return binderClassLoader.instantiateBinder(byte[].class, contract);

    } else /* is Java-contract binder */ {
      final JarClassLoader contractLoader = getContractClassLoader(contractJar, binderClassLoader);
      final Object contract = contractLoader.instantiateMainClass();
      return binderClassLoader.instantiateBinder(contract.getClass().getSuperclass(), contract);
    }
  }

  /**
   * Whether the given already-loaded contract has the same JAR as specified.
   *
   * @param contractWithJars Loaded contract. Not nullable..
   * @param binderJar Hash of the binder jar.
   * @param contractJar Hash of the contract jar.
   * @return true if the jars are precisely the same, false otherwise.
   */
  private boolean hasSameJars(
      BlockchainContractWithJars contractWithJars, Hash binderJar, Hash contractJar) {
    return contractWithJars.binderJar.equals(binderJar)
        && contractWithJars.contractJar.equals(contractJar);
  }

  /**
   * Whether the given class represents a "raw" binder, which is a binder that is initialized with
   * the contract as a byte array.
   *
   * @param type Class to check.
   * @return true if the class is a raw binder. False otherwise.
   */
  @SuppressWarnings("ReturnValueIgnored")
  private boolean isRawBinder(Class<?> type) {
    try {
      type.getDeclaredConstructor(byte[].class);
      return true;
    } catch (NoSuchMethodException e) {
      return false;
    }
  }

  /**
   * Gets (or initializes) the {@link ClassLoader} for the given binder JAR. The created {@link
   * ClassLoader} uses the system {@link ClassLoader} as fallback.
   *
   * @param binderJar Hash of the binder JAR. Not nullable.
   * @return Cached or newly initialized {@link JarClassLoader}. Never null.
   */
  private JarClassLoader getBinderClassLoader(Hash binderJar) {
    return binderClassLoaders.computeIfAbsent(
        binderJar, contractHash -> createClassLoader(createStateSerializer(), binderJar, null));
  }

  /**
   * Gets (or initializes) the {@link ClassLoader} for the given contract JAR. The created {@link
   * ClassLoader} uses the {@link ClassLoader} of the contract as fallback (which will itself use
   * the system {@link ClassLoader} as fallback.)
   *
   * @param contractJar Hash of the contract JAR. Not nullable.
   * @param binderClassLoader {@link ClassLoader} of the binder. Not nullable.
   * @return Cached or newly initialized {@link JarClassLoader}. Never null.
   */
  private JarClassLoader getContractClassLoader(
      Hash contractJar, JarClassLoader binderClassLoader) {
    ContractAndBinder contractAndBinder = new ContractAndBinder(contractJar, binderClassLoader);
    return contractClassLoaders.computeIfAbsent(
        contractAndBinder,
        k -> createClassLoader(createStateSerializer(), contractJar, binderClassLoader));
  }

  /**
   * Create new {@link JarClassLoader} for the given JAR, whether that be a binder, contract, or
   * plugin.
   *
   * @param jarLoader Serializer to load JAR through. Not nullable.
   * @param jarHash Hash of the JAR to load, and create a {@link ClassLoader} for. Not nullable.
   * @param fallbackClassLoader {@link ClassLoader} to use instead of the system {@link
   *     ClassLoader}. If {@code null}: set fallback to the system {@link ClassLoader}.
   * @return Newly created {@link JarClassLoader}. Not nullable.
   */
  private static JarClassLoader createClassLoader(
      StateSerializer jarLoader, Hash jarHash, JarClassLoader fallbackClassLoader) {
    LargeByteArray jarBytes = jarLoader.read(jarHash, LargeByteArray.class);
    JarClassLoader.Builder builder = JarClassLoader.builder(jarBytes.getData());
    if (fallbackClassLoader != null) {
      builder.defaultClassLoader(fallbackClassLoader);
    }
    return builder.build();
  }

  /**
   * Gets (or initializes) the blockchain plugin with the given JAR hash.
   *
   * @param pluginJar Hash of the plugin JAR. Not nullable.
   * @return Cached or newly initialized plugin. Never null.
   */
  <
          GlobalT extends StateSerializable,
          LocalT extends StateSerializable,
          PluginT extends BlockchainPlugin<GlobalT, LocalT>>
      PluginSerialization<GlobalT, LocalT, PluginT> getPlugin(Hash pluginJar) {
    return (PluginSerialization<GlobalT, LocalT, PluginT>)
        plugins.computeIfAbsent(
            pluginJar,
            hash ->
                new PluginSerialization<GlobalT, LocalT, PluginT>(
                    createStateSerializer(),
                    createStateSerializer(),
                    createClassLoader(createStateSerializer(), hash, null)));
  }

  /**
   * Update contract deletion counters. Given contracts are reset to {@code 0}, while all other
   * contracts are incremented.
   *
   * @param contractsForRemoval Contracts to mark for deletion.
   */
  void markContractsForRemoval(Set<BlockchainAddress> contractsForRemoval) {
    for (BlockchainAddress blockchainAddress : contractsForRemoval) {
      numBlocksUntilContractRemoval.put(blockchainAddress, CONTRACT_REMOVAL_DELAY_IN_BLOCKS);
    }
    for (BlockchainAddress blockchainAddress : Set.copyOf(numBlocksUntilContractRemoval.keySet())) {
      final int counter = numBlocksUntilContractRemoval.get(blockchainAddress);
      if (counter <= 0) {
        blockchainContracts.remove(blockchainAddress);
        numBlocksUntilContractRemoval.remove(blockchainAddress);
      } else {
        numBlocksUntilContractRemoval.put(blockchainAddress, counter - 1);
      }
    }
  }

  /**
   * Create {@link StateSerializer} from the underlying storage.
   *
   * <p>The produced {@link StateSerializer} assumes that written objects are either entirely new,
   * or were originally read from itself. Use {@link createStateSerializerFreshStorage} instead if
   * you need to write objects that were read from another {@link StateSerializer}.
   *
   * @return Newly created {@link StateSerializer}. Never null.
   */
  StateSerializer createStateSerializer() {
    return new StateSerializer(storage, true);
  }

  /**
   * Create {@link StateSerializer} from the underlying storage, assuming that the underlying
   * storage is freshly created, and doesn't necessarily contain objects that are being updated.
   *
   * <p>Implies worse write performance, as objects need to write all their constituents.
   *
   * <p>Primarily used for implementing {@code SyncEvent}s.
   *
   * @return Newly created {@link StateSerializer}. Never null.
   */
  StateSerializer createStateSerializerFreshStorage() {
    return new StateSerializer(storage, true, true);
  }

  /**
   * Create new {@link PluginLoader} for loading plugins through this {@link ChainStateCache}.
   *
   * @return Newly created plugin loader. Never null.
   */
  <L extends StateSerializable, P extends BlockchainPlugin<G, L>, G extends StateSerializable>
      PluginLoader<G, L, P> createPluginLoader() {
    return new PluginLoader<>() {
      @Override
      public PluginSerialization<G, L, P> load(Hash jar) {
        return getPlugin(jar);
      }

      @Override
      public PluginIdentifier loadId(Hash identifier) {
        return createStateSerializer().read(identifier, PluginIdentifier.class);
      }

      @Override
      public Hash saveId(PluginIdentifier pluginIdentifier) {
        return createStateSerializer().write(pluginIdentifier).hash();
      }
    };
  }
}
