package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.sys.SysBinderContext;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/** Default implementation of the contract contract. */
public final class SysBinderContextImpl extends BinderContextImpl implements SysBinderContext {

  SysBinderContextImpl(
      BlockchainContractContext contractContext, ExecutionContextTransaction executionContext) {
    super(contractContext, executionContext);
  }

  @Override
  public StateSerializable getGlobalAccountPluginState() {
    return getChainState().getGlobalPluginState(ChainPluginType.ACCOUNT);
  }

  @Override
  public PluginInteractionCreator getAccountPluginInteractions() {
    return getChainState().getPluginInteractions(ChainPluginType.ACCOUNT);
  }

  @Override
  public StateSerializable getGlobalConsensusPluginState() {
    return getChainState().getGlobalPluginState(ChainPluginType.CONSENSUS);
  }

  @Override
  public PluginInteractionCreator getConsensusPluginInteractions() {
    return getChainState().getPluginInteractions(ChainPluginType.CONSENSUS);
  }

  @Override
  public StateSerializable getGlobalSharedObjectStorePluginState() {
    return getChainState().getGlobalPluginState(ChainPluginType.SHARED_OBJECT_STORE);
  }

  @Override
  public Governance getGovernance() {
    return () -> getExecutionContext().getChainId();
  }

  @Override
  public String getFeature(String key) {
    return getChainState().getFeature(key);
  }

  @Override
  public void registerDeductedByocFees(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
    getExecutionContext().addPendingByocFee(amount, symbol, nodes);
  }

  @Override
  public long availableGas() {
    return getExecutionContext().availableGas();
  }

  @Override
  public void registerCpuFee(long gas) {
    getExecutionContext().registerCpuFee(gas);
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    getExecutionContext().addPendingServiceFees(target, gas);
  }

  @Override
  public void payInfrastructureFees(long gas, BlockchainAddress target) {
    getExecutionContext().addPendingInfrastructureFees(target, gas);
  }

  private MutableChainState getChainState() {
    return getExecutionContext().getState();
  }
}
