package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainLedgerTestHelper.createBlock;
import static com.partisiablockchain.blockchain.BlockchainLedgerTestHelper.createFinalBlock;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.TestContractByInvocation;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.ConnectionTest;
import com.partisiablockchain.flooding.NetworkNode;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import com.secata.tools.thread.ExecutorFactory;
import com.secata.tools.thread.TestExecutionQueue;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Test of {@link BlockchainConsensusPlugin} and various consensus models. */
public final class BlockchainLedgerConsensusTest extends CloseableTest {

  private static final byte[] JAR_PLUGIN_CONSENSUS = buildTestingPluginJar(SingleRollback.class);

  static final byte[] JAR_PLUGIN_CONSENSUS_SHARD = buildTestingPluginJar(WithShardConsensus.class);

  private static final byte[] JAR_PLUGIN_CONSENSUS_ZERO_ROLLBACK =
      buildTestingPluginJar(ZeroRollback.class);

  private static final byte[] JAR_PLUGIN_CONSENSUS_UPDATE_FOR_BLOCK_TEST_PLUGIN =
      buildTestingPluginJar(UpdateForBlockTestPlugin.class, LocalCounterState.class);

  private static final byte[] JAR_PLUGIN_FAST_TRACK =
      buildTestingPluginJar(MockFastTrackPlugin.class);

  static final byte[] JAR_PLUGIN_CONSENSUS_INNER = buildTestingPluginJar(InnerConsensus.class);

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private final KeyPair keyPair = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress signer = keyPair.getPublic().createAddress();
  private final KeyPair secondAccount = new KeyPair(BigInteger.valueOf(2));
  private BlockchainLedger blockchain;
  private Block block;
  private Block latestBlock;
  private byte nextFinalization = 0;

  private final ArrayList<Block> finalBlocks = new ArrayList<>();
  private final ArrayList<Block> proposals = new ArrayList<>();
  private BlockAndState genesis;
  private final DummyExecutableEventFinder finder = new DummyExecutableEventFinder();

  private final Map<MemoryStorage.PathAndHash, byte[]> storageData = new ConcurrentHashMap<>();
  private ExecutionFactoryFake executionFactory;

  /** Setup blockchain. */
  @BeforeEach
  public void setUp() {
    executionFactory = ExecutionFactoryFake.create();
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storageData),
                this::setupChain,
                network,
                finder,
                ExecutionFactoryThreaded.create()));
    finalBlocks.add(blockchain.getLatestBlock());
    blockchain.attach(
        new Listener() {
          @Override
          public void newFinalBlock(Block block, ImmutableChainState state) {
            finalBlocks.add(block);
          }

          @Override
          public void newBlockProposal(Block block) {
            proposals.add(block);
          }
        });
    genesis = blockchain.latest();
    latestBlock = blockchain.getLatestBlock();
    block = createBlock(blockchain.latest());
  }

  /** Getters on {@link ImmutableChainState} produce valid objects. */
  @Test
  public void getters() {
    ImmutableChainState state = blockchain.getChainState();
    Assertions.assertThat(state.getConsensusPlugin()).isInstanceOf(ConsensusPluginWrapper.class);

    final StateSerializable pluginState = state.getGlobalPluginState(ChainPluginType.CONSENSUS);
    Assertions.assertThat(pluginState).isInstanceOf(StateSerializable.class);
    Assertions.assertThat(pluginState.getClass().toString())
        .isEqualTo("class com.partisiablockchain.blockchain.BlockchainLedgerTestHelper$ValueCount");
  }

  @Test
  public void dropsConnectionOnError() throws InterruptedException {
    blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                mutable ->
                    mutable.setPlugin(
                        FunctionUtility.noOpBiConsumer(),
                        null,
                        ChainPluginType.CONSENSUS,
                        JAR_PLUGIN_FAST_TRACK,
                        new byte[0]),
                network,
                finder,
                executionFactory));
    StateStorageRaw storage = blockchain.getStateStorage();
    ExecutableEvent event = BlockchainLedgerShardTest.createEvent(null, 1);
    final String chainId = blockchain.getChainId();
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(storage, StateHelper::initial),
            chainId,
            null,
            event.identifier());
    StateSerializer stateSerializer = new StateSerializer(storage, true);
    stateSerializer.write(state.getExecutedState());

    Block block =
        new Block(
            System.currentTimeMillis(),
            3L,
            2L,
            Hash.create(s -> {}),
            state.getHash(),
            List.of(),
            List.of());
    final AtomicInteger closeCalled = new AtomicInteger();
    int pendingEventsBefore = blockchain.getPendingEvents().size();
    FinalBlock finalBlock = new FinalBlock(block, new byte[0]);
    ConnectionTest.MyInputStream inputStream =
        createInputForFloodable(FloodableEvent.create(event, finalBlock, storage));
    ConnectionTest.MyOutputStream outputStream = new ConnectionTest.MyOutputStream();
    String subChainId = blockchain.getChainState().getExecutedState().getSubChainId();
    NetworkRequestHandler networkRequestHandler =
        new NetworkRequestHandler(
            new BlockRequester(), blockchain, chainId, subChainId, executionFactory);
    CountDownLatch packetsHandled = new CountDownLatch(2);
    ConnectionTest.createTestConnection(
        (c, packet) -> {
          try {
            networkRequestHandler.newPacket(new NetworkNode.IncomingPacket(null, packet));
          } finally {
            packetsHandled.countDown();
          }
        },
        closeCalled::getAndIncrement,
        inputStream,
        outputStream,
        chainId,
        null,
        executionFactory);
    Assertions.assertThat(pendingEventsBefore).isEqualTo(blockchain.getPendingEvents().size());
    Assertions.assertThat(closeCalled.get()).isEqualTo(0);

    FinalBlock finalBlockAccepted = new FinalBlock(block, new byte[1]);
    ConnectionTest.MyInputStream inputStreamValid =
        createInputForFloodable(FloodableEvent.create(event, finalBlockAccepted, storage));
    ConnectionTest.createTestConnection(
        (c, packet) -> {
          try {
            networkRequestHandler.newPacket(new NetworkNode.IncomingPacket(null, packet));
          } finally {
            packetsHandled.countDown();
          }
        },
        closeCalled::getAndIncrement,
        inputStreamValid,
        outputStream,
        chainId,
        null,
        executionFactory);

    TestExecutionQueue executionQueue =
        executionFactory.getExecutionQueue(Connection.RECEIVER_THREAD_NAME);
    Assertions.assertThat(executionQueue.runStep(2)).isEqualTo(2);

    // Wait for the network thread to be done handling the incoming packet
    Assertions.assertThat(packetsHandled.await(15, TimeUnit.SECONDS)).isTrue();
    Assertions.assertThat(packetsHandled.getCount()).isEqualTo(0);

    Assertions.assertThat(pendingEventsBefore).isEqualTo(blockchain.getPendingEvents().size());
    Assertions.assertThat(closeCalled.get()).isEqualTo(0);
  }

  private static ConnectionTest.MyInputStream createInputForFloodable(
      FloodableEvent floodableEvent) {
    Packet<FloodableEvent> transactionPacket = new Packet<>(Packet.Type.EVENT, floodableEvent);
    return new ConnectionTest.MyInputStream(ConnectionTest.packetBytes(transactionPacket));
  }

  @Test
  public void invalidFinalBlock() {
    final byte[] invalidFinalization = new byte[0];
    final byte[] validFinalization = new byte[] {nextFinalization};
    testInvalidFinalized(block, invalidFinalization);
    testInvalidFinalized(
        createBlock(
            latestBlock.getProductionTime(),
            latestBlock.getBlockTime() + 1,
            TestObjects.EMPTY_HASH,
            blockchain.latest().getState().getHash(),
            List.of(),
            List.of()),
        validFinalization);
  }

  @Test
  public void getExecutedPossibleExecutedTransaction() {
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[10]);
    SignedTransaction signedTransaction =
        BlockchainLedgerTestHelper.signedTransaction(blockchain, keyPair, signer, interact);
    blockchain.addPendingTransaction(signedTransaction);
    FinalBlock finalBlock =
        new FinalBlock(
            createBlock(blockchain.latest(), signedTransaction.identifier()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(finalBlock);

    interact = InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[1]);
    SignedTransaction signedTransaction2 =
        BlockchainLedgerTestHelper.signedTransaction(
            blockchain, keyPair, keyPair.getPublic().createAddress(), interact);
    blockchain.addPendingTransaction(signedTransaction2);
    FinalBlock finalBlock2 =
        new FinalBlock(
            createBlock(blockchain.latest(), signedTransaction2.identifier()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(finalBlock2);

    BlockchainLedger.PossiblyFinalizedTransaction transaction =
        blockchain.getExecutedTransaction(signedTransaction2.identifier());
    boolean isInProposal = blockchain.isInProposals(finalBlock2.getBlock().identifier());
    Assertions.assertThat(isInProposal).isTrue();
    Assertions.assertThat(blockchain.isInProposals(Hash.create(s -> s.writeString("hej"))))
        .isFalse();
    Assertions.assertThat(blockchain.getTransaction(signedTransaction2.identifier())).isNull();
    Assertions.assertThat(transaction).isNotNull();
    Assertions.assertThat(transaction.finalized()).isFalse();
    Assertions.assertThat(transaction.transaction().getInner().identifier())
        .isEqualTo(signedTransaction2.identifier());
    Assertions.assertThat(finalBlock2.getBlock().identifier())
        .isEqualTo(transaction.transaction().getBlockHash());
    SignedTransaction signedTransactionGotten =
        (SignedTransaction) transaction.transaction().getInner();
    Assertions.assertThat(signedTransactionGotten).isEqualTo(signedTransaction2);
  }

  private void updatedConsensusPlugin(byte[] updatedPluginJar) {
    BlockchainLedgerTestHelper.appendBlock(
        blockchain,
        keyPair,
        signer,
        nextFinalization,
        UpdateConsensusInTest.create(updatedPluginJar));
    nextFinalization = 1;
  }

  @Test
  public void possibleHeads() {
    BlockAndState latest = blockchain.latest();
    FinalBlock block = createFinalBlock(latest, nextFinalization);
    blockchain.appendBlock(block);
    blockchain.appendBlock(
        new FinalBlock(
            new Block(
                System.currentTimeMillis() + 1,
                latest.getBlockTime() + 1,
                latest.getBlockTime(),
                latest.getBlock().identifier(),
                latest.getState().getHash(),
                List.of(),
                List.of(new Hash[] {})),
            new byte[] {nextFinalization}));
    Assertions.assertThat(blockchain.getProposals()).hasSize(2);
    List<BlockAndStateWithParent> possibleHeads = blockchain.getPossibleHeads();
    Assertions.assertThat(possibleHeads).hasSize(3);
    Assertions.assertThat(possibleHeads.get(0).finalState()).isEqualTo(latest.getState());
    Assertions.assertThat(possibleHeads.get(1).finalState()).isEqualTo(latest.getState());
    Assertions.assertThat(possibleHeads.get(2).currentBlock()).isEqualTo(latest.getBlock());
    Assertions.assertThat(possibleHeads.get(2).contracts().newContracts)
        .hasSize(1)
        .containsExactly(TestObjects.CONTRACT_SYS);
    Assertions.assertThat(possibleHeads.get(2).contracts().updatedContracts).isEmpty();
    Assertions.assertThat(possibleHeads.get(2).contracts().removedContract).isEmpty();
  }

  @Test
  public void proposeTwice() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(genesis.getBlock());
  }

  @Test
  public void finalizeProposal() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    FinalBlock secondBlock = createFinalBlock(blockchain.getProposals().get(0), nextFinalization);
    blockchain.appendBlock(secondBlock);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void proposalShouldBeConsiderProposalAfterReboot() {
    FinalBlock block = createFinalBlock(blockchain.latest(), nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);

    blockchain.close();
    this.blockchain =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(),
                MemoryStorage.createRootDirectory(temporaryFolder.toFile(), storageData),
                null));

    Assertions.assertThat(blockchain.getLatestBlock()).isNotEqualTo(block.getBlock());
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    Assertions.assertThat(blockchain.getProposals().get(0).getBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void proposalAndImmediateFinalizationOfNext() {
    updatedConsensusPlugin(JAR_PLUGIN_CONSENSUS_ZERO_ROLLBACK);
    appendBlockProposal();
  }

  @Test
  public void proposalRolledBackByImmediateFinalization() {
    BlockAndState latestBlock = blockchain.latest();
    FinalBlock block = createFinalBlock(latestBlock, nextFinalization);
    blockchain.appendBlock(block);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    FinalBlock finalBlock =
        new FinalBlock(
            new Block(
                // Adding 1 to currentTimeMillis so the block won't collide with previous making it
                // historical.
                System.currentTimeMillis() + 1,
                latestBlock.getBlockTime() + 1,
                latestBlock.getBlockTime() + 1,
                latestBlock.getBlock().identifier(),
                latestBlock.getState().getHash(),
                List.of(),
                List.of()),
            new byte[] {nextFinalization, 1});
    blockchain.appendBlock(finalBlock);
    Assertions.assertThat(blockchain.getProposals()).hasSize(0);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(finalBlock.getBlock());
  }

  @Test
  public void addPendingValidInProposal() {
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[0]);
    BlockchainLedgerTestHelper.appendBlock(blockchain, keyPair, signer, nextFinalization, interact);
    Assertions.assertThat(blockchain.getProposals()).hasSize(1);
    BlockAndState firstProposal = blockchain.getProposals().get(0);
    FinalBlock secondProposal = createFinalBlock(genesis, nextFinalization);
    blockchain.appendBlock(secondProposal);
    Assertions.assertThat(blockchain.getProposals()).hasSize(2);

    Assertions.assertThat(
            blockchain.addPendingTransaction(
                SignedTransaction.create(
                        CoreTransactionPart.create(
                            firstProposal.getState().getAccount(signer).getNonce(),
                            System.currentTimeMillis() + 100_000,
                            0),
                        interact)
                    .sign(keyPair, blockchain.getChainId())))
        .isTrue();

    Assertions.assertThat(blockchain.getPendingTransactions()).hasSize(2);
    blockchain.appendBlock(createFinalBlock(blockchain.getProposals().get(1), nextFinalization));
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(secondProposal.getBlock());
    Assertions.assertThat(blockchain.getPendingTransactions()).hasSize(1);
  }

  @Test
  public void noMatchingProposal() {
    FinalBlock finalBlock = createFinalBlock(genesis, nextFinalization);
    FinalBlock unknownParent =
        new FinalBlock(
            createBlock(
                finalBlock.getBlock().getProductionTime(),
                finalBlock.getBlock().getBlockTime() + 1,
                finalBlock.getBlock().identifier(),
                TestObjects.EMPTY_HASH,
                List.of(),
                List.of()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(unknownParent);
    Assertions.assertThat(blockchain.getProposals()).isEmpty();
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(genesis.getBlock());
  }

  @Test
  public void consensusJar() {
    ImmutableChainState immutableChainState = blockchain.getChainState();
    LargeByteArray statePluginJar = immutableChainState.getPluginJar(ChainPluginType.CONSENSUS);
    Assertions.assertThat(statePluginJar).isNotNull();
    Assertions.assertThat(statePluginJar.getData()).isEqualTo(JAR_PLUGIN_CONSENSUS);

    MutableChainState mutableChainState = immutableChainState.asMutable();
    LargeByteArray mutableStatePlugin = mutableChainState.getPluginJar(ChainPluginType.CONSENSUS);
    Assertions.assertThat(mutableStatePlugin).isNotNull();
    Assertions.assertThat(mutableStatePlugin.getData()).isEqualTo(JAR_PLUGIN_CONSENSUS);
  }

  /** Consensus plugin is called for each produced block. */
  @Test
  public void updateForBlockIsCalled() throws Exception {
    updatedConsensusPlugin(JAR_PLUGIN_CONSENSUS_ZERO_ROLLBACK);
    appendBlockProposal();
    updatedConsensusPlugin(JAR_PLUGIN_CONSENSUS_UPDATE_FOR_BLOCK_TEST_PLUGIN);

    for (int i = 0; i < 100; i++) {
      blockchain.appendBlock(
          new FinalBlock(createBlock(blockchain.latest()), new byte[] {nextFinalization}));
      // If the block production time is not strictly later (millisecond precision)
      // than the previous block, it will not be valid.
      // Therefore the test should wait 1 ms between appending blocks.
      TimeUnit.MILLISECONDS.sleep(1);
    }

    // Look for state in ImmutableChainState
    StateAccessor state =
        StateAccessor.create(blockchain.getChainState().getLocalConsensusPluginState());
    Assertions.assertThat(state.get("count").intValue()).isEqualTo(100);

    // Look for state in MutableChainState
    MutableChainState mutableChainState = blockchain.getChainState().asMutable();
    state = StateAccessor.create(mutableChainState.getLocalConsensusPluginState());
    Assertions.assertThat(state.get("count").intValue()).isEqualTo(100);
  }

  @Test
  public void noInlineExecutionWithUnhandledEvents() {
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                mutableChainState -> {
                  setupChain(mutableChainState);
                  // Fake a spawned event that has not yet been executed
                  mutableChainState.routeToShard((String) null);
                },
                network,
                new DummyExecutableEventFinder(),
                executionFactory));
    updatedConsensusPlugin(JAR_PLUGIN_CONSENSUS_SHARD);
    ImmutableChainState state = blockchain.getProposals().get(0).getState();
    Assertions.assertThat(state.getExecutedState().getExecutionStatus().size()).isEqualTo(1);
    Assertions.assertThat(blockchain.getPendingEvents()).hasSize(1);
  }

  @Test
  public void addValidPendingTransactionDuringAppendBlockCall()
      throws InterruptedException, ExecutionException, TimeoutException {
    BlockchainAddress blockingContractAddress =
        BlockchainAddress.fromString("020000000000000000000000000000000000000020");
    BlockchainAddress validContractAddress =
        BlockchainAddress.fromString("020000000000000000000000000000000006666666");
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                mutableChainState -> {
                  setupChain(mutableChainState);
                  CoreContractStateTest.createContract(
                      mutableChainState,
                      JarBuilder.buildJar(TestBlockingContract.class),
                      blockingContractAddress,
                      TestContracts.CONTRACT_BINDER_PUBLIC);
                  CoreContractStateTest.createContract(
                      mutableChainState,
                      JarBuilder.buildJar(TestContractByInvocation.class),
                      validContractAddress,
                      TestContracts.CONTRACT_BINDER_PUBLIC);
                },
                network,
                new DummyExecutableEventFinder(),
                executionFactory));

    AccountState secondAccountState =
        blockchain.getAccountState(secondAccount.getPublic().createAddress());
    SignedTransaction firstValidTransaction =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    secondAccountState.getNonce(), System.currentTimeMillis() + 100_000, 10_000),
                InteractWithContractTransaction.create(
                    validContractAddress,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeInt(1);
                          s.writeLong(1);
                        })))
            .sign(secondAccount, blockchain.getChainId());
    Assertions.assertThat(blockchain.addPendingTransaction(firstValidTransaction)).isTrue();

    FinalBlock finalBlock =
        new FinalBlock(
            createBlock(blockchain.latest(), firstValidTransaction.identifier()),
            new byte[] {nextFinalization});
    blockchain.appendBlock(finalBlock);

    // Assert that the first valid transaction is in a block proposal
    BlockchainLedger.PossiblyFinalizedTransaction firstExecutedTransaction =
        blockchain.getExecutedTransaction(firstValidTransaction.identifier());
    Assertions.assertThat(firstExecutedTransaction).isNotNull();
    Assertions.assertThat(firstExecutedTransaction.finalized()).isFalse();

    SignedTransaction secondValidTransaction =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    secondAccountState.getNonce() + 1,
                    System.currentTimeMillis() + 100_000,
                    10_000),
                InteractWithContractTransaction.create(
                    validContractAddress,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeInt(2);
                          s.writeLong(1);
                        })))
            .sign(secondAccount, blockchain.getChainId());
    // Second transaction is valid before executing the next block (e.g. in current block proposal)
    Assertions.assertThat(blockchain.isTransactionValid(secondValidTransaction)).isTrue();

    BlockAndState proposedBlock = blockchain.getProposals().stream().findFirst().orElseThrow();
    InteractWithContractTransaction blockingInteract =
        InteractWithContractTransaction.create(blockingContractAddress, new byte[0]);
    SignedTransaction blockingTransaction =
        BlockchainLedgerTestHelper.signedTransaction(blockchain, keyPair, signer, blockingInteract);
    Assertions.assertThat(blockchain.addPendingTransaction(blockingTransaction)).isTrue();
    FinalBlock blockingFinalBlock =
        new FinalBlock(
            createBlock(proposedBlock, blockingTransaction.identifier()),
            new byte[] {nextFinalization});
    ExecutorService testRunner = ExecutorFactory.newSingle("TestRunner");
    CountDownLatchPair countDownLatches =
        new CountDownLatchPair(new CountDownLatch(1), new CountDownLatch(1));
    final Future<?> submit =
        testRunner.submit(
            () -> {
              blockingContractThreadLocal.set(countDownLatches);
              blockchain.appendBlock(blockingFinalBlock);
            });

    Assertions.assertThat(countDownLatches.testLatch().await(5, TimeUnit.SECONDS)).isTrue();

    // Second transaction is valid during block execution, still in current proposal
    Assertions.assertThat(blockchain.isTransactionValid(secondValidTransaction)).isTrue();

    countDownLatches.contractLatch().countDown();

    // Wait for the future to complete.
    submit.get(1, TimeUnit.SECONDS);
    Assertions.assertThat(blockchain.getExecutedTransaction(blockingTransaction.identifier()))
        .isNotNull();
    // Second transaction is valid after block execution, now in latest final block
    Assertions.assertThat(blockchain.isTransactionValid(secondValidTransaction)).isTrue();
  }

  private void appendBlockProposal() {
    List<BlockAndState> proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(1);
    BlockAndState parent = proposals.get(0);
    FinalBlock nextBlock = appendProposal(parent);

    proposals = blockchain.getProposals();
    Assertions.assertThat(proposals).hasSize(0);
    Assertions.assertThat(blockchain.getLatestBlock()).isEqualTo(nextBlock.getBlock());

    assertAsync(finalBlocks, genesis.getBlock(), parent.getBlock(), nextBlock.getBlock());
    assertAsync(this.proposals, parent.getBlock(), nextBlock.getBlock());
  }

  private FinalBlock appendProposal(BlockAndState parent) {
    Block parentBlock = parent.getBlock();
    FinalBlock finalBlock =
        new FinalBlock(
            new Block(
                System.currentTimeMillis(),
                parentBlock.getBlockTime() + 1,
                parentBlock.getBlockTime(),
                parentBlock.identifier(),
                parent.getState().getHash(),
                List.of(),
                List.of()),
            new byte[] {1});
    blockchain.appendBlock(finalBlock);
    return finalBlock;
  }

  private void assertAsync(List<Block> proposals, Block... block) {
    waitForSize(proposals, block.length);
    Assertions.assertThat(proposals).containsExactly(block);
  }

  private void waitForSize(List<Block> proposals, int length) {

    //    TestExecutionQueue executionQueue =
    //        executionFactory.getExecutionQueue(BlockchainLedger.LEDGER_LISTENER);
    //     executionQueue.runStep(length);
    //    ThreadedTestHelper.waitForCondition(() -> proposals.size() == length, 1_000);
    Assertions.assertThat(proposals).hasSize(length);
  }

  private void testInvalidFinalized(Block block, byte[] finalizationData) {
    Assertions.assertThatThrownBy(
        () -> blockchain.appendBlock(new FinalBlock(block, finalizationData)));
  }

  private void setupChain(MutableChainState mutableChainState) {
    mutableChainState.createAccount(signer);
    mutableChainState.createAccount(secondAccount.getPublic().createAddress());
    configureConsensusWithRollback(mutableChainState);
    Assertions.assertThat(mutableChainState.getConsensusPlugin())
        .isInstanceOf(ConsensusPluginWrapper.class);
    StateSerializable globalConsensus =
        mutableChainState.getGlobalPluginState(ChainPluginType.CONSENSUS);
    Assertions.assertThat(globalConsensus).isNotNull();

    final StateAccessor access = StateAccessor.create(globalConsensus);

    Assertions.assertThat(access.get("value").byteValue()).isEqualTo((byte) 0);

    CoreContractStateTest.createContract(
        mutableChainState,
        JarBuilder.buildJar(UpdateConsensusInTest.class),
        TestObjects.CONTRACT_SYS,
        TestContracts.CONTRACT_BINDER_SYSTEM);
  }

  static void configureConsensusWithRollback(MutableChainState mutableChainState) {
    byte[] rpc = new byte[0];
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        JAR_PLUGIN_CONSENSUS,
        rpc);
  }

  /** Test. */
  @Immutable
  public abstract static class AbstractConsensus
      extends BlockchainConsensusPlugin<SingleAddressState, StateVoid> {

    @Override
    public Class<SingleAddressState> getGlobalStateClass() {
      return SingleAddressState.class;
    }

    @Override
    public InvokeResult<SingleAddressState> invokeGlobal(
        PluginContext pluginContext, SingleAddressState state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public StateVoid updateForBlock(
        PluginContext pluginContext,
        SingleAddressState globalState,
        StateVoid localState,
        Block block) {
      return localState;
    }

    @Override
    public SingleAddressState migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new SingleAddressState(BlockchainAddress.read(rpc));
    }

    @Override
    public final StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }

    @Override
    public Class<StateVoid> getLocalStateClass() {
      return StateVoid.class;
    }
  }

  /** Test. */
  @Immutable
  public static final class InnerConsensus extends AbstractConsensus {

    @Override
    public BlockValidation validateLocalBlock(
        SingleAddressState globalState, StateVoid local, FinalBlock block) {
      Signature signature =
          SafeDataInputStream.readFully(block.getFinalizationData(), Signature::read);
      BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
      boolean accepted = signer.equals(globalState.address());
      if (accepted) {
        return BlockValidation.createAccepted(false);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(SingleAddressState globalState, FinalBlock block) {
      Signature signature =
          SafeDataInputStream.readFully(block.getFinalizationData(), Signature::read);
      BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
      return signer.equals(globalState.address());
    }
  }

  /** Test state for {@link AbstractConsensus}. */
  @Immutable
  public record SingleAddressState(BlockchainAddress address) implements StateSerializable {}

  /** Test. */
  @Immutable
  public static final class UpdateForBlockTestPlugin
      extends BlockchainConsensusPlugin<StateVoid, LocalCounterState> {

    @Override
    public BlockValidation validateLocalBlock(
        StateVoid globalState, LocalCounterState local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public boolean validateExternalBlock(StateVoid globalState, FinalBlock block) {
      return false;
    }

    @Override
    public InvokeResult<StateVoid> invokeGlobal(
        PluginContext pluginContext, StateVoid state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public StateVoid migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }

    @Override
    public LocalCounterState migrateLocal(StateAccessor currentLocal) {
      return new LocalCounterState(0);
    }

    @Override
    public LocalCounterState updateForBlock(
        PluginContext pluginContext,
        StateVoid globalState,
        LocalCounterState localState,
        Block block) {
      return localState.increment();
    }

    @Override
    public Class<StateVoid> getGlobalStateClass() {
      return StateVoid.class;
    }

    @Override
    public Class<LocalCounterState> getLocalStateClass() {
      return LocalCounterState.class;
    }
  }

  /** Test consensus model with single rollback. */
  @Immutable
  public static final class SingleRollback
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    /** Default constructor. */
    public SingleRollback() {}

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      byte[] finalizationData = block.getFinalizationData();
      if (finalizationData[0] == globalState.value()) {
        return BlockValidation.createAccepted(
            finalizationData.length <= 1 || finalizationData[1] == 0);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return false;
    }

    @Override
    public InvokeResult<BlockchainLedgerTestHelper.ValueCount> invokeGlobal(
        PluginContext pluginContext, BlockchainLedgerTestHelper.ValueCount state, byte[] rpc) {
      return new InvokeResult<>(state.setByte(rpc[0]), rpc);
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new BlockchainLedgerTestHelper.ValueCount((byte) 0);
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }
  }

  /** Test. */
  @Immutable
  public static final class WithShardConsensus
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return false;
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new BlockchainLedgerTestHelper.ValueCount(rpc.readSignedByte());
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }
  }

  /** Test. */
  @Immutable
  public static final class ZeroRollback
      extends TestConsensusPlugin<BlockchainLedgerTestHelper.ValueCount> {

    private static final Logger logger = LoggerFactory.getLogger(ZeroRollback.class);

    @Override
    public BlockValidation validateLocalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, StateVoid local, FinalBlock block) {
      if (validateExternalBlock(globalState, block)) {
        return BlockValidation.createAccepted(false);
      } else {
        return BlockValidation.createRejected();
      }
    }

    @Override
    public boolean validateExternalBlock(
        BlockchainLedgerTestHelper.ValueCount globalState, FinalBlock block) {
      return block.getFinalizationData()[0] == globalState.value();
    }

    @Override
    public Class<BlockchainLedgerTestHelper.ValueCount> getGlobalStateClass() {
      return BlockchainLedgerTestHelper.ValueCount.class;
    }

    @Override
    public BlockchainLedgerTestHelper.ValueCount migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      BlockchainLedgerTestHelper.ValueCount valueCount =
          new BlockchainLedgerTestHelper.ValueCount((byte) 0);
      try {
        byte value = rpc.readSignedByte();
        valueCount = valueCount.setByte(value);
      } catch (Exception e) {
        logger.trace("Ignoring exception", e);
      }
      return valueCount;
    }

    @Override
    public StateVoid migrateLocal(StateAccessor currentLocal) {
      return null;
    }
  }

  /** Test. */
  public static final class UpdateConsensusInTest extends SysContract<StateVoid> {

    static InteractWithContractTransaction create(byte[] jar) {
      return InteractWithContractTransaction.create(
          TestObjects.CONTRACT_SYS,
          SafeDataOutputStream.serialize(stream -> stream.writeDynamicBytes(jar)));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      context.getInvocationCreator().updateConsensusPlugin(jar, new byte[] {1});
      return null;
    }
  }

  /** Test state for testing blockchain through {@link UpdateForBlockTestPlugin}. */
  @Immutable
  public record LocalCounterState(int count) implements StateSerializable {
    /**
     * Increment the counter.
     *
     * @return updated state with count incremented
     */
    public LocalCounterState increment() {
      return new LocalCounterState(count + 1);
    }
  }

  /** Test. */
  @Immutable
  public abstract static class TestConsensusPlugin<GlobalT extends StateSerializable>
      extends BlockchainConsensusPlugin<GlobalT, StateVoid> {

    @Override
    public BlockValidation validateLocalBlock(
        GlobalT globalState, StateVoid local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public InvokeResult<GlobalT> invokeGlobal(
        PluginContext pluginContext, GlobalT state, byte[] rpc) {
      return new InvokeResult<>(state, rpc);
    }

    @Override
    public GlobalT migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return null;
    }

    @Override
    public StateVoid updateForBlock(
        PluginContext pluginContext, GlobalT globalState, StateVoid localState, Block block) {
      return localState;
    }

    @Override
    public Class<StateVoid> getLocalStateClass() {
      return StateVoid.class;
    }
  }

  /** Test. */
  public record CountDownLatchPair(CountDownLatch testLatch, CountDownLatch contractLatch) {}

  /**
   * This thread local is used to hold a pair of CountDownLatches that allows us to block execution
   * of a FinalBlock in a thread. One latch is used to indicate that onInvoke() has been called, and
   * the other is used to indicate when the execution can continue.
   */
  public static final ThreadLocal<CountDownLatchPair> blockingContractThreadLocal =
      new ThreadLocal<>();

  /** Test contract. Blocks execution until latch is counted down. */
  public static final class TestBlockingContract extends PubContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        PubContractContext context, StateVoid state, SafeDataInputStream rpc) {
      CountDownLatchPair countDownLatchPair = blockingContractThreadLocal.get();
      countDownLatchPair.testLatch().countDown();

      final boolean gotThroughLatch =
          ExceptionConverter.call(
              () -> countDownLatchPair.contractLatch().await(5, TimeUnit.SECONDS));
      if (!gotThroughLatch) {
        throw new RuntimeException("Expected latch to open withing 5 seconds!");
      }
      return state;
    }
  }

  /**
   * Create new JAR for given plugin class, and additional classes. Will automatically include
   * certain important classes
   *
   * @param mainClass Main class of the plugin. Not nullable.
   * @param additionalClasses Any additional classes to include in the JAR.
   * @return Created JAR.
   */
  public static byte[] buildTestingPluginJar(
      final Class<?> mainClass, Class<?>... additionalClasses) {

    additionalClasses = Arrays.copyOf(additionalClasses, additionalClasses.length + 6);
    additionalClasses[additionalClasses.length - 6] = SingleAddressState.class;
    additionalClasses[additionalClasses.length - 5] = AbstractConsensus.class;
    additionalClasses[additionalClasses.length - 4] = TestConsensusPlugin.class;
    additionalClasses[additionalClasses.length - 3] = BlockchainLedgerTestHelper.class;
    additionalClasses[additionalClasses.length - 2] =
        BlockchainLedgerTestHelper.ByteVerifying.class;
    additionalClasses[additionalClasses.length - 1] = BlockchainLedgerTestHelper.ValueCount.class;

    return JarBuilder.buildJar(mainClass, additionalClasses);
  }
}
