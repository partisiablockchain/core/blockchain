package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializer;
import java.util.Comparator;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Storage for blocks and states. */
public final class BlockStateStorage implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(BlockStateStorage.class);
  private final BlockTimeStorage latestBlockTime;
  private final HashStorage<FinalBlock> blockDir;
  private final HashStorage<Hash> blockTimeDir;
  private final StateStorageCached stateDir;
  private final StateSerializer stateSerializer;
  private final HashStorage<Hash> blockToStateDir;

  /**
   * Create a new storage.
   *
   * @param root root directory to place data in
   */
  public BlockStateStorage(RootDirectory root) {
    this.blockDir = root.createHashStorage("blocks", FinalBlock::read);
    this.blockTimeDir = root.createHashStorage("blockTime", Hash::read, false);
    this.stateDir = root.createStateStorage("states");
    this.stateSerializer = new StateSerializer(stateDir, true);
    this.blockToStateDir = root.createHashStorage("blockStates", Hash::read);
    this.latestBlockTime = new BlockTimeStorage(root.createFile("latestBlockTime"), -1);
    if (latestBlockTime.getLatestBlockTime() == -1
        || blockTimeDir.isEmpty()
        || blockToStateDir.isEmpty()) {
      patchIndices();
    }
  }

  void patchIndices() {
    logger.info("Patching indices by reading all stored blocks");
    AtomicLong maximumBlockTime = new AtomicLong(-1);
    HashSet<Long> duplicates = new HashSet<>();
    blockTimeDir.clear();
    blockDir
        .readEvery()
        .forEach(
            finalized -> {
              Block block = finalized.getBlock();
              if (blockTimeDir.has(getHashForBlockTime(block.getBlockTime()))) {
                duplicates.add(block.getBlockTime());
              } else {
                updateBlockTimeIndex(block);
              }
              blockToStateDir.write(block.getParentBlock(), block.getState());
              maximumBlockTime.getAndUpdate(
                  currentMax -> Math.max(currentMax, block.getBlockTime()));
            });
    duplicates.stream()
        .sorted(Comparator.reverseOrder())
        .forEach(
            blockTime -> {
              Block nextBlock = getBlock(blockTime + 1);
              Hash blockTimeHash = getHashForBlockTime(blockTime);
              if (nextBlock == null) {
                blockTimeDir.remove(blockTimeHash);
              } else {
                blockTimeDir.write(blockTimeHash, nextBlock.getParentBlock());
              }
            });
    latestBlockTime.storeLatestBlockTime(maximumBlockTime.get());
  }

  /**
   * Get hash of block.
   *
   * @param blockTime block time to get hash for
   * @return hash of block for provided block time
   */
  public Hash getBlockHash(long blockTime) {
    return blockTimeDir.read(getHashForBlockTime(blockTime));
  }

  /**
   * Returns block from supplied hash.
   *
   * @param hash the block hash to look up
   * @return the block if found, null otherwise
   */
  public Block getBlock(Hash hash) {
    FinalBlock read = getFinalizedBlock(hash);
    if (read != null) {
      return read.getBlock();
    } else {
      return null;
    }
  }

  /**
   * Returns block from supplied block time.
   *
   * @param blockTime the block time to lookup
   * @return the block if found, null otherwise
   */
  public Block getBlock(long blockTime) {
    FinalBlock finalBlock = getFinalizedBlock(blockTime);
    if (finalBlock != null) {
      return finalBlock.getBlock();
    } else {
      return null;
    }
  }

  FinalBlock getFinalizedBlock(Hash hash) {
    return blockDir.read(hash);
  }

  /**
   * Returns finalized block from supplied block time.
   *
   * @param blockTime the block time to lookup
   * @return the finalized block if found, null otherwise
   */
  public FinalBlock getFinalizedBlock(long blockTime) {
    Hash hash = getBlockHash(blockTime);
    if (hash != null) {
      return getFinalizedBlock(hash);
    } else {
      return null;
    }
  }

  Hash getHashForBlockTime(long blockTime) {
    return Hash.create(stream -> stream.writeLong(blockTime));
  }

  ExecutedState loadState(Block block) {
    Hash stateHash = blockToStateDir.read(block.identifier());
    if (stateHash != null) {
      return getState(stateHash);
    } else {
      return null;
    }
  }

  /**
   * Get the state with the supplied identity hash.
   *
   * @param stateHash the identifier of the state
   * @return the loaded state or null if not found
   */
  public ExecutedState getState(Hash stateHash) {
    try {
      return stateSerializer.read(stateHash, ExecutedState.class);
    } catch (Exception e) {
      logger.info("Unable to read state", e);
      return null;
    }
  }

  /**
   * Registers a block as accepted - and final enough for storage.
   *
   * @param finalBlock the finalized block
   * @param state the state of the blockchain
   */
  public void registerAcceptedBlock(FinalBlock finalBlock, ImmutableChainState state) {
    Block block = finalBlock.getBlock();
    latestBlockTime.storeLatestBlockTime(block.getBlockTime());

    Hash stateHash =
        state.saveExecutedState(
            stateSerializable -> stateSerializer.write(stateSerializable).hash());
    blockToStateDir.write(block.identifier(), stateHash);
    updateBlockTimeIndex(block);

    // Save block last as this references the other parts
    blockDir.write(block.identifier(), finalBlock);
  }

  void updateBlockTimeIndex(Block block) {
    Hash hashForBlockTime = getHashForBlockTime(block.getBlockTime());
    blockTimeDir.write(hashForBlockTime, block.identifier());
  }

  /**
   * Finds the latest block and state pair that is stored in this storage.
   *
   * @return the loaded block and state or null if none is found
   */
  public BlockchainStorage.ValidBlock loadLatestValidBlock() {
    return loadLatestValidBlock(Long.MAX_VALUE);
  }

  /**
   * Finds the latest block and state pair that is stored in this storage.
   *
   * @param maximalBlockTime the maximal block time to find - allows caller to skip some blocks in
   *     storage
   * @return the loaded block and state or null if none is found
   */
  public BlockchainStorage.ValidBlock loadLatestValidBlock(long maximalBlockTime) {
    long latestBlockTime = Math.min(maximalBlockTime, this.latestBlockTime.getLatestBlockTime());
    for (long blockTime = latestBlockTime; blockTime >= 0; blockTime--) {
      FinalBlock block = getFinalizedBlock(blockTime);
      if (block != null) {
        Hash parentHash = block.getBlock().getParentBlock();
        boolean validParent;
        Block parentBlock;
        if (!Block.GENESIS_PARENT.equals(parentHash)) {
          parentBlock = getBlock(parentHash);
          validParent = parentBlock != null;
        } else {
          parentBlock = null;
          validParent = true;
        }
        ExecutedState state = loadState(block.getBlock());
        ExecutedState finalizedState = getState(block.getBlock().getState());
        if (state != null && finalizedState != null && validParent) {
          return new BlockchainStorage.ValidBlock(block, state, parentBlock, finalizedState);
        }
      }
    }
    return null;
  }

  @Override
  public void close() {
    latestBlockTime.close();
    stateDir.close();
  }

  /**
   * Get the raw state storage.
   *
   * @return raw state storage
   */
  public StateStorageRaw getStateStorage() {
    return stateDir;
  }
}
