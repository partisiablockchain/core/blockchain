package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.stream.Collectors;

final class LookupTask implements Callable<Void> {

  private final TreeSet<KademliaNode> seenNodes;
  private final TreeSet<KademliaNode> newNodes;
  private final HashSet<KademliaKey> askedNodes = new HashSet<>();
  private final List<ResolvableLookup> currentPending = new ArrayList<>();
  private final List<ResolvableLookup> previousPending = new ArrayList<>();

  private final LookupHandler handler;
  private final KademliaKey toLookup;
  private boolean finalRound;

  LookupTask(
      LookupHandler handler,
      KademliaKey toLookup,
      KademliaKey ownId,
      List<KademliaNode> nearestNodes) {
    this.handler = handler;
    this.toLookup = toLookup;
    askedNodes.add(ownId);
    seenNodes = new TreeSet<>(Kademlia.createComparator(toLookup));
    newNodes = new TreeSet<>(Kademlia.createComparator(toLookup));
    newNodes.addAll(nearestNodes);
  }

  KademliaKey getToLookup() {
    return toLookup;
  }

  @Override
  public Void call() throws Exception {
    int failCount = 0;
    failCount += consume(currentPending);
    consume(previousPending);
    if (!finalRound) {
      boolean nextRound = Kademlia.containsCloser(newNodes, seenNodes);
      if (nextRound) {
        seenNodes.addAll(newNodes);
        newNodes.clear();
        previousPending.addAll(currentPending);
        currentPending.clear();
        sendRequests(Kademlia.MAX_OUTSTANDING);
      } else {
        if (failCount > 0) {
          sendRequests(failCount);
        } else {
          boolean isDone = currentPending.isEmpty() && previousPending.isEmpty();
          if (isDone) {
            sendRequests(Kademlia.BUCKET_SIZE);
            this.finalRound = true;
          }
        }
      }
    }
    return null;
  }

  private int consume(List<ResolvableLookup> lookups) {
    int failCount = 0;
    for (Iterator<ResolvableLookup> iterator = lookups.iterator(); iterator.hasNext(); ) {
      ResolvableLookup lookup = iterator.next();
      if (lookup.isDone()) {
        newNodes.addAll(lookup.nodes);
        iterator.remove();
      } else if (lookup.isFailed()) {
        failCount++;
        iterator.remove();
      }
    }
    return failCount;
  }

  private void sendRequests(int maxOutstanding) throws IOException {
    List<KademliaNode> toAsk =
        seenNodes.stream()
            .sorted(Kademlia.createComparator(toLookup))
            .limit(Kademlia.BUCKET_SIZE)
            .filter(n -> !askedNodes.contains(n.getKey()))
            .limit(maxOutstanding)
            .collect(Collectors.toList());
    for (KademliaNode node : toAsk) {
      askedNodes.add(node.getKey());
      ResolvableLookup e = new ResolvableLookup(this);
      currentPending.add(e);
      handler.sendLookupMessage(e, node, toLookup);
    }
  }

  int newNodesSize() {
    return newNodes.size();
  }

  int currentPendingSize() {
    return currentPending.size();
  }

  static final class ResolvableLookup {

    private final LookupTask lookupTask;
    private List<KademliaNode> nodes;
    private boolean failed;

    ResolvableLookup(LookupTask lookupTask) {
      this.lookupTask = lookupTask;
    }

    void resolve(Consumer<LookupTask> taskConsumer, List<KademliaNode> nodes) {
      taskConsumer.accept(this.lookupTask);
      this.nodes = nodes;
    }

    void fail(Consumer<LookupTask> taskConsumer) {
      taskConsumer.accept(this.lookupTask);
      this.failed = true;
    }

    private boolean isDone() {
      return nodes != null;
    }

    private boolean isFailed() {
      return failed;
    }

    List<KademliaNode> getNodes() {
      return nodes;
    }
  }

  @FunctionalInterface
  interface LookupHandler {

    void sendLookupMessage(ResolvableLookup sender, KademliaNode node, KademliaKey toLookup)
        throws IOException;
  }
}
