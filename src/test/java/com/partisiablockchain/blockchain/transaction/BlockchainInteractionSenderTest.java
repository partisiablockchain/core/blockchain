package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerTransaction;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.EventManagerImpl;
import com.partisiablockchain.contract.InteractionBuilder;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class BlockchainInteractionSenderTest {

  private int eventIndex = 0;
  private final FeeCollector feeCollector = Mockito.mock(FeeCollector.class);
  private final EventCollector events =
      new EventCollector(
          null,
          StateHelper.initialWithAdditions(FunctionUtility.noOpConsumer()),
          1L,
          1L,
          TestObjects.EMPTY_HASH,
          0,
          null,
          feeCollector);
  private final ContractEventGroup contractEventGroup = new ContractEventGroup();
  private final EventManager contractEventManager = new EventManagerImpl(contractEventGroup);

  @Test
  public void sendInteractWithContract() {
    testInteractWithContract(true, TestObjects.CONTRACT_PUB1, new byte[] {1, 2, 3});
    testInteractWithContract(false, TestObjects.CONTRACT_SYS, new byte[] {-1, 55, 99});
    testInteractWithContract(true, TestObjects.CONTRACT_PUB1, null);
    testInteractWithContract(false, TestObjects.CONTRACT_SYS, null);
  }

  private void testInteractWithContract(
      boolean fromContract, BlockchainAddress contractId, byte[] rpc) {
    InteractionBuilder invoke = contractEventManager.invoke(contractId);

    if (rpc != null) {
      invoke = invoke.withPayload(safeDataOutputStream -> safeDataOutputStream.write(rpc));
    } else {
      invoke = invoke.withPayload(EventCreator.EMPTY_RPC);
    }

    long coverNetworkFee = 30 * 2;
    invoke = invoke.allocateCost(coverNetworkFee);

    dispatcher(fromContract, invoke);
    int index = eventIndex++;
    ExecutableEvent eventTransaction = events.getTransactions().get(index);
    Assertions.assertThat(eventTransaction.getEvent().getNonce()).isEqualTo(index + 1);
    Assertions.assertThat(eventTransaction.getEvent().getDestinationShard()).isNull();
    verifyInteractWithContract(
        eventTransaction.getEvent(), expectedSender(fromContract), contractId, rpc);
  }

  private void verifyInteractWithContract(
      EventTransaction eventTransaction,
      BlockchainAddress expectedSender,
      BlockchainAddress contractId,
      byte[] rpc) {
    InteractWithContractTransaction interactWithContract =
        verifyTransactionType(
            InteractWithContractTransaction.class, eventTransaction, expectedSender);
    Assertions.assertThat(interactWithContract.getAddress()).isEqualTo(contractId);
    if (rpc != null) {
      Assertions.assertThat(interactWithContract.getRpc()).isEqualTo(rpc);
    } else {
      Assertions.assertThat(interactWithContract.getRpc()).isEmpty();
    }
  }

  private void dispatcher(boolean fromContract, InteractionBuilder builder) {
    if (fromContract) {
      builder.sendFromContract();
    } else {
      builder.send();
    }
    ContractEventInteraction contractEvent =
        (ContractEventInteraction)
            contractEventGroup.contractEvents.get(contractEventGroup.contractEvents.size() - 1);
    events.interaction(
        contractEvent.contract,
        SafeDataOutputStream.serialize(contractEvent.rpc),
        contractEvent.originalSender,
        contractEvent.allocatedCost,
        contractEvent.costFromContract,
        new EventSender(TestObjects.ACCOUNT_TWO, TestObjects.CONTRACT_SYS));
  }

  private BlockchainAddress expectedSender(boolean fromContract) {
    BlockchainAddress expectedFrom;
    if (fromContract) {
      expectedFrom = TestObjects.CONTRACT_SYS;
    } else {
      expectedFrom = TestObjects.ACCOUNT_TWO;
    }
    return expectedFrom;
  }

  private <T extends Transaction> T verifyTransactionType(
      Class<T> expectedTransaction,
      EventTransaction eventTransaction,
      BlockchainAddress expectedSender) {
    Assertions.assertThat(eventTransaction.identifier()).isNotNull();
    InnerTransaction innerTransaction = (InnerTransaction) eventTransaction.getInner();
    Assertions.assertThat(innerTransaction.getSender()).isEqualTo(expectedSender);
    Transaction inner = innerTransaction.getTransaction();
    Assertions.assertThat(inner).isInstanceOf(expectedTransaction);
    return expectedTransaction.cast(inner);
  }
}
