package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.thread.ExecutionFactory;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.TestExecutionQueue;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.function.BiConsumer;
import org.bouncycastle.tls.TlsClient;
import org.bouncycastle.tls.TlsClientProtocol;
import org.bouncycastle.tls.TlsServer;
import org.bouncycastle.tls.TlsServerProtocol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ConnectionTest {

  private Packet<?> transactionPacket;
  private MyInputStream inputStream;
  private MyOutputStream outputStream;
  private int closeCalled = 0;
  private Connection connection;

  private Packet<?> received;
  private TestExecutionQueue senderThread;
  private TestExecutionQueue receiverThread;

  /** Setup. */
  @BeforeEach
  public void setup() {
    transactionPacket = new Packet<>(Packet.Type.TRANSACTION, createTransaction());
    inputStream = new MyInputStream(packetBytes(transactionPacket));
    outputStream = new MyOutputStream();
    ExecutionFactoryFake executionFactory = new ExecutionFactoryFake();
    connection =
        new Connection(
            (c, packet) -> received = packet,
            () -> closeCalled++,
            inputStream,
            outputStream,
            UUID.randomUUID().toString(),
            null,
            executionFactory);
    senderThread = executionFactory.getExecutionQueue(Connection.SENDER_THREAD_NAME);
    receiverThread = executionFactory.getExecutionQueue(Connection.RECEIVER_THREAD_NAME);
  }

  @Test
  public void gracefulClose() {
    connection.send(transactionPacket);

    assertThat(received).isNull();
    senderThread.runStep();
    assertThat(received).isNull();
    receiverThread.runStep();

    assertThat(connection.isAlive()).isTrue();
    assertThat(received).isNotNull();

    assertThat(senderThread.isExecutorShutDown()).isFalse();
    assertThat(receiverThread.isExecutorShutDown()).isFalse();
    connection.close();
    assertThat(connection.isAlive()).isFalse();

    assertThat(senderThread.isExecutorShutDown()).isTrue();
    assertThat(receiverThread.isExecutorShutDown()).isTrue();

    Object payload = received.getPayload();
    assertThat(payload).isInstanceOf(SignedTransaction.class);
    SignedTransaction transaction = (SignedTransaction) payload;
    assertThat(transaction.getCore().getNonce()).isEqualTo(0);

    assertThat(closeCalled).isEqualTo(1);
  }

  @Test
  public void exceedQueueLimit() {
    for (int i = 0; i < Connection.MAX_OUTBOUND_QUEUE_SIZE; i++) {
      connection.send(transactionPacket);
    }
    assertThat(connection.isAlive()).isTrue();
    connection.send(transactionPacket);
    assertThat(connection.isAlive()).isFalse();
  }

  @Test
  public void closeIsOnlyCalledOnceByConnection() {
    for (int i = 0; i < Connection.MAX_OUTBOUND_QUEUE_SIZE; i++) {
      connection.send(transactionPacket);
    }
    assertThat(closeCalled).isEqualTo(0);
    connection.send(transactionPacket);
    assertThat(closeCalled).isEqualTo(1);
    connection.send(transactionPacket);
    connection.send(transactionPacket);
    connection.send(transactionPacket);
    assertThat(closeCalled).isEqualTo(1);
  }

  @Test
  public void serverConnectionCloseIfHandshakeFails() {
    TlsServerProtocol tlsProtocol = new TlsServerProtocol(inputStream, outputStream);

    TlsServer tlsServer = Mockito.mock(TlsServer.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    () -> {},
                    tlsProtocol,
                    tlsServer,
                    TlsServerProtocol::accept,
                    null))
        .isInstanceOf(NullPointerException.class);

    assertThat(inputStream.closeCalled).isTrue();
    assertThat(outputStream.closeCalled).isTrue();
    assertThat(tlsProtocol.isClosed()).isTrue();
  }

  @Test
  public void closeOnWrongChainId() throws IOException {
    inputStream = new MyInputStream(new byte[4]);

    TlsClientProtocol tlsClientProtocol = Mockito.mock(TlsClientProtocol.class);
    Mockito.when(tlsClientProtocol.getInputStream()).thenReturn(inputStream);
    Mockito.when(tlsClientProtocol.getOutputStream()).thenReturn(outputStream);
    TlsClient tlsClient = Mockito.mock(TlsClient.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    () -> {},
                    tlsClientProtocol,
                    tlsClient,
                    (protocol, peer) -> {},
                    null))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Received invalid chainId. expected='chainId', received=''");

    Mockito.verify(tlsClientProtocol).close();
  }

  @Test
  public void clientConnectionCloseIfHandshakeFail() {
    TlsClientProtocol tlsProtocol = new TlsClientProtocol(inputStream, outputStream);

    TlsClient tlsClient = Mockito.mock(TlsClient.class);

    assertThatThrownBy(
            () ->
                Connection.createConnection(
                    (c, packet) -> received = packet,
                    "chainId",
                    () -> {},
                    tlsProtocol,
                    tlsClient,
                    TlsClientProtocol::connect,
                    null))
        .isInstanceOf(NullPointerException.class);

    assertThat(inputStream.closeCalled).isTrue();
    assertThat(outputStream.closeCalled).isTrue();
    assertThat(tlsProtocol.isClosed()).isTrue();
  }

  /**
   * Serializes the given packets into bytes.
   *
   * @param packets packets to serialize
   * @return the packets bytes
   */
  public static byte[] packetBytes(Packet<?>... packets) {
    return SafeDataOutputStream.serialize(
        stream -> {
          for (Packet<?> packet : packets) {
            packet.send(stream);
          }
        });
  }

  private SignedTransaction createTransaction() {
    return SignedTransaction.create(
            CoreTransactionPart.create(0, 0, 10),
            InteractWithContractTransaction.create(
                BlockchainAddress.fromString("000000000000000000000000000000000000000001"),
                new byte[] {1, 2, 3, 4}))
        .sign(new KeyPair(), "ConnectionTest");
  }

  /**
   * Helper constructor for creation of a connection.
   *
   * @param consumer network consumer
   * @param socket socket
   * @param input input stream
   * @param output output stream
   * @param chainId chain id of the connection
   * @param connectionPublicKey network key of the counter part in the connection
   * @param executionFactory the execution factory
   */
  public static void createTestConnection(
      final BiConsumer<Connection, Packet<?>> consumer,
      Closeable socket,
      InputStream input,
      OutputStream output,
      String chainId,
      BlockchainPublicKey connectionPublicKey,
      ExecutionFactory executionFactory) {
    new Connection(consumer, socket, input, output, chainId, connectionPublicKey, executionFactory);
  }

  /** Test input stream. Reads the first element and then stops reading. */
  @SuppressWarnings("InputStreamSlowMultibyteRead")
  public static final class MyInputStream extends InputStream {

    private int index = 0;
    private final byte[] bytes;
    private boolean closeCalled;
    private boolean didInterrupt;

    /**
     * Construct a new input stream.
     *
     * @param bytes the bytes that should be available
     */
    public MyInputStream(byte[] bytes) {
      this.bytes = bytes;
    }

    @Override
    public synchronized int read() {
      if (index < bytes.length) {
        byte value = bytes[index];
        index++;
        return Byte.toUnsignedInt(value);
      }
      this.didInterrupt = didInterrupt || Thread.currentThread().isInterrupted();
      return -1;
    }

    @Override
    public void close() {
      closeCalled = true;
    }
  }

  /** Test output stream. */
  public static final class MyOutputStream extends OutputStream {

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    private boolean closeCalled;

    @Override
    public void write(int b) {
      output.write(b);
    }

    @Override
    public void close() {
      closeCalled = true;
    }
  }
}
