package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.storage.RootDirectory;
import java.util.function.BooleanSupplier;

/**
 * Any module added when booting the blockchain. Initialized in server.
 *
 * @param <T> type of configuration
 */
public interface ServerModule<T extends ServerModuleConfigDto> {

  /**
   * Register this server module with the supplied blockchain.
   *
   * @param root storage directory to use for file persistence
   * @param blockchain the current blockchain
   * @param rest config object to register rest, listeners and closeables
   * @param config the user supplied config
   */
  void register(RootDirectory root, BlockchainLedger blockchain, ServerConfig rest, T config);

  /**
   * Get type of config.
   *
   * @return type of config
   */
  Class<T> configType();

  /** The interface for configuration of the server. */
  interface ServerConfig {

    /**
     * Register closable to be closed with server.
     *
     * @param closeable to register
     */
    void registerCloseable(AutoCloseable closeable);

    /**
     * Register restful component from class.
     *
     * @param componentClass class to register
     */
    void registerRestComponent(Class<?> componentClass);

    /**
     * Register restful component from instance.
     *
     * @param component instance to register
     */
    void registerRestComponent(Object component);

    /**
     * Add restful alive check.
     *
     * @param isAlive to add
     */
    void addRestAliveCheck(BooleanSupplier isAlive);

    /**
     * Add listener for contract creation.
     *
     * @param listener to add
     */
    void addCreateListener(CreateContractListener listener);
  }

  /**
   * Listener for contract creation events, triggered when a new contract has been deployed. Useful
   * for tracking contracts and listen to changes.
   */
  interface CreateContractListener {

    /**
     * Triggered when a contract has been created. Gets the address, core state and contract state
     * after the first create call to the contract, returns potentially a new listener for the rest
     * of the event on this contract.
     *
     * @param address the address of the new contract
     * @param coreState the core state of the contract
     * @param contractState contract state after the first create call to the contract
     * @return listener for the succeeding events for this contract, null for no listener
     */
    ContractListener createContract(
        BlockchainAddress address, CoreContractState coreState, StateSerializable contractState);
  }

  /** Listener for contract events, triggered when a contract has changed state. */
  interface ContractListener {

    /**
     * Triggered when a contract has updated state. Gets the core state and contract state after the
     * calls to the contract.
     *
     * @param coreState the core state of the contract
     * @param contractState contract state after the calls to the contract
     */
    void update(CoreContractState coreState, StateSerializable contractState);

    /** Triggered when a contract has been removed. */
    void remove();
  }
}
