package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.PublicContractBinder;
import com.partisiablockchain.binder.SysContractBinder;
import com.partisiablockchain.blockchain.contract.binder.AbstractPublicContractBinder;
import com.partisiablockchain.blockchain.contract.binder.AbstractZkContractBinder;
import com.partisiablockchain.blockchain.contract.binder.ZkComplexTestBinder;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;

/** Example contracts for internal testing of the blockchain. */
public final class TestContracts {

  /** {@link VoidContract} JAR. */
  public static final byte[] CONTRACT_VOID = JarBuilder.buildJar(VoidContract.class);

  /** {@link LongContract} JAR. */
  public static final byte[] CONTRACT_LONG = JarBuilder.buildJar(LongContract.class);

  /** {@link PublicContractBinder} JAR with all dependencies. */
  public static final byte[] CONTRACT_BINDER_PUBLIC =
      JarBuilder.buildJar(PublicContractBinder.class, AbstractPublicContractBinder.class);

  /** {@link SysContractBinder} JAR with all dependencies. */
  public static final byte[] CONTRACT_BINDER_SYSTEM = JarBuilder.buildJar(SysContractBinder.class);

  /** {@link ZkComplexTestBinder} JAR with all dependencies. */
  public static final byte[] CONTRACT_BINDER_ZK =
      JarBuilder.buildJar(ZkComplexTestBinder.class, AbstractZkContractBinder.class);

  /** Empty contract with void state. */
  public static final class VoidContract extends PubContract<StateVoid> {}

  /** Empty contract with long state. */
  public static final class LongContract extends PubContract<StateLong> {}
}
