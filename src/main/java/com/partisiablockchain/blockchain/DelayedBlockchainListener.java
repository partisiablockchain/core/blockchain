package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A decorator for a blockchain listener that wait to see a final block that was produced after it
 * was started before the decorated listener will be invoked.
 *
 * <p>Final state is taken special care of, the first time it is triggered the update event is
 * reconstructed to match an actual event.
 */
public final class DelayedBlockchainListener implements Listener {

  private static final Logger logger = LoggerFactory.getLogger(DelayedBlockchainListener.class);
  private final Listener decorated;
  private final long startTime;
  private boolean initialized = false;
  private boolean nextUpdateEventSpecial = false;

  /**
   * Create a waiting decorator.
   *
   * @param decorated to be decorated
   */
  public DelayedBlockchainListener(Listener decorated) {
    this(decorated, System.currentTimeMillis());
  }

  DelayedBlockchainListener(Listener decorated, long startTime) {
    this.decorated = decorated;
    this.startTime = startTime;
  }

  @Override
  public void newFinalBlock(Block block, ImmutableChainState state) {
    if (!initialized && block.getProductionTime() >= startTime) {
      initialized = true;
      nextUpdateEventSpecial = true;
    }
    if (initialized) {
      decorated.newFinalBlock(block, state);
    }
  }

  @Override
  public void newBlockProposal(Block block) {
    if (initialized) {
      decorated.newBlockProposal(block);
    }
  }

  @Override
  public void newPendingTransaction(SignedTransaction transaction) {
    if (initialized) {
      decorated.newPendingTransaction(transaction);
    }
  }

  @Override
  public void newFinalState(ImmutableChainState state, UpdateEvent contracts) {
    if (initialized) {
      if (nextUpdateEventSpecial) {
        logger.info(
            "Invoked listener for the first time on state {} contracts {} - ignoring event",
            state,
            state.getContracts());
        decorated.newFinalState(state, new UpdateEvent(state.getContracts(), List.of(), List.of()));
        nextUpdateEventSpecial = false;
      } else {
        decorated.newFinalState(state, contracts);
      }
    }
  }

  @Override
  public String toString() {
    return "DelayedBlockchainListener{" + "decorated=" + decorated + '}';
  }
}
