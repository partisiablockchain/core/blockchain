package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StorageTest extends CloseableTest {

  @Test
  public void removeFromImmutable() {
    MemoryStorage storage = new MemoryStorage(new HashMap<>(), "test", true);
    Assertions.assertThatThrownBy(() -> storage.remove(TestObjects.EMPTY_HASH))
        .isInstanceOf(IllegalStateException.class);
  }

  @Test
  public void shouldNotOverwriteIfAlreadyPresent() {
    MemoryStorage storage = new MemoryStorage(new HashMap<>(), "test", true);
    storage.write(TestObjects.EMPTY_HASH, stream -> stream.writeInt(0));
    storage.write(TestObjects.EMPTY_HASH, stream -> stream.writeInt(1));
    Integer value = storage.read(TestObjects.EMPTY_HASH, SafeDataInputStream::readInt);
    Assertions.assertThat(value).isEqualTo(0);
  }

  @Test
  public void writeTwice() {
    MemoryStorage storage = new MemoryStorage(new HashMap<>(), "test", true);
    String substring = TestObjects.EMPTY_HASH.toString().substring(0, 63);
    Hash firstHash = Hash.fromString(substring + "a");
    Hash secondHash = Hash.fromString(substring + "b");
    storage.write(firstHash, FunctionUtility.noOpConsumer());
    storage.write(firstHash, s -> s.writeByte(0));
    storage.write(secondHash, FunctionUtility.noOpConsumer());

    Assertions.assertThat(storage.has(firstHash)).isTrue();
    Assertions.assertThat(storage.readRaw(firstHash)).hasSize(0);
    Assertions.assertThat(storage.has(secondHash)).isTrue();
  }

  @Test
  @SuppressWarnings("FutureReturnValueIgnored")
  public void concurrentWritesToSameFile() throws InterruptedException {
    MemoryStorage storage = new MemoryStorage(new ConcurrentHashMap<>(), "test", true);
    int concurrency = 16;
    ExecutorService executorService = Executors.newFixedThreadPool(concurrency);
    register(executorService::shutdownNow);
    List<Hash> hashes = createHashesForSameTargetFile(concurrency);
    CountDownLatch countDownLatch = new CountDownLatch(concurrency);
    for (Hash hash : hashes) {
      executorService.submit(
          () ->
              ExceptionConverter.run(
                  () -> {
                    countDownLatch.countDown();
                    countDownLatch.await();
                    storage.write(hash, FunctionUtility.noOpConsumer());
                  },
                  "Unable to write to files"));
    }
    executorService.shutdown();
    Assertions.assertThat(executorService.awaitTermination(1, TimeUnit.SECONDS))
        .describedAs("All taskes must finish")
        .isTrue();
    for (Hash hash : hashes) {
      Assertions.assertThat(storage.has(hash)).isTrue();
    }
  }

  private List<Hash> createHashesForSameTargetFile(int count) {
    Assertions.assertThat(count).isLessThanOrEqualTo(16);
    String baseHash = TestObjects.EMPTY_HASH.toString().substring(0, 63);
    return IntStream.range(0, count)
        .mapToObj(c -> baseHash + Integer.toHexString(c))
        .map(Hash::fromString)
        .collect(Collectors.toList());
  }
}
