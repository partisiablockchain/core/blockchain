package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderContract;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.blockchain.contract.BlockchainResult;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Generic interface for contracts in the chain state.
 *
 * @param <StateT> the type of contract state.
 */
public interface BlockchainContract<StateT extends StateSerializable, EventT extends BinderEvent> {

  /**
   * Create contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param rpc payload for invocation
   * @return initial contract state
   */
  BlockchainResult<StateT, EventT> create(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      byte[] rpc);

  /**
   * Initiate invocation against contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param contractState contract state before invocation
   * @param rpc payload for invocation
   * @return next contract state
   */
  BlockchainResult<StateT, EventT> invoke(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      StateT contractState,
      byte[] rpc);

  /**
   * Initiate callback for contract.
   *
   * @param executionContext context for executing a transaction
   * @param contractContext context for execution in a contract
   * @param contractState contract state before callback
   * @param callbackContext context for callback
   * @param rpc payload for callback
   * @return next contract state
   */
  BlockchainResult<StateT, EventT> callback(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      StateT contractState,
      CallbackContext callbackContext,
      byte[] rpc);

  /**
   * Get serializer for the internal state of a contract.
   *
   * @return serializer for the internal state of a contract.
   */
  ContractSerialization<StateT> getContractSerialization();

  /**
   * Get binder contract.
   *
   * @return binder contract
   */
  BinderContract<?, ?, ?> getBinder();

  /**
   * Checks whether a contract is upgradable. Must be called before the upgradeState method.
   * Expected to throw an exception if the upgrade cannot be performed.
   *
   * @param contractContext the contract context
   * @param executionContext the execution context
   * @param contractState the contract state
   * @param oldContractBytecodeIds the bytecode identifiers of the old contract
   * @param newContractBytecodeIds the bytecode identifiers of the new contract
   * @param rpc the contract upgrade rpc
   * @return permit with contract data if upgrade is allowed
   */
  ContractUpgradePermit acquireUpgradePermit(
      BlockchainContractContext contractContext,
      ExecutionContextTransaction executionContext,
      StateT contractState,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc);

  /**
   * Upgrades the state of a contract if upgrade is allowed. Must only be called after a contract
   * upgrade permit has been granted. Expected to throw an exception if the state migration cannot
   * be performed.
   *
   * @param contractContext the contract context
   * @param executionContext the execution context
   * @param oldContractState the old contract state
   * @param permit the permit with additional data issued by the old contract
   * @param rpc the contract upgrade rpc
   * @return new contract state
   */
  StateT upgradeState(
      BlockchainContractContext contractContext,
      ExecutionContextTransaction executionContext,
      StateAccessor oldContractState,
      ContractUpgradePermit permit,
      byte[] rpc);
}
