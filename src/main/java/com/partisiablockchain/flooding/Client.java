package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutionFactory;
import java.io.Closeable;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class Client implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Client.class);
  static final int MINIMUM_OUTBOUND_CONNECTIONS = 3;
  static final int MINIMUM_CONNECTIONS = 5;
  static final String CONNECTOR_THREAD_NAME = "Connector";

  private final ScheduledExecutorService scheduler;
  private final Supplier<Address> kademlia;
  private final Address persistentConnection;
  private final Supplier<List<Neighbour>> connectedNeighbours;
  private final BiConsumer<Address, Socket> connectedCallback;
  private final Address myself;
  private boolean closed;

  /** Creates a client for a network node. */
  Client(
      Supplier<Address> kademlia,
      Address persistentConnection,
      Address myself,
      Supplier<List<Neighbour>> connectedNeighbours,
      BiConsumer<Address, Socket> connectedCallback,
      ExecutionFactory executionFactory) {
    this.scheduler = executionFactory.newScheduledThreadPool(CONNECTOR_THREAD_NAME, 1);
    this.kademlia = kademlia;
    this.persistentConnection = persistentConnection;
    this.connectedNeighbours = connectedNeighbours;
    this.connectedCallback = connectedCallback;
    this.myself = myself;
    initialize();
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void initialize() {
    scheduler.scheduleWithFixedDelay(
        () ->
            ExceptionLogger.handle(
                logger::debug, this::checkForConnect, "Error while checking connections"),
        0,
        randomInterval(1000, 1500),
        TimeUnit.MILLISECONDS);
  }

  void checkForConnect() {
    List<Neighbour> connectedNeighbours = this.connectedNeighbours.get();
    final long outboundNeighbours =
        connectedNeighbours.stream().filter(Neighbour::isOutbound).count();
    List<Address> connectedAccounts =
        connectedNeighbours.stream().map(Neighbour::getAddress).collect(Collectors.toList());
    connectIfUnconnected(connectedAccounts, persistentConnection);

    long outboundsNeeded = MINIMUM_OUTBOUND_CONNECTIONS - outboundNeighbours;
    int neededCount = MINIMUM_CONNECTIONS - connectedNeighbours.size();
    for (int i = 0; i < outboundsNeeded || i < neededCount; i++) {
      initializeConnection(connectedAccounts);
    }
  }

  private void initializeConnection(List<Address> connectedAccounts) {
    int picks = 0;
    boolean done = false;
    while (!done && picks < 20) {
      Address address = kademlia.get();
      done = connectIfUnconnected(connectedAccounts, address);
      picks++;
    }
  }

  boolean connectIfUnconnected(List<Address> connectedAddresses, Address toConnect) {
    if (toConnect == null || connectedAddresses.contains(toConnect) || toConnect.equals(myself)) {
      return false;
    }
    connectedAddresses.add(toConnect);
    connect(toConnect);
    return true;
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void connect(Address toConnect) {
    logger.info("Trying to connect to {}", toConnect);
    scheduler.schedule(
        () -> allocateSocket(toConnect), randomInterval(0, 100), TimeUnit.MILLISECONDS);
  }

  @SuppressWarnings("AddressSelection")
  private void allocateSocket(Address toConnect) {
    Socket socket =
        ExceptionConverter.call(
            // Intentional suppressed, TLS connection is established by Connection class.
            // It just doesn't use Java SSL sockets.
            // nosemgrep
            () -> new Socket(toConnect.hostname(), toConnect.port()));
    connectedCallback.accept(toConnect, socket);
  }

  @Override
  public void close() {
    closed = true;
    scheduler.shutdownNow();
  }

  boolean isClosed() {
    return closed;
  }

  static long randomInterval(long min, long max) {
    return (long) (min + Math.random() * (max - min));
  }
}
