package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ContractStateTest {

  private final List<EventCollector> eventCollectors = new ArrayList<>();

  /** Create contract state for tests. */
  public static ContractState create(byte[] handler) {
    return ContractState.create(
        CoreContractStateTest.create(
            TestObjects.EMPTY_HASH, TestObjects.EMPTY_HASH, handler.length));
  }

  @Test
  public void computeStorageLength() {
    ContractState contractState = create(new byte[64]);
    contractState = contractState.withHashAndSize(TestObjects.EMPTY_HASH, 67);
    Assertions.assertThat(contractState.computeStorageLength()).isEqualTo(64 + 67);
  }

  @Test
  public void callbackInfoSerialization() {
    ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(List.of(TestObjects.EMPTY_HASH), 0L, null);
    ContractState.InvokeWithCallback test =
        new ContractState.InvokeWithCallback(
            new ReturnEnvelope(TestObjects.CONTRACT_BOOTSTRAP),
            TestObjects.ACCOUNT_ONE,
            TestObjects.EMPTY_HASH,
            AvlTree.create(Map.of(callbackInfo.getCallbackIdentifier(), callbackInfo)));
    StateSerializer stateSerializer =
        new StateSerializer(ObjectCreator.createMemoryStateStorage(), true);
    SerializationResult write = stateSerializer.write(test);
    ContractState.InvokeWithCallback read =
        stateSerializer.read(write.hash(), ContractState.InvokeWithCallback.class);

    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(read.findCallbackInfo(TestObjects.EMPTY_HASH).getCallbackIdentifier())
        .isNotNull()
        .isEqualTo(test.findCallbackInfo(TestObjects.EMPTY_HASH).getCallbackIdentifier());
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  @Test
  public void withCallback() {
    ContractState contractState = create(new byte[0]);

    FeeCollector feeCollector = Mockito.mock(FeeCollector.class);

    long coverNetworkFee = 27 * 2;
    EventCollector collector = createCollector(feeCollector, FunctionUtility.noOpConsumer());
    collector.createEventTransaction(
        TestObjects.ACCOUNT_ONE,
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
        coverNetworkFee,
        false);
    Hash eventTransaction = getEvent(0);

    ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(
            List.of(eventTransaction), 0L, new LargeByteArray(new byte[] {0, 0, 0, 123}));
    ContractState updatedWithCallback =
        contractState.withCallbacks(
            new ReturnEnvelope(TestObjects.CONTRACT_PUB2),
            TestObjects.ACCOUNT_ONE,
            TestObjects.EMPTY_HASH,
            callbackInfo);

    Assertions.assertThat(updatedWithCallback).isNotEqualTo(contractState);
    Assertions.assertThat(updatedWithCallback.findSendingTransactionForEvent(eventTransaction))
        .isNotNull();
    Assertions.assertThatThrownBy(
            () -> updatedWithCallback.findSendingTransactionForEvent(TestObjects.EMPTY_HASH))
        .isInstanceOf(IllegalStateException.class);

    Hash callbackIdentifier = ContractState.CallbackInfo.hash(List.of(eventTransaction));
    Assertions.assertThat(updatedWithCallback.findSendingTransactionForCallback(callbackIdentifier))
        .isNotNull();
    Assertions.assertThatThrownBy(
            () -> updatedWithCallback.findSendingTransactionForCallback(TestObjects.EMPTY_HASH))
        .isInstanceOf(IllegalStateException.class);

    Assertions.assertThat(updatedWithCallback.findEventsForCallback(callbackIdentifier))
        .isNotNull();
    Assertions.assertThatThrownBy(
            () -> updatedWithCallback.findEventsForCallback(TestObjects.EMPTY_HASH))
        .isInstanceOf(IllegalStateException.class);
  }

  private Hash getEvent(int i) {
    return eventCollectors.stream()
        .flatMap(eventCollector -> eventCollector.getTransactions().stream())
        .skip(i)
        .limit(1)
        .findFirst()
        .orElseThrow()
        .identifier();
  }

  private EventCollector createCollector(
      FeeCollector feeCollector, Consumer<MutableChainState> state) {
    EventCollector eventCollector =
        new EventCollector(
            null,
            StateHelper.initialWithAdditions(state),
            1,
            1L,
            TestObjects.EMPTY_HASH,
            123,
            TestObjects.CONTRACT_PUB1,
            feeCollector);
    eventCollectors.add(eventCollector);
    return eventCollector;
  }

  private AvlTree<Hash, ContractState.CallbackInfo> infos(
      ContractState.CallbackInfo... callbackInfo) {
    AvlTree<Hash, ContractState.CallbackInfo> callbackInfoAvlTree = AvlTree.create();
    for (ContractState.CallbackInfo info : callbackInfo) {
      callbackInfoAvlTree = callbackInfoAvlTree.set(info.getCallbackIdentifier(), info);
    }
    return callbackInfoAvlTree;
  }

  @Test
  public void callbackEnd() {
    testCallbackEnd(true);
    testCallbackEnd(false);
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  private void testCallbackEnd(boolean success) {

    FeeCollector feeCollector = Mockito.mock(FeeCollector.class);

    EventCollector collector = createCollector(feeCollector, FunctionUtility.noOpConsumer());
    collector.setCallback(0, false);
    long coverNetworkFee = 27 * 2;
    collector.createEventTransaction(
        TestObjects.ACCOUNT_ONE,
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
        coverNetworkFee,
        false);
    final Hash eventTransaction1 = getEvent(0);
    collector.createEventTransaction(
        TestObjects.ACCOUNT_ONE,
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
        coverNetworkFee,
        false);
    Hash eventTransaction2 = getEvent(1);

    EventCollector collector2 = createCollector(feeCollector, FunctionUtility.noOpConsumer());
    collector2.setCallback(0, false);

    collector2.createEventTransaction(
        TestObjects.ACCOUNT_ONE,
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[10]),
        coverNetworkFee,
        false);
    Hash eventTransaction3 = getEvent(2);

    ReturnEnvelope returnEnvelope = new ReturnEnvelope(TestObjects.CONTRACT_PUB2);
    ContractState.CallbackInfo callbackInfo0 =
        ContractState.CallbackInfo.create(
            List.of(eventTransaction1, eventTransaction2),
            0L,
            new LargeByteArray(new byte[] {0, 0, 0, 123}));
    ContractState.CallbackInfo callbackInfo1 =
        ContractState.CallbackInfo.create(
            List.of(eventTransaction3), 0L, new LargeByteArray(new byte[] {0, 0, 0, 123}));

    ContractState contractState = create(new byte[0]);
    ContractState updatedWithCallback =
        contractState
            .withCallbacks(
                returnEnvelope, TestObjects.ACCOUNT_ONE, TestObjects.EMPTY_HASH, callbackInfo0)
            .callbackUpdated(callbackInfo0.getCallbackIdentifier(), callbackInfo1);
    Assertions.assertThat(updatedWithCallback).isNotEqualTo(contractState);
    Assertions.assertThat(updatedWithCallback.findSendingTransactionForEvent(eventTransaction1))
        .isNotNull();
    Assertions.assertThat(updatedWithCallback.findSendingTransactionForEvent(eventTransaction2))
        .isNotNull();

    Hash callbackIdentifier =
        ContractState.CallbackInfo.hash(List.of(eventTransaction1, eventTransaction2));
    Assertions.assertThat(updatedWithCallback.findSendingTransactionForCallback(callbackIdentifier))
        .isNotNull();
    FixedList<CallbackContext.ExecutionResult> eventsForCallback =
        updatedWithCallback.findEventsForCallback(callbackIdentifier);
    Assertions.assertThat(eventsForCallback).isNotNull().hasSize(2);

    AtomicReference<InnerSystemEvent.CallbackEvent> reference = new AtomicReference<>();
    ContractState withSingleCallbackResult =
        updatedWithCallback.callbackComplete(callbackIdentifier, reference::set, new byte[0]);
    Assertions.assertThat(reference.get()).isNull();
    Assertions.assertThat(withSingleCallbackResult).isEqualTo(updatedWithCallback);
    Assertions.assertThat(withSingleCallbackResult.findEventsForCallback(callbackIdentifier))
        .isNotNull()
        .hasSize(2);

    cannotSetCallbackResult(withSingleCallbackResult, callbackIdentifier, success);
    System.out.println("1: " + eventTransaction1);
    withSingleCallbackResult =
        withSingleCallbackResult.acknowledgeCallback(eventTransaction1, true, new byte[0])
            .contractState;
    cannotSetCallbackResult(withSingleCallbackResult, callbackIdentifier, success);

    System.out.println("2: " + eventTransaction2);
    withSingleCallbackResult =
        withSingleCallbackResult.acknowledgeCallback(eventTransaction2, true, new byte[0])
            .contractState;
    withSingleCallbackResult = withSingleCallbackResult.callbackResult(callbackIdentifier, success);

    Hash otherCallbackIdentifier = ContractState.CallbackInfo.hash(List.of(eventTransaction3));
    FixedList<CallbackContext.ExecutionResult> events =
        withSingleCallbackResult.findEventsForCallback(otherCallbackIdentifier);
    Assertions.assertThat(events).isNotNull().hasSize(1);
    Assertions.assertThat(events.get(0).isSucceeded()).isFalse();
    ContractState.CallbackResult callbackResult =
        withSingleCallbackResult.acknowledgeCallback(eventTransaction3, true, new byte[0]);
    Assertions.assertThat(callbackResult.callback).isNotNull();
    withSingleCallbackResult = callbackResult.contractState;
    withSingleCallbackResult =
        withSingleCallbackResult.callbackResult(otherCallbackIdentifier, success);

    ContractState withCompleteCallbackResult =
        withSingleCallbackResult.callbackComplete(
            otherCallbackIdentifier, reference::set, new byte[0]);
    InnerSystemEvent.CallbackEvent innerEvent = reference.get();
    Assertions.assertThat(reference.get()).isNotNull();
    Assertions.assertThat(innerEvent.getEventType()).isEqualTo(EventTransaction.EventType.SYSTEM);
    Assertions.assertThat(innerEvent.getSystemEventType())
        .isEqualTo(InnerSystemEvent.SystemEventType.CALLBACK);
    Assertions.assertThat(innerEvent.toString())
        .contains(returnEnvelope.toString())
        .contains("success=" + success);
    Assertions.assertThatThrownBy(
            () -> withCompleteCallbackResult.findEventsForCallback(callbackIdentifier))
        .isInstanceOf(IllegalStateException.class)
        .hasMessageContaining(callbackIdentifier.toString());
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  private void cannotSetCallbackResult(
      ContractState withSingleCallbackResult, Hash callbackIdentifier, boolean success) {
    Assertions.assertThatThrownBy(
            () -> withSingleCallbackResult.callbackResult(callbackIdentifier, success))
        .isInstanceOf(IllegalStateException.class);
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  @Test
  public void callbackInfo() {
    Hash event1 = Hash.create(stream -> stream.writeInt(123));
    Hash event2 = Hash.create(stream -> stream.writeInt(321));
    ContractState.InvokeWithCallback invokeWithCallback =
        new ContractState.InvokeWithCallback(
            null,
            TestObjects.ACCOUNT_ONE,
            TestObjects.EMPTY_HASH,
            infos(
                ContractState.CallbackInfo.create(
                    List.of(TestObjects.EMPTY_HASH), 0L, new LargeByteArray(new byte[0])),
                ContractState.CallbackInfo.create(
                    List.of(event1, event2), 0L, new LargeByteArray(new byte[0]))));
    ContractState.CallbackInfo callbackInfo =
        invokeWithCallback.findCallbackInfo(TestObjects.EMPTY_HASH);
    Assertions.assertThat(callbackInfo).isNotNull();
    Assertions.assertThat(callbackInfo.asResult(TestObjects.EMPTY_HASH).isSucceeded()).isFalse();

    byte[] testBytes = SafeDataOutputStream.serialize(stream -> stream.writeInt(123));
    testEventComplete(invokeWithCallback, TestObjects.EMPTY_HASH, testBytes);
    testEventComplete(invokeWithCallback, event1, testBytes);
    testEventComplete(invokeWithCallback, event2, testBytes);
    testEventComplete(invokeWithCallback, event2, testBytes);

    Assertions.assertThatThrownBy(
            () -> invokeWithCallback.findCallbackInfo(TestObjects.hashNumber(222)))
        .isInstanceOf(IllegalStateException.class);
  }

  private void testEventComplete(
      ContractState.InvokeWithCallback invokeWithCallback, Hash event, byte[] returnValue) {
    Assertions.assertThat(invokeWithCallback.findCallbackInfo(event)).isNotNull();

    Hash index = invokeWithCallback.findIndex(event);
    ContractState.InvokeWithCallback updatedInvokeWithCallback =
        invokeWithCallback.completeEvent(event, false, returnValue);
    Assertions.assertThat(updatedInvokeWithCallback.findIndex(event)).isEqualTo(index);
    CallbackContext.ExecutionResult executionResult =
        updatedInvokeWithCallback.findCallbackInfo(event).asResult(event);
    Assertions.assertThat(executionResult.isSucceeded()).isFalse();

    updatedInvokeWithCallback = invokeWithCallback.completeEvent(event, true, returnValue);
    Assertions.assertThat(updatedInvokeWithCallback.findIndex(event)).isEqualTo(index);
    executionResult = updatedInvokeWithCallback.findCallbackInfo(event).asResult(event);
    Assertions.assertThat(executionResult.isSucceeded()).isTrue();
    Assertions.assertThat(executionResult.returnValue().readInt())
        .isEqualTo(SafeDataInputStream.createFromBytes(returnValue).readInt());
  }

  @Test
  public void testFindMethod() {
    ContractState.CallbackInfo.CompletedTransaction completed =
        new ContractState.CallbackInfo.CompletedTransaction(
            0, ExecutionResult.Success, new LargeByteArray(new byte[0]));
    Hash hash = TestObjects.EMPTY_HASH;
    AvlTree<Hash, ContractState.CallbackInfo.CompletedTransaction> tree = AvlTree.create();
    AvlTree<Hash, ContractState.CallbackInfo.CompletedTransaction> finalTree =
        tree.set(hash, completed);
    Assertions.assertThat(ContractState.find(finalTree, 0).isSucceeded()).isTrue();
    Assertions.assertThat(ContractState.find(finalTree, 1)).isNull();
    Assertions.assertThat(ContractState.find(tree, 0)).isNull();
  }
}
