package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.kademlia.LookupTask.ResolvableLookup;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.thread.ExecutorFactory;
import com.secata.tools.thread.ThreadedLoop;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Kademlia layer implemented for pbc. */
public final class Kademlia implements AutoCloseable {

  static final int BUCKET_SIZE = 20;
  static final int MAX_OUTSTANDING = 3;
  private static final int BUFFER_SIZE = 1024;

  private final Map<MessageIdentifier, ResolvableLookup> awaitingLookups =
      new ConcurrentHashMap<>();
  private final Set<MessageIdentifier> outstandingPings = ConcurrentHashMap.newKeySet();

  private final ScheduledExecutorService service;
  private final ThreadedLoop inbox;

  private final KademliaKey id;
  private final List<KademliaBucket> buckets;
  private final DatagramSocket socket;

  /**
   * Construct and start a new Kademlia instance.
   *
   * @param id the id of the instance
   * @param port the port used to listen for incomming messages
   * @param knownNodes a list of bootstrap nodes
   */
  public Kademlia(KademliaKey id, int port, List<KademliaNode> knownNodes) {
    this(
        id,
        knownNodes,
        ExceptionConverter.call(() -> new DatagramSocket(port), "Unable to bind"),
        ExecutorFactory.newScheduledSingleThread("KademliaRefresh"));
  }

  Kademlia(
      KademliaKey id,
      List<KademliaNode> knownNodes,
      DatagramSocket socket,
      ScheduledExecutorService executor) {
    this.id = Objects.requireNonNull(id);
    this.socket = Objects.requireNonNull(socket);
    this.buckets = Stream.generate(KademliaBucket::new).limit(256).collect(Collectors.toList());
    this.service = executor;

    this.inbox = startInboxLoop(socket);
    for (KademliaNode knownNode : knownNodes) {
      store(knownNode);
    }
    lookup(this.id);
    scheduleRefresh();
  }

  private ThreadedLoop startInboxLoop(DatagramSocket socket) {
    byte[] data = new byte[BUFFER_SIZE];
    DatagramPacket packet = new DatagramPacket(data, BUFFER_SIZE);
    return ThreadedLoop.create(
            () -> {
              socket.receive(packet);
              incoming(packet);
            },
            "KademliaReceive")
        .start();
  }

  private void incoming(DatagramPacket packet) throws IOException {
    Message parsedMessage = new Message(packet.getData(), packet.getLength());
    KademliaNode sender =
        new KademliaNode(parsedMessage.senderId, packet.getAddress(), packet.getPort());
    handleIncoming(parsedMessage, sender);
  }

  void handleIncoming(Message parsedMessage, KademliaNode sender) throws IOException {
    MessageType type = parsedMessage.type;
    if (type == MessageType.LOOKUP) {
      handleLookup(parsedMessage, sender);
    } else if (type == MessageType.LOOKUP_REPLY) {
      handleLookupReply(parsedMessage, sender);
    } else /*if (type == MessageType.PING_REPLY)*/ {
      handlePing(parsedMessage, sender);
    }
  }

  private void handleLookup(Message parsedMessage, KademliaNode sender) throws IOException {
    byte[] incommingId = parsedMessage.messageId;
    KademliaKey requestedId = new KademliaKey(parsedMessage.payload);

    List<KademliaNode> nodes = getNearestNodes(requestedId);
    sendLookupReply(sender, incommingId, nodes);
  }

  private void sendLookupReply(KademliaNode receiver, byte[] incommingId, List<KademliaNode> nodes)
      throws IOException {
    Message message = createLookupReply(this.id, incommingId, nodes);
    MessageIdentifier e = new MessageIdentifier(message.messageId, receiver);
    outstandingPings.add(e);
    send(receiver, message);
    handleTimeout(e);
  }

  private void send(KademliaNode receiver, Message message) throws IOException {
    socket.send(message.toPacket(receiver));
  }

  static Message createLookupReply(KademliaKey id, byte[] incommingId, List<KademliaNode> nodes)
      throws IOException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    DataOutputStream stream = new DataOutputStream(out);
    stream.write(incommingId);
    for (KademliaNode node : nodes) {
      stream.write(node.getKey().toBytes());
      stream.write(node.getAddress().getAddress());
      stream.writeShort(node.getPort());
    }
    return new Message(id, MessageType.LOOKUP_REPLY, out.toByteArray());
  }

  private void handleLookupReply(Message parsedMessage, KademliaNode sender) throws IOException {
    DataInputStream stream = new DataInputStream(new ByteArrayInputStream(parsedMessage.payload));
    byte[] replyTo = new byte[20];
    stream.readFully(replyTo);
    ResolvableLookup lookup = awaitingLookups.remove(new MessageIdentifier(replyTo, sender));
    if (lookup != null) {
      int length = 32 + 4 + 2;
      int numberOfNodes = (parsedMessage.payload.length - 20) / length;
      ArrayList<KademliaNode> nodes = new ArrayList<>(numberOfNodes);
      for (int i = 0; i < numberOfNodes; i++) {
        byte[] bytes = new byte[32];
        stream.readFully(bytes);
        KademliaKey key = new KademliaKey(bytes);
        byte[] ip = new byte[4];
        stream.readFully(ip);
        nodes.add(new KademliaNode(key, InetAddress.getByAddress(ip), stream.readUnsignedShort()));
      }
      lookup.resolve(this::submit, nodes);

      store(sender);

      sendPingReply(sender, parsedMessage.messageId);
    }
  }

  private void sendPingReply(KademliaNode sender, byte[] messageId) throws IOException {
    Message message = new Message(this.id, MessageType.PING_REPLY, messageId);
    send(sender, message);
  }

  private void handlePing(Message parsedMessage, KademliaNode sender) {
    byte[] replyTo = parsedMessage.payload;
    if (outstandingPings.remove(new MessageIdentifier(replyTo, sender))) {
      store(sender);
    }
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void scheduleRefresh() {
    service.scheduleWithFixedDelay(
        new RefreshBucketsTask(this.id, this.buckets, this::lookup), 1, 60, TimeUnit.MINUTES);
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void handleTimeout(MessageIdentifier e) {
    service.schedule(
        () -> {
          boolean failedPing = outstandingPings.remove(e);
          ResolvableLookup remove = awaitingLookups.remove(e);
          boolean failedLookup = remove != null;
          if (failedLookup) {
            remove.fail(this::submit);
          }
          if (failedLookup || failedPing) {
            int distance = id.distance(e.node.getKey());
            buckets.get(distance).error(e.node);
          }
        },
        500,
        TimeUnit.MILLISECONDS);
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void submit(LookupTask lookupTask) {
    service.submit(lookupTask);
  }

  private void store(KademliaNode kademliaNode) {
    int distance = id.distance(kademliaNode.getKey());
    buckets.get(distance).add(kademliaNode);
  }

  private List<KademliaNode> getNearestNodes(KademliaKey id) {
    return buckets.stream()
        .flatMap(b -> b.getNodeCopy().stream())
        .sorted(createComparator(id))
        .limit(BUCKET_SIZE)
        .collect(Collectors.toList());
  }

  static Comparator<KademliaNode> createComparator(KademliaKey toLookup) {
    return (node1, node2) -> toLookup.compareRelativeToThis(node1.getKey(), node2.getKey());
  }

  @SuppressWarnings("NonApiType")
  static boolean containsCloser(TreeSet<KademliaNode> newNodes, TreeSet<KademliaNode> seenNodes) {
    return !newNodes.isEmpty() && seenNodes.floor(newNodes.first()) == null;
  }

  private void lookup(KademliaKey id) {
    submit(new LookupTask(this::sendLookupMessage, id, this.id, getNearestNodes(id)));
  }

  /**
   * Get id of the instance.
   *
   * @return id of the instance
   */
  public KademliaKey getId() {
    return this.id;
  }

  List<KademliaBucket> getBuckets() {
    return buckets;
  }

  ThreadedLoop getInbox() {
    return inbox;
  }

  /** Close this Kademlia. */
  @Override
  public void close() {
    this.inbox.close();
    this.socket.close();
    service.shutdownNow();
  }

  /**
   * Get the address of a random known peer.
   *
   * @return the random address found in the repository of nodes
   */
  public Address getRandomAddress() {
    List<KademliaBucket> nonEmptyBuckets =
        buckets.stream().filter(KademliaBucket::hasNodes).collect(Collectors.toList());
    if (nonEmptyBuckets.isEmpty()) {
      return null;
    }
    KademliaBucket bucket = randomElement(nonEmptyBuckets);
    KademliaNode node = randomElement(bucket.getNodeCopy());
    return new Address(node.getAddress().getHostAddress(), node.getPort());
  }

  private <T> T randomElement(List<T> elements) {
    int bucketIndex = ThreadLocalRandom.current().nextInt(elements.size());
    return elements.get(bucketIndex);
  }

  static final class Message {

    private static final SecureRandom random = new SecureRandom();
    private final MessageType type;
    private final byte[] messageId;
    private final byte[] payload;
    private final KademliaKey senderId;

    Message(KademliaKey senderId, MessageType type, byte[] payload) {
      this.senderId = senderId;
      this.type = type;
      this.payload = payload;
      this.messageId = new byte[20];
      random.nextBytes(this.messageId);
    }

    private Message(byte[] data, int length) throws IOException {
      DataInputStream dataInputStream =
          new DataInputStream(new ByteArrayInputStream(data, 0, length));
      this.type = MessageType.values()[dataInputStream.readUnsignedByte()];
      byte[] keyBytes = new byte[32];
      dataInputStream.readFully(keyBytes);
      this.senderId = new KademliaKey(keyBytes);
      int payloadLength = dataInputStream.readInt();
      this.payload = new byte[payloadLength];
      dataInputStream.readFully(this.payload);
      if (type != MessageType.PING_REPLY) {
        this.messageId = new byte[20];
        dataInputStream.readFully(this.messageId);
      } else {
        this.messageId = null;
      }
    }

    @SuppressWarnings("EnumOrdinal")
    DatagramPacket toPacket(KademliaNode kademliaNode) throws IOException {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      DataOutputStream dataStream = new DataOutputStream(out);
      dataStream.writeByte(type.ordinal());
      dataStream.write(senderId.toBytes());
      dataStream.writeInt(payload.length);
      dataStream.write(payload);
      if (type != MessageType.PING_REPLY) {
        dataStream.write(messageId);
      }
      byte[] bytes = out.toByteArray();
      return new DatagramPacket(
          bytes, bytes.length, kademliaNode.getAddress(), kademliaNode.getPort());
    }

    byte[] getMessageId() {
      return messageId;
    }
  }

  enum MessageType {
    LOOKUP,
    LOOKUP_REPLY,
    PING_REPLY
  }

  static final class MessageIdentifier {

    private final KademliaNode node;
    private final byte[] messageId;

    MessageIdentifier(byte[] messageId, KademliaNode node) {
      this.messageId = messageId;
      this.node = node;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      MessageIdentifier that = (MessageIdentifier) o;
      return Objects.equals(node, that.node) && Arrays.equals(messageId, that.messageId);
    }

    @Override
    public int hashCode() {
      return Arrays.deepHashCode(new Object[] {node, messageId});
    }
  }

  void sendLookupMessage(ResolvableLookup sender, KademliaNode node, KademliaKey toLookup)
      throws IOException {
    Message message = new Message(id, MessageType.LOOKUP, toLookup.toBytes());
    MessageIdentifier key = new MessageIdentifier(message.messageId, node);
    awaitingLookups.put(key, sender);
    send(node, message);
    handleTimeout(key);
  }
}
