package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;

/**
 * Locate an object stored in the state.
 *
 * <p>Helps with either saving an object locally and return its identifier, or lookup if an object
 * is registered to exist across all shards.
 */
public final class ObjectLocator {

  private final MutableChainState state;
  private final String errorMessageTemplate;

  private ObjectLocator(MutableChainState state, String errorMessageTemplate) {
    this.state = state;
    this.errorMessageTemplate = errorMessageTemplate;
  }

  /**
   * Create a new locator for objects that may exist across all shards.
   *
   * @param state to lookup in
   * @param errorMessagePrefix prefix for the error message to throw if lookup fails
   * @param objectType type of the object to lookup
   * @return new object locator
   */
  public static ObjectLocator create(
      MutableChainState state, String errorMessagePrefix, String objectType) {
    String errorMessageTemplate =
        errorMessagePrefix + " - " + objectType + " with identifier %s is not available";
    return new ObjectLocator(state, errorMessageTemplate);
  }

  /**
   * Convert the raw input bytes to an object identifier.
   *
   * <p>Assumes that if the input size is exactly 32 bytes, it is the raw value of an identifier
   * hash. In the case we ensure the object with whe given identifier exists in shared storage, so
   * it can be accessed.
   *
   * <p>Otherwise, assume the input is a raw object that should be saved in local storage.
   *
   * <p>Throws an exception if the shared object does not exist in the shared object store.
   *
   * @param rawObjectOrHash either the raw bytes of an object, or the raw value of a hash
   * @return a hash identifier of the input
   */
  public Hash getSharedIdentifierOrSaveLocalObject(byte[] rawObjectOrHash) {
    if (rawObjectOrHash.length == 32) {
      return getSharedIdentifier(rawObjectOrHash);
    } else {
      return saveLocalObject(rawObjectOrHash);
    }
  }

  private Hash getSharedIdentifier(byte[] rawIdentifier) {
    Hash identifier = Hash.read(SafeDataInputStream.createFromBytes(rawIdentifier));
    if (state.getSharedObjectStorePlugin().exists(identifier)) {
      return identifier;
    } else {
      String errorMessage = errorMessageTemplate.formatted(identifier);
      throw new IllegalArgumentException(errorMessage);
    }
  }

  private Hash saveLocalObject(byte[] rawObject) {
    return state.saveJar(rawObject);
  }
}
