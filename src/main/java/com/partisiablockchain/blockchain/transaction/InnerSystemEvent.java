package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.transaction.EventTransaction.EventType;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerEvent;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.Arrays;
import java.util.function.BiConsumer;

/** Event for manipulating the system state of the blockchain. */
public abstract class InnerSystemEvent implements InnerEvent {

  /** Empty return value instance. */
  public static final byte[] EMPTY_RETURN_VALUE = new byte[0];

  /**
   * Execute this system event.
   *
   * @param currentShardId the id of the current shard
   * @param syncSender callback to create a SyncEvent
   * @param eventManager callback to allow creating callbacks
   * @param state the current state of the chain
   * @param blockProductionTime the production time of the current block
   * @return return value to be returned to any registered callbacks
   */
  public abstract byte[] execute(
      String currentShardId,
      BiConsumer<String, SyncEvent> syncSender,
      CallbackCreator eventManager,
      MutableChainState state,
      long blockProductionTime);

  /**
   * Get type of system event.
   *
   * @return type of system event
   */
  public abstract SystemEventType getSystemEventType();

  /**
   * Write system event to stream.
   *
   * @param stream where to write to
   */
  protected abstract void writeSystemEvent(SafeDataOutputStream stream);

  @Override
  public final EventType getEventType() {
    return EventType.SYSTEM;
  }

  @Override
  public final void writeInner(SafeDataOutputStream stream) {
    stream.writeEnum(getSystemEventType());
    writeSystemEvent(stream);
  }

  static InnerSystemEvent read(SafeDataInputStream stream) {
    SystemEventType systemEventType = stream.readEnum(SystemEventType.values());
    return systemEventType.read(stream);
  }

  /** Type of system event. */
  public enum SystemEventType {
    /** Create account. */
    CREATE_ACCOUNT(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return CreateAccountEvent.read(stream);
      }
    },
    /** Check existence. */
    CHECK_EXISTENCE(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return CheckExistenceEvent.read(stream);
      }
    },
    /** Set feature. */
    SET_FEATURE(true) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return SetFeatureEvent.read(stream);
      }
    },
    /** Update local plugin state. */
    UPDATE_LOCAL_PLUGIN_STATE(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return UpdateLocalPluginStateEvent.read(stream);
      }
    },
    /** Update global plugin state. */
    UPDATE_GLOBAL_PLUGIN_STATE(true) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return UpdateGlobalPluginStateEvent.read(stream);
      }
    },
    /** Update plugin. */
    UPDATE_PLUGIN(true) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return UpdatePluginEvent.read(stream);
      }
    },
    /** Callback. */
    CALLBACK(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return CallbackEvent.read(stream);
      }
    },
    /** Create shard. */
    CREATE_SHARD(true) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return CreateShardEvent.read(stream);
      }
    },
    /** Remove shard. */
    REMOVE_SHARD(true) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return RemoveShardEvent.read(stream);
      }
    },
    /** Update context free plugin state. */
    UPDATE_CONTEXT_FREE_PLUGIN_STATE(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return UpdateContextFreePluginState.read(stream);
      }
    },
    /** Upgrade system contract. */
    UPGRADE_SYSTEM_CONTRACT(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return UpgradeSystemContractEvent.read(stream);
      }
    },
    /** Remove an existing contract from the state. */
    REMOVE_CONTRACT(false) {
      @Override
      InnerSystemEvent read(SafeDataInputStream stream) {
        return RemoveContract.read(stream);
      }
    };

    /** Decides if event is a governance event. */
    public final boolean governanceEvent;

    SystemEventType(boolean governanceEvent) {
      this.governanceEvent = governanceEvent;
    }

    abstract InnerSystemEvent read(SafeDataInputStream stream);
  }

  /** Event for creating an account. */
  public static final class CreateAccountEvent extends InnerSystemEvent {

    private final BlockchainAddress toCreate;

    /**
     * Create event for creating an account.
     *
     * @param toCreate address of account to create
     */
    public CreateAccountEvent(BlockchainAddress toCreate) {
      this.toCreate = toCreate;
    }

    @Override
    public BlockchainAddress target() {
      return toCreate;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.createAccount(toCreate);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.CREATE_ACCOUNT;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      toCreate.write(stream);
    }

    static CreateAccountEvent read(SafeDataInputStream stream) {
      BlockchainAddress toCreate = BlockchainAddress.read(stream);
      return new CreateAccountEvent(toCreate);
    }
  }

  /** Event for changing the owner of a contract. */
  public static final class CheckExistenceEvent extends InnerSystemEvent {

    private final BlockchainAddress contractOrAccountAddress;

    /**
     * Create event for checking the existence of an address.
     *
     * @param contractOrAccountAddress to check for existence
     */
    public CheckExistenceEvent(BlockchainAddress contractOrAccountAddress) {
      this.contractOrAccountAddress = contractOrAccountAddress;
    }

    @Override
    public BlockchainAddress target() {
      return contractOrAccountAddress;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      boolean contractExists = state.getContracts().contains(contractOrAccountAddress);
      boolean accountExists = state.existsAccounts(contractOrAccountAddress);
      return SafeDataOutputStream.serialize(
          stream -> stream.writeBoolean(contractExists || accountExists));
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.CHECK_EXISTENCE;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      contractOrAccountAddress.write(stream);
    }

    static CheckExistenceEvent read(SafeDataInputStream stream) {
      BlockchainAddress contractOrAccountAddress = BlockchainAddress.read(stream);
      return new CheckExistenceEvent(contractOrAccountAddress);
    }
  }

  /** Event for setting a feature in the state. */
  public static final class SetFeatureEvent extends InnerSystemEvent {

    private final String key;
    private final String value;

    /**
     * Create an event for setting a feature in the state.
     *
     * @param key indicates feature to set
     * @param value value to set on feature
     */
    public SetFeatureEvent(String key, String value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.setFeature(key, value);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.SET_FEATURE;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeString(key);
      stream.writeOptional(
          (valueOptional, streamOptional) -> streamOptional.writeString(valueOptional), value);
    }

    static SetFeatureEvent read(SafeDataInputStream stream) {
      String key = DataStreamLimit.readString(stream);
      String value = stream.readOptional(DataStreamLimit::readString);
      return new SetFeatureEvent(key, value);
    }
  }

  /** An event to update the local state of a plugin. */
  public static final class UpdateLocalPluginStateEvent extends InnerSystemEvent {

    private final ChainPluginType type;
    private final LocalPluginStateUpdate update;

    /**
     * Create an event for updating local state of a plugin.
     *
     * @param type the type of plugin to update
     * @param update an update to the local state of a plugin
     */
    public UpdateLocalPluginStateEvent(ChainPluginType type, LocalPluginStateUpdate update) {
      this.type = type;
      this.update = update;
    }

    @Override
    public BlockchainAddress target() {
      return update.getContext();
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      return state.updateLocalPluginState(
          type, update.getContext(), update.getRpc(), blockProductionTime);
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.UPDATE_LOCAL_PLUGIN_STATE;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeEnum(type);
      update.write(stream);
    }

    static UpdateLocalPluginStateEvent read(SafeDataInputStream stream) {
      ChainPluginType type = stream.readEnum(ChainPluginType.values());
      LocalPluginStateUpdate update = LocalPluginStateUpdate.read(stream);
      return new UpdateLocalPluginStateEvent(type, update);
    }
  }

  /** An event to update the local state of a plugin without context on a named shard. */
  public static final class UpdateContextFreePluginState extends InnerSystemEvent {

    private final ChainPluginType type;
    private final byte[] rpc;

    /**
     * Creates an event for updating the local state of a plugin without context.
     *
     * @param type the type of plugin to update
     * @param rpc local state migration rpc
     */
    public UpdateContextFreePluginState(ChainPluginType type, byte[] rpc) {
      this.type = type;
      this.rpc = rpc.clone();
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      return state.updateLocalPluginState(type, rpc, blockProductionTime);
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.UPDATE_CONTEXT_FREE_PLUGIN_STATE;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeEnum(type);
      stream.writeDynamicBytes(rpc);
    }

    static UpdateContextFreePluginState read(SafeDataInputStream stream) {
      ChainPluginType type = stream.readEnum(ChainPluginType.values());
      byte[] rpc = DataStreamLimit.readDynamicBytes(stream);
      return new UpdateContextFreePluginState(type, rpc);
    }
  }

  /** Event for updating the global state of a plugin. */
  public static final class UpdateGlobalPluginStateEvent extends InnerSystemEvent {

    private final ChainPluginType type;
    private final GlobalPluginStateUpdate update;

    /**
     * Creates a new event for updating global state of a plugin.
     *
     * @param type the type of plugin to update
     * @param update an update to the global state of a plugin
     */
    public UpdateGlobalPluginStateEvent(ChainPluginType type, GlobalPluginStateUpdate update) {
      this.type = type;
      this.update = update;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      return state.updateGlobalPluginState(blockProductionTime, type, update.getRpc());
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.UPDATE_GLOBAL_PLUGIN_STATE;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeEnum(type);
      update.write(stream);
    }

    static UpdateGlobalPluginStateEvent read(SafeDataInputStream stream) {
      ChainPluginType type = stream.readEnum(ChainPluginType.values());
      GlobalPluginStateUpdate update = GlobalPluginStateUpdate.read(stream);
      return new UpdateGlobalPluginStateEvent(type, update);
    }
  }

  /** Event for changing an active plugin. */
  public static final class UpdatePluginEvent extends InnerSystemEvent {

    private final ChainPluginType type;
    private final byte[] jar;
    private final byte[] rpc;

    /**
     * Construct a new event for updating an active plugin.
     *
     * @param type the type of plugin to update
     * @param jar the jar to use as plugin
     * @param rpc global state migration rpc
     */
    public UpdatePluginEvent(ChainPluginType type, byte[] jar, byte[] rpc) {
      this.type = type;
      this.jar = jar.clone();
      this.rpc = rpc.clone();
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.setPlugin(syncSender, currentShardId, type, jar, rpc);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.UPDATE_PLUGIN;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeEnum(type);
      stream.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeDynamicBytes), jar);
      stream.writeDynamicBytes(rpc);
    }

    static UpdatePluginEvent read(SafeDataInputStream stream) {
      ChainPluginType type = stream.readEnum(ChainPluginType.values());
      byte[] jar = stream.readOptional(DataStreamLimit::readDynamicBytes);
      byte[] invocation = DataStreamLimit.readDynamicBytes(stream);
      return new UpdatePluginEvent(type, jar, invocation);
    }
  }

  /** Callback sent when a transaction has been executed. */
  public static final class CallbackEvent extends InnerSystemEvent {

    private final ReturnEnvelope destination;
    private final Hash completedTransaction;
    private final boolean success;
    private final byte[] returnValue;

    /**
     * Creates a new callback event with a return value.
     *
     * @param destination the destination for this callback
     * @param completedTransaction the transaction that just has completed
     * @param success whether the transaction was successful
     * @param returnValue return value of the callback event
     */
    public CallbackEvent(
        ReturnEnvelope destination,
        Hash completedTransaction,
        boolean success,
        byte[] returnValue) {
      this.destination = destination;
      this.completedTransaction = completedTransaction;
      this.success = success;
      this.returnValue = returnValue.clone();
    }

    @Override
    public BlockchainAddress target() {
      return destination.contract();
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.callbackReceived(
          destination.contract(),
          completedTransaction,
          success,
          callback -> eventManager.create(destination.contract(), callback),
          returnValue);
      return returnValue.clone();
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.CALLBACK;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      destination.write(stream);
      completedTransaction.write(stream);
      stream.writeBoolean(success);
      stream.writeDynamicBytes(returnValue);
    }

    static CallbackEvent read(SafeDataInputStream stream) {
      ReturnEnvelope returnEnvelope = ReturnEnvelope.read(stream);
      Hash completedTransaction = Hash.read(stream);
      boolean success = stream.readBoolean();
      byte[] returnValue = DataStreamLimit.readDynamicBytes(stream);
      return new CallbackEvent(returnEnvelope, completedTransaction, success, returnValue);
    }

    @Override
    public String toString() {
      return "CallbackEvent{"
          + "destination="
          + destination
          + ", completedTransaction="
          + completedTransaction
          + ", success="
          + success
          + ", returnValue="
          + Arrays.toString(returnValue)
          + '}';
    }
  }

  /** Event for creating a shard. */
  public static final class CreateShardEvent extends InnerSystemEvent {

    private final String shardId;

    /**
     * Construct a new event for creating a shard.
     *
     * @param shardId id of the shard.
     */
    public CreateShardEvent(String shardId) {
      this.shardId = shardId;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.addActiveShard(syncSender, currentShardId, shardId);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.CREATE_SHARD;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeString(shardId);
    }

    static CreateShardEvent read(SafeDataInputStream stream) {
      String shardId = DataStreamLimit.readString(stream);
      return new CreateShardEvent(shardId);
    }
  }

  /** Event for removing a shard. */
  public static final class RemoveShardEvent extends InnerSystemEvent {

    private final String shardId;

    /**
     * Construct a new event for removing a shard.
     *
     * @param shardId id of the shard.
     */
    public RemoveShardEvent(String shardId) {
      this.shardId = shardId;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.removeActiveShard(syncSender, currentShardId, shardId);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.REMOVE_SHARD;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeString(shardId);
    }

    static RemoveShardEvent read(SafeDataInputStream stream) {
      String shardId = DataStreamLimit.readString(stream);
      return new RemoveShardEvent(shardId);
    }
  }

  /** Event for upgrading a system contract. */
  public static final class UpgradeSystemContractEvent extends InnerSystemEvent {

    private final byte[] contractJar;
    private final byte[] binderJar;
    private final byte[] abi;
    private final byte[] rpc;
    private final BlockchainAddress contractAddress;

    /**
     * Construct a new event for upgrading a contract.
     *
     * @param contractJar the contract.
     * @param binderJar the binder.
     * @param abi the abi for the contract
     * @param rpc the invocation.
     * @param contractAddress the address of the contract.
     */
    public UpgradeSystemContractEvent(
        byte[] contractJar,
        byte[] binderJar,
        byte[] abi,
        byte[] rpc,
        BlockchainAddress contractAddress) {
      this.contractJar = contractJar.clone();
      this.binderJar = binderJar.clone();
      this.abi = abi.clone();
      this.rpc = rpc.clone();
      this.contractAddress = contractAddress;
    }

    @Override
    public BlockchainAddress target() {
      return contractAddress;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      Hash binderHash = state.saveJar(binderJar);
      Hash contractHash = state.saveJar(contractJar);
      Hash abiHash = state.saveJar(abi);
      CoreContractState newCoreState =
          CoreContractState.create(binderHash, contractHash, abiHash, contractJar.length);
      state.upgradeContract(contractAddress, newCoreState, rpc);
      return EMPTY_RETURN_VALUE;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.UPGRADE_SYSTEM_CONTRACT;
    }

    static UpgradeSystemContractEvent read(SafeDataInputStream stream) {
      byte[] contractJar = DataStreamLimit.readDynamicBytes(stream);
      byte[] binderJar = DataStreamLimit.readDynamicBytes(stream);
      byte[] abi = DataStreamLimit.readDynamicBytes(stream);
      byte[] rpc = DataStreamLimit.readDynamicBytes(stream);
      BlockchainAddress contractAddress = BlockchainAddress.read(stream);
      return new UpgradeSystemContractEvent(contractJar, binderJar, abi, rpc, contractAddress);
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      stream.writeDynamicBytes(contractJar);
      stream.writeDynamicBytes(binderJar);
      stream.writeDynamicBytes(abi);
      stream.writeDynamicBytes(rpc);
      contractAddress.write(stream);
    }
  }

  /** System event used to remove a contract from the chain state. */
  public static final class RemoveContract extends InnerSystemEvent {

    private final BlockchainAddress contractAddress;

    /**
     * Create a new remove contract event.
     *
     * @param contract the contract that should be removed
     */
    public RemoveContract(BlockchainAddress contract) {
      this.contractAddress = contract;
    }

    @Override
    public byte[] execute(
        String currentShardId,
        BiConsumer<String, SyncEvent> syncSender,
        CallbackCreator eventManager,
        MutableChainState state,
        long blockProductionTime) {
      state.removeContract(contractAddress, blockProductionTime);
      return EMPTY_RETURN_VALUE;
    }

    static RemoveContract read(SafeDataInputStream stream) {
      return new RemoveContract(BlockchainAddress.read(stream));
    }

    @Override
    public BlockchainAddress target() {
      return contractAddress;
    }

    @Override
    public SystemEventType getSystemEventType() {
      return SystemEventType.REMOVE_CONTRACT;
    }

    @Override
    protected void writeSystemEvent(SafeDataOutputStream stream) {
      contractAddress.write(stream);
    }
  }
}
