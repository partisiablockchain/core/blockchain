package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/**
 * Super class for account plugin.
 *
 * @param <GlobalT> the type of the global plugin state
 * @param <ContextFreeT> type of the part of the state without context
 * @param <AccountT> type of the state associated with each account
 * @param <ContractT> type of the state associated with each contract
 */
@Immutable
public abstract class BlockchainAccountPlugin<
        GlobalT extends StateSerializable,
        ContextFreeT extends StateSerializable,
        AccountT extends StateSerializable,
        ContractT extends StateSerializable>
    implements BlockchainPlugin<GlobalT, AccountPluginState<ContextFreeT, AccountT, ContractT>> {

  /**
   * Converts network fee of public blockchain usage to gas equivalents.
   *
   * @param globalState the global fee plugin state
   * @param bytes number of bytes to pay for
   * @return the amount of gas equivalents
   */
  public abstract long convertNetworkFee(GlobalT globalState, long bytes);

  /**
   * Determine if an account is allowed to push a transaction with the declared cost.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global fee plugin state
   * @param localState the local fee plugin state
   * @param transaction the transaction to be pushed
   * @return true if the transaction should be allowed
   */
  public abstract boolean canCoverCost(
      PluginContext pluginContext,
      GlobalT globalState,
      AccountState<ContextFreeT, AccountT> localState,
      SignedTransaction transaction);

  /**
   * Pay fees for blockchain usage. Should only be called if {@link #canCoverCost(PluginContext,
   * StateSerializable, AccountState, SignedTransaction)} returns true.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global fee plugin state
   * @param localState the local fee plugin state
   * @param transaction the transaction to pay for
   * @return a new updated fee plugin with the updates applied
   */
  public abstract PayCostResult<ContextFreeT, AccountT> payCost(
      PluginContext pluginContext,
      GlobalT globalState,
      AccountState<ContextFreeT, AccountT> localState,
      SignedTransaction transaction);

  /**
   * Notify plugin that a contract has been created to enable storage fee.
   *
   * @param pluginContext the context of the plugin
   * @param globalState the global fee plugin state
   * @param contextFreeState the state without context
   * @param address the address of the contract that was created
   * @return the next contract state
   */
  public abstract ContractState<ContextFreeT, ContractT> contractCreated(
      PluginContext pluginContext,
      GlobalT globalState,
      ContextFreeT contextFreeState,
      BlockchainAddress address);

  /**
   * Force delete an account, any values should be collected by the plugin.
   *
   * @param pluginContext the plugin context for when the contract was removed
   * @param globalState the global fee plugin state
   * @param localState the local fee plugin state
   * @return a new updated fee plugin with the update applied
   */
  public abstract ContextFreeT removeContract(
      PluginContext pluginContext,
      GlobalT globalState,
      ContractState<ContextFreeT, ContractT> localState);

  /**
   * Move gas to target contract.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global fee plugin state
   * @param localState the local fee plugin state
   * @param contract the address of the contract to place the gas on
   * @param gas the amount of gas to move
   * @return a new updated fee plugin with the updates applied
   */
  public abstract ContractState<ContextFreeT, ContractT> updateContractGasBalance(
      PluginContext pluginContext,
      GlobalT globalState,
      ContractState<ContextFreeT, ContractT> localState,
      BlockchainAddress contract,
      long gas);

  /**
   * Register gas that has been used on the block chain.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global fee plugin state
   * @param contextFreeState the state without context
   * @param gas the amount of gas that has been used
   * @return the new updated context free state
   */
  public abstract ContextFreeT registerBlockchainUsage(
      PluginContext pluginContext, GlobalT globalState, ContextFreeT contextFreeState, long gas);

  /**
   * Handle service fees after a completed transaction.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global account plugin state
   * @param contractState state of the paying contract
   * @param pendingFees fees to be distributed to block producers
   * @return a new updated fee plugin with the updates applied
   */
  public abstract PayServiceFeesResult<ContractState<ContextFreeT, ContractT>> payServiceFees(
      PluginContext pluginContext,
      GlobalT globalState,
      ContractState<ContextFreeT, ContractT> contractState,
      List<PendingFee> pendingFees);

  /**
   * Handle infrastructure fees after a completed price update.
   *
   * @param pluginContext the context for the plugin
   * @param globalState the global account plugin state
   * @param localState the context free account plugin state
   * @param pendingFees fees to be distributed to price oracle nodes
   * @return updated context free state
   */
  public abstract ContextFreeT payInfrastructureFees(
      PluginContext pluginContext,
      GlobalT globalState,
      ContextFreeT localState,
      List<PendingFee> pendingFees);

  /**
   * Registers a new block with transaction, a block defines a point in time, the transactions the
   * active contracts. If storage fees are active on this chain, then the plugin can inspect the
   * active contracts as well as the overall chain state and collect fees. Returning immediately
   * disables storage fees.
   *
   * @param globalState the global fee plugin state
   * @param localState the local fee plugin state
   * @param context context for update
   * @return a new updated fee plugin with the updates applied
   */
  public abstract ContextFreeT updateForBlock(
      GlobalT globalState, ContextFreeT localState, BlockContext<ContractT> context);

  /**
   * Updates the state of a contract and attempts to collect storage fees. If the contract has
   * enough balance on this chain to cover the storage cost, then the plugin can inspect active
   * contract as well as the overall chain state and collect fees. If the contract storage does not
   * have enough balance to cover the storage fee, then the contract is marked for deletion.
   *
   * @param pluginContext plugin context for the block that will update the contract
   * @param globalState the global fee plugin state
   * @param currentState the local fee plugin state
   * @param contract the contract to update
   * @param size storage size for the contract
   * @return a new updated fee plugin with the updates applied, null contract state implies it
   *     should be deleted
   */
  public abstract ContractState<ContextFreeT, ContractT> updateActiveContract(
      PluginContext pluginContext,
      GlobalT globalState,
      ContractState<ContextFreeT, ContractT> currentState,
      BlockchainAddress contract,
      long size);

  /**
   * Updates the state of a contract and attempts to collect storage fees. If the contract has
   * enough balance on this chain to cover the storage cost, then the plugin can inspect active
   * contract as well as the overall chain state and collect fees. If the contract storage does not
   * have enough balance to cover the storage fee, but has enough stakes to cover being put on ice,
   * then the contract will be marked as iced. Otherwise, the contract is marked for deletion by
   * having inner state being null.
   *
   * @param pluginContext plugin context for the block that will update the contract
   * @param globalState the global fee plugin state
   * @param currentState the local fee plugin state
   * @param contract the contract to update
   * @param size storage size for the contract
   * @return IcedContractState with the updated contract and a boolean indicating if the contract is
   *     iced.
   */
  public IcedContractState<ContextFreeT, ContractT> updateActiveContractIced(
      PluginContext pluginContext,
      GlobalT globalState,
      ContractState<ContextFreeT, ContractT> currentState,
      BlockchainAddress contract,
      long size) {
    return new IcedContractState<>(
        false, updateActiveContract(pluginContext, globalState, currentState, contract, size));
  }

  /**
   * Struct with a ContractState and a boolean indicating if the contract is iced.
   *
   * @param iced indicates if the contract is iced
   * @param contractState the contract state
   * @param <L> type parameter for the contract state
   * @param <C> type parameter for the contract state
   */
  public record IcedContractState<L, C>(boolean iced, ContractState<L, C> contractState) {}

  @Override
  public final AccountPluginState<ContextFreeT, AccountT, ContractT> migrateLocal(
      StateAccessor currentLocal) {
    AvlTree<BlockchainAddress, AccountT> accounts = AvlTree.create();
    AvlTree<BlockchainAddress, ContractT> contracts = AvlTree.create();
    StateAccessor currentContextFreeT = null;
    if (currentLocal != null) {

      StateAccessor accountTree = currentLocal.get("accounts");
      AvlTree<BlockchainAddress, ? extends StateSerializable> currentAccounts =
          accountTree.typedAvlTree(BlockchainAddress.class, StateSerializable.class);
      for (BlockchainAddress address : currentAccounts.keySet()) {
        AccountT accountT = migrateAccount(accountTree.getTreeValue(address));
        if (accountT != null) {
          accounts = accounts.set(address, accountT);
        }
      }

      StateAccessor contractTree = currentLocal.get("contracts");
      AvlTree<BlockchainAddress, ? extends StateSerializable> currentContracts =
          contractTree.typedAvlTree(BlockchainAddress.class, StateSerializable.class);
      for (BlockchainAddress address : currentContracts.keySet()) {
        ContractT contract = migrateContract(address, contractTree.getTreeValue(address));
        if (contract != null) {
          contracts = contracts.set(address, contract);
        }
      }
      currentContextFreeT = currentLocal.get("contextFree");
    }
    return new AccountPluginState<>(migrateContextFree(currentContextFreeT), accounts, contracts);
  }

  @SuppressWarnings("unchecked")
  @Override
  public final Class<AccountPluginState<ContextFreeT, AccountT, ContractT>> getLocalStateClass() {
    Class<?> stateClass = AccountPluginState.class;
    return (Class<AccountPluginState<ContextFreeT, AccountT, ContractT>>) stateClass;
  }

  /**
   * Execute a migration against the context free state.
   *
   * @param current current context free state
   * @return next context free state
   */
  protected abstract ContextFreeT migrateContextFree(StateAccessor current);

  /**
   * Execute a migration against the contract state.
   *
   * @param address address of contract to migrate
   * @param current current contract state
   * @return next contract state
   */
  protected abstract ContractT migrateContract(BlockchainAddress address, StateAccessor current);

  /**
   * Execute a migration against the account state.
   *
   * @param current current account state
   * @return next account state
   */
  protected abstract AccountT migrateAccount(StateAccessor current);

  /**
   * Execute an invocation against the contract state.
   *
   * @param pluginContext context for plugin
   * @param global current global state
   * @param current current contract state
   * @param address context for the state
   * @param rpc stream to read payload for invocation
   * @return next contract state
   */
  protected abstract InvokeResult<ContractState<ContextFreeT, ContractT>> invokeContract(
      PluginContext pluginContext,
      GlobalT global,
      ContractState<ContextFreeT, ContractT> current,
      BlockchainAddress address,
      byte[] rpc);

  /**
   * Execute an invocation against the account state.
   *
   * @param pluginContext context for plugin
   * @param global current global state
   * @param current current account state
   * @param rpc stream to read payload for invocation
   * @return next account state
   */
  protected abstract InvokeResult<AccountState<ContextFreeT, AccountT>> invokeAccount(
      PluginContext pluginContext,
      GlobalT global,
      AccountState<ContextFreeT, AccountT> current,
      byte[] rpc);

  /**
   * Execute an invocation against the context free state.
   *
   * @param pluginContext context for plugin
   * @param global current global state
   * @param contextFree current context free state
   * @param rpc stream to read payload for invocation
   * @return next context free state
   */
  protected abstract InvokeResult<ContextFreeT> invokeContextFree(
      PluginContext pluginContext, GlobalT global, ContextFreeT contextFree, byte[] rpc);

  /**
   * Pay fees related to a BYOC transaction.
   *
   * @param pluginContext context for plugin
   * @param globalState the current global state
   * @param localState the local state
   * @param amount the tax to be paid
   * @param symbol the BYOC coin symbol
   * @param nodes the BYOC nodes responsible for the transaction
   * @return next context free state
   */
  protected abstract ContextFreeT payByocFees(
      PluginContext pluginContext,
      GlobalT globalState,
      ContextFreeT localState,
      Unsigned256 amount,
      String symbol,
      FixedList<BlockchainAddress> nodes);

  @Override
  public final InvokeResult<AccountPluginState<ContextFreeT, AccountT, ContractT>> invokeLocal(
      PluginContext pluginContext,
      GlobalT globalState,
      AccountPluginState<ContextFreeT, AccountT, ContractT> state,
      BlockchainAddress invocationContext,
      byte[] rpc) {
    if (invocationContext == null) {
      InvokeResult<ContextFreeT> contextFree =
          invokeContextFree(pluginContext, globalState, state.getContextFree(), rpc);
      return new InvokeResult<>(
          state.setContextFree(contextFree.updatedState()), contextFree.result());
    } else if (invocationContext.getType() == BlockchainAddress.Type.ACCOUNT) {
      InvokeResult<AccountState<ContextFreeT, AccountT>> accountState =
          invokeAccount(pluginContext, globalState, state.getAccountState(invocationContext), rpc);
      return new InvokeResult<>(
          state
              .setAccount(invocationContext, accountState.updatedState().account)
              .setContextFree(accountState.updatedState().local),
          accountState.result());
    } else {
      InvokeResult<ContractState<ContextFreeT, ContractT>> contractState =
          invokeContract(
              pluginContext,
              globalState,
              state.getContractState(invocationContext),
              invocationContext,
              rpc);
      return new InvokeResult<>(
          state
              .setContract(invocationContext, contractState.updatedState().contract)
              .setContextFree(contractState.updatedState().local),
          contractState.result());
    }
  }

  /**
   * Struct for the current state of an account.
   *
   * @param <ContextFreeT> type of the part of the state without context
   * @param <AccountT> type of the state associated with each account
   */
  public static final class AccountState<ContextFreeT, AccountT> {

    /** Context free state. */
    public final ContextFreeT local;

    /** Account state. */
    public final AccountT account;

    /**
     * Create an updated state for an account invocation.
     *
     * @param local the next local value
     * @param account the account state for the contract
     */
    public AccountState(ContextFreeT local, AccountT account) {
      this.local = local;
      this.account = account;
    }
  }

  /**
   * Struct for the current state of a contract.
   *
   * @param <ContextFreeT> type of the part of the state without context
   * @param <ContractT> type of the state associated with each contract
   */
  public static final class ContractState<ContextFreeT, ContractT> {

    /** Context free state. */
    public final ContextFreeT local;

    /** Contract state. */
    public final ContractT contract;

    /**
     * Create an updated state for a contract invocation.
     *
     * @param local the next local value
     * @param contract the contract state for the contract
     */
    public ContractState(ContextFreeT local, ContractT contract) {
      this.local = local;
      this.contract = contract;
    }
  }

  /**
   * Context for doing update for block.
   *
   * @param <ContractT> the type of contract values
   */
  public interface BlockContext<ContractT> {

    /**
     * Get the block that this is an update for.
     *
     * @return the current block
     */
    Block currentBlock();

    /**
     * Register a contract that should be checked as part of this update.
     *
     * @param contract the contract to check
     */
    void registerContractUpdate(BlockchainAddress contract);

    /**
     * Get all the contracts that has an associated state in the active account plugin.
     *
     * @return the active contracts
     */
    AvlTree<BlockchainAddress, ContractT> contracts();
  }

  /**
   * The result from paying for a transaction.
   *
   * @param <ContextFreeT> the context free part of the account state
   * @param <AccountT> the account state
   */
  public record PayCostResult<ContextFreeT, AccountT>(
      AccountState<ContextFreeT, AccountT> accountState, long remainingGas) {}
}
