package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link Address}. */
public final class AddressTest {

  // PMD does not allow hardcoded ips
  private static final String IP = "8.8" + ".8.8";

  /** {@link Address#toString} is simply the intuitive formatting of the {@link Address}. */
  @Test
  public void testToString() {
    Address address = new Address(IP, 8321);
    Assertions.assertThat(address.toString()).isEqualTo("8.8.8.8:8321");
  }

  /** Addresses can be parsed. */
  @Test
  public void parseString() {
    Address address = Address.parseAddress("MyNewHost.test:758");
    Assertions.assertThat(address.hostname()).isEqualTo("MyNewHost.test");
    Assertions.assertThat(address.port()).isEqualTo(758);
  }

  /** Addresses can be parsed through Jackson-compatible {@link Address#fromString}. */
  @Test
  public void fromString() {
    Address address = Address.fromString("MyNewHost.test:758");
    Assertions.assertThat(address.hostname()).isEqualTo("MyNewHost.test");
    Assertions.assertThat(address.port()).isEqualTo(758);
  }

  /** Ports must be within the well-defined range of ports. */
  @Test
  public void portRangeIsChecked() {
    Assertions.assertThatCode(() -> new Address("test", -1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Port must be in range [0, 65535], but was -1");
    Assertions.assertThatCode(() -> new Address("test", 65536))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Port must be in range [0, 65535], but was 65536");
  }

  /** Fail if addresses are not of the correct format. */
  @Test
  public void parsedAddressMustBeValid() {
    Assertions.assertThatCode(() -> Address.parseAddress("not:an:address"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Given string did not match expected address format <HOSTNAME>:<PORT>");
    Assertions.assertThatCode(() -> Address.parseAddress("not"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Given string did not match expected address format <HOSTNAME>:<PORT>");
    Assertions.assertThatCode(() -> Address.parseAddress("not:aninteger"))
        .isInstanceOf(NumberFormatException.class)
        .hasMessage("For input string: \"aninteger\"");
  }
}
