package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.providers.AdditionalStatusProvider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Status provider for network nodes. Returns a mapping of all public keys of our connections. All
 * connection is alive from this nodes view.
 */
public final class NetworkNodeStatusProvider implements AdditionalStatusProvider {

  private final Supplier<List<Neighbour>> connectedNeighbours;

  private final String name;

  NetworkNodeStatusProvider(String name, Supplier<List<Neighbour>> connectedNeighbours) {
    this.connectedNeighbours = connectedNeighbours;
    this.name = name;
  }

  @Override
  public String getName() {
    return nullSafeName() + "-FloodingNetworkStatus";
  }

  private String nullSafeName() {
    if (name == null) {
      return "Gov";
    } else {
      return name;
    }
  }

  @Override
  public Map<String, String> getStatus() {
    List<Neighbour> neighbours = connectedNeighbours.get();
    Map<String, String> connectedNeighbours = new HashMap<>();
    for (int i = 0; i < neighbours.size(); i++) {
      Neighbour neighbour = neighbours.get(i);
      String outboundConnection = neighbour.isOutbound() ? " (out)" : " (in)";
      connectedNeighbours.put(
          "connection" + i,
          neighbour.getConnection().getConnectionPublicKey().toString() + outboundConnection);
    }
    return connectedNeighbours;
  }
}
