package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.storage.MemoryStorage;
import com.secata.tools.thread.ExecutionFactoryFake;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of routing plugin system. */
public final class BlockchainLedgerRoutingFilterTest extends CloseableTest {

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private BlockchainLedger blockchain;

  @Test
  public void filterTransactions() {
    final Path newFolder = temporaryFolder.resolve("filterTransactions");
    Assertions.assertThat(newFolder.toFile().mkdirs()).isTrue();
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                chainState ->
                    BlockchainLedgerRoutingTest.setupChain(
                        chainState, BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_SHARD_ONE),
                network,
                new DummyExecutableEventFinder(),
                ExecutionFactoryFake.create()));

    InteractWithContractTransaction transaction =
        BlockchainLedgerRoutingTest.UpdateRoutingInTest.create(new byte[0]);

    Assertions.assertThat(blockchain.addPendingTransaction(signedTransaction(transaction)))
        .withFailMessage("Transaction was added to pending")
        .isFalse();
  }

  private SignedTransaction signedTransaction(InteractWithContractTransaction transaction) {
    return BlockchainLedgerRoutingTest.signedTransaction(blockchain, transaction);
  }
}
