package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.SignedTransaction;

/** Accessor used to get information for the account plugin. */
public abstract class AccountPluginAccessor {

  /**
   * Convert network fee of network usage to gas.
   *
   * @param bytes network usage in byte count
   * @return gas amount
   */
  public abstract long convertNetworkFee(long bytes);

  /**
   * Checks if fee for transaction is covered.
   *
   * @param transaction to check coverage for
   * @param blockProductionTime needed context for fee plugin
   * @return true if transaction fee is covered, false otherwise
   */
  public abstract boolean canCoverFee(SignedTransaction transaction, long blockProductionTime);
}
