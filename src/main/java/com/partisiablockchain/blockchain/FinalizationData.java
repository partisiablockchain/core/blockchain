package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.NetworkFloodable;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** The finalization data for a final block. */
public final class FinalizationData implements NetworkFloodable {

  private final byte[] data;

  private FinalizationData(byte[] data) {
    this.data = data;
  }

  /**
   * Create finalization data.
   *
   * @param data as bytes
   * @return created finalization data
   */
  public static FinalizationData create(byte[] data) {
    return new FinalizationData(data.clone());
  }

  /**
   * Read a FinalizationData from the supplied stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static FinalizationData read(SafeDataInputStream stream) {
    byte[] data = DataStreamLimit.readDynamicBytes(stream);
    return new FinalizationData(data);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeDynamicBytes(data);
  }

  /**
   * Get data as bytes.
   *
   * @return data as bytes
   */
  public byte[] getData() {
    return data.clone();
  }
}
