package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin.BlockValidation;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.genesis.GenesisFile;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableEventFinder;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEventCompressed;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.BlockChainNetwork;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.flooding.NetworkNode;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.flooding.SemiCompressedEvent;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.storage.BlockStateStorage;
import com.partisiablockchain.storage.BlockchainStorage;
import com.partisiablockchain.storage.BlockchainStorage.ValidBlock;
import com.partisiablockchain.storage.ExecutableBlock;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.util.LivenessMonitor;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.ThrowingRunnable;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.thread.ExecutionFactory;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The core class in the blockchain. This class holds the current state and add new blocks which
 * changes the state ({@link #appendBlock(FinalBlock)}. Furthermore, this class makes sure these
 * changes are persisted to disc and flooded to the network. We also check blocks and transactions
 * if they are valid.
 */
public final class BlockchainLedger implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(BlockchainLedger.class);

  /**
   * The amount of millis to allow a block production time to be into the future. If more that this
   * the block is considered invalid.
   */
  public static final long MAX_PRODUCTION_TIME_DRIFT = 60_000;

  static final String LISTENER_THREAD_NAME = "LedgerListener";

  private final Executor finalizationExecutor;
  private BlockView blockView;
  private final Map<Hash, SignedTransaction> pending = new ConcurrentHashMap<>();
  private final Map<Hash, ExecutableEvent> pendingEvents = new ConcurrentHashMap<>();
  private final BlockchainStorage blockchainStorage;
  private final List<BlockchainListener> listeners =
      Collections.synchronizedList(new ArrayList<>());
  private final List<FinalizationListener> finalizationListeners = new ArrayList<>();
  private final BlockChainNetwork blockChainNetwork;
  private final BlockRequester blockRequester;
  private final LivenessMonitor liveness;

  /** Timestamp indicating when the last block was finished appending. */
  private long latestAppend = System.currentTimeMillis();

  private final ExecutableEventFinder executableEventFinder;
  private final ExecutionFactory executionFactory;

  private BlockchainLedger(
      NetworkConfig networkConfig,
      BlockchainStorage storage,
      BlockWithProposal latest,
      KeyPair signer,
      ExecutableEventFinder executableEventFinder,
      Consumer<AdditionalStatusProvider> registerProvider,
      ExecutionFactory executionFactory) {
    this(
        storage,
        latest,
        incomingQueue ->
            new NetworkNode(
                networkConfig,
                latest.latest().finalState().getExecutedState().getChainId(),
                latest.latest().finalState().getExecutedState().getSubChainId(),
                signer,
                incomingQueue,
                registerProvider,
                executionFactory),
        executableEventFinder,
        registerProvider,
        executionFactory);
  }

  private BlockchainLedger(
      BlockchainStorage storage,
      BlockWithProposal latest,
      Function<Consumer<NetworkNode.IncomingPacket>, BlockChainNetwork> networkCreator,
      ExecutableEventFinder executableEventFinder,
      Consumer<AdditionalStatusProvider> registerProvider,
      ExecutionFactory executionFactory) {
    BlockAndStateWithParent latestBlock = latest.latest();
    final String chainId = latestBlock.finalState().getExecutedState().getChainId();
    final String subChainId = latestBlock.finalState().getExecutedState().getSubChainId();
    this.executionFactory = executionFactory;
    finalizationExecutor =
        this.executionFactory.newSingleThreadExecutor(ledgerFinalizationThreadName(subChainId));
    this.blockRequester = new BlockRequester();
    NetworkRequestHandler networkHandler =
        new NetworkRequestHandler(
            blockRequester, this, chainId, nullSafeShard(subChainId), executionFactory);
    this.blockChainNetwork = networkCreator.apply(networkHandler::newPacket);
    blockRequester.init(blockChainNetwork::sendToAny);
    this.blockchainStorage = storage;
    ArrayList<BlockAndState> blockProposals = new ArrayList<>();
    if (latest.proposal() != null) {
      blockProposals.add(latest.proposal());
    }
    this.blockView = new BlockView(latestBlock, blockProposals);
    this.executableEventFinder = executableEventFinder;

    this.liveness =
        new LivenessMonitor(
            this::requestNextBlock,
            this::getLatestProductionTime,
            Duration.ofMinutes(2).toMillis());
    logger.info(
        "Booted blockchain latest block={}, chainId={}, subChainId={}",
        latestBlock.current().getBlock(),
        chainId,
        subChainId);
    registerProvider.accept(new LedgerStatusProvider(subChainId, this::getBlockTime));
  }

  static String ledgerFinalizationThreadName(String subChainId) {
    String nullSafeShard = nullSafeShard(subChainId);
    return "LedgerFinalization-" + nullSafeShard;
  }

  long getLatestProductionTime() {
    return this.blockView.latestBlock().current().getBlockProductionTime();
  }

  static BlockchainLedger createForTest(
      RootDirectory rootDataDirectory,
      Consumer<MutableChainState> genesisStateBuilder,
      BlockChainNetwork network,
      ExecutableEventFinder finder) {
    return createShardForTest(
        rootDataDirectory,
        genesisStateBuilder,
        null,
        network,
        finder,
        FunctionUtility.noOpConsumer(),
        ExecutionFactoryThreaded.create());
  }

  static BlockchainLedger createForTest(
      RootDirectory rootDataDirectory,
      Consumer<MutableChainState> genesisStateBuilder,
      BlockChainNetwork network,
      ExecutableEventFinder finder,
      ExecutionFactory executionFactory) {
    return createShardForTest(
        rootDataDirectory,
        genesisStateBuilder,
        null,
        network,
        finder,
        FunctionUtility.noOpConsumer(),
        executionFactory);
  }

  static BlockchainLedger createForTest(
      RootDirectory rootDataDirectory,
      Consumer<MutableChainState> genesisStateBuilder,
      BlockChainNetwork network,
      ExecutableEventFinder finder,
      AdditionalStatusProviders additionalStatusProviders,
      ExecutionFactory executionFactory) {
    return createShardForTest(
        rootDataDirectory,
        genesisStateBuilder,
        null,
        network,
        finder,
        additionalStatusProviders::register,
        executionFactory);
  }

  static BlockchainLedger createShardForTest(
      RootDirectory rootDataDirectory,
      Consumer<MutableChainState> genesisStateBuilder,
      String shardId,
      BlockChainNetwork network,
      ExecutableEventFinder finder,
      Consumer<AdditionalStatusProvider> register,
      ExecutionFactory executionFactory) {
    String chainId = UUID.randomUUID().toString();
    BlockchainStorage storage = new BlockchainStorage(rootDataDirectory, chainId);
    return new BlockchainLedger(
        storage,
        buildAndSaveGenesis(chainId, shardId, genesisStateBuilder, storage),
        packetConsumer -> network,
        finder,
        register,
        executionFactory);
  }

  private static BlockAndState createAndStoreGenesisBlock(
      BlockchainStorage storage, ImmutableChainState initial) {
    Block block = Block.createGenesis(initial.getHash());
    FinalBlock finalBlock = new FinalBlock(block, new byte[0]);
    storage.registerAcceptedBlock(finalBlock, List.of(), initial, Map.of());
    return new BlockAndState(finalBlock, initial, fromInitial(initial));
  }

  private static UpdateEvent fromInitial(ImmutableChainState initial) {
    return new UpdateEvent(Set.copyOf(initial.getContracts()), Set.of(), Set.of());
  }

  /**
   * Creates a blockchain using the supplied storage directory. If the directory is empty a new
   * chain will be initialized using the supplied initial state. Will always look up in local
   * storage for events.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesis file containing genesis data
   * @param signer the key pair used for TLS handshake
   * @return blockchain created
   */
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig, RootDirectory rootDirectory, String genesis, KeyPair signer) {
    return createBlockChain(networkConfig, rootDirectory, genesis, signer, null);
  }

  /**
   * Creates a blockchain with a random key pair. Will always look up in local storage for events.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesisFile file containing genesis data
   * @return blockchain created
   */
  @SuppressWarnings("WeakerAccess")
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig, RootDirectory rootDirectory, String genesisFile) {
    KeyPair signer = new KeyPair();
    return createBlockChain(networkConfig, rootDirectory, genesisFile, signer, null);
  }

  /**
   * Creates a blockchain using the supplied storage directory. If the directory is empty a new
   * chain will be initialized using the supplied initial state.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesis file containing genesis data
   * @param signer the key pair used for TLS handshake
   * @param executableEventFinder function to find executable event on a shard.
   * @param registerProviders register status providers
   * @return blockchain created
   */
  @SuppressWarnings("WeakerAccess")
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig,
      RootDirectory rootDirectory,
      String genesis,
      KeyPair signer,
      ExecutableEventFinder executableEventFinder,
      Consumer<AdditionalStatusProvider> registerProviders) {
    BlockStateStorage blockStorage = new BlockStateStorage(rootDirectory);
    BlockAndStateWithParent latestBlock = validBlockFromStorage(blockStorage);
    BlockchainStorage storage;
    if (latestBlock == null) {
      logger.debug("Reading blockchain from genesis");
      // The blockchain needs to boot from a genesis block on disk
      // nosemgrep
      File genesisFile = new File(genesis);
      GenesisLoadResult result = loadGenesisFromFile(rootDirectory, blockStorage, genesisFile);
      storage = result.storage();
      latestBlock = result.block();
    } else {
      storage =
          new BlockchainStorage(
              blockStorage,
              rootDirectory,
              latestBlock.currentState().getExecutedState().getChainId());
    }
    if (executableEventFinder == null) {
      logger.warn("Creating a blockchain without shards");
      executableEventFinder = (unused, hash) -> storage.getEventTransaction(hash);
    }
    return new BlockchainLedger(
        networkConfig,
        storage,
        patchFromStorage(storage, latestBlock),
        signer,
        executableEventFinder,
        registerProviders,
        ExecutionFactoryThreaded.create());
  }

  /**
   * Creates a blockchain using the supplied storage directory. If the directory is empty a new
   * chain will be initialized using the supplied initial state.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesis file containing genesis data
   * @param signer the key pair used for TLS handshake
   * @param executableEventFinder function to find executable event on a shard.
   * @return blockchain created
   */
  @SuppressWarnings("WeakerAccess")
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig,
      RootDirectory rootDirectory,
      String genesis,
      KeyPair signer,
      ExecutableEventFinder executableEventFinder) {
    return createBlockChain(
        networkConfig, rootDirectory, genesis, signer, executableEventFinder, ignored -> {});
  }

  /**
   * Creates a blockchain with a random key pair.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesisFile file containing genesis data
   * @param executableEventFinder function to find executable event on a shard.
   * @return blockchain created
   */
  @SuppressWarnings("WeakerAccess")
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig,
      RootDirectory rootDirectory,
      String genesisFile,
      ExecutableEventFinder executableEventFinder) {
    KeyPair signer = new KeyPair();
    return createBlockChain(
        networkConfig, rootDirectory, genesisFile, signer, executableEventFinder);
  }

  /**
   * Creates a blockchain with a random key pair.
   *
   * @param networkConfig configuration for network
   * @param rootDirectory storage directory
   * @param genesisFile file containing genesis data
   * @param executableEventFinder function to find executable event on a shard.
   * @param registerStatusProvider register additional status provider
   * @return blockchain created
   */
  @SuppressWarnings("WeakerAccess")
  public static BlockchainLedger createBlockChain(
      NetworkConfig networkConfig,
      RootDirectory rootDirectory,
      String genesisFile,
      ExecutableEventFinder executableEventFinder,
      Consumer<AdditionalStatusProvider> registerStatusProvider) {
    KeyPair signer = new KeyPair();
    return createBlockChain(
        networkConfig,
        rootDirectory,
        genesisFile,
        signer,
        executableEventFinder,
        registerStatusProvider);
  }

  private static BlockAndStateWithParent validBlockFromStorage(BlockStateStorage blockStorage) {
    BlockAndStateWithParent withParent = loadBlockFromStorage(blockStorage, Long.MAX_VALUE);
    if (withParent != null) {
      return ensureFinalBlock(blockStorage, withParent);
    } else {
      return null;
    }
  }

  private static BlockAndStateWithParent ensureFinalBlock(
      BlockStateStorage blockStorage, BlockAndStateWithParent withParent) {
    if (withParent.currentBlock().getParentBlock().equals(Block.GENESIS_PARENT)) {
      return withParent;
    }
    BlockValidation blockValidation =
        withParent
            .finalState()
            .getConsensusPlugin()
            .validateLocalBlock(withParent.current().getFinalBlock());
    if (blockValidation.canRollback()) {
      return loadBlockFromStorage(blockStorage, withParent.currentBlock().getBlockTime() - 1);
    } else {
      return withParent;
    }
  }

  private static BlockAndStateWithParent loadBlockFromStorage(
      BlockStateStorage blockStorage, long maximalBlockTime) {
    ValidBlock validBlock = blockStorage.loadLatestValidBlock(maximalBlockTime);
    if (validBlock != null) {
      logger.debug("Create blockchain from state");
      ChainStateCache stateCache = new ChainStateCache(blockStorage.getStateStorage());
      ImmutableChainState state = new ImmutableChainState(stateCache, validBlock.getState());
      ImmutableChainState finalizedState =
          new ImmutableChainState(stateCache, validBlock.getFinalizedState());
      BlockAndStateWithParent withParent =
          new BlockAndStateWithParent(
              new BlockAndState(validBlock.getBlock(), state, fromInitial(state)),
              finalizedState,
              null);
      return withParent;
    } else {
      return null;
    }
  }

  /**
   * Load genesis from genesis file.
   *
   * @param root the directory to use as BlockchainStorage
   * @param blockStorage the storage of blocks and state
   * @param file the file to load genesis from
   * @return the loaded genesis and an initialized BlockchainStorage
   */
  private static GenesisLoadResult loadGenesisFromFile(
      RootDirectory root, BlockStateStorage blockStorage, File file) {

    StateStorageRaw stateStorage = blockStorage.getStateStorage();
    ChainStateCache context = new ChainStateCache(stateStorage);

    GenesisFile genesisFile = new GenesisFile(file);
    genesisFile.populateStateStorage(stateStorage);

    FinalBlock finalBlock = genesisFile.readGenesisBlockFromZip();
    Block block = finalBlock.getBlock();

    ImmutableChainState state =
        new ImmutableChainState(context, blockStorage.getState(block.getState()));

    String chainId = state.getExecutedState().getChainId();
    BlockchainStorage blockchainStorage = new BlockchainStorage(blockStorage, root, chainId);

    BlockAndState latestBlock = new BlockAndState(finalBlock, state, fromInitial(state));

    BlockAndStateWithParent newBlock;
    if (genesisFile.supportsTransactions()) {
      List<SignedTransaction> transactions =
          genesisFile.readTransactionsFromGenesisZip(chainId, block.getTransactions());
      List<ExecutableEvent> events =
          genesisFile.readEventsFromGenesisZip(block.getEventTransactions());
      ExecutableBlock executableBlock = new ExecutableBlock(finalBlock, transactions, events);
      newBlock =
          appendValidBlock(blockchainStorage, latestBlock, executableBlock, new ArrayList<>());
    } else {
      blockStorage.registerAcceptedBlock(finalBlock, state);
      newBlock = new BlockAndStateWithParent(latestBlock, latestBlock.state, null);
    }

    return new GenesisLoadResult(blockchainStorage, newBlock);
  }

  /**
   * Genesis loaded from genesis file.
   *
   * @param storage the blockchain storage
   * @param block the genesis block
   */
  record GenesisLoadResult(BlockchainStorage storage, BlockAndStateWithParent block) {}

  private static BlockWithProposal buildAndSaveGenesis(
      String chainId,
      String subChainId,
      Consumer<MutableChainState> defaultState,
      BlockchainStorage storage) {
    BlockAndState latestBlock;
    ChainStateCache context = new ChainStateCache(storage.getStateStorage());
    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    defaultState.accept(genesisState);
    latestBlock =
        createAndStoreGenesisBlock(
            storage,
            genesisState.asImmutable(chainId, subChainId, AvlTree.create(), AvlTree.create()));
    return new BlockWithProposal(
        new BlockAndStateWithParent(latestBlock, latestBlock.state, latestBlock.contracts));
  }

  private static BlockWithProposal patchFromStorage(
      BlockchainStorage blockchainStorage, BlockAndStateWithParent latestBlock) {
    logger.info(
        "Trying to patch ledger from local storage. BlockTime={}",
        latestBlock.current().getBlockTime());
    BlockWithProposal afterPatching = appendBlocksFromStorage(blockchainStorage, latestBlock);
    logger.info(
        "Finished patching ledger from local storage. BlockTime={}",
        afterPatching.latest().currentBlock().getBlockTime());
    return afterPatching;
  }

  private static BlockWithProposal appendBlocksFromStorage(
      BlockchainStorage storage, BlockAndStateWithParent latestBlock) {
    ExecutableBlock next;
    long blockTime = latestBlock.current().getBlockTime() + 1;
    BlockAndStateWithParent previous = null;
    ExecutableBlock previousBlock = null;
    while ((next = storage.loadExecutableBlock(blockTime)) != null) {
      logger.info("Appending executable block from storage {}", next.getBlock().getBlock());
      previous = latestBlock;
      previousBlock = next;
      latestBlock = appendValidBlock(storage, latestBlock.current(), next, new ArrayList<>());
      blockTime++;
    }
    boolean isLatestProposal =
        previous != null
            && previous
                .currentState()
                .getConsensusPlugin()
                .validateLocalBlock(previousBlock.getBlock())
                .canRollback();
    if (isLatestProposal) {
      return new BlockWithProposal(previous, latestBlock.current());
    } else {
      return new BlockWithProposal(latestBlock);
    }
  }

  /**
   * Finds executable block from compressed block pair.
   *
   * @param blockPairCompressed to find executable block from
   * @return executable block
   */
  public ExecutableBlock getExecutableBlock(
      BlockResponseSemiCompressed.BlockPairSemiCompressed blockPairCompressed) {
    List<ExecutableEvent> events =
        blockPairCompressed.getSemiCompressedEvents().stream()
            .map(this::findExecutableEvent)
            .collect(Collectors.toList());
    return new ExecutableBlock(
        blockPairCompressed.getBlock(), blockPairCompressed.getTransactions(), events);
  }

  ExecutableEvent findExecutableEvent(SemiCompressedEvent event) {
    if (event.isCompressed()) {
      SemiCompressedEvent.IdentifierAndShard identifierAndShard = event.getIdentifierAndShard();
      return executableEventFinder.find(
          identifierAndShard.shard(), identifierAndShard.identifier());
    } else {
      return event.getExecutableEvent();
    }
  }

  /**
   * Append a new block to the blockchain.
   *
   * @param finalBlock the final block to append
   */
  @SuppressWarnings("WeakerAccess")
  public synchronized void appendBlock(FinalBlock finalBlock) {
    if (isHistoricBlock(finalBlock)) {
      return;
    }
    Block block = finalBlock.getBlock();

    ExecutableBlock executableBlock =
        new ExecutableBlock(
            finalBlock,
            block.getTransactions().stream().map(pending::get).collect(Collectors.toList()),
            block.getEventTransactions().stream()
                .map(pendingEvents::get)
                .collect(Collectors.toList()));
    if (executableBlock.isMissingTransactionOrEvents()) {
      logger.info("Missing transaction/events for {}", executableBlock);
      blockRequester.requestBlock(block.getBlockTime());
      return;
    }
    appendExecutableBlock(executableBlock);
  }

  synchronized void appendBlock(ExecutableBlock executableBlock) {
    if (isHistoricBlock(executableBlock.getBlock())) {
      return;
    }
    if (executableBlock.isMissingTransactionOrEvents()) {
      logger.info("Received incomplete executable block: {}", executableBlock);
      throw new RuntimeException("Received incomplete executable block");
    }
    appendExecutableBlock(executableBlock);
  }

  private void appendExecutableBlock(ExecutableBlock executableBlock) {
    final FinalBlock finalBlock = executableBlock.getBlock();
    final Block block = finalBlock.getBlock();
    BlockAndStateWithParent currentLatestBlock = this.blockView.latestBlock();
    try {
      if (finalBlock.getBlock().getBlockTime() == getBlockTime() + 2) {
        for (int i = 0; i < blockView.blockProposals().size(); i++) {
          BlockAndState blockProposal = blockView.blockProposals().get(i);
          BlockValidation blockValidation = isBlockValid(blockProposal, executableBlock);
          if (blockValidation.isAccepted()) {
            BlockAndStateWithParent blockAndState =
                floodAndAppendValidBlock(blockProposal, executableBlock);
            ImmutableChainState currentState = currentLatestBlock.currentState();
            BlockAndStateWithParent latestBlock =
                new BlockAndStateWithParent(
                    blockProposal, currentState, currentLatestBlock.contracts());
            boolean nextIsFinal = !blockValidation.canRollback();
            List<BlockAndState> newProposals = new ArrayList<>();
            if (nextIsFinal) {
              latestBlock = blockAndState;
            } else {
              newProposals.add(blockAndState.current());
            }
            this.blockView = new BlockView(latestBlock, newProposals);

            notifyListeners(
                listener -> {
                  notifyFinalBlock(
                      listener,
                      blockProposal,
                      currentState,
                      currentLatestBlock.current().getContracts());
                  listener.newBlockProposal(blockAndState.currentBlock());
                  if (nextIsFinal) {
                    notifyFinalBlock(listener, blockAndState);
                  }
                });
            cleanPending();
            return;
          }
        }
      }
      long nextBlockTime = getBlockTime() + 1;
      if (block.getBlockTime() > nextBlockTime) {
        logger.debug("Requesting block in between {} and {}", nextBlockTime, block);
        requestNextBlock();
      } else { // block.getBlockTime() == nextBlockTime
        BlockAndState current = currentLatestBlock.current();
        BlockValidation blockValidation = isBlockValid(current, executableBlock);
        if (!blockValidation.isAccepted()) {
          logger.warn("Invalid block received {}", block);
          throw new RuntimeException("Invalid block received " + block);
        }
        BlockAndStateWithParent blockAndState = floodAndAppendValidBlock(current, executableBlock);

        Block blockToAppend = finalBlock.getBlock();
        if (!blockValidation.canRollback()) {
          this.blockView = new BlockView(blockAndState, List.of());
          notifyListeners(
              listener -> {
                listener.newBlockProposal(blockToAppend);
                notifyFinalBlock(listener, blockAndState);
              });
        } else {
          logger.debug(
              "Add block time {} to block proposals", blockAndState.current().getBlockTime());
          List<BlockAndState> proposals = new ArrayList<>(blockView.blockProposals());
          proposals.add(blockAndState.current());
          this.blockView = new BlockView(blockView.latestBlock(), proposals);
          notifyListeners(listener -> listener.newBlockProposal(blockToAppend));
        }
        cleanPending();
      }
    } catch (RuntimeException e) {
      logger.warn("Cannot append block: " + finalBlock.getBlock(), e);
      throw e;
    }
  }

  private boolean isHistoricBlock(FinalBlock finalBlock) {
    Block block = finalBlock.getBlock();
    if (block.getBlockTime() <= getBlockTime()) {
      logger.debug("The block was from the past {}", block);
      return true;
    }
    return isAlreadyProposed(finalBlock);
  }

  private void notifyFinalBlock(Listener listener, BlockAndStateWithParent blockAndState) {
    notifyFinalBlock(
        listener, blockAndState.current(), blockAndState.finalState(), blockAndState.contracts());
  }

  private void notifyFinalBlock(
      Listener listener,
      BlockAndState finalBlock,
      ImmutableChainState finalState,
      UpdateEvent updateEvent) {
    listener.newFinalBlock(finalBlock.getBlock(), finalBlock.getState());
    listener.newFinalState(finalState, updateEvent);
  }

  private void requestNextBlock() {
    blockRequester.requestBlock(getBlockTime() + 1);
  }

  private BlockAndStateWithParent floodAndAppendValidBlock(
      BlockAndState parentBlock, ExecutableBlock executableBlock) {
    FinalBlock finalBlock = executableBlock.getBlock();
    logger.debug("Flood and append {}", finalBlock.getBlock());
    blockChainNetwork.sendToAll(new Packet<>(Packet.Type.BLOCK, finalBlock));
    List<ExecutableEvent> nonExecutedLocalEvents = new ArrayList<>();
    BlockAndStateWithParent result =
        appendValidBlock(blockchainStorage, parentBlock, executableBlock, nonExecutedLocalEvents);
    nonExecutedLocalEvents.forEach(event -> pendingEvents.put(event.identifier(), event));
    setLatestAppend(System.currentTimeMillis());
    return result;
  }

  void setLatestAppend(long value) {
    this.latestAppend = value;
  }

  /**
   * Get the timestamp when the last block was finished executing. If a block is currently being
   * executed this method will block until the execution has finished.
   *
   * @return the timestamp for finished execution
   */
  public synchronized long getLatestAppend() {
    return latestAppend;
  }

  private boolean isAlreadyProposed(FinalBlock finalBlock) {
    for (BlockAndState proposal : blockView.blockProposals()) {
      if (proposal.getBlock().equals(finalBlock.getBlock())) {
        return true;
      }
    }
    return false;
  }

  private void cleanPending() {
    pending
        .values()
        .removeIf(
            t -> {
              if (!isTransactionValid(t)) {
                logger.debug("Cleaning invalid pending transaction {}", t);
                return true;
              } else {
                return false;
              }
            });
    pendingEvents.values().removeIf(this::isEventOutdated);
  }

  private static BlockAndStateWithParent appendValidBlock(
      BlockchainStorage blockchainStorage,
      BlockAndState latestBlock,
      ExecutableBlock executableBlock,
      List<ExecutableEvent> nonExecutedLocalEvents) {
    final FinalBlock finalBlock = executableBlock.getBlock();
    final Block block = finalBlock.getBlock();
    MutableChainState nextState = latestBlock.getState().asMutable();

    logger.info("Executing {}", executableBlock);

    String chainId = latestBlock.state.getExecutedState().getChainId();
    String subChainId = latestBlock.state.getExecutedState().getSubChainId();

    TransactionProcessor transactionProcessor =
        new TransactionProcessor(block, nextState, chainId, subChainId);

    nextState.updateForBlock(block);

    for (SignedTransaction transaction : executableBlock.getTransactions()) {
      transactionProcessor.processTransaction(transaction);
    }

    for (ExecutableEvent event : executableBlock.getEventTransactions()) {
      if (nextState.isSyncing()) {
        transactionProcessor.processTransactionInSync(event);
      } else {
        transactionProcessor.processTransaction(event);
      }
    }
    transactionProcessor.processLocalEvents(
        event -> {
          ShardNonces shardNonce = nextState.getShardNonce(subChainId);
          return executeInline(nextState.isSyncing(), shardNonce, event.getEvent().getNonce());
        });

    nonExecutedLocalEvents.addAll(transactionProcessor.localEvents());

    List<ExecutedTransaction> executedTransactions = new ArrayList<>();
    ExecutedStateBuilder executedStateBuilder = new ExecutedStateBuilder();
    Map<Hash, FailureCause> failures = new HashMap<>();
    Map<Hash, TransactionCost> cost = new HashMap<>();
    for (ExecutedTransactionEvents result : transactionProcessor.resultingEvents()) {
      result.storeEvents(blockchainStorage);

      Hash transactionIdentifier = result.transaction().identifier();

      ExecutedTransaction executedTransaction =
          ExecutedTransaction.create(
              block.identifier(),
              result.transaction(),
              result.isSuccess(),
              FixedList.create(result.events().stream().map(ExecutableEvent::identifier)));
      executedTransactions.add(executedTransaction);
      executedStateBuilder.add(executedTransaction);
      if (!result.isSuccess()) {
        failures.put(transactionIdentifier, result.exception());
      }

      if (result.cost() != null) {
        cost.put(transactionIdentifier, result.cost());
      }
    }

    ImmutableChainState state =
        nextState.asImmutable(
            chainId,
            subChainId,
            executedStateBuilder.spawnedEvents,
            executedStateBuilder.executionStatus);
    blockchainStorage.registerAcceptedBlock(
        finalBlock, executedTransactions, state, failures, cost);
    return new BlockAndStateWithParent(
        new BlockAndState(finalBlock, state, nextState.resultingUpdateEvent()),
        latestBlock.getState(),
        latestBlock.contracts);
  }

  static boolean executeInline(boolean syncing, ShardNonces shardNonce, long nonce) {
    boolean nextShard = shardNonce.getInbound() == nonce;
    return !syncing && nextShard;
  }

  /**
   * Get the id of this chain.
   *
   * @return chain id
   */
  public String getChainId() {
    return getChainState().getExecutedState().getChainId();
  }

  /**
   * Get the error message and stack trace if an exception occured.
   *
   * @param transaction the hash of the transaction
   * @return an object containing an error message and a stack trace.
   */
  public FailureCause getTransactionFailure(Hash transaction) {
    return blockchainStorage.getTransactionFailure(transaction);
  }

  /**
   * Get cost of a transaction.
   *
   * @param transaction the transaction identifier
   * @return transaction cost of the given transaction
   */
  public TransactionCost getTransactionCost(Hash transaction) {
    return blockchainStorage.getTransactionCost(transaction);
  }

  private static final class ExecutedStateBuilder {

    private AvlTree<Hash, Hash> spawnedEvents = AvlTree.create();
    private AvlTree<Hash, Boolean> executionStatus = AvlTree.create();

    void add(ExecutedTransaction executedTransaction) {
      Hash spawningTransaction = executedTransaction.getInner().identifier();
      for (Hash event : executedTransaction.getEvents()) {
        spawnedEvents = spawnedEvents.set(event, spawningTransaction);
      }
      executionStatus =
          executionStatus.set(spawningTransaction, executedTransaction.didExecutionSucceed());
    }
  }

  private void notifyListeners(Consumer<Listener> notifier) {
    listeners.forEach(listener -> listener.notify(notifier));
  }

  /**
   * Adds a listener to the "new block" event.
   *
   * @param listeners listeners to add
   */
  @SuppressWarnings("WeakerAccess")
  public synchronized void attach(Listener... listeners) {
    for (Listener listener : listeners) {
      logger.info("Attached listener={} on this blockchain", listener);
      BlockchainListener blockchainListener = new BlockchainListener(listener, executionFactory);
      this.listeners.add(blockchainListener);
    }
  }

  /**
   * Attach a finalization listener.
   *
   * @param listener to be attached
   */
  @SuppressWarnings("WeakerAccess")
  public void attachFinalizationListener(FinalizationListener listener) {
    this.finalizationListeners.add(listener);
  }

  /**
   * Removes a listener to the "new block" event.
   *
   * @param listener the listener to detach
   */
  public void detach(Listener listener) {
    this.listeners.removeIf(blockchainListener -> blockchainListener.listener == listener);
  }

  private static BlockValidation isBlockValid(BlockAndState latest, ExecutableBlock nextBlock) {
    FinalBlock finalBlock = nextBlock.getBlock();
    Block block = finalBlock.getBlock();
    ImmutableChainState latestChainState = latest.getState();
    List<SignedTransaction> transactions = nextBlock.getTransactions();
    List<ExecutableEvent> eventTransactions = nextBlock.getEventTransactions();
    boolean transactionsAndEventsValid =
        areTransactionsAndEventsValid(block, latestChainState, transactions, eventTransactions);
    boolean valid = isBlockStructurallyValid(latest, block) && transactionsAndEventsValid;
    if (valid) {
      ConsensusPluginAccessor consensusPlugin = latestChainState.getConsensusPlugin();
      return consensusPlugin.validateLocalBlock(finalBlock);
    } else {
      return BlockValidation.createRejected();
    }
  }

  /**
   * Check that the supplied transactions and events are valid in the given context.
   *
   * @param block the state in which the transactions are included
   * @param latestChainState the state the block is to be executed on
   * @param transactions the transactions to verify
   * @param eventTransactions the events to verify
   * @return true if all events and transactions are valid
   */
  public static boolean areTransactionsAndEventsValid(
      Block block,
      ImmutableChainState latestChainState,
      List<SignedTransaction> transactions,
      List<ExecutableEvent> eventTransactions) {
    long productionTime = block.getProductionTime();
    boolean syncing = latestChainState.isSyncing();
    boolean transactionsValid;
    if (syncing) {
      transactionsValid = transactions.isEmpty();
    } else {
      transactionsValid =
          transactions.stream()
              .allMatch(t -> latestChainState.isTransactionValidInState(t, productionTime));
    }
    boolean eventsValid =
        areAllEventsValid(
            syncing,
            latestChainState.getShardNonces(),
            eventTransactions,
            latestChainState.getGovernanceVersion());
    return transactionsValid && eventsValid;
  }

  static boolean areAllEventsValid(
      boolean syncing,
      AvlTree<ShardId, ShardNonces> shardNonces,
      List<ExecutableEvent> eventTransactions,
      long maximumGovernanceVersion) {
    Map<String, Long> nextExpectedNonce = new HashMap<>();
    Map<String, Long> nextMinimalFinalMaster = new HashMap<>();
    Map<String, Boolean> missingSync = new HashMap<>();

    for (ExecutableEvent eventTransaction : eventTransactions) {
      long nextNonce =
          nextExpectedNonce.computeIfAbsent(
              eventTransaction.getOriginShard(),
              shard ->
                  Objects.requireNonNullElse(
                          shardNonces.getValue(new ShardId(shard)), ShardNonces.EMPTY)
                      .getInbound());
      long minimalCommitteeId =
          nextMinimalFinalMaster.computeIfAbsent(
              eventTransaction.getOriginShard(),
              shard ->
                  Objects.requireNonNullElse(
                          shardNonces.getValue(new ShardId(shard)), ShardNonces.EMPTY)
                      .getLatestInboundCommitteeId());
      EventTransaction event = eventTransaction.getEvent();
      boolean valid =
          event.getCommitteeId() >= minimalCommitteeId
              && event.getGovernanceVersion() <= maximumGovernanceVersion
              && event.getNonce() == nextNonce;
      boolean isSync = event.getInner().getEventType() == EventTransaction.EventType.SYNC;
      if (syncing) {
        // When syncing all events are allowed from shards not yet fully synced
        valid &=
            missingSync.computeIfAbsent(
                eventTransaction.getOriginShard(),
                shard ->
                    Objects.requireNonNullElse(
                            shardNonces.getValue(new ShardId(shard)), ShardNonces.EMPTY)
                        .isMissingSync());
        missingSync.put(eventTransaction.getOriginShard(), !isSync);
      } else {
        // Do not allow sync when not syncing
        valid &= !isSync;
      }
      if (!valid) {
        logger.warn(
            "Received block containing invalid event."
                + " expectedNonce={},"
                + " actualNonce={},"
                + " minimalCommitteeId={},"
                + " actualCommitteeId={},"
                + " maximumGovernanceVersion={},"
                + " actualGovernanceVersion={},"
                + " transaction={},"
                + " isSync={},",
            nextNonce,
            event.getNonce(),
            minimalCommitteeId,
            event.getCommitteeId(),
            maximumGovernanceVersion,
            event.getGovernanceVersion(),
            eventTransaction,
            isSync);
        return false;
      }
      nextMinimalFinalMaster.put(eventTransaction.getOriginShard(), event.getCommitteeId());
      nextExpectedNonce.put(eventTransaction.getOriginShard(), nextNonce + 1);
    }
    return true;
  }

  /**
   * Determine if the supplied block is structurally valid in the current BlockAndState. It is
   * verified that the current block and state is indeed the parent of nextBlock. This check does
   * not do any checks against the consensus plugin and does not check validity of contained
   * transactions.
   *
   * @param current the state to test validity in
   * @param nextBlock the next block
   * @return true if the block is structurally valid
   */
  @SuppressWarnings("WeakerAccess")
  public static boolean isBlockStructurallyValid(BlockAndState current, Block nextBlock) {
    ImmutableChainState latestChainState = current.getState();
    Hash stateIdentifier = latestChainState.getHash();
    Hash blockIdentifier = current.getBlock().identifier();
    boolean validProductionTime =
        verifyProductionTime(
            nextBlock.getProductionTime(),
            current.getBlockProductionTime(),
            latestAllowedProductionTime(System::currentTimeMillis));
    long currentCommitteeId = current.getBlock().getCommitteeId();
    long nextCommitteeId = nextBlock.getCommitteeId();
    logger.debug(
        "LatestBlockHash: {}={}, "
            + "latestState: {}={}, "
            + "validProductionTime={}, "
            + "committeeId: {}<={}",
        blockIdentifier,
        nextBlock.getParentBlock(),
        stateIdentifier,
        nextBlock.getState(),
        validProductionTime,
        currentCommitteeId,
        nextCommitteeId);
    return blockIdentifier.equals(nextBlock.getParentBlock())
        && currentCommitteeId <= nextCommitteeId
        && stateIdentifier.equals(nextBlock.getState())
        && validProductionTime;
  }

  static long latestAllowedProductionTime(LongSupplier currentTime) {
    return currentTime.getAsLong() + MAX_PRODUCTION_TIME_DRIFT;
  }

  static boolean verifyProductionTime(long productionTime, long minimum, long maximum) {
    return productionTime > minimum && productionTime < maximum;
  }

  boolean isTransactionValid(SignedTransaction transaction) {
    BlockView view = blockView;
    boolean validInFinal = isTransactionValidInState(transaction, view.latestBlock().current());
    if (validInFinal) {
      return true;
    } else {
      return view.blockProposals().stream()
          .anyMatch(proposal -> isTransactionValidInState(transaction, proposal));
    }
  }

  private boolean isTransactionValidInState(
      SignedTransaction transaction, BlockAndState blockAndState) {
    return blockAndState
        .getState()
        .isTransactionValidInState(transaction, blockAndState.getBlockProductionTime());
  }

  /**
   * Add a signed transaction to be included in a future block.
   *
   * @param transaction the transaction to add
   * @return true if the transactions was added, false if it is not valid
   */
  @SuppressWarnings("WeakerAccess")
  public boolean addPendingTransaction(SignedTransaction transaction) {
    if (isTransactionValid(transaction)) {
      SignedTransaction existing = pending.get(transaction.identifier());
      if (existing == null) {
        // Compute if absent to ensure that multiple threads are not all adding to the map and that
        // the message is thus only flooded once.
        pending.computeIfAbsent(
            transaction.identifier(),
            ignored -> {
              blockChainNetwork.sendToAll(new Packet<>(Packet.Type.TRANSACTION, transaction));
              return transaction;
            });
        notifyListeners(listener -> listener.newPendingTransaction(transaction));
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Add an event transaction to be included in a future block.
   *
   * @param compressed compressed event to add.
   * @return true if the event was added, false if it is not valid.
   */
  @SuppressWarnings("WeakerAccess")
  public boolean addPendingEvent(FloodableEventCompressed compressed) {
    return addPendingEvent(compressed.decompress(executableEventFinder));
  }

  /**
   * Add an event transaction to be included in a future block.
   *
   * @param event the event to add
   * @return true if the event was added, false if it is not valid
   */
  @SuppressWarnings("WeakerAccess")
  public boolean addPendingEvent(FloodableEvent event) {
    ExecutableEvent executableEvent = event.getExecutableEvent();

    if (pendingEvents.containsKey(executableEvent.identifier())) {
      return true;
    } else if (isEventValid(event, executableEvent)) {
      addValidPendingEvent(event, executableEvent);
      return true;
    } else {
      return false;
    }
  }

  private boolean isEventValid(FloodableEvent event, ExecutableEvent executableEvent) {
    String subChainId = getChainState().getExecutedState().getSubChainId();
    boolean eventDestinationIsCurrentShard =
        Objects.equals(executableEvent.getEvent().getDestinationShard(), subChainId);
    return eventDestinationIsCurrentShard
        && getChainId().equals(event.getChainId())
        && !isEventOutdated(executableEvent)
        && isValidBlockForShard(getChainState(), event.getBlock());
  }

  private void addValidPendingEvent(FloodableEvent event, ExecutableEvent executableEvent) {
    // Compute if absent to ensure that multiple threads are not all adding to the map and that
    // the message is thus only flooded once.
    pendingEvents.computeIfAbsent(
        executableEvent.identifier(),
        ignored -> {
          blockChainNetwork.sendToAll(new Packet<>(Packet.Type.EVENT, event));
          return executableEvent;
        });
    notifyListeners(listener -> listener.newPendingEvent(executableEvent));
  }

  private boolean isValidBlockForShard(ImmutableChainState state, FinalBlock block) {
    ConsensusPluginAccessor consensusPlugin = state.getConsensusPlugin();
    try {
      return consensusPlugin.validateExternalBlock(block);
    } catch (Exception exception) {
      logger.info("Error validating external event", exception);
      return false;
    }
  }

  private boolean isEventOutdated(ExecutableEvent executableEvent) {
    ShardNonces value =
        Objects.requireNonNullElse(
            getChainState()
                .getShardNonces()
                .getValue(new ShardId(executableEvent.getOriginShard())),
            ShardNonces.EMPTY);
    return value.getInbound() > executableEvent.getEvent().getNonce();
  }

  /**
   * Get proposed blocks and states.
   *
   * @return list of proposed blocks and states
   */
  @SuppressWarnings("WeakerAccess")
  public synchronized List<BlockAndState> getProposals() {
    return List.copyOf(blockView.blockProposals());
  }

  /**
   * Get a list of the possible heads in the blockchain. That is the current finalized block and all
   * active proposals.
   *
   * @return the blocks with their corresponding finalized state
   */
  @SuppressWarnings("WeakerAccess")
  public synchronized List<BlockAndStateWithParent> getPossibleHeads() {
    List<BlockAndStateWithParent> possibleHeads =
        blockView.blockProposals().stream()
            .map(
                blockAndState ->
                    new BlockAndStateWithParent(
                        blockAndState,
                        blockView.latestBlock().currentState(),
                        blockAndState.contracts))
            .collect(Collectors.toCollection(ArrayList::new));
    possibleHeads.add(blockView.latestBlock());
    return possibleHeads;
  }

  /**
   * Get the latest block.
   *
   * @return latest block
   */
  @SuppressWarnings("WeakerAccess")
  public Block getLatestBlock() {
    return blockView.latestBlock().currentBlock();
  }

  /**
   * Get the latest block and state.
   *
   * @return latest block and state
   */
  @SuppressWarnings("WeakerAccess")
  public BlockAndState latest() {
    return blockView.latestBlock().current();
  }

  /**
   * Get pending transactions with identifying hashes.
   *
   * @return avl tree from identifying hash to pending signed transaction
   */
  @SuppressWarnings("WeakerAccess")
  public Map<Hash, SignedTransaction> getPending() {
    return Collections.unmodifiableMap(pending);
  }

  /**
   * Get pending transactions.
   *
   * @return list of pending signed transactions
   */
  @SuppressWarnings("WeakerAccess")
  public List<SignedTransaction> getPendingTransactions() {
    return List.copyOf(pending.values());
  }

  /**
   * Get pending events.
   *
   * @return avl tree from identifying hash to executable event
   */
  @SuppressWarnings("WeakerAccess")
  public Map<Hash, ExecutableEvent> getPendingEvents() {
    return Map.copyOf(pendingEvents);
  }

  /**
   * Get current state for account.
   *
   * @param account account address
   * @return account state
   */
  @SuppressWarnings("WeakerAccess")
  public AccountState getAccountState(BlockchainAddress account) {
    return blockView.latestBlock().currentState().getAccount(account);
  }

  /**
   * Get a block by its hash.
   *
   * @param hash the identifier of the block
   * @return the block if found and final, null otherwise
   */
  public Block getBlock(Hash hash) {
    Block block = blockchainStorage.getBlock(hash);
    if (block == null) {
      return null;
    }
    Block latestBlock = getLatestBlock();
    if (block.getBlockTime() > latestBlock.getBlockTime()) {
      logger.warn(
          "Found block in storage that are not yet finalized. Block={}. LatestBlock={}",
          block,
          latestBlock);
      return null;
    }
    Hash blockHash = blockchainStorage.getBlockHash(block.getBlockTime());
    if (!block.identifier().equals(blockHash)) {
      logger.warn(
          "Found block in storage that does not match the one in block time index. Block={}",
          block);
      return null;
    }
    return block;
  }

  /**
   * Get a block by block time.
   *
   * @param blockTime the time to fetch block for
   * @return the block if found and final, null otherwise
   */
  public Block getBlock(long blockTime) {
    if (blockTime > blockView.latestBlock().currentBlock().getBlockTime()) {
      return null;
    } else {
      return blockchainStorage.getBlock(blockTime);
    }
  }

  /**
   * Get the chain state that was present after the execution of the block with the supplied block
   * time.
   *
   * @param blockTime the block time of the block
   * @return the chain state or null if the block time does not correspond to a known block
   */
  public ImmutableChainState getState(long blockTime) {
    BlockAndStateWithParent latest = blockView.latestBlock();
    if (latest.currentBlock().getBlockTime() == blockTime) {
      return latest.currentState();
    } else if (latest.currentBlock().getBlockTime() == blockTime + 1) {
      return latest.finalState();
    } else {
      Block parent = getBlock(blockTime + 1);
      if (parent == null) {
        return null;
      } else {
        ExecutedState executedState = blockchainStorage.getState(parent.getState());
        return new ImmutableChainState(
            latest.finalState().getContext(),
            Objects.requireNonNull(executedState, "State storage is incomplete"));
      }
    }
  }

  /**
   * Get all transactions for a block.
   *
   * @param hash the identifier of the block
   * @return the found transactions or null if block or transactions are not found
   */
  @SuppressWarnings("WeakerAccess")
  public List<ExecutedTransaction> getTransactionsForBlock(Hash hash) {
    Block block = getBlock(hash);
    if (block == null) {
      return null;
    }
    List<ExecutedTransaction> transactions = getTransactionsForBlock(block);
    if (transactions.contains(null)) {
      logger.warn("Encountered block with missing transactions in storage: {}", block);
      return null;
    }
    return transactions;
  }

  /**
   * Get transactions for a specified block. Allows fetching transactions for pending blocks.
   *
   * @param block the block to get transactions for
   * @return the transactions included in the block
   */
  List<ExecutedTransaction> getTransactionsForBlock(Block block) {
    return blockchainStorage.getTransactionsForBlock(block);
  }

  /**
   * Lookup a finalized executed transaction.
   *
   * @param transactionHash the identity of the transaction
   * @return the transaction or null if not found or not final
   */
  @SuppressWarnings("WeakerAccess")
  public ExecutedTransaction getTransaction(Hash transactionHash) {
    PossiblyFinalizedTransaction executedTransaction = getExecutedTransaction(transactionHash);
    if (executedTransaction == null) {
      return null;
    }
    if (!executedTransaction.finalized()) {
      logger.info(
          "Found transaction with non final block. Transaction={}",
          executedTransaction.transaction);
      return null;
    }
    return executedTransaction.transaction();
  }

  /**
   * Lookup a possible not finalized executed transaction.
   *
   * @param transactionHash the identity of the transaction
   * @return the executed transaction and boolean indicating finalization
   */
  public PossiblyFinalizedTransaction getExecutedTransaction(Hash transactionHash) {
    ExecutedTransaction transaction = blockchainStorage.getTransaction(transactionHash);
    if (transaction == null) {
      return null;
    }

    boolean inProposals = isInProposals(transaction.getBlockHash());
    if (inProposals) {
      return new PossiblyFinalizedTransaction(false, transaction);
    }
    Block block = getBlock(transaction.getBlockHash());
    if (block != null) {
      return new PossiblyFinalizedTransaction(true, transaction);
    }
    return null;
  }

  boolean isInProposals(Hash transactionHash) {
    return blockView.blockProposals().stream()
        .anyMatch(p -> p.getBlock().identifier().equals(transactionHash));
  }

  /**
   * Get an event that has either been spawned from this chain or executed in this chain.
   *
   * @param transactionHash the hash of the transaction
   * @return the executable event
   */
  public ExecutableEvent getEventTransaction(Hash transactionHash) {
    return blockchainStorage.getEventTransaction(transactionHash);
  }

  /**
   * Get the current chain state.
   *
   * @return current chain state
   */
  @SuppressWarnings("WeakerAccess")
  public ImmutableChainState getChainState() {
    return blockView.latestBlock().currentState();
  }

  /**
   * Get the raw state storage.
   *
   * @return raw state storage
   */
  public StateStorageRaw getStateStorage() {
    return blockchainStorage.getStateStorage();
  }

  /**
   * Get block time of the latest block.
   *
   * @return block time of the latest block
   */
  public long getBlockTime() {
    return blockView.latestBlock().currentBlock().getBlockTime();
  }

  @Override
  public void close() {
    close(blockChainNetwork, blockchainStorage, liveness);
  }

  private void close(AutoCloseable... closeable) {
    for (AutoCloseable autoCloseable : closeable) {
      ExceptionLogger.handle(
          logger::info, autoCloseable::close, "Unable to close blockchain network");
    }
  }

  /**
   * Get finalized block for a provided block time.
   *
   * @param blockTime provided block time to get block for
   * @return final block from provided block time
   */
  public FinalBlock getFinalizedBlock(long blockTime) {
    return blockchainStorage.getFinalizedBlock(blockTime);
  }

  /**
   * Send a finalization request to all known - same as flooding.
   *
   * @param data finalization data
   */
  @SuppressWarnings("WeakerAccess")
  public void floodFinalizationData(FinalizationData data) {
    blockChainNetwork.sendToAll(new Packet<>(Packet.Type.FINALIZATION, data));
  }

  /**
   * Send a finalization request to a random recipient.
   *
   * @param data finalization data
   */
  @SuppressWarnings("WeakerAccess")
  public void sendFinalizationRequest(FinalizationData data) {
    blockChainNetwork.sendToAny(new Packet<>(Packet.Type.FINALIZATION, data));
  }

  void incomingFinalizationData(Connection connection, FinalizationData data) {
    finalizationListeners.forEach(
        listener ->
            finalizationExecutor.execute(
                loggedHandling(() -> listener.incomingFinalizationData(connection, data))));
  }

  private Runnable loggedHandling(ThrowingRunnable runnable) {
    return () -> ExceptionLogger.handle(logger::warn, runnable, "Error in internal listener");
  }

  static String nullSafeShard(String shard) {
    return shard == null ? "Gov" : shard;
  }

  /** Listener for finalization data. */
  public interface FinalizationListener {

    /**
     * Listen on incoming finalization data.
     *
     * @param connection sender
     * @param data finalization data
     */
    void incomingFinalizationData(Connection connection, FinalizationData data);
  }

  /**
   * State listener, listens to all state changes. Meant to expose bloc changes and not contract
   * changes. In blockchain server, API for registering a contract listener is available.
   *
   * @see ServerModule.CreateContractListener
   */
  public interface Listener {

    /**
     * Listen on new final block.
     *
     * @param block new final block
     * @param state new state with final block
     */
    default void newFinalBlock(Block block, ImmutableChainState state) {}

    /**
     * Listen on new block proposal.
     *
     * @param block proposed new block
     */
    default void newBlockProposal(Block block) {}

    /**
     * Listen on new pending transaction.
     *
     * @param transaction new pending transaction
     */
    default void newPendingTransaction(SignedTransaction transaction) {}

    /**
     * Listen on new pending event.
     *
     * @param event new pending event
     */
    default void newPendingEvent(ExecutableEvent event) {}

    /**
     * Listen on new final state.
     *
     * @param state new final state
     */
    default void newFinalState(ImmutableChainState state) {}

    /**
     * Listen on new final state with an update event.
     *
     * @param state new final state
     * @param contracts update event
     */
    default void newFinalState(ImmutableChainState state, UpdateEvent contracts) {
      newFinalState(state);
    }
  }

  /**
   * The event information related to a state change. Contracts can either be created, updated or
   * removed, they are in each of the collections respectively.
   */
  public static final class UpdateEvent {

    /** New contracts. */
    public final Collection<BlockchainAddress> newContracts;

    /** Updated contracts. */
    public final Collection<BlockchainAddress> updatedContracts;

    /** Removed contract. */
    public final Collection<BlockchainAddress> removedContract;

    /**
     * Creates a new update event.
     *
     * @param newContracts the collection of created contracts
     * @param updatedContracts the collection of updated contracts
     * @param removedContract the collection of removed contracts
     */
    public UpdateEvent(
        Collection<BlockchainAddress> newContracts,
        Collection<BlockchainAddress> updatedContracts,
        Collection<BlockchainAddress> removedContract) {
      this.newContracts = newContracts;
      this.updatedContracts = updatedContracts;
      this.removedContract = removedContract;
    }
  }

  /** The result of executing a block. */
  public static final class BlockAndState {

    private final FinalBlock finalBlock;
    private final ImmutableChainState state;
    private final UpdateEvent contracts;

    BlockAndState(FinalBlock finalBlock, ImmutableChainState state, UpdateEvent contracts) {
      this.finalBlock = finalBlock;
      this.state = state;
      this.contracts = contracts;
    }

    long getBlockTime() {
      return getBlock().getBlockTime();
    }

    long getBlockProductionTime() {
      return getBlock().getProductionTime();
    }

    /**
     * Get current state.
     *
     * @return current state
     */
    public ImmutableChainState getState() {
      return state;
    }

    /**
     * Get contract update event.
     *
     * @return contract update event
     */
    public UpdateEvent getContracts() {
      return contracts;
    }

    /**
     * Get inner block of final block.
     *
     * @return finalized block
     */
    public Block getBlock() {
      return getFinalBlock().getBlock();
    }

    FinalBlock getFinalBlock() {
      return finalBlock;
    }
  }

  private static final class BlockchainListener {

    private final ExecutorService executor;
    private final Listener listener;

    BlockchainListener(Listener listener, ExecutionFactory executionFactory) {
      this.listener = listener;
      this.executor = executionFactory.newSingleThreadExecutor(LISTENER_THREAD_NAME);
    }

    void notify(Consumer<Listener> notification) {
      executor.execute(() -> notification.accept(listener));
    }
  }

  record BlockWithProposal(BlockAndStateWithParent latest, BlockAndState proposal) {

    BlockWithProposal(BlockAndStateWithParent latest) {
      this(latest, null);
    }
  }

  /** Possible finalized executed transaction. */
  public record PossiblyFinalizedTransaction(boolean finalized, ExecutedTransaction transaction) {}

  private record BlockView(
      BlockAndStateWithParent latestBlock, List<BlockAndState> blockProposals) {}
}
