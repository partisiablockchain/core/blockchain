package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import org.bouncycastle.asn1.ASN1BitString;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.asn1.x509.Time;
import org.bouncycastle.asn1.x509.V3TBSCertificateGenerator;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.DefaultTlsCredentialedSigner;
import org.bouncycastle.tls.SignatureAndHashAlgorithm;
import org.bouncycastle.tls.TlsContext;
import org.bouncycastle.tls.crypto.TlsCertificate;
import org.bouncycastle.tls.crypto.TlsCryptoParameters;
import org.bouncycastle.tls.crypto.TlsSigner;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCertificate;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsECDSASigner;

/** Utility class for establishing a TLS connection. */
public final class TlsUtils {

  private static final short SHA_256 = 4;
  private static final short ECDSA = 3;

  /** Needed for code coverage. */
  private TlsUtils() {}

  /**
   * Validates the certificate.
   *
   * @param certificate the certificate to validate.
   * @return the connecting parties blockchain public key
   * @throws IOException on invalid certificate.
   */
  public static BlockchainPublicKey validateCertificate(Certificate certificate)
      throws IOException {
    BcTlsCertificate bcCertificate = (BcTlsCertificate) certificate.getCertificateAt(0);
    BlockchainPublicKey suppliedPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(bcCertificate.getPubKeyEC().getQ().getEncoded(true));
    org.bouncycastle.asn1.x509.Certificate retrievedCertificate = getCertificate(bcCertificate);
    Signature signature = getSignature(retrievedCertificate);
    Hash signedHash = getSignedHash(retrievedCertificate);
    BlockchainPublicKey signaturePublicKey = signature.recoverPublicKey(signedHash);
    if (!suppliedPublicKey.equals(signaturePublicKey)) {
      throw new SecurityException("Certificate could not be verified");
    }
    return suppliedPublicKey;
  }

  private static org.bouncycastle.asn1.x509.Certificate getCertificate(
      BcTlsCertificate bcCertificate) throws IOException {
    byte[] encoded = bcCertificate.getEncoded();
    return org.bouncycastle.asn1.x509.Certificate.getInstance(encoded);
  }

  private static Hash getSignedHash(org.bouncycastle.asn1.x509.Certificate certificate)
      throws IOException {
    TBSCertificate tbsCertificate = certificate.getTBSCertificate();
    byte[] certEncoded = tbsCertificate.getEncoded(ASN1Encoding.DER);
    return Hash.create(s -> s.writeDynamicBytes(certEncoded));
  }

  private static Signature getSignature(org.bouncycastle.asn1.x509.Certificate certificate) {
    ASN1BitString signatureAsDer = certificate.getSignature();
    String signatureAsString = new String(signatureAsDer.getBytes(), Charset.defaultCharset());
    return Signature.fromString(signatureAsString);
  }

  /**
   * Creates a {@link DefaultTlsCredentialedSigner} used in TLS handshake.
   *
   * @param signer the signer of the certificate.
   * @param tlsCrypto the client or server TLS crypto.
   * @param context the client or server TLS context.
   * @param selfSignedCertificate the self signed certificate.
   * @return a {@link DefaultTlsCredentialedSigner} used in TLS handshake.
   */
  public static DefaultTlsCredentialedSigner getTlsSignerCredentials(
      KeyPair signer,
      BcTlsCrypto tlsCrypto,
      TlsContext context,
      Certificate selfSignedCertificate) {
    ECPrivateKeyParameters ecPrivateKeyParameters =
        new ECPrivateKeyParameters(signer.getPrivateKey(), Curve.CURVE);
    TlsSigner tlsSigner = new BcTlsECDSASigner(tlsCrypto, ecPrivateKeyParameters);
    SignatureAndHashAlgorithm signatureAndHashAlgorithm = getSignatureAndHashAlgorithm();
    return new DefaultTlsCredentialedSigner(
        new TlsCryptoParameters(context),
        tlsSigner,
        selfSignedCertificate,
        signatureAndHashAlgorithm);
  }

  /**
   * Makes an object specifying the used hash and signature algorithm.
   *
   * @return the used signature and hash algorithm.
   */
  public static SignatureAndHashAlgorithm getSignatureAndHashAlgorithm() {
    return new SignatureAndHashAlgorithm(SHA_256, ECDSA);
  }

  /**
   * Makes a self signed certificate.
   *
   * @param signer the signer of the certificate.
   * @return a self signed certificate.
   * @throws IOException on an error when encoding the key.
   */
  public static Certificate createSelfSignedCertificate(KeyPair signer) throws IOException {
    return createSignedCertificate(signer, signer);
  }

  @SuppressWarnings("JavaUtilDate")
  static Certificate createSignedCertificate(KeyPair signer, KeyPair sentKey) throws IOException {
    Provider bcProvider = new BouncyCastleProvider();
    Security.addProvider(bcProvider);

    AlgorithmIdentifier algorithmIdentifier =
        new AlgorithmIdentifier(X9ObjectIdentifiers.ecdsa_with_SHA256);
    X500Name subject =
        new X500NameBuilder(BCStyle.INSTANCE).addRDN(BCStyle.CN, "PartisiaX500").build();

    V3TBSCertificateGenerator certGen = new V3TBSCertificateGenerator();
    certGen.setSerialNumber(new ASN1Integer(1));
    certGen.setIssuer(subject);
    certGen.setSubject(subject);
    certGen.setStartDate(new Time(new Date(0)));
    certGen.setEndDate(new Time(new Date(0)));
    certGen.setSignature(algorithmIdentifier);
    byte[] ecPointAsBytes = sentKey.getPublic().getEcPointBytes();
    ECPublicKeyParameters asymmetricKeyParameter =
        new ECPublicKeyParameters(Curve.CURVE.getCurve().decodePoint(ecPointAsBytes), Curve.CURVE);
    SubjectPublicKeyInfo subjectPublicKeyInfo =
        SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(asymmetricKeyParameter);
    certGen.setSubjectPublicKeyInfo(subjectPublicKeyInfo);
    TBSCertificate tbsCert = certGen.generateTBSCertificate();

    byte[] certEncoded = tbsCert.getEncoded(ASN1Encoding.DER);
    Hash toSign = Hash.create(s -> s.writeDynamicBytes(certEncoded));
    Signature signature = signer.sign(toSign);

    ASN1EncodableVector v = new ASN1EncodableVector();
    v.add(tbsCert);
    v.add(algorithmIdentifier);
    v.add(new DERBitString(signature.writeAsString().getBytes(Charset.defaultCharset())));

    byte[] encodedVector = new DERSequence(v).getEncoded(ASN1Encoding.DER);
    BcTlsCrypto tlsCrypto = new BcTlsCrypto(new SecureRandom());
    TlsCertificate tlsCertificate = tlsCrypto.createCertificate(encodedVector);
    return new Certificate(new TlsCertificate[] {tlsCertificate});
  }
}
