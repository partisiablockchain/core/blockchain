package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.AccountPluginState;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.MathUtil;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;

/**
 * Utility for working with the {@link AccountHolderAccountPlugin} (testing version of the account
 * plugin).
 */
public final class FeePluginHelper {

  /**
   * Get the balance for an account.
   *
   * @param state the current blockchain state
   * @param account the account to get information for
   * @return the current balance
   */
  public static long balance(ChainState state, BlockchainAddress account) {
    if (account.getType() == BlockchainAddress.Type.ACCOUNT) {
      return getStateForAccount(state, account).get("balance").longValue();
    } else {
      return contractStorage(state, account).get("balance").longValue();
    }
  }

  /**
   * Get the state for a contract. If the state does not exist or the supplied address type is not
   * for a contract a null value is returned.
   *
   * @param state the current blockchain state
   * @param address the address for the contract
   * @return the state for contract if it exists, null otherwise
   */
  public static StateAccessor contractStorage(ChainState state, BlockchainAddress address) {
    if (address.getType() == BlockchainAddress.Type.ACCOUNT) {
      return null;
    } else {
      AccountPluginState<?, ?, ?> accountPlugin = state.getLocalAccountPluginState();
      return StateAccessor.create(
          Objects.requireNonNullElseGet(accountPlugin.getContract(address), ContractStorage::new));
    }
  }

  /**
   * Get timestampForLatestStorageFeePayment for the given contract.
   *
   * @param state Chain state to access. Not nullable.
   * @param address Address of contract to get timestampForLatestStorageFeePayment from. Not
   *     nullable.
   * @return timestampForLatestStorageFeePayment
   */
  public static long timestampForLatestStorageFeePayment(
      ChainState state, BlockchainAddress address) {
    return contractStorage(state, address).get("timestampForLatestStorageFeePayment").longValue();
  }

  /**
   * Get registered usage from state.
   *
   * @param state state to access
   * @return registered usage
   */
  public static long accumulatedRegisteredUsage(ChainState state) {
    StateAccessor accumulatedFees = getAccumulatedFees(state);
    return accumulatedFees.get("registeredBlockchainUsage").getTreeLeaves().stream()
        .map(x -> x.getValue().longValue())
        .reduce(Long::sum)
        .orElse(0L);
  }

  /**
   * Get service fees from state.
   *
   * @param state the state to access
   * @param target the receiver of service fees
   * @return the service fees
   */
  public static Long serviceFees(ChainState state, BlockchainAddress target) {
    return getAccumulatedFees(state).get("registeredFees").getTreeValue(target).longValue();
  }

  /**
   * Get BYOC fees that have been registered in the account plugin.
   *
   * @param state State to access.
   * @return List of BYOC fees.
   */
  public static List<PendingByocFee> registeredByocFees(ChainState state) {
    StateAccessor local = StateAccessor.create(state.getLocalAccountPluginState().getContextFree());
    FixedList<PendingByocFee> ls =
        local.get("registeredByocFees").typedFixedList(PendingByocFee.class);
    return List.copyOf(ls);
  }

  /**
   * Get infrastructure fee sum from state.
   *
   * @param state the state to access
   * @return the infrastructure fees
   */
  public static Long infrastructureFeesSum(ChainState state) {
    return getAccumulatedFees(state).get("infrastructureFeesSum").longValue();
  }

  private static StateAccessor getAccumulatedFees(ChainState state) {
    AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
    return StateAccessor.create(localAccountPluginState.getContextFree());
  }

  private static StateAccessor getStateForAccount(ChainState state, BlockchainAddress account) {
    AccountPluginState<?, ?, ?> localAccountPluginState = state.getLocalAccountPluginState();
    StateSerializable accountState = localAccountPluginState.getAccount(account);
    return StateAccessor.create(Objects.requireNonNullElseGet(accountState, FeeState::create));
  }

  /**
   * Get the global fee plugin.
   *
   * @param state state to access
   * @return the global fee
   */
  public static StateAccessor getFeePluginGlobal(ChainState state) {
    return StateAccessor.create(state.getGlobalPluginState(ChainPluginType.ACCOUNT));
  }

  /** JAR containing {@link AccountHolderAccountPlugin} and related classes. */
  private static final byte[] JAR =
      JarBuilder.buildJar(
          AccountHolderAccountPlugin.class,
          AccumulatedFees.class,
          ContractStorage.class,
          FeePluginHelper.class,
          FeeState.class,
          GasAndCoinFeePluginGlobal.class,
          MathUtil.class);

  /**
   * Create a JAR containing {@link AccountHolderAccountPlugin} and related classes.
   *
   * @return byte array of jar
   */
  public static byte[] createJar() {
    return JAR;
  }

  /** Mint gas in the supplied mutable state. */
  public static void mintGas(MutableChainState mutable, BlockchainAddress account, long amount) {
    mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(true);
              s.writeLong(amount);
            }),
        0);
  }

  /**
   * Get gas balance for an account by invoking local plugin.
   *
   * @param mutable state
   * @param account account address
   * @return return value
   */
  public static byte[] getBalance(
      MutableChainState mutable, BlockchainAddress account, long blockProductionTime) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(s -> s.writeByte(2)),
        blockProductionTime);
  }

  /**
   * Get blockchain usage by invoking local plugin.
   *
   * @param mutable state
   * @return return value
   */
  public static byte[] getBlockchainUsage(MutableChainState mutable, long blockProductionTime) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(rpc -> rpc.writeBoolean(false)),
        blockProductionTime);
  }

  /** Enable storage fees. */
  public static void enableStorageFees(
      MutableChainState mutable, BlockchainAddress account, long productionTime) {
    mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(false);
              s.writeLong(productionTime);
            }),
        productionTime);
  }

  /** Set fee account. */
  public static void setFeeAccount(MutableChainState mutable, BlockchainAddress account) {
    mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(false);
              account.write(s);
            }));
  }

  /** Set zk fee account. */
  public static void setZkFeeAccount(MutableChainState mutable, BlockchainAddress account) {
    mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeBoolean(true);
              account.write(s);
            }));
  }

  /**
   * Get paid fees by invoking local plugin.
   *
   * @param mutable state
   * @param gas the amount of gas
   * @param blockProductionTime production time of block
   * @return plugin invocation return value
   */
  public static byte[] getPaidFees(
      MutableChainState mutable, long gas, long blockProductionTime, BlockchainAddress account) {
    return mutable.updateLocalPluginState(
        ChainPluginType.ACCOUNT,
        account,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(3);
              s.writeLong(gas);
            }),
        blockProductionTime);
  }

  /**
   * Make a dummy invocation to global plugin, that returns dummyValue.
   *
   * @param mutable state
   * @param dummyValue the value to be returned
   * @return plugin invocation return value
   */
  public static byte[] getDummyValueGlobal(MutableChainState mutable, long dummyValue) {
    return mutable.updateGlobalPluginState(
        0,
        ChainPluginType.ACCOUNT,
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(2);
              s.writeLong(dummyValue);
            }));
  }
}
