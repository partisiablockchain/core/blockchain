package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of {@link PubBinderContextImpl}. */
public final class PubBinderContextImplTest extends ContextTest<PubBinderContextImpl> {

  /** Setup context with mocks. */
  @BeforeEach
  public void setUp() {
    context = new PubBinderContextImpl(contractContext, executionContext);
  }

  @Test
  public void cpuFee() {
    Assertions.assertThat(feeCollector.sum()).isEqualTo(0);
    context.registerCpuFee(15);
    Assertions.assertThat(feeCollector.sum()).isEqualTo(15);
    Assertions.assertThat(context.availableGas()).isEqualTo(100);
  }
}
