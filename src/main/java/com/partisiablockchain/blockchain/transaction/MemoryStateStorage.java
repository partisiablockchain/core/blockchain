package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/** Stores state of byte arrays. */
public final class MemoryStateStorage implements StateStorage {
  private final Map<Hash, byte[]> data;

  /** Default constructor. */
  public MemoryStateStorage() {
    data = new HashMap<>();
  }

  /**
   * Constructor to create state storage with map of data.
   *
   * @param data hashes of information mapped to the information
   */
  public MemoryStateStorage(Map<Hash, byte[]> data) {
    this.data = data;
  }

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    data.put(hash, SafeDataOutputStream.serialize(writer));
    return true;
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    if (data.get(hash) != null) {
      return reader.apply(SafeDataInputStream.createFromBytes(data.get(hash)));
    } else {
      return null;
    }
  }

  /**
   * Retrieves a map of serialized data.
   *
   * @return the map of all serialized data
   */
  public Map<Hash, byte[]> getData() {
    return data;
  }
}
