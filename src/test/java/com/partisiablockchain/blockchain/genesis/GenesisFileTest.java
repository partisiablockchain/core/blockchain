package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.stream.SafeDataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class GenesisFileTest {
  @TempDir Path tmp;
  private File file;

  @BeforeEach
  void setUp() {
    file = tmp.resolve("genesisfile.zip").toFile();
  }

  @Test
  void failsOnUnknownVersion() throws IOException {
    assertFails(-1);
    assertFails(0);
    assertFails(2);
    assertFails(Integer.MAX_VALUE);
    assertFails(Integer.MIN_VALUE);
  }

  private void assertFails(int version) throws IOException {
    writeVersion(version);

    assertThatThrownBy(() -> new GenesisFile(file))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(String.valueOf(version));
  }

  private void writeVersion(int version) throws IOException {
    FileOutputStream out = new FileOutputStream(file);
    ZipOutputStream stream = new ZipOutputStream(out);
    stream.putNextEntry(new ZipEntry(GenesisStorage.GENESIS_VERSION_FILE));
    new SafeDataOutputStream(stream).writeInt(version);
    stream.close();
  }
}
