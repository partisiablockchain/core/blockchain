package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateSerializer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardNoncesTest {

  @Test
  public void initialValues() {
    assertValues(ShardNonces.EMPTY, 1L, 0L, 1L);
  }

  @Test
  public void bumpOutbound() {
    ShardNonces value = ShardNonces.EMPTY;
    value = value.bumpOutbound();
    assertValues(value, 1, 0, 2);
  }

  @Test
  public void bumpInbound() {
    ShardNonces value = ShardNonces.EMPTY;
    value = value.bumpInbound(55L);
    assertValues(value, 2, 55L, 1);
  }

  @Test
  public void serialization() {
    ShardNonces value = ShardNonces.EMPTY;
    value = value.bumpInbound(55L);
    value = value.bumpOutbound();
    StateSerializer stateSerializer = ObjectCreator.createMemoryStoredSerializer();
    ShardNonces result =
        stateSerializer.read(stateSerializer.write(value).hash(), ShardNonces.class);
    assertValues(
        result, value.getInbound(), value.getLatestInboundCommitteeId(), value.getOutbound());
  }

  @Test
  public void sync() {
    ShardNonces withMissingSync = ShardNonces.EMPTY.markMissingSync();
    Assertions.assertThat(withMissingSync.isMissingSync()).isTrue();
    ShardNonces afterSync = withMissingSync.markSyncReceived();
    Assertions.assertThat(afterSync.isMissingSync()).isFalse();

    Assertions.assertThatThrownBy(afterSync::markSyncReceived);
    Assertions.assertThatThrownBy(withMissingSync::markMissingSync);
  }

  private void assertValues(ShardNonces nonces, long inbound, long committeeId, long outbounds) {
    Assertions.assertThat(nonces.getInbound()).isEqualTo(inbound);
    Assertions.assertThat(nonces.getLatestInboundCommitteeId()).isEqualTo(committeeId);
    Assertions.assertThat(nonces.getOutbound()).isEqualTo(outbounds);
  }
}
