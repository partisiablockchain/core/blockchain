package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Stores state with a pending write and cached read cache. */
public final class StateStorageCached implements StateStorageRaw, AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(StateStorageCached.class);

  private final Storage storage;
  private final ExecutorService executor;
  private final Map<Hash, byte[]> pendingWrites = new ConcurrentHashMap<>();
  private final Map<Hash, byte[]> cachedReads = Collections.synchronizedMap(new WeakHashMap<>());

  StateStorageCached(Storage storage) {
    this(storage, ExecutorFactory.newSingle("StorageWriter"));
  }

  StateStorageCached(Storage storage, ExecutorService executor) {
    this.storage = storage;
    this.executor = executor;
  }

  @Override
  @SuppressWarnings("FutureReturnValueIgnored")
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    boolean exists =
        pendingWrites.containsKey(hash) || cachedReads.containsKey(hash) || storage.has(hash);
    if (!exists) {
      byte[] serialize = SafeDataOutputStream.serialize(writer);
      Hash hashOfData = Hash.create(s -> s.write(serialize));
      if (!hash.equals(hashOfData)) {
        throw new IllegalArgumentException(
            "Inconsistent data and hash expected=" + hashOfData + ", actual=" + hash);
      }
      cachedReads.put(hash, serialize);
      pendingWrites.put(hash, serialize);
      executor.submit(
          () -> {
            storage.write(hash, s -> s.write(serialize));
            pendingWrites.remove(hash);
          });
    }
    return !exists;
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    byte[] bytes = read(hash);
    if (bytes == null) {
      return null;
    } else {
      return reader.apply(SafeDataInputStream.createFromBytes(bytes));
    }
  }

  @Override
  public byte[] read(Hash hash) {
    return lookupBytes(hash);
  }

  private byte[] lookupBytes(Hash hash) {
    byte[] bytes = pendingWrites.get(hash);
    if (bytes != null) {
      return bytes;
    }
    byte[] cachedBytes = cachedReads.get(hash);
    if (cachedBytes != null) {
      return cachedBytes;
    }
    return readAndCache(hash);
  }

  private byte[] readAndCache(Hash hash) {
    byte[] fromStorage = storage.readRaw(hash);
    if (fromStorage != null) {
      cachedReads.put(hash, fromStorage);
    }
    return fromStorage;
  }

  @Override
  public void close() {
    ExceptionLogger.handle(
        logger::warn,
        () -> {
          executor.shutdown();
          executor.awaitTermination(1, TimeUnit.MINUTES);
        },
        "Error while closing executor");
  }
}
