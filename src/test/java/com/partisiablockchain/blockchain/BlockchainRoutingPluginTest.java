package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.tools.immutable.FixedList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainRoutingPluginTest {

  @Test
  public void invokeLocal() {
    BlockchainRoutingPlugin routingPlugin = new EmptyPlugin();
    Assertions.assertThatThrownBy(() -> routingPlugin.invokeLocal(null, null, null, null, null))
        .isInstanceOf(UnsupportedOperationException.class);
    Assertions.assertThat(routingPlugin.migrateLocal(null)).isNull();
    Assertions.assertThat(routingPlugin.route(null, null)).isNull();
    Assertions.assertThat(routingPlugin.getLocalStateClass()).isEqualTo(StateVoid.class);
  }

  @Test
  public void invokeGlobal() {
    BlockchainRoutingPlugin routingPlugin = new EmptyPlugin();
    Assertions.assertThatThrownBy(() -> routingPlugin.invokeGlobal(null, null, null))
        .isInstanceOf(UnsupportedOperationException.class);
    Assertions.assertThat(routingPlugin.getGlobalStateClass()).isEqualTo(StateVoid.class);
  }

  private static final class EmptyPlugin extends BlockchainRoutingPlugin {

    @Override
    public String route(FixedList<String> shards, BlockchainAddress target) {
      return null;
    }
  }
}
