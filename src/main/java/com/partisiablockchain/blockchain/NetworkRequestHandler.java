package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEventCompressed;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.flooding.BlockRequest;
import com.partisiablockchain.flooding.BlockResponse;
import com.partisiablockchain.flooding.BlockResponse.BlockPair;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed.BlockPairSemiCompressed;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.NetworkNode.IncomingPacket;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.storage.ExecutableBlock;
import com.secata.tools.thread.ExecutionFactory;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class NetworkRequestHandler {

  static final int MAX_BLOCKS = 0xFF;
  private static final Logger logger = LoggerFactory.getLogger(NetworkRequestHandler.class);
  static final String BLOCK_REQUESTER_THREAD_NAME = "BlockRequester-";

  private final BlockRequester requester;
  private final BlockchainLedger blockchainLedger;
  private final Executor executor;
  private final String chainId;

  NetworkRequestHandler(
      BlockRequester requester,
      BlockchainLedger blockchainLedger,
      String chainId,
      String subChainId,
      ExecutionFactory executionFactory) {
    this(
        requester,
        blockchainLedger,
        executionFactory.newSingleThreadExecutor(BLOCK_REQUESTER_THREAD_NAME + subChainId),
        chainId);
  }

  NetworkRequestHandler(
      BlockRequester requester,
      BlockchainLedger blockchainLedger,
      Executor executor,
      String chainId) {
    this.requester = requester;
    this.blockchainLedger = blockchainLedger;
    this.executor = executor;
    this.chainId = chainId;
  }

  public void newPacket(IncomingPacket incomingPacket) {
    Packet<?> packet = incomingPacket.getPacket();
    Packet.Type<?> type = packet.getType();
    if (Packet.Type.BLOCK.equals(type)) {
      logger.trace("Received block");
      executor.execute(() -> blockchainLedger.appendBlock((FinalBlock) packet.getPayload()));
    } else if (Packet.Type.FINALIZATION.equals(type)) {
      logger.trace("Received finalization data");
      blockchainLedger.incomingFinalizationData(
          incomingPacket.getSender(), (FinalizationData) packet.getPayload());
    } else if (Packet.Type.TRANSACTION.equals(type)) {
      logger.trace("Received transaction");
      blockchainLedger.addPendingTransaction((SignedTransaction) packet.getPayload());
    } else if (Packet.Type.EVENT.equals(type)) {
      logger.trace("Received event");
      blockchainLedger.addPendingEvent((FloodableEvent) packet.getPayload());
    } else if (Packet.Type.COMPRESSED_EVENT.equals(type)) {
      logger.trace("Received compressed event");
      blockchainLedger.addPendingEvent((FloodableEventCompressed) packet.getPayload());
    } else if (Packet.Type.BLOCK_RESPONSE.equals(type)) {
      BlockResponse response = (BlockResponse) packet.getPayload();
      List<BlockPair> blocks = response.getBlocks();
      long responseFor;
      if (blocks.isEmpty()) {
        logger.debug("Received response with 0 blocks");
        responseFor = 0;
      } else {
        logger.debug(
            "Received response with {} blocks from {} to {}",
            blocks.size(),
            blocks.get(0).getBlock().getBlock().getBlockTime(),
            blocks.get(blocks.size() - 1).getBlock().getBlock().getBlockTime());
        if (onlyKnownProposals(blocks)) {
          responseFor = 0;
        } else {
          responseFor = blocks.get(0).getBlock().getBlock().getBlockTime();
        }
      }
      // append received blocks to ledger
      executor.execute(
          () -> {
            for (BlockPair blockPair : blocks) {
              ExecutableBlock executableBlock = blockPair.createExecutableBlock();
              logger.debug("Appending response based block: {}", executableBlock);
              blockchainLedger.appendBlock(executableBlock);
            }

            requester.responseReceived(responseFor);

            requestBlocksFromSender(incomingPacket, response.getSenderBlockTime());
          });
    } else if (Packet.Type.BLOCK_REQUEST.equals(type)) {
      Connection sender = incomingPacket.getSender();
      BlockRequest request = (BlockRequest) packet.getPayload();

      logger.trace("Received block request with time {}", request.getBlockTime());

      BlockResponse blockResponse = handleBlockRequest(request.getBlockTime());

      if (blockResponse.getBlocks().isEmpty()) {
        logger.debug("Sending empty block response for requested time {}", request.getBlockTime());
      } else {
        logger.debug(
            "Sending block response from time {} to {} for request time {}",
            blockResponse.getBlocks().get(0).getBlock().getBlock().getBlockTime(),
            blockResponse
                .getBlocks()
                .get(blockResponse.getBlocks().size() - 1)
                .getBlock()
                .getBlock()
                .getBlockTime(),
            request.getBlockTime());
      }

      sender.send(new Packet<>(Packet.Type.BLOCK_RESPONSE, blockResponse));
    } else if (Packet.Type.COMPRESSED_BLOCK_RESPONSE.equals(type)) {
      BlockResponseSemiCompressed response = (BlockResponseSemiCompressed) packet.getPayload();
      List<BlockPairSemiCompressed> blocks = response.getBlocks();
      long responseFor = getResponseFor(blocks);
      // append received blocks to ledger
      executor.execute(
          () -> {
            for (BlockPairSemiCompressed blockPair : blocks) {
              ExecutableBlock executableBlock = blockchainLedger.getExecutableBlock(blockPair);
              logger.debug("Appending response based block: {}", executableBlock);
              blockchainLedger.appendBlock(executableBlock);
            }

            requester.responseReceived(responseFor);

            requestBlocksFromSender(incomingPacket, response.getSenderBlockTime());
          });
    } else /*if (Type.SYNC.equals(type))*/ {
      Connection sender = incomingPacket.getSender();

      logger.trace("Received sync request");

      logger.debug(
          "Sending block response for sync request, latest block is: {}",
          blockchainLedger.getLatestBlock());

      sender.send(
          new Packet<>(
              Packet.Type.BLOCK_RESPONSE, handleBlockRequest(blockchainLedger.getBlockTime())));
    }
  }

  private void requestBlocksFromSender(IncomingPacket incomingPacket, long senderBlockTime) {
    long currentBlockTime = blockchainLedger.getBlockTime();
    if (currentBlockTime < senderBlockTime) {
      logger.trace(
          "Request more blocks from sender, requesting time {} - sender has time {}",
          currentBlockTime + 1,
          senderBlockTime);
      Connection sender = incomingPacket.getSender();
      requester.requestBlocks(sender, currentBlockTime + 1);
    }
  }

  private long getResponseFor(List<BlockPairSemiCompressed> blocks) {
    long responseFor;
    if (blocks.isEmpty()) {
      logger.debug("Received response with 0 blocks");
      responseFor = 0;
    } else {
      logger.debug(
          "Received response with {} blocks from {} to {}",
          blocks.size(),
          blocks.get(0).getBlock().getBlock().getBlockTime(),
          blocks.get(blocks.size() - 1).getBlock().getBlock().getBlockTime());
      if (onlyKnownProposalsCompressed(blocks)) {
        responseFor = 0;
      } else {
        responseFor = blocks.get(0).getBlock().getBlock().getBlockTime();
      }
    }
    return responseFor;
  }

  boolean onlyKnownProposalsCompressed(List<BlockPairSemiCompressed> blocks) {
    for (BlockPairSemiCompressed blockPair : blocks) {
      if (!isInProposal(blockPair.getBlock().getBlock())) {
        return false;
      }
    }
    return true;
  }

  private boolean onlyKnownProposals(List<BlockPair> blocks) {
    for (BlockPair blockPair : blocks) {
      if (!isInProposal(blockPair.getBlock().getBlock())) {
        return false;
      }
    }
    return true;
  }

  private boolean isInProposal(Block block) {
    for (BlockAndState proposal : blockchainLedger.getProposals()) {
      if (proposal.getBlock().equals(block)) {
        return true;
      }
    }
    return false;
  }

  private BlockResponse handleBlockRequest(long blockTime) {
    long localBlockTime = blockchainLedger.getBlockTime();
    List<BlockPair> blocks =
        Stream.concat(
                LongStream.range(blockTime, localBlockTime + 1)
                    .mapToObj(blockchainLedger::getFinalizedBlock),
                blockchainLedger.getProposals().stream().map(BlockAndState::getFinalBlock))
            .limit(MAX_BLOCKS)
            .map(this::getBlock)
            .collect(Collectors.toList());
    if (!blocks.isEmpty()) {
      logger.debug(
          "Sending {} to {}",
          blocks.get(0).getBlock().getBlock().getBlockTime(),
          blocks.get(blocks.size() - 1).getBlock().getBlock().getBlockTime());
    }
    return new BlockResponse(localBlockTime, blocks, chainId);
  }

  private BlockPair getBlock(FinalBlock block) {
    List<ExecutedTransaction> transactionsForBlock =
        blockchainLedger.getTransactionsForBlock(block.getBlock());

    List<SignedTransaction> converted =
        SignedTransaction.select(transactionsForBlock.stream().map(ExecutedTransaction::getInner));
    List<ExecutableEvent> events =
        ExecutableEvent.select(transactionsForBlock.stream().map(ExecutedTransaction::getInner));
    return new BlockPair(block, converted, events, chainId);
  }
}
