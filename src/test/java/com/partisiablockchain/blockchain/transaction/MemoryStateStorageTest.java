package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test for {@link MemoryStateStorage}, the class functions will be tested. */
public final class MemoryStateStorageTest {
  private MemoryStateStorage memoryStateStorage;

  @BeforeEach
  void setUp() {
    memoryStateStorage = new MemoryStateStorage();
  }

  /** Test if write returns true as expected. */
  @Test
  public void write() {
    Hash hash = TestObjects.hashNumber(1);
    boolean result = memoryStateStorage.write(hash, ignored -> {});

    Assertions.assertThat(result).isTrue();
  }

  /** Test if a map is returned and it is empty as default. */
  @Test
  public void getData() {
    Assertions.assertThat(memoryStateStorage.getData()).isNotNull().isEmpty();
  }

  /** Test that we can read back a previously written value. */
  @Test
  public void read() {
    String testString = "test string";
    Hash testHash = TestObjects.hashNumber(123456789);
    memoryStateStorage.write(testHash, out -> out.writeString(testString));
    String testStringResult = memoryStateStorage.read(testHash, SafeDataInputStream::readString);
    Assertions.assertThat(testStringResult).isEqualTo(testString);
  }

  /** Test that read returns null when called with an unknown key. */
  @Test
  public void readNull() {
    Hash nullHash = TestObjects.hashNumber(1234567890);
    var readVariableNull = memoryStateStorage.read(nullHash, SafeDataInputStream::readString);
    Assertions.assertThat(readVariableNull).isEqualTo(null);
  }
}
