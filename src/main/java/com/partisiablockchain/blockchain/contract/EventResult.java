package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.CallResult;
import java.util.List;

/**
 * The events as a result of calling a contract.
 *
 * @param <EventT> the type of events
 */
public final class EventResult<EventT extends BinderEvent> {

  /** Resulting fire and forget transactions. */
  public final List<EventT> invocations;

  /** Resulting call result, either a new callback or a byte array as result. */
  public final CallResult result;

  /**
   * Simple creator for this result.
   *
   * @param invocations the invocations
   * @param result the byte array result
   */
  public EventResult(List<EventT> invocations, CallResult result) {
    this.invocations = invocations;
    this.result = result;
  }
}
