package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import java.util.ArrayList;
import java.util.List;

/** Dummy implementation of additional status providers. */
public final class DummyAdditionalStatusProviders implements AdditionalStatusProviders {

  private final List<AdditionalStatusProvider> providers = new ArrayList<>();

  @Override
  public List<AdditionalStatusProvider> getProviders() {
    return providers;
  }

  @Override
  public void register(AdditionalStatusProvider additionalStatusProvider) {
    providers.add(additionalStatusProvider);
  }
}
