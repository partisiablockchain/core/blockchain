package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateString;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test. */
public final class SysBlockchainContractTest {

  private final SysBinderContract<StateString> sysBinderContract =
      Mockito.mock(SysBinderContractStateString.class);
  private final SysBlockchainContract<StateString> sysBlockchainContract =
      new SysBlockchainContract<>(sysBinderContract);
  private final byte[] rpc = new byte[] {Byte.MIN_VALUE};
  private final ArgumentCaptor<byte[]> rpcCaptor = ArgumentCaptor.forClass(byte[].class);
  private final ArgumentCaptor<StateAccessor> stateAccessorCaptor =
      ArgumentCaptor.forClass(StateAccessor.class);

  @Test
  public void oldUpgradePath() {
    Mockito.when(sysBinderContract.upgrade(stateAccessorCaptor.capture(), rpcCaptor.capture()))
        .thenReturn(new StateString("return state"));

    StateAccessor stateAccessor = StateAccessor.create(new StateString("state accessor captured"));
    StateString upgrade = sysBlockchainContract.upgrade(stateAccessor, rpc);

    Assertions.assertThat(upgrade.value()).isEqualTo("return state");
    Assertions.assertThat(rpcCaptor.getValue()[0]).isEqualTo(Byte.MIN_VALUE);
    Assertions.assertThat(stateAccessorCaptor.getValue().stateStringValue().value())
        .isEqualTo("state accessor captured");
  }

  private interface SysBinderContractStateString extends SysBinderContract<StateString> {}
}
