package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Test. */
@Immutable
public final class FeeState implements StateSerializable {

  private final long balance;
  private final long freeTransactions;

  @SuppressWarnings("unused")
  FeeState() {
    balance = -1;
    freeTransactions = 0;
  }

  private FeeState(long balance, long freeTransactions) {
    this.balance = balance;
    this.freeTransactions = freeTransactions;
  }

  /**
   * Create a new account state.
   *
   * @return the created account state
   */
  public static FeeState create() {
    return new FeeState(0L, 0);
  }

  /**
   * Updates gas balance.
   *
   * @param diff amount to update.
   * @return the updated account state
   */
  public FeeState updatedBalance(long diff) {
    if (hasDebt()) {
      throw new RuntimeException("Unable to update balance with debt");
    }
    if (balance + diff < 0) {
      throw new RuntimeException("Insufficient funds: balance=" + balance + ", diff=" + diff);
    }
    return new FeeState(balance + diff, freeTransactions);
  }

  /**
   * Set debt of this state.
   *
   * @param debt the debt to set
   * @return the updated state
   */
  public FeeState setDebt(long debt) {
    return new FeeState(-debt, freeTransactions);
  }

  /**
   * Get the amount of gas hold by this account. Debt is not returned by this method and the result
   * is thus always &ge; 0.
   *
   * @return the amount of gas or 0 if no gas or debt
   */
  public long getBalance() {
    return Math.max(0, balance);
  }

  public long getFreeTransactions() {
    return freeTransactions;
  }

  /**
   * Increments the amount of free transactions used.
   *
   * @param blockProductionTime the production time of the block
   * @return the original Feestate or an updated FeeState.
   */
  public FeeState incrementFreeTransactions(long blockProductionTime) {
    if (blockProductionTime > 0) {
      return new FeeState(balance, freeTransactions + 1);
    }
    return new FeeState(balance, freeTransactions);
  }

  /**
   * Get the debt of this account.
   *
   * @return debt or zero
   */
  public long getDebt() {
    return Math.abs(Math.min(0, balance));
  }

  /**
   * Has the account debt.
   *
   * @return true if debt > 0
   */
  public boolean hasDebt() {
    return getDebt() != 0;
  }
}
