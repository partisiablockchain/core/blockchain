package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import org.assertj.core.api.Assertions;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCertificate;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TlsUtilsTest {

  private final KeyPair keyPair1 = new KeyPair(BigInteger.ONE);
  private final KeyPair keyPair2 = new KeyPair(BigInteger.TWO);

  @Test
  public void invalidCertificate() throws IOException {
    Certificate certificate = TlsUtils.createSignedCertificate(keyPair1, keyPair2);

    Assertions.assertThatThrownBy(() -> TlsUtils.validateCertificate(certificate))
        .hasMessage("Certificate could not be verified");
  }

  @Test
  public void correctPublicKeyInCertificate() throws Exception {
    Certificate certificate = TlsUtils.createSelfSignedCertificate(keyPair1);
    BcTlsCertificate bcCertificate = (BcTlsCertificate) certificate.getCertificateAt(0);
    ECPoint senderPublicKey = bcCertificate.getPubKeyEC().getQ();
    byte[] bytes = keyPair1.getPublic().getEcPointBytes();
    assertThat(senderPublicKey).isEqualTo(Curve.CURVE.getCurve().decodePoint(bytes));
  }

  @Test
  public void correctSignatureInCertificate() throws Exception {
    Certificate certificate = TlsUtils.createSelfSignedCertificate(keyPair1);

    BcTlsCertificate bcCertificate = (BcTlsCertificate) certificate.getCertificateAt(0);
    byte[] encoded = bcCertificate.getEncoded();
    org.bouncycastle.asn1.x509.Certificate retrievedCertificate =
        org.bouncycastle.asn1.x509.Certificate.getInstance(encoded);
    byte[] signedBytes = retrievedCertificate.getTBSCertificate().getEncoded(ASN1Encoding.DER);
    Hash toSign = Hash.create(s -> s.writeDynamicBytes(signedBytes));
    DERBitString signatureAsDerBitString =
        new DERBitString(keyPair1.sign(toSign).writeAsString().getBytes(Charset.defaultCharset()));

    assertThat(retrievedCertificate.getSignature()).isEqualTo(signatureAsDerBitString);
  }

  @Test
  public void validCertificate() throws IOException {
    Certificate selfSignedCertificate = TlsUtils.createSelfSignedCertificate(keyPair1);
    BlockchainPublicKey publicKey = TlsUtils.validateCertificate(selfSignedCertificate);
    assertThat(publicKey).isEqualTo(keyPair1.getPublic());
  }
}
