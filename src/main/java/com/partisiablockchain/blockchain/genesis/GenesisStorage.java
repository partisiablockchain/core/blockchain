package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.storage.ExecutableBlock;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/** Storage that persist everything in memory to allow saving it as a single genesis file. */
public final class GenesisStorage implements StateStorage {

  private final Map<Hash, byte[]> values = new HashMap<>();
  static final String TRANSACTIONS_DIR = "transactions/";
  static final String EVENTS_DIR = "events/";
  static final String GENESIS_BLOCK_FILE = "genesisBlock";
  static final String GENESIS_VERSION_FILE = "version";

  /**
   * Create a genesis file with all relevant states and the supplied genesis block.
   *
   * @param destination file for the genesis data
   * @param genesisBlock the genesis block
   */
  public synchronized void persistGenesis(File destination, FinalBlock genesisBlock) {
    persistGenesis(destination, new ExecutableBlock(genesisBlock, List.of(), List.of()));
  }

  /**
   * Create a genesis file with all relevant states and the supplied genesis block.
   *
   * @param destination file for the genesis data
   * @param executableBlock the executable genesis block
   */
  public synchronized void persistGenesis(File destination, ExecutableBlock executableBlock) {
    FinalBlock genesisBlock = executableBlock.getBlock();
    List<SignedTransaction> transactions = executableBlock.getTransactions();
    List<ExecutableEvent> events = executableBlock.getEventTransactions();

    if (executableBlock.isMissingTransactionOrEvents()) {
      throw new IllegalArgumentException("Missing transaction/events for " + executableBlock);
    }

    writeGenesisFile(destination, genesisBlock, transactions, events);
  }

  /**
   * Write genesis file to disk.
   *
   * @param destination file for the genesis data
   * @param genesisBlock the genesis block
   * @param transactions the transactions to be included in genesis block
   * @param events the events to be included in genesis block
   */
  synchronized void writeGenesisFile(
      File destination,
      FinalBlock genesisBlock,
      List<SignedTransaction> transactions,
      List<ExecutableEvent> events) {
    WithResource.accept(
        () -> new ZipOutputStream(new FileOutputStream(destination)),
        zip -> {
          zip.putNextEntry(new ZipEntry(GENESIS_BLOCK_FILE));
          genesisBlock.write(new SafeDataOutputStream(zip));

          for (Map.Entry<Hash, byte[]> entry : values.entrySet()) {
            String path = entry.getKey().toString();
            zip.putNextEntry(new ZipEntry(path));
            zip.write(entry.getValue());
          }

          for (SignedTransaction transaction : transactions) {
            String path = TRANSACTIONS_DIR + transaction.identifier().toString();
            zip.putNextEntry(new ZipEntry(path));
            SafeDataOutputStream output = new SafeDataOutputStream(zip);
            transaction.write(output);
          }

          for (ExecutableEvent event : events) {
            String path = EVENTS_DIR + event.identifier().toString();
            zip.putNextEntry(new ZipEntry(path));
            SafeDataOutputStream output = new SafeDataOutputStream(zip);
            event.write(output);
          }

          zip.putNextEntry(new ZipEntry(GENESIS_VERSION_FILE));
          new SafeDataOutputStream(zip).writeInt(GenesisFile.VERSION_1);
        },
        "Unable to persis genesis state");
  }

  @Override
  public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    if (values.containsKey(hash)) {
      return false;
    }
    values.put(hash, SafeDataOutputStream.serialize(writer));
    return true;
  }

  @Override
  public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    if (values.containsKey(hash)) {
      SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(values.get(hash));
      return reader.apply(bytes);
    } else {
      return null;
    }
  }
}
