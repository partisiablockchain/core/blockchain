package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.List;

/**
 * Interface for all plugins that can be deployed to the blockchain.
 *
 * @param <GlobalT> the global state shared across shards
 * @param <LocalT> the local state that lives on each shard
 */
@Immutable
public interface BlockchainPlugin<
        GlobalT extends StateSerializable, LocalT extends StateSerializable>
    extends PluginInteractionCreator {

  /**
   * Get class type for global state.
   *
   * @return class type for global state
   */
  Class<GlobalT> getGlobalStateClass();

  /**
   * Get class type for local state.
   *
   * @return class type for local state
   */
  Class<LocalT> getLocalStateClass();

  /**
   * Class type parameters for local state.
   *
   * @return list of class type parameters
   */
  List<Class<?>> getLocalStateClassTypeParameters();

  /**
   * Run invocation on local state.
   *
   * @param pluginContext context for plugin
   * @param globalState global state
   * @param state local state
   * @param rpc stream for inputting payload for invocation
   * @param invocationContext the context for invocation
   * @return next local state
   */
  InvokeResult<LocalT> invokeLocal(
      PluginContext pluginContext,
      GlobalT globalState,
      LocalT state,
      BlockchainAddress invocationContext,
      byte[] rpc);

  /**
   * Run invocation on global state.
   *
   * @param pluginContext context for plugin
   * @param state current global state
   * @param rpc payload for invocation
   * @return next global state
   */
  InvokeResult<GlobalT> invokeGlobal(PluginContext pluginContext, GlobalT state, byte[] rpc);

  /**
   * Migrate global state.
   *
   * @param currentGlobal access to global state
   * @param rpc payload for migration
   * @return next global state
   */
  GlobalT migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc);

  /**
   * Migrate local state.
   *
   * @param currentLocal access to local state
   * @return next local state
   */
  LocalT migrateLocal(StateAccessor currentLocal);

  /**
   * The result of an invocation, including a return value.
   *
   * @param <T> the type to update.
   */
  @SuppressWarnings("ArrayRecordComponent")
  record InvokeResult<T>(T updatedState, byte[] result) {}
}
