package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutionContext;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.MemoryStateStorage;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.coverage.FunctionUtility;
import java.io.Serial;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Marker;
import org.slf4j.event.Level;
import org.slf4j.helpers.AbstractLogger;

/** Tests for TransactionProcessor. */
public final class TransactionProcessorTest {

  @Nested
  final class Logger {

    private final Block block =
        new Block(
            1,
            1,
            0,
            Hash.create(FunctionUtility.noOpConsumer()),
            Hash.create(FunctionUtility.noOpConsumer()),
            List.of(),
            List.of());
    private final ExecutionContext context = ExecutionContext.init("Main", "Shard", block, 0);
    private final KeyPair signer = new KeyPair();
    private final MutableChainState state =
        MutableChainState.emptyMaster(new ChainStateCache(new MemoryStateStorage()));
    private final SignedTransaction signedTx =
        SignedTransaction.create(
                CoreTransactionPart.create(0, 1, 2134),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]))
            .sign(signer, "Main");

    @Test
    void triggered_if_tx_lasts_more_than_one_second() {
      TestLogger logger = new TestLogger();
      state.createAccount(signer.getPublic().createAddress());
      TransactionProcessor transactionProcessor =
          new TransactionProcessor(context, block, state, new MyTime(1234, 2235), logger);
      Assertions.assertThat(logger.calls).isEqualTo(0);
      transactionProcessor.processTransaction(signedTx);
      Assertions.assertThat(logger.calls).isEqualTo(1);
      Assertions.assertThat(logger.latestLevel).isEqualTo(Level.WARN);
      Assertions.assertThat(logger.latestString).contains("Long transaction");

      Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNull();
      ExecutableEvent executableEvent =
          new ExecutableEvent(
              "OtherShard",
              new EventTransaction(
                  TestObjects.EMPTY_HASH,
                  new ShardRoute("Shard", 1),
                  0,
                  0,
                  1,
                  null,
                  new InnerSystemEvent.CreateAccountEvent(TestObjects.ACCOUNT_FOUR)));
      transactionProcessor.processTransaction(executableEvent);
      Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNotNull();
      Assertions.assertThat(logger.calls).isEqualTo(2);
    }

    @Test
    void not_triggered_if_tx_lasts_one_second() {
      TestLogger logger = new TestLogger();
      state.createAccount(signer.getPublic().createAddress());

      TransactionProcessor transactionProcessor =
          new TransactionProcessor(context, block, state, new MyTime(1234, 2234), logger);
      Assertions.assertThat(logger.calls).isEqualTo(0);
      transactionProcessor.processTransaction(signedTx);
      Assertions.assertThat(logger.calls).isEqualTo(0);

      Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNull();
      ExecutableEvent executableEvent =
          new ExecutableEvent(
              "OtherShard",
              new EventTransaction(
                  TestObjects.EMPTY_HASH,
                  new ShardRoute("Shard", 1),
                  0,
                  0,
                  1,
                  null,
                  new InnerSystemEvent.CreateAccountEvent(TestObjects.ACCOUNT_FOUR)));
      transactionProcessor.processTransaction(executableEvent);
      Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNotNull();
      Assertions.assertThat(logger.calls).isEqualTo(0);
    }

    private static final class MyTime implements TransactionProcessor.SystemTime {

      private final long before;
      private final long after;
      private boolean first = false;

      public MyTime(long before, long after) {
        this.before = before;
        this.after = after;
      }

      @Override
      public long getCurrentTimeInMillis() {
        first = !first;
        if (first) {
          return before;
        } else {
          return after;
        }
      }
    }
  }

  private static final class TestLogger extends AbstractLogger {

    @Serial private static final long serialVersionUID = -1;
    private int calls;
    private Level latestLevel;
    private String latestString;

    @Override
    protected String getFullyQualifiedCallerName() {
      return this.getClass().getName();
    }

    @Override
    protected void handleNormalizedLoggingCall(
        Level level, Marker marker, String s, Object[] objects, Throwable throwable) {
      calls++;
      latestLevel = level;
      latestString = s;
    }

    @Override
    public boolean isTraceEnabled() {
      return true;
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
      return true;
    }

    @Override
    public boolean isDebugEnabled() {
      return true;
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
      return true;
    }

    @Override
    public boolean isInfoEnabled() {
      return true;
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
      return true;
    }

    @Override
    public boolean isWarnEnabled() {
      return true;
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
      return true;
    }

    @Override
    public boolean isErrorEnabled() {
      return true;
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
      return true;
    }
  }
}
