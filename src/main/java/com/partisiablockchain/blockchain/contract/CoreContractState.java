package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;

/** The part of a blockchain contract that are not expected to change very often. */
@Immutable
public final class CoreContractState implements StateSerializable {

  private final Hash binderJarIdentifier;
  private final Hash contractJarIdentifier;
  private final Hash abiIdentifier;
  private final int codeSize;

  @SuppressWarnings("unused")
  CoreContractState() {
    binderJarIdentifier = null;
    contractJarIdentifier = null;
    abiIdentifier = null;
    codeSize = -1;
  }

  private CoreContractState(
      Hash binderJarIdentifier, Hash contractJarIdentifier, Hash abiIdentifier, int codeSize) {
    this.binderJarIdentifier = binderJarIdentifier;
    this.contractJarIdentifier = contractJarIdentifier;
    this.abiIdentifier = abiIdentifier;
    this.codeSize = codeSize;
  }

  /**
   * Create a new CoreContractState.
   *
   * @param binderJarIdentifier hash of the saved the binder jar
   * @param contractJarIdentifier hash of the saved the contract jar
   * @param abiIdentifier hash of the saved abi
   * @param codeSize size of the code in this contract, used for pricing
   * @return the created object
   */
  public static CoreContractState create(
      Hash binderJarIdentifier, Hash contractJarIdentifier, Hash abiIdentifier, int codeSize) {
    return new CoreContractState(
        binderJarIdentifier, contractJarIdentifier, abiIdentifier, codeSize);
  }

  /**
   * Get the unique identifier for the ABI of the contract.
   *
   * @return the identifier of the ABI
   */
  public Hash getAbiIdentifier() {
    return abiIdentifier;
  }

  /**
   * Get a unique identifier for the binder used by this contract.
   *
   * @return a hash of the binder jar
   */
  public Hash getBinderIdentifier() {
    return binderJarIdentifier;
  }

  /**
   * Get identifier of contract jar.
   *
   * @return identifying hash
   */
  @SuppressWarnings("WeakerAccess")
  public Hash getContractIdentifier() {
    return contractJarIdentifier;
  }

  /**
   * Compute the byte length in storage.
   *
   * @return byte length in storage
   */
  public int computeStorageLength() {
    return codeSize;
  }

  /**
   * Serializer for the internal state of a contract.
   *
   * @param <InnerStateT> the type of contract state.
   */
  public static final class ContractSerialization<InnerStateT extends StateSerializable> {

    /** Type-erased class of contract state. */
    private final Class<InnerStateT> type;

    /** Type parameters to apply to contract state class. */
    private final Class<?>[] generics;

    /**
     * Default constructor.
     *
     * @param type type-erased type of contract state.
     * @param generics type parameters to apply to contract state class
     */
    public ContractSerialization(Class<InnerStateT> type, Class<?>... generics) {
      this.type = type;
      this.generics = generics;
    }

    /**
     * Read a previously saved contract from the supplied storage.
     *
     * @param storage the storage containing the contract state
     * @param identifier the hash under which the contract state is saved
     * @return the read contract state
     */
    public InnerStateT read(StateSerializer storage, Hash identifier) {
      return storage.read(identifier, type, generics);
    }

    /**
     * Write the supplied contract state to the supplied storage.
     *
     * @param serializer the storage to write the contract state to
     * @param state the contract state to write
     * @return the hash identifier of the contract state
     */
    public SerializationResult write(StateSerializer serializer, StateSerializable state) {
      if (state != null && !type.equals(state.getClass())) {
        throw new IllegalStateException(
            "Expected contract state of %s but received state of %s"
                .formatted(type, state.getClass()));
      } else {
        return serializer.write(state, generics);
      }
    }
  }
}
