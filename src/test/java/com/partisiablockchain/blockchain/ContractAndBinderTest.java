package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test of {@link ContractAndBinder}. */
public final class ContractAndBinderTest {

  @Test
  public void equals() {
    final JarClassLoader binderClassLoader =
        JarClassLoader.builder(TestContracts.CONTRACT_BINDER_PUBLIC).build();
    final JarClassLoader contractClassLoader =
        JarClassLoader.builder(JarBuilder.buildJar(SomeContract.class))
            .defaultClassLoader(binderClassLoader)
            .build();

    EqualsVerifier.forClass(ContractAndBinder.class)
        .withPrefabValues(JarClassLoader.class, binderClassLoader, contractClassLoader)
        .verify();
  }

  /** Testing contract. */
  public static final class SomeContract extends PubContract<StateVoid> {}
}
