package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Stores latest storage time and the current gas balance for a specific contract.
 *
 * <p>Used in {@link AccountHolderAccountPlugin} for the contract state.
 */
@Immutable
public final class ContractStorage implements StateSerializable {

  /** Timestamp for the last time the contract paid for storage fees. */
  private final long timestampForLatestStorageFeePayment;

  /** Gas balance of the contract. */
  private final long balance;

  /** Default constructor for {@link ContractStorage} with all field set to zero. */
  public ContractStorage() {
    this(0L, 0L);
  }

  /**
   * Default constructor.
   *
   * @param timestampForLatestStorageFeePayment Timestamp for the last time the contract paid for
   *     storage fees.
   * @param balance Gas balance of the contract.
   */
  public ContractStorage(long timestampForLatestStorageFeePayment, long balance) {
    this.timestampForLatestStorageFeePayment = timestampForLatestStorageFeePayment;
    this.balance = balance;
  }

  /**
   * Gas balance of the contract.
   *
   * @return Gas balance of the contract.
   */
  public long getBalance() {
    return balance;
  }

  /**
   * Timestamp for the last time the contract paid for storage fees.
   *
   * @return Timestamp for the last time the contract paid for storage fees.
   */
  public long getTimestampForLatestStorageFeePayment() {
    return timestampForLatestStorageFeePayment;
  }

  /**
   * Update the balance of the contract.
   *
   * @param diff gas diff to apply
   * @return update contract storage
   */
  public ContractStorage updateBalance(long diff) {
    return new ContractStorage(timestampForLatestStorageFeePayment, balance + diff);
  }
}
