package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

final class KademliaBucket {

  private final ArrayList<NodeDescriptor> nodes;
  private final HashSet<KademliaNode> replacementCache = new HashSet<>();

  KademliaBucket() {
    nodes = new ArrayList<>(Kademlia.BUCKET_SIZE);
  }

  List<KademliaNode> getNodeCopy() {
    return nodes.stream()
        .filter(NodeDescriptor::isAlive)
        .map(NodeDescriptor::getNode)
        .collect(Collectors.toList());
  }

  List<KademliaNode> getAllNodes() {
    return nodes.stream().map(NodeDescriptor::getNode).collect(Collectors.toList());
  }

  synchronized void add(KademliaNode kademliaNode) {
    NodeDescriptor nodeDescriptor = new NodeDescriptor(kademliaNode);
    if (!nodes.contains(nodeDescriptor)) {
      if (nodes.size() < Kademlia.BUCKET_SIZE || removeStale()) {
        nodes.add(nodeDescriptor);
      } else {
        replacementCache.add(kademliaNode);
      }
    }
  }

  private synchronized boolean removeStale() {
    for (Iterator<NodeDescriptor> iterator = nodes.iterator(); iterator.hasNext(); ) {
      NodeDescriptor node = iterator.next();
      if (!node.isAlive()) {
        iterator.remove();
        return true;
      }
    }
    return false;
  }

  synchronized void error(KademliaNode node) {
    NodeDescriptor nodeDescriptor = new NodeDescriptor(node);
    int index = nodes.indexOf(nodeDescriptor);
    if (index > -1) {
      if (!replacementCache.isEmpty()) {
        nodes.remove(index);
        Iterator<KademliaNode> iterator = replacementCache.iterator();
        nodes.add(new NodeDescriptor(iterator.next()));
        iterator.remove();
      } else {
        NodeDescriptor descriptor = nodes.get(index);
        descriptor.noOfErrors++;
      }
    }
    replacementCache.remove(node);
  }

  boolean hasNodes() {
    return !nodes.isEmpty();
  }

  boolean contains(KademliaNode node) {
    return nodes.contains(new NodeDescriptor(node));
  }

  int errorCount(KademliaNode node) {
    int index = nodes.indexOf(new NodeDescriptor(node));
    return nodes.get(index).noOfErrors;
  }

  static final class NodeDescriptor {

    private final KademliaNode node;
    private int noOfErrors;

    NodeDescriptor(KademliaNode kademliaNode) {
      this.node = kademliaNode;
    }

    boolean isAlive() {
      return noOfErrors < 5;
    }

    KademliaNode getNode() {
      return node;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      NodeDescriptor that = (NodeDescriptor) o;
      return Objects.equals(node, that.node);
    }

    @Override
    public int hashCode() {

      return Objects.hash(node);
    }
  }
}
