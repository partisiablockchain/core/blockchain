package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.TlsUtils;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutionFactory;
import java.io.Closeable;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.bouncycastle.tls.Certificate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A node in the flooding network, allows owners to connect to the flooding network. */
public final class NetworkNode implements Closeable, BlockChainNetwork {

  private static final Logger logger = LoggerFactory.getLogger(NetworkNode.class);

  private final Server server;
  private final Client client;
  private final Map<Address, Neighbour> neighbours = new ConcurrentHashMap<>();
  private final String chainId;
  private final KeyPair signer;
  private final Consumer<IncomingPacket> incoming;
  private final Certificate selfSignedCertificate;

  /**
   * Creates a network node.
   *
   * @param networkConfig network configuration
   * @param chainId the current id of the chain
   * @param shard the shard of the ledger
   * @param signer the key pair for TLS handshake
   * @param incoming the queue for the received
   * @param registerProvider registers the node as status provider
   * @param executionFactory factory for creating threads for {@link Client} and {@link Server}
   */
  @SuppressWarnings("FutureReturnValueIgnored")
  public NetworkNode(
      NetworkConfig networkConfig,
      String chainId,
      String shard,
      KeyPair signer,
      Consumer<NetworkNode.IncomingPacket> incoming,
      Consumer<AdditionalStatusProvider> registerProvider,
      ExecutionFactory executionFactory) {
    this.selfSignedCertificate = createSelfSignedCertificate(signer);
    this.server =
        new Server(
            networkConfig.getNodeAddress().port(),
            this::setServerConnectedAddress,
            executionFactory);
    this.client =
        new Client(
            networkConfig.getKnownNodes(),
            networkConfig.getPersistentConnection(),
            networkConfig.getNodeAddress(),
            this::getConnectedNeighbours,
            this::setClientConnectedAddress,
            executionFactory);
    this.chainId = chainId;
    this.signer = signer;
    this.incoming = incoming;
    registerProvider.accept(new NetworkNodeStatusProvider(shard, this::getConnectedNeighbours));
  }

  NetworkNode(
      Server server,
      Client client,
      String chainId,
      KeyPair signer,
      Consumer<NetworkNode.IncomingPacket> incoming) {
    this.selfSignedCertificate = createSelfSignedCertificate(signer);
    this.server = server;
    this.client = client;
    this.chainId = chainId;
    this.signer = signer;
    this.incoming = incoming;
  }

  List<Neighbour> getConnectedNeighbours() {
    return neighbours.values().stream().filter(Neighbour::isConnected).collect(Collectors.toList());
  }

  Neighbour get(Address address) {
    return neighbours.get(address);
  }

  private Neighbour findExistingNeighbour(
      BlockchainPublicKey connectionPublicKey, boolean outbound) {
    for (Neighbour neighbour : neighbours.values()) {
      if (neighbour.isOutbound() == outbound
          && neighbour.getConnection().getConnectionPublicKey().equals(connectionPublicKey)) {
        return neighbour;
      }
    }
    return null;
  }

  synchronized void create(Address address, Connection connection, boolean outbound) {
    final BlockchainPublicKey connectingIdentity = connection.getConnectionPublicKey();
    Neighbour neighbour = findExistingNeighbour(connectingIdentity, outbound);
    if (neighbour != null) {
      logger.info(
          "Connection re-established to {} with public key {} (outbound={})",
          address,
          connectingIdentity,
          outbound);
      neighbour.close();
    } else {
      logger.info(
          "Connection established to {} with public key {} (outbound={})",
          address,
          connectingIdentity,
          outbound);
    }
    neighbours.put(address, new Neighbour(address, connection, outbound));
  }

  void setClientConnectedAddress(Address toConnect, Socket socket) {
    ExceptionLogger.handle(
        logger::warn,
        () -> {
          Connection connection =
              Connection.createClientConnection(
                  this::incoming, socket, chainId, signer, selfSignedCertificate);
          create(toConnect, connection, true);
          incoming(connection, SyncRequest.PACKET);
        },
        "Connection was not established to " + toConnect);
  }

  void setServerConnectedAddress(Address toConnect, Socket socket) {
    ExceptionLogger.handle(
        logger::debug,
        () -> {
          Connection connection =
              Connection.createServerConnection(
                  this::incoming, socket, chainId, signer, selfSignedCertificate);
          create(toConnect, connection, false);
          incoming(connection, SyncRequest.PACKET);
        },
        "Connection was not established to " + toConnect);
  }

  @Override
  public <T extends NetworkFloodable> void sendToAll(Packet<T> packet) {
    List<Connection> connections = getLiveConnections();
    for (Connection connection : connections) {
      outboundToConnection(packet, connection);
    }
  }

  @Override
  public void sendToAny(Packet<?> packet) {
    List<Connection> connections = getLiveConnections();
    if (!connections.isEmpty()) {
      int index = ThreadLocalRandom.current().nextInt(connections.size());
      Connection connection = connections.get(index);
      outboundToConnection(packet, connection);
    } else {
      logger.warn("Cannot send {} to an empty network", packet);
    }
  }

  private void outboundToConnection(Packet<?> packet, Connection connection) {
    connection.send(packet);
  }

  List<Connection> getLiveConnections() {
    removeClosedConnections();
    return neighbours.values().stream().map(Neighbour::getConnection).collect(Collectors.toList());
  }

  /** Removes closed connections in neighbours, so we don't keep its pending queue in memory. */
  private void removeClosedConnections() {
    neighbours.entrySet().removeIf(e -> !e.getValue().isConnected());
  }

  private void incoming(Connection sender, Packet<?> packet) {
    incoming.accept(new IncomingPacket(sender, packet));
  }

  @Override
  public void close() {
    client.close();
    server.close();
    neighbours.values().stream()
        .map(Neighbour::getConnection)
        .filter(Objects::nonNull)
        .forEach(Connection::close);
    removeClosedConnections();
  }

  private Certificate createSelfSignedCertificate(KeyPair signer) {
    return ExceptionConverter.call(
        () -> TlsUtils.createSelfSignedCertificate(signer),
        "Unable to create self signed certificate for public key");
  }

  Server getServer() {
    return server;
  }

  Client getClient() {
    return client;
  }

  /** A packet arrived in the network node. */
  public static final class IncomingPacket {

    private final Connection sender;
    private final Packet<?> packet;

    /**
     * Default constructor.
     *
     * @param sender who sent packet
     * @param packet incoming packet
     */
    public IncomingPacket(Connection sender, Packet<?> packet) {
      this.sender = sender;
      this.packet = packet;
    }

    /**
     * Get sender for incoming packet.
     *
     * @return connection to sender
     */
    public Connection getSender() {
      return sender;
    }

    /**
     * Get incoming packet.
     *
     * @return incoming packet
     */
    public Packet<?> getPacket() {
      return packet;
    }
  }
}
