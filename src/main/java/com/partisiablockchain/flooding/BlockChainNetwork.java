package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Describes the behaviour of the underlying network seen from the blockchain, the network can send
 * and receive, implementors must provide the send methods as well as expose the received packages.
 */
public interface BlockChainNetwork extends AutoCloseable {

  /**
   * Sends a packet to everyone known - same as flooding.
   *
   * @param packet the packet to send
   * @param <T> the type of packet to flood
   */
  <T extends NetworkFloodable> void sendToAll(Packet<T> packet);

  /**
   * Sends a packet to a random recipient - useful for asking for a single block/transaction.
   *
   * @param packet the packet to send
   */
  void sendToAny(Packet<?> packet);

  @Override
  void close();
}
