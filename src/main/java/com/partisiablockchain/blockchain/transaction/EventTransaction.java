package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** An event spawned from the blockchain. */
public final class EventTransaction implements DataStreamSerializable {

  private final long committeeId;
  private final long governanceVersion;
  private final Hash originatingTransaction;
  private final InnerEvent inner;
  private final ShardRoute shardRoute;
  private final ReturnEnvelope returnEnvelope;
  private final int height;

  private EventTransaction(
      Hash originatingTransaction,
      BlockchainAddress from,
      long cost,
      Transaction transaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion,
      int height,
      ReturnEnvelope returnEnvelope) {
    this(
        originatingTransaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        returnEnvelope,
        new InnerTransaction(from, cost, transaction));
  }

  private EventTransaction(
      Hash originatingTransaction,
      CallbackToContract transaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion,
      int height,
      ReturnEnvelope returnEnvelope) {
    this(
        originatingTransaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        returnEnvelope,
        transaction);
  }

  /**
   * Create a new event transaction.
   *
   * @param originatingTransaction the SignedTransaction initiating the tree of events that this
   *     event is a part of
   * @param shardRoute the shard this event is going to and nonce for this event
   * @param committeeId the committee id for the block producing this event
   * @param governanceVersion the version of governance when this event was produced
   * @param height current call height in the event stack
   * @param returnEnvelope callback if any
   * @param inner the actual inner transaction
   */
  public EventTransaction(
      Hash originatingTransaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion,
      int height,
      ReturnEnvelope returnEnvelope,
      InnerEvent inner) {
    this.committeeId = committeeId;
    this.originatingTransaction = originatingTransaction;
    this.shardRoute = shardRoute;
    this.governanceVersion = governanceVersion;
    this.inner = inner;
    this.height = height;
    this.returnEnvelope = returnEnvelope;
  }

  /**
   * Create a new event transaction.
   *
   * @param originatingTransaction the SignedTransaction initiating the tree of events that this
   *     event is a part of
   * @param from the sender
   * @param cost the cost this transaction may incur on fees
   * @param transaction the transaction to be executed in the event
   * @param shardRoute the shard this event is going to and nonce for this event
   * @param committeeId the commitee id for the block producing this event
   * @param governanceVersion governance version when this event was spawned
   * @param height current call height in the event stack
   * @param returnEnvelope callback if any
   * @return the created event transaction
   */
  public static EventTransaction create(
      Hash originatingTransaction,
      BlockchainAddress from,
      long cost,
      Transaction transaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion,
      int height,
      ReturnEnvelope returnEnvelope) {
    return new EventTransaction(
        originatingTransaction,
        from,
        cost,
        transaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        returnEnvelope);
  }

  /**
   * Create a new event transaction.
   *
   * @param originatingTransaction the SignedTransaction initiating the tree of events that this
   *     event is a part of
   * @param transaction the transaction to be executed in the event
   * @param shardRoute the shard this event is going to and nonce for this event
   * @param committeeId the commitee id for the block producing this event
   * @param governanceVersion governance version when this event was spawned
   * @param height current call height in the event stack
   * @return the created event transaction
   */
  public static EventTransaction create(
      Hash originatingTransaction,
      CallbackToContract transaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion,
      int height) {
    return new EventTransaction(
        originatingTransaction,
        transaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        null);
  }

  /**
   * Create a new event transaction.
   *
   * @param originatingTransaction the SignedTransaction initiating the tree of events that this
   *     event is a part of
   * @param from the sender
   * @param cost the cost this transaction will incur on fees
   * @param transaction the transaction to be executed in the event
   * @param shardRoute the shard this event is going to and nonce for this event
   * @param committeeId the committee id for the block producing this event
   * @param governanceVersion governance version when this event was spawend
   * @return the created event transaction
   */
  public static EventTransaction createStandalone(
      Hash originatingTransaction,
      BlockchainAddress from,
      long cost,
      Transaction transaction,
      ShardRoute shardRoute,
      long committeeId,
      long governanceVersion) {
    return new EventTransaction(
        originatingTransaction,
        from,
        cost,
        transaction,
        shardRoute,
        committeeId,
        governanceVersion,
        0,
        null);
  }

  /**
   * Read an EventTransaction from the supplied stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static EventTransaction read(SafeDataInputStream stream) {
    Hash originatingTransaction = Hash.read(stream);
    InnerEvent inner = InnerEvent.read(stream);
    ShardRoute shardRoute = ShardRoute.read(stream);
    long committeeId = stream.readLong();
    long governanceVersion = stream.readLong();
    int height = stream.readUnsignedByte();
    ReturnEnvelope returnEnvelope = stream.readOptional(ReturnEnvelope::read);
    return new EventTransaction(
        originatingTransaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        returnEnvelope,
        inner);
  }

  /**
   * Read an EventTransaction from the supplied stream without checking data stream limits.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static EventTransaction unsafeSyncRead(SafeDataInputStream stream) {
    Hash originatingTransaction = Hash.read(stream);
    InnerEvent inner = InnerEvent.unsafeSyncRead(stream);
    ShardRoute shardRoute = ShardRoute.read(stream);
    long committeeId = stream.readLong();
    long governanceVersion = stream.readLong();
    int height = stream.readUnsignedByte();
    ReturnEnvelope returnEnvelope = stream.readOptional(ReturnEnvelope::read);
    return new EventTransaction(
        originatingTransaction,
        shardRoute,
        committeeId,
        governanceVersion,
        height,
        returnEnvelope,
        inner);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    originatingTransaction.write(stream);
    inner.write(stream);
    shardRoute.write(stream);
    stream.writeLong(committeeId);
    stream.writeLong(governanceVersion);
    stream.writeByte(height);
    stream.writeOptional(ReturnEnvelope::write, returnEnvelope);
  }

  /**
   * Get inner event.
   *
   * @return inner event
   */
  public InnerEvent getInner() {
    return inner;
  }

  /**
   * Get original transaction.
   *
   * @return hash identifier
   */
  public Hash getOriginatingTransaction() {
    return originatingTransaction;
  }

  /**
   * Get destination shard from route.
   *
   * @return destination shard
   */
  public String getDestinationShard() {
    return shardRoute.targetShard;
  }

  /**
   * Get current call height in the event stack.
   *
   * @return current call height in the event stack
   */
  public int getHeight() {
    return height;
  }

  /**
   * Get information about who requires a callback about the execution of a transaction.
   *
   * @return return envelope
   */
  public ReturnEnvelope getReturnEnvelope() {
    return returnEnvelope;
  }

  /**
   * Get nonce of unique route to shard.
   *
   * @return nonce of unique route to shard
   */
  public long getNonce() {
    return shardRoute.nonce;
  }

  /**
   * Get identifier of this event transaction.
   *
   * @return identifying hash
   */
  public Hash identifier() {
    return Hash.create(this);
  }

  /**
   * Get id of committee.
   *
   * @return id of committee
   */
  public long getCommitteeId() {
    return committeeId;
  }

  @Override
  public String toString() {
    return "EventTransaction{"
        + "inner="
        + inner
        + ", transaction="
        + identifier()
        + ", shardRoute="
        + shardRoute
        + '}';
  }

  /**
   * Get version of governance.
   *
   * @return version of governance
   */
  public long getGovernanceVersion() {
    return governanceVersion;
  }

  /** The type of events. */
  public enum EventType {
    /** Transaction. */
    TRANSACTION {
      @Override
      InnerEvent read(SafeDataInputStream stream) {
        return InnerTransaction.read(stream);
      }
    },
    /** Callback. */
    CALLBACK {
      @Override
      InnerEvent read(SafeDataInputStream stream) {
        return CallbackToContract.readInner(stream);
      }
    },
    /** System. */
    SYSTEM {
      @Override
      InnerEvent read(SafeDataInputStream stream) {
        return InnerSystemEvent.read(stream);
      }
    },
    /** Sync. */
    SYNC {
      @Override
      InnerEvent read(SafeDataInputStream stream) {
        return SyncEvent.read(stream);
      }
    };

    abstract InnerEvent read(SafeDataInputStream stream);
  }

  /** The actual event. */
  public interface InnerEvent extends DataStreamSerializable {

    /**
     * Read event from stream.
     *
     * @param safeDataInputStream to read from
     * @return read event
     */
    static InnerEvent read(SafeDataInputStream safeDataInputStream) {
      EventType type = safeDataInputStream.readEnum(EventType.values());
      return type.read(safeDataInputStream);
    }

    /**
     * Read event from stream without checkout data stream limits.
     *
     * @param safeDataInputStream to read from
     * @return read event
     */
    static InnerEvent unsafeSyncRead(SafeDataInputStream safeDataInputStream) {

      EventType type = safeDataInputStream.readEnum(EventType.values());
      if (type != EventType.SYNC) {
        return type.read(safeDataInputStream);
      } else {
        return SyncEvent.unsafeRead(safeDataInputStream);
      }
    }

    @Override
    default void write(SafeDataOutputStream safeDataOutputStream) {
      safeDataOutputStream.writeEnum(getEventType());
      writeInner(safeDataOutputStream);
    }

    /**
     * Write inner event to a stream.
     *
     * @param stream where to write
     */
    void writeInner(SafeDataOutputStream stream);

    /**
     * Get type of inner event.
     *
     * @return event type
     */
    EventType getEventType();

    /**
     * Get target of inner event.
     *
     * @return blockchain address
     */
    default BlockchainAddress target() {
      return null;
    }
  }

  /** Marker interface for events that interacts with a contract. */
  public interface ContractInnerEvent extends InnerEvent {}

  /**
   * An event that is a {@link Transaction}. Also caries an associated sender and an associated
   * cost.
   */
  public static final class InnerTransaction implements ContractInnerEvent {

    private final BlockchainAddress from;
    private final long cost;
    private final Transaction transaction;

    private InnerTransaction(BlockchainAddress from, long cost, Transaction transaction) {
      this.from = from;
      this.cost = cost;
      this.transaction = transaction;
    }

    static InnerTransaction read(SafeDataInputStream stream) {
      BlockchainAddress from = BlockchainAddress.read(stream);
      long cost = stream.readLong();
      Transaction transaction = Transaction.read(stream);
      return new InnerTransaction(from, cost, transaction);
    }

    @Override
    public void writeInner(SafeDataOutputStream stream) {
      from.write(stream);
      stream.writeLong(cost);
      transaction.write(stream);
    }

    /**
     * Get sender's address.
     *
     * @return sender's address
     */
    public BlockchainAddress getSender() {
      return from;
    }

    /**
     * Get cost of transaction.
     *
     * @return cost
     */
    public long getCost() {
      return cost;
    }

    /**
     * Get transaction.
     *
     * @return transaction
     */
    public Transaction getTransaction() {
      return transaction;
    }

    @Override
    public EventType getEventType() {
      return EventType.TRANSACTION;
    }

    @Override
    public String toString() {
      return "InnerTransaction{" + "from=" + from + ", cost=" + cost + '}';
    }

    @Override
    public BlockchainAddress target() {
      return getTransaction().getTargetContract();
    }
  }
}
