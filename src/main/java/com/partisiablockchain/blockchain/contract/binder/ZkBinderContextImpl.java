package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;

final class ZkBinderContextImpl extends BinderContextImpl implements ZkBinderContext {

  ZkBinderContextImpl(
      BlockchainContractContext contractContext, ExecutionContextTransaction executionContext) {
    super(contractContext, executionContext);
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    getExecutionContext().addPendingServiceFees(target, gas);
  }

  @Override
  public long availableGas() {
    return getExecutionContext().availableGas();
  }

  @Override
  public void registerCpuFee(long gas) {
    getExecutionContext().registerCpuFee(gas);
  }
}
