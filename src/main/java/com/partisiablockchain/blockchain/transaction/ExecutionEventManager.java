package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/** Event manager responsible for every event created during an execution. */
public final class ExecutionEventManager {

  private final Supplier<EventCollector> eventCollectorCreator;
  private final List<EventCollector> eventCreators;

  /**
   * Creates a new ExecutionEventManager responsible for every event created during an execution.
   *
   * @param eventCollectorCreator creates event collectors, indicate true for callback active
   */
  public ExecutionEventManager(Supplier<EventCollector> eventCollectorCreator) {
    this.eventCollectorCreator = eventCollectorCreator;
    this.eventCreators = new ArrayList<>();
  }

  /**
   * Creates a new event collector.
   *
   * @return the created event collector
   */
  public EventCollector createCollector() {
    EventCollector eventCollector = eventCollectorCreator.get();
    eventCreators.add(eventCollector);
    return eventCollector;
  }

  /**
   * Returns the list of events generated in this execution.
   *
   * @return the list of generated events
   */
  public List<ExecutableEvent> getTransactions() {
    List<ExecutableEvent> list = new ArrayList<>();
    for (EventCollector eventCreator : eventCreators) {
      list.addAll(eventCreator.getTransactions());
    }
    return list;
  }
}
