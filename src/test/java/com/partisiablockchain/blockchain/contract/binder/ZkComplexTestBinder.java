package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.blockchain.contract.UpgradableContract;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.contract.zk.ZkContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import java.util.function.Supplier;

/** Test binder. */
public final class ZkComplexTestBinder<OpenT extends StateSerializable>
    extends AbstractZkContractBinder<OpenT> {

  /**
   * Construct a new binder.
   *
   * @param contract the associated contract
   */
  public ZkComplexTestBinder(
      ZkContract<OpenT, StateVoid, ZkClosed<StateVoid>, Supplier<String>> contract) {
    super(contract);
  }

  @SuppressWarnings("unchecked")
  @Override
  public ContractUpgradePermit acquireUpgradePermit(
      ZkBinderContext binderContext,
      OpenT state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    AbstractZkContractBinder.ZkContext context = new ZkContext(binderContext);
    boolean upgradeIsAllowed =
        ((UpgradableContract<ZkContractContext, OpenT>) contract)
            .onAcquireUpgradePermit(
                context, state, oldContractBytecodeIds, newContractBytecodeIds, rpc);
    if (upgradeIsAllowed) {
      return new ContractUpgradePermit(new byte[0]);
    } else {
      throw new RuntimeException("Contract upgrade is not allowed.");
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public OpenT upgradeState(
      ZkBinderContext binderContext,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    AbstractZkContractBinder.ZkContext context = new ZkContext(binderContext);
    return ((UpgradableContract<ZkContractContext, OpenT>) contract)
        .onUpgradeState(context, oldState, permit, rpc);
  }
}
