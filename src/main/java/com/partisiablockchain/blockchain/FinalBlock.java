package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.NetworkFloodable;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** A final block contains a block along with the finalization data. */
public final class FinalBlock implements NetworkFloodable {

  private final Block block;
  private final byte[] finalizationData;

  /**
   * Create final block.
   *
   * @param block to make final
   * @param finalizationData data for finalization
   */
  public FinalBlock(Block block, byte[] finalizationData) {
    this.block = block;
    this.finalizationData = finalizationData.clone();
  }

  /**
   * Get block to make final.
   *
   * @return block to make final
   */
  public Block getBlock() {
    return block;
  }

  /**
   * Get finalization data.
   *
   * @return finalization data
   */
  public byte[] getFinalizationData() {
    return finalizationData.clone();
  }

  /**
   * Read a FinalizedBlock from the supplied stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static FinalBlock read(SafeDataInputStream stream) {
    Block read = Block.read(stream);
    byte[] finalizationData = DataStreamLimit.readDynamicBytes(stream);
    return new FinalBlock(read, finalizationData);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    block.write(stream);
    stream.writeDynamicBytes(finalizationData);
  }

  @Override
  public String toString() {
    return "FinalBlock{blockTime="
        + block.getBlockTime()
        + ", transactions="
        + block.getTransactions().size()
        + ", events="
        + block.getEventTransactions().size()
        + ", hash="
        + block.identifier()
        + '}';
  }
}
