package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.FinalizationData;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEventCompressed;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A packet in the flooding network, can contain be any type T.
 *
 * @param <T> the type of packet.
 */
public final class Packet<T extends DataStreamSerializable> {

  private final Type<T> type;
  private final T payload;

  /**
   * Default constructor.
   *
   * @param type type of packet
   * @param payload payload of packet
   */
  public Packet(Type<T> type, T payload) {
    this.type = type;
    this.payload = payload;
  }

  void send(SafeDataOutputStream stream) {
    stream.writeByte(type.getType());
    payload.write(stream);
  }

  /**
   * Get payload of packet.
   *
   * @return payload of packet
   */
  public T getPayload() {
    return payload;
  }

  /**
   * Get type of packet.
   *
   * @return type of packet
   */
  public Type<T> getType() {
    return type;
  }

  /**
   * The types of packets in the flooding network.
   *
   * @param <T> the type of packet
   */
  public static final class Type<T extends DataStreamSerializable> {

    /** Transaction type. */
    public static final Type<SignedTransaction> TRANSACTION =
        new Packet.Type<>(0, SignedTransaction::read);

    /** Block type. */
    public static final Type<FinalBlock> BLOCK = new Packet.Type<>(1, FinalBlock::read);

    /** Block request type. */
    public static final Type<BlockRequest> BLOCK_REQUEST = new Type<>(2, BlockRequest::read);

    /** Block response type. */
    public static final Type<BlockResponse> BLOCK_RESPONSE = new Type<>(3, BlockResponse::read);

    /** Finalization type. */
    public static final Type<FinalizationData> FINALIZATION = new Type<>(4, FinalizationData::read);

    /** Sync type. */
    public static final Type<SyncRequest> SYNC = new Type<>(5, stream -> SyncRequest.read());

    /** Event type. */
    public static final Type<FloodableEvent> EVENT = new Type<>(6, FloodableEvent::read);

    /** Compressed event type. */
    public static final Type<FloodableEventCompressed> COMPRESSED_EVENT =
        new Type<>(7, FloodableEventCompressed::read);

    /** Block response compressed type. */
    public static final Type<BlockResponseSemiCompressed> COMPRESSED_BLOCK_RESPONSE =
        new Type<>(8, BlockResponseSemiCompressed::read);

    private static final List<Type<?>> values =
        List.of(
            TRANSACTION,
            BLOCK,
            BLOCK_REQUEST,
            BLOCK_RESPONSE,
            FINALIZATION,
            SYNC,
            EVENT,
            COMPRESSED_EVENT,
            COMPRESSED_BLOCK_RESPONSE);

    static <T extends DataStreamSerializable> BiFunction<String, SafeDataInputStream, T> biFunction(
        Function<SafeDataInputStream, T> read) {
      return (string, stream) -> read.apply(stream);
    }

    private final int type;
    private final BiFunction<String, SafeDataInputStream, T> reader;

    private Type(int type, Function<SafeDataInputStream, T> reader) {
      this(type, biFunction(reader));
    }

    private Type(int type, BiFunction<String, SafeDataInputStream, T> reader) {
      this.type = type;
      this.reader = reader;
    }

    Packet<T> parse(String chainId, SafeDataInputStream stream) {
      T payload = reader.apply(chainId, stream);
      return new Packet<>(this, payload);
    }

    int getType() {
      return type;
    }

    /** Parses a type from the given ordinal. */
    static Type<?> parseInt(int type) {
      return values.get(type);
    }
  }
}
