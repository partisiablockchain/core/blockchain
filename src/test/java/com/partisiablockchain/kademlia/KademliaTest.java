package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.ThreadedTestHelper;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.kademlia.Kademlia.Message;
import com.partisiablockchain.kademlia.Kademlia.MessageIdentifier;
import com.partisiablockchain.kademlia.Kademlia.MessageType;
import com.partisiablockchain.kademlia.LookupTask.ResolvableLookup;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test of {@link Kademlia}. */
public final class KademliaTest {

  private static final int port = 7788;
  private static final String otherHexId =
      "107df7bff5b930eff2b358f310720f90ce30d25191cab359269046bb1d776bc0";

  private final LinkedBlockingQueue<DatagramPacket> pending = new LinkedBlockingQueue<>();
  private final List<Kademlia> opened = new ArrayList<>();
  private final ScheduledExecutorService executorService =
      Mockito.mock(ScheduledExecutorService.class);
  private Kademlia kademlia;
  private KademliaKey id;
  private DatagramSocket socket;
  private String hexId;
  private final KademliaKey otherId = new KademliaKey(Hex.decode(otherHexId));
  private final InetAddress local = getLocal();
  private final KademliaNode otherNode = new KademliaNode(otherId, local, 1234);

  private InetAddress getLocal() throws UnknownHostException {
    return InetAddress.getByAddress(new byte[] {127, 0, 0, 1});
  }

  private int receiveInvocations = 0;

  /**
   * Default constructor.
   *
   * @throws Exception on error in setup
   */
  public KademliaTest() throws Exception {}

  /** Setup. */
  @BeforeEach
  public void setUp() throws Exception {
    hexId = "007df7bff5b930eff2b358f310720f90ce30d25191cab359269046bb1d776bc0";
    id = new KademliaKey(Hex.decode(hexId));
    socket = Mockito.mock(DatagramSocket.class);
    Mockito.doAnswer(
            invocation -> {
              receiveInvocations++;
              DatagramPacket argument = invocation.getArgument(0);
              DatagramPacket toSend = pending.take();
              argument.setAddress(toSend.getAddress());
              argument.setPort(toSend.getPort());

              System.arraycopy(toSend.getData(), 0, argument.getData(), 0, toSend.getLength());
              argument.setLength(toSend.getLength());
              return null;
            })
        .when(socket)
        .receive(Mockito.any(DatagramPacket.class));
    KademliaNode node = new KademliaNode(id, local, port);
    kademlia = new Kademlia(id, List.of(node), socket, executorService);
    opened.add(kademlia);
  }

  /** Tear down. */
  @AfterEach
  public void tearDown() {
    opened.forEach(Kademlia::close);
    receiveSingle();
  }

  private int packets = 0;

  private void send(DatagramPacket packet) {
    packets++;
    pending.add(packet);
    int waitFor = packets + 1;
    ThreadedTestHelper.waitForCondition(() -> this.receiveInvocations == waitFor);
  }

  private void receiveSingle() {
    pending.add(new DatagramPacket(new byte[0], 0, local, 2000));
  }

  @Test
  public void messageIdentifierEquals() {
    EqualsVerifier.forClass(MessageIdentifier.class).verify();
  }

  @Test
  public void messageIdIsRandom() {
    Message first = new Message(id, MessageType.PING_REPLY, new byte[0]);
    Message second = new Message(id, MessageType.PING_REPLY, new byte[0]);

    assertThat(first.getMessageId()).isNotEqualTo(second.getMessageId());
  }

  @Test
  public void unexpectedIncomming() throws Exception {
    byte[] incommingId = new byte[20];
    incommingId[0] = 42;
    Message lookupReply =
        Kademlia.createLookupReply(
            otherId,
            incommingId,
            List.of(new KademliaNode(otherId, otherNode.getAddress(), otherNode.getPort())));
    kademlia.handleIncoming(lookupReply, otherNode);
    byte[] pingReply =
        Hex.decode("02" + otherHexId + "00000014" + "0000000000000000000000000000121212121212");
    send(
        new DatagramPacket(
            pingReply, pingReply.length, otherNode.getAddress(), otherNode.getPort()));

    assertThat(kademlia.getBuckets().get(id.distance(otherId)).contains(otherNode)).isFalse();
  }

  @Test
  public void malformedIncomming() {
    byte[] bytes = new byte[22];
    DatagramPacket packet = new DatagramPacket(bytes, bytes.length, local, 1331);
    send(packet);

    assertThat(kademlia.getInbox().isRunning()).isTrue();
  }

  @Test
  public void containsCloser() {
    KademliaKey kademliaKey = new KademliaKey();
    Comparator<KademliaNode> comparator = Kademlia.createComparator(kademliaKey);
    TreeSet<KademliaNode> seenNodes = new TreeSet<>(comparator);
    TreeSet<KademliaNode> newNodes = new TreeSet<>(comparator);

    assertThat(Kademlia.containsCloser(newNodes, seenNodes)).isFalse();

    newNodes.add(new KademliaNode(kademliaKey.createForDistance(2), local, 0));

    assertThat(Kademlia.containsCloser(newNodes, seenNodes)).isTrue();

    seenNodes.addAll(newNodes);

    assertThat(Kademlia.containsCloser(newNodes, seenNodes)).isFalse();
  }

  @Test
  public void compareRelativeToThis() {
    KademliaKey kademliaKey = new KademliaKey(new byte[32]);
    byte[] first = new byte[32];
    Arrays.fill(first, (byte) -1);
    byte[] second = new byte[32];
    Arrays.fill(second, (byte) 1);
    int i = kademliaKey.compareRelativeToThis(new KademliaKey(first), new KademliaKey(second));
    assertThat(i).isEqualTo(-1);
  }

  @Test
  public void shouldScheduleInitialLookup() {
    LookupTask taskCaptor = getScheduledLookup();

    assertThat(taskCaptor.getToLookup()).isEqualTo(id);
  }

  @Test
  public void shouldScheduleBucketRefresh() {
    Mockito.verify(executorService)
        .scheduleWithFixedDelay(
            Mockito.any(RefreshBucketsTask.class),
            Mockito.eq(1L),
            Mockito.eq(60L),
            Mockito.eq(TimeUnit.MINUTES));
  }

  @Test
  public void outboundLookup() throws Exception {
    final KademliaKey otherId =
        new KademliaKey(
            Hex.decode("107df7bff5b930eff2b358f310720f90ce30d25191cab359269046bb1d776bc0"));
    KademliaKey toLookup = new KademliaKey(new byte[32]);
    LookupTask lookupTask = new LookupTask(kademlia::sendLookupMessage, toLookup, id, List.of());
    final KademliaNode node = new KademliaNode(toLookup, local, 1234);
    ResolvableLookup resolvableLookup = new ResolvableLookup(lookupTask);
    kademlia.sendLookupMessage(resolvableLookup, otherNode, toLookup);
    verifyTimeoutScheduled();

    DatagramPacket packet = getSentPacket();
    int length = packet.getLength();
    assertThat(Hex.toHexString(Arrays.copyOfRange(packet.getData(), 0, length - 20)))
        .isEqualTo(
            "00007df7bff5b930eff2b358f310720f90ce30d25191cab359269046bb1d776bc0000000"
                + "200000000000000000000000000000000000000000000000000000000000000000");
    byte[] messageId = Arrays.copyOfRange(packet.getData(), length - 20, length);

    Message lookupReply = Kademlia.createLookupReply(otherId, messageId, List.of(node));
    kademlia.handleIncoming(lookupReply, otherNode);

    // Should resubmit the task
    Mockito.verify(executorService).submit(lookupTask);
    assertThat(resolvableLookup.getNodes()).containsExactly(node);

    assertThat(kademlia.getBuckets().get(id.distance(otherId)).contains(otherNode)).isTrue();

    DatagramPacket pingReply = getSentPacket();
    byte[] data = Arrays.copyOfRange(pingReply.getData(), 0, pingReply.getLength() - 20);
    assertThat(Hex.toHexString(data))
        .isEqualTo("02007df7bff5b930eff2b358f310720f90ce30d25191cab359269046bb1d776bc000000014");
    assertThat(
            Arrays.copyOfRange(
                pingReply.getData(), pingReply.getLength() - 20, pingReply.getLength()))
        .isEqualTo(lookupReply.getMessageId());
  }

  @Test
  public void inboundLookup() throws Exception {
    String messageId = "10720f90ce30d25191cab359269046bb1d776bc0";
    byte[] lookupMessage =
        Hex.decode(
            "00"
                + otherHexId
                + "00000020"
                + "0000000000000000000000000000000000000000000000000000000000000000"
                + messageId);
    send(
        new DatagramPacket(
            lookupMessage, lookupMessage.length, otherNode.getAddress(), otherNode.getPort()));
    final Runnable timeoutRunnable = verifyTimeoutScheduled();
    DatagramPacket packet = getSentPacket();
    byte[] lookupReply =
        Hex.decode("01" + hexId + "0000003a" + messageId + hexId + "7f000001" + "1e6c");
    int length = packet.getLength();

    assertThat(Arrays.copyOfRange(packet.getData(), 0, length - 20)).isEqualTo(lookupReply);
    byte[] lookupReplyMessageId = Arrays.copyOfRange(packet.getData(), length - 20, length);
    byte[] pingReply =
        Hex.decode("02" + otherHexId + "00000014" + Hex.toHexString(lookupReplyMessageId));
    send(
        new DatagramPacket(
            pingReply, pingReply.length, otherNode.getAddress(), otherNode.getPort()));

    assertThat(kademlia.getBuckets().get(id.distance(otherId)).contains(otherNode)).isTrue();

    timeoutRunnable.run();
    assertThat(kademlia.getBuckets().get(id.distance(otherId)).errorCount(otherNode)).isEqualTo(0);
  }

  @Test
  public void timeoutPing() {
    kademlia.getBuckets().get(id.distance(otherId)).add(otherNode);

    String messageId = "10720f90ce30d25191cab359269046bb1d776bc0";
    byte[] lookupMessage =
        Hex.decode(
            "00"
                + otherHexId
                + "00000020"
                + "0000000000000000000000000000000000000000000000000000000000000000"
                + messageId);
    send(
        new DatagramPacket(
            lookupMessage, lookupMessage.length, otherNode.getAddress(), otherNode.getPort()));

    Runnable timeoutRunnable = verifyTimeoutScheduled();
    timeoutRunnable.run();

    assertThat(kademlia.getBuckets().get(id.distance(otherId)).errorCount(otherNode)).isEqualTo(1);
  }

  @Test
  public void timeoutLookup() throws Exception {
    kademlia.getBuckets().get(id.distance(otherId)).add(otherNode);

    KademliaKey toLookup = new KademliaKey(new byte[32]);
    LookupTask lookupTask = new LookupTask(kademlia::sendLookupMessage, toLookup, id, List.of());

    kademlia.sendLookupMessage(new ResolvableLookup(lookupTask), otherNode, toLookup);

    Runnable timeoutRunnable = verifyTimeoutScheduled();
    timeoutRunnable.run();

    // Should resubmit the task
    Mockito.verify(executorService).submit(lookupTask);

    assertThat(kademlia.getBuckets().get(id.distance(otherId)).errorCount(otherNode)).isEqualTo(1);
  }

  @Test
  public void shouldContinueOnError() {
    receiveSingle();

    ThreadedTestHelper.waitForCondition(() -> receiveInvocations == 2);
  }

  private Runnable verifyTimeoutScheduled() {
    ArgumentCaptor<Runnable> timeout = ArgumentCaptor.forClass(Runnable.class);
    Mockito.verify(executorService)
        .schedule(timeout.capture(), Mockito.eq(500L), Mockito.eq(TimeUnit.MILLISECONDS));
    return timeout.getValue();
  }

  @Test
  public void close() {
    assertThat(kademlia.getInbox().isRunning()).isTrue();
    kademlia.close();
    Mockito.verify(executorService).shutdownNow();
    Mockito.verify(socket).close();
    assertThat(kademlia.getInbox().isRunning()).isFalse();
  }

  @Test
  public void randomAddress() {
    KademliaKey first = new KademliaKey();
    KademliaKey second = new KademliaKey();
    kademlia.getBuckets().get(id.distance(first)).add(new KademliaNode(first, local, 1144));
    kademlia.getBuckets().get(id.distance(second)).add(new KademliaNode(second, local, 1143));

    Set<Integer> seen = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      Address randomAddress = kademlia.getRandomAddress();
      assertThat(randomAddress).isNotNull();
      seen.add(randomAddress.port());
      assertThat(randomAddress.hostname()).isEqualTo(local.getHostAddress());
    }
    assertThat(seen).containsExactlyInAnyOrder(1143, 1144, port);
  }

  @Test
  public void emptyRandomAddress() {
    Kademlia emptyKademlia = new Kademlia(id, ObjectCreator.port(), List.of());
    Address randomAddress = emptyKademlia.getRandomAddress();
    assertThat(randomAddress).isNull();
  }

  @Test
  public void getId() {
    assertThat(kademlia.getId()).isEqualTo(id);
  }

  private int sentPackets = 0;

  private DatagramPacket getSentPacket() throws IOException {
    sentPackets++;
    ArgumentCaptor<DatagramPacket> taskCaptor = ArgumentCaptor.forClass(DatagramPacket.class);
    Mockito.verify(socket, Mockito.times(sentPackets)).send(taskCaptor.capture());
    return taskCaptor.getValue();
  }

  private LookupTask getScheduledLookup() {
    ArgumentCaptor<LookupTask> taskCaptor = ArgumentCaptor.forClass(LookupTask.class);
    Mockito.verify(executorService).submit(taskCaptor.capture());
    return taskCaptor.getValue();
  }
}
