package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class TransactionCostTest {

  @Test
  public void getCpuFee() {
    Map<Hash, Long> networkCost = new HashMap<>();
    TransactionCost transactionCost = new TransactionCost(0, 88, 0, 0, networkCost);

    Assertions.assertThat(transactionCost.getCpu()).isEqualTo(88);
  }

  @Test
  public void getStorageFee() {
    Map<Hash, Long> networkCost = new HashMap<>();
    TransactionCost transactionCost = new TransactionCost(0, 0, 343, 0, networkCost);

    Assertions.assertThat(transactionCost.getRemaining()).isEqualTo(343);
  }

  @Test
  public void getAllocated() {
    Map<Hash, Long> networkCost = new HashMap<>();
    TransactionCost transactionCost = new TransactionCost(754, 88, 0, 0, networkCost);

    Assertions.assertThat(transactionCost.getAllocatedForEvents()).isEqualTo(754);
  }

  @Test
  public void getNetworkCosts() {
    Map<Hash, Long> networkCost = new HashMap<>();
    networkCost.put(Hash.fromString("a".repeat(64)), 5L);
    networkCost.put(Hash.fromString("b".repeat(64)), 7L);
    networkCost.put(Hash.fromString("c".repeat(64)), 3L);
    TransactionCost transactionCost = new TransactionCost(0, 0, 0, 0, networkCost);

    Assertions.assertThat(transactionCost.getNetworkCosts())
        .containsExactlyInAnyOrderEntriesOf(networkCost);
  }

  @Test
  public void calculateNetworkCost() {
    Map<Hash, Long> networkCost = new HashMap<>();
    networkCost.put(Hash.fromString("a".repeat(64)), 5L);
    networkCost.put(Hash.fromString("b".repeat(64)), 7L);
    networkCost.put(Hash.fromString("c".repeat(64)), 3L);
    TransactionCost transactionCost = new TransactionCost(0, 0, 0, 0, networkCost);

    Assertions.assertThat(transactionCost.getTotalNetworkCost()).isEqualTo(15);
  }

  @Test
  public void getTotalCost() {
    Map<Hash, Long> networkCost = new HashMap<>();
    networkCost.put(Hash.fromString("a".repeat(64)), 11L);
    networkCost.put(Hash.fromString("b".repeat(64)), 35L);
    networkCost.put(Hash.fromString("c".repeat(64)), 74L);
    TransactionCost transactionCost = new TransactionCost(0, 45, 0, 0, networkCost);

    Assertions.assertThat(transactionCost.getTotalCost()).isEqualTo(165);
  }

  @Test
  public void addTransactionCost() {
    TransactionCost empty = new TransactionCost(0, 0, 0, 0, Map.of());
    Assertions.assertThat(empty.getTotalNetworkCost()).isEqualTo(0);

    Map<Hash, Long> networkCost = new HashMap<>();
    networkCost.put(Hash.fromString("a".repeat(64)), 12L);

    TransactionCost withEvents = new TransactionCost(0, 0, 0, 0, networkCost);
    Assertions.assertThat(withEvents.getTotalNetworkCost()).isEqualTo(12);
  }

  @Test
  public void readFromInput() {
    TransactionCost expected =
        new TransactionCost(444, 555, 666, 777, Map.of(TestObjects.hashNumber(1), 888L));

    SafeDataInputStream bytes =
        SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(expected::write));

    TransactionCost actual = TransactionCost.read(bytes);
    Assertions.assertThat(actual.getAllocatedForEvents()).isEqualTo(444);
    Assertions.assertThat(actual.getCpu()).isEqualTo(555);
    Assertions.assertThat(actual.getRemaining()).isEqualTo(666);
    Assertions.assertThat(actual.getPaidByContract()).isEqualTo(777);
    Assertions.assertThat(actual.getTotalNetworkCost()).isEqualTo(888);
    Assertions.assertThat(actual.getTotalCost()).isEqualTo(expected.getTotalCost());
  }
}
