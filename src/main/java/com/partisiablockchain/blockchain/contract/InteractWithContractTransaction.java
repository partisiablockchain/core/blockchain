package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContextImpl;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.Transaction;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/**
 * Interact with contract transaction allows to do any interaction with a contract. rpc bytes
 * defines the input to the interaction.
 */
public final class InteractWithContractTransaction implements Transaction {

  private final BlockchainAddress address;
  private final byte[] rpc;

  private InteractWithContractTransaction(BlockchainAddress address, byte[] payload) {
    this.address = address;
    this.rpc = payload;
  }

  /**
   * Get address of contract to interact with.
   *
   * @return address of contract
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Get the rpc payload.
   *
   * @return payload as bytes
   */
  public byte[] getRpc() {
    return rpc.clone();
  }

  @Override
  public BlockchainAddress getTargetContract() {
    return address;
  }

  @Override
  public EventResult<? extends BinderEvent> execute(ExecutionContextTransaction executionContext) {
    return typedInvoke(executionContext);
  }

  @SuppressWarnings("unchecked")
  private <T extends StateSerializable> EventResult<? extends BinderEvent> typedInvoke(
      ExecutionContextTransaction executionContext) {
    MutableChainState state = executionContext.getState();

    BlockchainContract<T, ? extends BinderEvent> binder = state.getBlockchainContract(address);
    T contract = (T) state.getContractState(address);

    BlockchainContractContextImpl context = new BlockchainContractContextImpl(address);

    BlockchainResult<T, ? extends BinderEvent> result =
        binder.invoke(executionContext, context, contract, rpc);

    state.setContractState(address, result.getState());
    return result.events();
  }

  @Override
  public Type getType() {
    return Type.INTERACT_CONTRACT;
  }

  @Override
  public void writeInner(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeInt(rpc.length);
    stream.write(rpc);
  }

  /**
   * Read a transaction from the specified stream.
   *
   * @param stream the stream to read from
   * @return transaction read from the stream
   */
  public static InteractWithContractTransaction read(SafeDataInputStream stream) {
    BlockchainAddress contractId = BlockchainAddress.read(stream);
    byte[] payload = DataStreamLimit.readDynamicBytes(stream);

    return new InteractWithContractTransaction(contractId, payload);
  }

  /**
   * Create a new instance of a transaction.
   *
   * @param contractId the contract this interaction works on
   * @param payload the actual transactions
   * @return the created contract interaction
   */
  public static InteractWithContractTransaction create(
      BlockchainAddress contractId, byte[] payload) {
    return new InteractWithContractTransaction(contractId, payload);
  }
}
