package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.kademlia.KademliaBucket.NodeDescriptor;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KademliaBucketTest {

  private final ArrayList<KademliaNode> nodes = new ArrayList<>();

  @Test
  public void nodeDescriptorTest() {
    EqualsVerifier.forClass(NodeDescriptor.class).withIgnoredFields("noOfErrors").verify();
  }

  @Test
  public void only20AreAdded() throws Exception {
    KademliaBucket bucket = createFilledBucket();
    KademliaKey key = new KademliaKey();
    bucket.add(new KademliaNode(key, InetAddress.getLocalHost(), 0));
    assertThat(bucket.getNodeCopy())
        .containsAll(nodes)
        .noneMatch(node -> node.getKey().equals(key));
  }

  @Test
  public void unableToAddTwice() throws Exception {
    KademliaBucket bucket = new KademliaBucket();
    KademliaKey key = new KademliaKey();
    KademliaNode kademliaNode = new KademliaNode(key, InetAddress.getLocalHost(), 0);
    bucket.add(kademliaNode);
    bucket.add(kademliaNode);
    assertThat(bucket.getNodeCopy()).containsExactly(kademliaNode);
  }

  @Test
  public void staleAreReplaced() throws Exception {
    KademliaBucket bucket = createFilledBucket();
    for (int i = 0; i < 5; i++) {
      bucket.error(nodes.get(0));
    }
    KademliaKey key = new KademliaKey();
    bucket.add(new KademliaNode(key, InetAddress.getLocalHost(), 0));
    ArrayList<KademliaNode> kademliaNodes = new ArrayList<>(nodes);
    kademliaNodes.remove(0);
    assertThat(bucket.getAllNodes())
        .hasSize(20)
        .containsAll(kademliaNodes)
        .anyMatch(node -> node.getKey().equals(key));
  }

  @Test
  public void errorWithReplacementReady() throws Exception {
    KademliaBucket bucket = createFilledBucket();
    KademliaKey key = new KademliaKey();
    bucket.add(new KademliaNode(key, InetAddress.getLocalHost(), 0));
    bucket.error(nodes.get(5));
    ArrayList<KademliaNode> kademliaNodes = new ArrayList<>(nodes);
    kademliaNodes.remove(5);
    assertThat(bucket.getAllNodes())
        .hasSize(20)
        .containsAll(kademliaNodes)
        .anyMatch(node -> node.getKey().equals(key));

    // Error on another should not have any effect
    bucket.error(nodes.get(4));
    assertThat(bucket.getAllNodes())
        .hasSize(20)
        .containsAll(kademliaNodes)
        .anyMatch(node -> node.getKey().equals(key));
  }

  @Test
  public void doesNotReturnStale() throws Exception {
    KademliaBucket bucket = createFilledBucket();
    for (int i = 0; i < 5; i++) {
      bucket.error(nodes.get(0));
    }
    ArrayList<KademliaNode> kademliaNodes = new ArrayList<>(nodes);
    kademliaNodes.remove(0);
    assertThat(bucket.getNodeCopy()).hasSize(19).containsAll(kademliaNodes);
  }

  @Test
  public void errorForUnknown() throws Exception {
    KademliaBucket bucket = createFilledBucket();
    KademliaNode unknownNode = new KademliaNode(new KademliaKey(), InetAddress.getLocalHost(), 0);
    for (int i = 0; i < 5; i++) {
      bucket.error(unknownNode);
    }
    assertThat(bucket.getNodeCopy()).containsAll(nodes);
  }

  private KademliaBucket createFilledBucket() throws UnknownHostException {
    KademliaBucket bucket = new KademliaBucket();
    for (int i = 0; i < Kademlia.BUCKET_SIZE; i++) {
      byte[] bytes = new byte[32];
      bytes[0] = (byte) i;
      KademliaNode kademliaNode =
          new KademliaNode(new KademliaKey(bytes), InetAddress.getLocalHost(), i);
      nodes.add(kademliaNode);
      bucket.add(kademliaNode);
    }
    return bucket;
  }
}
