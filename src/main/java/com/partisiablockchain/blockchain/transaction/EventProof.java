package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.AvlProof;
import com.partisiablockchain.serialization.StateSerializableProof;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** A proof that an event is included in a blockchain state. */
public final class EventProof implements DataStreamSerializable {

  private final StateSerializableProof stateProof;
  private final AvlProof avlProof;

  /**
   * Default constructor.
   *
   * @param stateProof a proof that can be used to argue about the values of the event.
   * @param avlProof proof attesting to the existence of spawned events in the state.
   */
  public EventProof(StateSerializableProof stateProof, AvlProof avlProof) {
    this.stateProof = stateProof;
    this.avlProof = avlProof;
  }

  /**
   * Construct a new event proof for an event.
   *
   * @param stateStorage the storage used to fetch state parts
   * @param stateIdentifier the identifier of the chain state
   * @param eventIdentifier the identifier of the spawned event
   * @return the created proof
   */
  public static EventProof constructEventProof(
      StateStorageRaw stateStorage, Hash stateIdentifier, Hash eventIdentifier) {
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ExecutedState executedState = stateSerializer.read(stateIdentifier, ExecutedState.class);
    StateSerializableProof stateProof = stateSerializer.proof(executedState);
    AvlProof avlProof = stateSerializer.avlProof(executedState, "spawnedEvents", eventIdentifier);
    return new EventProof(stateProof, avlProof);
  }

  /**
   * Verify that all data matches the supplied hashes.
   *
   * @param expectedStateHash the hash of the chain state
   * @param eventIdentifier the hash of the event transaction
   * @return the id of the shard that this event originates from
   */
  public ChainIds verify(Hash expectedStateHash, Hash eventIdentifier) {
    Hash stateIdentifier = stateProof.getIdentifier();
    if (!expectedStateHash.equals(stateIdentifier)) {
      throw new IllegalStateException(
          "State hash do not match expected hash. "
              + "Expected="
              + expectedStateHash
              + ", Got="
              + stateIdentifier);
    }
    StateSerializableProof.ValueReader executedStateReader = stateProof.reader(ExecutedState.class);
    Hash spawnedEvents = executedStateReader.getSubIdentifier("spawnedEvents");
    Hash avlIdentifier = avlProof.getRootIdentifier();
    if (!spawnedEvents.equals(avlIdentifier)) {
      throw new IllegalStateException(
          "Tree root do not match expected hash. "
              + "Expected="
              + spawnedEvents
              + ", Got="
              + avlIdentifier);
    }
    String chainId = executedStateReader.getInlineValue("chainId");
    String subChainId = executedStateReader.getInlineValue("subChainId");
    if (!eventIdentifier.equals(avlProof.getKey())) {
      throw new IllegalStateException("Proof was for another event");
    }
    return new ChainIds(chainId, subChainId);
  }

  /**
   * Verify that all data matches the supplied hashes.
   *
   * @param expectedStateHash the hash of the chain state
   * @param eventTransaction the event transaction
   * @return the id of the shard that this event originates from
   */
  public ChainIds verify(Hash expectedStateHash, EventTransaction eventTransaction) {
    StateSerializableProof.ValueReader executedStateReader = stateProof.reader(ExecutedState.class);
    String subChainId = executedStateReader.getInlineValue("subChainId");
    return verify(
        expectedStateHash, new ExecutableEvent(subChainId, eventTransaction).identifier());
  }

  StateSerializableProof getStateProof() {
    return stateProof;
  }

  AvlProof getAvlProof() {
    return avlProof;
  }

  static EventProof read(SafeDataInputStream stream) {
    StateSerializableProof stateProof = StateSerializableProof.read(stream, true);
    AvlProof avlProof = AvlProof.read(stream);
    return new EventProof(stateProof, avlProof);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stateProof.write(stream);
    avlProof.write(stream);
  }

  record ChainIds(String chainId, String subChainId) {}
}
