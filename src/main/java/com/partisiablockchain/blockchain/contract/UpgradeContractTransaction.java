package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.ObjectLocator;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContextImpl;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Objects;

/** Upgrade contract transaction for upgradable contract. */
public final class UpgradeContractTransaction implements Transaction {

  private final BlockchainAddress address;
  private final byte[] newBinderJar;
  private final byte[] newContractJar;
  private final byte[] newAbi;
  private final byte[] rpc;

  private UpgradeContractTransaction(
      BlockchainAddress address,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      byte[] rpc) {
    this.address = Objects.requireNonNull(address);
    this.newBinderJar = Objects.requireNonNull(newBinderJar);
    this.newContractJar = Objects.requireNonNull(newContractJar);
    this.newAbi = Objects.requireNonNull(newAbi);
    this.rpc = Objects.requireNonNull(rpc);
  }

  /**
   * Create a new upgrade contract transaction on upgradable contract.
   *
   * @param address address of contract to upgrade
   * @param newBinderJar new binder jar
   * @param newContractJar new contract jar
   * @param newAbi new abi
   * @param rpc contract upgrade rpc
   * @return the created contract upgrade transaction
   */
  public static UpgradeContractTransaction create(
      BlockchainAddress address,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      byte[] rpc) {
    return new UpgradeContractTransaction(address, newBinderJar, newContractJar, newAbi, rpc);
  }

  @Override
  public EventResult<? extends BinderEvent> execute(ExecutionContextTransaction executionContext) {
    return typedUpgrade(executionContext, address, newBinderJar, newContractJar, newAbi, rpc);
  }

  @Override
  public BlockchainAddress getTargetContract() {
    return address;
  }

  @Override
  public Type getType() {
    return Type.UPGRADE_CONTRACT;
  }

  @Override
  public void writeInner(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeDynamicBytes(newBinderJar);
    stream.writeDynamicBytes(newContractJar);
    stream.writeDynamicBytes(newAbi);
    stream.writeDynamicBytes(rpc);
  }

  private static EventResult<? extends BinderEvent> typedUpgrade(
      ExecutionContextTransaction executionContext,
      BlockchainAddress address,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      byte[] rpc) {
    MutableChainState state = executionContext.getState();
    ObjectLocator objectLocator = ObjectLocator.create(state, "Contract upgrade failed", "Binder");
    Hash binderHash = objectLocator.getSharedIdentifierOrSaveLocalObject(newBinderJar);
    Hash contractHash = state.saveJar(newContractJar);
    Hash abiHash = state.saveJar(newAbi);

    CoreContractState newCoreState =
        CoreContractState.create(binderHash, contractHash, abiHash, newContractJar.length);
    BlockchainContractContextImpl contractContext = new BlockchainContractContextImpl(address);

    if (state.getContracts().contains(address)) {
      state.upgradeContract(address, contractContext, executionContext, newCoreState, rpc);
      return new EventResult<>(List.of(), null);
    } else {
      String errorMessage =
          "Contract upgrade failed - Contract with identifier %s does not exist"
              .formatted(address.writeAsString());
      throw new IllegalArgumentException(errorMessage);
    }
  }

  /**
   * Read the transaction from the specified stream.
   *
   * @param stream the stream to read from
   * @return the upgrade contract transaction read from the stream
   */
  public static Transaction read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    byte[] newBinderJar = DataStreamLimit.readDynamicBytes(stream);
    byte[] newContractJar = DataStreamLimit.readDynamicBytes(stream);
    byte[] newAbi = DataStreamLimit.readDynamicBytes(stream);
    byte[] rpc = DataStreamLimit.readDynamicBytes(stream);

    return create(address, newBinderJar, newContractJar, newAbi, rpc);
  }
}
