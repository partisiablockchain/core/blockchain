package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.MutableChainState;

/** Test. */
public final class ExecutionContextTransactionTest {

  /** Creates execution context for test transaction. */
  public static ExecutionContextTransaction create(
      String chainId,
      MutableChainState state,
      BlockchainAddress sender,
      long blockTime,
      long productionTime,
      ExecutionEventManager manager,
      long remainingGas) {
    return create(
        chainId,
        state,
        sender,
        blockTime,
        productionTime,
        manager,
        FeeCollector.create(null, remainingGas));
  }

  /** Creates execution context for test transaction. */
  public static ExecutionContextTransaction create(
      String chainId,
      MutableChainState state,
      BlockchainAddress sender,
      long blockTime,
      long productionTime,
      ExecutionEventManager manager,
      FeeCollector collector) {
    return ExecutionContextTransaction.create(
        chainId,
        state,
        TestObjects.EMPTY_HASH,
        TestObjects.EMPTY_HASH,
        sender,
        manager,
        blockTime,
        productionTime,
        collector);
  }
}
