package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.blockchain.BlockchainLedgerSharedObjectStoreTest;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.TestContracts;
import com.partisiablockchain.blockchain.contract.binder.AbstractPublicContractBinder;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.UpgradablePubContractBinder;
import com.partisiablockchain.blockchain.contract.binder.UpgradablePubContractBinderV2;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.blockchain.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Tests for upgradable public contracts and binders. */
public final class UpgradeContractTransactionTest {

  private static final byte[] JAR_BINDER_PUB_UPGRADABLE =
      JarBuilder.buildJar(UpgradablePubContractBinder.class, AbstractPublicContractBinder.class);
  private static final byte[] JAR_BINDER_PUB_UPGRADABLE_V2 =
      JarBuilder.buildJar(UpgradablePubContractBinderV2.class, AbstractPublicContractBinder.class);

  private static final byte[] JAR_CONTRACT_PUB_UPGRADABLE =
      JarBuilder.buildJar(UpgradablePubTestContract.class);
  private static final byte[] JAR_CONTRACT_PUB_UPGRADABLE_V2 =
      JarBuilder.buildJar(UpgradablePubTestContractV2.class);

  private static final byte[] JAR_CONTRACT_SYS_UPGRADABLE =
      JarBuilder.buildJar(UpgradableSysContract.class);

  private static final byte[] JAR_CONTRACT_ZK_UPGRADABLE =
      JarBuilder.buildJar(UpgradableZkTestContract.class, StateComplex.class);

  @Test
  public void getters() {
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] rpc = SafeDataOutputStream.serialize(s -> s.writeBoolean(true));

    UpgradeContractTransaction transaction =
        createTransaction(address, binderJar, contractJar, new byte[0], rpc);

    Assertions.assertThat(transaction.getTargetContract()).isEqualTo(address);
    Assertions.assertThat(transaction.getType()).isEqualTo(Transaction.Type.UPGRADE_CONTRACT);
  }

  @Test
  public void writeAndRead() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    UpgradeContractTransaction transaction =
        createTransaction(address, new byte[1], new byte[2], new byte[3], new byte[4]);
    byte[] bytes = SafeDataOutputStream.serialize(transaction::write);

    Assertions.assertThat(Hex.toHexString(bytes))
        .isEqualTo(
            ""
                + "02" // Transaction type (UPGRADE_CONTRACT)
                + "02a000000000000000000000000000000000000003" // address
                + "0000000100" // new binder jar
                + "000000020000" // new contract jar
                + "00000003000000" // new ABI
                + "0000000400000000"); // RPC

    Transaction tx = Transaction.read(SafeDataInputStream.createFromBytes(bytes));
    Assertions.assertThat(tx.getTargetContract()).isEqualTo(address);
  }

  @Test
  public void upgradePublicContract() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] newContractJar = JAR_CONTRACT_PUB_UPGRADABLE_V2;
    byte[] abi = new byte[] {1, 2, 3};

    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] newAbi = new byte[] {2, 3, 4, 5};
    rpcBuilder.newContract = newContractJar;
    rpcBuilder.newAbi = newAbi;
    byte[] rpc =
        rpcBuilder.build(
            s -> {
              s.writeBoolean(true);
              s.writeLong(150);
            });

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    StateLong oldState = (StateLong) state.getContractState(address);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, binderJar, newContractJar, newAbi, rpc);

    Assertions.assertThat(oldState.value()).isEqualTo(0L);
    EventResult<? extends BinderEvent> result = transaction.execute(executionContext);
    Assertions.assertThat(result).isNotNull();
    StateString newState = (StateString) state.getContractState(address);
    Assertions.assertThat(newState.value()).isEqualTo("150");
  }

  @Test
  public void upgradePublicContractsBinder() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] newBinderJar = JAR_BINDER_PUB_UPGRADABLE_V2;
    byte[] abi = new byte[] {1, 2, 3};

    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    rpcBuilder.newBinder = newBinderJar;
    byte[] rpc =
        rpcBuilder.build(
            s -> {
              s.writeBoolean(true);
              s.writeLong(150);
            });

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    StateLong oldState = (StateLong) state.getContractState(address);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, newBinderJar, contractJar, abi, rpc);

    Assertions.assertThat(oldState.value()).isEqualTo(0L);
    EventResult<? extends BinderEvent> result = transaction.execute(executionContext);
    Assertions.assertThat(result).isNotNull();
    StateLong newState = (StateLong) state.getContractState(address);
    Assertions.assertThat(newState.value()).isEqualTo(150L);
  }

  @Test
  public void upgradeZkContract() {
    BlockchainAddress address = TestObjects.CONTRACT_ZK1;
    byte[] binderJar = TestContracts.CONTRACT_BINDER_ZK;
    byte[] contractJar = JAR_CONTRACT_ZK_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};

    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] rpc = rpcBuilder.build(s -> s.writeBoolean(true));

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, binderJar, contractJar, abi, rpc);

    Assertions.assertThatNoException().isThrownBy(() -> transaction.execute(executionContext));
    Assertions.assertThat(transaction.execute(executionContext)).isNotNull();
  }

  @Test
  public void upgradeSysContractWithNewUpgradePath() {
    BlockchainAddress address = TestObjects.CONTRACT_SYS;
    byte[] binderJar = TestContracts.CONTRACT_BINDER_SYSTEM;
    byte[] contractJar = JAR_CONTRACT_SYS_UPGRADABLE;
    byte[] newContractJar = JAR_CONTRACT_SYS_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};

    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] newAbi = new byte[] {4, 1, 4};
    rpcBuilder.newContract = newContractJar;
    rpcBuilder.newAbi = newAbi;
    byte[] rpc =
        rpcBuilder.build(
            s -> {
              s.writeBoolean(true);
              s.writeLong(42);
            });

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    StateLong oldState = (StateLong) state.getContractState(address);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, binderJar, newContractJar, newAbi, rpc);

    Assertions.assertThat(oldState.value()).isEqualTo(0L);
    EventResult<? extends BinderEvent> result = transaction.execute(executionContext);
    Assertions.assertThat(result).isNotNull();
    StateLong newState = (StateLong) state.getContractState(address);
    Assertions.assertThat(newState.value()).isEqualTo(42);
  }

  @Test
  public void upgradeWithNonexistentContract() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    byte[] rpc = SafeDataOutputStream.serialize(s -> s.writeBoolean(true));

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    state.removeContract(address, 0L);

    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, binderJar, contractJar, abi, rpc);

    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract upgrade failed - Contract with identifier "
                + address.writeAsString()
                + " does not exist");
  }

  @Test
  public void upgradeWithNonexistentBinder() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    byte[] rpc = SafeDataOutputStream.serialize(s -> s.writeBoolean(true));

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, new byte[32], contractJar, abi, rpc);

    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Contract upgrade failed - Binder with identifier "
                + Hash.read(SafeDataInputStream.createFromBytes(new byte[32]))
                + " is not available");
  }

  @Test
  public void upgradeWithNonUpgradableBinder() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] nonUpgradableBinderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    byte[] rpc = SafeDataOutputStream.serialize(s -> s.writeBoolean(true));

    MutableChainState state = createState(address, nonUpgradableBinderJar, contractJar, abi);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, nonUpgradableBinderJar, contractJar, abi, rpc);

    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage(
            "Binder com.partisiablockchain.binder.PublicContractBinder does not implement support"
                + " for acquireUpgradePermit");
  }

  @Test
  public void upgradeWithBinderIdentifierThatIsAlreadyStored() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    Hash binderHash = new LargeByteArray(binderJar).getIdentifier();
    byte[] abi = new byte[] {1, 2, 3};
    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] rpc =
        rpcBuilder.build(
            s -> {
              s.writeBoolean(true);
              s.writeLong(500);
            });

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    state.updateGlobalPluginState(0L, ChainPluginType.SHARED_OBJECT_STORE, binderJar);
    ExecutionContextTransaction executionContext = createExecutionContext(address, state);
    UpgradeContractTransaction transaction =
        createTransaction(address, binderHash.getBytes(), contractJar, abi, rpc);

    Assertions.assertThat(state.getSharedObjectStorePlugin().exists(binderHash)).isTrue();
    Assertions.assertThatNoException().isThrownBy(() -> transaction.execute(executionContext));
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  public void acquireUpgradePermitAndUpgradeStateForPublicContract() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] rpc =
        rpcBuilder.build(
            s -> {
              s.writeBoolean(true);
              s.writeLong(42);
            });

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    BlockchainContract contract = state.getBlockchainContract(address);

    Hash binderHash = state.saveJar(binderJar);
    Hash contractHash = state.saveJar(contractJar);
    Hash abiHash = state.saveJar(abi);
    ContractIdentifiers bytecodeIds = new ContractIdentifiers(contractHash, binderHash, abiHash);
    Assertions.assertThatNoException()
        .isThrownBy(
            () -> contract.acquireUpgradePermit(null, null, null, bytecodeIds, bytecodeIds, rpc));

    ContractUpgradePermit permit =
        contract.acquireUpgradePermit(null, null, null, bytecodeIds, bytecodeIds, rpc);
    StateAccessor stateAccessor = StateAccessor.create(new StateLong(0));
    StateLong upgradedState =
        (StateLong) contract.upgradeState(null, null, stateAccessor, null, rpc);
    Assertions.assertThat(permit).isNotNull();
    Assertions.assertThat(upgradedState).isNotNull();
    Assertions.assertThat(upgradedState.value()).isEqualTo(42L);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  public void acquireUpgradePermitAndUpgradeStateForZkContract() {
    BlockchainAddress address = TestObjects.CONTRACT_ZK1;
    byte[] binderJar = TestContracts.CONTRACT_BINDER_ZK;
    byte[] contractJar = JAR_CONTRACT_ZK_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] rpc = rpcBuilder.build(s -> s.writeBoolean(true));

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    BlockchainContract contract = state.getBlockchainContract(address);

    Hash binderHash = state.saveJar(binderJar);
    Hash contractHash = state.saveJar(contractJar);
    Hash abiHash = state.saveJar(abi);
    ContractIdentifiers bytecodeIds = new ContractIdentifiers(contractHash, binderHash, abiHash);

    ContractUpgradePermit permit =
        contract.acquireUpgradePermit(null, null, null, bytecodeIds, bytecodeIds, rpc);
    StateSerializable upgradedState = contract.upgradeState(null, null, null, null, rpc);
    Assertions.assertThat(permit).isNotNull();
    Assertions.assertThat(upgradedState).isNotNull();
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  public void acquireUpgradeAndUpgradeStateAreNotAllowed() {
    BlockchainAddress address = TestObjects.CONTRACT_WASM_PUB1;
    byte[] binderJar = JAR_BINDER_PUB_UPGRADABLE;
    byte[] contractJar = JAR_CONTRACT_PUB_UPGRADABLE;
    byte[] abi = new byte[] {1, 2, 3};
    UpgradeRpcBuilder rpcBuilder = new UpgradeRpcBuilder(contractJar, binderJar, abi);
    byte[] rpc = rpcBuilder.build(s -> s.writeBoolean(false));

    MutableChainState state = createState(address, binderJar, contractJar, abi);
    BlockchainContract contract = state.getBlockchainContract(address);

    Hash binderHash = state.saveJar(binderJar);
    Hash contractHash = state.saveJar(contractJar);
    Hash abiHash = state.saveJar(abi);
    ContractIdentifiers bytecodeIds = new ContractIdentifiers(contractHash, binderHash, abiHash);

    Assertions.assertThatThrownBy(
            () -> contract.acquireUpgradePermit(null, null, null, bytecodeIds, bytecodeIds, rpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Contract upgrade is not allowed.");
  }

  private MutableChainState createState(
      BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] abi) {
    return StateHelper.initialWithAdditions(
        builder -> {
          builder.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.SHARED_OBJECT_STORE,
              BlockchainLedgerSharedObjectStoreTest.JAR_PLUGIN_STORAGE,
              SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
          CoreContractStateTest.createContract(
              builder,
              contractJar,
              address,
              binderJar,
              (b) ->
                  b.create(
                          executionContext(),
                          null,
                          new byte[] {
                            0, 0, 0, 0, 0, 0, 0, 0,
                          })
                      .getState(),
              abi);
          Assertions.assertThat(builder.getGlobalPluginState(ChainPluginType.SHARED_OBJECT_STORE))
              .isNull();
          Assertions.assertThat(builder.getSharedObjectStorePlugin()).isNotNull();
        });
  }

  private ExecutionContextTransaction executionContext() {
    return ExecutionContextTransactionTest.create("ChainId", null, null, 1L, 1, null, 0);
  }

  private ExecutionContextTransaction createExecutionContext(
      BlockchainAddress address, MutableChainState state) {
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    EventCollector eventCollector =
        new EventCollector(null, state, 1L, 1L, TestObjects.EMPTY_HASH, 9, null, feeCollector);
    ExecutionEventManager eventManager = new ExecutionEventManager(() -> eventCollector);
    return ExecutionContextTransactionTest.create(
        "ChainId", state, address, 1L, 1L, eventManager, 0);
  }

  private UpgradeContractTransaction createTransaction(
      BlockchainAddress address, byte[] binderJar, byte[] contractJar, byte[] abi, byte[] rpc) {
    return UpgradeContractTransaction.create(address, binderJar, contractJar, abi, rpc);
  }

  /**
   * Build RPC to upgrade transaction. Adds expected contract, binder and ABI identifiers that the
   * test contracts expects in the 'onAcquireUpgradePermit()' implementations.
   */
  private static final class UpgradeRpcBuilder {

    private final byte[] oldContract;
    private final byte[] oldBinder;
    private final byte[] oldAbi;

    private byte[] newContract;
    private byte[] newBinder;
    private byte[] newAbi;

    private UpgradeRpcBuilder(byte[] contract, byte[] binder, byte[] abi) {
      this.oldContract = contract;
      this.newContract = contract;
      this.oldBinder = binder;
      this.newBinder = binder;
      this.oldAbi = abi;
      this.newAbi = abi;
    }

    private byte[] build(Consumer<SafeDataOutputStream> additionalRpc) {
      return SafeDataOutputStream.serialize(
          s -> {
            new LargeByteArray(oldContract).getIdentifier().write(s);
            new LargeByteArray(oldBinder).getIdentifier().write(s);
            new LargeByteArray(oldAbi).getIdentifier().write(s);

            new LargeByteArray(newContract).getIdentifier().write(s);
            new LargeByteArray(newBinder).getIdentifier().write(s);
            new LargeByteArray(newAbi).getIdentifier().write(s);
            additionalRpc.accept(s);
          });
    }
  }
}
