package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.MutableChainState.setFeatureOrExit;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.PubTestContract;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.StateComplex;
import com.partisiablockchain.blockchain.contract.ZkTestContractComplex;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.fee.AccumulatedFees;
import com.partisiablockchain.blockchain.fee.ContractStorage;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.fee.FeeState;
import com.partisiablockchain.blockchain.fee.GasAndCoinFeePluginGlobal;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTest;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.MemoryStateStorage;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntConsumer;
import org.junit.jupiter.api.Test;

/** Test of {@link MutableChainState}. */
public final class MutableChainStateTest {

  private final List<String> knownFeaturesTest = new ArrayList<>();

  private MutableChainState state =
      StateHelper.initialWithAdditions(
          mutableChainState -> {
            mutableChainState.createAccount(TestObjects.ACCOUNT_ONE);
            mutableChainState.createAccount(TestObjects.ACCOUNT_TWO);
          });

  @Test
  public void createAccount() {
    BlockchainAddress account = TestObjects.ACCOUNT_FOUR;
    assertThat(state.existsAccounts(account)).isFalse();
    state.createAccount(account);
    assertThat(state.existsAccounts(account)).isTrue();
  }

  @Test
  public void updateLocalPluginState_accountContextDoesNotExist() {
    assertThatThrownBy(
            () ->
                state.updateLocalPluginState(
                    ChainPluginType.ACCOUNT, TestObjects.ACCOUNT_FOUR, new byte[0], 0))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Supplied context does not exist " + TestObjects.ACCOUNT_FOUR);
  }

  @Test
  void checkExistenceShouldNotReturnTrueForContractAddressWithOpenAccountActivated() {
    // Contract that does not exist and OPEN ACCOUNT CREATION off.
    state.setFeature(Features.FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT, "OK");
    InnerSystemEvent.CheckExistenceEvent event =
        new InnerSystemEvent.CheckExistenceEvent(TestObjects.CONTRACT_SYS);
    byte[] result =
        event.execute(
            "Shard0", FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    assertThat(result).isEqualTo(new byte[] {0});

    // Should still be false.
    state.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "OK");
    result =
        event.execute(
            "Shard0", FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    assertThat(result).isEqualTo(new byte[] {0});

    // Account that is in state.
    event = new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_ONE);
    result =
        event.execute(
            "Shard0", FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    assertThat(result).isEqualTo(new byte[] {1});

    // Account that is not in state but with OPEN ACCOUNT CREATION on.
    event = new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_THREE);
    result =
        event.execute(
            "Shard0", FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    assertThat(result).isEqualTo(new byte[] {1});

    // Account that is not in state without OPEN ACCOUNT CREATION
    state.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, null);
    event = new InnerSystemEvent.CheckExistenceEvent(TestObjects.ACCOUNT_THREE);
    result =
        event.execute(
            "Shard0", FunctionUtility.noOpBiConsumer(), (contract, callback) -> {}, state, 0);
    assertThat(result).isEqualTo(new byte[] {0});
  }

  @Test
  public void updateLocalPluginState_createdIfOpenAccountCreation() {
    state.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "OK");
    state.updateLocalPluginState(
        ChainPluginType.ACCOUNT, TestObjects.ACCOUNT_FOUR, new byte[] {2}, 0);
    assertThat(state.getAccounts()).contains(TestObjects.ACCOUNT_FOUR);
  }

  @Test
  public void updateLocalPluginState_shouldNotOverwriteIfOpenAccountCreation() {
    state.createAccount(TestObjects.ACCOUNT_FOUR);
    state.bumpNonce(TestObjects.ACCOUNT_FOUR);
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR).getNonce()).isEqualTo(2);
    state.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "OK");
    state.updateLocalPluginState(
        ChainPluginType.ACCOUNT, TestObjects.ACCOUNT_FOUR, new byte[] {2}, 0);
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR).getNonce()).isEqualTo(2);
  }

  @Test
  public void getAccount_createdIfOpenAccountCreation() {
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNull();
    state.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "OK");
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNotNull();
  }

  @Test
  public void updateLocalPluginState_returnValue() {
    int amount = 1000;
    BlockchainAddress account = TestObjects.ACCOUNT_ONE;
    FeePluginHelper.mintGas(state, account, amount);
    byte[] returnValue = FeePluginHelper.getBalance(state, account, 0);
    assertThat(SafeDataInputStream.createFromBytes(returnValue).readLong()).isEqualTo(amount);
  }

  @Test
  public void registerBlockchainUsageAndUpdateLocalPluginState_blockProductionTime() {
    int amount = 1000;
    state.registerBlockchainUsage((long) Math.pow(2, 21), amount);
    byte[] returnValue = FeePluginHelper.getBlockchainUsage(state, 1);
    assertThat(SafeDataInputStream.createFromBytes(returnValue).readLong()).isEqualTo(amount);
  }

  @Test
  public void remainingGasIsAddedToBlockchainUsageAfterDeletion() {

    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;

    CoreContractStateTest.createContract(
        state,
        JarBuilder.buildJar(PubTestContract.class),
        contractAddress,
        TestContracts.CONTRACT_BINDER_PUBLIC);

    long amount = 1000;
    FeePluginHelper.mintGas(state, contractAddress, amount);
    state.removeContract(contractAddress, 10);
    byte[] returnValue = FeePluginHelper.getBlockchainUsage(state, 10);
    long gas = SafeDataInputStream.createFromBytes(returnValue).readLong();
    assertThat(gas).isEqualTo(amount);
  }

  @Test
  public void payFee() {
    int amount = 0;
    BlockchainAddress account = TestObjects.ACCOUNT_ONE;
    FeePluginHelper.mintGas(state, account, amount);
    state.payFee(
        1,
        account,
        SignedTransaction.create(
                CoreTransactionPart.create(0, 120, amount),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]))
            .sign(new KeyPair(), "Test"));
    byte[] returnValue = FeePluginHelper.getPaidFees(state, amount, 1, account);
    assertThat(SafeDataInputStream.createFromBytes(returnValue).readLong()).isEqualTo(1);
  }

  @Test
  public void calculateNetworkFee() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(0, 120, 0),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]))
            .sign(new KeyPair(), "Test");
    long fee = calculateNetworkFee(transaction);
    assertThat(fee).isEqualTo(114L);
  }

  /**
   * Calculate network fee of transaction.
   *
   * @param transaction the transaction to calculate fee for
   * @return network fee of transaction
   */
  private long calculateNetworkFee(SignedTransaction transaction) {
    long networkByteCount = transaction.computeNetworkByteCount();
    return state.getAccountPlugin().convertNetworkFee(networkByteCount);
  }

  @Test
  public void payByocServiceFees() {
    FixedList<BlockchainAddress> nodes =
        FixedList.create(List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO));
    PendingByocFee pending = PendingByocFee.create(nodes, Unsigned256.create(10L), "symbol");
    state.payByocServiceFees(123, List.of(pending));

    assertThat(FeePluginHelper.registeredByocFees(state))
        .usingRecursiveComparison()
        .isEqualTo(List.of(pending));
  }

  @Test
  public void updateGlobalPluginState_returnValue() {
    long dummyValue = 1234;
    byte[] returnValue = FeePluginHelper.getDummyValueGlobal(state, dummyValue);
    assertThat(SafeDataInputStream.createFromBytes(returnValue).readLong()).isEqualTo(dummyValue);
  }

  @Test
  public void getContracts() {
    assertThat(state.getContracts()).isEmpty();
  }

  @Test
  public void getContractStorageLength() {
    BlockchainAddress contract = TestObjects.CONTRACT_PUB1;
    assertThat(state.getContractStorageLength(contract)).isNull();
    assertThat(state.existsContract(contract)).isFalse();

    byte[] contractInfo = JarBuilder.buildJar(PubTestContract.class);
    CoreContractStateTest.createContract(
        state, contractInfo, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
    assertThat(state.existsContract(contract)).isTrue();
    assertThat(state.getContractStorageLength(contract)).isEqualTo(contractInfo.length);
  }

  @Test
  public void getShards() {
    ShardId shardId = new ShardId("Shard0");
    assertThat(state.getActiveShards().size()).isEqualTo(0);
    state.addActiveShard(FunctionUtility.noOpBiConsumer(), "Shard0", shardId.getId());
    assertThat(state.getActiveShards().size()).isEqualTo(1);
    assertThatThrownBy(
            () -> state.addActiveShard(FunctionUtility.noOpBiConsumer(), "Shard0", shardId.getId()))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Unable to add existing shard " + shardId.getId());
    state.removeActiveShard(FunctionUtility.noOpBiConsumer(), "Shard0", shardId.getId());
    assertThat(state.getActiveShards().size()).isEqualTo(0);
  }

  @Test
  public void getGovernanceVersion() {
    assertThat(state.getGovernanceVersion()).isEqualTo(0L);
    state.bumpGovernanceVersion();
    assertThat(state.getGovernanceVersion()).isEqualTo(1L);
  }

  @Test
  public void getAndIncrementShardNonce() {
    assertThat(state.routeToShard("Shard_0").nonce).isEqualTo(1L);
    assertThat(state.routeToShard("Shard_1").nonce).isEqualTo(1L);
    assertThat(state.routeToShard("Shard_1").nonce).isEqualTo(2L);
  }

  @Test
  public void invokeLocalWithIllegalContext() {
    assertThatThrownBy(
            () ->
                state.updateLocalPluginState(
                    ChainPluginType.ACCOUNT, TestObjects.ACCOUNT_FOUR, new byte[0], 0))
        .isInstanceOf(IllegalStateException.class);
    assertThatThrownBy(
            () ->
                state.updateLocalPluginState(
                    ChainPluginType.ACCOUNT, TestObjects.CONTRACT_PUB1, new byte[0], 0))
        .isInstanceOf(IllegalStateException.class);
  }

  @Test
  public void removeContractWithoutAccountPlugin() {
    BlockchainAddress contract = TestObjects.CONTRACT_PUB1;

    CoreContractStateTest.createContract(
        state,
        JarBuilder.buildJar(PubTestContract.class),
        TestObjects.CONTRACT_PUB1,
        TestContracts.CONTRACT_BINDER_PUBLIC);

    assertThat(state.getCoreContractState(contract)).isNotNull();
    state.removeContract(contract, 0);
    assertThat(state.getCoreContractState(contract)).isNull();
  }

  @Test
  public void removeContractWithAllocatedCost() {
    BlockchainAddress contract = TestObjects.CONTRACT_PUB1;

    CoreContractStateTest.createContract(
        state,
        JarBuilder.buildJar(PubTestContract.class),
        TestObjects.CONTRACT_PUB1,
        TestContracts.CONTRACT_BINDER_PUBLIC);

    long allocatedCost = 2;
    ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(List.of(TestObjects.EMPTY_HASH), allocatedCost, null);
    ContractState contractState = create(new byte[0]);
    ContractState newContractState =
        contractState.withCallbacks(
            new ReturnEnvelope(TestObjects.CONTRACT_BOOTSTRAP),
            TestObjects.ACCOUNT_ONE,
            TestObjects.EMPTY_HASH,
            callbackInfo);
    state.setContract(contract, newContractState);
    state.removeContract(contract, (long) Math.pow(2, 21));

    byte[] returnValue = FeePluginHelper.getBlockchainUsage(state, 1);
    long blockChainUsage = SafeDataInputStream.createFromBytes(returnValue).readLong();
    assertThat(blockChainUsage).isEqualTo(allocatedCost);
  }

  /** Removal of null contract address must throw, and have no effect on the state. */
  @Test
  public void cannotDeleteNullContracts() {
    final int sizeBeforeRemoval = state.getContracts().size();

    assertThatThrownBy(() -> state.removeContract(null, 0))
        .isInstanceOf(NullPointerException.class);

    assertThat(state.getContracts().size()).isEqualTo(sizeBeforeRemoval);
  }

  /** Removal of non-existing contracts is allowed to throw. Must not have an effect on state. */
  @Test
  public void cannotDeleteNonExistingContracts() {
    final int sizeBeforeRemoval = state.getContracts().size();

    assertThatThrownBy(() -> state.removeContract(TestObjects.ACCOUNT_FOUR, 0))
        .isInstanceOf(NullPointerException.class);

    assertThat(state.getContracts().size()).isEqualTo(sizeBeforeRemoval);
  }

  @Test
  public void shouldMarkForContractsDeletion() {
    StateSerializer chainStateCache = state.getContext().createStateSerializer();
    byte[] publicBinderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    SerializationResult publicBinder = chainStateCache.write(new LargeByteArray(publicBinderJar));
    SerializationResult contractBinder =
        chainStateCache.write(new LargeByteArray(TestContracts.CONTRACT_VOID));

    assertThat(state.getContracts()).isEmpty();
    state.createContract(
        TestObjects.CONTRACT_PUB1,
        CoreContractStateTest.create(publicBinder.hash(), contractBinder.hash(), 1234));
    FeePluginHelper.enableStorageFees(state, TestObjects.CONTRACT_PUB1, 1234);
    assertThat(state.getContracts()).hasSize(1).containsExactly(TestObjects.CONTRACT_PUB1);
    UpdateEvent updateEvent = state.resultingUpdateEvent();
    assertThat(updateEvent.newContracts).hasSize(1);
    assertThat(updateEvent.updatedContracts).isEmpty();
    assertThat(updateEvent.removedContract).isEmpty();

    BlockchainContract<StateSerializable, BinderEvent> actual =
        state
            .getContext()
            .get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash());
    assertThat(actual).isNotNull();
    state = state.asImmutable("chainId", null, AvlTree.create(), AvlTree.create()).asMutable();
    state.removeContract(TestObjects.CONTRACT_PUB1, 0);
    for (int i = 0; i < ChainStateCache.CONTRACT_REMOVAL_DELAY_IN_BLOCKS + 1; i++) {
      state = state.asImmutable("chainId", null, AvlTree.create(), AvlTree.create()).asMutable();
    }
    assertThat(
            state
                .getContext()
                .get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotSameAs(actual);
  }

  @Test
  public void setFeatures() {
    AvlTree<String, String> returnedFeature;
    AvlTree<String, String> features = AvlTree.create();
    String myKey = "testFeature";
    String myValue = "testValue";
    AvlTree<String, String> updatedFeatures = features.set(myKey, myValue);
    knownFeaturesTest.add("testFeature");
    List<Integer> ints = new ArrayList<>();
    IntConsumer intConsumer = ints::add;

    returnedFeature =
        setFeatureOrExit(updatedFeatures, myKey, null, knownFeaturesTest, intConsumer);
    assertThat(returnedFeature.size()).isEqualTo(0);

    returnedFeature = setFeatureOrExit(features, myKey, myValue, knownFeaturesTest, intConsumer);
    assertThat(returnedFeature.size()).isEqualTo(1);

    returnedFeature =
        setFeatureOrExit(features, "dontHaveThisFeature", myValue, knownFeaturesTest, intConsumer);
    assertThat(returnedFeature).isNotNull();
    assertThat(ints.contains(1)).isTrue();
  }

  @Test
  public void nonce() {
    assertThat(state.lookupNonce(TestObjects.ACCOUNT_ONE)).isEqualTo(1L);
    assertThat(state.lookupNonce(TestObjects.ACCOUNT_TWO)).isEqualTo(1L);
    assertThat(
            state.lookupNonce(
                BlockchainAddress.fromString("000001234000000000000000000000000000000000")))
        .isNull();
  }

  @Test
  public void createNewContractShouldNotOverwrite() {
    state =
        StateHelper.initialWithAdditions(
            state -> {
              Hash publicBinderJar = state.saveJar(TestContracts.CONTRACT_BINDER_PUBLIC);
              Hash contractJar = state.saveJar(TestContracts.CONTRACT_VOID);
              state.createAccount(TestObjects.ACCOUNT_ONE);
              state.createAccount(TestObjects.ACCOUNT_TWO);
              state.createContract(
                  TestObjects.CONTRACT_PUB1,
                  CoreContractStateTest.create(publicBinderJar, contractJar, 256));
            });
    assertThatThrownBy(
            () ->
                state.createContract(
                    TestObjects.CONTRACT_PUB1,
                    CoreContractStateTest.create(
                        state.saveJar(TestContracts.CONTRACT_BINDER_PUBLIC),
                        state.saveJar(TestContracts.CONTRACT_VOID),
                        256)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "There already exists a contract for the given address: " + TestObjects.CONTRACT_PUB1);
  }

  private static final Block TEST_BLOCK =
      Block.createGenesis(Hash.create(s -> s.writeString("Some hash")));

  /** {@code updateActiveContract} does not accept {@code null} contract addresses. */
  @Test
  public void updateActiveContractButWaitThatsNotaContractThatsNull() {
    assertThatThrownBy(() -> state.updateActiveContract(TEST_BLOCK, null))
        .isInstanceOf(NullPointerException.class);
  }

  /** {@code updateActiveContract} does accept unknown contract addresses. */
  @Test
  public void updateActiveContractButWaitThatsNotaContract() {
    assertThat(state.updateActiveContract(TEST_BLOCK, TestObjects.ACCOUNT_FOUR)).isFalse();
  }

  /** {@code updateActiveContract} does accept null blocks. */
  @Test
  public void updateActiveContractButWaitThatsNotaBlock() {
    assertThatThrownBy(() -> state.updateActiveContract(null, TestObjects.ACCOUNT_FOUR))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void icedContract() {
    byte[] jar = JarBuilder.buildJar(ExecutionContextTest.AllocatedCostSysContract.class);
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    MutableChainState state =
        createStateWithContractAndAccountPlugin(
            jar,
            contract,
            TestContracts.CONTRACT_BINDER_SYSTEM,
            ICING_ACCOUNT_PLUGIN,
            new byte[] {0, 0});
    boolean iced = state.updateActiveContract(TEST_BLOCK, contract);
    Set<BlockchainAddress> contractsBefore = state.getContracts();
    assertThat(iced).isFalse();
    assertThat(contractsBefore).isEqualTo(state.getContracts());
    ContractState contractStateAfter = state.getContract(contract);
    assertThat(contractStateAfter).isNotNull();
    state =
        createStateWithContractAndAccountPlugin(
            jar,
            contract,
            TestContracts.CONTRACT_BINDER_SYSTEM,
            ICING_ACCOUNT_PLUGIN,
            new byte[] {1, 0});
    iced = state.updateActiveContract(TEST_BLOCK, contract);
    assertThat(iced).isTrue();
    assertThat(state.getContract(contract)).isNotNull();
    state =
        createStateWithContractAndAccountPlugin(
            jar,
            contract,
            TestContracts.CONTRACT_BINDER_SYSTEM,
            ICING_ACCOUNT_PLUGIN,
            new byte[] {0, 1});
    state.updateActiveContract(TEST_BLOCK, contract);
    assertThat(state.getContract(contract)).isNull();
  }

  private MutableChainState createStateWithContractAndAccountPlugin(
      byte[] jar,
      BlockchainAddress contractAddress,
      byte[] binderJar,
      byte[] accountPluginJar,
      byte[] rpc) {
    return StateHelper.initialWithAdditions(
        builder -> {
          CoreContractStateTest.createContract(builder, jar, contractAddress, binderJar);
          builder.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.ACCOUNT,
              accountPluginJar,
              rpc);
        });
  }

  @Test
  public void testCallbackComplete() {
    state =
        StateHelper.initialWithAdditions(
            state -> {
              Hash publicBinderJar = state.saveJar(TestContracts.CONTRACT_BINDER_PUBLIC);
              Hash contractJar = state.saveJar(TestContracts.CONTRACT_VOID);
              state.createAccount(TestObjects.ACCOUNT_ONE);
              state.createAccount(TestObjects.ACCOUNT_TWO);
              state.createContract(
                  TestObjects.CONTRACT_PUB1,
                  CoreContractStateTest.create(publicBinderJar, contractJar, 256));
            });

    assertThatThrownBy(
            () ->
                state.callbackExecutionComplete(
                    TestObjects.CONTRACT_PUB1, TestObjects.EMPTY_HASH, null, new byte[0]))
        .isInstanceOf(IllegalStateException.class);
    ReturnEnvelope returnEnvelope = new ReturnEnvelope(TestObjects.CONTRACT_PUB1);
    ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(
                List.of(TestObjects.EMPTY_HASH), 0L, new LargeByteArray(new byte[0]))
            .completeEvent(
                TestObjects.EMPTY_HASH,
                true,
                SafeDataOutputStream.serialize(stream -> stream.writeInt(123)));
    state.addCallbacks(
        TestObjects.ACCOUNT_ONE,
        TestObjects.CONTRACT_PUB1,
        returnEnvelope,
        TestObjects.EMPTY_HASH,
        callbackInfo);
    Hash callbackIdentifier = ContractState.CallbackInfo.hash(List.of(TestObjects.EMPTY_HASH));
    CallbackContext callbackContext =
        state.callbackContext(TestObjects.CONTRACT_PUB1, callbackIdentifier);
    assertThat(callbackContext).isNotNull();
    assertThat(callbackContext.isSuccess()).isTrue();
    assertThat(callbackContext.results()).hasSize(1);
    assertThat(callbackContext.results().get(0).returnValue().readInt()).isEqualTo(123);
    AtomicReference<CallbackCreator.Callback> consumer = new AtomicReference<>();
    state.callbackReceived(
        TestObjects.CONTRACT_PUB1, TestObjects.EMPTY_HASH, true, consumer::set, new byte[0]);
    assertThat(consumer.get()).isNotNull();

    AtomicReference<InnerSystemEvent.CallbackEvent> reference = new AtomicReference<>();
    state.setCallbackResult(TestObjects.CONTRACT_PUB1, callbackIdentifier, true);
    state.callbackExecutionComplete(
        TestObjects.CONTRACT_PUB1, callbackIdentifier, reference::set, new byte[0]);
    assertThat(reference.get()).isNotNull();
    assertThat(reference.get().target()).isEqualTo(TestObjects.CONTRACT_PUB1);

    reference.set(null);
    assertThatThrownBy(
            () ->
                state.callbackExecutionComplete(
                    TestObjects.CONTRACT_PUB1,
                    callbackIdentifier,
                    FunctionUtility.noOpConsumer(),
                    new byte[0]))
        .isInstanceOf(IllegalStateException.class);
    assertThat(reference.get()).isNull();
  }

  @Test
  public void checkRoutingRemoveShard() {
    MutableChainState state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    List<SyncEvent> syncs = new ArrayList<>();
    state.removeActiveShard((s, syncEvent) -> syncs.add(syncEvent), "Shard0", "Shard0");
    assertThat(state.isSyncing()).isFalse();
    assertThat(state.getShardNonce(null).isMissingSync()).isFalse();
    assertThat(state.getShardNonce("Shard0").isMissingSync()).isFalse();
    SyncEvent syncEvent = syncs.get(0);
    assertThat(syncEvent.getAccountTransfers()).hasSize(1);
    assertThat(syncEvent.getContractTransfers()).hasSize(1);
    assertThat(syncs).hasSize(1);
    assertThat(state.existsAccounts(TestObjects.ACCOUNT_ONE)).isFalse();
  }

  @Test
  public void checkRoutingPartOfNext() {
    MutableChainState state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    checkRouting(state, StateLong.class);
  }

  @Test
  public void checkRoutingPartOfNextNoLocalAccountPluginState() {
    MutableChainState state = initializeRoutingStateWithoutAccountPlugin();
    checkRoutingWithoutAccountPlugin(state);
  }

  @Test
  public void checkRoutingPartOfNextWithZeroKnowledgeContract() {
    MutableChainState state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_ZK,
            JarBuilder.buildJar(ZkTestContractComplex.class, StateComplex.class),
            TestObjects.CONTRACT_ZK1,
            null);
    checkRouting(state, StateComplex.class);
  }

  private void checkRoutingWithoutAccountPlugin(MutableChainState state) {
    List<SyncEvent> events = new ArrayList<>();
    assertThat(state.getLocalAccountPluginState()).isNull();
    state.addActiveShard(
        (s, syncEvent) -> {
          events.add(syncEvent);
        },
        "Shard0",
        "Shard1");
    SyncEvent event = events.get(0);
    SyncEvent.ContractTransfer contractTransfer = event.getContractTransfers().get(0);
    StateSerializer stateSerializer = setupStateSerializer(event);
    ContractStorage contractStorage =
        stateSerializer.read(contractTransfer.pluginStateHash(), ContractStorage.class);
    assertThat(contractStorage).isNull();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private void checkRouting(MutableChainState state, Class stateClass) {
    // Mint to ensure contract exists.
    FeePluginHelper.mintGas(state, state.getContracts().iterator().next(), 1000);
    List<String> syncs = new ArrayList<>();
    List<SyncEvent> events = new ArrayList<>();
    assertThat(state.getLocalAccountPluginState().getContracts().size()).isEqualTo(1);
    state.addActiveShard(
        (s, syncEvent) -> {
          syncs.add(s);
          events.add(syncEvent);
        },
        "Shard0",
        "Shard1");
    SyncEvent event = events.get(0);
    SyncEvent.ContractTransfer contractTransfer = event.getContractTransfers().get(0);
    assertCheckRoutingPartOfNextInitial(state, syncs, contractTransfer);
    StateSerializer stateSerializer = setupStateSerializer(event);
    ContractState contractState =
        stateSerializer.read(contractTransfer.contractStateHash(), ContractState.class);
    if (stateClass.equals(StateLong.class)) {
      StateLong innerContractState =
          stateSerializer.read(contractState.getStateHash(), StateLong.class);
      assertThat(innerContractState.value()).isEqualTo(2L);
    } else {
      final StateSerializable innerContractState =
          stateSerializer.read(contractState.getStateHash(), StateSerializable.class);
      assertThat(innerContractState).isNull();
    }
    assertCheckRoutingPartOfNext(contractState, stateSerializer, contractTransfer, event, syncs);
  }

  private StateSerializer setupStateSerializer(SyncEvent event) {
    List<byte[]> eventStates = event.getStateStorage();
    Map<Hash, byte[]> mappedEventStates = new HashMap<>();
    for (byte[] bytes : eventStates) {
      Hash hash = Hash.create(stream -> stream.write(bytes));
      mappedEventStates.put(hash, bytes);
    }
    MemoryStateStorage stateStorage = new MemoryStateStorage(mappedEventStates);
    return new StateSerializer(stateStorage, true, true);
  }

  private void assertCheckRoutingPartOfNextInitial(
      MutableChainState state, List<String> syncs, SyncEvent.ContractTransfer contractTransfer) {
    assertThat(state.getLocalAccountPluginState().getContracts().size()).isEqualTo(0);
    assertThat(state.isSyncing()).isTrue();
    assertThat(state.getShardNonce(null).isMissingSync()).isTrue();
    assertThat(state.getShardNonce("Shard0").isMissingSync()).isFalse();
    assertThat(state.getShardNonce("Shard1").isMissingSync()).isFalse();
    assertThat(syncs).containsExactlyInAnyOrder(null, "Shard1");
    assertThat(contractTransfer.contractStateHash()).isNotNull();
    assertThat(contractTransfer.pluginStateHash()).isNotNull();
  }

  private void assertCheckRoutingPartOfNext(
      ContractState contractState,
      StateSerializer stateSerializer,
      SyncEvent.ContractTransfer contractTransfer,
      SyncEvent event,
      List<String> syncs) {
    CoreContractState core = contractState.getCore();
    assertThat(stateSerializer.read(core.getContractIdentifier(), LargeByteArray.class))
        .isNotNull();

    ContractStorage contractStorage =
        stateSerializer.read(contractTransfer.pluginStateHash(), ContractStorage.class);
    assertThat(contractStorage).isNotNull();

    SyncEvent.AccountTransfer accountTransfer = event.getAccountTransfers().get(0);
    assertThat(accountTransfer).isNotNull();

    state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    syncs.clear();
    state.addActiveShard((s, syncEvent) -> syncs.add(s), "Shard1", "Shard1");
    assertThat(state.isSyncing()).isTrue();
    assertThat(state.getShardNonce(null).isMissingSync()).isTrue();
    assertThat(state.getShardNonce("Shard0").isMissingSync()).isTrue();
    assertThat(state.getShardNonce("Shard1").isMissingSync()).isFalse();
    assertThat(syncs).isEmpty();

    state =
        initializeRoutingState(
            2,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    syncs.clear();
    state.setPlugin(
        (s, syncEvent) -> syncs.add(s),
        null,
        ChainPluginType.ROUTING,
        BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_INNER,
        new byte[0]);
    assertThat(state.isSyncing()).isTrue();
    assertThat(state.getShardNonce(null).isMissingSync()).isFalse();
    assertThat(state.getShardNonce("Shard0").isMissingSync()).isTrue();
    assertThat(state.getShardNonce("Shard1").isMissingSync()).isTrue();
    assertThat(syncs).containsExactlyInAnyOrder("Shard0", "Shard1");
  }

  @Test
  public void getChainPluginTypes() {
    MutableChainState state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    Set<ChainPluginType> plugins = state.getChainPluginTypes();
    assertThat(plugins).hasSize(2);
    assertThat(plugins).containsExactlyInAnyOrder(ChainPluginType.ACCOUNT, ChainPluginType.ROUTING);
  }

  @Test
  public void determineRoute() {
    MutableChainState state =
        initializeRoutingState(
            1,
            TestContracts.CONTRACT_BINDER_PUBLIC,
            TestContracts.CONTRACT_LONG,
            TestObjects.CONTRACT_PUB1,
            new StateLong(2L));
    assertThat(state.determineRoute(null)).isEqualTo(null);
  }

  @Test
  public void getPluginInteraction() {
    assertThat(state.getPluginInteractions(ChainPluginType.ACCOUNT))
        .isNotNull()
        .isInstanceOf(BlockchainAccountPlugin.class);
    assertThat(state.getPluginInteractions(ChainPluginType.CONSENSUS)).isNull();
    assertThat(state.getPluginInteractions(ChainPluginType.ROUTING)).isNull();
  }

  @Test
  public void getAccountPluginJarIdentifier() {
    Hash expected = state.getPluginJar(ChainPluginType.ACCOUNT).getIdentifier();
    assertThat(state.getPluginJarIdentifier(ChainPluginType.ACCOUNT)).isEqualTo(expected);
  }

  @Test
  public void getNullAsPluginJarIdentifierWhenNoPlugin() {
    assertThat(state.getPluginJarIdentifier(ChainPluginType.CONSENSUS)).isEqualTo(null);
  }

  @Test
  public void unableToCreateNonAccountAccount() {
    assertThatThrownBy(() -> state.createAccount(TestObjects.CONTRACT_PUB1))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private MutableChainState initializeRoutingState(
      int shardsAfter,
      byte[] binderClass,
      byte[] contractClass,
      BlockchainAddress contract,
      StateSerializable innerState) {
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(),
            builder -> {
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ACCOUNT,
                  FeePluginHelper.createJar(),
                  SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ROUTING,
                  BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_INNER,
                  SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
              clearSync(builder);
              for (int i = 0; i < shardsAfter; i++) {
                builder.addActiveShard(FunctionUtility.noOpBiConsumer(), null, "Shard" + i);
                clearSync(builder);
              }
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              Hash binder = builder.saveJar(binderClass);
              Hash abiJar = builder.saveJar(TestContracts.CONTRACT_LONG);
              Hash contractJarHash = builder.saveJar(contractClass);
              CoreContractState core =
                  CoreContractState.create(binder, contractJarHash, abiJar, 1234);
              builder.createContract(contract, core);
              builder.setContractState(contract, innerState);
            });
    assertThat(state.getActiveShards()).hasSize(shardsAfter);
    assertThat(state.isSyncing()).isFalse();

    return state;
  }

  private MutableChainState initializeRoutingStateWithoutAccountPlugin() {
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(),
            builder -> {
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ROUTING,
                  BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_INNER,
                  SafeDataOutputStream.serialize(FunctionUtility.noOpConsumer()));
              clearSync(builder);
              builder.addActiveShard(FunctionUtility.noOpBiConsumer(), null, "Shard0");
              clearSync(builder);
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              Hash binder = builder.saveJar(TestContracts.CONTRACT_BINDER_PUBLIC);
              Hash abiJar = builder.saveJar(TestContracts.CONTRACT_LONG);
              Hash contractJarHash = builder.saveJar(TestContracts.CONTRACT_LONG);
              CoreContractState core =
                  CoreContractState.create(binder, contractJarHash, abiJar, 1234);
              builder.createContract(TestObjects.CONTRACT_PUB1, core);
              builder.setContractState(TestObjects.CONTRACT_PUB1, new StateLong(2L));
            });
    assertThat(state.getActiveShards()).hasSize(1);
    assertThat(state.isSyncing()).isFalse();

    return state;
  }

  private void clearSync(MutableChainState builder) {
    for (String activeShard : builder.getActiveShards()) {
      clearSync(builder, activeShard);
    }
    clearSync(builder, null);
  }

  private void clearSync(MutableChainState builder, String activeShard) {
    ShardNonces shardNonce = builder.getShardNonce(activeShard);
    if (shardNonce.isMissingSync()) {
      builder.incomingSync(activeShard, new SyncEvent(List.of(), List.of(), List.of()));
    }
  }

  /** Create contract state for tests. */
  public static ContractState create(byte[] handler) {
    return ContractState.create(
        CoreContractStateTest.create(
            TestObjects.EMPTY_HASH, TestObjects.EMPTY_HASH, handler.length));
  }

  private static final byte[] ICING_ACCOUNT_PLUGIN =
      JarBuilder.buildJar(
          IcingAccountPlugin.class,
          NullBlockchainAccountPlugin.class,
          AccumulatedFees.class,
          ContractStorage.class,
          FeePluginHelper.class,
          FeeState.class,
          GasAndCoinFeePluginGlobal.class);

  /** Test account plugin returning all contracts as iced. */
  public static final class IcingAccountPlugin extends NullBlockchainAccountPlugin {

    @Override
    public GasAndCoinFeePluginGlobal migrateGlobal(
        StateAccessor currentGlobal, SafeDataInputStream rpc) {
      return new GasAndCoinFeePluginGlobal(null, null, rpc.readBoolean(), rpc.readBoolean());
    }

    @Override
    public IcedContractState<AccumulatedFees, ContractStorage> updateActiveContractIced(
        PluginContext pluginContext,
        GasAndCoinFeePluginGlobal globalState,
        BlockchainAccountPlugin.ContractState<AccumulatedFees, ContractStorage> currentState,
        BlockchainAddress contract,
        long size) {
      BlockchainAccountPlugin.ContractState<AccumulatedFees, ContractStorage> state;
      if (globalState.removeContract()) {
        state = null;
      } else {
        state = new BlockchainAccountPlugin.ContractState<>(null, new ContractStorage());
      }
      return new IcedContractState<>(globalState.markContractIced(), state);
    }
  }
}
