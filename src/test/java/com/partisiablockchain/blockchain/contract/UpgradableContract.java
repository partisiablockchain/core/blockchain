package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import org.assertj.core.api.Assertions;

/**
 * An extension to the contract that allows it to be upgraded, by first checking if the upgrade is
 * allowed and then migrating the state.
 */
public interface UpgradableContract<ContextT, OpenT extends StateSerializable> {

  /**
   * Called during the flow for upgrading a contract. Ensures that the supplied contract identifiers
   * matches the expected identifiers supplied in the RPC.
   *
   * @param context context for the current execution
   * @param state state for the contract
   * @param oldContractBytecodeIds identifiers for old contract's bytecode
   * @param newContractBytecodeIds identifiers for new contract's bytecode
   * @param rpc invocation data
   * @return true if upgrade allowed, false otherwise
   */
  @SuppressWarnings("unused")
  default boolean onAcquireUpgradePermit(
      ContextT context,
      OpenT state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(rpc);
    Hash expectedOldContractIdentifier = Hash.read(input);
    Hash expectedOldBinderIdentifier = Hash.read(input);
    Hash expectedOldAbiIdentifier = Hash.read(input);
    ContractIdentifiers expectedOldIdentifiers =
        new ContractIdentifiers(
            expectedOldContractIdentifier, expectedOldBinderIdentifier, expectedOldAbiIdentifier);
    Assertions.assertThat(oldContractBytecodeIds).isEqualTo(expectedOldIdentifiers);

    Hash expectedNewContractIdentifier = Hash.read(input);
    Hash expectedNewBinderIdentifier = Hash.read(input);
    Hash expectedNewAbiIdentifier = Hash.read(input);
    ContractIdentifiers expectedNewIdentifiers =
        new ContractIdentifiers(
            expectedNewContractIdentifier, expectedNewBinderIdentifier, expectedNewAbiIdentifier);
    Assertions.assertThat(newContractBytecodeIds).isEqualTo(expectedNewIdentifiers);

    return input.readBoolean();
  }

  /**
   * Migrate the state if contract upgrade was allowed.
   *
   * @param context context for the current execution
   * @param oldState state before upgrade
   * @param permit permit to upgrade contract
   * @param rpc invocation data
   * @return migrated state
   */
  OpenT onUpgradeState(
      ContextT context, StateAccessor oldState, ContractUpgradePermit permit, byte[] rpc);

  /**
   * Remove the 6 hashes at the head of the RPC.
   *
   * @param rpc to remove header from
   * @return remaining bytes
   */
  default byte[] removeContractIdentifierHeader(byte[] rpc) {
    int headerSize = 32 * 6;
    byte[] result = new byte[rpc.length - headerSize];
    System.arraycopy(rpc, headerSize, result, 0, rpc.length - headerSize);
    return result;
  }
}
