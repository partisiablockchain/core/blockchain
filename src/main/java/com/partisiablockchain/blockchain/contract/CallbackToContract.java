package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContextImpl;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.ContractExecution;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** A transaction sent to a contract for callbacks. */
public final class CallbackToContract
    implements EventTransaction.ContractInnerEvent, ContractExecution {

  private final BlockchainAddress from;
  private final BlockchainAddress address;
  private final long cost;
  private final Hash callbackIdentifier;
  private final LargeByteArray callbackRpc;

  /**
   * Creates a new callback transaction.
   *
   * @param address the contract address to target
   * @param callback the callback to create for
   */
  public CallbackToContract(BlockchainAddress address, CallbackCreator.Callback callback) {
    this(
        address,
        callback.callbackIdentifier,
        callback.from,
        callback.rpcForCallback,
        callback.allocatedCost);
  }

  private CallbackToContract(
      BlockchainAddress address,
      Hash callbackIdentifier,
      BlockchainAddress from,
      LargeByteArray callbackRpc,
      long cost) {
    this.callbackIdentifier = callbackIdentifier;
    this.from = from;
    this.callbackRpc = callbackRpc;
    this.address = address;
    this.cost = cost;
  }

  /**
   * Reads transaction from the stream.
   *
   * @param stream the stream to read
   * @return the read CallbackToContractTransaction
   */
  public static CallbackToContract readInner(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    Hash callbackIdentifier = Hash.read(stream);
    BlockchainAddress from = BlockchainAddress.read(stream);
    long cost = stream.readLong();
    LargeByteArray callbackRpc = new LargeByteArray(DataStreamLimit.readDynamicBytes(stream));
    return new CallbackToContract(address, callbackIdentifier, from, callbackRpc, cost);
  }

  @Override
  public EventTransaction.EventType getEventType() {
    return EventTransaction.EventType.CALLBACK;
  }

  @Override
  public void writeInner(SafeDataOutputStream safeDataOutputStream) {
    address.write(safeDataOutputStream);
    callbackIdentifier.write(safeDataOutputStream);
    from.write(safeDataOutputStream);
    safeDataOutputStream.writeLong(cost);
    safeDataOutputStream.writeDynamicBytes(callbackRpc.getData());
  }

  @Override
  public EventResult<? extends BinderEvent> execute(ExecutionContextTransaction executionContext) {
    return typedInvoke(executionContext);
  }

  @SuppressWarnings("unchecked")
  private <T extends StateSerializable> EventResult<? extends BinderEvent> typedInvoke(
      ExecutionContextTransaction executionContext) {
    MutableChainState state = executionContext.getState();

    BlockchainContractContextImpl context = new BlockchainContractContextImpl(address);

    BlockchainContract<T, BinderEvent> binder = state.getBlockchainContract(address);
    T currentContractState = (T) state.getContractState(address);
    CallbackContext callbackContext = state.callbackContext(address, callbackIdentifier);
    BlockchainResult<T, BinderEvent> result =
        binder.callback(
            executionContext,
            context,
            currentContractState,
            callbackContext,
            callbackRpc.getData());

    state.setCallbackResult(address, callbackIdentifier, callbackContext.isSuccess());
    state.setContractState(address, result.getState());
    return result.events();
  }

  @Override
  public BlockchainAddress target() {
    return address;
  }

  /**
   * Get identifier of callback.
   *
   * @return identifier of callback
   */
  public Hash callbackIdentifier() {
    return callbackIdentifier;
  }

  @Override
  public String toString() {
    return "CallbackToContract{"
        + "address="
        + address
        + ", callbackIdentifier="
        + callbackIdentifier
        + ", cost="
        + cost
        + '}';
  }

  /**
   * Get allocated cost of callback.
   *
   * @return allocated cost
   */
  public long getCost() {
    return cost;
  }

  /**
   * Get sender of callback.
   *
   * @return blockchain address of sender
   */
  public BlockchainAddress from() {
    return from;
  }
}
