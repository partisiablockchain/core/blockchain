package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import java.util.Objects;

/** A final block with the corresponding transactions. */
public final class ExecutableBlock {

  private final FinalBlock block;
  private final List<SignedTransaction> transactions;
  private final List<ExecutableEvent> eventTransactions;

  /**
   * Create executable block.
   *
   * @param block the final block
   * @param transactions the list of signed transactions
   * @param eventTransactions the list of event transactions
   */
  public ExecutableBlock(
      FinalBlock block,
      List<SignedTransaction> transactions,
      List<ExecutableEvent> eventTransactions) {
    this.block = block;
    this.transactions = transactions;
    this.eventTransactions = eventTransactions;
  }

  /**
   * Get final block.
   *
   * @return final block
   */
  public FinalBlock getBlock() {
    return block;
  }

  /**
   * Get signed transactions.
   *
   * @return list of signed transactions
   */
  public List<SignedTransaction> getTransactions() {
    return transactions;
  }

  /**
   * Get executable events.
   *
   * @return list of executable events
   */
  public List<ExecutableEvent> getEventTransactions() {
    return eventTransactions;
  }

  @Override
  public String toString() {
    return "ExecutableBlock{block="
        + block
        + ", transactions="
        + transactions
        + ", events="
        + eventTransactions
        + '}';
  }

  /**
   * Determine if the transactions and events contained in this block is the same as the one in the
   * contained block.
   *
   * @return true if the transactions and events are complete
   */
  public boolean isMissingTransactionOrEvents() {
    boolean containsAllTransactions =
        transactionsMatches(eventTransactions, block.getBlock().getEventTransactions())
            && transactionsMatches(transactions, block.getBlock().getTransactions());
    return !containsAllTransactions;
  }

  private boolean transactionsMatches(
      List<? extends ExecutableTransaction> executableTransactions, List<Hash> expected) {
    List<Hash> actualTransactions =
        executableTransactions.stream()
            .filter(Objects::nonNull)
            .map(ExecutableTransaction::identifier)
            .toList();
    boolean containsNullElements = actualTransactions.size() != executableTransactions.size();
    return !containsNullElements && expected.equals(actualTransactions);
  }
}
