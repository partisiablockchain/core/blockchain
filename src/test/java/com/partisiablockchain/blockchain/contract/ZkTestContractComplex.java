package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.contract.zk.ZkContractContext;
import com.partisiablockchain.contract.zk.ZkState;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import java.util.function.Supplier;

/** A test Zk contract using a state object with parameterized types. */
public final class ZkTestContractComplex
    extends ZkContract<
        StateComplex<Integer, String, Long>, StateVoid, ZkClosed<StateVoid>, Supplier<String>> {

  private static final Supplier<String> computation = new TestComputation();

  /** Default constructor. */
  public ZkTestContractComplex() {
    super(computation);
  }

  @Override
  public StateComplex<Integer, String, Long> onCreate(
      ZkContractContext context,
      ZkState<StateVoid, ZkClosed<StateVoid>> zkState,
      SafeDataInputStream invocation) {
    return new StateComplex<>(0, "Init", 0L);
  }

  @Override
  public StateComplex<Integer, String, Long> onCallback(
      ZkContractContext context,
      ZkState<StateVoid, ZkClosed<StateVoid>> zkState,
      StateComplex<Integer, String, Long> state,
      CallbackContext callbackContext,
      SafeDataInputStream rpc) {
    return new StateComplex<>(rpc.readInt(), rpc.readString(), rpc.readLong());
  }

  @Override
  public StateComplex<Integer, String, Long> onOpenInput(
      ZkContractContext context,
      ZkState<StateVoid, ZkClosed<StateVoid>> zkState,
      StateComplex<Integer, String, Long> state,
      SafeDataInputStream invocation) {
    return new StateComplex<>(invocation.readInt(), invocation.readString(), invocation.readLong());
  }

  private static final class TestComputation implements Supplier<String> {

    @Override
    public String get() {
      return "Expected: Unable to compute";
    }
  }
}
