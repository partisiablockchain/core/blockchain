package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ExecutedTransactionTest {

  @Test
  public void didExecutionSucceed() {
    ExecutedTransaction executedTransaction = create(true, events(3));
    Assertions.assertThat(executedTransaction.didExecutionSucceed()).isTrue();
    Assertions.assertThat(create(false, null).didExecutionSucceed()).isFalse();
  }

  @Test
  public void numberOfEvents() {
    Assertions.assertThat(create(true, events(3)).numberOfEvents()).isEqualTo(3);
    Assertions.assertThat(create(true, events(0)).numberOfEvents()).isEqualTo(0);
    Assertions.assertThat(create(false, null).numberOfEvents()).isEqualTo(0);
  }

  @Test
  public void getEvents() {
    FixedList<Hash> events = events(3);
    Assertions.assertThat(create(true, events).getEvents()).containsExactlyElementsOf(events);
    Assertions.assertThat(create(true, events(0)).getEvents()).isEmpty();
    Assertions.assertThat(create(false, null).getEvents()).isNull();
  }

  @Test
  public void getBlockHash() {
    Assertions.assertThat(create(false, null).getBlockHash()).isEqualTo(TestObjects.EMPTY_HASH);
  }

  @Test
  public void toStringTest() {
    EventTransaction eventTransaction =
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_ONE,
            10_000L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("destinationShard", 13),
            0L,
            1L);

    FixedList<Hash> events = events(2);
    ExecutedTransaction executedTransaction =
        ExecutedTransaction.create(
            TestObjects.EMPTY_HASH,
            new ExecutableEvent("originShard", eventTransaction),
            true,
            events);

    Assertions.assertThat(executedTransaction.toString())
        .contains(executedTransaction.getBlockHash().toString())
        .contains(eventTransaction.toString())
        .contains(events.toString());
  }

  /**
   * When reading an executable event the actual event is read from another storage location. If the
   * event cannot be found null should be returned instead of an incomplete executed transaction.
   */
  @Test
  void readingExecutableTransactionWithMissingStorageShouldReturnNull() {
    ExecutableEvent executableEvent =
        new ExecutableEvent(
            "originShard",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                TestObjects.ACCOUNT_ONE,
                10_000L,
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
                new ShardRoute("destinationShard", 13),
                0L,
                1L));
    ExecutedTransaction executedTransaction =
        ExecutedTransaction.create(
            TestObjects.EMPTY_HASH, executableEvent, true, FixedList.create());

    byte[] serializedTransaction = SafeDataOutputStream.serialize(executedTransaction);

    ExecutedTransaction transactionWithIncompleteStorage =
        ExecutedTransaction.read(
            SafeDataInputStream.createFromBytes(serializedTransaction), "ChainId", hash -> null);
    Assertions.assertThat(transactionWithIncompleteStorage).isNull();

    ExecutedTransaction transactionFromStorage =
        ExecutedTransaction.read(
            SafeDataInputStream.createFromBytes(serializedTransaction),
            "ChainId",
            hash -> {
              Assertions.assertThat(hash).isEqualTo(executableEvent.identifier());
              return executableEvent;
            });
    Assertions.assertThat(transactionFromStorage)
        .usingRecursiveComparison()
        .isEqualTo(executedTransaction);
  }

  private ExecutedTransaction create(boolean success, FixedList<Hash> events) {
    SignedTransaction signedTransaction =
        SignedTransaction.create(
                new CoreTransactionPart(123, 1234, 12345),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]))
            .sign(new KeyPair(), "SomeChain");
    ExecutedTransaction executedTransaction =
        ExecutedTransaction.create(TestObjects.EMPTY_HASH, signedTransaction, success, events);
    ExecutedTransaction transaction =
        ExecutedTransaction.read(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(executedTransaction::write)),
            "SomeChain",
            hash -> null);
    Assertions.assertThat(transaction).usingRecursiveComparison().isEqualTo(executedTransaction);
    return transaction;
  }

  private FixedList<Hash> events(int count) {
    return FixedList.create(
        IntStream.range(0, count).mapToObj(i -> Hash.create(s -> s.writeInt(i))));
  }
}
