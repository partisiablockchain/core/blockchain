package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CallbackToContractTest {

  @Test
  public void simpleMethods() {
    CallbackToContract transaction =
        new CallbackToContract(
            TestObjects.CONTRACT_PUB1,
            new CallbackCreator.Callback(
                TestObjects.EMPTY_HASH, TestObjects.ACCOUNT_ONE, 5_000L, null));
    Assertions.assertThat(transaction.getEventType())
        .isEqualTo(EventTransaction.EventType.CALLBACK);
    Assertions.assertThat(transaction.getCost()).isEqualTo(5_000);
    Assertions.assertThat(transaction.from()).isEqualTo(TestObjects.ACCOUNT_ONE);
  }
}
