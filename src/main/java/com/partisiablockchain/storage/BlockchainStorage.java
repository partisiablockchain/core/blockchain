package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.ExecutedState;
import com.partisiablockchain.blockchain.FailureCause;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Stores every part of the persisted state for the blockchain. */
public final class BlockchainStorage implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(BlockchainStorage.class);
  private final BlockStateStorage blockStateStorage;
  private final HashStorage<ExecutableEvent> eventTransactionDir;
  private final HashStorage<ExecutedTransaction> transactionDir;
  private final HashStorage<FailureCause> transactionFailure;
  private final HashStorage<TransactionCost> transactionCost;

  /**
   * Creates a new storage for the Blockchain that stores the received data in the directory.
   *
   * @param root the directory to store files in.
   * @param chainId the id of the chain
   */
  public BlockchainStorage(RootDirectory root, String chainId) {
    this(new BlockStateStorage(root), root, chainId);
  }

  /**
   * Creates a new storage for the Blockchain that stores the received data in the directory.
   *
   * @param blockStorage storage for blocks and states
   * @param root the directory to store files in.
   * @param chainId the id of the chain
   */
  public BlockchainStorage(BlockStateStorage blockStorage, RootDirectory root, String chainId) {
    this.blockStateStorage = blockStorage;
    this.eventTransactionDir =
        root.createHashStorage("eventTransactions", ExecutableEvent::unsafeSyncRead);
    this.transactionDir =
        root.createHashStorage(
            "transactions",
            inputStream ->
                ExecutedTransaction.read(inputStream, chainId, this::getEventTransaction),
            false);
    this.transactionFailure =
        root.createHashStorage("transactionFailureCauses", FailureCause::read);
    this.transactionCost = root.createHashStorage("transactionCost", TransactionCost::read);
  }

  /**
   * Get hash of block.
   *
   * @param blockTime block time to get hash for
   * @return hash of block for provided block time
   */
  public Hash getBlockHash(long blockTime) {
    return blockStateStorage.getBlockHash(blockTime);
  }

  /**
   * Returns block from supplied hash.
   *
   * @param hash the block hash to look up
   * @return the block if found, null otherwise
   */
  public Block getBlock(Hash hash) {
    return blockStateStorage.getBlock(hash);
  }

  /**
   * Returns block from supplied block time.
   *
   * @param blockTime the block time to lookup
   * @return the block if found, null otherwise
   */
  public Block getBlock(long blockTime) {
    return blockStateStorage.getBlock(blockTime);
  }

  /**
   * Returns finalized block from supplied block time.
   *
   * @param blockTime the block time to lookup
   * @return the finalized block if found, null otherwise
   */
  public FinalBlock getFinalizedBlock(long blockTime) {
    return blockStateStorage.getFinalizedBlock(blockTime);
  }

  /**
   * Get the state with the supplied identity hash.
   *
   * @param stateHash the identifier of the state
   * @return the loaded state or null if not found
   */
  public ExecutedState getState(Hash stateHash) {
    return blockStateStorage.getState(stateHash);
  }

  /**
   * Gets the transactions for a specific block hash.
   *
   * @param block the block
   * @return the list of executed transactions
   */
  public List<ExecutedTransaction> getTransactionsForBlock(Block block) {
    return Stream.concat(block.getEventTransactions().stream(), block.getTransactions().stream())
        .map(transactionDir::read)
        .collect(Collectors.toList());
  }

  /**
   * Gets the failure object for a transaction for a specific transaction hash.
   *
   * @param transactionHash the hash of the transaction
   * @return the error object
   */
  public FailureCause getTransactionFailure(Hash transactionHash) {
    return transactionFailure.read(transactionHash);
  }

  /**
   * Get cost of transaction.
   *
   * @param transactionHash the transaction identifier
   * @return the cost of the transaction
   */
  public TransactionCost getTransactionCost(Hash transactionHash) {
    return transactionCost.read(transactionHash);
  }

  /**
   * Gets a transaction for a specific transaction hash.
   *
   * @param transactionHash the hash of the transaction
   * @return the transaction
   */
  public ExecutedTransaction getTransaction(Hash transactionHash) {
    return transactionDir.read(transactionHash);
  }

  /**
   * Store list of event transactions.
   *
   * @param eventTransactions the list of event transactions
   */
  public void storeEventTransactions(List<ExecutableEvent> eventTransactions) {
    for (ExecutableEvent eventTransaction : eventTransactions) {
      storeEventTransaction(eventTransaction);
    }
  }

  private void storeEventTransaction(ExecutableEvent eventTransaction) {
    eventTransactionDir.write(eventTransaction.identifier(), eventTransaction);
  }

  /**
   * Gets an event transaction for a specific transaction hash.
   *
   * @param transactionHash the hash of the event transaction
   * @return the event transaction
   */
  public ExecutableEvent getEventTransaction(Hash transactionHash) {
    return eventTransactionDir.read(transactionHash);
  }

  /**
   * Registers a block as accepted - and final enough for storage.
   *
   * @param finalBlock the finalized block
   * @param executedTransactions the transactions with their execution status
   * @param state the state of the blockchain
   * @param failures any error in the transactions
   */
  public void registerAcceptedBlock(
      FinalBlock finalBlock,
      List<ExecutedTransaction> executedTransactions,
      ImmutableChainState state,
      Map<Hash, FailureCause> failures) {
    registerAcceptedBlock(finalBlock, executedTransactions, state, failures, Map.of());
  }

  /**
   * Registers a block as accepted.
   *
   * @param finalBlock the block to register
   * @param executedTransactions the transactions included in the block
   * @param state the state of the chain
   * @param failures the failed transactions
   * @param cost the cost of each transaction
   */
  public void registerAcceptedBlock(
      FinalBlock finalBlock,
      List<ExecutedTransaction> executedTransactions,
      ImmutableChainState state,
      Map<Hash, FailureCause> failures,
      Map<Hash, TransactionCost> cost) {
    Block block = finalBlock.getBlock();
    logger.debug("Writing new block {}", block);
    for (ExecutedTransaction executedTransaction : executedTransactions) {
      ExecutableTransaction inner = executedTransaction.getInner();
      transactionDir.write(inner.identifier(), executedTransaction);
      if (inner instanceof ExecutableEvent) {
        storeEventTransaction((ExecutableEvent) inner);
      }
    }
    for (Hash hash : failures.keySet()) {
      transactionFailure.write(hash, failures.get(hash));
    }

    for (Map.Entry<Hash, TransactionCost> entry : cost.entrySet()) {
      transactionCost.write(entry.getKey(), entry.getValue());
    }

    // Save block last as this references the other parts
    blockStateStorage.registerAcceptedBlock(finalBlock, state);
  }

  /**
   * Finds the latest block and state pair that is stored in this storage.
   *
   * @return the loaded block and state or null if none is found
   */
  public ValidBlock loadLatestValidBlock() {
    return blockStateStorage.loadLatestValidBlock();
  }

  /**
   * Load a block with corresponding transactions from storage.
   *
   * @param blockTime the block time to load block for
   * @return the loaded block, or null if block or transactions are missing
   */
  public ExecutableBlock loadExecutableBlock(long blockTime) {
    FinalBlock block = blockStateStorage.getFinalizedBlock(blockTime);
    if (block != null) {
      List<ExecutedTransaction> transactions = getTransactionsForBlock(block.getBlock());
      if (!transactions.contains(null)) {
        logger.info("Loaded executable block from storage {}", block.getBlock());

        List<SignedTransaction> signedTransactions =
            SignedTransaction.select(transactions.stream().map(ExecutedTransaction::getInner));
        List<ExecutableEvent> eventTransactions =
            ExecutableEvent.select(transactions.stream().map(ExecutedTransaction::getInner));
        return new ExecutableBlock(block, signedTransactions, eventTransactions);
      } else {
        logger.info(
            "Unable to load executable block due to missing transactions for block {}", block);
      }
    }
    return null;
  }

  @Override
  public void close() {
    blockStateStorage.close();
  }

  /**
   * Get the raw state storage.
   *
   * @return raw state storage
   */
  public StateStorageRaw getStateStorage() {
    return blockStateStorage.getStateStorage();
  }

  /** A valid block is a final block along with the state. */
  public static final class ValidBlock {

    private final FinalBlock block;
    private final ExecutedState state;
    private final Block parentBlock;
    private final ExecutedState finalizedState;

    ValidBlock(
        FinalBlock block, ExecutedState state, Block parentBlock, ExecutedState finalizedState) {
      this.block = block;
      this.state = state;
      this.parentBlock = parentBlock;
      this.finalizedState = finalizedState;
    }

    /**
     * Get final block.
     *
     * @return final block
     */
    public FinalBlock getBlock() {
      return block;
    }

    /**
     * Get state for block.
     *
     * @return state for block
     */
    public ExecutedState getState() {
      return state;
    }

    /**
     * Get parent block.
     *
     * @return parent block
     */
    public Block getParentBlock() {
      return parentBlock;
    }

    /**
     * Check if block is genesis block for chain.
     *
     * @return true if block is genesis
     */
    public boolean isGenesis() {
      return getParentBlock() == null;
    }

    /**
     * Get finalized state.
     *
     * @return finalized state
     */
    public ExecutedState getFinalizedState() {
      return finalizedState;
    }
  }
}
