package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FloodableEventCompressedTest {

  private final StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
  private final StateSerializer stateSerializer = new StateSerializer(stateStorage, true);

  @Test
  public void compressAndDecompress() {
    ExecutableEvent event = FloodableEventTest.createEvent(1);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            "ChainId",
            "ShardId",
            event.identifier());
    stateSerializer.write(state.getExecutedState());
    FloodableEvent floodEvent =
        FloodableEvent.create(
            event,
            new FinalBlock(
                new Block(10L, 1L, 2, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of()),
                new byte[0]),
            stateStorage);
    FloodableEventCompressed compressed = floodEvent.compress();
    FloodableEvent decompressed = compressed.decompress((unused1, unused2) -> event);
    Assertions.assertThat(floodEvent.getBlock()).isEqualTo(decompressed.getBlock());
    Assertions.assertThat(floodEvent.getChainId()).isEqualTo(decompressed.getChainId());
    Assertions.assertThat(floodEvent.getExecutableEvent().identifier())
        .isEqualTo(decompressed.getExecutableEvent().identifier());
    Assertions.assertThat(floodEvent.getCommitteeForFinalizingBlock())
        .isEqualTo(decompressed.getCommitteeForFinalizingBlock());

    Assertions.assertThat(floodEvent.getBlock()).isEqualTo(compressed.block());
    Assertions.assertThat(floodEvent.getExecutableEvent().identifier())
        .isEqualTo(compressed.eventIdentifier());
  }

  @Test
  public void readWrite() {
    ExecutableEvent event = FloodableEventTest.createEvent(3);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            "Chain Chain",
            "ShardId",
            event.identifier());
    stateSerializer.write(state.getExecutedState());
    FloodableEvent floodEvent =
        FloodableEvent.create(
            event,
            new FinalBlock(
                new Block(12L, 2L, 5, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of()),
                new byte[0]),
            stateStorage);
    FloodableEventCompressed compressed = floodEvent.compress();
    FloodableEventCompressed read =
        FloodableEventCompressed.read(
            SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(compressed::write)));
    Assertions.assertThat(read.block().getBlock()).isEqualTo(compressed.block().getBlock());
    Assertions.assertThat(read.eventIdentifier()).isEqualTo(compressed.eventIdentifier());
    Assertions.assertThat(read.proof().getAvlProof().getKey())
        .isEqualTo(compressed.proof().getAvlProof().getKey());
  }
}
