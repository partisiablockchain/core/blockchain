package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/** Generic hash based storage in pbc. */
public interface Storage {

  /**
   * Read a value from storage.
   *
   * @param hash where value is stored
   * @param reader function for reading a value from stream
   * @param <T> type of the value to read
   * @return value read from storage
   */
  <T> T read(Hash hash, Function<SafeDataInputStream, T> reader);

  /**
   * Stream values from storage.
   *
   * @param reader function for reading a value from stream
   * @param <T> type of the values to stream
   * @return stream of value type
   */
  <T> Stream<T> stream(Function<SafeDataInputStream, T> reader);

  /**
   * Read value as raw bytes.
   *
   * @param hash where value is stored
   * @return raw bytes read
   */
  byte[] readRaw(Hash hash);

  /**
   * Check if storage has a value on provided location.
   *
   * @param hash location to check
   * @return true if storage has a value on provided location, false otherwise
   */
  boolean has(Hash hash);

  /**
   * Check if storage is empty.
   *
   * @return true if storage is empty, false otherwise
   */
  boolean isEmpty();

  /**
   * Remove value from storage.
   *
   * @param hash where value is stored
   */
  void remove(Hash hash);

  /**
   * Write value to storage.
   *
   * @param hash where to store value
   * @param writer method for writing value to stream
   */
  void write(Hash hash, Consumer<SafeDataOutputStream> writer);

  /** Fully clear storage. */
  void clear();
}
