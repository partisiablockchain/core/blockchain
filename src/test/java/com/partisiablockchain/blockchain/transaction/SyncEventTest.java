package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SyncEventTest {

  @Test
  public void readWrite() {
    SyncEvent.AccountTransfer transfer =
        new SyncEvent.AccountTransfer(
            TestObjects.ACCOUNT_TWO,
            Hash.create(s -> s.writeInt(1)),
            Hash.create(s -> s.writeInt(1)));
    SyncEvent.ContractTransfer contractTransfer =
        new SyncEvent.ContractTransfer(
            TestObjects.ACCOUNT_TWO,
            Hash.create(s -> s.writeInt(1)),
            Hash.create(s -> s.writeInt(1)));
    byte[] value = new byte[] {1};

    Assertions.assertThat(transfer.address()).isEqualTo(TestObjects.ACCOUNT_TWO);
    SyncEvent syncEvent =
        new SyncEvent(List.of(transfer), List.of(contractTransfer), List.of(value));
    SyncEvent deserialize =
        (SyncEvent)
            SafeDataInputStream.deserialize(
                EventTransaction.InnerEvent::read, SafeDataOutputStream.serialize(syncEvent));
    SyncEvent.AccountTransfer returnValue = deserialize.getAccountTransfers().get(0);
    Assertions.assertThat(returnValue.address()).isEqualTo(transfer.address());
    SyncEvent.ContractTransfer returnContracts = deserialize.getContractTransfers().get(0);
    Assertions.assertThat(returnContracts.address()).isEqualTo(transfer.address());
    Assertions.assertThat(returnContracts.contractStateHash()).isNotNull();
    Assertions.assertThat(returnContracts.contractStateHash())
        .isEqualTo(contractTransfer.contractStateHash());
    Assertions.assertThat(returnContracts.pluginStateHash())
        .isEqualTo(contractTransfer.pluginStateHash());
    Assertions.assertThat(returnContracts.pluginStateHash()).isNotNull();
    List<byte[]> returnStorage = deserialize.getStateStorage();
    Assertions.assertThat(returnStorage.get(0)).isEqualTo(value);
  }

  @Test
  public void readWriteUnableToReadListOfExcessiveSize() {
    SyncEvent.AccountTransfer transfer =
        new SyncEvent.AccountTransfer(
            TestObjects.ACCOUNT_TWO,
            Hash.create(s -> s.writeInt(1)),
            Hash.create(s -> s.writeInt(1)));
    SyncEvent.ContractTransfer contractTransfer =
        new SyncEvent.ContractTransfer(
            TestObjects.ACCOUNT_TWO,
            Hash.create(s -> s.writeInt(1)),
            Hash.create(s -> s.writeInt(1)));

    Assertions.assertThat(transfer.address()).isEqualTo(TestObjects.ACCOUNT_TWO);
    List<byte[]> stateStorage = new ArrayList<>();
    byte[] value = new byte[] {1};

    for (int i = 0; i < DataStreamLimit.MAX_LIST_SIZE; i++) {
      stateStorage.add(value);
    }

    SyncEvent syncEvent = new SyncEvent(List.of(transfer), List.of(contractTransfer), stateStorage);
    SyncEvent deserialize =
        (SyncEvent)
            SafeDataInputStream.deserialize(
                EventTransaction.InnerEvent::read, SafeDataOutputStream.serialize(syncEvent));
    List<byte[]> returnStorage = deserialize.getStateStorage();
    Assertions.assertThat(returnStorage.get(0)).isEqualTo(value);

    stateStorage.add(value);

    SyncEvent excessiveEvent =
        new SyncEvent(List.of(transfer), List.of(contractTransfer), stateStorage);
    Assertions.assertThatThrownBy(
            () ->
                SafeDataInputStream.deserialize(
                    EventTransaction.InnerEvent::read,
                    SafeDataOutputStream.serialize(excessiveEvent)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Unable to read list of size 1001 from stream. Maximum allowed is 1000");
  }

  @Test
  public void unsafeReadWriteAbleToReadListOfExcessiveSize() {
    List<SyncEvent.AccountTransfer> accountTransfers = new ArrayList<>();
    List<SyncEvent.ContractTransfer> contractTransfers = new ArrayList<>();
    List<byte[]> stateStorage = new ArrayList<>();
    byte[] value = new byte[] {1};

    for (int i = 0; i < DataStreamLimit.MAX_LIST_SIZE + 1; i++) {
      accountTransfers.add(
          new SyncEvent.AccountTransfer(
              TestObjects.ACCOUNT_TWO,
              Hash.create(s -> s.writeInt(1)),
              Hash.create(s -> s.writeInt(1))));
      contractTransfers.add(
          new SyncEvent.ContractTransfer(
              TestObjects.ACCOUNT_TWO,
              Hash.create(s -> s.writeInt(1)),
              Hash.create(s -> s.writeInt(1))));
      stateStorage.add(value);
    }
    SyncEvent syncEvent = new SyncEvent(accountTransfers, contractTransfers, stateStorage);
    SyncEvent deserialize =
        (SyncEvent)
            SafeDataInputStream.deserialize(
                EventTransaction.InnerEvent::unsafeSyncRead,
                SafeDataOutputStream.serialize(syncEvent));
    List<SyncEvent.AccountTransfer> returnAccountTransfers = deserialize.getAccountTransfers();
    Assertions.assertThat(returnAccountTransfers.size()).isNotZero();
    List<SyncEvent.ContractTransfer> returnContractTransfers = deserialize.getContractTransfers();
    Assertions.assertThat(returnContractTransfers.size()).isNotZero();
    List<byte[]> returnStorage = deserialize.getStateStorage();
    Assertions.assertThat(returnStorage.get(0)).isEqualTo(value);
  }
}
