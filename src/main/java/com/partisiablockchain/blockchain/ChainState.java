package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.blockchain.ChainPlugin.PluginLoader;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.tools.immutable.FixedList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Underlying state of the entire blockchain. Exposes shared interface between {@link
 * ImmutableChainState} and {@link MutableChainState}.
 *
 * <p>From an {@link ChainState} it is possible to inspect the state (and code, if applicable) of
 * all plugins, contracts ({@link CoreContractState}) and accounts ({@link AccountState}). It also
 * includes additional state for maintainance of the blockchain.
 *
 * @see ImmutableChainState variant used to store and transfer data on the blockchain.
 * @see MutableChainState variant used as a block is being executed.
 */
public abstract class ChainState {

  final StateSerializer serializer;
  private final ChainStateCache context;

  /**
   * Default constructor with cache.
   *
   * @param context cache of chain state
   */
  protected ChainState(ChainStateCache context) {
    this.context = context;
    this.serializer = context.createStateSerializer();
  }

  /**
   * Get a blockchain contract for a contract.
   *
   * @param contractAddress the address of the contract
   * @param <T> the type of the contract state
   * @param <U> the type of event
   * @return the BlockchainContract for this contract
   */
  public <T extends StateSerializable, U extends BinderEvent>
      BlockchainContract<T, U> getBlockchainContract(BlockchainAddress contractAddress) {
    CoreContractState core =
        Objects.requireNonNull(getCoreContractState(contractAddress), "Contract does not exist");
    return context.get(contractAddress, core.getBinderIdentifier(), core.getContractIdentifier());
  }

  /**
   * Get active shards.
   *
   * @return list of shard ids
   */
  public abstract FixedList<String> getActiveShards();

  /**
   * Get addresses of all accounts.
   *
   * @return set of blockchain addresses
   */
  public abstract Set<BlockchainAddress> getAccounts();

  /**
   * Get the nonce associated with an account in the state. <code>null</code> nonce indicates that
   * the account is not found in the chain state.
   *
   * @param address the account to lookup
   * @return the nonce of the account or null if the account does not exist
   */
  @SuppressWarnings("WeakerAccess")
  public Long lookupNonce(BlockchainAddress address) {
    AccountState accountState = getAccount(address);
    if (accountState != null) {
      return accountState.getNonce();
    } else {
      return null;
    }
  }

  /**
   * Get account state from address.
   *
   * @param address account address
   * @return account state
   */
  public abstract AccountState getAccount(BlockchainAddress address);

  /**
   * Get addresses of all contracts.
   *
   * @return set of blockchain addresses
   */
  public abstract Set<BlockchainAddress> getContracts();

  /**
   * Get contract state.
   *
   * @param address contract address
   * @return contract state
   */
  abstract ContractState getContract(BlockchainAddress address);

  /**
   * Get byte length of contract state in storage.
   *
   * @param address contract address
   * @return byte length of contract state in storage
   */
  public abstract Long getContractStorageLength(BlockchainAddress address);

  /**
   * Get the core contract state for a contract.
   *
   * @param contractAddress the address of the contract
   * @return the core contract state or null if the contract does not exist
   */
  public CoreContractState getCoreContractState(BlockchainAddress contractAddress) {
    ContractState value = getContract(contractAddress);
    if (value != null) {
      return value.getCore();
    } else {
      return null;
    }
  }

  /**
   * Get the state of a contract.
   *
   * @param address the address where the contract is located
   * @return the deserialized contract or null if it does not exist
   */
  public StateSerializable getContractState(BlockchainAddress address) {
    ContractState value = getContract(address);
    if (value == null) {
      return null;
    } else {
      ContractSerialization<StateSerializable> serialization =
          getBlockchainContract(address).getContractSerialization();
      return serialization.read(serializer, value.getStateHash());
    }
  }

  /**
   * Checks if a contract have changed between this and another state.
   *
   * @param contract the contract address to check
   * @param otherState the other state to check against
   * @return true if the contract has changed
   */
  public boolean hasContractChanged(BlockchainAddress contract, ChainState otherState) {
    Hash contractStateHash = getContractStateHash(contract);
    Hash otherStateHash = otherState.getContractStateHash(contract);
    return !Objects.equals(contractStateHash, otherStateHash);
  }

  private Hash getContractStateHash(BlockchainAddress address) {
    ContractState value = getContract(address);
    if (value == null) {
      return null;
    } else {
      return value.getStateHash();
    }
  }

  /**
   * Check if account exists in state.
   *
   * @param to address of account to check
   * @return true if account exists, false otherwise
   */
  public boolean existsAccounts(BlockchainAddress to) {
    return getAccount(to) != null;
  }

  <L extends StateSerializable, G extends StateSerializable, P extends BlockchainPlugin<G, L>>
      PluginLoader<G, L, P> getPluginSerialization() {
    return context.createPluginLoader();
  }

  /**
   * Get a list of all installed plugins.
   *
   * @return list of all plugin types
   */
  public abstract Set<ChainPluginType> getChainPluginTypes();

  /**
   * Get the identifier of a specific plugin.
   *
   * @param type of the plugin
   * @return Hash of the plugin
   */
  public abstract Hash getPluginJarIdentifier(ChainPluginType type);

  /**
   * Get global plugin state of specific plugin type.
   *
   * @param type of plugin
   * @return global plugin state
   */
  public abstract StateSerializable getGlobalPluginState(ChainPluginType type);

  /**
   * Get plugin jar as byte array.
   *
   * @param type of plugin
   * @return plugin jar as byte array
   */
  public abstract LargeByteArray getPluginJar(ChainPluginType type);

  /**
   * Get local account plugin state.
   *
   * @return local account plugin state
   */
  public abstract AccountPluginState<?, ?, ?> getLocalAccountPluginState();

  /**
   * Get account plugin accessor.
   *
   * @return account plugin accessor
   */
  public abstract AccountPluginAccessor getAccountPlugin();

  ChainStateCache getContext() {
    return context;
  }

  /**
   * Get consensus plugin accessor.
   *
   * @return consensus plugin accessor
   */
  public abstract ConsensusPluginAccessor getConsensusPlugin();

  /**
   * Get local consensus plugin state.
   *
   * @return local consensus plugin state
   */
  public abstract StateSerializable getLocalConsensusPluginState();

  RootConsensusPlugin defaultConsensus() {
    return new RootConsensusPlugin();
  }

  /**
   * Get routing plugin accessor.
   *
   * @return routing plugin accessor
   */
  public abstract RoutingPluginAccessor getRoutingPlugin();

  RootRoutingPlugin defaultRouting() {
    return new RootRoutingPlugin();
  }

  /**
   * Get the shared object store plugin accessor.
   *
   * @return shared object store plugin accessor
   */
  public abstract SharedObjectStorePluginAccessor getSharedObjectStorePlugin();

  EmptySharedObjectStorePlugin defaultSharedObjectStore() {
    return new EmptySharedObjectStorePlugin();
  }

  /**
   * Get interface for creating interactions against a plugin.
   *
   * @param pluginType type of plugin to interact with
   * @return plugin interaction creator
   */
  public abstract PluginInteractionCreator getPluginInteractions(ChainPluginType pluginType);

  static final class PluginSerialization<
      GlobalT extends StateSerializable,
      LocalT extends StateSerializable,
      PluginT extends BlockchainPlugin<GlobalT, LocalT>> {

    private final StateSerializer globalSerializer;
    private final StateSerializer serializer;
    private final PluginT mainClass;

    @SuppressWarnings("unchecked")
    PluginSerialization(
        StateSerializer globalSerializer, StateSerializer serializer, JarClassLoader pluginLoader) {
      this.globalSerializer = globalSerializer;
      this.serializer = serializer;
      this.mainClass = (PluginT) pluginLoader.instantiateMainClass();
    }

    PluginT instantiatePlugin() {
      return mainClass;
    }

    LocalT loadLocalValue(Hash stateHash) {
      return loadValue(
          stateHash,
          mainClass.getLocalStateClass(),
          serializer,
          mainClass.getLocalStateClassTypeParameters());
    }

    GlobalT loadGlobalValue(Hash stateHash) {
      return loadValue(stateHash, mainClass.getGlobalStateClass(), globalSerializer, List.of());
    }

    Hash saveLocalValue(LocalT local) {
      return saveValue(local, mainClass.getLocalStateClassTypeParameters());
    }

    Hash saveGlobalValue(GlobalT global) {
      return saveValue(global, List.of());
    }

    private Hash saveValue(StateSerializable state, List<Class<?>> localStateClassTypeParameters) {
      Class<?>[] typeParameters = localStateClassTypeParameters.toArray(new Class<?>[0]);
      return serializer.write(state, typeParameters).hash();
    }

    private <T extends StateSerializable> T loadValue(
        Hash stateHash,
        Class<T> localStateClass,
        StateSerializer serializer,
        List<Class<?>> localStateClassTypeParameters) {
      Class<?>[] typeParameters = localStateClassTypeParameters.toArray(new Class<?>[0]);
      return serializer.read(stateHash, localStateClass, typeParameters);
    }
  }

  private static final Map<ContractType, ContractCreator> CREATORS = create();

  private static Map<ContractType, ContractCreator> create() {
    Map<ContractType, ContractCreator> contractCreators = new HashMap<>();
    contractCreators.put(ContractType.SYSTEM, JarClassLoader::readSysBlockchainContract);
    contractCreators.put(ContractType.PUBLIC, JarClassLoader::readPubBlockchainContract);
    contractCreators.put(ContractType.ZERO_KNOWLEDGE, JarClassLoader::readZkBlockchainContract);
    contractCreators.put(ContractType.GOVERNANCE, JarClassLoader::readSysBlockchainContract);
    return contractCreators;
  }

  static BlockchainContract<?, ?> createContract(ContractType type, Object binder) {
    ContractCreator contractCreator = CREATORS.get(type);
    if (contractCreator == null) {
      throw new IllegalArgumentException("Unknown type " + type);
    } else {
      return contractCreator.create(binder);
    }
  }

  /** Functional interface for creating contracts, there is one for each type. */
  @FunctionalInterface
  public interface ContractCreator {

    /**
     * Create contract.
     *
     * @param binderParameter binder for contract
     * @return created contract
     */
    BlockchainContract<?, ?> create(Object binderParameter);
  }
}
