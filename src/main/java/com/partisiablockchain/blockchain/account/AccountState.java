package com.partisiablockchain.blockchain.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Current state of an account - wrapping the nonce. */
@Immutable
public final class AccountState implements StateSerializable {

  private final long nonce;

  @SuppressWarnings("unused")
  AccountState() {
    nonce = -1;
  }

  /**
   * Create a new account state.
   *
   * @param nonce initial nonce
   */
  private AccountState(long nonce) {
    this.nonce = nonce;
  }

  /**
   * Create a new account state.
   *
   * @return the created account state
   */
  public static AccountState create() {
    return create(1L);
  }

  /**
   * Create a new account state with a nonce.
   *
   * @param nonce initial nonce for state
   * @return created account state
   */
  public static AccountState create(long nonce) {
    return new AccountState(nonce);
  }

  /**
   * Bumps nonce.
   *
   * @return the updated account state
   */
  public AccountState bumpNonce() {
    return new AccountState(getNonce() + 1);
  }

  /**
   * Get nonce.
   *
   * @return the current nonce
   */
  public long getNonce() {
    return nonce;
  }

  @Override
  public String toString() {
    return "AccountState{" + "nonce=" + nonce + '}';
  }
}
