package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.binder.pub.PubBinderContext;
import com.partisiablockchain.blockchain.contract.UpgradableContract;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;

/**
 * A faked implementation of a pub binder for test purposes. Can be used to upgrade public contract.
 */
public final class UpgradablePubContractBinder<StateT extends StateSerializable>
    extends AbstractPublicContractBinder<StateT> {

  /**
   * Construct a new binder.
   *
   * @param contract the associated contract
   */
  public UpgradablePubContractBinder(PubContract<StateT> contract) {
    super(contract);
  }

  @SuppressWarnings("unchecked")
  @Override
  public ContractUpgradePermit acquireUpgradePermit(
      PubBinderContext binderContext,
      StateT state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    AbstractPublicContractBinder.PubContext context = new PubContext(binderContext);
    boolean upgradeIsAllowed =
        ((UpgradableContract<PubContractContext, StateT>) contract)
            .onAcquireUpgradePermit(
                context, state, oldContractBytecodeIds, newContractBytecodeIds, rpc);
    if (upgradeIsAllowed) {
      return new ContractUpgradePermit(new byte[0]);
    } else {
      throw new RuntimeException("Contract upgrade is not allowed.");
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public StateT upgradeState(
      PubBinderContext binderContext,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    AbstractPublicContractBinder.PubContext context = new PubContext(binderContext);
    boolean upgradeIsAllowed = SafeDataInputStream.createFromBytes(rpc).readBoolean();
    if (upgradeIsAllowed) {
      return ((UpgradableContract<PubContractContext, StateT>) contract)
          .onUpgradeState(context, oldState, permit, rpc);
    } else {
      throw new RuntimeException("Contract upgrade is not allowed.");
    }
  }
}
