package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.flooding.tls.CustomTlsServer;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CustomTlsServerTest {

  private KeyPair keyPair;
  private CustomTlsServer tlsServer;

  /** Sets up the custom tls server. */
  @BeforeEach
  public void setUp() {
    keyPair = new KeyPair(BigInteger.ONE);
    BcTlsCrypto tlsCrypto = new BcTlsCrypto(new SecureRandom());
    tlsServer = new CustomTlsServer(tlsCrypto, keyPair, null, null);
  }

  @Test
  public void validationFails() throws IOException {
    Certificate notSelfSignedCertificate =
        TlsUtils.createSignedCertificate(keyPair, new KeyPair(BigInteger.TWO));

    assertThatThrownBy(() -> tlsServer.notifyClientCertificate(notSelfSignedCertificate))
        .hasMessage("Certificate could not be verified");
  }

  @Test
  public void sendsCertificateRequest() {
    assertThat(tlsServer.getCertificateRequest()).isNotNull();
  }
}
