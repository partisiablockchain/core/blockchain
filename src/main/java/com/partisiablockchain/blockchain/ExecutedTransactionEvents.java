package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.storage.BlockchainStorage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Execution result of a transaction. Including any spawned events. */
public final class ExecutedTransactionEvents {

  private static final Logger logger = LoggerFactory.getLogger(ExecutedTransactionEvents.class);

  private final List<ExecutableEvent> events;
  private final boolean success;
  private final List<ExecutableEvent> ownEvents;
  private final ExecutableTransaction transaction;
  private final FailureCause exception;
  private final TransactionCost transactionCost;

  private ExecutedTransactionEvents(
      ExecutableTransaction transaction,
      boolean success,
      List<ExecutableEvent> events,
      Throwable exception,
      TransactionCost cost) {
    this.success = success;
    this.events = events;
    this.ownEvents =
        events.stream()
            .filter(ExecutedTransactionEvents::sameShard)
            .sorted(getComparator())
            .toList();
    this.transaction = transaction;
    if (exception == null) {
      this.exception = null;
    } else {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      exception.printStackTrace(pw);
      String message = Objects.requireNonNullElse(exception.getMessage(), "");
      this.exception = new FailureCause(message, sw.toString());
    }
    logger.debug("Execution of {} spawned events: {}", transaction, events);
    transactionCost = cost;
  }

  /**
   * Creates ExecutedTransactionEvents.
   *
   * @param transaction the transaction in this execution
   * @param events resulting events - both local and distributed
   * @param cost the cost
   * @return the created ExecutedTransactionEvents
   */
  public static ExecutedTransactionEvents success(
      ExecutableTransaction transaction, List<ExecutableEvent> events, TransactionCost cost) {
    return new ExecutedTransactionEvents(transaction, true, events, null, cost);
  }

  /**
   * Create a failed transaction events.
   *
   * @param transaction the transaction in this execution
   * @param exception the error
   * @return the failed transactions event
   */
  public static ExecutedTransactionEvents fail(
      ExecutableTransaction transaction, Throwable exception) {
    return fail(transaction, List.of(), exception);
  }

  /**
   * Create a failed transaction events.
   *
   * @param transaction the transaction in this execution
   * @param transactions resulting events - both local and distributed
   * @param exception the error
   * @return the failed transactions event
   */
  public static ExecutedTransactionEvents fail(
      ExecutableTransaction transaction, List<ExecutableEvent> transactions, Throwable exception) {
    logger.info("Error in execution: " + transaction.identifier(), exception);
    return new ExecutedTransactionEvents(transaction, false, transactions, exception, null);
  }

  static boolean sameShard(ExecutableEvent event) {
    return Objects.equals(event.getEvent().getDestinationShard(), event.getOriginShard());
  }

  void storeEvents(BlockchainStorage blockchainStorage) {
    blockchainStorage.storeEventTransactions(events);
  }

  /**
   * Check if even is successful.
   *
   * @return true if successful, false otherwise
   */
  public boolean isSuccess() {
    return success;
  }

  /**
   * Get events on current shard in execution.
   *
   * @return list of executable events
   */
  public List<ExecutableEvent> ownEvents() {
    return ownEvents;
  }

  /**
   * Get all events in execution.
   *
   * @return list of executable events
   */
  public List<ExecutableEvent> events() {
    return events;
  }

  /**
   * Get the error message and stack trace if an exception occured.
   *
   * @return an object containing an error message and a stack trace.
   */
  public FailureCause exception() {
    return exception;
  }

  /**
   * Get transaction of execution.
   *
   * @return executable transaction
   */
  public ExecutableTransaction transaction() {
    return transaction;
  }

  static Comparator<ExecutableEvent> getComparator() {
    return Comparator.comparingLong(e -> e.getEvent().getNonce());
  }

  /**
   * Get the cost of the executed event. The cost is <code>null</code> if the event is free or the
   * event failed.
   *
   * @return the cost of the event, <code>null</code> if the event failed or is free
   */
  public TransactionCost cost() {
    return transactionCost;
  }
}
