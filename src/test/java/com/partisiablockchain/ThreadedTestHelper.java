package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.util.Locale;
import java.util.Objects;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import org.assertj.core.api.Assertions;

/** Test. */
public final class ThreadedTestHelper {

  /**
   * Wait up to 10 seconds for the supplier to return true. Sleeps a bit between queries. Should be
   * used for tests that are dependent on something happening in another thread.
   *
   * <p>If the method returns successfully it means the supplier returned true in the last
   * invocation.
   */
  public static void waitForCondition(BooleanSupplier supplier) {
    long waitTime = 60_000L;
    Predicate<Boolean> actualWait = ((Predicate<Boolean>) Objects::isNull).or(bool -> !bool);
    long timeout = System.currentTimeMillis() + waitTime;
    Boolean value;
    do {
      ExceptionConverter.run(() -> Thread.sleep(50), "Unable to sleep");
      try {
        value = supplier.getAsBoolean();
      } catch (Exception e) {
        value = null;
      }
    } while (System.currentTimeMillis() < timeout && actualWait.test(value));

    String description =
        "Timeout" + " (" + String.format(Locale.US, "%,d", waitTime).replace(",", " ") + "ms)";
    Assertions.assertThat(actualWait.test(value)).describedAs(description).isFalse();
  }
}
