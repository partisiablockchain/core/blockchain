# Blockchain 

Includes several central component of the blockchain:
- Ledger: Core component receiving, evaluating and executing transactions on the shared ledger.
- Flooding network: Allows P2P network to flow data
- Exposed Storage
- Exposed plugins
  - Consensus
  - Account
  - Routing
