package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Types of plugins in pbc. A plugin is a smart contract that works identically across shards. */
public enum ChainPluginType {
  /**
   * Account plugin controls additional information about account and fees. There are different
   * types of state in relation to the account plugin:
   *
   * <ol>
   *   <li>Global state defines the behaviour and rules.
   *   <li>Shard state gathers the collected fees for services on chain
   *   <li>Account state holds any additional information, this could be BYOC.
   *   <li>Contract state specific parts is for e.g. fees
   * </ol>
   */
  ACCOUNT,
  /** Consensus validates block proposals and finalizes correct blocks. */
  CONSENSUS,
  /** Routing selects the right shard for any transaction. */
  ROUTING,
  /** Store binary objects across all shards. */
  SHARED_OBJECT_STORE
}
