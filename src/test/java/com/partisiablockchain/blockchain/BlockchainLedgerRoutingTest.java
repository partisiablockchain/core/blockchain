package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainLedgerTestHelper.createBlock;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.thread.ExecutionFactoryFake;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of routing plugin system. */
public final class BlockchainLedgerRoutingTest extends CloseableTest {

  private static final KeyPair SIGNER_KEYPAIR = new KeyPair(BigInteger.ONE);
  private static final BlockchainAddress SIGNER_ADDRESS =
      SIGNER_KEYPAIR.getPublic().createAddress();

  static final byte[] JAR_PLUGIN_ROUTING_OTHER_SHARD =
      BlockchainLedgerConsensusTest.buildTestingPluginJar(
          OtherShardAccountRouting.class, TestRoutingPlugin.class, AbstractRouting.class);

  public static final byte[] JAR_PLUGIN_ROUTING_SHARD_ONE =
      BlockchainLedgerConsensusTest.buildTestingPluginJar(
          ShardOneRouting.class, TestRoutingPlugin.class, AbstractRouting.class);

  static final byte[] JAR_PLUGIN_ROUTING_INNER =
      BlockchainLedgerConsensusTest.buildTestingPluginJar(
          InnerRouting.class, TestRoutingPlugin.class, AbstractRouting.class);

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private BlockchainLedger blockchain;
  private byte nextFinalization = 0;

  /** Setup blockchain for testing routing plugin. */
  @BeforeEach
  public void setUp() {
    this.blockchain =
        register(
            BlockchainLedger.createForTest(
                MemoryStorage.createRootDirectory(temporaryFolder.toFile()),
                chainState -> setupChain(chainState, JAR_PLUGIN_ROUTING_OTHER_SHARD),
                network,
                new DummyExecutableEventFinder(),
                ExecutionFactoryFake.create()));
  }

  @Test
  public void getters() {
    ImmutableChainState state = blockchain.getChainState();
    Assertions.assertThat(state.getRoutingPlugin()).isInstanceOf(RoutingPluginWrapper.class);
  }

  @Test
  public void routingJar() {
    ImmutableChainState immutableChainState = blockchain.getChainState();
    LargeByteArray pluginJar = immutableChainState.getPluginJar(ChainPluginType.ROUTING);
    Assertions.assertThat(pluginJar).isNotNull();
    Assertions.assertThat(pluginJar.getData()).isEqualTo(JAR_PLUGIN_ROUTING_OTHER_SHARD);

    MutableChainState mutableChainState = immutableChainState.asMutable();
    LargeByteArray mutablePlugin = mutableChainState.getPluginJar(ChainPluginType.ROUTING);
    Assertions.assertThat(mutablePlugin).isNotNull();
    Assertions.assertThat(mutablePlugin.getData()).isEqualTo(JAR_PLUGIN_ROUTING_OTHER_SHARD);
  }

  @Test
  public void routeToOtherShard() {
    updateRouting();
    blockchain.appendBlock(
        new FinalBlock(createBlock(blockchain.latest()), new byte[] {nextFinalization}));

    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
    appendBlock(transaction);
    Assertions.assertThat(blockchain.getPendingEvents()).hasSize(0);

    Hash event =
        blockchain.latest().getState().getExecutedState().getEventTransactions().iterator().next();
    ExecutableEvent eventTransaction = blockchain.getEventTransaction(event);
    Assertions.assertThat(eventTransaction.getEvent().getDestinationShard()).isEqualTo("Shard1");
  }

  private void updateRouting() {
    appendBlock(UpdateRoutingInTest.create(JarBuilder.buildJar(OtherShardAccountRouting.class)));
    nextFinalization++;
  }

  private void appendBlock(InteractWithContractTransaction transaction) {
    SignedTransaction signed = signedTransaction(transaction);
    Assertions.assertThat(blockchain.addPendingTransaction(signed)).isTrue();
    blockchain.appendBlock(
        new FinalBlock(
            createBlock(blockchain.latest(), signed.identifier()), new byte[] {nextFinalization}));
  }

  private SignedTransaction signedTransaction(InteractWithContractTransaction transaction) {
    return signedTransaction(blockchain, transaction);
  }

  static SignedTransaction signedTransaction(
      BlockchainLedger blockchain, InteractWithContractTransaction transaction) {
    Long nonce = blockchain.latest().getState().lookupNonce(SIGNER_ADDRESS);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                nonce == null ? 0 : nonce, System.currentTimeMillis() + 100_000, 0),
            transaction)
        .sign(SIGNER_KEYPAIR, blockchain.getChainId());
  }

  static void setupChain(MutableChainState mutableChainState, byte[] routingPluginJar) {
    mutableChainState.createAccount(SIGNER_ADDRESS);
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        BlockchainLedgerConsensusTest.JAR_PLUGIN_CONSENSUS_SHARD,
        new byte[] {1});
    mutableChainState.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ROUTING,
        routingPluginJar,
        new byte[] {0});
    Assertions.assertThat(mutableChainState.getRoutingPlugin())
        .isInstanceOf(RoutingPluginWrapper.class);
    CoreContractStateTest.createContract(
        mutableChainState,
        JarBuilder.buildJar(UpdateRoutingInTest.class),
        TestObjects.CONTRACT_SYS,
        TestContracts.CONTRACT_BINDER_SYSTEM);
    mutableChainState.addActiveShard(
        FunctionUtility.noOpBiConsumer(), "Shard0", TestObjects.SHARD_ONE);
  }

  /** Testing routing plugin. */
  @Immutable
  public static final class InnerRouting extends AbstractRouting {}

  /** Testing routing plugin. */
  @Immutable
  public abstract static class AbstractRouting extends BlockchainRoutingPlugin {

    @Override
    public String route(FixedList<String> shards, BlockchainAddress target) {
      return null;
    }
  }

  /** Testing routing plugin. */
  @Immutable
  public static final class ShardOneRouting extends TestRoutingPlugin {

    @Override
    public String route(FixedList<String> shards, BlockchainAddress target) {
      return shards.isEmpty() ? null : TestObjects.SHARD_ONE;
    }
  }

  /** Testing routing plugin. */
  @Immutable
  public static final class OtherShardAccountRouting extends TestRoutingPlugin {

    @Override
    public String route(FixedList<String> shards, BlockchainAddress target) {
      if (target.getType() != BlockchainAddress.Type.ACCOUNT && !shards.isEmpty()) {
        return shards.get(0);
      } else {
        return null;
      }
    }
  }

  /** Testing contract. */
  public static final class UpdateRoutingInTest extends SysContract<StateVoid> {

    static InteractWithContractTransaction create(byte[] jar) {
      return InteractWithContractTransaction.create(
          TestObjects.CONTRACT_SYS,
          SafeDataOutputStream.serialize(stream -> stream.writeDynamicBytes(jar)));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      context.getInvocationCreator().updateRoutingPlugin(jar, new byte[] {1});
      return null;
    }
  }

  /** Testing routing plugin. */
  @Immutable
  public abstract static class TestRoutingPlugin extends BlockchainRoutingPlugin {

    @Override
    public String route(FixedList<String> shards, BlockchainAddress target) {
      return null;
    }
  }
}
