package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedgerRoutingTest;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ContractState;
import com.partisiablockchain.blockchain.ExecutedTransactionEvents;
import com.partisiablockchain.blockchain.Features;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.TestContracts;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent.SetFeatureEvent;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test of {@link ExecutionContext}. */
public final class ExecutionContextTest {

  private static final String chainId = UUID.randomUUID().toString();

  private static final KeyPair signer = new KeyPair(BigInteger.valueOf(27L));
  private static final BlockchainAddress account = signer.getPublic().createAddress();
  private static final BlockchainAddress contract = TestObjects.CONTRACT_PUB1;
  private static final BlockchainAddress contractSys = TestObjects.CONTRACT_SYS;
  private static final BlockchainAddress contractGov = TestObjects.CONTRACT_GOV1;

  private static final byte[] CONTRACT_SYS_ALLOCATED_COST =
      JarBuilder.buildJar(AllocatedCostSysContract.class);
  private static final byte[] CONTRACT_REGISTER_CALLBACK_CONTRACT =
      JarBuilder.buildJar(RegisterCallbackContract.class, TestObjects.class);
  private static final byte[] CONTRACT_ALLOCATED_COST =
      JarBuilder.buildJar(AllocatedCostContract.class, TestObjects.class);
  private static final byte[] CONTRACT_NO_OP = JarBuilder.buildJar(NoOpContract.class);
  private static final byte[] CONTRACT_PAY_BYOC_SERVICE_FEES =
      JarBuilder.buildJar(PayByocServiceFees.class, TestObjects.class);
  private static final byte[] CONTRACT_PAY_SERVICE_FEES =
      JarBuilder.buildJar(PayServiceFees.class, TestObjects.class);
  private static final byte[] CONTRACT_PAY_INFRASTRUCTURE_FEES =
      JarBuilder.buildJar(PayInfrastructureFees.class, TestObjects.class);

  @Test
  public void noNetworkFeeOnFreeTransaction() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 25 + 10, 0),
                InteractWithContractTransaction.create(contractGov, new byte[0]))
            .sign(signer, chainId);

    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, 10_000L);
            });
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, transaction).events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(1);

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(10_000L);
  }

  @Test
  public void networkFeesOnNormalFails() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 25 + 10, 0),
                InteractWithContractTransaction.create(contract, new byte[0]))
            .sign(signer, chainId);

    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, 10_000L);
            });
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, transaction).events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(0);

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(10_000L);
  }

  @Test
  public void signedTransactionUnableToSpawnEventDueToGas_withRegisterUsageFeature() {
    MutableChainState state =
        sendSignedTransactionUnableToSpawnEvent(
            StateHelper.initialWithAdditions(
                builder -> {
                  builder.createAccount(account);
                  FeePluginHelper.mintGas(builder, account, 10_000L);
                  builder.setFeature(
                      Features.REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION, "enabled");
                }));

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(9_880L);
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(120L);
  }

  @Test
  public void signedTransactionUnableToSpawnEventDueToGas() {
    MutableChainState state =
        sendSignedTransactionUnableToSpawnEvent(
            StateHelper.initialWithAdditions(
                builder -> {
                  builder.createAccount(account);
                  FeePluginHelper.mintGas(builder, account, 10_000L);
                }));

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(9_880L);
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(114L);
  }

  private MutableChainState sendSignedTransactionUnableToSpawnEvent(MutableChainState state) {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 25 + 10, 120),
                InteractWithContractTransaction.create(contract, new byte[0]))
            .sign(signer, chainId);

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    ExecutedTransactionEvents executionResult = context.execute(state, transaction);
    Assertions.assertThat(executionResult.isSuccess()).isFalse();
    List<ExecutableEvent> execute = executionResult.events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(0);
    return state;
  }

  @Test
  public void networkFeeOnSigned() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 25 + 10, 400),
                InteractWithContractTransaction.create(contract, new byte[100]))
            .sign(signer, chainId);

    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, 10_000L);
            });
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, transaction).events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(1);
    Assertions.assertThat(execute.get(0).getEvent()).isInstanceOf(EventTransaction.class);
    Assertions.assertThat(
            ((EventTransaction.InnerTransaction) execute.get(0).getEvent().getInner()).getCost())
        .isEqualTo(60L);

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(10_000L - 400);
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(400 - 60);
  }

  @Test
  public void registerBlockchainUsage() {
    long allocatedCost = 100;
    long networkFee = 26;

    InteractWithContractTransaction transaction =
        createAllocatedCostPubContractTransaction(allocatedCost, false, false);

    ExecutableEvent event = createEvent(allocatedCost, transaction);

    MutableChainState state = createStateWithPublicContractAndFeePlugin(CONTRACT_ALLOCATED_COST);

    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(networkFee);
  }

  @Test
  public void registerBlockchainUsage_sysContract() {
    long allocatedCost = 100;
    long networkFee = 26;

    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(allocatedCost, false, contractSys);

    ExecutableEvent event = createEvent(allocatedCost, transaction);

    MutableChainState state = createStateWithSysContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);

    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(networkFee);
  }

  @Test
  public void tryingToExecuteTransactionOnIcedContract() {

    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(100, false, contractGov);
    ExecutableEvent event = createEvent(100, transaction);
    MutableChainState state = createStateWithGovContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    ExecutedTransactionEvents executed = context.execute(state, event, true);
    Assertions.assertThat(executed.isSuccess()).isFalse();
    Assertions.assertThat(executed.exception().getErrorMessage()).isEqualTo("Contract is iced");

    Assertions.assertThat(FeePluginHelper.balance(state, contractGov)).isEqualTo(100L);
  }

  @Test
  public void registerBlockchainUsageInsufficient_sysContract() {
    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(100, false, contractSys);
    ExecutableEvent event = createEvent(0, transaction);

    MutableChainState state = createStateWithGovContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isFalse();
  }

  @Test
  public void registerBlockchainUsage_govContract() {
    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(100, false, contractGov);
    ExecutableEvent event = createEvent(100, transaction);

    MutableChainState state = createStateWithGovContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(0);
  }

  @Test
  public void registerBlockchainUsageInsufficient_govContract() {
    int allocatedCost = 100;
    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(allocatedCost, false, contractGov);
    ExecutableEvent event = createEvent(allocatedCost - 1, transaction);

    MutableChainState state = createStateWithGovContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isFalse();
  }

  @Test
  public void registerAllocatedCostForEvents() {
    long allocatedCost = 100;
    InteractWithContractTransaction transaction =
        createAllocatedCostPubContractTransaction(allocatedCost, false, false);
    InteractWithContractTransaction insufficientTransaction =
        createAllocatedCostPubContractTransaction(allocatedCost + 1, false, false);
    InteractWithContractTransaction transactionMultiple =
        createAllocatedCostPubContractTransaction(allocatedCost / 2, false, true);
    InteractWithContractTransaction insufficientTransactionMultiple =
        createAllocatedCostPubContractTransaction(allocatedCost / 2 + 1, false, true);

    ExecutableEvent event = createEvent(allocatedCost, transaction);
    ExecutableEvent insufficientEvent = createEvent(allocatedCost, insufficientTransaction);
    ExecutableEvent eventMultiple = createEvent(allocatedCost, transactionMultiple);
    ExecutableEvent insufficientEventMultiple =
        createEvent(allocatedCost, insufficientTransactionMultiple);

    MutableChainState state = createStateWithPublicContractAndFeePlugin(CONTRACT_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(context.execute(state, insufficientEvent).isSuccess()).isFalse();
    Assertions.assertThat(context.execute(state, eventMultiple).isSuccess()).isTrue();
    Assertions.assertThat(context.execute(state, insufficientEventMultiple).isSuccess()).isFalse();
  }

  private MutableChainState createStateWithPublicContractAndFeePlugin(byte[] jar) {
    return createStateWithContractAndFeePlugin(jar, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
  }

  private MutableChainState createStateWithSysContractAndFeePlugin(byte[] jar) {
    return createStateWithContractAndFeePlugin(
        jar, contractSys, TestContracts.CONTRACT_BINDER_SYSTEM);
  }

  private MutableChainState createStateWithGovContractAndFeePlugin(byte[] jar) {
    return createStateWithContractAndFeePlugin(
        jar, contractGov, TestContracts.CONTRACT_BINDER_SYSTEM);
  }

  private MutableChainState createStateWithContractAndFeePlugin(
      byte[] jar, BlockchainAddress contractAddress, byte[] binderJar) {
    return StateHelper.initialWithAdditions(
        builder -> {
          CoreContractStateTest.createContract(builder, jar, contractAddress, binderJar);
          builder.setPlugin(
              FunctionUtility.noOpBiConsumer(),
              null,
              ChainPluginType.ACCOUNT,
              FeePluginHelper.createJar(),
              new byte[0]);
        });
  }

  @Test
  public void registerAllocatedCostForCallbacks() {
    long allocatedCallbackCost = 100L;
    long eventCost = 27L; // network fees
    InteractWithContractTransaction transaction =
        createAllocatedCostPubContractTransaction(eventCost, true, false);

    long expectedFees = allocatedCallbackCost + eventCost;

    ExecutableEvent event = createEvent(expectedFees, transaction);

    MutableChainState state = createStateWithPublicContractAndFeePlugin(CONTRACT_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
  }

  @Test
  public void registerAllocatedCostForCallback_sysContract() {
    long allocatedCallbackCost = 100L;
    long eventCost = 27L; // network fees
    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(eventCost, true, contractSys);

    long expectedFees = allocatedCallbackCost + eventCost;

    ExecutableEvent event = createEvent(expectedFees, transaction);

    MutableChainState state = createStateWithSysContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
  }

  @Test
  public void registerAllocatedCostForCallback_govContract() {
    long allocatedCost = 100L;
    long allocatedCallbackCost = 100L;
    InteractWithContractTransaction transaction =
        createAllocatedCostSysContractTransaction(allocatedCost, true, contractGov);

    ExecutableEvent event = createEvent(allocatedCost + allocatedCallbackCost, transaction);

    MutableChainState state = createStateWithGovContractAndFeePlugin(CONTRACT_SYS_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(0);
  }

  @Test
  public void registeringCallbackWithoutCallbackSpawningEventsThrowsException() {

    InteractWithContractTransaction interaction =
        InteractWithContractTransaction.create(
            contract, SafeDataOutputStream.serialize(EventCreator.EMPTY_RPC));
    long cost = 2L * CONTRACT_REGISTER_CALLBACK_CONTRACT.length + 1000L;
    ExecutableEvent event = createEvent(cost, interaction);

    MutableChainState state =
        createStateWithPublicContractAndFeePlugin(CONTRACT_REGISTER_CALLBACK_CONTRACT);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    ExecutedTransactionEvents execute = context.execute(state, event);
    Assertions.assertThat(execute.isSuccess()).isFalse();
    Assertions.assertThat(execute.exception().getStackTrace())
        .contains("java.lang.IllegalStateException: Unable to create callbacks");
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(cost);
  }

  @Test
  public void allocatedCostLessThanNetworkFeeShouldFail() {
    long networkFee = 26;
    long allocatedCost = networkFee - 1;
    InteractWithContractTransaction transaction =
        createAllocatedCostPubContractTransaction(allocatedCost, false, false);

    long expectedFees = 9999;

    ExecutableEvent event = createEvent(expectedFees, transaction);

    MutableChainState state = createStateWithPublicContractAndFeePlugin(CONTRACT_ALLOCATED_COST);
    ExecutionContext context = initExecutionContext(null, 0, 0);
    Assertions.assertThat(context.execute(state, event).isSuccess()).isFalse();
  }

  private InteractWithContractTransaction createAllocatedCostPubContractTransaction(
      long allocatedCost, boolean callback, boolean multiple) {
    return InteractWithContractTransaction.create(
        contract,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeBoolean(callback);
              stream.writeBoolean(multiple);
              stream.writeLong(allocatedCost);
            }));
  }

  private InteractWithContractTransaction createAllocatedCostSysContractTransaction(
      long allocatedCost, boolean callback, BlockchainAddress contractAddress) {
    return InteractWithContractTransaction.create(
        contractAddress,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeBoolean(callback);
              stream.writeLong(allocatedCost);
            }));
  }

  private ExecutableEvent createEvent(long cost, Transaction transaction) {
    return new ExecutableEvent(
        "originShard",
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            account,
            cost,
            transaction,
            new ShardRoute("destinationShard", 27),
            1,
            1));
  }

  @Test
  public void surplusFromCreateContractTransactionIsForwardedToContract() {
    byte[] jar = CONTRACT_NO_OP;
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(contract, binderJar, jar, new byte[0], rpc);
    long cost = 50_000L;
    ExecutableEvent event = createEvent(cost, transaction);

    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, 50_000L);
            });

    Assertions.assertThat(FeePluginHelper.balance(state, contract)).isEqualTo(0L);
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.balance(state, contract)).isEqualTo(cost);
  }

  @Test
  public void surplusGasFromInteractionTransactionIsForwardedToContract() {
    long cost = 10_000L;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);
    ExecutableEvent event = createEvent(cost, transaction);

    MutableChainState state = initialStateWithContract(CONTRACT_NO_OP);

    Assertions.assertThat(FeePluginHelper.balance(state, contract)).isEqualTo(0L);
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThat(context.execute(state, event).isSuccess()).isTrue();
    Assertions.assertThat(FeePluginHelper.balance(state, contract)).isEqualTo(cost);
  }

  @Test
  public void allocatedCostForFailingTransactionIsRegisteredAsChainUsage() {
    // Setting jar as an empty array will cause transaction execution to fail
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    byte[] contractJar = new byte[0];
    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(contract, binderJar, contractJar, new byte[0], rpc);
    long cost = 50_000L;
    ExecutableEvent event = createEvent(cost, transaction);

    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, cost);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThat(context.execute(state, event).isSuccess()).isFalse();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(cost);
  }

  private MutableChainState initialStateWithContract(byte[] jar) {
    return StateHelper.initialWithAdditions(
        builder -> {
          CoreContractStateTest.createContract(
              builder, jar, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
          builder.createAccount(account);
          FeePluginHelper.mintGas(builder, account, 10_000L);
        });
  }

  @Test
  public void allocatedCostForFailingCallbackIsRegisteredAsChainUsage() {
    long allocatedCost = 5_000L;
    ExecutableEvent event = callbackEvent(allocatedCost);

    // Callback is not added to state, causing the execution to throw and exception
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder, CONTRACT_NO_OP, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, 10_000L);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThatThrownBy(() -> context.execute(state, event));
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state))
        .isEqualTo(allocatedCost);
  }

  private ExecutableEvent callbackEvent(long allocatedCost) {
    return callbackEvent(allocatedCost, contract);
  }

  private ExecutableEvent callbackEvent(long allocatedCost, BlockchainAddress contract) {
    CallbackToContract callbackToContract =
        new CallbackToContract(
            contract,
            new CallbackCreator.Callback(
                TestObjects.EMPTY_HASH, account, allocatedCost, new LargeByteArray(new byte[0])));
    return new ExecutableEvent(
        "originShard",
        EventTransaction.create(
            TestObjects.EMPTY_HASH, callbackToContract, new ShardRoute("destShard", 27), 1, 1, 0));
  }

  @Test
  public void systemEventWrongShard() {
    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute("destShard", 27),
                1,
                1,
                0,
                null,
                new SetFeatureEvent("testFeature", null)));
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(), FunctionUtility.noOpConsumer());

    ExecutionContext context = initExecutionContext("destShard", 1, 0);
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(execute).isNotNull().isEmpty();
  }

  @Test
  public void systemEventSuccess() {
    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute(null, 27),
                1,
                1,
                0,
                null,
                new SetFeatureEvent("testFeature", null)));
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(), FunctionUtility.noOpConsumer());

    ExecutionContext context = initExecutionContext(null, 1, 0);
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(state.getFeature("testFeature")).isNull();
  }

  @Test
  public void getBlockTime() {
    ExecutionContext context = initExecutionContext(null, 1, 12340);
    Assertions.assertThat(context.getBlockTime()).isEqualTo(10L);
    Assertions.assertThat(context.getBlockProductionTime()).isEqualTo(12340L);
  }

  @Test
  public void getChainId() {
    ExecutionContext context = initExecutionContext(null, 1, 12340);
    Assertions.assertThat(context.getChainId()).isEqualTo(chainId);
  }

  @Test
  public void unableToPayStorageFees() {
    long cost = 7000L;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);

    ExecutableEvent event = createEvent(cost, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder, CONTRACT_NO_OP, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
              FeePluginHelper.mintGas(builder, contract, cost);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(execute).isEmpty();
  }

  @Test
  public void createSystemEvent() {
    BlockchainAddress contract = TestObjects.CONTRACT_SYS;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);

    byte[] jar = JarBuilder.buildJar(SetFeatureContract.class);
    ExecutableEvent event = createEvent(50, transaction);
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(),
            builder ->
                CoreContractStateTest.createContract(
                    builder, jar, contract, TestContracts.CONTRACT_BINDER_SYSTEM));

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(1);
    Assertions.assertThat(execute.get(0).getEvent().getNonce()).isEqualTo(1L);
    Assertions.assertThat(execute.get(0).getEvent().getDestinationShard()).isNull();
  }

  @Test
  public void createSystemEventWithNetworkFee() {
    final int setFeatureEventNetworkFee = 0;

    BlockchainAddress contract = TestObjects.CONTRACT_SYS;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);

    byte[] jar = JarBuilder.buildJar(SetFeatureContract.class);
    ExecutableEvent event = createEvent(10050, transaction);
    MutableChainState state = createStateWithSysContractAndFeePlugin(jar);

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state))
        .isEqualTo(setFeatureEventNetworkFee);
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(1);
    Assertions.assertThat(execute.get(0).getEvent().getNonce()).isEqualTo(1L);
    Assertions.assertThat(execute.get(0).getEvent().getDestinationShard()).isNull();
  }

  @Test
  public void createSystemEventWithoutNetworkFee() {
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);

    byte[] jar = JarBuilder.buildJar(SetFeatureContract.class);
    ExecutableEvent event = createEvent(10050, transaction);
    MutableChainState state = createStateWithGovContractAndFeePlugin(jar);

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    List<ExecutableEvent> execute = context.execute(state, event).events();
    Assertions.assertThat(FeePluginHelper.accumulatedRegisteredUsage(state)).isEqualTo(0);
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThat(execute).hasSize(1);
    Assertions.assertThat(execute.get(0).getEvent().getNonce()).isEqualTo(1L);
    Assertions.assertThat(execute.get(0).getEvent().getDestinationShard()).isNull();
  }

  @Test
  public void executeInSync_determineRerouteShard_withTarget() {
    EventTransaction.InnerEvent innerEvent = Mockito.mock(SyncEvent.class);
    Mockito.when(innerEvent.target()).thenReturn(TestObjects.ACCOUNT_FOUR);
    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            new EventTransaction(
                TestObjects.EMPTY_HASH, new ShardRoute(null, 27), 1, 1, 0, null, innerEvent));
    MutableChainState state =
        StateHelper.mutableFromPopulate(
            ObjectCreator.createMemoryStateStorage(),
            builder -> {
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ROUTING,
                  BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_SHARD_ONE,
                  new byte[0]);
              builder.addActiveShard(FunctionUtility.noOpBiConsumer(), null, TestObjects.SHARD_ONE);
            });
    ExecutionContext context = initExecutionContext(null, 1, 0);
    ExecutedTransactionEvents events = context.executeInSync(state, event);
    Assertions.assertThat(events.events().get(0).getEvent().getDestinationShard())
        .isEqualTo(TestObjects.SHARD_ONE);
  }

  @Test
  public void registerByocRelatedFees() {
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contract, new byte[0]);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder ->
                CoreContractStateTest.createContract(
                    builder,
                    CONTRACT_PAY_BYOC_SERVICE_FEES,
                    contract,
                    TestContracts.CONTRACT_BINDER_SYSTEM));
    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    ExecutableEvent event = createEvent(0, transaction);
    context.execute(state, event);
    FixedList<BlockchainAddress> nodes =
        FixedList.create(
            List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO, TestObjects.ACCOUNT_THREE));
    PendingByocFee pending = PendingByocFee.create(nodes, Unsigned256.create(10L), "symbol");

    Assertions.assertThat(FeePluginHelper.registeredByocFees(state))
        .usingRecursiveComparison()
        .isEqualTo(List.of(pending));
  }

  @Test
  public void contractPaysForServiceFees() {
    long serviceFeesToPay = 200L;
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(
            contract, SafeDataOutputStream.serialize(stream -> stream.writeLong(serviceFeesToPay)));

    ExecutableEvent event = createEvent(0, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder,
                  CONTRACT_PAY_SERVICE_FEES,
                  contract,
                  TestContracts.CONTRACT_BINDER_SYSTEM);
              FeePluginHelper.mintGas(builder, contract, serviceFeesToPay);
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              builder.createAccount(TestObjects.ACCOUNT_TWO);
              builder.createAccount(TestObjects.ACCOUNT_THREE);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    context.execute(state, event);
    Assertions.assertThat(FeePluginHelper.balance(state, TestObjects.CONTRACT_SYS)).isEqualTo(0L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_ONE))
        .isEqualTo(100L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_TWO))
        .isEqualTo(50L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_THREE))
        .isEqualTo(50L);
  }

  @Test
  public void shouldNotCatchVirtualMachineErrorOnInvoke() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(contractGov, new byte[0]);

    byte[] jar = JarBuilder.buildJar(VirtualMachineErrorContract.class);
    ExecutableEvent event = createEvent(0, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder ->
                CoreContractStateTest.createContract(
                    builder, jar, contractGov, TestContracts.CONTRACT_BINDER_SYSTEM));

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThatThrownBy(() -> context.execute(state, event))
        .isInstanceOf(VirtualMachineError.class);
  }

  @Test
  public void shouldNotCatchVirtualMachineErrorOnCallback() {
    final ContractState.CallbackInfo callbackInfo =
        ContractState.CallbackInfo.create(
            List.of(TestObjects.EMPTY_HASH), 0L, new LargeByteArray(new byte[0]));
    byte[] jar = JarBuilder.buildJar(VirtualMachineErrorContract.class);
    CallbackToContract callbackToContract =
        new CallbackToContract(
            contractGov,
            new CallbackCreator.Callback(
                callbackInfo.getCallbackIdentifier(),
                account,
                0L,
                new LargeByteArray(new byte[0])));
    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            EventTransaction.create(
                TestObjects.EMPTY_HASH,
                callbackToContract,
                new ShardRoute("destShard", 27),
                1,
                1,
                0));
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder, jar, contractGov, TestContracts.CONTRACT_BINDER_SYSTEM);
              builder.addCallbacks(
                  account, contractGov, null, TestObjects.EMPTY_HASH, callbackInfo);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    Assertions.assertThatThrownBy(() -> context.execute(state, event))
        .isInstanceOf(VirtualMachineError.class);
  }

  @Test
  public void contractUnableToPayForServiceFeesIsRemoved() {
    long serviceFeesToPay = 200L;
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(
            contract, SafeDataOutputStream.serialize(stream -> stream.writeLong(serviceFeesToPay)));

    ExecutableEvent event = createEvent(0, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder,
                  CONTRACT_PAY_SERVICE_FEES,
                  contract,
                  TestContracts.CONTRACT_BINDER_SYSTEM);
              FeePluginHelper.mintGas(builder, contract, serviceFeesToPay - 1L);
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              builder.createAccount(TestObjects.ACCOUNT_TWO);
              builder.createAccount(TestObjects.ACCOUNT_THREE);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    context.execute(state, event);
    Assertions.assertThat(state.getContracts()).doesNotContain(contract);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_ONE))
        .isEqualTo(99L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_TWO))
        .isEqualTo(50L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_THREE))
        .isEqualTo(50L);
  }

  @Test
  public void contractRetainsSurplusGasAfterPayingServiceFees() {
    long serviceFeesToPay = 200L;
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(
            contract, SafeDataOutputStream.serialize(stream -> stream.writeLong(serviceFeesToPay)));

    ExecutableEvent event = createEvent(0, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder,
                  CONTRACT_PAY_SERVICE_FEES,
                  contract,
                  TestContracts.CONTRACT_BINDER_SYSTEM);
              FeePluginHelper.mintGas(builder, contract, serviceFeesToPay + 1L);
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              builder.createAccount(TestObjects.ACCOUNT_TWO);
              builder.createAccount(TestObjects.ACCOUNT_THREE);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    context.execute(state, event);
    Assertions.assertThat(FeePluginHelper.balance(state, contract)).isEqualTo(1L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_ONE))
        .isEqualTo(100L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_TWO))
        .isEqualTo(50L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_THREE))
        .isEqualTo(50L);
  }

  private ExecutionContext initExecutionContext(
      String subChainId, int blockTime, long productionTime) {
    ExecutionContext init =
        ExecutionContext.init(chainId, subChainId, 9L, blockTime, 10, productionTime);
    Assertions.assertThat(init.getChainId()).isEqualTo(chainId);
    return init;
  }

  @Test
  public void payInfrastructureFees() {
    long infrastructureFees = 200L;
    BlockchainAddress contract = TestObjects.CONTRACT_GOV1;
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(
            contract,
            SafeDataOutputStream.serialize(stream -> stream.writeLong(infrastructureFees)));

    ExecutableEvent event = createEvent(0, transaction);
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder,
                  CONTRACT_PAY_INFRASTRUCTURE_FEES,
                  contract,
                  TestContracts.CONTRACT_BINDER_SYSTEM);
              builder.createAccount(TestObjects.ACCOUNT_ONE);
              builder.createAccount(TestObjects.ACCOUNT_TWO);
              builder.createAccount(TestObjects.ACCOUNT_THREE);
            });

    ExecutionContext context = initExecutionContext(null, 1, System.currentTimeMillis());
    context.execute(state, event);

    Assertions.assertThat(FeePluginHelper.infrastructureFeesSum(state))
        .isEqualTo(infrastructureFees);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_ONE))
        .isEqualTo(100L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_TWO))
        .isEqualTo(50L);
    Assertions.assertThat(FeePluginHelper.serviceFees(state, TestObjects.ACCOUNT_THREE))
        .isEqualTo(50L);
  }

  /** Test. */
  public static final class AllocatedCostContract extends PubContract<StateLong> {

    @Override
    public StateLong onInvoke(
        PubContractContext context, StateLong state, SafeDataInputStream rpc) {
      boolean callback = rpc.readBoolean();
      boolean multiple = rpc.readBoolean();
      long allocatedCost = rpc.readLong();
      if (callback) {
        EventManager eventManager = context.getRemoteCallsCreator();
        dummyInvoke(eventManager, allocatedCost);
        eventManager.registerCallbackWithCostFromRemaining(EventCreator.EMPTY_RPC);
      } else if (multiple) {
        EventCreator manager = context.getInvocationCreator();
        dummyInvoke(manager, allocatedCost);
        dummyInvoke(manager, allocatedCost);
      } else {
        dummyInvoke(context.getInvocationCreator(), allocatedCost);
      }
      return new StateLong(context.getBlockProductionTime());
    }

    private void dummyInvoke(EventCreator manager, long allocatedCost) {
      manager
          .invoke(TestObjects.CONTRACT_PUB2)
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateCost(allocatedCost)
          .send();
    }
  }

  /** Test. */
  public static final class AllocatedCostSysContract extends SysContract<StateLong> {

    @Override
    public StateLong onInvoke(
        SysContractContext sysContractContext, StateLong state, SafeDataInputStream rpc) {
      boolean callback = rpc.readBoolean();
      long allocatedCost = rpc.readLong();
      EventCreator eventManager;
      if (callback) {
        SystemEventManager remoteCallsCreator = sysContractContext.getRemoteCallsCreator();
        remoteCallsCreator.registerCallback(EventCreator.EMPTY_RPC, 0);
        eventManager = remoteCallsCreator;
      } else {
        eventManager = sysContractContext.getInvocationCreator();
      }
      dummyInvoke(sysContractContext.getContractAddress(), eventManager, allocatedCost);
      return new StateLong(sysContractContext.getBlockProductionTime());
    }

    private void dummyInvoke(
        BlockchainAddress contractAddress, EventCreator manager, long allocatedCost) {
      manager
          .invoke(contractAddress)
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateCost(allocatedCost)
          .send();
    }
  }

  /** Test. */
  public static final class NoOpContract extends PubContract<StateLong> {

    @Override
    public StateLong onInvoke(
        PubContractContext context, StateLong state, SafeDataInputStream rpc) {
      return new StateLong(10L);
    }
  }

  /** Test. */
  public static final class SetFeatureContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      context.getInvocationCreator().setFeature("Feature", "value");
      return null;
    }
  }

  /** Test. */
  public static final class RegisterCallbackContract extends PubContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        PubContractContext context, StateVoid state, SafeDataInputStream rpc) {
      context.getRemoteCallsCreator().registerCallback(EventCreator.EMPTY_RPC, 100L);

      EventCreator callback = context.getInvocationCreator();
      callback
          .invoke(TestObjects.CONTRACT_PUB2)
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateCost(500L)
          .send();
      return state;
    }
  }

  /** Test. */
  public static final class PayServiceFees extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {

      long gas = rpc.readLong();
      FixedList<BlockchainAddress> nodes =
          FixedList.create(
              List.of(
                  TestObjects.ACCOUNT_ONE,
                  TestObjects.ACCOUNT_TWO,
                  TestObjects.ACCOUNT_THREE,
                  TestObjects.ACCOUNT_ONE));
      FixedList<Integer> weights = FixedList.create(List.of(1, 1, 1, 1));
      context.payServiceFees(gas, nodes, weights);

      return state;
    }
  }

  /** Test. */
  public static final class PayInfrastructureFees extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {

      long gas = rpc.readLong();
      FixedList<BlockchainAddress> nodes =
          FixedList.create(
              List.of(
                  TestObjects.ACCOUNT_ONE,
                  TestObjects.ACCOUNT_TWO,
                  TestObjects.ACCOUNT_THREE,
                  TestObjects.ACCOUNT_ONE));
      FixedList<Integer> weights = FixedList.create(List.of(1, 1, 1, 1));
      context.payInfrastructureFees(gas, nodes, weights);

      return state;
    }
  }

  /** Test. */
  public static final class PayByocServiceFees extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      FixedList<BlockchainAddress> nodes =
          FixedList.create(
              List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_TWO, TestObjects.ACCOUNT_THREE));
      context.registerDeductedByocFees(Unsigned256.create(10), "symbol", nodes);
      return state;
    }
  }

  /** A contract that triggers a {@link VirtualMachineError} on invoke and callback. */
  public static final class VirtualMachineErrorContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      throw new OutOfMemoryError("Expected");
    }

    @Override
    public StateVoid onCallback(
        SysContractContext context,
        StateVoid state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      throw new OutOfMemoryError("Expected");
    }
  }
}
