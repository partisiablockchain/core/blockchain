package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.BlockChainNetwork;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.partisiablockchain.flooding.Packet;
import java.util.ArrayList;
import java.util.List;

/** Test. */
public final class BlockChainTestNetwork implements BlockChainNetwork {

  final List<Packet<?>> queued = new ArrayList<>();
  boolean closed;

  @Override
  public <T extends NetworkFloodable> void sendToAll(Packet<T> packet) {
    queued.add(packet);
  }

  @Override
  public void sendToAny(Packet<?> packet) {
    queued.add(packet);
  }

  @Override
  public synchronized void close() {
    closed = true;
    notifyAll();
  }
}
