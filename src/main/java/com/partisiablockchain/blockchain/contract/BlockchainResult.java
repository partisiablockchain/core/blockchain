package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.List;

/**
 * A result of a call to a contract.
 *
 * @param <T> state type
 */
public final class BlockchainResult<T extends StateSerializable, EventT extends BinderEvent> {

  private final T state;
  private final List<EventT> invocations;
  private final CallResult result;

  private BlockchainResult(T state, List<EventT> invocations, CallResult result) {
    this.state = state;
    this.invocations = invocations;
    this.result = result;
  }

  /**
   * Converts from binder result to blockchain result.
   *
   * @param callstack the binder result
   * @param <T> state type
   * @param <E> the type of event
   * @return the created object
   */
  public static <T extends StateSerializable, E extends BinderEvent> BlockchainResult<T, E> create(
      BinderResult<T, E> callstack) {
    return new BlockchainResult<>(
        callstack.getState(), callstack.getInvocations(), callstack.getCallResult());
  }

  T getState() {
    return state;
  }

  /**
   * Gets the event result for this blockchain result, that is the events without state.
   *
   * @return the events
   */
  public EventResult<EventT> events() {
    return new EventResult<>(invocations, result);
  }
}
