package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutionContext;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import java.time.Duration;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class TransactionProcessor {

  private static final long ONE_SECOND = Duration.ofSeconds(1).toMillis();
  private static final Logger LOGGER = LoggerFactory.getLogger(TransactionProcessor.class);
  private final Logger logger;
  private final ExecutionContext context;
  private final List<ExecutedTransactionEvents> results;
  private final Queue<ExecutableEvent> localEvents;
  private final Block block;
  private final SystemTime systemTime;
  private final MutableChainState state;

  /**
   * Creates a new transaction processor used to process each transaction in {@link
   * BlockchainLedger}.
   *
   * @param block the block we are processing and used to determine icing.
   * @param nextState the state we are mutating as part of the execution
   * @param chainId current chain id
   * @param subChainId the shard id
   */
  public TransactionProcessor(
      Block block, MutableChainState nextState, String chainId, String subChainId) {
    this(
        ExecutionContext.init(chainId, subChainId, block, nextState.getGovernanceVersion()),
        block,
        nextState,
        System::currentTimeMillis,
        LOGGER);
  }

  TransactionProcessor(
      ExecutionContext context,
      Block block,
      MutableChainState state,
      SystemTime systemTime,
      Logger logger) {
    this.state = state;
    this.block = block;
    this.systemTime = systemTime;
    this.results = new ArrayList<>();
    this.localEvents = new ArrayDeque<>();
    this.context = context;
    this.logger = logger;
  }

  void processTransactionInSync(ExecutableEvent executableEvent) {
    collectEvents(context.executeInSync(state, executableEvent));
  }

  void processTransaction(ExecutableEvent executableEvent) {
    checkDuration(
        () -> {
          EventTransaction.InnerEvent innerEvent = executableEvent.getEvent().getInner();
          ExecutedTransactionEvents events;
          if (innerEvent instanceof EventTransaction.ContractInnerEvent contractEvent) {
            boolean iced = state.updateActiveContract(block, contractEvent.target());
            events = context.execute(state, executableEvent, iced);
          } else {
            events = context.execute(state, executableEvent);
          }
          collectEvents(events);
        },
        executableEvent.identifier());
  }

  void processTransaction(SignedTransaction transaction) {
    checkDuration(
        () -> collectEvents(context.execute(state, transaction)), transaction.identifier());
  }

  private void checkDuration(Runnable executeTransaction, Hash identifier) {
    long before = systemTime.getCurrentTimeInMillis();
    executeTransaction.run();
    long after = systemTime.getCurrentTimeInMillis();
    long timeToExecute = after - before;
    if (timeToExecute > ONE_SECOND) {
      logger.warn("Long transaction {} took {} ms", identifier, timeToExecute);
    }
  }

  private void collectEvents(ExecutedTransactionEvents result) {
    List<ExecutableEvent> ownEvents = result.ownEvents();
    localEvents.addAll(ownEvents);
    results.add(result);
  }

  public void processLocalEvents(Predicate<ExecutableEvent> isImmediatelyExecutable) {
    while (!localEvents.isEmpty() && isImmediatelyExecutable.test(localEvents.peek())) {
      ExecutableEvent localEvent = localEvents.remove();
      processTransaction(localEvent);
    }
  }

  public List<ExecutedTransactionEvents> resultingEvents() {
    return results;
  }

  public Collection<ExecutableEvent> localEvents() {
    return localEvents;
  }

  interface SystemTime {

    long getCurrentTimeInMillis();
  }
}
