package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A transaction that has been signed by the private key. */
public final class SignedTransaction implements NetworkFloodable, ExecutableTransaction {

  private static final Logger logger = LoggerFactory.getLogger(SignedTransaction.class);

  private final InnerPart inner;
  private final Signature signature;
  private final Hash hash;
  private final BlockchainAddress sender;

  /**
   * Selects the SignedTransaction instances from a stream and return these in a filtered list.
   *
   * @param transactions the stream to filter
   * @return the resulting filtered list
   */
  public static List<SignedTransaction> select(Stream<ExecutableTransaction> transactions) {
    return transactions
        .filter(transaction -> transaction instanceof SignedTransaction)
        .map(transaction -> (SignedTransaction) transaction)
        .collect(Collectors.toList());
  }

  /**
   * Create signed transaction builder for interacting with a contract.
   *
   * @param core core part of transaction
   * @param transaction transaction for interacting with a contract
   * @return builder
   */
  public static Builder create(
      CoreTransactionPart core, InteractWithContractTransaction transaction) {
    return new Builder(core, transaction);
  }

  static SignedTransaction create(String chainId, KeyPair signer, InnerPart inner) {
    Hash transactionHash = createHashToSign(inner, chainId);
    Signature signature = signer.sign(transactionHash);
    return new SignedTransaction(signature, transactionHash, inner);
  }

  private static SignedTransaction create(String chainId, Signature signature, InnerPart inner) {
    Hash hashWithSecret = createHashToSign(inner, chainId);
    return new SignedTransaction(signature, hashWithSecret, inner);
  }

  private static Hash createHashToSign(InnerPart inner, String chainId) {
    return Hash.create(
        stream -> {
          inner.write(stream);
          stream.writeString(chainId);
        });
  }

  private SignedTransaction(Signature signature, Hash hashWithSecret, InnerPart inner) {
    this.inner = inner;
    this.signature = signature;
    this.hash = hashWithSecret;
    this.sender = signature.recoverSender(hashWithSecret);
  }

  /**
   * Get inner transaction.
   *
   * @return inner transaction
   */
  public InteractWithContractTransaction getTransaction() {
    return inner.transaction;
  }

  private Signature getSignature() {
    return signature;
  }

  /**
   * Get human time inner core transaction is valid to.
   *
   * @return time in milliseconds
   */
  @SuppressWarnings("WeakerAccess")
  public long getValidToTime() {
    return inner.core.getValidToTime();
  }

  /**
   * Get cost from inner core part of transaction.
   *
   * @return cost
   */
  public long getCost() {
    return inner.core.getCost();
  }

  /**
   * Is this transaction valid with respect to signature, time and nonce. If true, this means that
   * the transaction is valid in the given human time, have the same nonce in the state as in the
   * transaction and that is has been signed correctly. <code>null</code> nonce from the lookup
   * indicates that the account is not found in the chain state. If false, one of these cases fails.
   *
   * @param productionTime the human time to check against
   * @param nonceLookup the current state and the nonce for the accounts
   * @return true if the transaction is valid and calling execute is safe, false otherwise
   */
  public boolean checkValidity(long productionTime, Function<BlockchainAddress, Long> nonceLookup) {
    if (!isSignatureValid()) {
      logger.warn("Invalid signature in transaction {}", identifier());
      return false;
    }

    BlockchainAddress signer = getSender();
    Long nonceInState = nonceLookup.apply(signer);
    boolean correctNonce = nonceInState != null && nonceInState == inner.core.getNonce();
    return correctNonce && isValidForTime(productionTime);
  }

  /**
   * Returns true if this transaction is valid at a specific time.
   *
   * @param productionTime the human time to check against
   * @return true if valid
   */
  @SuppressWarnings("WeakerAccess")
  public boolean isValidForTime(long productionTime) {
    return getValidToTime() > productionTime;
  }

  /**
   * Determine if the fee of this transaction can be covered in the given state.
   *
   * @param state the current state of the chain
   * @param blockProductionTime block production time
   * @return true if the payer of the transaction are able to cover the fee
   */
  public boolean canCoverFee(ChainState state, long blockProductionTime) {
    return state.getAccountPlugin().canCoverFee(this, blockProductionTime);
  }

  /**
   * Compute network byte count.
   *
   * @return amount of bytes
   */
  public long computeNetworkByteCount() {
    return SafeDataOutputStream.serialize(this::write).length;
  }

  boolean isSignatureValid() {
    return sender != null;
  }

  /**
   * Read a SignedTransaction from the supplied stream.
   *
   * @param chainId the chain id for this running blockchain
   * @param stream to read from
   * @return SignedTransaction read from the stream
   */
  public static SignedTransaction read(String chainId, SafeDataInputStream stream) {
    Signature signature = Signature.read(stream);
    InnerPart inner = InnerPart.read(stream);
    SignedTransaction signedTransaction = create(chainId, signature, inner);
    if (signedTransaction.isSignatureValid()) {
      return signedTransaction;
    } else {
      logger.error("Invalid transaction is filtered away: {}", signedTransaction);
      throw new RuntimeException(
          "Reading a invalid signed transaction - who created this transaction?");
    }
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    getSignature().write(stream);
    inner.write(stream);
  }

  /**
   * Unique identifier for this signed transaction.
   *
   * @return the identifier
   */
  @Override
  public Hash identifier() {
    return Hash.create(
        stream -> {
          hash.write(stream);
          signature.write(stream);
        });
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SignedTransaction that = (SignedTransaction) o;
    return Objects.equals(identifier(), that.identifier());
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier());
  }

  @Override
  public String toString() {
    return "SignedTransaction{" + "transaction=" + identifier() + ", core=" + inner.core + '}';
  }

  /**
   * Get sender of transaction.
   *
   * @return sender's address
   */
  public BlockchainAddress getSender() {
    return sender;
  }

  /**
   * Get core part of transaction.
   *
   * @return core transaction part
   */
  public CoreTransactionPart getCore() {
    return inner.core;
  }

  /** A builder class for creating signed transaction. */
  public static final class Builder {

    private final InnerPart inner;

    Builder(CoreTransactionPart core, InteractWithContractTransaction transaction) {
      inner = new InnerPart(core, transaction);
    }

    /**
     * Sign the transaction using the keypair.
     *
     * @param signer the signer of this builder
     * @param chainId the id of the chain
     * @return the signed transaction
     */
    public SignedTransaction sign(KeyPair signer, String chainId) {
      return create(chainId, signer, inner);
    }

    /**
     * Create a signed transaction with th fixed signature.
     *
     * @param signature the actual signature created elsewhere
     * @param chainId the id of the chain
     * @return the signed transaction
     */
    @SuppressWarnings("WeakerAccess")
    public SignedTransaction withSignature(Signature signature, String chainId) {
      return create(chainId, signature, inner);
    }
  }

  static final class InnerPart implements DataStreamSerializable {

    private final InteractWithContractTransaction transaction;
    private final CoreTransactionPart core;

    InnerPart(CoreTransactionPart core, InteractWithContractTransaction transaction) {
      this.transaction = transaction;
      this.core = core;
    }

    @Override
    public void write(SafeDataOutputStream stream) {
      core.write(stream);
      transaction.writeInner(stream);
    }

    static InnerPart read(SafeDataInputStream stream) {
      CoreTransactionPart core = CoreTransactionPart.read(stream);
      InteractWithContractTransaction transaction = InteractWithContractTransaction.read(stream);
      return new InnerPart(core, transaction);
    }
  }
}
