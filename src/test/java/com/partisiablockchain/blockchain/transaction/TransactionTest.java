package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TransactionTest {

  @Test
  public void executionContextGetters() {
    MutableChainState state = StateHelper.initialWithAdditions(FunctionUtility.noOpConsumer());
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    ExecutionEventManager manager =
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null, state, 1L, 1L, TestObjects.EMPTY_HASH, 0, null, feeCollector));
    long blockTime = 753L;
    long productionTime = 1259L;
    String chainId = "ChainId";
    ExecutionContextTransaction executionContext =
        ExecutionContextTransactionTest.create(
            chainId, state, TestObjects.ACCOUNT_ONE, blockTime, productionTime, manager, 0);
    Assertions.assertThat(executionContext.getChainId()).isEqualTo(chainId);
    Assertions.assertThat(executionContext.getTransactionHash()).isEqualTo(TestObjects.EMPTY_HASH);
    Assertions.assertThat(executionContext.getFrom()).isEqualTo(TestObjects.ACCOUNT_ONE);
    Assertions.assertThat(executionContext.getOriginatingTransactionHash())
        .isEqualTo(TestObjects.EMPTY_HASH);
    Assertions.assertThat(executionContext.getState()).isEqualTo(state);
    Assertions.assertThat(executionContext.getBlockTime()).isEqualTo(blockTime);
    Assertions.assertThat(executionContext.getBlockProductionTime()).isEqualTo(productionTime);
    Assertions.assertThat(executionContext.getExecutionEventManager()).isEqualTo(manager);
  }
}
