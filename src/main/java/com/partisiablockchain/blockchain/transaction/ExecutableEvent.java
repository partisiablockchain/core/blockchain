package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** An event that is executable in the blockchain. */
public final class ExecutableEvent implements ExecutableTransaction {

  /** Utility for reading a list of executable events from a stream. */
  public static final SafeListStream<ExecutableEvent> LIST_STREAM =
      SafeListStream.create(ExecutableEvent::read, ExecutableEvent::write);

  private final EventTransaction transaction;
  private final String originShard;

  /**
   * Create a new event.
   *
   * @param originShard the originating shard
   * @param transaction the actual event transaction
   */
  public ExecutableEvent(String originShard, EventTransaction transaction) {
    this.originShard = originShard;
    this.transaction = transaction;
  }

  /**
   * Selects the ExecutableEvent instances from a stream and return these in a filtered list.
   *
   * @param transactions the stream to filter
   * @return the resulting filtered list
   */
  public static List<ExecutableEvent> select(Stream<ExecutableTransaction> transactions) {
    return transactions
        .filter(transaction -> transaction instanceof ExecutableEvent)
        .map(transaction -> (ExecutableEvent) transaction)
        .collect(Collectors.toList());
  }

  @Override
  public Hash identifier() {
    return Hash.create(this);
  }

  /**
   * Read an event from the stream.
   *
   * @param stream the stream to read from
   * @return the read event
   */
  public static ExecutableEvent read(SafeDataInputStream stream) {
    String originShard = stream.readOptional(SafeDataInputStream::readString);
    EventTransaction transaction = EventTransaction.read(stream);
    return new ExecutableEvent(originShard, transaction);
  }

  /**
   * Read an event from the stream with no data limit.
   *
   * @param stream the stream to read from
   * @return the read event
   */
  public static ExecutableEvent unsafeSyncRead(SafeDataInputStream stream) {
    String originShard = stream.readOptional(SafeDataInputStream::readString);
    EventTransaction transaction = EventTransaction.unsafeSyncRead(stream);
    return new ExecutableEvent(originShard, transaction);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeOptional(SafeListStream.primitive(SafeDataOutputStream::writeString), originShard);
    transaction.write(stream);
  }

  /**
   * Get event transaction.
   *
   * @return event transaction
   */
  public EventTransaction getEvent() {
    return transaction;
  }

  /**
   * Get originating shard.
   *
   * @return shard id
   */
  public String getOriginShard() {
    return originShard;
  }

  @Override
  public String toString() {
    return "ExecutableEvent{" + "transaction=" + transaction + '}';
  }
}
