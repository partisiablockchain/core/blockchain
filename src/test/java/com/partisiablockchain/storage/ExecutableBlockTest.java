package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ExecutableBlockTest {

  @Test
  public void isMissingTransactionOrEvents() {
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
    ExecutableEvent event =
        new ExecutableEvent(
            "origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                TestObjects.ACCOUNT_FOUR,
                0,
                interact,
                new ShardRoute("dest", 0),
                0,
                1L));
    SignedTransaction transaction =
        SignedTransaction.create(CoreTransactionPart.create(1, 1000, 0), interact)
            .sign(new KeyPair(BigInteger.ONE), "chain");

    isMissing(event, event.identifier(), transaction, transaction.identifier(), false);
    isMissing(null, event.identifier(), transaction, transaction.identifier(), true);
    isMissing(event, event.identifier(), null, transaction.identifier(), true);
    isMissing(event, TestObjects.EMPTY_HASH, transaction, transaction.identifier(), true);
    isMissing(event, event.identifier(), transaction, TestObjects.EMPTY_HASH, true);
  }

  private void isMissing(
      ExecutableEvent event,
      Hash eventIdentifier,
      SignedTransaction transaction,
      Hash transactionIdentifier,
      boolean expected) {
    ExecutableBlock executableBlock =
        new ExecutableBlock(
            new FinalBlock(
                new Block(
                    0,
                    0,
                    0,
                    TestObjects.EMPTY_HASH,
                    TestObjects.EMPTY_HASH,
                    List.of(eventIdentifier),
                    List.of(transactionIdentifier)),
                new byte[0]),
            Collections.singletonList(transaction),
            Collections.singletonList(event));
    Assertions.assertThat(executableBlock.isMissingTransactionOrEvents()).isEqualTo(expected);
  }
}
