package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.function.Function;
import java.util.stream.Stream;

final class HashStorageImpl<T extends DataStreamSerializable> implements HashStorage<T> {

  private final Function<SafeDataInputStream, T> reader;
  private final Storage storage;

  HashStorageImpl(Storage storage, Function<SafeDataInputStream, T> reader) {
    this.storage = storage;
    this.reader = reader;
  }

  @Override
  public Stream<T> readEvery() {
    return storage.stream(reader);
  }

  @Override
  public T read(Hash hash) {
    return storage.read(hash, reader);
  }

  @Override
  public void write(Hash hash, T writer) {
    storage.write(hash, writer::write);
  }

  @Override
  public boolean isEmpty() {
    return storage.isEmpty();
  }

  @Override
  public boolean has(Hash hash) {
    return storage.has(hash);
  }

  @Override
  public void clear() {
    storage.clear();
  }

  @Override
  public void remove(Hash hash) {
    storage.remove(hash);
  }
}
