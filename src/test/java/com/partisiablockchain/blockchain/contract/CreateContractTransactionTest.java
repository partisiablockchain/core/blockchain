package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.ZkTestBinder;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.TestContracts;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.PubTestContract.PubTestState;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.SysTestContract.SysTestState;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CreateContractTransactionTest {

  private final KeyPair rootKey = new KeyPair(BigInteger.valueOf(213));
  private final BlockchainAddress root = rootKey.getPublic().createAddress();
  private final BlockchainAddress from = TestObjects.ACCOUNT_TWO;
  private final MutableChainState state =
      StateHelper.createMutableState(
          Map.of(TestObjects.ACCOUNT_ONE, 10L, from, 10L, root, 10L), root);

  @Test
  public void nullAbi() {
    CreateContractTransaction transaction =
        CreateContractTransaction.create(from, new byte[1], new byte[2], null, new byte[3]);

    byte[] bytes = SafeDataOutputStream.serialize(transaction::write);
    Assertions.assertThat(Hex.toHexString(bytes))
        .isEqualTo(
            ""
                + "00000000000000000000000000"
                + "00000000000000000200000001"
                + "00000000020000000000000000"
                + "0003000000");
  }

  @Test
  public void deploySys() {
    byte[] bindingJar = TestContracts.CONTRACT_BINDER_SYSTEM;
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_SYS, bindingJar, contractJar, new byte[0], rpc);
    long previousBalance = FeePluginHelper.balance(state, from);
    Assertions.assertThat(previousBalance).isEqualTo(10);

    transaction.execute(executionContext(root));

    int storageGas = 0;
    long currentBalance = FeePluginHelper.balance(state, from);
    Assertions.assertThat(currentBalance).isEqualTo(previousBalance - storageGas);
    StateSerializable serializedContract = state.getContractState(TestObjects.CONTRACT_SYS);
    Assertions.assertThat(serializedContract).hasFieldOrPropertyWithValue("state", 37);
  }

  @Test
  public void deployTooLargeRpc() {
    byte[] bindingJar = TestContracts.CONTRACT_BINDER_SYSTEM;
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    byte[] rpc = new byte[1];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_SYS, bindingJar, contractJar, new byte[0], rpc);
    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext(root)))
        .hasMessageContaining("Entire content of stream was not read");
  }

  @Test
  public void simpleProperties() {
    byte[] binderJar = new byte[0];
    byte[] contractJar = new byte[0];
    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_SYS, binderJar, contractJar, new byte[0], rpc);

    Assertions.assertThat(transaction.getTargetContract()).isEqualTo(TestObjects.CONTRACT_SYS);
  }

  @Test
  public void deployingSystemContractDoesNotEnablesStorageFees() {
    byte[] bindingJar = TestContracts.CONTRACT_BINDER_SYSTEM;
    byte[] contractJar = JarBuilder.buildJar(SysTestContract.class);

    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_GOV1, bindingJar, contractJar, new byte[0], rpc);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    Assertions.assertThat(
            FeePluginHelper.timestampForLatestStorageFeePayment(state, TestObjects.CONTRACT_GOV1))
        .isEqualTo(-1L);
    Assertions.assertThat(FeePluginHelper.balance(state, TestObjects.CONTRACT_GOV1)).isEqualTo(0L);
  }

  @Test
  public void deployingPublicContractEnablesStorageFees() {
    byte[] bindingJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    byte[] contractJar = JarBuilder.buildJar(PubTestContract.class);

    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_PUB1, bindingJar, contractJar, new byte[0], rpc);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    Assertions.assertThat(
            FeePluginHelper.timestampForLatestStorageFeePayment(state, TestObjects.CONTRACT_PUB1))
        .isEqualTo(executionContext.getBlockProductionTime());
    Assertions.assertThat(FeePluginHelper.balance(state, TestObjects.CONTRACT_PUB1)).isEqualTo(0L);
  }

  @Test
  public void deployingZkContractEnablesStorageFees() {
    byte[] bindingJar = JarBuilder.buildJar(ZkTestBinder.class);
    byte[] contractJar = JarBuilder.buildJar(ZkTestContract.class);

    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_ZK1, bindingJar, contractJar, new byte[0], rpc);

    ExecutionContextTransaction executionContext = executionContext(root);
    transaction.execute(executionContext);

    Assertions.assertThat(
            FeePluginHelper.timestampForLatestStorageFeePayment(state, TestObjects.CONTRACT_ZK1))
        .isEqualTo(executionContext.getBlockProductionTime());
    Assertions.assertThat(FeePluginHelper.balance(state, TestObjects.CONTRACT_ZK1)).isEqualTo(0L);

    state.removeContract(TestObjects.CONTRACT_ZK1, 0);
    Assertions.assertThat(
            FeePluginHelper.timestampForLatestStorageFeePayment(state, TestObjects.CONTRACT_ZK1))
        .isEqualTo(0L);
    Assertions.assertThat(FeePluginHelper.balance(state, TestObjects.CONTRACT_ZK1)).isEqualTo(0L);
  }

  private ExecutionContextTransaction executionContext(BlockchainAddress root) {
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    return ExecutionContextTransactionTest.create(
        "ChainId",
        state,
        root,
        1L,
        1L,
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null, state, 1L, 1L, TestObjects.EMPTY_HASH, 0, null, feeCollector)),
        0);
  }

  /** Test. */
  public static final class PubTestContract extends PubContract<PubTestState> {

    /** Test. */
    @Immutable
    public static final class PubTestState implements StateSerializable {

      public final int state;

      @SuppressWarnings("unused")
      PubTestState() {
        state = 0;
      }

      PubTestState(int nextState) {
        state = nextState;
      }
    }

    @Override
    public PubTestState onCreate(PubContractContext context, SafeDataInputStream rpc) {
      return new PubTestState(37);
    }
  }

  /** Test. */
  public static final class SysTestContract extends SysContract<SysTestState> {

    /** Test. */
    @Immutable
    public static final class SysTestState implements StateSerializable {

      public final int state;

      @SuppressWarnings("unused")
      SysTestState() {
        state = 0;
      }

      SysTestState(int nextState) {
        state = nextState;
      }
    }

    @Override
    public SysTestState onCreate(SysContractContext context, SafeDataInputStream rpc) {
      return new SysTestState(37);
    }
  }
}
