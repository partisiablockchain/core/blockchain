package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.util.NumericConversion;
import java.security.SecureRandom;
import java.util.Arrays;

/** Key for kademlia. */
public final class KademliaKey {

  private static final SecureRandom RANDOM = new SecureRandom();

  private final long[] value;

  /**
   * Create a new Kademlia key from the supplied bytes.
   *
   * @param bytes 32 bytes representing the id
   */
  public KademliaKey(byte[] bytes) {
    if (bytes.length != 32) {
      throw new IllegalArgumentException("Key is not allowed to exceed 256 bits");
    }
    this.value =
        new long[] {
          NumericConversion.longFromBytes(bytes, 0),
          NumericConversion.longFromBytes(bytes, 8),
          NumericConversion.longFromBytes(bytes, 16),
          NumericConversion.longFromBytes(bytes, 24)
        };
  }

  /** Create a new random KademliaKey. */
  public KademliaKey() {
    this(randomByteKey());
  }

  int distance(KademliaKey other) {
    int i = Arrays.mismatch(this.value, other.value);
    if (i >= 0) {
      return i * 64 + Long.numberOfLeadingZeros(this.value[i] ^ other.value[i]);
    }
    // Special case for distance to own key
    return 0;
  }

  private static byte[] randomByteKey() {
    byte[] bytes = new byte[32];
    RANDOM.nextBytes(bytes);
    return bytes;
  }

  byte[] toBytes() {
    byte[] bytes = new byte[32];
    for (int i = 0; i < value.length; i++) {
      NumericConversion.longToBytes(value[i], bytes, i * 8);
    }
    return bytes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KademliaKey that = (KademliaKey) o;
    return Arrays.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(value);
  }

  int compareRelativeToThis(KademliaKey first, KademliaKey second) {
    long[] firstDistance = xor(this.value, first.value);
    long[] secondDistance = xor(this.value, second.value);
    return Arrays.compareUnsigned(secondDistance, firstDistance);
  }

  private long[] xor(long[] left, long[] right) {
    long[] result = new long[4];
    for (int i = 0; i < result.length; i++) {
      result[i] = left[i] ^ right[i];
    }
    return result;
  }

  KademliaKey createForDistance(int index) {
    KademliaKey kademliaKey = new KademliaKey();

    int wordIndex = index / 64;

    /*
     * Using 'index - wordIndex * 64' instead of index is equivalent since
     * the n*64 extra bit shift is a no-op.
     */
    long withBitIndexSet = 1L << (63 - index);
    long firstMask = withBitIndexSet - 1;
    long secondMask = ~firstMask;

    System.arraycopy(this.value, 0, kademliaKey.value, 0, wordIndex);

    long firstPart = this.value[wordIndex] & secondMask;
    long secondPart = kademliaKey.value[wordIndex] & firstMask;
    kademliaKey.value[wordIndex] = (firstPart | secondPart) ^ withBitIndexSet;
    return kademliaKey;
  }
}
