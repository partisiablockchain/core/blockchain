package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KademliaKeyTest {

  @Test
  public void distanceOfOne() {
    byte[] bytes = new byte[32];
    bytes[0] = (byte) (bytes[0] | 0b10000000);
    KademliaKey firstKey = new KademliaKey(bytes);
    byte[] second = new byte[32];
    second[0] = (byte) (second[0] | 0b11000000);
    KademliaKey secondKey = new KademliaKey(second);

    int distance = firstKey.distance(secondKey);
    assertThat(distance).isEqualTo(1);
  }

  @Test
  public void distanceToSelf() {
    KademliaKey kademliaKey = new KademliaKey();
    KademliaKey same = new KademliaKey(kademliaKey.toBytes());

    assertThat(kademliaKey.distance(same)).isEqualTo(0);
  }

  @Test
  public void toBytes() {
    byte[] randomBytes =
        new byte[] {
          80, -74, 36, -93, 126, -127, 6, 72, 58, -12, -36, 47, 34, 53, -120, 60, 104, 87, -4, -13,
          119, -48, -98, 80, -110, -7, 94, -101, 25, -9, -67, 111
        };
    KademliaKey kademliaKey = new KademliaKey(randomBytes);
    assertThat(kademliaKey.toBytes()).isEqualTo(randomBytes);
  }

  @Test
  public void invalidByteCount() {
    assertThatThrownBy(() -> new KademliaKey(new byte[33]))
        .isInstanceOf(IllegalArgumentException.class);

    assertThatThrownBy(() -> new KademliaKey(new byte[31]))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void createForDistance() {
    KademliaKey kademliaKey = new KademliaKey();
    for (int j = 0; j < 30; j++) {
      for (int i = 0; i < 256; i++) {
        KademliaKey forDistance = kademliaKey.createForDistance(i);
        assertThat(forDistance.distance(kademliaKey)).isEqualTo(i);
      }
    }
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(KademliaKey.class).verify();
  }
}
