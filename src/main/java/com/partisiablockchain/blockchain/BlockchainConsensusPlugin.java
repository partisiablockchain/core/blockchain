package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.List;

/** The blockchain consensus plugin which validates a final block. */
@Immutable
public abstract class BlockchainConsensusPlugin<
        GlobalT extends StateSerializable, LocalT extends StateSerializable>
    implements BlockchainPlugin<GlobalT, LocalT> {

  /**
   * Validate local block.
   *
   * @param globalState global chain state
   * @param local local chain state
   * @param block to validate
   * @return true if block is valid, false otherwise
   */
  public abstract BlockValidation validateLocalBlock(
      GlobalT globalState, LocalT local, FinalBlock block);

  /**
   * Validate external block.
   *
   * @param globalState global chain state
   * @param block to validate
   * @return true if block is valid, false otherwise
   */
  public abstract boolean validateExternalBlock(GlobalT globalState, FinalBlock block);

  @Override
  public abstract Class<LocalT> getLocalStateClass();

  @Override
  public final List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of();
  }

  @Override
  public final InvokeResult<LocalT> invokeLocal(
      PluginContext pluginContext,
      GlobalT globalState,
      LocalT state,
      BlockchainAddress invocationContext,
      byte[] rpc) {
    throw new UnsupportedOperationException();
  }

  /**
   * Updates the local state for every block.
   *
   * @param pluginContext context for plugin
   * @param globalState the current global state
   * @param localState the current local state
   * @param block the current block
   * @return the new local state
   */
  public abstract LocalT updateForBlock(
      PluginContext pluginContext, GlobalT globalState, LocalT localState, Block block);

  /**
   * The result of validating a can be either accept or reject. If accept, then the consensus may
   * have changed.
   */
  @SuppressWarnings("WeakerAccess")
  public static final class BlockValidation {

    private static final BlockValidation REJECTED = new BlockValidation(false, false);

    private final boolean accepted;
    private final boolean canRollback;

    BlockValidation(boolean accepted, boolean canRollback) {
      this.accepted = accepted;
      this.canRollback = canRollback;
    }

    /**
     * Check if block is accepted.
     *
     * @return true if block is accepted, false otherwise
     */
    public boolean isAccepted() {
      return accepted;
    }

    /**
     * Check if block can be rolled back.
     *
     * @return true if block can be rolled back, false otherwise
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean canRollback() {
      return canRollback;
    }

    /**
     * Create accepted block validation.
     *
     * @param canRollback true if block can be rolled back, false otherwise
     * @return accepted block validation
     */
    public static BlockValidation createAccepted(boolean canRollback) {
      return new BlockValidation(true, canRollback);
    }

    /**
     * Create rejected block validation.
     *
     * @return rejected block validation
     */
    public static BlockValidation createRejected() {
      return REJECTED;
    }
  }
}
