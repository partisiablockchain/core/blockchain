package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.NetworkFloodable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Objects;

/**
 * A block is the most central object in pbc. This building block defines the ordering of
 * transactions and events on the blockchain. It consists of times (blocktime and productionTime as
 * well as its parent/predecessor block and the included transactions.
 */
public final class Block implements NetworkFloodable {

  /** Genesis parent hash. */
  public static final Hash GENESIS_PARENT =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000000");

  private final long productionTime;
  private final long blockTime;
  private final long committeeId;
  private final Hash parentBlock;
  private final Hash state;
  private final List<Hash> eventTransactions;
  private final List<Hash> transactions;
  private final short producerIndex;

  private final Hash identifier;

  /**
   * Construct a new block.
   *
   * @param productionTime the production timestamp.
   * @param blockTime the block time for this block.
   * @param committeeId the id of the committee that produced this block
   * @param parentBlock the hash of the parent block.
   * @param state the hash of the state.
   * @param eventTransactions event transaction from predecessor block
   * @param transactions the list of hashes of transactions contained in this block.
   */
  public Block(
      long productionTime,
      long blockTime,
      long committeeId,
      Hash parentBlock,
      Hash state,
      List<Hash> eventTransactions,
      List<Hash> transactions) {
    this(
        productionTime,
        blockTime,
        committeeId,
        parentBlock,
        state,
        eventTransactions,
        transactions,
        -1);
  }

  /**
   * Construct a new block.
   *
   * @param productionTime the production timestamp.
   * @param blockTime the block time for this block.
   * @param committeeId the id of the committee that produced this block
   * @param parentBlock the hash of the parent block.
   * @param state the hash of the state.
   * @param eventTransactions event transaction from predecessor block
   * @param transactions the list of hashes of transactions contained in this block.
   * @param producerIndex the producer index in the committee at the time of block production
   */
  public Block(
      long productionTime,
      long blockTime,
      long committeeId,
      Hash parentBlock,
      Hash state,
      List<Hash> eventTransactions,
      List<Hash> transactions,
      int producerIndex) {
    this.productionTime = productionTime;
    this.blockTime = blockTime;
    this.committeeId = committeeId;
    this.parentBlock = parentBlock;
    this.state = state;
    this.eventTransactions = eventTransactions;
    this.transactions = transactions;
    this.identifier = Hash.create(this);
    this.producerIndex = (short) producerIndex;
  }

  private Block(long committeeId, Hash initialStateHash) {
    this(0L, 0L, committeeId, GENESIS_PARENT, initialStateHash, List.of(), List.of(), (short) -1);
  }

  static Block createGenesis(Hash initialStateHash) {
    return new Block(0L, initialStateHash);
  }

  /**
   * Get the block time for this block.
   *
   * @return the block time for this block
   */
  public long getBlockTime() {
    return blockTime;
  }

  /**
   * Get the id of the committee that produced this block.
   *
   * @return the id of the committee that produced this block
   */
  public long getCommitteeId() {
    return committeeId;
  }

  /**
   * Get the production timestamp.
   *
   * @return the production timestamp
   */
  public long getProductionTime() {
    return productionTime;
  }

  /**
   * Get the hash of the parent block.
   *
   * @return the hash of the parent block
   */
  public Hash getParentBlock() {
    return parentBlock;
  }

  /**
   * Get the hash of the state.
   *
   * @return the hash of the state
   */
  public Hash getState() {
    return state;
  }

  /**
   * Get identifier of block.
   *
   * @return identifying hash
   */
  public Hash identifier() {
    return identifier;
  }

  /**
   * Get the list of hashes of transactions contained in this block.
   *
   * @return the list of hashes of transactions contained in this block
   */
  public List<Hash> getTransactions() {
    return transactions;
  }

  /**
   * Get event transaction from predecessor block.
   *
   * @return event transaction from predecessor block
   */
  public List<Hash> getEventTransactions() {
    return eventTransactions;
  }

  /**
   * Get the producer index in the committee at the time of block production.
   *
   * @return the producer index in the committee at the time of block production
   */
  public short getProducerIndex() {
    return producerIndex;
  }

  /**
   * Reads a block from the stream.
   *
   * @param stream the stream to read from
   * @return the block read from the stream
   */
  public static Block read(SafeDataInputStream stream) {
    long productionTime = stream.readLong();
    long blockTime = stream.readLong();
    long committeeId = stream.readLong();
    Hash parentHash = Hash.read(stream);

    Hash stateHash = Hash.read(stream);

    int transactionHashes = stream.readUnsignedByte();
    List<Hash> events = Hash.LIST_SERIALIZER.readFixed(stream, transactionHashes);
    transactionHashes = stream.readUnsignedByte();
    List<Hash> hashes = Hash.LIST_SERIALIZER.readFixed(stream, transactionHashes);
    short producer = stream.readShort();
    return new Block(
        productionTime, blockTime, committeeId, parentHash, stateHash, events, hashes, producer);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeLong(productionTime);
    stream.writeLong(blockTime);
    stream.writeLong(committeeId);
    parentBlock.write(stream);
    state.write(stream);
    stream.writeByte(eventTransactions.size());
    Hash.LIST_SERIALIZER.writeFixed(stream, eventTransactions);
    stream.writeByte(transactions.size());
    Hash.LIST_SERIALIZER.writeFixed(stream, transactions);
    stream.writeShort(producerIndex);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Block block = (Block) o;
    return Objects.equals(identifier(), block.identifier());
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier());
  }

  @Override
  public String toString() {
    return "Block{blockTime="
        + blockTime
        + ", hash="
        + identifier()
        + ", events="
        + eventTransactions
        + ", transactions="
        + transactions
        + ", producerIndex="
        + producerIndex
        + '}';
  }
}
