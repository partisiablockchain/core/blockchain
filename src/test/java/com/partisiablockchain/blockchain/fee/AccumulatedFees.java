package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** Fees accumulated by the account plugin. */
@Immutable
public final class AccumulatedFees implements StateSerializable {

  private final long fees;
  private final AvlTree<Long, Long> registeredBlockchainUsage; // BP fees
  private final AvlTree<BlockchainAddress, Long> registeredFees;
  private final FixedList<PendingByocFee> registeredByocFees;
  private final long infrastructureFeesSum;

  /** Default constructor. */
  @SuppressWarnings("unused")
  AccumulatedFees() {
    fees = 0;
    registeredBlockchainUsage = null;
    registeredFees = null;
    registeredByocFees = null;
    infrastructureFeesSum = 0;
  }

  private AccumulatedFees(
      long fees,
      AvlTree<Long, Long> registeredBlockchainUsage,
      AvlTree<BlockchainAddress, Long> registeredFees,
      FixedList<PendingByocFee> registeredByocFees,
      Long infrastructureFeesSum) {
    this.fees = fees;
    this.registeredBlockchainUsage = registeredBlockchainUsage;
    this.registeredFees = registeredFees;
    this.registeredByocFees = registeredByocFees;
    this.infrastructureFeesSum = infrastructureFeesSum;
  }

  /**
   * Create a new account state.
   *
   * @return the created account state
   */
  public static AccumulatedFees create() {
    return new AccumulatedFees(0L, AvlTree.create(), AvlTree.create(), FixedList.create(), 0L);
  }

  /**
   * Add fees that have been paid.
   *
   * @param fees amount that has been paid
   * @return the updated state
   */
  public AccumulatedFees addFees(long fees) {
    return new AccumulatedFees(
        this.fees + fees,
        this.registeredBlockchainUsage,
        registeredFees,
        registeredByocFees,
        infrastructureFeesSum);
  }

  /**
   * Register that the blockchain has been used for a specified epoch, to facilitate distributing
   * the used gas between BPs.
   *
   * @param usage the amount of gas that has been used
   * @return the updated state
   */
  public AccumulatedFees registerBlockchainUsage(long usage, long blockProductionTime) {
    long currentUsage = getBlockchainUsageForEpoch(blockProductionTime);
    return new AccumulatedFees(
        this.fees,
        this.registeredBlockchainUsage.set(blockProductionTime, currentUsage + usage),
        registeredFees,
        registeredByocFees,
        infrastructureFeesSum);
  }

  long getBlockchainUsageForEpoch(long epoch) {
    if (registeredBlockchainUsage.containsKey(epoch)) {
      return registeredBlockchainUsage.getValue(epoch);
    } else {
      return 0;
    }
  }

  /**
   * Pay service fees.
   *
   * @param target node that is being paid
   * @param gas amount of gas to pay
   * @return the updated state
   */
  public AccumulatedFees payServiceFees(BlockchainAddress target, Long gas) {
    AvlTree<BlockchainAddress, Long> updatedServiceFees = this.registeredFees;
    long paidFees = Objects.requireNonNullElse(updatedServiceFees.getValue(target), 0L);
    updatedServiceFees = updatedServiceFees.set(target, paidFees + gas);

    return new AccumulatedFees(
        this.fees,
        this.registeredBlockchainUsage,
        updatedServiceFees,
        registeredByocFees,
        infrastructureFeesSum);
  }

  /**
   * Register BYOC fees.
   *
   * @param fee amount of gas
   * @return the updated state
   */
  public AccumulatedFees registerByocFee(PendingByocFee fee) {
    FixedList<PendingByocFee> updated = registeredByocFees.addElement(fee);
    return new AccumulatedFees(
        fees, registeredBlockchainUsage, registeredFees, updated, infrastructureFeesSum);
  }

  public List<PendingByocFee> getRegisteredByocFees() {
    return new ArrayList<>(registeredByocFees);
  }

  /**
   * Returns collected fees.
   *
   * @return the fees
   */
  public long fees() {
    return fees;
  }

  public long getRegisteredBlockchainUsage() {
    return registeredBlockchainUsage.values().stream().reduce(Long::sum).orElse(0L);
  }

  /**
   * Registers blockchain usage per time.
   *
   * @param blockProductionTime the block production time
   * @return the usage or 0
   */
  public long getRegisteredBlockchainUsage(long blockProductionTime) {
    Long value = registeredBlockchainUsage.getValue(blockProductionTime);
    if (value == null) {
      value = 0L;
    }
    return value;
  }

  /**
   * Get amount of services fees for an account.
   *
   * @param address the account to lookup
   * @return the amount of service fees
   */
  public Long serviceFeesDistributedTo(BlockchainAddress address) {
    return registeredFees.getValue(address);
  }

  /**
   * Add infrastructure fee.
   *
   * @param difference the amount of gas
   * @return the updated state
   */
  public AccumulatedFees addInfrastructureFees(long difference) {
    return new AccumulatedFees(
        fees,
        registeredBlockchainUsage,
        registeredFees,
        registeredByocFees,
        infrastructureFeesSum + difference);
  }

  public long getInfrastructureFeesSum() {
    return infrastructureFeesSum;
  }
}
