package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.List;
import org.assertj.core.api.Assertions;

/** Helper class for BlockchainLedger tests. */
public final class BlockchainLedgerTestHelper {

  static Block createBlock(BlockchainLedger.BlockAndState parent, Hash... transactions) {
    return createBlock(
        parent.getBlockProductionTime(),
        parent.getBlockTime() + 1,
        parent.getBlock().identifier(),
        parent.getState().getHash(),
        List.of(transactions),
        List.of());
  }

  static Block createBlock(
      long parentProductionTime,
      long blockTime,
      Hash parentHash,
      Hash stateHash,
      List<Hash> transactions,
      List<Hash> events) {
    return new Block(
        parentProductionTime + 1,
        blockTime,
        blockTime,
        parentHash,
        stateHash,
        events,
        transactions);
  }

  static FinalBlock createFinalBlock(
      BlockchainLedger.BlockAndState parent, byte nextFinalization, Hash... transactions) {
    return new FinalBlock(createBlock(parent, transactions), new byte[] {nextFinalization});
  }

  static void appendBlock(
      BlockchainLedger blockchain,
      KeyPair keyPair,
      BlockchainAddress signer,
      byte nextFinalization,
      InteractWithContractTransaction transaction) {
    SignedTransaction signed = signedTransaction(blockchain, keyPair, signer, transaction);
    Assertions.assertThat(blockchain.addPendingTransaction(signed)).isTrue();
    blockchain.appendBlock(
        new FinalBlock(
            createBlock(blockchain.latest(), signed.identifier()), new byte[] {nextFinalization}));
  }

  static SignedTransaction signedTransaction(
      BlockchainLedger blockchain,
      KeyPair keyPair,
      BlockchainAddress signer,
      InteractWithContractTransaction transaction) {
    return SignedTransaction.create(
            CoreTransactionPart.create(
                blockchain.latest().getState().lookupNonce(signer),
                System.currentTimeMillis() + 100_000,
                10_000),
            transaction)
        .sign(keyPair, blockchain.getChainId());
  }

  /** Test. */
  @Immutable
  public interface ByteVerifying extends StateSerializable {

    /**
     * Get the byte.
     *
     * @return the byte
     */
    byte value();
  }

  /** Test. */
  @Immutable
  public record ValueCount(byte value) implements ByteVerifying {
    /** Default constructor. */
    public ValueCount() {
      this((byte) 0);
    }

    /**
     * Set the value in state.
     *
     * @param next the updated value
     * @return state with updated value
     */
    public ValueCount setByte(byte next) {
      return new ValueCount(next);
    }
  }
}
