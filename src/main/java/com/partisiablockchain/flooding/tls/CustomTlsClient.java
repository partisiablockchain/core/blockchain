package com.partisiablockchain.flooding.tls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.TlsUtils;
import java.io.IOException;
import java.util.function.Consumer;
import org.bouncycastle.tls.Certificate;
import org.bouncycastle.tls.CertificateRequest;
import org.bouncycastle.tls.CipherSuite;
import org.bouncycastle.tls.DefaultTlsClient;
import org.bouncycastle.tls.ProtocolVersion;
import org.bouncycastle.tls.TlsAuthentication;
import org.bouncycastle.tls.TlsCredentials;
import org.bouncycastle.tls.TlsServerCertificate;
import org.bouncycastle.tls.crypto.impl.bc.BcTlsCrypto;

/** The TLS client used when making a handshake. */
public final class CustomTlsClient extends DefaultTlsClient {

  private final BcTlsCrypto tlsCrypto;
  private final KeyPair signer;
  private final Certificate selfSignedCertificate;
  private final Consumer<BlockchainPublicKey> publicKeyConsumer;

  /**
   * Create a TLS client.
   *
   * @param tlsCrypto the TLS crypto used by the default client.
   * @param signer the key pair used to sign the certificate.
   * @param selfSignedCertificate the self signed certificate.
   * @param publicKeyConsumer to obtain public key of tls counterpart.
   */
  public CustomTlsClient(
      BcTlsCrypto tlsCrypto,
      KeyPair signer,
      Certificate selfSignedCertificate,
      Consumer<BlockchainPublicKey> publicKeyConsumer) {
    super(tlsCrypto);
    this.tlsCrypto = tlsCrypto;
    this.signer = signer;
    this.selfSignedCertificate = selfSignedCertificate;
    this.publicKeyConsumer = publicKeyConsumer;
  }

  @Override
  public ProtocolVersion[] getProtocolVersions() {
    return new ProtocolVersion[] {ProtocolVersion.TLSv12};
  }

  @Override
  protected int[] getSupportedCipherSuites() {
    return new int[] {CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256};
  }

  @Override
  public TlsAuthentication getAuthentication() {
    return new TlsAuthentication() {

      @Override
      public void notifyServerCertificate(TlsServerCertificate tlsServerCertificate)
          throws IOException {
        BlockchainPublicKey publicKey =
            TlsUtils.validateCertificate(tlsServerCertificate.getCertificate());
        publicKeyConsumer.accept(publicKey);
      }

      @Override
      public TlsCredentials getClientCredentials(CertificateRequest certificateRequest) {
        return TlsUtils.getTlsSignerCredentials(signer, tlsCrypto, context, selfSignedCertificate);
      }
    };
  }
}
