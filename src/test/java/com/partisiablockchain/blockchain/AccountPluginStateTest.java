package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountPluginStateTest {

  @Test
  public void removeAccounts() {
    AvlTree<BlockchainAddress, AccountState> accounts = AvlTree.create();
    accounts = accounts.set(TestObjects.ACCOUNT_ONE, new AccountState());
    AvlTree<BlockchainAddress, ContractState> contracts = AvlTree.create();

    AccountPluginState<ContextFreeState, AccountState, ContractState> state =
        new AccountPluginState<>(new ContextFreeState(), accounts, contracts);
    Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_ONE)).isNotNull();
    state = state.setAccount(TestObjects.ACCOUNT_ONE, null);
    Assertions.assertThat(state.getAccount(TestObjects.ACCOUNT_ONE)).isNull();
  }

  /** Test. */
  @Immutable
  private static final class ContextFreeState implements StateSerializable {}

  /** Test. */
  @Immutable
  private static final class AccountState implements StateSerializable {}

  /** Test. */
  @Immutable
  private static final class ContractState implements StateSerializable {}
}
