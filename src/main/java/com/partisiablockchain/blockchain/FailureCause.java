package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Stores the error message and stack trace of a failed transaction. */
public final class FailureCause implements DataStreamSerializable {

  private final String errorMessage;
  private final String stackTrace;

  /**
   * Creates a new {@link FailureCause} for storing failed transaction messages.
   *
   * @param errorMessage the error message from an exception.
   * @param stackTrace the stack trace from an exception.
   */
  public FailureCause(String errorMessage, String stackTrace) {
    this.errorMessage = errorMessage;
    this.stackTrace = stackTrace;
  }

  /**
   * Returns an error message.
   *
   * @return an error message.
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  /**
   * Returns an stack trace.
   *
   * @return an stack trace.
   */
  public String getStackTrace() {
    return stackTrace;
  }

  /**
   * Reads a {@link FailureCause} from an input stream.
   *
   * @param input input stream for reading object.
   * @return object containing error message and stack trace.
   */
  public static FailureCause read(SafeDataInputStream input) {
    return new FailureCause(input.readString(), input.readString());
  }

  /**
   * Writes content to the output stream.
   *
   * @param output stream for serializing object.
   */
  @Override
  public void write(SafeDataOutputStream output) {
    output.writeString(this.getErrorMessage());
    output.writeString(this.getStackTrace());
  }
}
