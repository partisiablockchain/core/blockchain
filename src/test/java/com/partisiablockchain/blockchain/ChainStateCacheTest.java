package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.pub.PubBinderContext;
import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;
import java.io.InputStream;
import java.util.Objects;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of {@link ChainStateCache}. */
public final class ChainStateCacheTest {

  private SerializationResult publicBinder;
  private SerializationResult contractBinder;
  private ChainStateCache chainStateCache;

  /** Sets up test. */
  @BeforeEach
  public void setUp() {
    chainStateCache = new ChainStateCache(ObjectCreator.createMemoryStateStorage());
    byte[] publicBinderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    publicBinder =
        chainStateCache.createStateSerializer().write(new LargeByteArray(publicBinderJar));
    contractBinder =
        chainStateCache
            .createStateSerializer()
            .write(new LargeByteArray(TestContracts.CONTRACT_VOID));
  }

  /** Contracts are cached, and repeat access returns the same object. */
  @Test
  public void shouldCacheContracts() {
    BlockchainContract<StateSerializable, BinderEvent> contract =
        chainStateCache.get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash());
    Assertions.assertThat(contract).isNotNull();
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isSameAs(contract);
  }

  /**
   * Contracts with differing contract bytecode does not result in the same {@link
   * BlockchainContract} object.
   */
  @Test
  public void shouldNotCacheContracts_differentContractJar() {
    BlockchainContract<StateSerializable, BinderEvent> contract =
        chainStateCache.get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash());
    Assertions.assertThat(contract).isNotNull();
    ChainStateCache.BlockchainContractWithJars blockchainContractWithJars =
        new ChainStateCache.BlockchainContractWithJars(
            contract, publicBinder.hash(), contractBinder.hash());

    contractBinder =
        chainStateCache
            .createStateSerializer()
            .write(new LargeByteArray(TestContracts.CONTRACT_LONG));
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotSameAs(contract);

    ChainStateCache.BlockchainContractWithJars otherBlockchainContractWithJars =
        new ChainStateCache.BlockchainContractWithJars(
            contract, publicBinder.hash(), contractBinder.hash());
    Assertions.assertThat(blockchainContractWithJars.blockchainContract())
        .isEqualTo(otherBlockchainContractWithJars.blockchainContract());
    Assertions.assertThat(blockchainContractWithJars.binderJar())
        .isEqualTo(otherBlockchainContractWithJars.binderJar());
    Assertions.assertThat(blockchainContractWithJars.contractJar())
        .isNotEqualTo(otherBlockchainContractWithJars.contractJar());
  }

  /**
   * Contracts with differing binder bytecode does not result in the same {@link BlockchainContract}
   * object.
   */
  @Test
  public void shouldNotCacheContracts_differentBinderJar() {
    BlockchainContract<StateSerializable, BinderEvent> contract =
        chainStateCache.get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash());
    Assertions.assertThat(contract).isNotNull();

    byte[] otherBinderJar = JarBuilder.buildJar(RawBinder.class);
    publicBinder =
        chainStateCache.createStateSerializer().write(new LargeByteArray(otherBinderJar));
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotSameAs(contract);
  }

  /** WASM Contracts are cached, and repeat access returns the same object. */
  @Test
  public void shouldCacheWasmContracts() {
    chainStateCache = new ChainStateCache(ObjectCreator.createMemoryStateStorage());
    byte[] publicBinderJar = JarBuilder.buildJar(RawBinder.class);
    byte[] contractData = loadWasmContract();
    publicBinder =
        chainStateCache.createStateSerializer().write(new LargeByteArray(publicBinderJar));
    contractBinder =
        chainStateCache.createStateSerializer().write(new LargeByteArray(contractData));

    BlockchainContract<StateSerializable, BinderEvent> contract =
        chainStateCache.get(
            TestObjects.CONTRACT_WASM_PUB1, publicBinder.hash(), contractBinder.hash());
    Assertions.assertThat(contract).isNotNull();
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_WASM_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isSameAs(contract);
  }

  /** Contracts are not removed from the cache if they are used in the deletion delay period. */
  @Test
  public void shouldMarkUsedOnRead() {
    BlockchainContract<StateSerializable, BinderEvent> contract = create();
    chainStateCache.markContractsForRemoval(Set.of(TestObjects.CONTRACT_PUB1));
    for (int i = 0; i < ChainStateCache.CONTRACT_REMOVAL_DELAY_IN_BLOCKS - 1; i++) {
      chainStateCache.markContractsForRemoval(Set.of());
    }
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotNull()
        .isEqualTo(contract);
    chainStateCache.markContractsForRemoval(Set.of());
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotNull()
        .isEqualTo(contract);
  }

  /**
   * Contracts are removed from the cache if they are marked for removal and isn't used in the
   * deletion delay period.
   */
  @Test
  public void shouldRemoveAfterDelay() {
    BlockchainContract<StateSerializable, BinderEvent> contract = create();
    chainStateCache.markContractsForRemoval(Set.of(TestObjects.CONTRACT_PUB1));
    for (int i = 0; i < ChainStateCache.CONTRACT_REMOVAL_DELAY_IN_BLOCKS; i++) {
      chainStateCache.markContractsForRemoval(Set.of());
    }
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isNotNull()
        .isNotSameAs(contract);
  }

  private BlockchainContract<StateSerializable, BinderEvent> create() {
    BlockchainContract<StateSerializable, BinderEvent> contract =
        chainStateCache.get(TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash());
    chainStateCache.markContractsForRemoval(Set.of(TestObjects.CONTRACT_PUB1));
    Assertions.assertThat(contract).isNotNull();
    Assertions.assertThat(
            chainStateCache.get(
                TestObjects.CONTRACT_PUB1, publicBinder.hash(), contractBinder.hash()))
        .isSameAs(contract);
    return contract;
  }

  static byte[] loadWasmContract() {
    try (InputStream stream =
        ChainStateCacheTest.class.getResourceAsStream("wasm-invoker-test.wasm")) {
      return Objects.requireNonNull(stream).readAllBytes();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /** A test version of a raw binder. */
  public static final class RawBinder implements PubBinderContract<StateVoid> {

    public final byte[] bytes;

    /**
     * Raw binder is identified by the system via this constructor.
     *
     * @param bytes dummy byte array.
     */
    public RawBinder(byte[] bytes) {
      this.bytes = bytes;
    }

    @Override
    public Class<StateVoid> getStateClass() {
      return StateVoid.class;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> create(
        PubBinderContext pubBinderContext, byte[] bytes) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> invoke(
        PubBinderContext pubBinderContext, StateVoid stateVoid, byte[] bytes) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> callback(
        PubBinderContext pubBinderContext,
        StateVoid stateVoid,
        CallbackContext callbackContext,
        byte[] bytes) {
      return null;
    }
  }
}
