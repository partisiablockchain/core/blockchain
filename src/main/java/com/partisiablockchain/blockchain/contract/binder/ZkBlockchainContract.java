package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.blockchain.contract.BlockchainResult;
import com.partisiablockchain.blockchain.contract.CoreContractState.ContractSerialization;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.List;

/**
 * ZK contracts in the chain state.
 *
 * @param <T> the type of state
 */
public final class ZkBlockchainContract<T extends StateSerializable>
    implements BlockchainContract<T, BinderInteraction> {

  private final ZkBinderContract<T> binder;
  private final ContractSerialization<T> serialization;

  /**
   * Construct a new ZkBlockchainContract.
   *
   * @param binder the binder
   */
  public ZkBlockchainContract(ZkBinderContract<T> binder) {
    this.binder = binder;
    Class<T> stateClazz = binder.getStateClass();
    List<Class<?>> stateClazzTypeParameters = binder.getStateClassTypeParameters();
    Class<?>[] parameters = stateClazzTypeParameters.toArray(new Class<?>[0]);
    this.serialization = new ContractSerialization<>(stateClazz, parameters);
  }

  @Override
  public ZkBinderContract<T> getBinder() {
    return binder;
  }

  @Override
  public BlockchainResult<T, BinderInteraction> create(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result = binder.create(context, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public BlockchainResult<T, BinderInteraction> invoke(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result = binder.invoke(context, contract, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public BlockchainResult<T, BinderInteraction> callback(
      ExecutionContextTransaction executionContext,
      BlockchainContractContext contractContext,
      T contract,
      CallbackContext callbackContext,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(contractContext, executionContext);
    BinderResult<T, BinderInteraction> result =
        binder.callback(context, contract, callbackContext, rpc);
    return BlockchainResult.create(result);
  }

  @Override
  public ContractSerialization<T> getContractSerialization() {
    return serialization;
  }

  @Override
  public ContractUpgradePermit acquireUpgradePermit(
      BlockchainContractContext contractContext,
      ExecutionContextTransaction executionContext,
      T state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(contractContext, executionContext);
    return binder.acquireUpgradePermit(
        context, state, oldContractBytecodeIds, newContractBytecodeIds, rpc);
  }

  @Override
  public T upgradeState(
      BlockchainContractContext contractContext,
      ExecutionContextTransaction executionContext,
      StateAccessor oldState,
      ContractUpgradePermit permit,
      byte[] rpc) {
    ZkBinderContextImpl context = new ZkBinderContextImpl(contractContext, executionContext);
    return binder.upgradeState(context, oldState, permit, rpc);
  }
}
