package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventTransactionReturnValueTest extends CloseableTest {

  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();

  private BlockchainLedger ledger;

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    this.ledger = register(EventTransactionOnBlockchainCallbackTest.setupForTest(temporaryFolder));
  }

  @Test
  public void createSimpleEvents() {
    runTest(2);
  }

  @Test
  public void createManyEvents() {
    runTest(3);
  }

  private void runTest(int eventCount) {
    SignedTransaction deployEvent =
        createDeployPub(
            SafeDataOutputStream.serialize(
                safeDataOutputStream -> safeDataOutputStream.writeInt(eventCount)));
    ledger.addPendingTransaction(deployEvent);

    BlockchainLedgerTest.produce(ledger, deployEvent);

    BlockAndState latest = ledger.latest();

    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    EventTransactionOnBlockchainTest.printEventTree(deployEvent.identifier(), ledger, "");
    Assertions.assertThat(executionResults.size()).isEqualTo(1 + 2 + eventCount * 3 + 1);
    executionResults.values().forEach(v -> Assertions.assertThat(v).isTrue());
  }

  private long getNonce(BlockchainAddress account) {
    return ledger.latest().getState().getAccount(account).getNonce();
  }

  private static final byte[] jar = JarBuilder.buildJar(CalleeContract.class);

  private SignedTransaction createDeployPub(byte[] rpc) {
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;

    InteractWithContractTransaction deployTransaction =
        BlockchainLedgerTest.DeployContractInTest.create(
            TestObjects.CONTRACT_PUB2, binderJar, jar, rpc, 0);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 0),
            deployTransaction)
        .sign(producerKey, ledger.getChainId());
  }

  /** Test. */
  public static final class CalleeContract extends PubContract<StateVoid> {

    @Override
    public StateVoid onCreate(PubContractContext context, SafeDataInputStream rpc) {
      int count = rpc.readInt();
      context
          .getInvocationCreator()
          .invoke(context.getContractAddress())
          .withPayload(stream -> stream.writeInt(count))
          .send();
      return null;
    }

    @Override
    public StateVoid onInvoke(
        PubContractContext context, StateVoid state, SafeDataInputStream rpc) {
      int callAgain = rpc.readInt();
      if (callAgain == 0) {
        context.setResult(s -> s.writeInt(1));
      } else {
        EventManager manager = context.getRemoteCallsCreator();
        manager
            .invoke(context.getContractAddress())
            .withPayload(stream -> stream.writeInt(callAgain - 1))
            .send();
        manager.registerCallbackWithCostFromRemaining(EventCreator.EMPTY_RPC);
      }
      return null;
    }

    @Override
    public StateVoid onCallback(
        PubContractContext context,
        StateVoid state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      CallbackContext.ExecutionResult executionResult = callbackContext.results().get(0);
      SafeDataInputStream inputStream = executionResult.returnValue();
      int oneValue = inputStream.readInt();
      if (oneValue != 1) {
        throw new RuntimeException("Expected oneValue to be equal to 1!");
      }
      context.setResult(s -> s.writeInt(oneValue));
      return null;
    }
  }
}
