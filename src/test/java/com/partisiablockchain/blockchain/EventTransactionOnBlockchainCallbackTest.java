package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.thread.ExecutionFactoryFake;
import java.math.BigInteger;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Advanced integration test of the callback system.
 *
 * <p>These tests check that contracts can create complex chains of event groups, where the
 * callbacks are capable of triggering further event groups.
 */
public final class EventTransactionOnBlockchainCallbackTest extends CloseableTest {

  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();

  private BlockchainLedger ledger;

  /** Create {@link BlockchainLedger} for individual tests. */
  @BeforeEach
  public void setUp() {
    this.ledger = register(setupForTest(temporaryFolder));
  }

  /** Create {@link BlockchainLedger} for individual tests. */
  static BlockchainLedger setupForTest(Path temporaryFolder) {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    BlockChainTestNetwork network = new BlockChainTestNetwork();
    return BlockchainLedger.createForTest(
        rootDirectory,
        chainstate -> {
          StateHelper.initial(chainstate);
          byte[] jar = JarBuilder.buildJar(BlockchainLedgerTest.DeployContractInTest.class);

          BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
          CoreContractStateTest.createContract(
              chainstate, jar, deployPublicContract, TestContracts.CONTRACT_BINDER_SYSTEM);
          FeePluginHelper.mintGas(chainstate, deployPublicContract, 1_000_000);
          byte[] rpc = new byte[0];
          chainstate.setPlugin(
              FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
        },
        network,
        new DummyExecutableEventFinder(),
        ExecutionFactoryFake.create());
  }

  /**
   * Callback invocations can create chains of event groups, by creating new callback event groups.
   * In this case three chained event groups with two invocations each.
   */
  @Test
  public void callbackEventGroupsCanRunForFewRounds() {
    testCallbackEventGroups(2, 3);
  }

  /**
   * Callback invocations can create chains of event groups, by creating new callback event groups.
   * In this case seven chained event groups with three invocations each.
   */
  @Test
  public void callbackEventGroupsCanRunForManyRounds() {
    testCallbackEventGroups(3, 7);
  }

  /**
   * Contracts can create complex chains of event groups, where the callbacks are capable of
   * triggering event groups.
   *
   * @param numInvocationsPerEventGroup Number of invocations to create for each callback.
   * @param numCallbackRounds Number of rounds of callbacks to create.
   */
  private void testCallbackEventGroups(int numInvocationsPerEventGroup, int numCallbackRounds) {
    SignedTransaction deployEvent =
        deployTransactionForEventWithCallbackContract(
            numInvocationsPerEventGroup, numCallbackRounds);
    ledger.addPendingTransaction(deployEvent);

    BlockchainLedgerTest.produce(ledger, deployEvent);

    BlockAndState latest = ledger.latest();

    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    EventTransactionOnBlockchainTest.printEventTree(deployEvent.identifier(), ledger, "");
    int expectedCountedInvocationsOnceAllRoundsAreComplete =
        numInvocationsPerEventGroup * numCallbackRounds;
    assertThat(executionResults.size())
        .isEqualTo(
            1 + 2 + expectedCountedInvocationsOnceAllRoundsAreComplete * 2 + numCallbackRounds);

    BlockchainLedgerTest.produce(ledger);

    StateSerializable contractState = latest.getState().getContractState(TestObjects.CONTRACT_PUB2);
    assertThat(contractState).isNotNull();
    assertThat(contractState)
        .hasFieldOrPropertyWithValue(
            "expectedCountedInvocationsOnceAllRoundsAreComplete",
            numInvocationsPerEventGroup * (numCallbackRounds + 1));

    latest = ledger.latest();
    assertThat(ledger.getTransactionsForBlock(latest.getBlock())).hasSize(0);
  }

  private long getNonce(BlockchainAddress account) {
    return ledger.latest().getState().getAccount(account).getNonce();
  }

  /**
   * Create transaction for deploying the {@link EventWithCallbackContract}.
   *
   * @param numInvocationsPerEventGroup Number of invocations to create for each callback.
   * @param numCallbackRounds Number of rounds of callbacks to create.
   * @return Transactions for deploying and initializing {@link EventWithCallbackContract}.
   */
  private SignedTransaction deployTransactionForEventWithCallbackContract(
      int numInvocationsPerEventGroup, int numCallbackRounds) {
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    byte[] jar = JarBuilder.buildJar(EventWithCallbackContract.class);
    byte[] rpc =
        SafeDataOutputStream.serialize(
            new EventWithCallbackContract.CallbackCount(
                numInvocationsPerEventGroup, numCallbackRounds));
    InteractWithContractTransaction deployTransaction =
        BlockchainLedgerTest.DeployContractInTest.create(
            TestObjects.CONTRACT_PUB2, binderJar, jar, rpc, 0);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 0),
            deployTransaction)
        .sign(producerKey, ledger.getChainId());
  }

  /**
   * Contract for testing behaviour of events and related callbacks.
   *
   * <p>Contract creates several groups of events (each invokes the same contract). When every
   * invocation is complete, a callback is invoked to itself. The callback will then trigger the
   * next round of event groups. The last callback returns a value. This should be seen as a list of
   * events called depth first with callbacks serving as the return to the next element in the list
   * - which is selfpopulating.
   *
   * <p>The number of invocations and how many rounds to run for are tracked in {@link
   * CallbackCount}.
   */
  public static final class EventWithCallbackContract
      extends PubContract<EventWithCallbackContract.State> {

    @Override
    public State onCreate(PubContractContext context, SafeDataInputStream rpc) {
      final CallbackCount initialCallbackCount = CallbackCount.read(rpc);

      createNextInvocationsOrReturn(context, initialCallbackCount);
      final int expectedCountedInvocationsOnceAllRoundsAreComplete =
          initialCallbackCount.numInvocationsPerEventGroup()
              * (initialCallbackCount.callbackRoundsRemaining() + 1);
      return new State(0, expectedCountedInvocationsOnceAllRoundsAreComplete);
    }

    @Override
    public State onInvoke(PubContractContext context, State state, SafeDataInputStream rpc) {
      return new State(
          state.countedInvocations() + 1,
          state.expectedCountedInvocationsOnceAllRoundsAreComplete());
    }

    @Override
    public State onCallback(
        PubContractContext context,
        State state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      final CallbackCount callbackCount = CallbackCount.read(rpc);
      checkInvariantNumRecordedInvocations(callbackContext, callbackCount);
      checkInvariantContractOnPace(state, callbackCount);
      createNextInvocationsOrReturn(
          context,
          new CallbackCount(
              callbackCount.numInvocationsPerEventGroup(),
              callbackCount.callbackRoundsRemaining() - 1));
      return state;
    }

    /** Contract must record the correct number of invocations for each callback. */
    private static void checkInvariantNumRecordedInvocations(
        CallbackContext callbackContext, CallbackCount callbackCount) {
      if (callbackCount.numInvocationsPerEventGroup() != callbackContext.results().size()) {
        throw new RuntimeException(
            "Expecting no of numInvocationsPerEventGroup "
                + callbackCount.numInvocationsPerEventGroup()
                + "="
                + callbackContext.results().size());
      }
    }

    /** Contract must be on-pace for completing all the required callbacks. */
    private static void checkInvariantContractOnPace(State state, CallbackCount callbackCount) {
      if (callbackCount.numInvocationsPerEventGroup() * callbackCount.callbackRoundsRemaining()
              + state.countedInvocations()
          != state.expectedCountedInvocationsOnceAllRoundsAreComplete()) {
        final String computation =
            callbackCount.numInvocationsPerEventGroup()
                + "*"
                + callbackCount.callbackRoundsRemaining()
                + "+"
                + state.countedInvocations()
                + "="
                + state.expectedCountedInvocationsOnceAllRoundsAreComplete();
        throw new RuntimeException("Expecting " + computation);
      }
    }

    /**
     * Create the next round of invocations, or set the return value, depending upon whether the
     * current round is the last.
     *
     * <p>Creates {@link nextDef#numInvocationsPerEventGroup} number of invocations, or a single
     * return value.
     *
     * @param context Context to create invocations or returns in. Not nullable.
     * @param nextDef Definition of the next round of invocations. Not nullable.
     */
    private static void createNextInvocationsOrReturn(
        PubContractContext context, CallbackCount nextDef) {
      final EventManager callCreator = context.getRemoteCallsCreator();
      if (nextDef.callbackRoundsRemaining() > 0) {
        for (int i = 0; i < nextDef.numInvocationsPerEventGroup(); i++) {
          callCreator
              .invoke(context.getContractAddress())
              .withPayload(EventCreator.EMPTY_RPC)
              .sendFromContract();
        }
        callCreator.registerCallbackWithCostFromRemaining(nextDef);
      } else {
        context.setResult(s -> s.writeLong(1234));
      }
    }

    /**
     * State of test contract {@link EventWithCallbackContract}.
     *
     * @param countedInvocations Number of normal {@link #onInvoke}s that have been recorded by the
     *     contract.
     * @param expectedCountedInvocationsOnceAllRoundsAreComplete The number fo invocations that the
     *     contract must have registered once the contract have finished the last round of
     *     callbacks.
     */
    @Immutable
    public record State(
        int countedInvocations, int expectedCountedInvocationsOnceAllRoundsAreComplete)
        implements StateSerializable {}

    /**
     * Transient state of the callback rounds. Exists only in the callback RPC.
     *
     * @param numInvocationsPerEventGroup Number of invocations that will be created each callback.
     * @param callbackRoundsRemaining Number of callback rounds that are remaining.
     */
    public record CallbackCount(int numInvocationsPerEventGroup, int callbackRoundsRemaining)
        implements DataStreamSerializable {

      @Override
      public void write(SafeDataOutputStream stream) {
        stream.writeInt(numInvocationsPerEventGroup);
        stream.writeInt(callbackRoundsRemaining);
      }

      static CallbackCount read(SafeDataInputStream stream) {
        final int numInvocationsPerEventGroup = stream.readInt();
        final int callbackRoundsRemaining = stream.readInt();
        return new CallbackCount(numInvocationsPerEventGroup, callbackRoundsRemaining);
      }
    }
  }
}
