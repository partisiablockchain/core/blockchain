package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Result for an interaction with the account plugin. It exclusively contains either a success value
 * T, or an error value E.
 *
 * @param <T> type of the result either successful or error
 */
public final class PayServiceFeesResult<T> {

  private final T result;
  private final boolean couldCoverFee;

  private PayServiceFeesResult(T result, boolean couldCoverFee) {
    this.result = result;
    this.couldCoverFee = couldCoverFee;
  }

  /**
   * Create succeeded result.
   *
   * @param value inner result
   * @param <T> type of result
   * @return created succeeded result
   */
  public static <T> PayServiceFeesResult<T> coveredFee(T value) {
    return new PayServiceFeesResult<>(value, true);
  }

  /**
   * Create failed result.
   *
   * @param value inner result
   * @param <T> type of result
   * @return created failed result
   */
  public static <T> PayServiceFeesResult<T> failedToCoverFees(T value) {
    return new PayServiceFeesResult<>(value, false);
  }

  /**
   * Check if contract covered fee.
   *
   * @return true of contract covered fee, false otherwise
   */
  public boolean contractCoveredFee() {
    return couldCoverFee;
  }

  /**
   * Get result of paying service fees.
   *
   * @return result
   */
  public T getResult() {
    return result;
  }
}
