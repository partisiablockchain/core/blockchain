package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.FinalizationData;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.BlockResponse.BlockPair;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.BiConsumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PacketTest {

  private final BlockchainAddress account1 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress account2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");
  private final String chainId = UUID.randomUUID().toString();

  @Test
  public void parseTypeLegal() {
    Assertions.assertThat(Packet.Type.parseInt(0)).isSameAs(Packet.Type.TRANSACTION);
    Assertions.assertThat(Packet.Type.parseInt(1)).isSameAs(Packet.Type.BLOCK);
    Assertions.assertThat(Packet.Type.parseInt(2)).isSameAs(Packet.Type.BLOCK_REQUEST);
    Assertions.assertThat(Packet.Type.parseInt(3)).isSameAs(Packet.Type.BLOCK_RESPONSE);
    Assertions.assertThat(Packet.Type.parseInt(4)).isSameAs(Packet.Type.FINALIZATION);
    Assertions.assertThat(Packet.Type.parseInt(5)).isSameAs(Packet.Type.SYNC);
    Assertions.assertThat(Packet.Type.parseInt(6)).isSameAs(Packet.Type.EVENT);
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  @Test
  public void parseTypeIllegal() {
    Assertions.assertThatThrownBy(() -> Packet.Type.parseInt(42))
        .isInstanceOf(IndexOutOfBoundsException.class);
  }

  @Test
  public void readWriteTransaction() {
    SignedTransaction expected =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 0, 10),
                InteractWithContractTransaction.create(account2, new byte[] {0x22, 0x66, 0x11}))
            .sign(new KeyPair(), chainId);

    byte[] bytes = toBytes(expected, SignedTransaction::write);
    SignedTransaction actual =
        SafeDataInputStream.deserialize(stream -> SignedTransaction.read(chainId, stream), bytes);
    actual.checkValidity(1, address -> 1L);
    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void getType() {
    List<Packet.Type<?>> expected =
        List.of(
            Packet.Type.TRANSACTION,
            Packet.Type.BLOCK,
            Packet.Type.BLOCK_REQUEST,
            Packet.Type.BLOCK_RESPONSE,
            Packet.Type.FINALIZATION,
            Packet.Type.SYNC,
            Packet.Type.EVENT,
            Packet.Type.COMPRESSED_EVENT,
            Packet.Type.COMPRESSED_BLOCK_RESPONSE);
    for (int i = 0; i < expected.size(); i++) {
      Packet.Type<?> type = expected.get(i);
      Assertions.assertThat(type.getType()).isEqualTo(i);
      Assertions.assertThat(Packet.Type.parseInt(i)).isEqualTo(type);
    }
    Assertions.assertThatThrownBy(
            () -> Objects.requireNonNull(Packet.Type.parseInt(expected.size())))
        .isInstanceOf(IndexOutOfBoundsException.class);
  }

  @Test
  public void readWriteFinalization() {
    FinalizationData finalizationData = FinalizationData.create(new byte[2]);
    Packet<FinalizationData> packet = new Packet<>(Packet.Type.FINALIZATION, finalizationData);
    byte[] serialized = SafeDataOutputStream.serialize(packet::send);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(serialized);
    Assertions.assertThat(input.readUnsignedByte()).isEqualTo(4);
    Packet<FinalizationData> parsed = Packet.Type.FINALIZATION.parse(chainId, input);
    FinalizationData payload = parsed.getPayload();
    Assertions.assertThat(payload.getData()).isEqualTo(finalizationData.getData());
  }

  @Test
  public void readWriteSync() {
    Packet<SyncRequest> packet = SyncRequest.PACKET;
    byte[] serialized = SafeDataOutputStream.serialize(packet::send);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(serialized);
    Assertions.assertThat(input.readUnsignedByte()).isEqualTo(5);
    Packet<SyncRequest> parsed = Packet.Type.SYNC.parse(chainId, input);
    SyncRequest payload = parsed.getPayload();
    Assertions.assertThat(payload).isSameAs(SyncRequest.INSTANCE);
  }

  @Test
  public void readWriteEvent() {
    ExecutableEvent executableEvent = createEvent();
    StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            StateHelper.mutableFromPopulate(stateStorage, StateHelper::initial),
            "ChainId",
            "ShardId",
            executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    FloodableEvent event =
        FloodableEvent.create(
            executableEvent,
            new FinalBlock(
                new Block(10L, 1L, 1L, Block.GENESIS_PARENT, state.getHash(), List.of(), List.of()),
                new byte[0]),
            stateStorage);
    Packet<FloodableEvent> packet = new Packet<>(Packet.Type.EVENT, event);
    byte[] serialized = SafeDataOutputStream.serialize(packet::send);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(serialized);
    Assertions.assertThat(input.readUnsignedByte()).isEqualTo(6);
    Packet<FloodableEvent> parsed = Packet.Type.EVENT.parse(chainId, input);
    FloodableEvent payload = parsed.getPayload();
    Assertions.assertThat(payload.getExecutableEvent().identifier())
        .isEqualTo(event.getExecutableEvent().identifier());
  }

  private ExecutableEvent createEvent() {
    return new ExecutableEvent(
        "ShardId",
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            account1,
            10L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("DestinationShard", 27),
            0,
            1L));
  }

  @Test
  public void readWriteFulfillTransaction() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(
            BlockchainAddress.fromString("010101010101010101010101010101010101010101"),
            new byte[] {42, 43});

    SignedTransaction expected =
        SignedTransaction.create(new CoreTransactionPart(1, 1, 10), transaction)
            .sign(new KeyPair(), chainId);
    byte[] bytes = toBytes(expected, SignedTransaction::write);
    SignedTransaction actual =
        SafeDataInputStream.deserialize(stream -> SignedTransaction.read(chainId, stream), bytes);
    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void readWriteBlock() {
    SignedTransaction transaction1 = createSignedInteraction(1, account2);
    SignedTransaction transaction2 = createSignedInteraction(2, account1);

    Hash hash = TestObjects.EMPTY_HASH;
    Block expected =
        new Block(
            System.currentTimeMillis(),
            0,
            0,
            hash,
            hash,
            List.of(),
            List.of(transaction1.identifier(), transaction2.identifier()));

    byte[] bytes = toBytes(expected, Block::write);
    Block actual = SafeDataInputStream.deserialize(Block::read, bytes);
    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void readWriteBlockRequest() {
    BlockRequest expected = new BlockRequest(75);

    byte[] bytes = toBytes(expected, BlockRequest::write);
    BlockRequest actual = SafeDataInputStream.deserialize(BlockRequest::read, bytes);
    Assertions.assertThat(actual.getBlockTime()).isEqualTo(expected.getBlockTime());
  }

  @Test
  public void readWriteBlockResponse() {
    SignedTransaction transaction1 = createSignedInteraction(3, account2);
    SignedTransaction transaction2 = createSignedInteraction(5, account1);
    ExecutableEvent event = createEvent();

    Hash hash = TestObjects.EMPTY_HASH;
    Block block =
        new Block(
            System.currentTimeMillis(),
            0,
            0,
            hash,
            hash,
            List.of(event.identifier()),
            List.of(transaction1.identifier(), transaction2.identifier()));

    List<SignedTransaction> transactionList = List.of(transaction1, transaction2);
    BlockResponse expected =
        new BlockResponse(
            1,
            List.of(
                new BlockPair(
                    new FinalBlock(block, new byte[0]), transactionList, List.of(event), chainId)),
            chainId);

    byte[] bytes = toBytes(expected, BlockResponse::write);
    BlockResponse actual =
        SafeDataInputStream.deserialize(stream -> BlockResponse.read(chainId, stream), bytes);
    Assertions.assertThat(actual.getSenderBlockTime()).isEqualTo(1L);
    List<BlockPair> blocks = actual.getBlocks();
    Assertions.assertThat(blocks).hasSize(1);
    BlockPair actualBlockPair = blocks.get(0);
    Assertions.assertThat(actualBlockPair.getBlock().getBlock()).isEqualTo(block);
    actualBlockPair
        .getTransactions()
        .forEach(signedTransaction -> signedTransaction.checkValidity(10, address -> -1L));
    Assertions.assertThat(actualBlockPair.getTransactions()).hasSameElementsAs(transactionList);
    Assertions.assertThat(actualBlockPair.createExecutableBlock().isMissingTransactionOrEvents())
        .isFalse();
  }

  @Test
  public void readWriteBlockResponseSemiCompressed() {
    SignedTransaction transaction1 = createSignedInteraction(3, account2);
    SignedTransaction transaction2 = createSignedInteraction(5, account1);
    ExecutableEvent event = createEvent();

    Hash hash = TestObjects.EMPTY_HASH;
    Block block =
        new Block(
            System.currentTimeMillis(),
            0,
            0,
            hash,
            hash,
            List.of(event.identifier()),
            List.of(transaction1.identifier(), transaction2.identifier()));

    List<SignedTransaction> transactionList = List.of(transaction1, transaction2);
    BlockResponseSemiCompressed expected =
        new BlockResponseSemiCompressed(
            1,
            List.of(
                new BlockResponseSemiCompressed.BlockPairSemiCompressed(
                    new FinalBlock(block, new byte[0]),
                    transactionList,
                    List.of(
                        new SemiCompressedEvent(event),
                        new SemiCompressedEvent(
                            new SemiCompressedEvent.IdentifierAndShard(
                                event.identifier(), event.getOriginShard()))),
                    chainId)),
            chainId);

    byte[] bytes = toBytes(expected, BlockResponseSemiCompressed::write);
    BlockResponseSemiCompressed actual =
        SafeDataInputStream.deserialize(
            stream -> BlockResponseSemiCompressed.read(chainId, stream), bytes);
    Assertions.assertThat(actual.getSenderBlockTime()).isEqualTo(1L);
    List<BlockResponseSemiCompressed.BlockPairSemiCompressed> blocks = actual.getBlocks();
    Assertions.assertThat(blocks).hasSize(1);
    BlockResponseSemiCompressed.BlockPairSemiCompressed actualBlockPair = blocks.get(0);
    Assertions.assertThat(actualBlockPair.getBlock().getBlock()).isEqualTo(block);
    actualBlockPair
        .getTransactions()
        .forEach(signedTransaction -> signedTransaction.checkValidity(10, address -> -1L));
    Assertions.assertThat(actualBlockPair.getTransactions()).hasSameElementsAs(transactionList);
    Assertions.assertThat(actualBlockPair.getSemiCompressedEvents().get(0).isCompressed())
        .isFalse();
    Assertions.assertThat(
            actualBlockPair.getSemiCompressedEvents().get(0).getExecutableEvent().identifier())
        .isEqualTo(event.identifier());
    Assertions.assertThatThrownBy(
            () -> actualBlockPair.getSemiCompressedEvents().get(0).getIdentifierAndShard())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Event is not compressed");
    Assertions.assertThat(
            actualBlockPair.getSemiCompressedEvents().get(1).getIdentifierAndShard().identifier())
        .isEqualTo(event.identifier());
    Assertions.assertThatThrownBy(
            () -> actualBlockPair.getSemiCompressedEvents().get(1).getExecutableEvent())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Event is compressed");
  }

  @Test
  public void biFunction() {
    DataStreamSerializable dataStreamSerializable = safeDataOutputStream -> {};

    DataStreamSerializable dummy =
        Packet.Type.biFunction(safeDataInputStream -> dataStreamSerializable)
            .apply("Dummy", SafeDataInputStream.createFromBytes(new byte[0]));
    Assertions.assertThat(dummy).isSameAs(dataStreamSerializable);
  }

  private SignedTransaction createSignedInteraction(long nonce, BlockchainAddress to) {
    return SignedTransaction.create(
            new CoreTransactionPart(nonce, 0, 10),
            InteractWithContractTransaction.create(to, new byte[0]))
        .sign(new KeyPair(), chainId);
  }

  private <T> byte[] toBytes(T object, BiConsumer<T, SafeDataOutputStream> writer) {
    return SafeDataOutputStream.serialize(stream -> writer.accept(object, stream));
  }
}
