package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.thread.ExecutionFactoryFake;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ClientTest {

  private final List<Neighbour> connected = new ArrayList<>();
  private final Address first = new Address("localhost", 8080);
  private final Address myself = new Address("localhost", 6669);
  private Address toReturn = null;
  private long kademliaPicks = 0;
  private int connectedCalls = 0;
  private final Client client =
      new Client(
          () -> {
            kademliaPicks++;
            return toReturn;
          },
          null,
          myself,
          () -> connected,
          (address, socket) -> connectedCalls++,
          new ExecutionFactoryFake());

  @Test
  public void randomInterval() {
    long l = Client.randomInterval(100, 100);
    assertThat(l).isEqualTo(100);

    assertThat(Client.randomInterval(1000, 1500)).isBetween(1000L, 1500L);
  }

  @Test
  public void connectIfUnconnected() {
    List<Address> connectedAddress =
        connected.stream().map(Neighbour::getAddress).collect(Collectors.toList());
    assertThat(client.connectIfUnconnected(connectedAddress, null)).isFalse();
    assertThat(client.connectIfUnconnected(connectedAddress, first)).isTrue();
    assertThat(client.connectIfUnconnected(connectedAddress, first)).isFalse();
  }

  @Test
  public void shouldTryToPick20TimesWhenConnectingToMyself() {
    for (int i = 0; i < Client.MINIMUM_CONNECTIONS - 1; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, true));
    }
    toReturn = myself;
    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(20);
  }

  @Test
  public void shouldTryToPick20Times() {
    for (int i = 0; i < Client.MINIMUM_CONNECTIONS - 1; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, true));
    }

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(20);
  }

  @Test
  public void shouldPickOnceWhenInitiatingConnection() {
    for (int i = 0; i < Client.MINIMUM_CONNECTIONS - 1; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, true));
    }
    toReturn = new Address("localhost", 9999);

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(1);
  }

  @Test
  public void triesToFindAddressEvenWhenMinimumOutboundIsSatisfied() {
    for (int i = 0; i < Client.MINIMUM_OUTBOUND_CONNECTIONS - 1; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, true));
    }
    toReturn = new Address("localhost", 9999);

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(41);
  }

  @Test
  public void shouldNotPickWhenBothMinimumsAreSatisfied() {
    for (int i = 0; i < Client.MINIMUM_OUTBOUND_CONNECTIONS; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, true));
    }
    for (int i = Client.MINIMUM_OUTBOUND_CONNECTIONS; i < Client.MINIMUM_CONNECTIONS; i++) {
      connected.add(new Neighbour(new Address("localhost", 8080 + i), null, false));
    }
    toReturn = new Address("localhost", 9999);

    client.checkForConnect();

    assertThat(kademliaPicks).isEqualTo(0);
  }
}
