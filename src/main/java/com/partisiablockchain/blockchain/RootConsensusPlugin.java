package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin.BlockValidation;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataInputStream;

/** Consensus plugin allowing root to do everything. */
@Immutable
final class RootConsensusPlugin extends ConsensusPluginAccessor {

  RootConsensusPlugin() {}

  @Override
  public BlockValidation validateLocalBlock(FinalBlock block) {
    if (validateBlock(block)) {
      return BlockValidation.createAccepted(false);
    } else {
      return BlockValidation.createRejected();
    }
  }

  @Override
  public boolean validateExternalBlock(FinalBlock block) {
    return validateBlock(block);
  }

  private boolean validateBlock(FinalBlock block) {
    byte[] finalizationData = block.getFinalizationData();
    Signature signature = Signature.read(SafeDataInputStream.createFromBytes(finalizationData));
    BlockchainAddress signer = signature.recoverSender(block.getBlock().identifier());
    return signer != null && finalizationData.length == 65;
  }
}
