(module
  (type (;0;) (func (param i32 i32 i32 i32) (result i64)))
  (type (;1;) (func (param i32 i32 i32 i32)))
  (type (;2;) (func (param i32 i32 i32 i32 i32 i32) (result i64)))
  (func $init (type 0) (param i32 i32 i32 i32) (result i64)
    local.get 0
    i32.load
    local.set 0

    i32.const 42
    local.get 0
    i32.store

    i32.const 42
    local.get 1
    i32.store offset=4

    i32.const 42
    local.get 2
    i32.store offset=8

    i32.const 42
    local.get 3
    i32.store offset=12

    i64.const 29
    i64.const 32
    i64.shl

    i64.const 42
    i64.or
  )
  (func $action (type 2) (param i32 i32 i32 i32 i32 i32) (result i64)
    local.get 0
    i32.load
    local.set 0

    i32.const 42
    local.get 0
    i32.store

    i32.const 42
    local.get 1
    i32.store offset=4

    i32.const 42
    local.get 2
    i32.store offset=8

    i32.const 42
    local.get 3
    i32.store offset=12

    i32.const 42
    local.get 4
    i32.store offset=16

    i32.const 42
    local.get 5
    i32.store offset=20

    i64.const 29
    i64.const 32
    i64.shl

    i64.const 42
    i64.or
  )
  (func $noresult (type 1) (param i32 i32 i32 i32)
    nop
  )
  (memory 1)
  (export "init" (func $init))
  (export "action_a125a953" (func $action))
  (export "noresult" (func $noresult))
  (data (;0;) (i32.const 58) "fun with WASM")
)
