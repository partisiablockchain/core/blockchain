package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.flooding.BlockRequest;
import com.partisiablockchain.flooding.Connection;
import com.partisiablockchain.flooding.Packet;
import java.util.function.Consumer;
import java.util.function.LongSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class BlockRequester {

  private static final Logger logger = LoggerFactory.getLogger(BlockRequester.class);

  private Consumer<Packet<?>> network;
  private final int waitBetweenRequests;

  private long lastFetch;
  private long lastFetchTime;

  BlockRequester() {
    this(60_000);
  }

  BlockRequester(int waitBetweenRequests) {
    this.waitBetweenRequests = waitBetweenRequests;
  }

  void init(Consumer<Packet<?>> network) {
    this.network = network;
  }

  void requestBlock(long blockTime) {
    requestBlocks(null, blockTime);
  }

  synchronized void requestBlocks(Connection peer, long blockTime) {
    if (blockTime >= lastFetch + NetworkRequestHandler.MAX_BLOCKS
        || isAfter(System::currentTimeMillis, lastFetchTime + waitBetweenRequests)) {
      logger.info("Fetching block: {}", blockTime);
      Packet<BlockRequest> packet =
          new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(blockTime));
      if (peer != null) {
        peer.send(packet);
      } else {
        network.accept(packet);
      }
      lastFetchTime = System.currentTimeMillis();
      lastFetch = blockTime;
    }
  }

  synchronized void responseReceived(long blockTime) {
    if (lastFetch == blockTime) {
      lastFetchTime = 0;
    }
  }

  static boolean isAfter(LongSupplier now, long threshold) {
    return now.getAsLong() > threshold;
  }
}
