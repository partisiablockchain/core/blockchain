package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/** Cost of an executed transaction. */
public final class TransactionCost implements DataStreamSerializable {

  private final long allocatedForEvents;
  private final long cpu;
  private final Map<Hash, Long> networkFees;
  private final long remaining;
  private final long paidByContract;

  /**
   * Create a transaction cost.
   *
   * @param allocatedForEvents the amount allocated for other fees
   * @param cpu the cpu cost
   * @param remaining the storage cost
   * @param paidByContract the cost paid by the contract spawning the associated event
   * @param networkFees the network cost for each transaction. If the cost is associated with a
   *     {@link com.partisiablockchain.blockchain.transaction.SignedTransaction}, its network cost
   *     is included here.
   */
  public TransactionCost(
      long allocatedForEvents,
      long cpu,
      long remaining,
      long paidByContract,
      Map<Hash, Long> networkFees) {
    this.allocatedForEvents = allocatedForEvents;
    this.cpu = cpu;
    this.remaining = remaining;
    this.networkFees = new HashMap<>(networkFees);
    this.paidByContract = paidByContract;
  }

  @Override
  public void write(SafeDataOutputStream out) {
    out.writeLong(allocatedForEvents);
    out.writeLong(cpu);
    out.writeLong(remaining);
    out.writeLong(paidByContract);
    out.writeInt(networkFees.size());
    for (Entry<Hash, Long> entry : networkFees.entrySet()) {
      entry.getKey().write(out);
      out.writeLong(entry.getValue());
    }
  }

  /**
   * Read a {@link TransactionCost} object from a stream.
   *
   * @param in the input stream to read from
   * @return transaction cost from input stream
   */
  public static TransactionCost read(SafeDataInputStream in) {
    long allocatedCost = in.readLong();
    long cpuCost = in.readLong();
    long storageCost = in.readLong();
    long paidByContract = in.readLong();

    final var networkCost = new LinkedHashMap<Hash, Long>();
    int eventCount = in.readInt();
    for (int i = 0; i < eventCount; i++) {
      Hash identifier = Hash.read(in);
      long cost = in.readLong();
      networkCost.put(identifier, cost);
    }

    return new TransactionCost(allocatedCost, cpuCost, storageCost, paidByContract, networkCost);
  }

  /**
   * Get allocated cost.
   *
   * @return allocated gas
   */
  public long getAllocatedForEvents() {
    return allocatedForEvents;
  }

  /**
   * Get cpu cost.
   *
   * @return cost of cpu time.
   */
  public long getCpu() {
    return cpu;
  }

  /**
   * Get storage cost.
   *
   * @return cost of storage.
   */
  public long getRemaining() {
    return remaining;
  }

  /**
   * Get network costs.
   *
   * @return network cost for all transactions
   */
  public Map<Hash, Long> getNetworkCosts() {
    return Collections.unmodifiableMap(networkFees);
  }

  /**
   * Get combined network cost of spawned events.
   *
   * @return network cost
   */
  public long getTotalNetworkCost() {
    return networkFees.values().stream().reduce(0L, Long::sum);
  }

  /**
   * Get combination of all costs.
   *
   * @return sum of cpu cost and network costs.
   */
  public long getTotalCost() {
    return cpu + remaining + getTotalNetworkCost();
  }

  /**
   * The amount paid by the contract spawning the cost's associated event.
   *
   * @return the amount paid by the contract
   */
  public long getPaidByContract() {
    return paidByContract;
  }
}
