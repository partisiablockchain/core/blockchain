package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import com.secata.tools.immutable.FixedList;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** An executed transaction. */
public final class ExecutedTransaction implements DataStreamSerializable {

  private static final Logger logger = LoggerFactory.getLogger(ExecutedTransaction.class);

  private final Hash blockHash;
  private final ExecutableTransaction transaction;
  private final boolean executionSucceeded;
  private final FixedList<Hash> events;

  private ExecutedTransaction(
      ExecutableTransaction transaction,
      Hash blockHash,
      boolean executionSucceeded,
      FixedList<Hash> events) {
    this.transaction = transaction;
    this.blockHash = blockHash;
    this.executionSucceeded = executionSucceeded;
    this.events = events;
  }

  /**
   * Creates a new executed transaction, only for test.
   *
   * @param blockHash block hash this belongs to
   * @param transaction the actual signed transaction
   * @param executionSucceeded the execution status
   * @param events the identifiers of the events created by the execution
   * @return the created executed transaction
   */
  public static ExecutedTransaction create(
      Hash blockHash,
      ExecutableTransaction transaction,
      boolean executionSucceeded,
      FixedList<Hash> events) {
    return new ExecutedTransaction(transaction, blockHash, executionSucceeded, events);
  }

  /**
   * Reads the state from the stream.
   *
   * @param inputStream the origin
   * @param chainId the id of the chain
   * @param eventTransactionLookup function for looking up an event transaction from a hash
   * @return the object read
   */
  public static ExecutedTransaction read(
      SafeDataInputStream inputStream,
      String chainId,
      Function<Hash, ExecutableEvent> eventTransactionLookup) {
    Hash blockHash = Hash.read(inputStream);
    boolean isSignedTransaction = inputStream.readBoolean();
    ExecutableTransaction transaction;
    if (isSignedTransaction) {
      transaction = SignedTransaction.read(chainId, inputStream);
    } else {
      Hash identifier = Hash.read(inputStream);
      transaction = eventTransactionLookup.apply(identifier);
      if (transaction == null) {
        logger.warn("Unable to find event {} in storage", identifier);
        return null;
      }
    }
    boolean executionSucceeded = inputStream.readBoolean();
    FixedList<Hash> numberOfEvents =
        inputStream.readOptional(Hash.FIXED_LIST_SERIALIZER::readDynamic);
    return new ExecutedTransaction(transaction, blockHash, executionSucceeded, numberOfEvents);
  }

  /**
   * Writes the state into the stream.
   *
   * @param outputStream the destination
   */
  @Override
  public void write(SafeDataOutputStream outputStream) {
    blockHash.write(outputStream);
    boolean isSignedTransaction = transaction instanceof SignedTransaction;
    outputStream.writeBoolean(isSignedTransaction);
    if (isSignedTransaction) {
      transaction.write(outputStream);
    } else {
      transaction.identifier().write(outputStream);
    }
    outputStream.writeBoolean(executionSucceeded);
    outputStream.writeOptional(
        SafeListStream.primitive(Hash.FIXED_LIST_SERIALIZER::writeDynamic), events);
  }

  /**
   * Get actual signed transaction.
   *
   * @return executable signed transaction
   */
  public ExecutableTransaction getInner() {
    return transaction;
  }

  /**
   * Check if execution succeeded.
   *
   * @return true if succeeded, false otherwise
   */
  public boolean didExecutionSucceed() {
    return executionSucceeded;
  }

  /**
   * Get number of events created by the execution.
   *
   * @return number of events
   */
  public int numberOfEvents() {
    return events != null ? events.size() : 0;
  }

  /**
   * Get identifiers for events created by the execution.
   *
   * @return list of identifying hashes
   */
  public FixedList<Hash> getEvents() {
    return events;
  }

  /**
   * Get block hash.
   *
   * @return block hash
   */
  public Hash getBlockHash() {
    return blockHash;
  }

  @Override
  public String toString() {
    return "ExecutedTransaction{"
        + "blockHash="
        + blockHash
        + ", transaction="
        + transaction
        + ", events="
        + events
        + '}';
  }
}
