package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyAdditionalStatusProviders;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.NetworkNode.IncomingPacket;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.thread.ExecutionFactory;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class NetworkNodeTest extends CloseableTest {

  private final KeyPair keyPair1 = new KeyPair(BigInteger.ONE);
  private final KeyPair keyPair2 = new KeyPair(BigInteger.TWO);
  private NetworkNode networkNode;
  private FinalBlock block;
  private String chainId;
  private Connection firstConnection;
  private final Address address = new Address("localhost", 7777);
  private ExecutionFactoryFake executionFactory;

  /** Setup default. */
  @BeforeEach
  @SuppressWarnings("FutureReturnValueIgnored")
  public void setup() throws IOException {
    executionFactory = ExecutionFactoryFake.create();
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(temporaryFolder, executionFactory);
    register(blockchainTestHelper.blockchainLedger);
    block = blockchainTestHelper.createFinalBlock(new KeyPair(BigInteger.TEN), List.of());
    chainId = UUID.randomUUID().toString();
    networkNode = createTestNode(this);
    firstConnection =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            new KeyPair(BigInteger.ONE).getPublic(),
            ExecutionFactoryThreaded.create());
  }

  /** Creates a test version of their network node for use in unit test. */
  private NetworkNode createTestNode(CloseableTest registration) throws IOException {
    NetworkNode networkNode =
        new NetworkNode(
            ObjectCreator.noOpServer(registration),
            ObjectCreator.noOpClient(),
            chainId,
            keyPair1,
            FunctionUtility.noOpConsumer());
    registration.register(networkNode);
    return networkNode;
  }

  @Test
  public void networkNodeProvider() {
    AdditionalStatusProviders additionalStatusProviders = new DummyAdditionalStatusProviders();
    final NetworkNode node =
        createNetworkNode(
            "Shard1",
            new Address("my address", ObjectCreator.port()),
            new Address("other address", ObjectCreator.port()),
            new PacketReceiver(),
            new KeyPair(),
            additionalStatusProviders::register,
            executionFactory);
    register(node);

    // The node is not connected to anyone, so we would expect an empty map.
    List<AdditionalStatusProvider> gottenProviders = additionalStatusProviders.getProviders();
    assertThat(gottenProviders.size()).isEqualTo(1);
    assertThat(gottenProviders.get(0).getName()).isEqualTo("Shard1-FloodingNetworkStatus");
    assertThat(gottenProviders.get(0).getStatus()).isEqualTo(Map.of());

    Connection connectionOne =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            keyPair1.getPublic(),
            ExecutionFactoryThreaded.create());
    Connection connectionTwo =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            keyPair2.getPublic(),
            ExecutionFactoryThreaded.create());
    node.create(new Address("Some address", 9090), connectionOne, true);
    node.create(new Address("Some address", 9091), connectionTwo, false);

    gottenProviders = additionalStatusProviders.getProviders();
    assertThat(gottenProviders.size()).isEqualTo(1);
    assertThat(gottenProviders.get(0).getName()).isEqualTo("Shard1-FloodingNetworkStatus");
    assertThat(gottenProviders.get(0).getStatus())
        .isEqualTo(
            Map.of(
                "connection0",
                keyPair2.getPublic().toString() + " (in)",
                "connection1",
                keyPair1.getPublic().toString() + " (out)"));
    register(
        createNetworkNode(
            null,
            new Address("my address", ObjectCreator.port()),
            new Address("other address", ObjectCreator.port()),
            new PacketReceiver(),
            new KeyPair(),
            additionalStatusProviders::register,
            executionFactory));

    gottenProviders = additionalStatusProviders.getProviders();
    assertThat(gottenProviders.size()).isEqualTo(2);
    assertThat(gottenProviders.get(1).getName()).isEqualTo("Gov-FloodingNetworkStatus");
  }

  @Test
  public void closeConnectionOnReconnectAndInClose() {
    networkNode.create(address, firstConnection, false);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(firstConnection.isAlive()).isTrue();

    Connection secondConnection =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            keyPair1.getPublic(),
            ExecutionFactoryThreaded.create());
    networkNode.create(address, secondConnection, false);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(firstConnection.isAlive()).isFalse();
    assertThat(secondConnection.isAlive()).isTrue();

    assertThat(networkNode.getServer().isClosed()).isFalse();
    assertThat(networkNode.getClient().isClosed()).isFalse();
    networkNode.close();
    assertThat(networkNode.getServer().isClosed()).isTrue();
    assertThat(networkNode.getClient().isClosed()).isTrue();

    assertThat(networkNode.get(address)).isNull();
  }

  @Test
  public void reconnectingInboundConnection() {
    networkNode.create(address, firstConnection, false);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(networkNode.get(address).getConnection().getConnectionPublicKey())
        .isEqualTo(keyPair1.getPublic());
    Connection reconnectingConnection =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            keyPair1.getPublic(),
            ExecutionFactoryThreaded.create());
    networkNode.create(address, reconnectingConnection, false);
    assertThat(reconnectingConnection.isAlive()).isTrue();
    assertThat(firstConnection.isAlive()).isFalse();
  }

  @Test
  public void newOutboundConnectionWithInbound() {
    networkNode.create(address, firstConnection, false);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(networkNode.get(address).getConnection().getConnectionPublicKey())
        .isEqualTo(keyPair1.getPublic());
    Connection outboundConnection = createConnection();
    networkNode.create(address, outboundConnection, true);
    assertThat(firstConnection.isAlive()).isTrue();
    assertThat(outboundConnection.isAlive()).isTrue();
  }

  @Test
  public void newInboundConnectionWithOutbound() {
    networkNode.create(address, firstConnection, true);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(networkNode.get(address).getConnection().getConnectionPublicKey())
        .isEqualTo(keyPair1.getPublic());
    Connection outboundConnection = createConnection();
    networkNode.create(address, outboundConnection, false);
    assertThat(firstConnection.isAlive()).isTrue();
    assertThat(outboundConnection.isAlive()).isTrue();
  }

  private Connection createConnection() {
    return new Connection(
        (c, p) -> {},
        () -> {},
        ObjectCreator.blockingInput(this),
        ObjectCreator.blockingOutput(this),
        chainId,
        keyPair1.getPublic(),
        ExecutionFactoryFake.create());
  }

  @Test
  public void acceptNewNeighbour() {
    networkNode.create(address, firstConnection, true);
    assertThat(networkNode.get(address).isConnected()).isTrue();
    assertThat(networkNode.get(address).getConnection().getConnectionPublicKey())
        .isEqualTo(keyPair1.getPublic());
    Connection newNeighbour =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            keyPair2.getPublic(),
            ExecutionFactoryThreaded.create());
    networkNode.create(address, newNeighbour, false);
    Connection thirdNeighbour =
        new Connection(
            (c, p) -> {},
            () -> {},
            ObjectCreator.blockingInput(this),
            ObjectCreator.blockingOutput(this),
            chainId,
            new KeyPair(BigInteger.valueOf(3)).getPublic(),
            ExecutionFactoryThreaded.create());
    networkNode.create(address, thirdNeighbour, false);
    assertThat(firstConnection.isAlive()).isTrue();
    assertThat(newNeighbour.isAlive()).isTrue();
    assertThat(thirdNeighbour.isAlive()).isTrue();
  }

  @Test
  public void getLiveConnections() throws InterruptedException {
    final Address firstAddress = new Address("localhost", ObjectCreator.port());
    final Address secondAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        createNetworkNode(
            "Shard0",
            firstAddress,
            secondAddress,
            incoming1,
            new KeyPair(BigInteger.TEN),
            FunctionUtility.noOpConsumer(),
            ExecutionFactoryThreaded.create());
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        createNetworkNode(
            null,
            secondAddress,
            null,
            incoming2,
            keyPair2,
            FunctionUtility.noOpConsumer(),
            ExecutionFactoryThreaded.create());
    register(node2);

    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming1);
    assertThat(node1.getConnectedNeighbours().size()).isEqualTo(1);
    assertThat(node1.getLiveConnections().size()).isEqualTo(1);
    node2.close();
    Thread.sleep(100);
    assertThat(node1.getLiveConnections().size()).isEqualTo(0);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void queueOutgoingNonFlood() {
    Address firstAddress = new Address("localhost", ObjectCreator.port());
    Address secondAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        new NetworkNode(
            new NetworkConfig(secondAddress, FunctionUtility.nullSupplier(), firstAddress),
            chainId,
            "Shard1",
            keyPair2,
            incoming2,
            FunctionUtility.noOpConsumer(),
            ExecutionFactoryThreaded.create());
    register(node2);

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        new NetworkNode(
            new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), null),
            chainId,
            "Shard1",
            keyPair1,
            incoming1,
            FunctionUtility.noOpConsumer(),
            ExecutionFactoryThreaded.create());
    register(node1);
    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming1);

    assertThat(node1.getLiveConnections().get(0).getConnectionPublicKey())
        .isEqualTo(keyPair2.getPublic());
    assertThat(node2.getLiveConnections().get(0).getConnectionPublicKey())
        .isEqualTo(keyPair1.getPublic());
    long blockTime = 7;
    node1.sendToAny(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(blockTime)));

    IncomingPacket nextPacket = incoming2.next();
    assertThat(nextPacket.getPacket().getType()).isEqualTo(Packet.Type.BLOCK_REQUEST);
    Packet<BlockRequest> packet = (Packet<BlockRequest>) nextPacket.getPacket();
    assertThat(packet.getPayload().getBlockTime()).isEqualTo(blockTime);
  }

  @Test
  void emptyNode() {
    networkNode.sendToAny(new Packet<>(Packet.Type.BLOCK_REQUEST, new BlockRequest(0)));
  }

  @Test
  public void queueOutgoingFlood() {
    final Address firstAddress = new Address("localhost", ObjectCreator.port());
    final Address secondAddress = new Address("localhost", ObjectCreator.port());
    final Address thirdAddress = new Address("localhost", ObjectCreator.port());

    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        createNetworkNode(
            firstAddress, secondAddress, incoming1, keyPair1, ExecutionFactoryThreaded.create());
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        createNetworkNode(
            secondAddress, firstAddress, incoming2, keyPair2, ExecutionFactoryThreaded.create());
    register(node2);

    // Wait for sync packets
    waitForSync(incoming2);
    waitForSync(incoming2);
    waitForSync(incoming1);

    Packet<FinalBlock> sentPacket = new Packet<>(Packet.Type.BLOCK, block);
    node1.sendToAll(sentPacket);

    waitForAndReceiveBlock(incoming2);

    PacketReceiver incoming3 = new PacketReceiver();
    final NetworkNode node3 =
        createNetworkNode(thirdAddress, firstAddress, incoming3, new KeyPair(), executionFactory);
    register(node3);

    node1.sendToAll(sentPacket);
    assertThatThrownBy(() -> waitForAndReceiveBlock(incoming3)).isInstanceOf(AssertionError.class);
  }

  @Test
  public void connectedNodes() {
    Address firstAddress = new Address("localhost", ObjectCreator.port());
    Address secondAddress = new Address("localhost", ObjectCreator.port());
    PacketReceiver incoming1 = new PacketReceiver();
    final NetworkNode node1 =
        createNetworkNode(
            firstAddress, secondAddress, incoming1, keyPair1, ExecutionFactoryThreaded.create());
    register(node1);
    PacketReceiver incoming2 = new PacketReceiver();
    final NetworkNode node2 =
        createNetworkNode(
            secondAddress, firstAddress, incoming2, keyPair2, ExecutionFactoryThreaded.create());
    register(node2);

    // Wait for sync packets
    waitForSync(incoming1);
    waitForSync(incoming2);
    // Wait twice, since syncs are sent both for established server and client connections
    waitForSync(incoming1);
    waitForSync(incoming2);

    // The next two lines ensures that nothing is left in the network nodes, since we can
    // send directly two and from
    sendBlock(node1, incoming2);
    sendBlock(node2, incoming1);
  }

  private NetworkNode createNetworkNode(
      Address firstAddress,
      Address secondAddress,
      PacketReceiver incoming,
      KeyPair keyPair,
      ExecutionFactory executionFactory) {
    return createNetworkNode(
        chainId,
        firstAddress,
        secondAddress,
        incoming,
        keyPair,
        FunctionUtility.noOpConsumer(),
        executionFactory);
  }

  private NetworkNode createNetworkNode(
      String shard,
      Address firstAddress,
      Address secondAddress,
      PacketReceiver incoming,
      KeyPair keyPair,
      Consumer<AdditionalStatusProvider> register,
      ExecutionFactory executionFactory1) {
    return new NetworkNode(
        new NetworkConfig(firstAddress, FunctionUtility.nullSupplier(), secondAddress),
        chainId,
        shard,
        keyPair,
        incoming,
        register,
        executionFactory1);
  }

  @Test
  public void handshakeFails() throws IOException {
    Address toConnect = new Address("localhost", 4578);
    Socket socket = Mockito.mock(Socket.class);
    Mockito.when(socket.getInputStream()).thenThrow(new IOException("69"));

    networkNode.setClientConnectedAddress(toConnect, socket);
    assertThat(networkNode.get(toConnect)).isNull();

    networkNode.setServerConnectedAddress(toConnect, socket);
    assertThat(networkNode.get(toConnect)).isNull();
  }

  private void waitForSync(PacketReceiver receiver) {
    assertThat(receiver.next().getPacket().getPayload()).isInstanceOf(SyncRequest.class);
  }

  private void sendBlock(NetworkNode sender, PacketReceiver packetReceiver) {
    sender.sendToAll(new Packet<>(Packet.Type.BLOCK, block));

    waitForAndReceiveBlock(packetReceiver);
  }

  private void waitForAndReceiveBlock(PacketReceiver packetReceiver) {
    Packet<?> packet = packetReceiver.next().getPacket();
    assertThat(packet.getPayload()).isInstanceOf(FinalBlock.class);
    FinalBlock response = (FinalBlock) packet.getPayload();
    assertThat(response.getBlock()).isEqualTo(block.getBlock());
  }

  private static final class PacketReceiver implements Consumer<NetworkNode.IncomingPacket> {

    private final LinkedBlockingQueue<IncomingPacket> packets = new LinkedBlockingQueue<>();

    private PacketReceiver() {}

    @Override
    public void accept(IncomingPacket incomingPacket) {
      packets.add(incomingPacket);
    }

    private IncomingPacket next() {
      IncomingPacket next = ExceptionConverter.call(() -> packets.poll(2, TimeUnit.SECONDS));
      assertThat(next)
          .describedAs("Expected packet was not received by packet receiver")
          .isNotNull();
      return next;
    }
  }
}
