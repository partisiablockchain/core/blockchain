package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.ContractStateTest;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.TestContracts;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.TestContractByInvocation;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SignedTransactionTest {

  private final String chainId = UUID.randomUUID().toString();
  private final KeyPair keyPairForEmpty = new KeyPair(BigInteger.valueOf(2));
  private final BlockchainAddress emptyAccount = keyPairForEmpty.getPublic().createAddress();
  private final KeyPair signer = new KeyPair(BigInteger.valueOf(27L));
  private final BlockchainAddress account = signer.getPublic().createAddress();
  private final BlockchainAddress contract = TestObjects.CONTRACT_PUB1;

  @Test
  public void equals() {
    EqualsVerifier.forClass(SignedTransaction.class)
        .withNonnullFields("hash", "signature")
        .withOnlyTheseFields("hash", "signature")
        .verify();
  }

  @Test
  public void unrecoverableSenderInSignature() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 10, Long.MAX_VALUE), createInteraction())
            .withSignature(
                Signature.fromString(
                    "03fec44de44581bddd95e5a13cc612026ed1a07f9de5155f586be85fb329800"
                        + "0000ec44de44581bddd95e5a13cc612026ed1a07f9de5155f586be85fb329800701"),
                chainId);
    Assertions.assertThat(transaction.isSignatureValid()).isFalse();
    Assertions.assertThat(transaction.checkValidity(1, address -> 1L)).isFalse();

    byte[] serialize = SafeDataOutputStream.serialize(transaction::write);
    Assertions.assertThatThrownBy(
            () -> SignedTransaction.read(chainId, SafeDataInputStream.createFromBytes(serialize)))
        .hasMessage("Reading a invalid signed transaction - who created this transaction?");
  }

  @Test
  public void validityInTime() {
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 25 + 10, Long.MAX_VALUE), createInteraction())
            .sign(signer, chainId);

    Assertions.assertThat(transaction.checkValidity(24, a -> 1L)).isTrue();
    Assertions.assertThat(transaction.checkValidity(25, a -> 1L)).isTrue();
    Assertions.assertThat(transaction.checkValidity(34, a -> 1L)).isTrue();
    Assertions.assertThat(transaction.checkValidity(35, a -> 1L)).isFalse();
  }

  @Test
  public void invalidTransaction() {
    int cost = 500_000;
    KeyPair anyOtherAccount = new KeyPair(BigInteger.TEN);
    BlockchainAddress account = anyOtherAccount.getPublic().createAddress();
    MutableChainState state =
        StateHelper.initialWithAdditions(
            builder -> {
              builder.createAccount(account);
              FeePluginHelper.mintGas(builder, account, cost);
            });
    executeFailing(cost, anyOtherAccount, state);

    Assertions.assertThat(FeePluginHelper.balance(state, account)).isEqualTo(cost);
  }

  private void executeFailing(long cost, KeyPair anyOtherAccount, MutableChainState state) {

    byte[] binderJar = new byte[32];
    byte[] contractJar = new byte[32];
    byte[] rpc = new byte[0];
    CreateContractTransaction transaction =
        CreateContractTransaction.create(
            TestObjects.CONTRACT_PUB1, binderJar, contractJar, new byte[0], rpc);
    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                anyOtherAccount.getPublic().createAddress(),
                cost,
                transaction,
                new ShardRoute("destinationShard", 27),
                0,
                1L));
    ExecutionContext context = ExecutionContext.init(chainId, null, 9L, 1L, 0, 0);
    boolean executed = !context.execute(state, event).events().isEmpty();
    Assertions.assertThat(executed).isFalse();
  }

  @Test
  public void cannotSendAnyTransactionWithDebt() {
    ChainState state =
        StateHelper.createState(
            Map.of(account, -1L, TestObjects.CONTRACT_PUB1, 1000L),
            Map.of(TestObjects.CONTRACT_PUB1, ContractStateTest.create(new byte[0])),
            emptyAccount);
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 10), transaction)
            .sign(signer, chainId);

    Assertions.assertThat(signedTransaction.canCoverFee(state, 0)).isFalse();
  }

  @Test
  public void missingSender() {
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 10L, 10), transaction)
            .sign(new KeyPair(), chainId);

    boolean valid = signedTransaction.checkValidity(1L, address -> null);

    Assertions.assertThat(valid).isFalse();
  }

  @Test
  public void wrongNonce() {
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(2L, 10L, 10), transaction)
            .sign(signer, chainId);

    boolean valid = signedTransaction.checkValidity(1L, address -> 1L);
    Assertions.assertThat(valid).isFalse();
  }

  @Test
  public void checkToString() {
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 10), transaction)
            .sign(signer, chainId);

    String string = signedTransaction.toString();
    Assertions.assertThat(string)
        .contains("SignedTransaction")
        .contains("transaction=" + signedTransaction.identifier())
        .contains("core=CoreTransactionPart")
        .contains("nonce=1")
        .contains("validToTime=1");
  }

  @Test
  public void notEnoughForFee() {
    ChainState state =
        StateHelper.initialWithAdditions(
            mutable -> {
              CoreContractStateTest.createContract(
                  mutable,
                  JarBuilder.buildJar(TestContractByInvocation.class),
                  contract,
                  TestContracts.CONTRACT_BINDER_PUBLIC);
              mutable.createAccount(account);
              FeePluginHelper.mintGas(mutable, account, 10_000L);
            });
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 10), transaction)
            .sign(signer, chainId);

    boolean canCoverFee = signedTransaction.canCoverFee(state, 0);
    Assertions.assertThat(canCoverFee).isFalse();
  }

  @Test
  public void zeroFee() {
    MutableChainState state =
        StateHelper.initialWithAdditions(
            mutable -> {
              CoreContractStateTest.createContract(
                  mutable,
                  JarBuilder.buildJar(TestContractByInvocation.class),
                  contract,
                  TestContracts.CONTRACT_BINDER_PUBLIC);
              mutable.createAccount(account);
              FeePluginHelper.mintGas(mutable, account, 10_000L);
            });
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 0), transaction)
            .sign(signer, chainId);
    boolean canCoverFee = signedTransaction.canCoverFee(state, 1);
    Assertions.assertThat(canCoverFee).isTrue();
  }

  @Test
  public void hashTest() {
    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 10), transaction)
            .sign(signer, "ChainId");

    Assertions.assertThat(signedTransaction.identifier().toString())
        .isEqualTo("a78602b6c5196a4ef616f35a2561657c92f08add3c622fdb8d4517454c7dc4ca");
  }

  private InteractWithContractTransaction createInteraction() {
    return InteractWithContractTransaction.create(contract, new byte[0]);
  }

  @Test
  public void select() {
    Assertions.assertThat(SignedTransaction.select(Stream.of((ExecutableTransaction) null)))
        .hasSize(0);

    InteractWithContractTransaction transaction = createInteraction();
    SignedTransaction signedTransaction =
        SignedTransaction.create(CoreTransactionPart.create(1L, 1L, 10), transaction)
            .sign(signer, "ChainId");
    Assertions.assertThat(SignedTransaction.select(Stream.of(signedTransaction)))
        .hasSize(1)
        .containsExactly(signedTransaction);
  }
}
