package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyAdditionalStatusProviders;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.ZkTestBinder;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.PubTestContract;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.TestContractByInvocation;
import com.partisiablockchain.blockchain.contract.ZkTestContract;
import com.partisiablockchain.blockchain.contract.binder.ZkBlockchainContract;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.BlockRequest;
import com.partisiablockchain.flooding.BlockResponseSemiCompressed;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.flooding.Packet;
import com.partisiablockchain.flooding.SemiCompressedEvent;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.BlockTimeStorage;
import com.partisiablockchain.storage.BlockchainStorage;
import com.partisiablockchain.storage.BlockchainStorage.ValidBlock;
import com.partisiablockchain.storage.BlockchainStorageTest;
import com.partisiablockchain.storage.ExecutableBlock;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.thread.ExecutionFactoryFake;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import com.secata.tools.thread.TestExecutionQueue;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Testing of {@link BlockchainLedger}. */
public final class BlockchainLedgerTest extends CloseableTest {

  private static final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private static final KeyPair rootKey = new KeyPair(BigInteger.ONE);
  private static final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();
  private static final KeyPair secondKey = new KeyPair(BigInteger.TEN);
  private static final BlockchainAddress secondAccount = secondKey.getPublic().createAddress();
  private static final KeyPair thirdKey = new KeyPair(BigInteger.valueOf(0xAA11BB55FFL));
  private static final BlockchainAddress thirdAccount = thirdKey.getPublic().createAddress();

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private final Map<MemoryStorage.PathAndHash, byte[]> memoryStorage = new ConcurrentHashMap<>();
  private final Random random = new Random();
  private RootDirectory rootDirectory;
  private BlockchainLedger blockchainLedger;
  private String chainId;
  private final KeyPair signer = new KeyPair(BigInteger.ONE);
  private final DummyAdditionalStatusProviders additionalStatusProviders =
      new DummyAdditionalStatusProviders();
  private DummyExecutableEventFinder finder;

  private ExecutionFactoryFake executionFactory;

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    finder = new DummyExecutableEventFinder();
    rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile(), memoryStorage);
    executionFactory = ExecutionFactoryFake.create();
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                this::bootstrapDeployment,
                network,
                finder,
                additionalStatusProviders,
                executionFactory));
    chainId = blockchainLedger.getChainId();
  }

  private void bootstrapDeployment(MutableChainState chainstate) {
    StateHelper.initial(chainstate);
    byte[] jar = JarBuilder.buildJar(DeployContractInTest.class);

    BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
    CoreContractStateTest.createContract(
        chainstate, jar, deployPublicContract, TestContracts.CONTRACT_BINDER_SYSTEM);
  }

  private SignedTransaction createInteraction(
      BlockchainAddress contractAddress, Consumer<SafeDataOutputStream> consumer) {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    SafeDataOutputStream stream = new SafeDataOutputStream(output);
    consumer.accept(stream);
    byte[] payload = output.toByteArray();

    SignedTransaction interaction =
        SignedTransaction.create(
                CoreTransactionPart.create(getNonce(secondAccount), Long.MAX_VALUE / 10, 10_000),
                InteractWithContractTransaction.create(contractAddress, payload))
            .sign(secondKey, chainId);
    assertThat(blockchainLedger.addPendingTransaction(interaction)).isTrue();
    return interaction;
  }

  @Test
  public void triggerStorageFees() {
    int rounds = 5;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("020" + rounds + "00000000000000000000000000000000000002");

    byte[] contractInfo = JarBuilder.buildJar(TestContractByInvocation.class);
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  CoreContractStateTest.createContract(
                      mutable, contractInfo, contractAddress, TestContracts.CONTRACT_BINDER_PUBLIC);
                  mutable.createAccount(secondAccount);
                  FeePluginHelper.mintGas(mutable, secondAccount, 10_000);
                  FeePluginHelper.mintGas(mutable, contractAddress, 5_000);
                  FeePluginHelper.enableStorageFees(mutable, contractAddress, 0);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    int millisHour = 1_000 * 60 * 60;

    SignedTransaction interaction =
        createInteraction(
            contractAddress,
            s -> {
              s.writeBoolean(false);
              s.writeInt(1);
              s.writeLong(millisHour + blockchainLedger.getLatestBlock().getProductionTime());
            });

    assertBalance(secondAccount, 10_000);
    produce(1, blockchainLedger, interaction);
    ExecutableEvent eventTransaction =
        blockchainLedger.getEventTransaction(latestEvent(interaction));
    InnerTransaction inner = (InnerTransaction) eventTransaction.getEvent().getInner();
    assertThat(inner.getCost()).isEqualTo(10_000 - /* network cost */ 166);
    produce(millisHour - 1, blockchainLedger);
    assertBalance(secondAccount, 0);

    assertBalance(contractAddress, 15_000 - /* storageFee */ contractInfo.length - 2);
    produce(millisHour, blockchainLedger);
    while (blockchainLedger.getBlockTime() < rounds) {
      produce();
    }
    assertThat(blockchainLedger.latest().getState().getAccount(contractAddress)).isNull();
    assertThat(blockchainLedger.latest().getState().getCoreContractState(contractAddress)).isNull();
  }

  @Test
  public void getLatestAppend() {
    long before = System.currentTimeMillis() - 1;
    blockchainLedger.setLatestAppend(before);
    long latestAppend = blockchainLedger.getLatestAppend();
    assertThat(latestAppend).isEqualTo(before);
    produce();
    assertThat(blockchainLedger.getLatestAppend()).isGreaterThan(latestAppend);
  }

  @Test
  public void failsToPayStorageFeeForContractWhileInteracting() {
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;

    byte[] jar = JarBuilder.buildJar(TestContractByInvocation.class);
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  CoreContractStateTest.createContract(
                      mutable, jar, contractAddress, TestContracts.CONTRACT_BINDER_PUBLIC);
                  mutable.createAccount(secondAccount);
                  FeePluginHelper.mintGas(mutable, secondAccount, 20_000);
                  FeePluginHelper.mintGas(mutable, contractAddress, 0);
                  FeePluginHelper.enableStorageFees(
                      mutable, contractAddress, System.currentTimeMillis());
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    SignedTransaction interaction1 =
        createInteraction(
            contractAddress,
            s -> {
              s.writeBoolean(false);
              s.writeInt(1);
              s.writeLong(blockchainLedger.getLatestBlock().getProductionTime());
            });
    SignedTransaction interaction2 =
        createInteraction(
            contractAddress,
            s -> {
              s.writeBoolean(false);
              s.writeInt(1);
              s.writeLong(blockchainLedger.getLatestBlock().getProductionTime());
            });

    int millisHour = 1_000 * 60 * 60;
    produce(millisHour, blockchainLedger, interaction1, interaction2);
    produce();

    assertThat(blockchainLedger.latest().getState().getAccount(contractAddress)).isNull();
    assertThat(blockchainLedger.latest().getState().getCoreContractState(contractAddress)).isNull();
  }

  @Test
  public void updateEventNotNull() {
    assertThat(blockchainLedger.latest().getContracts()).isNotNull();
  }

  @Test
  public void produceWithMissingContract() {
    long nonceBefore = getNonce(producerAccount);
    SignedTransaction interact =
        SignedTransaction.create(
                CoreTransactionPart.create(nonceBefore, System.currentTimeMillis() * 10, 100_000),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_BOOTSTRAP, new byte[0]))
            .sign(producerKey, chainId);
    blockchainLedger.addPendingTransaction(interact);
    produce(interact);
    assertThat(blockchainLedger.latest().getState().lookupNonce(producerAccount))
        .isEqualTo(nonceBefore + 1);
  }

  @Test
  public void isValidFromBlockTime() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  mutable.createAccount(secondAccount);
                  FeePluginHelper.mintGas(mutable, secondAccount, 10_000);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    for (int i = 0; i < 10; i++) {
      produce();
    }
    Block latestBlock = blockchainLedger.getLatestBlock();
    long productionTime = latestBlock.getProductionTime() - 10;
    assertThat(
            blockchainLedger.addPendingTransaction(
                createValidity(productionTime, 10, secondKey, getNonce(secondAccount))))
        .isFalse();
    assertThat(
            blockchainLedger.addPendingTransaction(
                createValidity(productionTime + 1, 10, secondKey, getNonce(secondAccount))))
        .isTrue();
    assertThat(
            blockchainLedger.addPendingTransaction(
                createValidity(productionTime + 1, 10, thirdKey, getNonce(secondAccount))))
        .isFalse();
  }

  @Test
  public void isValidFromAccountBalance() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  mutable.createAccount(secondAccount);
                  mutable.createAccount(thirdAccount);
                  FeePluginHelper.mintGas(mutable, secondAccount, 9_999);
                  FeePluginHelper.mintGas(mutable, thirdAccount, 10_000);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    assertThat(
            blockchainLedger.addPendingTransaction(
                createValidity(0, 1, secondKey, getNonce(secondAccount))))
        .isFalse();

    assertThat(
            blockchainLedger.addPendingTransaction(
                createValidity(0, 1, thirdKey, getNonce(thirdAccount))))
        .isTrue();
  }

  @Test
  public void selectExpired() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  mutable.createAccount(secondAccount);
                  mutable.createAccount(thirdAccount);
                  FeePluginHelper.mintGas(mutable, secondAccount, Long.MAX_VALUE);
                  FeePluginHelper.mintGas(mutable, thirdAccount, Long.MAX_VALUE);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    produce();
    assertThat(blockchainLedger.getPendingEvents()).isEmpty();

    blockchainLedger.addPendingTransaction(
        createValidity(0, 1, secondKey, getNonce(secondAccount)));

    SignedTransaction produceMe = createValidity(1, 1000, thirdKey, getNonce(thirdAccount));
    blockchainLedger.addPendingTransaction(produceMe);

    blockchainLedger.addPendingTransaction(
        createValidity(1, 1000, thirdKey, getNonce(thirdAccount)));

    produce(produceMe);
    assertThat(blockchainLedger.getPending()).isEmpty();
    assertThat(blockchainLedger.getPendingTransactions()).isEmpty();
    assertThat(blockchainLedger.getPendingEvents()).hasSize(0);

    SignedTransaction expected = createValidity(1, 2, secondKey, getNonce(secondAccount));
    blockchainLedger.addPendingTransaction(expected);

    assertThat(blockchainLedger.getPending()).containsEntry(expected.identifier(), expected);
    assertThat(blockchainLedger.getPendingTransactions()).containsExactly(expected);

    produce();

    expected = createValidity(1, 1000, secondKey, getNonce(secondAccount));
    blockchainLedger.addPendingTransaction(expected);

    assertThat(blockchainLedger.getPending())
        .hasSize(1)
        .containsEntry(expected.identifier(), expected);

    produce();
    assertThat(blockchainLedger.getPendingEvents()).isEmpty();

    produce(expected);
    assertThat(blockchainLedger.getPending()).isEmpty();
  }

  private long getNonce(BlockchainAddress account) {
    return blockchainLedger.latest().getState().getAccount(account).getNonce();
  }

  private SignedTransaction createValidity(
      long validFromTime, int validityPeriod, KeyPair key, long nonce) {
    byte[] payload = new byte[10];
    random.nextBytes(payload);

    return SignedTransaction.create(
            CoreTransactionPart.create(nonce, validFromTime + validityPeriod, 10_000),
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, payload))
        .sign(key, chainId);
  }

  @Test
  public void ledgerProviderLatestBlockTime() {
    long blockTime = blockchainLedger.getBlockTime();
    Map<String, String> status = additionalStatusProviders.getProviders().get(0).getStatus();
    long providerBlockTime = Long.parseLong(status.get("latestBlockTime"));
    assertThat(blockTime).isEqualTo(providerBlockTime);
    assertThat(additionalStatusProviders.getProviders().get(0).getName())
        .isEqualTo("Gov-LedgerStatus");
  }

  @Test
  public void pastBlock() {
    Block before = blockchainLedger.getLatestBlock();
    Block block =
        new Block(
            System.currentTimeMillis(),
            0,
            before.getBlockTime(),
            before.identifier(),
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of());
    FinalBlock finalBlock = appendFinal(blockchainLedger, block);
    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(before);

    blockchainLedger.appendBlock(new ExecutableBlock(finalBlock, List.of(), List.of()));
    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(before);
  }

  @Test
  public void getLatestProductionTime() {
    long latestProductionTime = blockchainLedger.getLatestProductionTime();
    produce(10, blockchainLedger);
    assertThat(blockchainLedger.getLatestProductionTime())
        .isEqualTo(blockchainLedger.getLatestBlock().getProductionTime())
        .isEqualTo(latestProductionTime + 10);
  }

  private static FinalBlock appendFinal(BlockchainLedger blockchainLedger, Block block) {
    return appendFinal(blockchainLedger, block, producerKey);
  }

  private static FinalBlock appendFinal(
      BlockchainLedger blockchainLedger, Block block, KeyPair producerKey) {
    FinalBlock finalBlock = createFinalBlock(block, producerKey);
    blockchainLedger.appendBlock(finalBlock);
    return finalBlock;
  }

  private static FinalBlock createFinalBlock(Block block, KeyPair producerKey) {
    byte[] signature = SafeDataOutputStream.serialize(producerKey.sign(block.identifier())::write);
    return new FinalBlock(block, signature);
  }

  private void appendInvalid(Block block) {
    appendInvalid(block, producerKey);
  }

  private void appendInvalid(Block block, KeyPair producerKey) {
    assertThatThrownBy(() -> appendFinal(blockchainLedger, block, producerKey))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid block received");
  }

  @Test
  public void blockProductionTime() {
    Block before = blockchainLedger.getLatestBlock();
    appendInvalid(
        new Block(
            before.getProductionTime() - 1,
            before.getBlockTime() + 1,
            before.getBlockTime(),
            before.identifier(),
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(before);
    appendInvalid(
        new Block(
            before.getProductionTime(),
            before.getBlockTime() + 1,
            before.getBlockTime(),
            before.identifier(),
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(before);
    Block after =
        new Block(
            before.getProductionTime() + 1,
            before.getBlockTime() + 1,
            before.getBlockTime(),
            before.identifier(),
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of());
    appendFinal(blockchainLedger, after);
    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(after);
  }

  @Test
  public void blockWithInvalidTransaction() {
    long validToTime = System.currentTimeMillis() + 1000;
    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, validToTime, 10_000),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]))
            .sign(producerKey, chainId);
    assertThat(blockchainLedger.addPendingTransaction(transaction)).isTrue();
    appendInvalid(
        new Block(
            validToTime + 1,
            blockchainLedger.getBlockTime() + 1,
            blockchainLedger.getBlockTime(),
            blockchainLedger.getLatestBlock().identifier(),
            blockchainLedger.getLatestBlock().getState(),
            List.of(),
            List.of(transaction.identifier())));
  }

  @Test
  public void verifyBlockProductionTime() {
    long produced = System.currentTimeMillis();
    assertThat(BlockchainLedger.verifyProductionTime(produced, produced - 1, produced + 1))
        .isTrue();
    assertThat(BlockchainLedger.verifyProductionTime(produced, produced, produced + 1)).isFalse();
    assertThat(BlockchainLedger.verifyProductionTime(produced, produced - 1, produced)).isFalse();
  }

  @Test
  public void latestAllowedProductionTime() {
    assertThat(BlockchainLedger.latestAllowedProductionTime(() -> 1))
        .isEqualTo(BlockchainLedger.MAX_PRODUCTION_TIME_DRIFT + 1);
    assertThat(BlockchainLedger.latestAllowedProductionTime(() -> 1000))
        .isEqualTo(BlockchainLedger.MAX_PRODUCTION_TIME_DRIFT + 1000);
  }

  @Test
  public void fetchMissingBlocks() {
    Hash hash = TestObjects.hashNumber(123L);
    Block before = blockchainLedger.getLatestBlock();
    long blockTime = before.getBlockTime() + 2;
    appendFinal(
        blockchainLedger,
        new Block(
            System.currentTimeMillis(),
            blockTime,
            before.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
    verifyBlockRequest(network.queued, blockchainLedger.getLatestBlock().getBlockTime() + 1);
  }

  @Test
  public void shouldSaveGenesisToStorage() {
    Block block = blockchainLedger.getBlock(0);
    assertThat(block).isNotNull();

    blockchainLedger.close();

    ValidBlock blockAndState =
        register(new BlockchainStorage(rootDirectory, chainId)).loadLatestValidBlock();
    assertThat(blockAndState).isNotNull();
    assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block);
  }

  @Test
  public void getStateStorage() {
    StateStorage stateStorage = blockchainLedger.getStateStorage();
    assertThat(stateStorage).isNotNull();
    Hash hash = blockchainLedger.getChainState().getHash();
    Byte read = stateStorage.read(hash, SafeDataInputStream::readSignedByte);
    assertThat(read).isNotNull();
  }

  @Test
  public void shouldGetBlockByHash() {
    Block byBlockTime = blockchainLedger.getBlock(0);
    assertThat(byBlockTime).isNotNull();

    Hash hash = byBlockTime.identifier();
    Block block = blockchainLedger.getBlock(hash);

    blockchainLedger.close();

    BlockchainStorage blockchainStorage =
        register(register(new BlockchainStorage(rootDirectory, chainId)));
    ValidBlock blockAndState = blockchainStorage.loadLatestValidBlock();
    assertThat(blockAndState).isNotNull();
    assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block);
  }

  @Test
  public void getTransactionsForBlock() {
    Hash unknownHash = Hash.create(FunctionUtility.noOpConsumer());
    assertThat(blockchainLedger.getTransactionsForBlock(unknownHash)).isNull();
    SignedTransaction deployPub = createDeployPub();
    blockchainLedger.addPendingTransaction(deployPub);
    produce(deployPub);
    Block latestBlock = blockchainLedger.getLatestBlock();
    assertThat(blockchainLedger.getTransactionsForBlock(latestBlock.identifier())).hasSize(1);
    BlockchainStorageTest.clearTransactions(rootDirectory, memoryStorage);
    assertThat(blockchainLedger.getTransactionsForBlock(latestBlock.identifier())).isNull();
  }

  @Test
  public void getTransaction() {
    SignedTransaction deployPub = createDeployPub();
    assertThat(blockchainLedger.getTransaction(deployPub.identifier())).isNull();
    Block unknownBlock =
        new Block(
            System.currentTimeMillis(),
            blockchainLedger.getBlockTime() + 1,
            blockchainLedger.getBlockTime(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            List.of(),
            List.of(deployPub.identifier()));
    BlockchainStorage storage = register(new BlockchainStorage(rootDirectory, chainId));
    storage.registerAcceptedBlock(
        new FinalBlock(unknownBlock, new byte[0]),
        List.of(
            ExecutedTransaction.create(
                unknownBlock.identifier(), deployPub, true, FixedList.create())),
        blockchainLedger.getChainState(),
        Map.of());
    // Present in storage but belongs to an unknown block
    assertThat(blockchainLedger.getTransaction(deployPub.identifier())).isNull();
  }

  @Test
  public void getFutureBlockNumber() {
    assertThat(blockchainLedger.getBlock(blockchainLedger.getBlockTime() + 1)).isNull();
  }

  @Test
  public void getBlockByHash() {
    BlockchainStorage storage = register(new BlockchainStorage(rootDirectory, chainId));
    Block unknownBlock =
        new Block(
            System.currentTimeMillis(),
            blockchainLedger.getBlockTime() + 1,
            blockchainLedger.getBlockTime(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            List.of(),
            List.of());
    Hash blockIdentifier = unknownBlock.identifier();
    assertThat(blockchainLedger.getBlock(blockIdentifier))
        .describedAs("Block not present in storage")
        .isNull();

    storage.registerAcceptedBlock(
        new FinalBlock(unknownBlock, new byte[0]),
        List.of(),
        blockchainLedger.getChainState(),
        Map.of());
    assertThat(blockchainLedger.getBlock(blockIdentifier))
        .describedAs("Block time in future")
        .isNull();

    // Overwrite block time index
    produce();
    assertThat(blockchainLedger.getBlock(blockIdentifier))
        .describedAs("Block not matching block time index")
        .isNull();
  }

  @Test
  public void getState() {
    produce();
    produce();
    long blockTime = blockchainLedger.getBlockTime();
    assertThat(blockchainLedger.getState(blockTime - 2)).isNotNull();
    assertThat(blockchainLedger.getState(blockTime - 1)).isNotNull();
    assertThat(blockchainLedger.getState(blockTime)).isNotNull();
    assertThat(blockchainLedger.getState(blockTime + 1)).isNull();
  }

  @Test
  public void genesisState() {
    RootDirectory dir = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    BlockchainAddress rootAccount = rootKey.getPublic().createAddress();
    blockchainLedger =
        BlockchainLedger.createBlockChain(
            ObjectCreator.networkConfig(), dir, createGenesisFile(chainId, rootAccount), finder);
    register(blockchainLedger);

    Block latestBlock = blockchainLedger.getLatestBlock();
    assertThat(latestBlock.getBlockTime()).isEqualTo(0);

    blockchainLedger.close();

    BlockchainStorage storage = register(new BlockchainStorage(dir, chainId));
    ValidBlock validBlock = storage.loadLatestValidBlock();

    assertThat(validBlock).isNotNull();
  }

  @Test
  public void shouldBeAbleToReadState() {
    SignedTransaction deploy = createDeployPub();

    this.blockchainLedger.addPendingTransaction(deploy);
    Block first =
        new Block(
            blockchainLedger.getLatestProductionTime() + 10,
            1,
            0,
            this.blockchainLedger.getLatestBlock().identifier(),
            this.blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of(deploy.identifier()));
    appendFinal(blockchainLedger, first);

    BlockAndState latest = this.blockchainLedger.latest();
    ExecutedTransaction transaction = blockchainLedger.getTransaction(deploy.identifier());
    assertThat(transaction).isNotNull();

    Block second =
        new Block(
            blockchainLedger.getLatestProductionTime() + 10,
            2,
            1,
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of());
    appendFinal(blockchainLedger, second);

    latest = this.blockchainLedger.latest();
    Block third =
        new Block(
            blockchainLedger.getLatestProductionTime() + 10,
            3,
            1,
            latest.getBlock().identifier(),
            latest.getState().getHash(),
            List.of(),
            List.of());
    appendFinal(blockchainLedger, third);

    blockchainLedger.close();

    RootDirectory rootDirectory = this.rootDirectory;
    BlockchainStorageTest.clearTransactions(rootDirectory, memoryStorage);

    BlockchainLedger blockchainLedger =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, "genesis", signer));

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(third);
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;
    assertThat(blockchainLedger.getChainState().getContractState(contractAddress))
        .hasFieldOrPropertyWithValue("state", 37);
  }

  @Test
  public void deployPubContract() {
    final ImmutableChainState initialState = blockchainLedger.getChainState();
    SignedTransaction deploy = createDeployPub();
    blockchainLedger.addPendingTransaction(deploy);
    produce(deploy);

    produce();
    produce();

    ImmutableChainState state = blockchainLedger.getChainState();
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB1;
    assertThat(state.getContractState(contractAddress)).hasFieldOrPropertyWithValue("state", 37);
    assertThat(state.hasContractChanged(contractAddress, initialState)).isTrue();
    assertThat(initialState.hasContractChanged(contractAddress, state)).isTrue();
    assertThat(initialState.hasContractChanged(contractAddress, initialState)).isFalse();
    assertThat(state.hasContractChanged(contractAddress, state)).isFalse();
    assertThat(state.getBlockchainContract(contractAddress).getBinder()).isNotNull();
  }

  @Test
  void getSysBinder() {
    ImmutableChainState state = blockchainLedger.getChainState();
    assertThat(state.getBlockchainContract(TestObjects.CONTRACT_BOOTSTRAP).getBinder()).isNotNull();
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @Test
  public void deployZkContract() {
    BlockchainAddress contractAddress = TestObjects.CONTRACT_ZK1;
    SignedTransaction deploy = createDeployZk(contractAddress);
    blockchainLedger.addPendingTransaction(deploy);
    produce(deploy);
    produce();

    ImmutableChainState chainState = blockchainLedger.latest().getState();
    CoreContractState core = chainState.getCoreContractState(contractAddress);
    assertThat(core).isNotNull();
    ZkBlockchainContract<?> blockchainContract =
        (ZkBlockchainContract) chainState.getBlockchainContract(contractAddress);
    ZkBinderContract<?> binder = blockchainContract.getBinder();
    Supplier<String> computationCreator = (Supplier<String>) binder.getContract().getComputation();
    assertThat(computationCreator.get()).isEqualTo("Expected: Unable to compute");

    StateString contractState = (StateString) chainState.getContractState(contractAddress);
    assertThat(contractState.value()).isEqualTo("Init");
    assertThat(chainState.getContract(contractAddress)).isNotNull();
  }

  private Hash latestEvent(SignedTransaction tx) {
    AvlTree<Hash, Hash> spawnedEvents =
        blockchainLedger.latest().getState().getExecutedState().getSpawnedEvents();
    for (Hash spawnedEvent : spawnedEvents.keySet()) {
      if (spawnedEvents.getValue(spawnedEvent).equals(tx.identifier())) {
        return spawnedEvent;
      }
    }
    throw new IllegalStateException("Should not happen");
  }

  @Test
  public void shouldFloodTransactions() {
    SignedTransaction transaction = createDeployJar(100_000, TestObjects.CONTRACT_PUB1);
    boolean appended = blockchainLedger.addPendingTransaction(transaction);
    assertThat(appended).isTrue();
    assertThat(network.queued).hasSize(1);
    assertThat(network.queued.get(0).getType()).isEqualTo(Packet.Type.TRANSACTION);
    boolean appendedAgain = blockchainLedger.addPendingTransaction(transaction);
    assertThat(appendedAgain).isTrue();
    assertThat(network.queued).hasSize(1);
  }

  @Test
  public void shouldFloodBlocks() {
    produce();
    assertThat(network.queued).hasSize(1);
    assertThat(network.queued.get(0).getType()).isEqualTo(Packet.Type.BLOCK);
  }

  @Test
  public void shouldPatchFromStorage() {
    final RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), memoryStorage);

    String genesisFile = createGenesisFile(chainId, producerAccount);
    this.blockchainLedger =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, genesisFile, signer));

    SignedTransaction deploy = createDeployPub();
    this.blockchainLedger.addPendingTransaction(deploy);
    Block first =
        new Block(
            System.currentTimeMillis(),
            1,
            0,
            this.blockchainLedger.getLatestBlock().identifier(),
            this.blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of(deploy.identifier()));
    appendFinal(blockchainLedger, first);

    this.blockchainLedger.close();

    BlockchainStorageTest.clearStates(rootDirectory, memoryStorage);

    BlockchainLedger blockchainLedger =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, genesisFile, signer));

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(first);
  }

  private String createGenesisFile(String chainId, BlockchainAddress root) {
    File genesis = ExceptionConverter.call(() -> File.createTempFile("genesis", ".zip"), "");

    GenesisStorage storage = new GenesisStorage();
    ChainStateCache context = new ChainStateCache(storage);

    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    genesisState.createAccount(root);
    Hash stateHash =
        genesisState
            .asImmutable(chainId, null, AvlTree.create(), AvlTree.create())
            .saveExecutedState(
                s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());

    storage.persistGenesis(genesis, new FinalBlock(Block.createGenesis(stateHash), new byte[0]));
    return genesis.getAbsolutePath();
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void executeInline() {
    ShardNonces shardNonce = ShardNonces.EMPTY;
    assertThat(BlockchainLedger.executeInline(false, shardNonce, shardNonce.getInbound())).isTrue();
    assertThat(BlockchainLedger.executeInline(true, shardNonce, shardNonce.getInbound())).isFalse();
    assertThat(BlockchainLedger.executeInline(false, shardNonce, shardNonce.getInbound() + 1))
        .isFalse();
  }

  private SignedTransaction createDeployPub() {
    return createDeployPub(new byte[0], TestObjects.CONTRACT_PUB1);
  }

  private SignedTransaction createDeployPub(byte[] rpc, BlockchainAddress address) {
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    return createDeploy(address, binderJar, PubTestContract.class, rpc);
  }

  private SignedTransaction createDeployZk(BlockchainAddress address) {
    byte[] zkTestBinder = JarBuilder.buildJar(ZkTestBinder.class);
    return createDeploy(address, zkTestBinder, ZkTestContract.class, new byte[0]);
  }

  private SignedTransaction createDeploy(
      BlockchainAddress address, byte[] binderJar, Class<?> contract, byte[] rpc) {
    byte[] jar = JarBuilder.buildJar(contract);
    InteractWithContractTransaction deployTransaction =
        DeployContractInTest.create(address, binderJar, jar, rpc);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 100_000),
            deployTransaction)
        .sign(producerKey, chainId);
  }

  @Test
  public void noFeesShouldDisappear() {
    int signedTransactionCost = 100_000;

    byte[] jar = JarBuilder.buildJar(FeeTestContract.class);
    InteractWithContractTransaction createContractTransaction =
        DeployContractInTest.create(
            TestObjects.CONTRACT_SYS, TestContracts.CONTRACT_BINDER_SYSTEM, jar, new byte[0], 5000);
    SignedTransaction deploy =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    getNonce(producerAccount),
                    System.currentTimeMillis() + 100_000,
                    signedTransactionCost),
                createContractTransaction)
            .sign(producerKey, chainId);

    blockchainLedger.addPendingTransaction(deploy);
    produce(deploy);
    produce();

    long registeredUsage =
        FeePluginHelper.accumulatedRegisteredUsage(blockchainLedger.getChainState());
    assertThat(registeredUsage).isEqualTo(100_000);
  }

  @Test
  public void shouldBeAbleToReadStateFromStorage() {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                this::stateWithEverything,
                network,
                finder,
                ExecutionFactoryThreaded.create()));
    produce();

    Hash stateHash = blockchainLedger.getChainState().getHash();
    blockchainLedger.close();

    BlockchainLedger fromStorage =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, "genesis", signer));

    assertThat(fromStorage.getChainState().getHash()).isEqualTo(stateHash);
    assertThat(fromStorage.getChainState().getLocalAccountPluginState()).isNotNull();
    assertThat(fromStorage.getChainState().getGlobalPluginState(ChainPluginType.ACCOUNT))
        .isNotNull();
  }

  @Test
  public void noExecutionEventFinderGiven() {
    final RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(temporaryFolder.toFile(), memoryStorage);

    String genesisFile = createGenesisFile(chainId, producerAccount);
    BlockchainLedger chain =
        register(
            BlockchainLedger.createBlockChain(
                ObjectCreator.networkConfig(), rootDirectory, genesisFile, signer));
    ExecutableEvent event = createEvent("syncEvent");
    ExecutableEvent event2 = createEvent("executable1");
    ExecutableEvent event3 = createEvent("executable2");
    chain.getStateStorage().write(event.identifier(), event::write);
    chain.getStateStorage().write(event2.identifier(), event2::write);
    chain.getStateStorage().write(event3.identifier(), event3::write);
    BlockResponseSemiCompressed.BlockPairSemiCompressed blockPairCompressed =
        new BlockResponseSemiCompressed.BlockPairSemiCompressed(
            createFinalBlock(
                new Block(
                    0,
                    0,
                    0,
                    Hash.create(s -> s.writeString("one")),
                    Hash.create(s -> s.writeString("one")),
                    List.of(event.identifier()),
                    List.of()),
                producerKey),
            List.of(),
            List.of(
                new SemiCompressedEvent(
                    new SemiCompressedEvent.IdentifierAndShard(
                        event.identifier(), chain.getChainId())),
                new SemiCompressedEvent(event2),
                new SemiCompressedEvent(event3)),
            chainId);
    memoryStorage.put(
        new MemoryStorage.PathAndHash(
            MemoryStorage.getPath("eventTransactions"), event.identifier()),
        SafeDataOutputStream.serialize(event::write));
    memoryStorage.put(
        new MemoryStorage.PathAndHash(
            MemoryStorage.getPath("eventTransactions"), event2.identifier()),
        SafeDataOutputStream.serialize(event2::write));
    ExecutableBlock executableBlock = chain.getExecutableBlock(blockPairCompressed);
    assertThat(executableBlock).isNotNull();
    assertThat(executableBlock.getEventTransactions().get(0).identifier())
        .isEqualTo(event.identifier());
    assertThat(executableBlock.getEventTransactions().get(1).identifier())
        .isEqualTo(event2.identifier());
  }

  private ExecutableEvent createEvent(String hashString) {
    return new ExecutableEvent(
        chainId,
        EventTransaction.createStandalone(
            Hash.create(s -> s.writeString(hashString)),
            new KeyPair().getPublic().createAddress(),
            0L,
            InteractWithContractTransaction.create(
                BlockchainAddress.fromString("02A000000000000000000000000000000000000002"),
                new byte[0]),
            new ShardRoute(chainId, 0),
            0L,
            0L));
  }

  @Test
  public void unableToStartShardFromMasterState() {
    File newFolder = temporaryFolder.toFile();
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(newFolder);
    blockchainLedger =
        BlockchainLedger.createForTest(
            rootDirectory, this::stateWithEverything, network, finder, executionFactory);
    produce();

    blockchainLedger.close();
  }

  @Test
  public void appendShouldBumpLatestBlockInStorage() {
    long blockTime = blockchainLedger.getBlockTime();
    produce();

    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    BlockTimeStorage latestBlockTime =
        register(new BlockTimeStorage(rootDirectory.createFile("latestBlockTime"), -1));
    assertThat(latestBlockTime.getLatestBlockTime()).isEqualTo(blockTime + 1);
  }

  @Test
  public void fetchMissingBlockWithMultiplePending() {
    Hash hash = TestObjects.hashNumber(123L);
    Block latestBlock = blockchainLedger.getLatestBlock();
    long firstPending = latestBlock.getBlockTime() + 2;
    appendFinal(
        blockchainLedger,
        new Block(
            System.currentTimeMillis(),
            firstPending,
            latestBlock.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
    Hash hash2 = Hash.create(hash);
    latestBlock = blockchainLedger.getLatestBlock();
    long blockTime = latestBlock.getBlockTime() + 3;
    appendFinal(
        blockchainLedger,
        new Block(
            System.currentTimeMillis(),
            blockTime,
            latestBlock.getBlockTime(),
            hash2,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
    verifyBlockRequest(network.queued, blockchainLedger.getBlockTime() + 1);
  }

  @Test
  public void fetchForMissingTransaction() {
    Hash transactionHash = TestObjects.hashNumber(753);
    Block before = blockchainLedger.getLatestBlock();
    Hash hash = before.identifier();
    Block block =
        new Block(
            System.currentTimeMillis(),
            before.getBlockTime() + 1,
            before.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of(transactionHash));
    appendFinal(blockchainLedger, block);
    verifyBlockRequest(network.queued, block.getBlockTime());
  }

  @Test
  public void missingEventsInExecutableEvent() {
    Hash eventHash = TestObjects.hashNumber(753);
    Block before = blockchainLedger.getLatestBlock();
    Hash hash = before.identifier();
    Block block =
        new Block(
            System.currentTimeMillis(),
            before.getBlockTime() + 1,
            before.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(eventHash),
            List.of());
    FinalBlock finalBlock = createFinalBlock(block, producerKey);
    assertThatThrownBy(
            () ->
                blockchainLedger.appendBlock(new ExecutableBlock(finalBlock, List.of(), List.of())))
        .hasMessageContaining("Received incomplete executable block");
  }

  @Test
  public void fetchForMissingEvents() {
    Hash transactionHash = TestObjects.hashNumber(112);
    Block before = blockchainLedger.getLatestBlock();
    Hash hash = before.identifier();
    Block block =
        new Block(
            System.currentTimeMillis(),
            before.getBlockTime() + 1,
            before.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(transactionHash),
            List.of());
    appendFinal(blockchainLedger, block);
    verifyBlockRequest(network.queued, block.getBlockTime());
  }

  private void verifyBlockRequest(List<Packet<?>> sentPackets, long blockTime) {
    assertThat(sentPackets).hasSize(1);
    Packet<?> packet = sentPackets.get(0);
    assertThat(packet.getType()).isEqualTo(Packet.Type.BLOCK_REQUEST);
    Object payload = packet.getPayload();
    assertThat(payload).isInstanceOf(BlockRequest.class);
    assertThat(((BlockRequest) payload).getBlockTime()).isEqualTo(blockTime);
  }

  @Test
  public void shouldHandleMissingRpc() {
    Hash hash = TestObjects.hashNumber(123L);
    Block latestBlock = blockchainLedger.getLatestBlock();
    appendFinal(
        blockchainLedger,
        new Block(
            System.currentTimeMillis(),
            latestBlock.getBlockTime() + 2,
            latestBlock.getBlockTime(),
            hash,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));
  }

  @Test
  public void invalidStateHashForBlock() {
    final Block blockBefore = blockchainLedger.getLatestBlock();

    appendInvalid(
        new Block(
            System.currentTimeMillis(),
            blockBefore.getBlockTime() + 1,
            blockBefore.getBlockTime(),
            blockBefore.identifier(),
            TestObjects.EMPTY_HASH,
            List.of(),
            List.of()));

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(blockBefore);
  }

  @Test
  public void invalidCommitteeIdForBlock() {
    final Block blockBefore = blockchainLedger.getLatestBlock();

    appendInvalid(
        new Block(
            System.currentTimeMillis(),
            blockBefore.getBlockTime() + 1,
            blockBefore.getCommitteeId() - 1,
            blockBefore.identifier(),
            blockchainLedger.latest().getState().getHash(),
            List.of(),
            List.of()));

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(blockBefore);
  }

  @Test
  public void invalidParentHashForBlock() {
    final Block blockBefore = blockchainLedger.getLatestBlock();

    appendInvalid(
        new Block(
            System.currentTimeMillis(),
            blockBefore.getBlockTime() + 1,
            blockBefore.getBlockTime(),
            TestObjects.EMPTY_HASH,
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of()));

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(blockBefore);
  }

  @Test
  public void nonBlockProducerForBlock() {
    Block blockBefore = blockchainLedger.getLatestBlock();
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockBefore.getBlockTime() + 1,
            blockBefore.getBlockTime(),
            blockBefore.identifier(),
            blockchainLedger.getChainState().getHash(),
            List.of(),
            List.of());

    byte[] signature = SafeDataOutputStream.serialize(producerKey.sign(block.identifier())::write);
    FinalBlock finalBlock =
        new FinalBlock(block, Arrays.copyOfRange(signature, 0, signature.length + 1));
    assertThatThrownBy(() -> blockchainLedger.appendBlock(finalBlock))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid block received");

    assertThat(blockchainLedger.getLatestBlock()).isEqualTo(blockBefore);
  }

  @Test
  public void getTransactionCost() {
    SignedTransaction transaction = createDeployJar(50_000, TestObjects.CONTRACT_PUB1);
    blockchainLedger.addPendingTransaction(transaction);
    produce(transaction);

    Hash identifier = transaction.identifier();
    TransactionCost transactionCost = blockchainLedger.getTransactionCost(identifier);
    assertThat(transactionCost).isNotNull();
  }

  @Test
  public void notifyListeners() {
    AtomicInteger called = new AtomicInteger();
    AtomicReference<Block> calledWithBlock =
        new AtomicReference<>(blockchainLedger.getLatestBlock());
    AtomicReference<Hash> finalState = new AtomicReference<>();
    Listener listener =
        new Listener() {

          @Override
          public void newBlockProposal(Block block) {
            calledWithBlock.set(block);
          }

          @Override
          public void newFinalBlock(Block block, ImmutableChainState state) {
            called.incrementAndGet();
            assertThat(calledWithBlock).hasValue(block);
          }

          @Override
          public void newFinalState(ImmutableChainState state) {
            finalState.set(state.getHash());
          }
        };
    blockchainLedger.attach(listener);
    TestExecutionQueue executionQueue =
        executionFactory.getExecutionQueue(BlockchainLedger.LISTENER_THREAD_NAME);
    assertThat(executionQueue.runStep()).isFalse();
    produce();
    assertThat(executionQueue.runStep()).isTrue();
    assertThat(finalState.get()).isNotNull();
    assertThat(called.get()).isEqualTo(1);
    assertThat(calledWithBlock.get()).isEqualTo(blockchainLedger.getLatestBlock());
    blockchainLedger.detach(listener);
    final Block expectedBlock = blockchainLedger.getLatestBlock();
    produce();
    assertThat(executionQueue.runStep()).isFalse();
    assertThat(called.get()).isEqualTo(1);
    assertThat(calledWithBlock.get()).isEqualTo(expectedBlock);
  }

  @Test
  public void detachNonExistingShouldNotRemoveOther() {
    AtomicInteger called = new AtomicInteger();
    Listener listener =
        new Listener() {

          @Override
          public void newFinalBlock(Block block, ImmutableChainState state) {
            called.incrementAndGet();
          }
        };
    blockchainLedger.attach(listener);
    Listener nonAttachedListener = new Listener() {};
    blockchainLedger.detach(nonAttachedListener);
    TestExecutionQueue executionQueue =
        executionFactory.getExecutionQueue(BlockchainLedger.LISTENER_THREAD_NAME);
    assertThat(executionQueue.runStep()).isFalse();

    produce();

    assertThat(executionQueue.runStep()).isTrue();
    assertThat(called.get()).isEqualTo(1);
  }

  @Test
  public void pendingTransactionListener() {
    AtomicReference<SignedTransaction> transaction = new AtomicReference<>();
    blockchainLedger.attach(
        new Listener() {
          @Override
          public void newPendingTransaction(SignedTransaction newTransaction) {
            transaction.set(newTransaction);
          }
        });
    SignedTransaction deployPub = createDeployPub();

    TestExecutionQueue executionQueue =
        executionFactory.getExecutionQueue(BlockchainLedger.LISTENER_THREAD_NAME);
    assertThat(executionQueue.runStep()).isFalse();
    blockchainLedger.addPendingTransaction(deployPub);
    assertThat(executionQueue.runStep()).isTrue();
    assertThat(transaction.get()).isEqualTo(deployPub);
  }

  @Test
  public void defaultListenerImplementation() {
    Listener listener = new Listener() {};
    listener.newPendingTransaction(createDeployPub());
    listener.newBlockProposal(blockchainLedger.getLatestBlock());
    listener.newFinalBlock(blockchainLedger.getLatestBlock(), blockchainLedger.getChainState());
    listener.newFinalState(blockchainLedger.getChainState());
  }

  @Test
  public void updateGasBalance() {
    long baseBalance = 100_000L;
    assertBalance(producerAccount, baseBalance);

    MutableChainState mutable = blockchainLedger.getChainState().asMutable();
    mutable.createAccount(secondAccount);

    assertBalance(mutable, producerAccount, baseBalance);

    FeePluginHelper.mintGas(mutable, producerAccount, 1000);
    assertBalance(mutable, producerAccount, baseBalance + 1000);

    StateAccessor feePluginGlobal = FeePluginHelper.getFeePluginGlobal(mutable);
    BlockchainAddress zkFeeAccount = feePluginGlobal.get("zkFeeAccount").blockchainAddressValue();
    BlockchainAddress feeAccount = feePluginGlobal.get("feeAccount").blockchainAddressValue();
    assertThat(zkFeeAccount).isNotEqualTo(feeAccount);

    assertBalance(mutable, zkFeeAccount, baseBalance);
    long balance = FeePluginHelper.balance(mutable, producerAccount);
    FeePluginHelper.mintGas(mutable, producerAccount, -balance);
  }

  @Test
  public void emptyFeePlugin() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
            },
            UUID.randomUUID().toString());
    assertThat(immutableChainState.getLocalAccountPluginState()).isNull();
    assertThat(immutableChainState.getGlobalPluginState(ChainPluginType.ACCOUNT)).isNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getLocalAccountPluginState()).isNull();
    assertThat(mutableChainState.getGlobalPluginState(ChainPluginType.ACCOUNT)).isNull();
  }

  @Test
  public void defaultConsensus() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.CONSENSUS, null, rpc);
            },
            UUID.randomUUID().toString());
    assertThat(immutableChainState.getConsensusPlugin()).isInstanceOf(RootConsensusPlugin.class);
    assertThat(immutableChainState.getGlobalPluginState(ChainPluginType.CONSENSUS)).isNull();
    assertThat(immutableChainState.getPluginJar(ChainPluginType.CONSENSUS)).isNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getConsensusPlugin()).isInstanceOf(RootConsensusPlugin.class);
    assertThat(mutableChainState.getGlobalPluginState(ChainPluginType.CONSENSUS)).isNull();
    assertThat(mutableChainState.getPluginJar(ChainPluginType.CONSENSUS)).isNull();
    assertThat(mutableChainState.getConsensusPlugin()).isInstanceOf(RootConsensusPlugin.class);
  }

  @Test
  public void accountJarNotNull() {
    final byte[] accountJar = FeePluginHelper.createJar();
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, accountJar, rpc);
            },
            UUID.randomUUID().toString());
    assertThat(immutableChainState.getPluginJar(ChainPluginType.ACCOUNT)).isNotNull();
    assertThat(immutableChainState.getPluginJar(ChainPluginType.ACCOUNT).getData())
        .isEqualTo(accountJar);
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getPluginJar(ChainPluginType.ACCOUNT)).isNotNull();
  }

  @Test
  public void accountJarNull() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
            },
            UUID.randomUUID().toString());
    assertThat(immutableChainState.getPluginJar(ChainPluginType.ACCOUNT)).isNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getPluginJar(ChainPluginType.ACCOUNT)).isNull();
  }

  @Test
  public void defaultRouting() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ROUTING, null, rpc);
            },
            UUID.randomUUID().toString());
    assertThat(immutableChainState.getRoutingPlugin()).isInstanceOf(RootRoutingPlugin.class);
    assertThat(immutableChainState.getPluginJar(ChainPluginType.ROUTING)).isNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getRoutingPlugin()).isInstanceOf(RootRoutingPlugin.class);
    assertThat(mutableChainState.getPluginJar(ChainPluginType.ROUTING)).isNull();
    assertThat(mutableChainState.getRoutingPlugin()).isInstanceOf(RootRoutingPlugin.class);
  }

  @Test
  public void defaultSharedObjectStore() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.SHARED_OBJECT_STORE,
                  null,
                  rpc);
            },
            UUID.randomUUID().toString());
    SharedObjectStorePluginAccessor plugin = immutableChainState.getSharedObjectStorePlugin();
    assertThat(plugin).isInstanceOf(EmptySharedObjectStorePlugin.class);
    assertThat(plugin.exists(null)).isFalse();
    assertThat(immutableChainState.getPluginJar(ChainPluginType.SHARED_OBJECT_STORE)).isNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getSharedObjectStorePlugin())
        .isInstanceOf(EmptySharedObjectStorePlugin.class);
    assertThat(mutableChainState.getPluginJar(ChainPluginType.SHARED_OBJECT_STORE)).isNull();
    assertThat(mutableChainState.getSharedObjectStorePlugin())
        .isInstanceOf(EmptySharedObjectStorePlugin.class);
  }

  @Test
  public void wrappedSharedObjectStore() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.SHARED_OBJECT_STORE,
                  JarBuilder.buildJar(BlockchainSharedObjectStorePluginTest.EmptyPlugin.class),
                  rpc);
            },
            UUID.randomUUID().toString());
    SharedObjectStorePluginAccessor plugin = immutableChainState.getSharedObjectStorePlugin();
    assertThat(plugin).isInstanceOf(SharedObjectStorePluginWrapper.class);
    assertThat(plugin.exists(null)).isFalse();
    assertThat(immutableChainState.getPluginJar(ChainPluginType.SHARED_OBJECT_STORE)).isNotNull();
    MutableChainState mutableChainState = immutableChainState.asMutable();
    assertThat(mutableChainState.getSharedObjectStorePlugin())
        .isInstanceOf(SharedObjectStorePluginWrapper.class);
    assertThat(mutableChainState.getPluginJar(ChainPluginType.SHARED_OBJECT_STORE)).isNotNull();
  }

  @Test
  public void chainStateHashShouldBeStable() {
    ImmutableChainState immutableChainState =
        StateHelper.fromPopulate(
            builder -> {
              stateWithEverything(builder);
              byte[] rpc = new byte[0];
              builder.setPlugin(
                  FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
            },
            "StableChainId");

    assertThat(immutableChainState.getHash().toString())
        .isEqualTo("6cab7adef3312bbf36eb3c5c54e64e122012c820ed541056af5a467882cd57c8");

    assertThat(immutableChainState.asMutable().getAccounts())
        .containsExactlyInAnyOrderElementsOf(immutableChainState.getAccounts());
  }

  private void stateWithEverything(MutableChainState chainState) {
    StateHelper.initial(chainState);
    chainState.createAccount(secondAccount);
  }

  @Test
  public void executedTransactions() {
    SignedTransaction transaction = createDeployJar(50_000, TestObjects.CONTRACT_PUB1);
    blockchainLedger.addPendingTransaction(transaction);
    produce(transaction);

    final Hash expectedBlock = blockchainLedger.getLatestBlock().identifier();

    Hash hash = transaction.identifier();
    ExecutedTransaction executed = blockchainLedger.getTransaction(hash);
    assertThat(executed).isNotNull();
    assertThat(executed.getInner()).isEqualTo(transaction);
    assertThat(executed.didExecutionSucceed()).isTrue();
    assertThat(executed.getBlockHash()).isEqualTo(expectedBlock);
  }

  @Test
  public void sendPluginInteractToNamedShard() {
    byte[] jar = JarBuilder.buildJar(PluginInteractionToNamedShardContract.class);
    this.blockchainLedger =
        register(
            BlockchainLedger.createShardForTest(
                rootDirectory,
                mutable -> {
                  StateHelper.initial(mutable);
                  mutable.setPlugin(
                      FunctionUtility.noOpBiConsumer(),
                      null,
                      ChainPluginType.ROUTING,
                      BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_SHARD_ONE,
                      new byte[0]);
                  mutable.addActiveShard(
                      FunctionUtility.noOpBiConsumer(), null, TestObjects.SHARD_ONE);
                  mutable.createAccount(producerAccount);
                  FeePluginHelper.mintGas(mutable, producerAccount, 100_000);
                  BlockchainAddress deployPublicContract = TestObjects.CONTRACT_SYS;
                  CoreContractStateTest.createContract(
                      mutable, jar, deployPublicContract, TestContracts.CONTRACT_BINDER_SYSTEM);
                },
                TestObjects.SHARD_ONE,
                network,
                finder,
                FunctionUtility.noOpConsumer(),
                executionFactory));
    chainId = blockchainLedger.getChainId();

    String shardId = TestObjects.SHARD_ONE;
    byte[] rpc =
        SafeDataOutputStream.serialize(
            stream -> stream.writeBoolean(true /* dummy fee distribution */));
    InteractWithContractTransaction interactWithContractTransaction =
        InteractWithContractTransaction.create(
            TestObjects.CONTRACT_SYS,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeString(shardId);
                  stream.writeDynamicBytes(rpc);
                }));

    SignedTransaction interact =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    getNonce(producerAccount), System.currentTimeMillis() + 100_000, 6000),
                interactWithContractTransaction)
            .sign(producerKey, chainId);
    blockchainLedger.addPendingTransaction(interact);
    produce(interact);
    produce();

    long bpFees = FeePluginHelper.accumulatedRegisteredUsage(blockchainLedger.getChainState());
    assertThat(bpFees).isEqualTo(0);
  }

  @Test
  public void closeBlockchain() {
    assertThat(network.closed).isFalse();
    blockchainLedger.close();
    assertThat(network.closed).isTrue();
  }

  @Test
  public void finalizationRequests() {
    FinalizationData finalizationData = FinalizationData.create(new byte[1]);
    blockchainLedger.sendFinalizationRequest(finalizationData);
    assertThat(network.queued).hasSize(1);
    blockchainLedger.floodFinalizationData(finalizationData);
    assertThat(network.queued).hasSize(2);
  }

  @Test
  public void latestBlockAndState() {
    BlockAndState latest = blockchainLedger.latest();
    assertThat(latest.getBlock()).isEqualTo(blockchainLedger.getLatestBlock());
    assertThat(latest.getState()).isEqualTo(blockchainLedger.getChainState());
  }

  @Test
  public void ledgerFinalizationThreadName() {
    assertThat(BlockchainLedger.ledgerFinalizationThreadName(null))
        .isEqualTo("LedgerFinalization-Gov");
    assertThat(BlockchainLedger.ledgerFinalizationThreadName("Shard0"))
        .isEqualTo("LedgerFinalization-Shard0");
  }

  private SignedTransaction createDeployJar(long cost, BlockchainAddress address) {
    byte[] jar = JarBuilder.buildJar(TestContractByInvocation.class);
    byte[] binderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    InteractWithContractTransaction deployTransaction =
        DeployContractInTest.create(address, binderJar, jar, new byte[0]);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount),
                blockchainLedger.getLatestBlock().getBlockTime() + 10,
                cost),
            deployTransaction)
        .sign(rootKey, chainId);
  }

  private void produce(SignedTransaction... transaction) {
    produce(blockchainLedger, transaction);
  }

  static void produce(BlockchainLedger blockchainLedger, SignedTransaction... transaction) {
    produce(1, blockchainLedger, transaction);
  }

  static void produce(
      long productionTimeOffset,
      BlockchainLedger blockchainLedger,
      SignedTransaction... transaction) {
    BlockAndState latest = blockchainLedger.latest();
    Block latestBlock = latest.getBlock();
    long productionTime = latestBlock.getProductionTime() + productionTimeOffset;
    List<Hash> eventHashes = new ArrayList<>();
    List<Hash> transactionHashes =
        Arrays.stream(transaction)
            .filter(t -> t.checkValidity(productionTime, latest.getState()::lookupNonce))
            .filter(
                t -> {
                  if (!t.canCoverFee(latest.getState(), 0)) {
                    throw new RuntimeException("Could not cover fee: " + t);
                  } else {
                    return true;
                  }
                })
            .map(SignedTransaction::identifier)
            .collect(Collectors.toList());
    appendFinal(
        blockchainLedger,
        new Block(
            productionTime,
            latestBlock.getBlockTime() + 1,
            latestBlock.getBlockTime(),
            latestBlock.identifier(),
            latest.getState().getHash(),
            eventHashes,
            transactionHashes));
  }

  private void assertBalance(ChainState state, BlockchainAddress address, long expectedBalance) {
    long actual = FeePluginHelper.balance(state, address);
    assertThat(actual).isEqualTo(expectedBalance);
  }

  private void assertBalance(BlockchainAddress address, long expectedBalance) {
    assertBalance(blockchainLedger.latest().getState(), address, expectedBalance);
  }

  /** Test contract. */
  public static final class DeployContractInTest extends SysContract<StateVoid> {

    private static final Logger logger = LoggerFactory.getLogger(DeployContractInTest.class);

    static InteractWithContractTransaction create(
        BlockchainAddress address, byte[] binderJar, byte[] jar, byte[] rpc) {
      return create(address, binderJar, jar, rpc, 2 * (binderJar.length + jar.length + 250));
    }

    static InteractWithContractTransaction create(
        BlockchainAddress address, byte[] binderJar, byte[] jar, byte[] rpc, int fee) {
      return InteractWithContractTransaction.create(
          TestObjects.CONTRACT_BOOTSTRAP,
          SafeDataOutputStream.serialize(
              stream -> {
                address.write(stream);
                stream.writeDynamicBytes(binderJar);
                stream.writeDynamicBytes(jar);
                stream.writeDynamicBytes(rpc);
                stream.writeInt(fee);
              }));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      BlockchainAddress address = BlockchainAddress.read(rpc);
      byte[] bindingJar = rpc.readDynamicBytes();
      byte[] contractJar = rpc.readDynamicBytes();
      byte[] invocation = rpc.readDynamicBytes();
      int fee = rpc.readInt();
      logger.info("Invocation read for address {}", address);
      SystemEventCreator eventManager = context.getInvocationCreator();
      eventManager
          .deployContract(address)
          .withBinderJar(bindingJar)
          .withContractJar(contractJar)
          .withPayload(stream -> stream.write(invocation))
          .allocateCost(fee)
          .send();

      return super.onInvoke(context, state, rpc);
    }
  }

  /** Test contract. */
  public static final class FeeTestContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onCreate(SysContractContext context, SafeDataInputStream rpc) {
      BlockchainAddress contractAddress = context.getContractAddress();

      context
          .getInvocationCreator()
          .invoke(contractAddress)
          .allocateCost(2500)
          .withPayload(EventCreator.EMPTY_RPC)
          .send();

      context.getInvocationCreator().removeContract(contractAddress);

      return null;
    }
  }

  /** Test contract. */
  public static final class PluginInteractionToNamedShardContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      String shardId = rpc.readString();
      byte[] updateRpc = rpc.readDynamicBytes();
      context.getInvocationCreator().updateContextFreeAccountPluginState(shardId, updateRpc);
      return null;
    }
  }
}
