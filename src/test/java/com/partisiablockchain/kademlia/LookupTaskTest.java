package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.kademlia.LookupTask.ResolvableLookup;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LookupTaskTest {

  private final List<LookupTask> consumedTasks = new ArrayList<>();

  private final List<Lookup> lookups = new ArrayList<>();
  private final KademliaKey toLookup = new KademliaKey();
  private final KademliaKey myId = new KademliaKey();

  @Test
  public void shouldCallUpToMaxOutstanding() throws Exception {
    LookupTask task = createTask(Kademlia.MAX_OUTSTANDING + 1);

    task.call();

    Assertions.assertThat(lookups)
        .hasSize(Kademlia.MAX_OUTSTANDING)
        .allSatisfy(lookup -> Assertions.assertThat(lookup.toLookup).isEqualTo(toLookup));
  }

  @Test
  public void shouldCallNextWhenFailed() throws Exception {
    LookupTask task = createTask(Kademlia.MAX_OUTSTANDING + 2);

    task.call();

    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING);

    lookups.get(0).sender.fail(consumedTasks::add);
    verifyConsumed(task, 1);

    task.call();

    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING + 1);

    // Calling again after failed should not trigger new lookups
    task.call();
    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING + 1);
  }

  @Test
  public void shouldProceedToNextRoundWhenCloserFound() throws Exception {
    LookupTask task = createTask(2);
    Assertions.assertThat(task.newNodesSize()).isEqualTo(2);

    task.call();

    Assertions.assertThat(task.newNodesSize()).isEqualTo(0);
    Assertions.assertThat(lookups).hasSize(2);

    KademliaNode closer = createAtDistanceToLookup(4);
    lookups.get(0).sender.resolve(consumedTasks::add, List.of(closer));
    verifyConsumed(task, 1);

    task.call();

    Assertions.assertThat(lookups).hasSize(3);
    Assertions.assertThat(lookups.get(2).node).isEqualTo(closer);

    Assertions.assertThat(task.newNodesSize()).isEqualTo(0);
    Assertions.assertThat(task.currentPendingSize()).isEqualTo(1);
  }

  @Test
  public void isDone() throws Exception {
    LookupTask task = createTask(2);

    task.call();

    Assertions.assertThat(lookups).hasSize(2);

    // Proceed to next round
    List<KademliaNode> closer = createKnownAtDistance(Kademlia.BUCKET_SIZE, 4);
    lookups.get(0).sender.resolve(consumedTasks::add, closer);
    verifyConsumed(task, 1);

    final Lookup previousRoundLookup = lookups.get(1);
    lookups.clear();

    task.call();

    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING);

    // No change as we haven't found anyone closer and pending and current is still populated
    lookups.get(0).sender.resolve(consumedTasks::add, createKnownAtDistance(5, 6));
    verifyConsumed(task, 2);
    task.call();
    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING);

    // Make current empty - still nothing new should happen
    for (int i = 1; i < Kademlia.MAX_OUTSTANDING; i++) {
      lookups.get(i).sender.resolve(consumedTasks::add, createKnownAtDistance(5, 6));
      verifyConsumed(task, i + 2);
    }
    task.call();
    Assertions.assertThat(lookups).hasSize(Kademlia.MAX_OUTSTANDING);

    // Make previous outstanding empty and thus entering the final round
    previousRoundLookup.sender.resolve(consumedTasks::add, createKnownAtDistance(5, 6));
    verifyConsumed(task, Kademlia.MAX_OUTSTANDING + 2);
    task.call();
    Assertions.assertThat(lookups).hasSize(Kademlia.BUCKET_SIZE);

    for (int i = Kademlia.MAX_OUTSTANDING; i < Kademlia.BUCKET_SIZE; i++) {
      // Even if finding closer in the final lookup we should not make any further requests
      lookups.get(i).sender.resolve(consumedTasks::add, createKnownAtDistance(5, 2));
      task.call();
      Assertions.assertThat(lookups).hasSize(Kademlia.BUCKET_SIZE);
    }
  }

  private void verifyConsumed(LookupTask task, int size) {
    Assertions.assertThat(consumedTasks).hasSize(size).allMatch(task::equals);
  }

  private LookupTask createTask(int numberOfKnown) {
    List<KademliaNode> known = createKnownAtDistance(numberOfKnown, 5);
    return new LookupTask(
        (sender, node, toLookup) -> lookups.add(new Lookup(sender, node, toLookup)),
        toLookup,
        myId,
        known);
  }

  private List<KademliaNode> createKnownAtDistance(int numberOfKnown, int distance) {
    return Stream.generate(() -> createAtDistanceToLookup(distance))
        .limit(numberOfKnown)
        .collect(Collectors.toList());
  }

  private KademliaNode createAtDistanceToLookup(int distance) {
    InetAddress localHost;
    try {
      localHost = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    }
    return new KademliaNode(toLookup.createForDistance(distance), localHost, 2222);
  }

  private static final class Lookup {

    private final ResolvableLookup sender;
    private final KademliaNode node;
    private final KademliaKey toLookup;

    private Lookup(ResolvableLookup sender, KademliaNode node, KademliaKey toLookup) {
      this.sender = sender;
      this.node = node;
      this.toLookup = toLookup;
    }
  }
}
