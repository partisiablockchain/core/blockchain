package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Objects;

/**
 * Address in the flooding network, including both host and port.
 *
 * @param hostname Hostname of the address. Not nullable.
 * @param port Network port of the address. Must be in the port range [0, 65535].
 */
public record Address(String hostname, int port) {

  /**
   * Constructor for {@link Address}.
   *
   * @param hostname Hostname of the address. Not nullable.
   * @param port Network port of the address. Must be in the port range [0, 65535].
   */
  public Address {
    // Invariants
    if (port < 0 || 65535 < port) {
      throw new IllegalArgumentException(
          "Port must be in range [0, 65535], but was %d".formatted(port));
    }

    // Null checks
    Objects.requireNonNull(hostname);
  }

  /**
   * Splits the input into two different components used in the constructor.
   *
   * @param address the address to parse
   * @return the parsed address
   */
  public static Address parseAddress(final String address) {
    final String[] addressSplit = address.split(":", -1);
    if (addressSplit.length != 2) {
      throw new IllegalArgumentException(
          "Given string did not match expected address format <HOSTNAME>:<PORT>");
    }
    return new Address(addressSplit[0], Integer.parseInt(addressSplit[1]));
  }

  /**
   * Alias of {@link #parseAddress} for Jackson support.
   *
   * @param address the address to parse
   * @return the parsed address
   * @see <a
   *     href="https://github.com/FasterXML/jackson-databind/blob/2.18/src/main/java/com/fasterxml/jackson/databind/introspect/BasicBeanDescription.java#L580">Jackson
   *     Source Code</a>
   */
  public static Address fromString(final String address) {
    return parseAddress(address);
  }

  @Override
  public String toString() {
    return "%s:%d".formatted(hostname, port);
  }
}
