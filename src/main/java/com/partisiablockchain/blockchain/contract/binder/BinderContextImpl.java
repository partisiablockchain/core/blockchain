package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.crypto.Hash;

/**
 * Implements a contract context for binders, shared for each of the three types of contracts.
 *
 * @see PubBinderContextImpl
 * @see ZkBinderContextImpl
 * @see SysBinderContextImpl
 */
public abstract class BinderContextImpl implements SharedContext {

  private final BlockchainContractContext contractContext;
  private final ExecutionContextTransaction executionContext;

  BinderContextImpl(
      BlockchainContractContext contractContext, ExecutionContextTransaction executionContext) {
    this.contractContext = contractContext;
    this.executionContext = executionContext;
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return contractContext.getContractAddress();
  }

  @Override
  public long getBlockTime() {
    return executionContext.getBlockTime();
  }

  @Override
  public long getBlockProductionTime() {
    return executionContext.getBlockProductionTime();
  }

  @Override
  public BlockchainAddress getFrom() {
    return executionContext.getFrom();
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return executionContext.getTransactionHash();
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return executionContext.getOriginatingTransactionHash();
  }

  ExecutionContextTransaction getExecutionContext() {
    return executionContext;
  }
}
