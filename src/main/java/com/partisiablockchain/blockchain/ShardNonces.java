package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** The nonces of a shards. */
@Immutable
public final class ShardNonces implements StateSerializable {

  /** Empty shard nonces instance. */
  public static final ShardNonces EMPTY = new ShardNonces(1, 0, 1, false);

  private final long inbound;
  private final long latestInboundCommitteeId;
  private final long outBound;
  private final boolean missingSync;

  @SuppressWarnings("unused")
  private ShardNonces() {
    this.inbound = 0L;
    this.latestInboundCommitteeId = 0L;
    this.outBound = 0L;
    missingSync = false;
  }

  private ShardNonces(
      long inbound, long latestInboundCommitteeId, long outBound, boolean missingSync) {
    this.inbound = inbound;
    this.latestInboundCommitteeId = latestInboundCommitteeId;
    this.outBound = outBound;
    this.missingSync = missingSync;
  }

  ShardNonces bumpOutbound() {
    return new ShardNonces(inbound, latestInboundCommitteeId, outBound + 1, missingSync);
  }

  ShardNonces bumpInbound(long committeeId) {
    return new ShardNonces(inbound + 1, committeeId, outBound, missingSync);
  }

  long getOutbound() {
    return outBound;
  }

  long getLatestInboundCommitteeId() {
    return latestInboundCommitteeId;
  }

  long getInbound() {
    return inbound;
  }

  ShardNonces markMissingSync() {
    if (missingSync) {
      throw new IllegalStateException("Unable to mark as missing sync when already marked");
    }
    return new ShardNonces(inbound, latestInboundCommitteeId, outBound, true);
  }

  ShardNonces markSyncReceived() {
    if (!missingSync) {
      throw new IllegalStateException("Unable to mark sync received when not missing sync");
    }
    return new ShardNonces(inbound, latestInboundCommitteeId, outBound, false);
  }

  boolean isMissingSync() {
    return missingSync;
  }
}
