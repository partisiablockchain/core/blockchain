package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.ContractIdentifiers;
import com.partisiablockchain.binder.ContractUpgradePermit;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.ChainPlugin.SerializedPlugin;
import com.partisiablockchain.blockchain.ImmutableChainState.SerializableChainState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContractContext;
import com.partisiablockchain.blockchain.contract.binder.SysBlockchainContract;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.MemoryStateStorage;
import com.partisiablockchain.blockchain.transaction.PendingByocFee;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.ReturnEnvelope;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mutable {@link ChainState} that represents the blockchain state as it is being modified and
 * updated within a {@link Block}.
 *
 * <p>Exists only within individual block producers, and is not stored, nor transferred between
 * nodes.
 *
 * @see ImmutableChainState variant used to store and transfer data on the blockchain.
 */
public final class MutableChainState extends ChainState implements Routing {

  private static final Logger logger = LoggerFactory.getLogger(MutableChainState.class);
  private AvlTree<BlockchainAddress, AccountState> accounts;
  private AvlTree<BlockchainAddress, ContractState> contracts;
  private Map<
          ChainPluginType,
          ChainPlugin<
              StateSerializable,
              ? extends StateSerializable,
              ? extends BlockchainPlugin<StateSerializable, StateSerializable>>>
      plugins;
  private AvlTree<String, String> features;
  private transient Set<BlockchainAddress> newContracts;
  private transient Set<BlockchainAddress> updatedContracts;
  private transient Set<BlockchainAddress> removedContracts;
  private AvlTree<ShardId, ShardNonces> shards;
  private FixedList<String> activeShards;
  private long governanceVersion;

  private MutableChainState(MutableChainState state) {
    super(state.getContext());
    apply(state);
  }

  MutableChainState(
      ChainStateCache context,
      AvlTree<ChainPluginType, SerializedPlugin> plugins,
      AvlTree<ChainPluginType, ChainPlugin.LocalPlugin> localPluginState,
      AvlTree<BlockchainAddress, AccountState> accounts,
      AvlTree<BlockchainAddress, ContractState> contracts,
      AvlTree<String, String> features,
      AvlTree<ShardId, ShardNonces> shards,
      FixedList<String> activeShards,
      long governanceVersion) {
    super(context);
    this.features = features;
    this.governanceVersion = governanceVersion;
    this.plugins = new HashMap<>();
    SerializedPlugin feePlugin = plugins.getValue(ChainPluginType.ACCOUNT);
    if (feePlugin != null) {
      this.plugins.put(
          ChainPluginType.ACCOUNT,
          feePlugin.asPlugin(
              getPluginSerialization(), localPluginState.getValue(ChainPluginType.ACCOUNT)));
    }
    SerializedPlugin consensusPlugin = plugins.getValue(ChainPluginType.CONSENSUS);
    if (consensusPlugin != null) {
      this.plugins.put(
          ChainPluginType.CONSENSUS,
          consensusPlugin.asPlugin(
              getPluginSerialization(), localPluginState.getValue(ChainPluginType.CONSENSUS)));
    }
    SerializedPlugin routingPlugin = plugins.getValue(ChainPluginType.ROUTING);
    if (routingPlugin != null) {
      this.plugins.put(
          ChainPluginType.ROUTING,
          routingPlugin.asPlugin(
              getPluginSerialization(), localPluginState.getValue(ChainPluginType.ROUTING)));
    }
    SerializedPlugin sharedObjectStorePlugin =
        plugins.getValue(ChainPluginType.SHARED_OBJECT_STORE);
    if (sharedObjectStorePlugin != null) {
      this.plugins.put(
          ChainPluginType.SHARED_OBJECT_STORE,
          sharedObjectStorePlugin.asPlugin(
              getPluginSerialization(),
              localPluginState.getValue(ChainPluginType.SHARED_OBJECT_STORE)));
    }
    this.accounts = accounts;
    this.contracts = contracts;
    this.shards = shards;
    this.activeShards = activeShards;
    this.newContracts = new HashSet<>();
    this.updatedContracts = new HashSet<>();
    this.removedContracts = new HashSet<>();
  }

  static MutableChainState emptyMaster(ChainStateCache context) {
    return new MutableChainState(
        context,
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        FixedList.create(),
        0);
  }

  static boolean isShardActive(FixedList<String> shards, String subChainId) {
    return subChainId == null || shards.contains(subChainId);
  }

  @Override
  public Set<BlockchainAddress> getContracts() {
    return contracts.keySet();
  }

  @Override
  public FixedList<String> getActiveShards() {
    return activeShards;
  }

  @Override
  public Hash getPluginJarIdentifier(ChainPluginType type) {
    ChainPlugin<?, ?, ?> plugin = plugins.get(type);
    if (plugin == null) {
      return null;
    } else {
      return plugin.getIdentifier().getJarHash();
    }
  }

  @Override
  public Set<BlockchainAddress> getAccounts() {
    return accounts.keySet();
  }

  @Override
  public AccountState getAccount(BlockchainAddress address) {
    AccountState value = accounts.getValue(address);
    if (value == null && features().hasOpenAccountCreation()) {
      if (!features().hasCheckExistenceForContractWithOpenAccount()) {
        value = AccountState.create();
      } else if (address.getType().equals(BlockchainAddress.Type.ACCOUNT)) {
        value = AccountState.create();
      }
    }
    return value;
  }

  /**
   * Adds an account to the system.
   *
   * @param address the account to create
   */
  public void createAccount(BlockchainAddress address) {
    if (address.getType() != BlockchainAddress.Type.ACCOUNT) {
      throw new IllegalArgumentException("Only Type.ACCOUNT allowed as account: " + address);
    }
    if (accounts.getValue(address) != null) {
      throw new IllegalStateException("Unable to create existing account " + address);
    }
    setAccount(address, AccountState.create());
  }

  void setAccount(BlockchainAddress address, AccountState accountState) {
    accounts = accounts.set(address, accountState);
  }

  /**
   * Notify the account plugin that the contract has been created to enable storage fee.
   *
   * @param contract the newly created contract
   * @param blockProductionTime the production for the contract's block
   */
  public void contractCreated(BlockchainAddress contract, long blockProductionTime) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            BlockchainAccountPlugin.ContractState<L, C> updatedLocal =
                plugin.contractCreated(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    local.getContextFree(),
                    contract);
            return local
                .setContextFree(updatedLocal.local)
                .setContract(contract, updatedLocal.contract);
          }
        });
  }

  /**
   * Removes an account from the system, moving any values to the beneficiary.
   *
   * @param contract the account to remove. Non-nullable.
   * @param blockProductionTime Timestamp of the block being executed.
   */
  private void removeContractAccount(BlockchainAddress contract, long blockProductionTime) {
    logger.info("Removing contract {}", contract);
    removedContracts.add(contract);

    ContractState contractState = contracts.getValue(contract);
    final long pendingCallbackGas = contractState.calculateAllocatedCostForPendingCallbacks();

    contracts = contracts.remove(contract);
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            L removeContractContextFree =
                plugin.removeContract(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    local.getContractState(contract));
            L registerBlockchainUsageContextFree =
                plugin.registerBlockchainUsage(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    removeContractContextFree,
                    pendingCallbackGas);
            return local
                .setContextFree(registerBlockchainUsageContextFree)
                .setContract(contract, null);
          }
        });
  }

  /** Bump the governance version running. */
  public void bumpGovernanceVersion() {
    this.governanceVersion++;
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  private void updateAccountPluginInternal(AccountPluginUpdate update) {
    this.plugins.computeIfPresent(
        ChainPluginType.ACCOUNT, (s, plugin) -> typedAccountUpdate(update, (ChainPlugin) plugin));
  }

  private <
          G extends StateSerializable,
          A extends StateSerializable,
          C extends StateSerializable,
          L extends StateSerializable>
      ChainPlugin<?, ?, ?> typedAccountUpdate(
          AccountPluginUpdate update,
          ChainPlugin<G, AccountPluginState<L, A, C>, BlockchainAccountPlugin<G, L, A, C>> asd) {
    AccountPluginState<L, A, C> local =
        update.apply(asd.getGlobalState(), asd.getLocalState(), asd.getPlugin());
    return asd.setLocalState(local);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  private void updateConsensusPluginInternal(ConsensusPluginUpdate update) {
    this.plugins.computeIfPresent(
        ChainPluginType.CONSENSUS,
        (s, plugin) -> typedConsensusUpdate(update, (ChainPlugin) plugin));
  }

  private <GlobalT extends StateSerializable, LocalT extends StateSerializable>
      ChainPlugin<?, ?, ?> typedConsensusUpdate(
          ConsensusPluginUpdate update,
          ChainPlugin<GlobalT, LocalT, BlockchainConsensusPlugin<GlobalT, LocalT>> plugin) {

    GlobalT globalState = plugin.getGlobalState();
    LocalT localState = plugin.getLocalState();

    LocalT newState = update.apply(globalState, localState, plugin.getPlugin());
    return plugin.setLocalState(newState);
  }

  @Override
  ContractState getContract(BlockchainAddress address) {
    return contracts.getValue(address);
  }

  @Override
  public Long getContractStorageLength(BlockchainAddress address) {
    ContractState contractState = contracts.getValue(address);
    if (contractState != null) {
      return contractState.computeStorageLength();
    } else {
      return null;
    }
  }

  /**
   * Sets the contract own state for the given contract.
   *
   * @param contractAddress the contract
   * @param state the state to set
   */
  public void setContractState(BlockchainAddress contractAddress, StateSerializable state) {
    updatedContracts.add(contractAddress);
    ContractState value = getContract(contractAddress);
    setContract(contractAddress, saveContract(contractAddress, value, state));
  }

  private ContractState saveContract(
      BlockchainAddress contractAddress, ContractState value, StateSerializable state) {
    BlockchainContract<StateSerializable, BinderEvent> blockchainContract =
        getBlockchainContract(contractAddress);
    SerializationResult result =
        blockchainContract.getContractSerialization().write(serializer, state);
    return value.withHashAndSize(result.hash(), result.totalByteCount());
  }

  /**
   * Create a new contract in the chain state with uninitialized state.
   *
   * @param <T> the type of the contract state
   * @param <EventT> the type of event
   * @param contractAddress the address of the contract to create
   * @param core the core contract information
   * @return the BlockchainContract associated with the new contract
   */
  public <T extends StateSerializable, EventT extends BinderEvent>
      BlockchainContract<T, EventT> createContract(
          BlockchainAddress contractAddress, CoreContractState core) {
    if (contracts.containsKey(contractAddress)) {
      throw new RuntimeException(
          "There already exists a contract for the given address: " + contractAddress.toString());
    }
    setContract(contractAddress, ContractState.create(core));
    newContracts.add(contractAddress);
    return getBlockchainContract(contractAddress);
  }

  void updateForBlock(Block block) {
    List<BlockchainAddress> toUpdate = new ArrayList<>();
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> localState,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            L nextState =
                plugin.updateForBlock(
                    globalState,
                    localState.getContextFree(),
                    new BlockchainAccountPlugin.BlockContext<>() {
                      @Override
                      public Block currentBlock() {
                        return block;
                      }

                      @Override
                      public void registerContractUpdate(BlockchainAddress contract) {
                        toUpdate.add(contract);
                      }

                      @Override
                      public AvlTree<BlockchainAddress, C> contracts() {
                        return localState.getContracts();
                      }
                    });
            return localState.setContextFree(nextState);
          }
        });

    for (BlockchainAddress blockchainAddress : toUpdate) {
      updateActiveContract(block, blockchainAddress);
    }

    updateConsensusPluginInternal(
        new ConsensusPluginUpdate() {
          @Override
          public <GlobalT extends StateSerializable, LocalT extends StateSerializable> LocalT apply(
              GlobalT globalState,
              LocalT local,
              BlockchainConsensusPlugin<GlobalT, LocalT> plugin) {
            return plugin.updateForBlock(block::getProductionTime, globalState, local, block);
          }
        });
  }

  /**
   * Updates the account plugin for the given contract.
   *
   * <p>Following changes occur:
   *
   * <ul>
   *   <li>Collects storage fees from the contract.
   *   <li>Contract is <b>iced</b> if it doesn't have enough gas, but it has stakes.
   *   <li>Contract is <b>removed</b> if it doesn't have enough gas nor stakes.
   * </ul>
   *
   * @param block The block to include this update in. Not nullable.
   * @param contract Address of the contract. Not nullable.
   * @return true if and only if the contract was iced.
   */
  boolean updateActiveContract(Block block, BlockchainAddress contract) {
    Objects.requireNonNull(block, "Block must not be nullable");
    Objects.requireNonNull(contract, "Contract address must not be nullable");

    final Long storageLength = getContractStorageLength(contract);
    final AtomicBoolean dead = new AtomicBoolean(false);
    final AtomicBoolean iced = new AtomicBoolean(false);
    if (storageLength != null) {
      updateAccountPluginInternal(
          new AccountPluginUpdate() {
            @Override
            public <
                    G extends StateSerializable,
                    A extends StateSerializable,
                    C extends StateSerializable,
                    L extends StateSerializable>
                AccountPluginState<L, A, C> apply(
                    G globalState,
                    AccountPluginState<L, A, C> localState,
                    BlockchainAccountPlugin<G, L, A, C> plugin) {

              BlockchainAccountPlugin.IcedContractState<L, C> nextState =
                  plugin.updateActiveContractIced(
                      block::getProductionTime,
                      globalState,
                      localState.getContractState(contract),
                      contract,
                      storageLength);
              if (nextState.contractState() == null) {
                dead.set(true);
                return localState;
              } else {
                iced.set(nextState.iced());
                return localState
                    .setContextFree(nextState.contractState().local)
                    .setContract(contract, nextState.contractState().contract);
              }
            }
          });

      if (dead.get()) {
        removeContract(contract, block.getProductionTime());
      }
    }
    return iced.get();
  }

  /**
   * Checks whether a contract exists with the given {@link BlockchainAddress}.
   *
   * @param blockchainAddress Contract address to check. Not nullable.
   * @return True if and only contract exists.
   */
  boolean existsContract(BlockchainAddress blockchainAddress) {
    Objects.requireNonNull(blockchainAddress, "blockchainAddress must be non-null!");
    return contracts.containsKey(blockchainAddress);
  }

  void setContract(BlockchainAddress blockchainAddress, ContractState contractState) {
    Objects.requireNonNull(blockchainAddress, "blockchainAddress must be non-null!");
    contracts = contracts.set(blockchainAddress, contractState);
  }

  /**
   * Remove a contract from the chain state.
   *
   * @param blockchainAddress the address of the contract to remove. Not nullable.
   * @param blockProductionTime Timestamp of the block being executed.
   */
  public void removeContract(BlockchainAddress blockchainAddress, long blockProductionTime) {
    Objects.requireNonNull(blockchainAddress, "blockchainAddress must be non-null!");
    removeContractAccount(blockchainAddress, blockProductionTime);
  }

  /**
   * Convert this MutableChainState to an ImmutableChainState.
   *
   * @param chainId id the the chain
   * @param subChainId id of the current shard
   * @param spawnedEvents the events spawning the the execution
   * @param executionStatus the execution status for transactions leading to this chain state
   * @return the constructed ImmutableChainState
   */
  ImmutableChainState asImmutable(
      String chainId,
      String subChainId,
      AvlTree<Hash, Hash> spawnedEvents,
      AvlTree<Hash, Boolean> executionStatus) {
    AvlTree<ChainPluginType, SerializedPlugin> plugins = AvlTree.create();
    AvlTree<ChainPluginType, ChainPlugin.LocalPlugin> localPluginState = AvlTree.create();
    for (ChainPluginType pluginType : ChainPluginType.values()) {
      ChainPlugin<?, ?, ?> chainPlugin = this.plugins.get(pluginType);
      if (chainPlugin != null) {
        plugins = plugins.set(pluginType, chainPlugin.serialize(getPluginSerialization()));
        localPluginState =
            localPluginState.set(pluginType, chainPlugin.serializeLocal(getPluginSerialization()));
      }
    }

    getContext().markContractsForRemoval(removedContracts);
    return new ImmutableChainState(
        getContext(),
        new ExecutedState(
            new SerializableChainState(
                accounts,
                contracts,
                plugins,
                localPluginState,
                features,
                shards,
                activeShards,
                governanceVersion),
            chainId,
            subChainId,
            spawnedEvents,
            executionStatus));
  }

  /**
   * Pays fee of public blockchain usage - which sends the money to the governance structure.
   *
   * @param blockProductionTime block production time
   * @param address paying account
   * @param transaction entity to pay for
   * @return the gas left for the remainder of the transaction
   */
  public long payFee(
      long blockProductionTime, BlockchainAddress address, SignedTransaction transaction) {
    AtomicLong atomicLong = new AtomicLong(transaction.getCost());
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> localState,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            BlockchainAccountPlugin.PayCostResult<L, A> nextAccount =
                plugin.payCost(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    localState.getAccountState(address),
                    transaction);
            atomicLong.set(nextAccount.remainingGas());
            return localState
                .setContextFree(nextAccount.accountState().local)
                .setAccount(address, nextAccount.accountState().account);
          }
        });
    return atomicLong.get();
  }

  /**
   * Updates the balance of a contract. Used to redistribute surplus gas to the contract and to let
   * the contract pay for outgoing events.
   *
   * @param blockProductionTime block production time
   * @param address the contract to receive surplus
   * @param gasDiff amount of gas to modify the balance by
   */
  public void updateContractBalance(
      long blockProductionTime, BlockchainAddress address, long gasDiff) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            BlockchainAccountPlugin.ContractState<L, C> updatedContractState =
                plugin.updateContractGasBalance(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    local.getContractState(address),
                    address,
                    gasDiff);
            return local
                .setContextFree(updatedContractState.local)
                .setContract(address, updatedContractState.contract);
          }
        });
  }

  /**
   * Register blockchain usage.
   *
   * @param blockProductionTime current time for the block
   * @param gas amount of gas that has been used
   */
  public void registerBlockchainUsage(long blockProductionTime, long gas) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            L contextFree =
                plugin.registerBlockchainUsage(
                    new BlockProduction(blockProductionTime),
                    globalState,
                    local.getContextFree(),
                    gas);
            return local.setContextFree(contextFree);
          }
        });
  }

  /**
   * Pay pending service fees. If the paying contract is not able to cover the total cost it will be
   * deleted.
   *
   * @param blockProductionTime block production time
   * @param pendingFees the fees pending being paid
   * @param payer the contract that should pay the fees
   */
  public void payServiceFees(
      long blockProductionTime, List<PendingFee> pendingFees, BlockchainAddress payer) {
    if (!pendingFees.isEmpty()) {
      AtomicBoolean coveredFee = new AtomicBoolean(true);
      updateAccountPluginInternal(
          new AccountPluginUpdate() {
            @Override
            public <
                    G extends StateSerializable,
                    A extends StateSerializable,
                    C extends StateSerializable,
                    L extends StateSerializable>
                AccountPluginState<L, A, C> apply(
                    G globalState,
                    AccountPluginState<L, A, C> local,
                    BlockchainAccountPlugin<G, L, A, C> plugin) {
              PayServiceFeesResult<BlockchainAccountPlugin.ContractState<L, C>> result =
                  plugin.payServiceFees(
                      new BlockProduction(blockProductionTime),
                      globalState,
                      local.getContractState(payer),
                      pendingFees);
              BlockchainAccountPlugin.ContractState<L, C> updatedState = result.getResult();
              coveredFee.set(result.contractCoveredFee());

              return local
                  .setContextFree(updatedState.local)
                  .setContract(payer, updatedState.contract);
            }
          });
      if (!coveredFee.get()) {
        logger.debug("Contract {} was unable to cover total cost of service fees", payer);
        removeContractAccount(payer, blockProductionTime);
      }
    }
  }

  /**
   * Pay pending infrastructure fees. The cost will be covered from a pool on the fee distribution
   * contract.
   *
   * @param blockProductionTime block production time
   * @param pendingFees the fees pending being paid
   */
  public void payInfrastructureFees(long blockProductionTime, List<PendingFee> pendingFees) {
    if (!pendingFees.isEmpty()) {
      updateAccountPluginInternal(
          new AccountPluginUpdate() {
            @Override
            public <
                    G extends StateSerializable,
                    A extends StateSerializable,
                    C extends StateSerializable,
                    L extends StateSerializable>
                AccountPluginState<L, A, C> apply(
                    G globalState,
                    AccountPluginState<L, A, C> local,
                    BlockchainAccountPlugin<G, L, A, C> plugin) {
              L updatedState =
                  plugin.payInfrastructureFees(
                      new BlockProduction(blockProductionTime),
                      globalState,
                      local.getContextFree(),
                      pendingFees);
              return local.setContextFree(updatedState);
            }
          });
    }
  }

  /**
   * Pay pending fees related to BYOC transfers.
   *
   * @param blockProductionTime the current time
   * @param byocFees the pending BYOC fees
   */
  public void payByocServiceFees(long blockProductionTime, List<PendingByocFee> byocFees) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            L contextFreeState = local.getContextFree();
            for (PendingByocFee fee : byocFees) {
              contextFreeState =
                  plugin.payByocFees(
                      new BlockProduction(blockProductionTime),
                      globalState,
                      contextFreeState,
                      fee.getAmount(),
                      fee.getSymbol(),
                      fee.getNodes());
            }
            return local.setContextFree(contextFreeState);
          }
        });
  }

  /**
   * Bump nonce for account.
   *
   * @param account account address
   */
  public void bumpNonce(BlockchainAddress account) {
    setAccount(account, getAccount(account).bumpNonce());
  }

  /**
   * Apply the new state to this state.
   *
   * @param state the state which will overwrite this state.
   */
  public void apply(MutableChainState state) {
    this.accounts = state.accounts;
    this.contracts = state.contracts;
    this.plugins = new HashMap<>(state.plugins);
    this.features = state.features;
    this.newContracts = new HashSet<>(state.newContracts);
    this.updatedContracts = new HashSet<>(state.updatedContracts);
    this.removedContracts = new HashSet<>(state.removedContracts);
    this.shards = state.shards;
    this.activeShards = state.activeShards;
    this.governanceVersion = state.governanceVersion;
  }

  /**
   * Create copy of state.
   *
   * @return created copy
   */
  public MutableChainState copy() {
    return new MutableChainState(this);
  }

  @Override
  public AccountPluginState<?, ?, ?> getLocalAccountPluginState() {
    return (AccountPluginState<?, ?, ?>) getLocalState(ChainPluginType.ACCOUNT);
  }

  @Override
  public StateSerializable getLocalConsensusPluginState() {
    return getLocalState(ChainPluginType.CONSENSUS);
  }

  private StateSerializable getLocalState(ChainPluginType type) {
    ChainPlugin<?, ?, ?> plugin = plugins.get(type);
    if (plugin == null) {
      return null;
    } else {
      return plugin.getLocalState();
    }
  }

  @Override
  public StateSerializable getGlobalPluginState(ChainPluginType type) {
    ChainPlugin<?, ?, ?> plugin = plugins.get(type);
    if (plugin == null) {
      return null;
    } else {
      return plugin.getGlobalState();
    }
  }

  @Override
  @SuppressWarnings({"unchecked", "rawtypes"})
  public AccountPluginAccessor getAccountPlugin() {
    if (!plugins.containsKey(ChainPluginType.ACCOUNT)) {
      return new NoFeesAccountPlugin();
    } else {
      return new AccountPluginWrapper<>((ChainPlugin) plugins.get(ChainPluginType.ACCOUNT));
    }
  }

  @Override
  public Set<ChainPluginType> getChainPluginTypes() {
    return plugins.keySet();
  }

  @Override
  public LargeByteArray getPluginJar(ChainPluginType type) {
    Hash identifier = getPluginJarIdentifier(type);
    if (identifier == null) {
      return null;
    } else {
      return getContext().createStateSerializer().read(identifier, LargeByteArray.class);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public ConsensusPluginAccessor getConsensusPlugin() {
    if (!plugins.containsKey(ChainPluginType.CONSENSUS)) {
      return defaultConsensus();
    } else {
      ChainPlugin<StateSerializable, ?, ?> plugin = plugins.get(ChainPluginType.CONSENSUS);
      return new ConsensusPluginWrapper<>(
          (ChainPlugin<
                  StateSerializable,
                  StateSerializable,
                  BlockchainConsensusPlugin<StateSerializable, StateSerializable>>)
              plugin);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public RoutingPluginAccessor getRoutingPlugin() {
    if (!plugins.containsKey(ChainPluginType.ROUTING)) {
      return defaultRouting();
    } else {
      ChainPlugin<?, ?, ?> plugin = plugins.get(ChainPluginType.ROUTING);
      return new RoutingPluginWrapper(
          (ChainPlugin<StateVoid, StateVoid, BlockchainRoutingPlugin>) plugin);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public SharedObjectStorePluginAccessor getSharedObjectStorePlugin() {
    if (!plugins.containsKey(ChainPluginType.SHARED_OBJECT_STORE)) {
      return defaultSharedObjectStore();
    } else {
      ChainPlugin<?, ?, ?> plugin = plugins.get(ChainPluginType.SHARED_OBJECT_STORE);
      return new SharedObjectStorePluginWrapper<>(
          (ChainPlugin<
                  StateSerializable,
                  StateVoid,
                  BlockchainSharedObjectStorePlugin<StateSerializable>>)
              plugin);
    }
  }

  @Override
  public PluginInteractionCreator getPluginInteractions(ChainPluginType pluginType) {
    ChainPlugin<?, ?, ?> plugin = plugins.get(pluginType);
    if (plugin == null) {
      return null;
    } else {
      return plugin.getPlugin();
    }
  }

  /**
   * Get the current value of a feature.
   *
   * @param key the feature key
   * @return the current value of the feature or null if not set
   */
  public String getFeature(String key) {
    return features.getValue(key);
  }

  /**
   * Set a feature in the state.
   *
   * @param key the key of the feature
   * @param value the value of the feature or null to remove the feature
   */
  public void setFeature(String key, String value) {
    this.features = setFeatureOrExit(features, key, value, Features.KNOWN_FEATURES, System::exit);
  }

  /**
   * Current feature set of the blockchain.
   *
   * @return accessor for inspecting enabled features
   */
  public Features features() {
    return new Features(features);
  }

  static AvlTree<String, String> setFeatureOrExit(
      AvlTree<String, String> features,
      String key,
      String value,
      List<String> knownFeatures,
      IntConsumer systemExit) {
    if (value == null) {
      return features.remove(key);
    } else if (knownFeatures.contains(key)) {
      return features.set(key, value);
    } else {
      systemExit.accept(1);
      return features;
    }
  }

  /**
   * Serialize and save jar.
   *
   * @param jar to serialize and save
   * @return result of serialization
   */
  public Hash saveJar(byte[] jar) {
    return serializer.write(new LargeByteArray(jar)).hash();
  }

  UpdateEvent resultingUpdateEvent() {
    return new UpdateEvent(
        Set.copyOf(newContracts), Set.copyOf(updatedContracts), Set.copyOf(removedContracts));
  }

  /**
   * Determines the destination shard for a target.
   *
   * @param target the intended target to interact with, null indicates global state change
   * @return the string identifier of the subshard, null for governance shard
   */
  public String determineRoute(BlockchainAddress target) {
    if (target != null) {
      return getRoutingPlugin().route(getActiveShards(), target);
    } else {
      return null;
    }
  }

  @Override
  public ShardRoute routeToShard(BlockchainAddress target) {
    String targetShard = determineRoute(target);
    return routeToShard(targetShard);
  }

  /**
   * Finds the routing information for a shard and bumps the nonce.
   *
   * @param targetShard the shard to send to
   * @return the resulting route
   */
  @Override
  public ShardRoute routeToShard(String targetShard) {
    ShardId shardId = new ShardId(targetShard);
    ShardNonces nonces = getShardNonce(shardId);
    shards = shards.set(shardId, nonces.bumpOutbound());
    return new ShardRoute(targetShard, nonces.getOutbound());
  }

  /**
   * Bump the inbound nonce for the given shard.
   *
   * @param originShard the origin shard
   * @param committeeId committee id for this event
   */
  public void bumpInboundShardNonce(String originShard, long committeeId) {
    ShardId shardId = new ShardId(originShard);
    ShardNonces shardNonce = getShardNonce(shardId);
    shards = shards.set(shardId, shardNonce.bumpInbound(committeeId));
  }

  ShardNonces getShardNonce(String shard) {
    return getShardNonce(new ShardId(shard));
  }

  private ShardNonces getShardNonce(ShardId shardId) {
    return Objects.requireNonNullElse(shards.getValue(shardId), ShardNonces.EMPTY);
  }

  /**
   * Update the local state of a plugin.
   *
   * @param type the plugin to update
   * @param context the account context for the update
   * @param invocation the rpc to send to the plugin
   * @param blockProductionTime the production time for the block
   * @return return value of invocation
   */
  public byte[] updateLocalPluginState(
      ChainPluginType type,
      BlockchainAddress context,
      byte[] invocation,
      long blockProductionTime) {
    if (context.getType() == BlockchainAddress.Type.ACCOUNT
        && features().hasOpenAccountCreation()) {
      ensureExistence(context);
    } else {
      assertExistence(context);
    }
    return updateLocalPluginStateNullableContext(type, context, invocation, blockProductionTime);
  }

  /**
   * Update the local state of a plugin.
   *
   * @param type the plugin to update
   * @param invocation the rpc to send to the plugin
   * @param blockProductionTime the production time for the block
   * @return return value of invocation
   */
  public byte[] updateLocalPluginState(
      ChainPluginType type, byte[] invocation, long blockProductionTime) {
    return updateLocalPluginStateNullableContext(type, null, invocation, blockProductionTime);
  }

  private byte[] updateLocalPluginStateNullableContext(
      ChainPluginType type,
      BlockchainAddress context,
      byte[] invocation,
      long blockProductionTime) {
    BlockchainPlugin.InvokeResult<
            ? extends
                ChainPlugin<
                    StateSerializable,
                    ? extends StateSerializable,
                    ? extends BlockchainPlugin<StateSerializable, StateSerializable>>>
        chainPluginInvokeResult =
            plugins
                .get(type)
                .invokeLocal(new BlockProduction(blockProductionTime), context, invocation);
    plugins.put(type, chainPluginInvokeResult.updatedState());
    return chainPluginInvokeResult.result();
  }

  private void assertExistence(BlockchainAddress context) {
    AvlTree<BlockchainAddress, ?> map =
        context.getType() == BlockchainAddress.Type.ACCOUNT ? accounts : contracts;
    if (!map.containsKey(context)) {
      throw new IllegalStateException("Supplied context does not exist " + context);
    }
  }

  private void ensureExistence(BlockchainAddress account) {
    if (!accounts.containsKey(account)) {
      setAccount(account, AccountState.create());
    }
  }

  /**
   * Update the global state of a plugin.
   *
   * @param blockProductionTime current block production time
   * @param type the plugin to update
   * @param invocation the rpc to send to the plugin
   * @return return value of invocation
   */
  public byte[] updateGlobalPluginState(
      long blockProductionTime, ChainPluginType type, byte[] invocation) {
    BlockchainPlugin.InvokeResult<
            ? extends
                ChainPlugin<
                    StateSerializable,
                    ? extends StateSerializable,
                    ? extends BlockchainPlugin<StateSerializable, StateSerializable>>>
        chainPluginInvokeResult =
            plugins.get(type).invokeGlobal(new BlockProduction(blockProductionTime), invocation);
    plugins.put(type, chainPluginInvokeResult.updatedState());
    return chainPluginInvokeResult.result();
  }

  /**
   * Update a plugin.
   *
   * @param syncSender callback used to create sync events
   * @param currentShardId the id of the active shard
   * @param type the plugin to update
   * @param jar the new jar file
   * @param rpc the migration rpc to supply to the plugin
   */
  public void setPlugin(
      BiConsumer<String, SyncEvent> syncSender,
      String currentShardId,
      ChainPluginType type,
      byte[] jar,
      byte[] rpc) {
    if (jar == null) {
      plugins.remove(type);
    } else {
      ChainPlugin<StateSerializable, ?, ?> plugin = plugins.get(type);
      plugins.put(type, ChainPlugin.create(getPluginSerialization(), plugin, saveJar(jar), rpc));
    }
    if (type == ChainPluginType.ROUTING) {
      routingChanged(syncSender, currentShardId, activeShards, activeShards);
    }
    logger.info("Updated {} plugin to {}", type, plugins.get(type));
  }

  /**
   * Adds callback for a contract.
   *
   * @param currentSender the sender of the transaction that registers callbacks
   * @param contract the contract to register callback for
   * @param returnEnvelope current transaction callback if any
   * @param sendingTransaction the sending transaction hash
   * @param callbackInfo the actual events with a triggered callback
   */
  public void addCallbacks(
      BlockchainAddress currentSender,
      BlockchainAddress contract,
      ReturnEnvelope returnEnvelope,
      Hash sendingTransaction,
      ContractState.CallbackInfo callbackInfo) {
    ContractState contractState =
        getContract(contract)
            .withCallbacks(returnEnvelope, currentSender, sendingTransaction, callbackInfo);
    setContract(contract, contractState);
  }

  /**
   * Updates the chain state with a completed transaction.
   *
   * @param contract the contract the send the original event
   * @param completedEventTransaction the event transaction that has completed
   * @param success if the completedTransaction was successful executed
   * @param callbackCreator used to trigger new callback events
   * @param returnValue the return value of the completedTransaction
   */
  public void callbackReceived(
      BlockchainAddress contract,
      Hash completedEventTransaction,
      boolean success,
      Consumer<CallbackCreator.Callback> callbackCreator,
      byte[] returnValue) {
    ContractState.CallbackResult callbackResult =
        getContract(contract).acknowledgeCallback(completedEventTransaction, success, returnValue);
    setContract(contract, callbackResult.contractState);
    if (callbackResult.callback != null) {
      callbackCreator.accept(callbackResult.callback);
    }
  }

  /**
   * Creates a callback context for a specific callback, most notably every event and it's execution
   * result is included.
   *
   * @param contract the contract address
   * @param callbackIdentifier the callback identifier
   * @return the callback context
   */
  public CallbackContext callbackContext(BlockchainAddress contract, Hash callbackIdentifier) {
    FixedList<CallbackContext.ExecutionResult> eventsForCallback =
        getContract(contract).findEventsForCallback(callbackIdentifier);
    return CallbackContext.create(eventsForCallback);
  }

  /**
   * Sets the result of a callback.
   *
   * @param contract the contract address
   * @param callbackIdentifier the callback identifier
   * @param success true if the contract signals success
   */
  public void setCallbackResult(
      BlockchainAddress contract, Hash callbackIdentifier, boolean success) {
    updateContract(
        contract, contractState -> contractState.callbackResult(callbackIdentifier, success));
  }

  /**
   * Called when a callback has been executed.
   *
   * @param contract the contract
   * @param callbackIdentifier the callback identifier
   * @param eventConsumer event consumer if a callback event is generated
   * @param returnValue the resulting return value
   */
  public void callbackExecutionComplete(
      BlockchainAddress contract,
      Hash callbackIdentifier,
      Consumer<InnerSystemEvent.CallbackEvent> eventConsumer,
      byte[] returnValue) {
    updateContract(
        contract,
        contractState ->
            contractState.callbackComplete(callbackIdentifier, eventConsumer, returnValue));
  }

  /**
   * Called when a callback has been executed but spawned more callbacks.
   *
   * @param contract the contract
   * @param callbackIdentifier the callback identifier
   * @param newCallback event consumer if a callback event is generated
   */
  public void callbackExecutionUpdated(
      BlockchainAddress contract, Hash callbackIdentifier, ContractState.CallbackInfo newCallback) {
    updateContract(
        contract, contractState -> contractState.callbackUpdated(callbackIdentifier, newCallback));
  }

  private void updateContract(
      BlockchainAddress contract, Function<ContractState, ContractState> update) {
    ContractState contractState = getContract(contract);
    if (contractState != null) {
      ContractState nextState = update.apply(contractState);
      setContract(contract, nextState);
    }
  }

  boolean isSyncing() {
    Stream<ShardId> shards = activeShards.stream().map(ShardId::new);
    return Stream.concat(shards, Stream.ofNullable(new ShardId(null)))
        .map(this::getShardNonce)
        .anyMatch(ShardNonces::isMissingSync);
  }

  private LargeByteArray readJar(Hash hash) {
    return serializer.read(hash, LargeByteArray.class);
  }

  private void writeJars(CoreContractState core, StateSerializer stateSerializer) {
    LargeByteArray binderJar = readJar(core.getBinderIdentifier());
    LargeByteArray abiJar = readJar(core.getAbiIdentifier());
    LargeByteArray contractJar = readJar(core.getContractIdentifier());
    stateSerializer.write(binderJar, LargeByteArray.class);
    stateSerializer.write(abiJar, LargeByteArray.class);
    stateSerializer.write(contractJar, LargeByteArray.class);
  }

  private SyncEvent.ContractTransfer createContractTransfer(
      BlockchainAddress contractAddress,
      StateSerializer stateSerializer,
      StateSerializable pluginState) {
    ContractState contractState = this.contracts.getValue(contractAddress);
    Hash contractStateHash = stateSerializer.write(contractState).hash();
    writeJars(contractState.getCore(), stateSerializer);
    StateSerializable innerContractState = getContractState(contractAddress);

    CoreContractState.ContractSerialization<StateSerializable> contractSerialization =
        getBlockchainContract(contractAddress).getContractSerialization();
    contractSerialization.write(stateSerializer, innerContractState);

    SerializationResult serializationResult = stateSerializer.write(pluginState);
    Hash pluginStateHash = serializationResult.hash();
    return new SyncEvent.ContractTransfer(contractAddress, contractStateHash, pluginStateHash);
  }

  private SyncEvent.AccountTransfer createAccountTransfer(
      BlockchainAddress accountAddress,
      StateSerializer stateSerializer,
      StateSerializable pluginState) {
    AccountState accountState = this.accounts.getValue(accountAddress);
    SerializationResult serializationResult = stateSerializer.write(accountState);
    Hash accountStateHash = serializationResult.hash();
    serializationResult = stateSerializer.write(pluginState);
    Hash pluginStateHash = serializationResult.hash();
    return new SyncEvent.AccountTransfer(accountAddress, accountStateHash, pluginStateHash);
  }

  void routingChanged(
      BiConsumer<String, SyncEvent> syncSender,
      String shardId,
      FixedList<String> previousShards,
      FixedList<String> nextShards) {
    boolean partOfNextRouting = isShardActive(nextShards, shardId);
    if (partOfNextRouting) {
      otherChains(shardId, previousShards).forEach(this::markPendingSync);
    }

    boolean partOfCurrentRouting = isShardActive(previousShards, shardId);
    if (partOfCurrentRouting) {
      Map<String, List<SyncEvent.AccountTransfer>> accountsToTransfer = new HashMap<>();
      Map<String, List<SyncEvent.ContractTransfer>> contractsToTransfer = new HashMap<>();
      Map<String, MemoryStateStorage> storages = new HashMap<>();
      Map<String, StateSerializer> serializers = new HashMap<>();

      for (String otherShard : otherChains(shardId, nextShards)) {
        accountsToTransfer.put(otherShard, new ArrayList<>());
        contractsToTransfer.put(otherShard, new ArrayList<>());
        MemoryStateStorage storage = new MemoryStateStorage();
        storages.put(otherShard, storage);
        StateSerializer stateSerializer = new StateSerializer(storage, true, true);
        serializers.put(otherShard, stateSerializer);
      }

      List<BlockchainAddress> contractsToRemove = new ArrayList<>();
      AccountPluginState<?, ?, ?> accountPluginState = this.getLocalAccountPluginState();
      for (BlockchainAddress contractAddress : getContracts()) {
        String destination = determineRoute(contractAddress);
        if (!Objects.equals(destination, shardId)) {
          StateSerializable accountPluginStateContract =
              accountPluginState == null ? null : accountPluginState.getContract(contractAddress);
          SyncEvent.ContractTransfer contractTransfer =
              createContractTransfer(
                  contractAddress, serializers.get(destination), accountPluginStateContract);
          contractsToTransfer.get(destination).add(contractTransfer);
          this.contracts = this.contracts.remove(contractAddress);
          contractsToRemove.add(contractAddress);
          removedContracts.add(contractAddress);
        }
      }

      List<BlockchainAddress> accountsToRemove = new ArrayList<>();
      for (BlockchainAddress accountAddress : getAccounts()) {
        String destination = determineRoute(accountAddress);
        if (!Objects.equals(destination, shardId)) {
          StateSerializable accountPluginStateAccount =
              accountPluginState == null ? null : accountPluginState.getAccount(accountAddress);
          SyncEvent.AccountTransfer accountTransfer =
              createAccountTransfer(
                  accountAddress, serializers.get(destination), accountPluginStateAccount);
          accountsToTransfer.get(destination).add(accountTransfer);
          this.accounts = this.accounts.remove(accountAddress);
          accountsToRemove.add(accountAddress);
        }
      }
      removeFromAccountPlugin(contractsToRemove, accountsToRemove);

      for (String otherShard : otherChains(shardId, nextShards)) {
        SyncEvent syncEvent =
            new SyncEvent(
                accountsToTransfer.get(otherShard),
                contractsToTransfer.get(otherShard),
                storages.get(otherShard).getData().values().stream().toList());
        syncSender.accept(otherShard, syncEvent);
      }
    }
  }

  private void markPendingSync(String targetShard) {
    ShardId shardId = new ShardId(targetShard);
    ShardNonces nonces = getShardNonce(shardId);
    shards = shards.set(shardId, nonces.markMissingSync());
  }

  private List<String> otherChains(String subChainId, FixedList<String> nextRouting) {
    List<String> ids = new ArrayList<>();
    if (subChainId != null) {
      ids.add(null);
    }
    for (String shard : nextRouting) {
      if (!Objects.equals(shard, subChainId)) {
        ids.add(shard);
      }
    }
    return ids;
  }

  private Map<Hash, byte[]> mapStateStorage(List<byte[]> stateStorage) {
    Map<Hash, byte[]> mappedStagedStorage = new HashMap<>();
    for (byte[] bytes : stateStorage) {
      mappedStagedStorage.put(Hash.create(stream -> stream.write(bytes)), bytes);
    }
    return mappedStagedStorage;
  }

  /**
   * Handle an incoming sync event.
   *
   * @param originShard the sender of the sync
   * @param sync the incoming sync
   */
  public void incomingSync(String originShard, SyncEvent sync) {

    MemoryStateStorage memoryStateStorage =
        new MemoryStateStorage(mapStateStorage(sync.getStateStorage()));
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true, true);
    Map<BlockchainAddress, StateSerializable> updatedContracts =
        updateContractsInIncomingSync(sync.getContractTransfers(), stateSerializer);
    Map<BlockchainAddress, StateSerializable> updatedAccounts =
        updateAccountsInIncomingSync(sync.getAccountTransfers(), stateSerializer);

    updateAccountPlugin(updatedContracts, updatedAccounts);

    ShardId shardId = new ShardId(originShard);
    ShardNonces nonces = getShardNonce(shardId);
    shards = shards.set(shardId, nonces.markSyncReceived());
  }

  @SuppressWarnings("unchecked")
  private Class<? extends StateSerializable> getAccountPluginAccountStateClass() {
    ChainPlugin<?, ?, ?> chainPlugin = plugins.get(ChainPluginType.ACCOUNT);
    if (chainPlugin == null) {
      return null;
    } else {
      return (Class<? extends StateSerializable>)
          chainPlugin.getPlugin().getLocalStateClassTypeParameters().get(1);
    }
  }

  @SuppressWarnings("unchecked")
  private Class<? extends StateSerializable> getAccountPluginContractStateClass() {
    ChainPlugin<?, ?, ?> chainPlugin = plugins.get(ChainPluginType.ACCOUNT);
    if (chainPlugin == null) {
      return null;
    } else {
      return (Class<? extends StateSerializable>)
          chainPlugin.getPlugin().getLocalStateClassTypeParameters().get(2);
    }
  }

  private Map<BlockchainAddress, StateSerializable> updateAccountsInIncomingSync(
      List<SyncEvent.AccountTransfer> accountTransfers, StateSerializer byteBackedSerializer) {
    Map<BlockchainAddress, StateSerializable> accountPluginAccountStates = new LinkedHashMap<>();

    Class<? extends StateSerializable> clazz = getAccountPluginAccountStateClass();
    StateSerializer localStorageSerializer = getContext().createStateSerializerFreshStorage();
    StorageTransferUtil storageTransfer =
        new StorageTransferUtil(byteBackedSerializer, localStorageSerializer);
    for (SyncEvent.AccountTransfer accountTransfer : accountTransfers) {
      AccountState accountState =
          storageTransfer.transferBetweenStorage(
              accountTransfer.accountStateHash(), AccountState.class);
      StateSerializable pluginState =
          storageTransfer.transferBetweenStorage(accountTransfer.pluginStateHash(), clazz);
      accounts = accounts.set(accountTransfer.address(), accountState);
      accountPluginAccountStates.put(accountTransfer.address(), pluginState);
    }
    return accountPluginAccountStates;
  }

  private Map<BlockchainAddress, StateSerializable> updateContractsInIncomingSync(
      List<SyncEvent.ContractTransfer> contractTransfers,
      StateSerializer memoryStateStorageSerializer) {
    Map<BlockchainAddress, StateSerializable> pluginStates = new LinkedHashMap<>();

    Class<? extends StateSerializable> clazz = getAccountPluginContractStateClass();
    StateSerializer localStorageSerializer = getContext().createStateSerializerFreshStorage();
    StorageTransferUtil storageTransfer =
        new StorageTransferUtil(memoryStateStorageSerializer, localStorageSerializer);
    for (SyncEvent.ContractTransfer contractTransfer : contractTransfers) {
      newContracts.add(contractTransfer.address());
      ContractState contractState =
          storageTransfer.transferBetweenStorage(
              contractTransfer.contractStateHash(), ContractState.class);
      CoreContractState core = contractState.getCore();
      storageTransfer.transferBetweenStorage(core.getBinderIdentifier(), LargeByteArray.class);
      storageTransfer.transferBetweenStorage(core.getAbiIdentifier(), LargeByteArray.class);
      storageTransfer.transferBetweenStorage(core.getContractIdentifier(), LargeByteArray.class);
      final StateSerializable pluginState =
          storageTransfer.transferBetweenStorage(contractTransfer.pluginStateHash(), clazz);
      contracts = contracts.set(contractTransfer.address(), contractState);
      CoreContractState.ContractSerialization<StateSerializable> serialization =
          getBlockchainContract(contractTransfer.address()).getContractSerialization();
      StateSerializable innerContractState =
          serialization.read(memoryStateStorageSerializer, contractState.getStateHash());

      BlockchainContract<StateSerializable, BinderEvent> blockchainContract =
          getBlockchainContract(contractTransfer.address());
      blockchainContract
          .getContractSerialization()
          .write(localStorageSerializer, innerContractState);

      contracts = contracts.set(contractTransfer.address(), contractState);
      pluginStates.put(contractTransfer.address(), pluginState);
    }
    return pluginStates;
  }

  private void removeFromAccountPlugin(
      List<BlockchainAddress> contracts, List<BlockchainAddress> accounts) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            for (BlockchainAddress address : contracts) {
              local = local.setContract(address, null);
            }
            for (BlockchainAddress address : accounts) {
              local = local.setAccount(address, null);
            }
            return local;
          }
        });
  }

  @SuppressWarnings("unchecked")
  private void updateAccountPlugin(
      Map<BlockchainAddress, StateSerializable> contracts,
      Map<BlockchainAddress, StateSerializable> accounts) {
    updateAccountPluginInternal(
        new AccountPluginUpdate() {
          @Override
          public <
                  G extends StateSerializable,
                  A extends StateSerializable,
                  C extends StateSerializable,
                  L extends StateSerializable>
              AccountPluginState<L, A, C> apply(
                  G globalState,
                  AccountPluginState<L, A, C> local,
                  BlockchainAccountPlugin<G, L, A, C> plugin) {
            for (BlockchainAddress address : contracts.keySet()) {
              local = local.setContract(address, (C) contracts.get(address));
            }
            for (BlockchainAddress address : accounts.keySet()) {
              local = local.setAccount(address, (A) accounts.get(address));
            }
            return local;
          }
        });
  }

  /**
   * Add an active shard.
   *
   * @param syncSender callback used to create sync events
   * @param currentShard the id of the current shard
   * @param shardToCreate id of the shard to create
   */
  public void addActiveShard(
      BiConsumer<String, SyncEvent> syncSender, String currentShard, String shardToCreate) {
    if (activeShards.contains(shardToCreate)) {
      throw new IllegalStateException("Unable to add existing shard " + shardToCreate);
    }

    FixedList<String> previousShards = this.activeShards;
    this.activeShards = previousShards.addElement(shardToCreate);

    routingChanged(syncSender, currentShard, previousShards, this.activeShards);
  }

  /**
   * Remove an active shard.
   *
   * @param syncSender callback used to create sync events
   * @param currentShard the id of the current shard
   * @param shardToRemove id of the shard to remove
   */
  public void removeActiveShard(
      BiConsumer<String, SyncEvent> syncSender, String currentShard, String shardToRemove) {
    FixedList<String> previousShards = this.activeShards;
    this.activeShards = previousShards.removeElement(shardToRemove);
    routingChanged(syncSender, currentShard, previousShards, this.activeShards);
  }

  /**
   * Get version of governance running.
   *
   * @return version of governance
   */
  public long getGovernanceVersion() {
    return governanceVersion;
  }

  /**
   * Upgrades the state of a system contract.
   *
   * @param contractAddress the address of the contract to be upgraded.
   * @param newCoreState the new core state.
   * @param rpc the invocation.
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void upgradeContract(
      BlockchainAddress contractAddress, CoreContractState newCoreState, byte[] rpc) {
    StateSerializable oldState = getContractState(contractAddress);
    StateAccessor stateAccessor = StateAccessor.create(oldState);
    ContractState oldContractState = getContract(contractAddress);
    ContractState newContractState = oldContractState.withCore(newCoreState);
    setContract(contractAddress, newContractState);
    SysBlockchainContract<StateSerializable> newContract =
        (SysBlockchainContract) getBlockchainContract(contractAddress);
    StateSerializable newState = newContract.upgrade(stateAccessor, rpc);
    setContractState(contractAddress, newState);
  }

  /**
   * Upgrades the state of a contract.
   *
   * @param contractAddress the address of the contract to be upgraded.
   * @param contractContext the contract context
   * @param executionContext the execution context
   * @param newCoreState the new core state
   * @param rpc the contract upgrade rpc
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public void upgradeContract(
      BlockchainAddress contractAddress,
      BlockchainContractContext contractContext,
      ExecutionContextTransaction executionContext,
      CoreContractState newCoreState,
      byte[] rpc) {
    StateSerializable oldState = getContractState(contractAddress);
    StateAccessor stateAccessor = StateAccessor.create(oldState);
    ContractState oldContractState = getContract(contractAddress);

    CoreContractState oldCoreState = oldContractState.getCore();
    ContractIdentifiers oldContractBytecodeIds = getContractIdentifiers(oldCoreState);
    ContractIdentifiers newContractBytecodeIds = getContractIdentifiers(newCoreState);
    BlockchainContract oldContract = getBlockchainContract(contractAddress);
    ContractUpgradePermit permit =
        oldContract.acquireUpgradePermit(
            contractContext,
            executionContext,
            oldState,
            oldContractBytecodeIds,
            newContractBytecodeIds,
            rpc);

    ContractState newContractState = oldContractState.withCore(newCoreState);
    setContract(contractAddress, newContractState);

    BlockchainContract newContract = getBlockchainContract(contractAddress);
    StateSerializable newState =
        newContract.upgradeState(contractContext, executionContext, stateAccessor, permit, rpc);
    setContractState(contractAddress, newState);
  }

  private ContractIdentifiers getContractIdentifiers(CoreContractState coreState) {
    Hash contractHash = coreState.getContractIdentifier();
    Hash binderHash = coreState.getBinderIdentifier();
    Hash abiHash = coreState.getAbiIdentifier();

    return new ContractIdentifiers(contractHash, binderHash, abiHash);
  }

  private interface AccountPluginUpdate {

    <
            G extends StateSerializable,
            A extends StateSerializable,
            C extends StateSerializable,
            L extends StateSerializable>
        AccountPluginState<L, A, C> apply(
            G globalState,
            AccountPluginState<L, A, C> local,
            BlockchainAccountPlugin<G, L, A, C> plugin);
  }

  private interface ConsensusPluginUpdate {

    <GlobalT extends StateSerializable, LocalT extends StateSerializable> LocalT apply(
        GlobalT globalState, LocalT localState, BlockchainConsensusPlugin<GlobalT, LocalT> plugin);
  }

  private static final class BlockProduction implements PluginContext {

    private final long blockProductionTime;

    private BlockProduction(long blockProductionTime) {
      this.blockProductionTime = blockProductionTime;
    }

    @Override
    public long blockProductionTime() {
      return blockProductionTime;
    }
  }
}
