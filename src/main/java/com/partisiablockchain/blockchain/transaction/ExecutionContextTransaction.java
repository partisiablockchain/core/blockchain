package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;

/** The context for executing a transaction. */
public final class ExecutionContextTransaction {

  private final String chainId;
  private final MutableChainState state;
  private final Hash originatingTransactionHash;
  private final Hash transactionHash;
  private final BlockchainAddress from;
  private final ExecutionEventManager manager;
  private final long blockTime;
  private final long blockProductionTime;
  private final FeeCollector feeCollector;
  private AvlTree<BlockchainAddress, Long> pendingServiceFees;
  private AvlTree<BlockchainAddress, Long> pendingInfrastructureFees;
  private FixedList<PendingByocFee> pendingByocFees;

  private ExecutionContextTransaction(
      String chainId,
      MutableChainState state,
      Hash transactionHash,
      Hash originatingTransactionHash,
      BlockchainAddress from,
      ExecutionEventManager manager,
      long blockTime,
      long blockProductionTime,
      FeeCollector feeCollector) {
    this.chainId = chainId;
    this.state = state;
    this.transactionHash = transactionHash;
    this.originatingTransactionHash = originatingTransactionHash;
    this.from = from;
    this.manager = manager;
    this.blockTime = blockTime;
    this.blockProductionTime = blockProductionTime;
    this.feeCollector = feeCollector;
    this.pendingServiceFees = AvlTree.create();
    this.pendingInfrastructureFees = AvlTree.create();
    this.pendingByocFees = FixedList.create();
  }

  /**
   * Creates a new ExecutionContextTransaction.
   *
   * @param context the original context
   * @param state the actual state
   * @param transactionHash the hash for current transaction
   * @param originatingTransaction the identifier for the originating transaction
   * @param from the sender
   * @param manager the event manager
   * @param feeCollector the fee collector
   * @return the created ExecutionContextTransaction
   */
  public static ExecutionContextTransaction create(
      ExecutionContext context,
      MutableChainState state,
      Hash transactionHash,
      Hash originatingTransaction,
      BlockchainAddress from,
      ExecutionEventManager manager,
      FeeCollector feeCollector) {
    return create(
        context.getChainId(),
        state,
        transactionHash,
        originatingTransaction,
        from,
        manager,
        context.getBlockTime(),
        context.getBlockProductionTime(),
        feeCollector);
  }

  static ExecutionContextTransaction create(
      String chainId,
      MutableChainState state,
      Hash transactionHash,
      Hash originatingTransaction,
      BlockchainAddress from,
      ExecutionEventManager manager,
      long blockTime,
      long blockProductionTime,
      FeeCollector collector) {
    return new ExecutionContextTransaction(
        chainId,
        state,
        transactionHash,
        originatingTransaction,
        from,
        manager,
        blockTime,
        blockProductionTime,
        collector);
  }

  /**
   * Get block time for the current block.
   *
   * @return block time for the current block
   */
  public long getBlockTime() {
    return blockTime;
  }

  /**
   * Get production time for the current block.
   *
   * @return production time for the current block
   */
  public long getBlockProductionTime() {
    return blockProductionTime;
  }

  /**
   * Get current mutable state.
   *
   * @return mutable chain state
   */
  public MutableChainState getState() {
    return state;
  }

  /**
   * Get hash of current transaction.
   *
   * @return hash of current transaction
   */
  public Hash getTransactionHash() {
    return transactionHash;
  }

  /**
   * Get sender of transaction.
   *
   * @return sender's address
   */
  public BlockchainAddress getFrom() {
    return from;
  }

  /**
   * Get execution event manager.
   *
   * @return execution event manager
   */
  public ExecutionEventManager getExecutionEventManager() {
    return manager;
  }

  /**
   * Get hash of original transaction.
   *
   * @return hash of original transaction
   */
  public Hash getOriginatingTransactionHash() {
    return originatingTransactionHash;
  }

  /**
   * Add pending service fee.
   *
   * @param node receiver of fee
   * @param fees amount to add
   */
  public void addPendingServiceFees(BlockchainAddress node, long fees) {
    long current = pendingServiceFees.containsKey(node) ? pendingServiceFees.getValue(node) : 0L;
    this.pendingServiceFees = pendingServiceFees.set(node, current + fees);
  }

  /**
   * Get a list of the pending service fees.
   *
   * @return a list of pending service fees, or an empty list if no pending fees exists
   */
  public List<PendingFee> getPendingServiceFees() {
    List<PendingFee> result = new ArrayList<>();
    for (BlockchainAddress node : pendingServiceFees.keySet()) {
      result.add(PendingFee.create(node, pendingServiceFees.getValue(node)));
    }
    return result;
  }

  /**
   * Add pending infrastructure fee.
   *
   * @param node receiver of fee
   * @param fees amount to add
   */
  public void addPendingInfrastructureFees(BlockchainAddress node, long fees) {
    long current =
        pendingInfrastructureFees.containsKey(node) ? pendingInfrastructureFees.getValue(node) : 0L;
    this.pendingInfrastructureFees = pendingInfrastructureFees.set(node, current + fees);
  }

  /**
   * Get a list of the pending infrastructure fees.
   *
   * @return a list of pending infrastructure fees, or an empty list if no pending fees exists
   */
  public List<PendingFee> getPendingInfrastructureFees() {
    List<PendingFee> result = new ArrayList<>();
    for (BlockchainAddress node : pendingInfrastructureFees.keySet()) {
      result.add(PendingFee.create(node, pendingInfrastructureFees.getValue(node)));
    }
    return result;
  }

  /**
   * Register fee for cpu usage.
   *
   * @param cpuFee gas fee for usage
   */
  public void registerCpuFee(long cpuFee) {
    feeCollector.registerCpuFee(cpuFee);
  }

  /**
   * Get id of the chain.
   *
   * @return the chain id
   */
  public String getChainId() {
    return chainId;
  }

  /**
   * Gets the amount of gas available for this transaction.
   *
   * @return the available cost of the CPU usage
   */
  public long availableGas() {
    return feeCollector.getGasPaidByUser();
  }

  /**
   * Add a pending BYOC fee related to a BYOC transaction.
   *
   * @param amount the fee amount
   * @param symbol the BYOC symbol
   * @param nodes the nodes that created the transaction
   */
  public void addPendingByocFee(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
    this.pendingByocFees = pendingByocFees.addElement(PendingByocFee.create(nodes, amount, symbol));
  }

  /**
   * Gets the list of pending BYOC fees.
   *
   * @return the pending BYOC fees.
   */
  public List<PendingByocFee> getPendingByocFees() {
    return new ArrayList<>(pendingByocFees);
  }
}
