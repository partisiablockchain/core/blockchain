package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import java.util.List;

/**
 * Data store plugin that holds binary data that should be synchronized and available on all shards.
 */
@Immutable
public abstract class BlockchainSharedObjectStorePlugin<GlobalT extends StateSerializable>
    implements BlockchainPlugin<GlobalT, StateVoid> {

  /**
   * Check if there exists a binary object with the identifier that is available on all shards.
   *
   * @param globalState the global state of the shared object store plugin
   * @param hash identifier of the binary object
   * @return true if the binary object is available, false otherwise
   */
  public abstract boolean exists(GlobalT globalState, Hash hash);

  @Override
  public final Class<StateVoid> getLocalStateClass() {
    return StateVoid.class;
  }

  @Override
  public final List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of();
  }

  @Override
  public final InvokeResult<StateVoid> invokeLocal(
      PluginContext pluginContext,
      GlobalT globalState,
      StateVoid state,
      BlockchainAddress invocationContext,
      byte[] rpc) {
    throw new UnsupportedOperationException();
  }

  @Override
  public final StateVoid migrateLocal(StateAccessor currentLocal) {
    return null;
  }
}
