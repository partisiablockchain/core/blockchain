package com.partisiablockchain.blockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;

/** Test. */
@Immutable
public record GasAndCoinFeePluginGlobal(
    BlockchainAddress feeAccount,
    BlockchainAddress zkFeeAccount,
    boolean markContractIced,
    boolean removeContract)
    implements StateSerializable {

  /**
   * Set new fee account.
   *
   * @param feeAccount new fee account
   * @return updated global state
   */
  public GasAndCoinFeePluginGlobal setFeeAccount(BlockchainAddress feeAccount) {
    return new GasAndCoinFeePluginGlobal(
        feeAccount, zkFeeAccount, markContractIced, removeContract);
  }

  /**
   * Set new fee account.
   *
   * @param zkFeeAccount new zk fee account
   * @return updated global state
   */
  public GasAndCoinFeePluginGlobal setZkFeeAccount(BlockchainAddress zkFeeAccount) {
    return new GasAndCoinFeePluginGlobal(
        feeAccount, zkFeeAccount, markContractIced, removeContract);
  }
}
