package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.DummyExecutableEventFinder;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.TestContractByInvocation;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.InnerSystemEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.thread.ExecutionFactoryThreaded;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Test. */
public final class EventTransactionOnBlockchainTest extends CloseableTest {

  private static final Logger logger =
      LoggerFactory.getLogger(EventTransactionOnBlockchainTest.class);
  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final BlockchainAddress producerAccount = producerKey.getPublic().createAddress();
  private final KeyPair secondKey = new KeyPair(BigInteger.TEN);
  private final BlockchainAddress secondAccount = secondKey.getPublic().createAddress();

  private final BlockChainTestNetwork network = new BlockChainTestNetwork();
  private RootDirectory rootDirectory;
  private BlockchainLedger blockchainLedger;
  private String chainId;
  private ShardRoute shardRoute = new ShardRoute("dest", 1L);

  private DummyExecutableEventFinder finder;

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    finder = new DummyExecutableEventFinder();
    rootDirectory = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                this::bootstrapDeployment,
                network,
                finder,
                ExecutionFactoryThreaded.create()));
    chainId = blockchainLedger.getChainId();
  }

  private void bootstrapDeployment(MutableChainState chainstate) {
    StateHelper.initial(chainstate);
    byte[] jar = JarBuilder.buildJar(BlockchainLedgerTest.DeployContractInTest.class);

    BlockchainAddress deployPublicContract = TestObjects.CONTRACT_BOOTSTRAP;
    CoreContractStateTest.createContract(
        chainstate, jar, deployPublicContract, TestContracts.CONTRACT_BINDER_SYSTEM);
    FeePluginHelper.mintGas(chainstate, deployPublicContract, 1_000_000);
  }

  @Test
  public void createEvents() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                chainstate -> {
                  bootstrapDeployment(chainstate);

                  CoreContractStateTest.createContract(
                      chainstate,
                      JarBuilder.buildJar(TestContractByInvocation.class),
                      TestObjects.CONTRACT_PUB1,
                      TestContracts.CONTRACT_BINDER_PUBLIC);
                  byte[] rpc = new byte[0];
                  chainstate.setPlugin(
                      FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    SignedTransaction deployEvent =
        createDeployPub(
            EventTestContract.class,
            SafeDataOutputStream.serialize(
                safeDataOutputStream -> {
                  TestObjects.CONTRACT_PUB1.write(safeDataOutputStream);
                  safeDataOutputStream.writeByte(7);
                  safeDataOutputStream.writeByte(3);
                }));
    blockchainLedger.addPendingTransaction(deployEvent);

    BlockchainLedgerTest.produce(blockchainLedger, deployEvent);
    final BlockchainAddress contract = TestObjects.CONTRACT_PUB2;

    BlockAndState latest = blockchainLedger.latest();

    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    printEventTree(deployEvent.identifier(), blockchainLedger, "");
    assertThat(executionResults.size()).isEqualTo(1 + 2 + 7 * 2 + 3 * 2 + 1);

    AvlTree<Hash, Hash> events = latest.getState().getExecutedState().getSpawnedEvents();
    assertThat(events.size()).isEqualTo(2 + (7 + 3) * 2 + 1);

    ExecutableEvent event = nextEvent(deployEvent.identifier(), events);
    assertThat(((InnerTransaction) event.getEvent().getInner()).getSender())
        .isEqualTo(producerAccount);
    event = nextEvent(event.identifier(), events);
    assertThat(((InnerTransaction) event.getEvent().getInner()).getSender())
        .isEqualTo(producerAccount);
    List<ExecutableEvent> eventTransactions =
        findSpawnedEvents(events, event.identifier()).stream()
            .map(blockchainLedger::getEventTransaction)
            .sorted(ExecutedTransactionEvents.getComparator())
            .toList();
    assertThat(((InnerTransaction) eventTransactions.get(0).getEvent().getInner()).getSender())
        .isEqualTo(contract);
    assertThat(((InnerTransaction) eventTransactions.get(6).getEvent().getInner()).getSender())
        .isEqualTo(contract);
    BlockchainAddress sender = producerAccount;
    assertThat(((InnerTransaction) eventTransactions.get(7).getEvent().getInner()).getSender())
        .isEqualTo(sender);
    assertThat(((InnerTransaction) eventTransactions.get(9).getEvent().getInner()).getSender())
        .isEqualTo(sender);
    assertThat(blockchainLedger.getTransactionsForBlock(latest.getBlock())).hasSize(1);

    BlockchainLedgerTest.produce(blockchainLedger);

    latest = blockchainLedger.latest();
    assertThat(latest.getBlock().getEventTransactions()).hasSize(0);
    assertThat(latest.getBlock().getTransactions()).hasSize(0);

    StateSerializable contractState = latest.getState().getContractState(TestObjects.CONTRACT_PUB1);
    assertThat(contractState).isNotNull();
    assertThat(contractState).hasFieldOrPropertyWithValue("state", 10);

    assertThat(blockchainLedger.getTransactionsForBlock(latest.getBlock())).hasSize(0);
    BlockchainLedgerTest.produce(blockchainLedger);

    long blockTime = latest.getBlock().getBlockTime();
    ImmutableChainState loadedStateDeploy = blockchainLedger.getState(blockTime - 1);
    assertThat(loadedStateDeploy).isNotNull();
    assertThat(loadedStateDeploy.getExecutedState().getEventTransactions())
        .hasSize(2 + (7 + 3) * 2 + 1);
    assertThat(loadedStateDeploy.getExecutedState().getExecutionStatus().size())
        .isEqualTo(2 + 1 + (7 + 3) * 2 + 1);
    ImmutableChainState loadedStateEvent = blockchainLedger.getState(blockTime);
    assertThat(loadedStateEvent).isNotNull();
    assertThat(loadedStateEvent.getExecutedState().getEventTransactions()).hasSize(0);
    assertThat(loadedStateEvent.getExecutedState().getExecutionStatus().size()).isEqualTo(0);
  }

  static void printEventTree(Hash event, BlockchainLedger ledger, String path) {
    logger.info(
        path
            + ledger.latest().getState().getExecutedState().getExecutionStatus().getValue(event)
            + ": "
            + ledger.getEventTransaction(event));
    Stream<Hash> children = findChildren(event, ledger);
    children.forEach(spawnedEvent -> printEventTree(spawnedEvent, ledger, path + " "));
  }

  private static Stream<Hash> findChildren(Hash event, BlockchainLedger ledger) {
    AvlTree<Hash, Hash> spawnedEvents =
        ledger.latest().getState().getExecutedState().getSpawnedEvents();
    return spawnedEvents.keySet().stream()
        .filter(hash -> spawnedEvents.getValue(hash).equals(event));
  }

  private ExecutableEvent nextEvent(Hash deployEvent, AvlTree<Hash, Hash> events) {
    List<Hash> spawned = findSpawnedEvents(events, deployEvent);
    assertThat(spawned).hasSize(1);
    return blockchainLedger.getEventTransaction(spawned.get(0));
  }

  @Test
  public void createRecursiveEvents() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                chainstate -> {
                  bootstrapDeployment(chainstate);
                  byte[] rpc = new byte[0];
                  chainstate.setPlugin(
                      FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    int signedTransactionCount = 1;
    int eventTransactionsCount = 2;
    int levelsToTest = 129 - signedTransactionCount - eventTransactionsCount;
    SignedTransaction deployEvent =
        createDeployPub(
            EventRecursiveTestContract.class,
            SafeDataOutputStream.serialize(stream -> stream.writeInt(levelsToTest)));
    blockchainLedger.addPendingTransaction(deployEvent);

    BlockchainLedgerTest.produce(blockchainLedger, deployEvent);
    BlockAndState latest = blockchainLedger.latest();

    final ExecutedState executedState = latest.getState().getExecutedState();
    List<ExecutableEvent> eventTransactions =
        executedState.getEventTransactions().stream()
            .map(blockchainLedger::getEventTransaction)
            .collect(Collectors.toList());
    assertThat(eventTransactions).hasSize(levelsToTest * 3 + eventTransactionsCount);

    AvlTree<Hash, Boolean> results = executedState.getExecutionStatus();
    AvlTree<Hash, Hash> events = executedState.getSpawnedEvents();
    ArrayDeque<Hash> pending =
        new ArrayDeque<>(findSpawnedEvents(events, deployEvent.identifier()));
    int i = 0;
    while (!pending.isEmpty()) {
      Hash next = pending.removeFirst();
      // We expect to fail just after the 128'th event
      boolean expected = i != 127;
      assertThat(results.getValue(next)).isEqualTo(expected);
      if (expected) {
        assertThat(blockchainLedger.getTransactionFailure(next)).isEqualTo(null);
      } else {
        assertThat(blockchainLedger.getTransactionFailure(next).getErrorMessage())
            .startsWith("Cannot add another level");
        assertThat(blockchainLedger.getTransactionFailure(next).getStackTrace())
            .startsWith("java.lang.IllegalStateException: Cannot add another level");
      }
      pending.addAll(findSpawnedEvents(events, next));
      i++;
    }
  }

  @Test
  public void systemEventsWithCallbacks() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                chainstate -> {
                  chainstate.createAccount(producerAccount);
                  chainstate.createAccount(secondAccount);
                  BlockchainAddress deployPublicContract = TestObjects.CONTRACT_SYS;
                  CoreContractStateTest.createContract(
                      chainstate,
                      JarBuilder.buildJar(SystemEventsInTest.class),
                      deployPublicContract,
                      TestContracts.CONTRACT_BINDER_SYSTEM);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    SignedTransaction failInCallback =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    getNonce(producerAccount), System.currentTimeMillis() + 100_000, 100_000),
                InteractWithContractTransaction.create(
                    TestObjects.CONTRACT_SYS,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeBoolean(false);
                          s.writeBoolean(true);
                        })))
            .sign(producerKey, chainId);
    SignedTransaction failInEvent =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    getNonce(secondAccount), System.currentTimeMillis() + 100_000, 100_000),
                InteractWithContractTransaction.create(
                    TestObjects.CONTRACT_SYS,
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeBoolean(true);
                          s.writeBoolean(false);
                        })))
            .sign(secondKey, chainId);
    blockchainLedger.addPendingTransaction(failInCallback);
    blockchainLedger.addPendingTransaction(failInEvent);
    BlockchainLedgerTest.produce(blockchainLedger, failInCallback, failInEvent);

    AvlTree<Hash, Hash> spawnedEvents =
        blockchainLedger.latest().getState().getExecutedState().getSpawnedEvents();
    assertThat(spawnedEvents.size()).isEqualTo(8);
    verifyEventStatus(spawnedEvents, List.of(true, true, true, false), failInCallback.identifier());
    verifyEventStatus(spawnedEvents, List.of(true, false, true, true), failInEvent.identifier());
  }

  private void verifyEventStatus(
      AvlTree<Hash, Hash> spawnedEvents, List<Boolean> status, Hash identifier) {
    List<Boolean> actual = new ArrayList<>();
    for (int i = 0; i < status.size(); i++) {
      List<Hash> foundEvents = findSpawnedEvents(spawnedEvents, identifier);
      assertThat(foundEvents).hasSize(1);
      identifier = foundEvents.get(0);
      ExecutedTransaction transaction = blockchainLedger.getTransaction(identifier);
      assertThat(transaction).isNotNull();
      actual.add(transaction.didExecutionSucceed());
    }
    assertThat(actual).isEqualTo(status);
  }

  private List<Hash> findSpawnedEvents(AvlTree<Hash, Hash> events, Hash identifier) {
    List<Hash> spawnedEvents = new ArrayList<>();
    for (Hash hash : events.keySet()) {
      if (events.getValue(hash).equals(identifier)) {
        spawnedEvents.add(hash);
      }
    }
    return spawnedEvents;
  }

  @Test
  public void createSpecifiedEvents() {
    BlockchainAddress contractAddress = prepareContract();

    test(contractAddress, List.of(Boolean.FALSE, Boolean.TRUE));
    test(contractAddress, List.of(Boolean.TRUE, Boolean.FALSE));
    test(contractAddress, List.of(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));

    test(
        contractAddress,
        List.of(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE));
    test(
        contractAddress,
        List.of(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE));
    test(
        contractAddress,
        List.of(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE));
    test(
        contractAddress,
        List.of(Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE));
  }

  @Test
  public void createSmallCallStack() {
    BlockchainAddress contractAddress = prepareContract();

    List<Boolean> success = List.of(Boolean.TRUE, Boolean.FALSE);
    InteractWithContractTransaction transaction =
        SendingEventsToSubsetTestContract.create(contractAddress, success, 5, false);
    SignedTransaction testEvents = sign(transaction);
    blockchainLedger.addPendingTransaction(testEvents);
    BlockchainLedgerTest.produce(blockchainLedger, testEvents);

    BlockAndState latest = blockchainLedger.latest();
    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    printEventTree(testEvents.identifier(), blockchainLedger, "");
    assertThat(executionResults.size()).isEqualTo(27);
    for (Hash hash : executionResults.keySet()) {
      assertThat(executionResults.getValue(hash)).isTrue();
    }
  }

  private BlockchainAddress prepareContract() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                chainstate -> {
                  bootstrapDeployment(chainstate);
                  byte[] rpc = new byte[0];
                  chainstate.setPlugin(
                      FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    SignedTransaction deployEvent =
        createDeployPub(
            SendingEventsToSubsetTestContract.class,
            SafeDataOutputStream.serialize(EventCreator.EMPTY_RPC));
    blockchainLedger.addPendingTransaction(deployEvent);
    BlockchainLedgerTest.produce(blockchainLedger, deployEvent);
    return TestObjects.CONTRACT_PUB2;
  }

  @Test
  public void createFailingCallback() {
    BlockchainAddress contractAddress = prepareContract();

    List<Boolean> success = List.of(Boolean.TRUE, Boolean.FALSE);
    InteractWithContractTransaction transaction =
        SendingEventsToSubsetTestContract.create(contractAddress, success, 3, true);
    SignedTransaction testEvents = sign(transaction);
    blockchainLedger.addPendingTransaction(testEvents);
    BlockchainLedgerTest.produce(blockchainLedger, testEvents);

    BlockAndState latest = blockchainLedger.latest();
    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    printEventTree(testEvents.identifier(), blockchainLedger, "");
    assertThat(executionResults.size()).isEqualTo(14);

    List<Hash> children = list(findChildren(testEvents.identifier(), blockchainLedger));
    assertThat(children.size()).isEqualTo(1);
    Hash firstContractInteract = children.get(0);
    assertThat(executionResults.getValue(firstContractInteract)).isTrue();

    children = list(findChildren(firstContractInteract, blockchainLedger));
    assertThat(children.size()).isEqualTo(2);
    ExecutableEvent withCallback = getOnNonce(children, true);
    assertThat(executionResults.getValue(withCallback.identifier())).isTrue();
    ExecutableEvent withoutCallback = getOnNonce(children, false);
    assertThat(executionResults.getValue(withoutCallback.identifier())).isTrue();

    children = list(findChildren(withCallback.identifier(), blockchainLedger));
    assertThat(children.size())
        .withFailMessage("Cannot find children for %s", withCallback.identifier().toString())
        .isEqualTo(2);
    withCallback = getOnNonce(children, true);
    assertThat(executionResults.getValue(withCallback.identifier())).isTrue();
    withoutCallback = getOnNonce(children, false);
    assertThat(executionResults.getValue(withoutCallback.identifier())).isTrue();

    children = list(findChildren(withCallback.identifier(), blockchainLedger));
    assertThat(children.size()).isEqualTo(2);
    withCallback = getOnNonce(children, true);
    assertThat(executionResults.getValue(withCallback.identifier())).isTrue();
    withoutCallback = getOnNonce(children, false);
    assertThat(executionResults.getValue(withoutCallback.identifier())).isTrue();

    children = list(findChildren(withCallback.identifier(), blockchainLedger));
    assertThat(children.size()).isEqualTo(1);
    Hash callbackEvent = children.get(0);
    ExecutableEvent eventTransaction = blockchainLedger.getEventTransaction(callbackEvent);
    assertThat(eventTransaction.getEvent().getInner())
        .isInstanceOf(InnerSystemEvent.CallbackEvent.class);
    assertThat(executionResults.getValue(callbackEvent)).isTrue();

    children = list(findChildren(callbackEvent, blockchainLedger));
    assertThat(children.size()).isEqualTo(1);
    Hash callbackToContract = children.get(0);
    eventTransaction = blockchainLedger.getEventTransaction(callbackToContract);
    assertThat(eventTransaction.getEvent().getInner()).isInstanceOf(CallbackToContract.class);
    assertThat(executionResults.getValue(callbackToContract)).isFalse();
  }

  private ExecutableEvent getOnNonce(List<Hash> children, boolean lowNonce) {
    List<ExecutableEvent> collect =
        children.stream()
            .map(h -> blockchainLedger.getEventTransaction(h))
            .sorted(ExecutedTransactionEvents.getComparator())
            .collect(Collectors.toList());
    assertThat(collect).hasSize(2);
    if (!lowNonce) {
      return collect.get(0);
    } else {
      return collect.get(1);
    }
  }

  private List<Hash> list(Stream<Hash> children) {
    return children.collect(Collectors.toList());
  }

  private void test(BlockchainAddress contractAddress, List<Boolean> success) {
    InteractWithContractTransaction transaction =
        SendingEventsToSubsetTestContract.create(contractAddress, success, 1, false);
    SignedTransaction testEvents1 = sign(transaction);
    blockchainLedger.addPendingTransaction(testEvents1);
    BlockchainLedgerTest.produce(blockchainLedger, testEvents1);

    BlockAndState latest = blockchainLedger.latest();
    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    printEventTree(testEvents1.identifier(), blockchainLedger, "");
    long eventSpawners = success.stream().filter(Boolean::booleanValue).count();
    int countFromCallbacks = eventSpawners > 0 ? 2 : 0;
    long expected = 2 + success.size() + eventSpawners + countFromCallbacks;
    assertThat(executionResults.size()).isEqualTo((int) expected);
    for (Hash hash : executionResults.keySet()) {
      assertThat(executionResults.getValue(hash)).isTrue();
    }
  }

  @Test
  public void createWealthOfEvents() {
    this.blockchainLedger =
        register(
            BlockchainLedger.createForTest(
                rootDirectory,
                chainstate -> {
                  bootstrapDeployment(chainstate);
                  byte[] rpc = new byte[0];
                  chainstate.setPlugin(
                      FunctionUtility.noOpBiConsumer(), null, ChainPluginType.ACCOUNT, null, rpc);
                },
                network,
                finder,
                ExecutionFactoryThreaded.create()));

    SignedTransaction deployEvent =
        createDeployPub(
            SendingEventsToSubsetTestContract.class,
            SafeDataOutputStream.serialize(safeDataOutputStream -> {}));
    blockchainLedger.addPendingTransaction(deployEvent);
    BlockchainLedgerTest.produce(blockchainLedger, deployEvent);
    BlockchainAddress contractAddress = TestObjects.CONTRACT_PUB2;

    List<Boolean> success =
        List.of(Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
    InteractWithContractTransaction transaction =
        SendingEventsToSubsetTestContract.create(contractAddress, success, 3, false);
    SignedTransaction testEvents1 = sign(transaction);
    blockchainLedger.addPendingTransaction(testEvents1);
    BlockchainLedgerTest.produce(blockchainLedger, testEvents1);

    BlockAndState latest = blockchainLedger.latest();
    AvlTree<Hash, Boolean> executionResults =
        latest.getState().getExecutedState().getExecutionStatus();
    printEventTree(testEvents1.identifier(), blockchainLedger, "");

    assertThat(executionResults.size()).isEqualTo(132);
    for (Hash hash : executionResults.keySet()) {
      assertThat(executionResults.getValue(hash)).isTrue();
    }
  }

  private SignedTransaction sign(InteractWithContractTransaction deployTransaction) {
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 100_000),
            deployTransaction)
        .sign(producerKey, chainId);
  }

  private long getNonce(BlockchainAddress account) {
    return blockchainLedger.latest().getState().getAccount(account).getNonce();
  }

  private SignedTransaction createDeployPub(
      Class<? extends PubContract<? extends StateSerializable>> contractMainClass, byte[] rpc) {
    byte[] publicBinderJar = TestContracts.CONTRACT_BINDER_PUBLIC;
    return createDeploy(publicBinderJar, contractMainClass, rpc);
  }

  private SignedTransaction createDeploy(byte[] binderJar, Class<?> contract, byte[] rpc) {
    byte[] jar = JarBuilder.buildJar(contract);
    InteractWithContractTransaction deployTransaction =
        BlockchainLedgerTest.DeployContractInTest.create(
            TestObjects.CONTRACT_PUB2, binderJar, jar, rpc, 0);
    return SignedTransaction.create(
            CoreTransactionPart.create(
                getNonce(producerAccount), System.currentTimeMillis() + 100_000, 0),
            deployTransaction)
        .sign(producerKey, chainId);
  }

  @Test
  public void areEventsValidInvalidNonce() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 1L));
    ExecutableEvent second =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 1L));
    assertThat(
            BlockchainLedger.areAllEventsValid(
                false, AvlTree.create(), List.of(first, second), 100))
        .isFalse();
  }

  @Test
  public void areEventsValidDecreasingFinalMaster() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 1L));
    ExecutableEvent second =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 9L, 1L));
    assertThat(
            BlockchainLedger.areAllEventsValid(
                false, AvlTree.create(), List.of(first, second), 100))
        .isFalse();
  }

  @Test
  public void areEventsValidTooLargeGovernanceVersion() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 10L));

    assertThat(BlockchainLedger.areAllEventsValid(false, AvlTree.create(), List.of(first), 10L))
        .isTrue();
    assertThat(BlockchainLedger.areAllEventsValid(false, AvlTree.create(), List.of(first), 9L))
        .isFalse();
  }

  @Test
  public void areEventsValidInitialMaster() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
    shardRoute = new ShardRoute("dest", 2L);

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 7L, 1L));
    AvlTree<ShardId, ShardNonces> shardNonces =
        AvlTree.<ShardId, ShardNonces>create()
            .set(new ShardId("Origin"), ShardNonces.EMPTY.bumpInbound(8));
    assertThat(BlockchainLedger.areAllEventsValid(false, shardNonces, List.of(first), 100))
        .isFalse();
    ExecutableEvent second =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 8L, 1L));
    assertThat(BlockchainLedger.areAllEventsValid(false, shardNonces, List.of(second), 100))
        .isTrue();
  }

  @Test
  public void areEventsValidSync() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);

    ExecutableEvent syncEvent =
        new ExecutableEvent(
            "Origin",
            new EventTransaction(
                TestObjects.EMPTY_HASH,
                new ShardRoute("dest", 1L),
                2,
                1L,
                0,
                null,
                new SyncEvent(List.of(), List.of(), List.of())));

    assertThat(BlockchainLedger.areAllEventsValid(true, AvlTree.create(), List.of(syncEvent), 100))
        .describedAs("Sync not allowed when not waiting for sync")
        .isFalse();

    assertThat(BlockchainLedger.areAllEventsValid(false, AvlTree.create(), List.of(syncEvent), 100))
        .describedAs("Sync not allowed when not sync")
        .isFalse();

    AvlTree<ShardId, ShardNonces> pendingSync =
        AvlTree.<ShardId, ShardNonces>create()
            .set(new ShardId("Origin"), ShardNonces.EMPTY.markMissingSync());
    assertThat(BlockchainLedger.areAllEventsValid(true, pendingSync, List.of(syncEvent), 100))
        .describedAs("Sync allowed when waiting for sync")
        .isTrue();

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                secondAccount,
                0L,
                transaction,
                new ShardRoute("dest", 2L),
                7L,
                1L));
    assertThat(
            BlockchainLedger.areAllEventsValid(true, pendingSync, List.of(syncEvent, first), 100))
        .describedAs("Event not allowed after sync")
        .isFalse();
  }

  @Test
  public void areEventsValid() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);

    ExecutableEvent first =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 1L));
    ExecutableEvent second =
        new ExecutableEvent(
            "Origin2",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 9L, 1L));
    shardRoute = new ShardRoute("dest", 2L);

    ExecutableEvent third =
        new ExecutableEvent(
            "Origin",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH, secondAccount, 0L, transaction, shardRoute, 10L, 1L));

    assertThat(
            BlockchainLedger.areAllEventsValid(
                false, AvlTree.create(), List.of(first, second, third), 100))
        .isTrue();
  }

  /** Test. */
  public static final class EventTestContract
      extends PubContract<EventTestContract.EventTestState> {

    @Override
    public EventTestState onCreate(PubContractContext context, SafeDataInputStream rpc) {
      EventManager callCreator = context.getRemoteCallsCreator();
      BlockchainAddress contract = BlockchainAddress.read(rpc);
      int contractEvents = rpc.readUnsignedByte();
      for (int i = 0; i < contractEvents; i++) {
        final int index = i + 1;
        callCreator
            .invoke(contract)
            .withPayload(
                stream -> {
                  stream.writeInt(index);
                  stream.writeLong(context.getBlockProductionTime());
                })
            .sendFromContract();
      }
      int senderEvents = rpc.readUnsignedByte();
      for (int i = 0; i < senderEvents; i++) {
        final int index = i + contractEvents + 1;
        callCreator
            .invoke(contract)
            .withPayload(
                stream -> {
                  stream.writeInt(index);
                  stream.writeLong(context.getBlockProductionTime());
                })
            .send();
      }
      callCreator.registerCallbackWithCostFromRemaining(EventCreator.EMPTY_RPC);
      return new EventTestState(contractEvents + senderEvents, FixedList.create());
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public EventTestState onCallback(
        PubContractContext context,
        EventTestState state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      if (callbackContext.results().size() != state.numberOfEvents) {
        throw new IllegalStateException("Should not happen");
      }
      return new EventTestState(
          state.numberOfEvents - 1,
          state
              .hashes
              .addElement(callbackContext.results().get(0).eventTransaction())
              .addElement(context.getCurrentTransactionHash()));
    }

    /** Test. */
    @Immutable
    public static final class EventTestState implements StateSerializable {

      public final int numberOfEvents;
      public final FixedList<Hash> hashes;

      @SuppressWarnings("unused")
      EventTestState() {
        numberOfEvents = 0;
        hashes = null;
      }

      EventTestState(int numberOfEvents, FixedList<Hash> hashes) {
        this.numberOfEvents = numberOfEvents;
        this.hashes = hashes;
      }
    }
  }

  /** Test. */
  public static final class EventRecursiveTestContract extends PubContract<StateLong> {

    @Override
    public StateLong onCreate(PubContractContext context, SafeDataInputStream rpc) {
      EventManager eventManager = context.getRemoteCallsCreator();
      eventManager
          .invoke(context.getContractAddress())
          .withPayload(rpcForInteract(rpc.readInt()))
          .sendFromContract();
      eventManager.registerCallbackWithCostFromRemaining(rpcForInteract(0));
      return new StateLong(1);
    }

    @Override
    public StateLong onInvoke(
        PubContractContext context, StateLong currentIndex, SafeDataInputStream rpc) {
      int numberOfEventsToSend = rpc.readInt();
      if (numberOfEventsToSend > 0) {
        EventManager manager = context.getRemoteCallsCreator();
        manager
            .invoke(context.getContractAddress())
            .withPayload(rpcForInteract(numberOfEventsToSend - 1))
            .sendFromContract();
        manager.registerCallbackWithCostFromRemaining(rpcForInteract((int) currentIndex.value()));
        int counter = (int) currentIndex.value() + 1;
        return new StateLong(counter);
      } else {
        return currentIndex;
      }
    }

    @Override
    public StateLong onCallback(
        PubContractContext context,
        StateLong currentIndex,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      int indexAtCallTime = rpc.readInt();
      long expectedValue = currentIndex.value() - 1;
      if (expectedValue != indexAtCallTime) {
        throw new RuntimeException(expectedValue + "<>" + indexAtCallTime);
      } else {
        return new StateLong(expectedValue);
      }
    }

    private DataStreamSerializable rpcForInteract(int counter) {
      return stream -> stream.writeInt(counter);
    }
  }

  /** Test. */
  public static final class SendingEventsToSubsetTestContract extends PubContract<StateLong> {

    static InteractWithContractTransaction create(
        BlockchainAddress contractAddress,
        List<Boolean> success,
        int count,
        boolean failInCallback) {
      return InteractWithContractTransaction.create(
          contractAddress,
          SafeDataOutputStream.serialize(
              stream -> {
                stream.writeInt(count);
                stream.writeBoolean(failInCallback);
                stream.writeInt(success.size());
                success.forEach(stream::writeBoolean);
              }));
    }

    @Override
    public StateLong onInvoke(
        PubContractContext context, StateLong state, SafeDataInputStream rpc) {
      context.getInvocationCreator(); // EventManager to be filtered away
      EventCreator manager = context.getInvocationCreator();
      BlockchainAddress contract = context.getContractAddress();
      int spawnCounter = rpc.readInt();
      boolean failInCallback = rpc.readBoolean();

      if (spawnCounter > 0) {
        int count = rpc.readInt();
        List<Boolean> spawn = IntStream.range(0, count).mapToObj(i -> rpc.readBoolean()).toList();
        final int newCounter = spawnCounter - 1;

        EventCreator creator;
        if (spawn.contains(Boolean.TRUE)) {
          EventManager callback = context.getRemoteCallsCreator();
          callback.registerCallbackWithCostFromRemaining(
              stream -> stream.writeBoolean(failInCallback));
          creator = callback;
        } else {
          creator = context.getInvocationCreator();
        }
        for (Boolean triggerCallback : spawn) {
          if (triggerCallback) {
            creator
                .invoke(contract)
                .withPayload(
                    stream -> {
                      stream.writeInt(newCounter);
                      stream.writeBoolean(failInCallback);
                      if (newCounter > 0) {
                        stream.writeInt(spawn.size());
                        spawn.forEach(stream::writeBoolean);
                      }
                    })
                .sendFromContract();
          } else {
            context.getInvocationCreator(); // EventManager to be filtered away
            context.getInvocationCreator(); // EventManager to be filtered away
            manager
                .invoke(contract)
                .withPayload(
                    stream -> {
                      stream.writeInt(0);
                      stream.writeBoolean(failInCallback);
                    })
                .sendFromContract();
          }
        }
      }
      context.getInvocationCreator(); // EventManager to be filtered away

      long value = 0;
      if (state != null) {
        value = state.value();
      }
      return new StateLong(value + 1);
    }

    @Override
    public StateLong onCallback(
        PubContractContext context,
        StateLong state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      if (rpc.readBoolean()) {
        throw new RuntimeException("Intended failure");
      }
      callbackContext.setSuccess(true);
      EventCreator manager = context.getInvocationCreator();
      BlockchainAddress contract = context.getContractAddress();
      manager
          .invoke(contract)
          .withPayload(
              stream -> {
                stream.writeInt(0);
                stream.writeBoolean(false);
              })
          .sendFromContract();
      return new StateLong(state.value() + 1);
    }
  }

  /** Test. */
  public static final class SystemEventsInTest extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      boolean failInEvent = rpc.readBoolean();
      boolean failInCallback = rpc.readBoolean();
      SystemEventManager eventManager = context.getRemoteCallsCreator();
      if (failInEvent) {
        eventManager.updateLocalAccountPluginState(
            LocalPluginStateUpdate.create(context.getContractAddress(), new byte[0]));
      } else {
        eventManager.setFeature("Feature", null);
      }
      eventManager.registerCallback(s -> s.writeBoolean(failInCallback), 0);
      return super.onInvoke(context, state, rpc);
    }

    @Override
    public StateVoid onCallback(
        SysContractContext context,
        StateVoid state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      if (rpc.readBoolean()) {
        throw new RuntimeException("expected");
      }
      return null;
    }
  }
}
