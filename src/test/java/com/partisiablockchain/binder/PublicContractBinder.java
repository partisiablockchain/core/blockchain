package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.contract.binder.AbstractPublicContractBinder;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Default binder contract for binding public contracts with the blockchain.
 *
 * @param <OpenT> the state type of the contract
 */
public final class PublicContractBinder<OpenT extends StateSerializable>
    extends AbstractPublicContractBinder<OpenT> {

  /**
   * Default constructor.
   *
   * @param contract public contract open state
   */
  public PublicContractBinder(PubContract<OpenT> contract) {
    super(contract);
  }
}
