package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.tree.AvlTree;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of {@link DelayedBlockchainListener}. */
public final class DelayedBlockchainListenerTest {

  private final RecordingListener listener = new RecordingListener();
  private final DelayedBlockchainListener delayed = new DelayedBlockchainListener(listener, 1_000);
  private Block before;
  private Block after;
  private final ImmutableChainState state =
      StateHelper.initialWithAdditions(
              state -> {
                Hash publicBinderJar = state.saveJar(TestContracts.CONTRACT_BINDER_PUBLIC);
                Hash contractJar = state.saveJar(TestContracts.CONTRACT_VOID);
                state.createContract(
                    TestObjects.CONTRACT_PUB1,
                    CoreContractStateTest.create(publicBinderJar, contractJar, 256));
                state.createContract(
                    TestObjects.CONTRACT_PUB2,
                    CoreContractStateTest.create(publicBinderJar, contractJar, 256));
              })
          .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
  private SignedTransaction transaction;

  /** Initialize state. */
  @BeforeEach
  public void setUp() {
    transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(0L, 0L, 0L),
                InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[0]))
            .sign(new KeyPair(BigInteger.TEN), "MyCID");
    before = createBlock(999);
    after = createBlock(1_000);
  }

  private Block createBlock(long productionTime) {
    Hash randomHash = Hash.create(stream -> stream.writeInt(12));
    return new Block(productionTime, 1, 1, randomHash, randomHash, List.of(), List.of());
  }

  @Test
  public void newFinalBlock() {
    delayed.newFinalBlock(before, state);
    Assertions.assertThat(listener.finalBlocks).isEqualTo(0);
    delayed.newFinalBlock(after, state);
    Assertions.assertThat(listener.finalBlocks).isEqualTo(1);
    delayed.newFinalBlock(before, state);
    Assertions.assertThat(listener.finalBlocks).isEqualTo(2);
  }

  @Test
  public void newBlockProposal() {
    delayed.newBlockProposal(after);
    Assertions.assertThat(listener.blockProposals).isEqualTo(0);
    delayed.newFinalBlock(after, state);
    delayed.newBlockProposal(after);
    Assertions.assertThat(listener.blockProposals).isEqualTo(1);
  }

  @Test
  public void newPendingTransaction() {
    delayed.newPendingTransaction(transaction);
    Assertions.assertThat(listener.transactions).isEqualTo(0);
    delayed.newFinalBlock(after, state);
    delayed.newPendingTransaction(transaction);
    Assertions.assertThat(listener.transactions).isEqualTo(1);
  }

  @Test
  public void newFinalState() {
    delayed.newFinalState(state, new UpdateEvent(state.getContracts(), Set.of(), Set.of()));
    Assertions.assertThat(listener.finalStates).isEqualTo(0);
    delayed.newFinalBlock(after, state);
    delayed.newFinalState(state, new UpdateEvent(Set.of(), Set.of(), Set.of()));
    Assertions.assertThat(listener.finalStates).isEqualTo(1);
    Assertions.assertThat(listener.contracts.newContracts)
        .hasSize(2)
        .isEqualTo(state.getContracts());
    Assertions.assertThat(listener.contracts.updatedContracts).isEmpty();
    Assertions.assertThat(listener.contracts.removedContract).isEmpty();
    delayed.newFinalState(state, new UpdateEvent(Set.of(), Set.of(), Set.of()));
    Assertions.assertThat(listener.finalStates).isEqualTo(2);
    Assertions.assertThat(listener.contracts.newContracts).isEmpty();
    Assertions.assertThat(listener.contracts.updatedContracts).isEmpty();
    Assertions.assertThat(listener.contracts.removedContract).isEmpty();

    // Decorator uses new API
    delayed.newFinalState(state);
    delayed.newFinalState(state);
    delayed.newFinalState(state);
    Assertions.assertThat(listener.finalStates).isEqualTo(2);
  }

  @Test
  public void defaultConstructor() {
    DelayedBlockchainListener blockchainListener = new DelayedBlockchainListener(listener);
    blockchainListener.newFinalBlock(createBlock(System.currentTimeMillis() + 1), state);
    Assertions.assertThat(listener.finalBlocks).isEqualTo(1);
  }

  private static final class RecordingListener implements Listener {

    int finalBlocks = 0;
    int blockProposals = 0;
    int transactions = 0;
    int finalStates = 0;
    UpdateEvent contracts;

    @Override
    public void newFinalBlock(Block block, ImmutableChainState state) {
      finalBlocks++;
    }

    @Override
    public void newBlockProposal(Block block) {
      blockProposals++;
    }

    @Override
    public void newPendingTransaction(SignedTransaction transaction) {
      transactions++;
    }

    @Override
    public void newFinalState(ImmutableChainState state, UpdateEvent contracts) {
      this.contracts = contracts;
      finalStates++;
    }

    @Override
    public String toString() {
      return "RecordingListener{}";
    }
  }

  @Test
  public void testOfToString() {
    DelayedBlockchainListener blockchainListener = new DelayedBlockchainListener(listener);
    Assertions.assertThat(blockchainListener.toString())
        .isEqualTo("DelayedBlockchainListener{decorated=RecordingListener{}}");
  }
}
