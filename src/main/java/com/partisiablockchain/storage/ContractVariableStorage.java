package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** Stores zk variables for contracts. */
public interface ContractVariableStorage {

  /**
   * Write variable to contract.
   *
   * @param address address of contract
   * @param variableId id of variable
   * @param value value of variable in bytes
   */
  void write(BlockchainAddress address, int variableId, byte[] value);

  /**
   * Remove variable from contract.
   *
   * @param address address of contract
   * @param variableId id of variable
   */
  default void remove(BlockchainAddress address, int variableId) {
    write(address, variableId, new byte[0]);
  }

  /**
   * Read variable from contract.
   *
   * @param address address of contract
   * @param variableId id of variable
   * @return variable as bytes
   */
  byte[] read(BlockchainAddress address, int variableId);

  /**
   * Check if contract contains variable.
   *
   * @param address address of contract
   * @param variableId id of variable
   * @return true if contract contains variable
   */
  boolean containsVariable(BlockchainAddress address, int variableId);
}
