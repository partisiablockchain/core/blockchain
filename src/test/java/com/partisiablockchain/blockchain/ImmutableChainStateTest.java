package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CoreContractStateTest;
import com.partisiablockchain.blockchain.contract.CreateContractTransactionTest.PubTestContract;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ImmutableChainStateTest {

  private final BlockchainAddress contract = TestObjects.CONTRACT_PUB1;
  private final BlockchainAddress account = TestObjects.ACCOUNT_ONE;

  @Test
  public void getters() {
    Hash tx1 = TestObjects.hashNumber(1);
    Hash tx2 = TestObjects.hashNumber(2);
    AvlTree<Hash, Hash> events =
        AvlTree.create(
            Map.of(
                TestObjects.hashNumber(7), tx1,
                TestObjects.hashNumber(8), tx1,
                TestObjects.hashNumber(9), tx2));
    byte[] contractJar = JarBuilder.buildJar(PubTestContract.class);
    ShardId shardId = new ShardId("Shard0");
    ImmutableChainState state =
        StateHelper.initialWithAdditions(
                builder -> {
                  CoreContractStateTest.createContract(
                      builder, contractJar, contract, TestContracts.CONTRACT_BINDER_PUBLIC);
                  builder.createAccount(account);
                  builder.addActiveShard(
                      FunctionUtility.noOpBiConsumer(), "Shard0", shardId.getId());
                })
            .asImmutable("chainId", null, events, AvlTree.create(Map.of(tx1, true, tx2, false)));

    assertThat(state.getAccount(account)).isNotNull();
    assertThat(state.existsAccounts(account)).isTrue();
    assertThat(state.existsAccounts(TestObjects.ACCOUNT_TWO)).isFalse();
    assertThat(state.getAccount(contract)).isNull();

    assertThat(state.getContractState(contract)).isNull();
    assertThat(state.getContractState(account)).isNull();

    Set<BlockchainAddress> contracts = state.getContracts();
    assertThat(contracts).hasSize(1);
    assertThat(contracts).containsExactly(contract);

    assertThat(state.getContractStorageLength(contract)).isEqualTo(contractJar.length);
    assertThat(state.getContractStorageLength(TestObjects.CONTRACT_SYS)).isNull();

    Set<ChainPluginType> plugins = state.getChainPluginTypes();
    assertThat(plugins).hasSize(1);
    assertThat(plugins).contains(ChainPluginType.ACCOUNT);

    Set<BlockchainAddress> accounts = state.getAccounts();
    assertThat(accounts).hasSize(4);
    assertThat(accounts).contains(account);

    FixedList<String> shards = state.getActiveShards();
    assertThat(shards).hasSize(1);
    assertThat(shards.contains(shardId.getId())).isTrue();

    assertThat(state.getLocalAccountPluginState()).isInstanceOf(AccountPluginState.class);
    assertThat(state.getGlobalPluginState(ChainPluginType.ACCOUNT).getClass().toString())
        .isEqualTo("class com.partisiablockchain.blockchain.fee.GasAndCoinFeePluginGlobal");
    assertThat(state.getPluginInteractions(ChainPluginType.ACCOUNT))
        .isNotNull()
        .isInstanceOf(BlockchainAccountPlugin.class);
    assertThat(state.getGlobalPluginState(ChainPluginType.CONSENSUS)).isNull();
    assertThat(state.getPluginInteractions(ChainPluginType.CONSENSUS)).isNull();
    assertThat(state.getPluginInteractions(ChainPluginType.ROUTING)).isNull();

    assertThat(state.existsAccounts(account)).isTrue();
    assertThat(state.getContract(contract)).isNotNull();

    AvlTree<Hash, Boolean> executionStatuses = state.getExecutedState().getExecutionStatus();
    assertThat(executionStatuses.keySet()).containsExactlyInAnyOrder(tx1, tx2);
    assertThat(executionStatuses.getValue(tx1)).isTrue();
    assertThat(executionStatuses.getValue(tx2)).isFalse();
    AvlTree<Hash, Hash> spawnedEvents = state.getExecutedState().getSpawnedEvents();
    assertThat(spawnedEvents.keySet())
        .containsExactlyElementsOf(state.getExecutedState().getEventTransactions())
        .containsExactlyElementsOf(events.keySet());
  }

  @Test
  public void getFeature() {
    AvlTree<String, String> features = AvlTree.create();
    features = features.set("key", "value");
    ImmutableChainState.SerializableChainState chainState =
        new ImmutableChainState.SerializableChainState(
            null, null, null, null, features, null, null, 0);
    ExecutedState mockExecutedState = Mockito.mock(ExecutedState.class);
    Mockito.when(mockExecutedState.getChainState()).thenReturn(chainState);
    ChainStateCache mockContext = Mockito.mock(ChainStateCache.class);
    Mockito.when(mockContext.createStateSerializer()).thenReturn(null);
    ImmutableChainState state = new ImmutableChainState(mockContext, mockExecutedState);
    assertThat(state.getFeature("key")).isEqualTo("value");
  }

  @Test
  public void getFeatures() {
    AvlTree<String, String> expected = AvlTree.create();
    expected = expected.set("key", "value");
    expected = expected.set("key2", "value2");
    ImmutableChainState.SerializableChainState chainState =
        new ImmutableChainState.SerializableChainState(
            null, null, null, null, expected, null, null, 0);
    ExecutedState mockExecutedState = Mockito.mock(ExecutedState.class);
    Mockito.when(mockExecutedState.getChainState()).thenReturn(chainState);
    ChainStateCache mockContext = Mockito.mock(ChainStateCache.class);
    Mockito.when(mockContext.createStateSerializer()).thenReturn(null);
    ImmutableChainState state = new ImmutableChainState(mockContext, mockExecutedState);
    assertThat(state.getFeatures()).isEqualTo(expected);
    assertThat(state.getFeature("key")).isEqualTo("value");
    assertThat(state.getFeature("key2")).isEqualTo("value2");
  }

  @Test
  void getAccountWithOpenAccountCreation() {
    ImmutableChainState state =
        StateHelper.initialWithAdditions(
                builder -> builder.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, ""))
            .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNotNull();
  }

  @Test
  void checkExistenceShouldNotReturnAddressForContractAddressWithOpenAccountActivated() {
    ImmutableChainState state =
        StateHelper.initialWithAdditions(
                builder -> {
                  builder.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "");
                })
            .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
    assertThat(state.getAccount(TestObjects.ACCOUNT_FOUR)).isNotNull();
    assertThat(state.getAccount(TestObjects.CONTRACT_SYS)).isNotNull();
    state =
        StateHelper.initialWithAdditions(
                builder -> {
                  builder.setFeature(Features.FEATURE_OPEN_ACCOUNT_CREATION, "");
                  builder.setFeature(
                      Features.FEATURE_CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT, "OK");
                })
            .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
    assertThat(state.getAccount(TestObjects.CONTRACT_PUB1)).isNull();
    assertThat(state.getAccount(TestObjects.ACCOUNT_THREE)).isNotNull();
  }

  @Test
  public void getRoutingPluginHash() {
    byte[] routingPlugin = BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_OTHER_SHARD;
    ImmutableChainState state =
        StateHelper.initialWithAdditions(
                builder -> {
                  byte[] rpc = new byte[0];
                  builder.setPlugin(
                      FunctionUtility.noOpBiConsumer(),
                      null,
                      ChainPluginType.ROUTING,
                      routingPlugin,
                      rpc);
                })
            .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
    Hash expected = state.getPluginJar(ChainPluginType.ROUTING).getIdentifier();
    assertThat(state.getPluginJarIdentifier(ChainPluginType.ROUTING)).isEqualTo(expected);
  }

  @Test
  public void getNullWhenPluginJarNotSet() {
    ImmutableChainState state =
        StateHelper.initialWithAdditions(builder -> {})
            .asImmutable("chainId", null, AvlTree.create(), AvlTree.create());
    assertThat(state.getPluginJarIdentifier(ChainPluginType.ROUTING)).isEqualTo(null);
  }

  @Test
  public void isTransactionValidInState_notRoutedToUs() {
    SignedTransaction transaction = Mockito.mock(SignedTransaction.class);
    long blockProductionTime = 2;
    Mockito.when(transaction.checkValidity(Mockito.eq(blockProductionTime), Mockito.any()))
        .thenReturn(true);

    byte[] routingPlugin = BlockchainLedgerRoutingTest.JAR_PLUGIN_ROUTING_OTHER_SHARD;
    ImmutableChainState state =
        StateHelper.initialWithAdditions(
                builder -> {
                  byte[] rpc = new byte[0];
                  builder.setPlugin(
                      FunctionUtility.noOpBiConsumer(),
                      null,
                      ChainPluginType.ROUTING,
                      routingPlugin,
                      rpc);
                  builder.addActiveShard(
                      FunctionUtility.noOpBiConsumer(), "OtherShard", "OtherShard");
                })
            .asImmutable("chainId", "OtherShard", AvlTree.create(), AvlTree.create());

    Mockito.when(transaction.getSender()).thenReturn(contract);
    Mockito.when(transaction.canCoverFee(Mockito.eq(state), Mockito.eq(blockProductionTime)))
        .thenReturn(true);
    assertThat(state.isTransactionValidInState(transaction, blockProductionTime)).isTrue();

    Mockito.when(transaction.getSender()).thenReturn(contract);
    Mockito.when(transaction.canCoverFee(Mockito.eq(state), Mockito.eq(blockProductionTime)))
        .thenReturn(false);
    assertThat(state.isTransactionValidInState(transaction, blockProductionTime)).isFalse();

    Mockito.when(transaction.getSender()).thenReturn(account);
    Mockito.when(transaction.canCoverFee(Mockito.eq(state), Mockito.eq(blockProductionTime)))
        .thenReturn(true);
    assertThat(state.isTransactionValidInState(transaction, blockProductionTime)).isFalse();
  }

  @Test
  public void isTransactionValidInState_cannotCoverNetworkFee() {
    ImmutableChainState state = StateHelper.initial();

    SignedTransaction transaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1, 30, 1),
                InteractWithContractTransaction.create(contract, new byte[0]))
            .sign(new KeyPair(BigInteger.ONE), "chain-id");

    boolean transactionValidInState = state.isTransactionValidInState(transaction, 29);
    assertThat(transactionValidInState).isFalse();
  }
}
