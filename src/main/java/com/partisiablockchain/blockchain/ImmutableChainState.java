package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainPlugin.PluginIdentifier;
import com.partisiablockchain.blockchain.ChainPlugin.SerializedPlugin;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Immutable {@link ChainState} that represents the state of the blockchain at a specific point in
 * time.
 *
 * <p>Identified by a unique hash, which is used to identify and verify the state as it is stored
 * and transferred between blockchain nodes.
 *
 * @see MutableChainState variant used as a block is being executed.
 */
public final class ImmutableChainState extends ChainState {

  private final ExecutedState executedState;
  private transient Hash hash;

  /**
   * Create immutable chain state from executed state.
   *
   * @param context the state context
   * @param executedState origin of creation
   */
  ImmutableChainState(ChainStateCache context, ExecutedState executedState) {
    super(context);
    this.executedState = executedState;
  }

  MutableChainState asMutable() {
    return new MutableChainState(
        getContext(),
        executedState.getChainState().plugins,
        executedState.getChainState().localPluginState,
        executedState.getChainState().accounts,
        executedState.getChainState().contracts,
        executedState.getChainState().features,
        executedState.getChainState().shards,
        executedState.getChainState().activeShards,
        executedState.getChainState().governanceVersion);
  }

  @Override
  public Set<BlockchainAddress> getContracts() {
    return executedState.getChainState().contracts.keySet();
  }

  @Override
  ContractState getContract(BlockchainAddress address) {
    return executedState.getChainState().contracts.getValue(address);
  }

  @Override
  public Long getContractStorageLength(BlockchainAddress address) {
    ContractState contractState = executedState.getChainState().contracts.getValue(address);
    if (contractState != null) {
      return contractState.computeStorageLength();
    } else {
      return null;
    }
  }

  @Override
  public FixedList<String> getActiveShards() {
    return executedState.getChainState().activeShards;
  }

  @Override
  public Set<ChainPluginType> getChainPluginTypes() {
    return executedState.getChainState().plugins.keySet();
  }

  @Override
  public Hash getPluginJarIdentifier(ChainPluginType type) {
    SerializedPlugin plugin = getPlugin(type);
    if (plugin == null) {
      return null;
    } else {
      PluginIdentifier identifier =
          serializer.read(plugin.getIdentifierHash(), PluginIdentifier.class);
      return identifier.getJarHash();
    }
  }

  @Override
  public LargeByteArray getPluginJar(ChainPluginType type) {
    Hash identifier = getPluginJarIdentifier(type);
    if (identifier == null) {
      return null;
    } else {
      return serializer.read(identifier, LargeByteArray.class);
    }
  }

  @Override
  public Set<BlockchainAddress> getAccounts() {
    return executedState.getChainState().accounts.keySet();
  }

  @Override
  public AccountState getAccount(BlockchainAddress address) {
    AccountState value = executedState.getChainState().accounts.getValue(address);
    if (value == null && features().hasOpenAccountCreation()) {
      if (!features().hasCheckExistenceForContractWithOpenAccount()) {
        value = AccountState.create();
      } else if (address.getType().equals(BlockchainAddress.Type.ACCOUNT)) {
        value = AccountState.create();
      }
    }
    return value;
  }

  @Override
  public AccountPluginState<?, ?, ?> getLocalAccountPluginState() {
    return (AccountPluginState<?, ?, ?>) getLocalPluginState(ChainPluginType.ACCOUNT);
  }

  @Override
  public StateSerializable getLocalConsensusPluginState() {
    return getLocalPluginState(ChainPluginType.CONSENSUS);
  }

  private StateSerializable getLocalPluginState(ChainPluginType type) {
    SerializedPlugin plugin = getPlugin(type);
    if (plugin == null) {
      return null;
    }

    SerializableChainState chainState = executedState.getChainState();
    AvlTree<ChainPluginType, ChainPlugin.LocalPlugin> localPluginState =
        chainState.localPluginState;
    return plugin.localState(getPluginSerialization(), localPluginState.getValue(type));
  }

  AvlTree<ChainPluginType, SerializedPlugin> getPlugins() {
    return executedState.getChainState().plugins;
  }

  private SerializedPlugin getPlugin(ChainPluginType pluginType) {
    return getPlugins().getValue(pluginType);
  }

  @Override
  public StateSerializable getGlobalPluginState(ChainPluginType type) {
    SerializedPlugin plugin = getPlugin(type);
    if (plugin == null) {
      return null;
    } else {
      return plugin.globalState(getPluginSerialization());
    }
  }

  @Override
  public AccountPluginAccessor getAccountPlugin() {
    SerializedPlugin plugin = getPlugin(ChainPluginType.ACCOUNT);
    if (plugin == null) {
      return new NoFeesAccountPlugin();
    } else {
      return new AccountPluginWrapper<>(
          plugin.asPlugin(
              getPluginSerialization(),
              executedState.getChainState().localPluginState.getValue(ChainPluginType.ACCOUNT)));
    }
  }

  @Override
  public ConsensusPluginAccessor getConsensusPlugin() {
    SerializedPlugin plugin = getPlugin(ChainPluginType.CONSENSUS);
    if (plugin == null) {
      return defaultConsensus();
    } else {
      return new ConsensusPluginWrapper<>(
          plugin.asPlugin(
              getPluginSerialization(),
              executedState.getChainState().localPluginState.getValue(ChainPluginType.CONSENSUS)));
    }
  }

  @Override
  public RoutingPluginAccessor getRoutingPlugin() {
    SerializedPlugin plugin = getPlugin(ChainPluginType.ROUTING);
    if (plugin == null) {
      return defaultRouting();
    } else {
      return new RoutingPluginWrapper(
          plugin.asPlugin(
              getPluginSerialization(),
              executedState.getChainState().localPluginState.getValue(ChainPluginType.ROUTING)));
    }
  }

  @Override
  public SharedObjectStorePluginAccessor getSharedObjectStorePlugin() {
    SerializedPlugin plugin = getPlugin(ChainPluginType.SHARED_OBJECT_STORE);
    if (plugin == null) {
      return defaultSharedObjectStore();
    } else {
      return new SharedObjectStorePluginWrapper<>(
          plugin.asPlugin(
              getPluginSerialization(),
              executedState
                  .getChainState()
                  .localPluginState
                  .getValue(ChainPluginType.SHARED_OBJECT_STORE)));
    }
  }

  @Override
  public PluginInteractionCreator getPluginInteractions(ChainPluginType pluginType) {
    SerializedPlugin plugin = getPlugin(pluginType);
    if (plugin == null) {
      return null;
    } else {
      return plugin.plugin(getPluginSerialization());
    }
  }

  /**
   * Get the current value of a feature.
   *
   * @param key the feature key
   * @return the current value of the feature or null if not set
   */
  @SuppressWarnings("WeakerAccess")
  public String getFeature(String key) {
    return executedState.getChainState().features.getValue(key);
  }

  /**
   * Current feature set of the blockchain.
   *
   * @return accessor for inspecting enabled features
   */
  public Features features() {
    return new Features(getFeatures());
  }

  /**
   * Get all current features on the chain.
   *
   * @return all currents features and state
   */
  public AvlTree<String, String> getFeatures() {
    return executedState.getChainState().features;
  }

  /**
   * Get the hash for this state.
   *
   * @return the hash for this state
   */
  public Hash getHash() {
    if (hash == null) {
      hash = serializer.createHash(executedState);
    }
    return hash;
  }

  /**
   * Get executed state.
   *
   * @return executed state
   */
  public ExecutedState getExecutedState() {
    return executedState;
  }

  /**
   * Store executed state.
   *
   * @param stateWriter writes state to storage and returns identifying hash
   * @return hash identifying written state
   */
  public Hash saveExecutedState(Function<StateSerializable, Hash> stateWriter) {
    hash = stateWriter.apply(executedState);
    return hash;
  }

  /**
   * Is the transaction valid in this state for the supplied block production time.
   *
   * @param transaction the transaction to check
   * @param blockProductionTime the block production time to check against
   * @return true if the transaction is valid, false otherwise
   */
  public boolean isTransactionValidInState(
      SignedTransaction transaction, long blockProductionTime) {
    if (!transaction.checkValidity(blockProductionTime, this::lookupNonce)) {
      return false;
    }
    if (!transaction.canCoverFee(this, blockProductionTime)) {
      return false;
    } else {
      return isRoutedToCurrentChain(transaction);
    }
  }

  private boolean isRoutedToCurrentChain(SignedTransaction transaction) {
    String subChainId = executedState.getSubChainId();
    String targetShard = getRoutingPlugin().route(getActiveShards(), transaction.getSender());
    return Objects.equals(targetShard, subChainId);
  }

  /**
   * Are we missing sync messages from any shards.
   *
   * @return true at least one sync message has not yet been received
   */
  public boolean isSyncing() {
    FixedList<String> shards = getActiveShards();
    AvlTree<ShardId, ShardNonces> shardNonce = getShardNonces();
    return Stream.concat(shards.stream().map(ShardId::new), Stream.ofNullable(new ShardId(null)))
        .map(shardNonce::getValue)
        .filter(Objects::nonNull)
        .anyMatch(ShardNonces::isMissingSync);
  }

  /**
   * Get nonces for shard.
   *
   * @return avl tree from shard id to shard nonces.
   */
  public AvlTree<ShardId, ShardNonces> getShardNonces() {
    return executedState.getChainState().shards;
  }

  /**
   * Get version of governance running.
   *
   * @return version of governance
   */
  public long getGovernanceVersion() {
    return executedState.getChainState().governanceVersion;
  }

  /** Serialized version of the immutable chain state. */
  @Immutable
  @SuppressWarnings("WeakerAccess")
  public static final class SerializableChainState implements StateSerializable {

    private final AvlTree<BlockchainAddress, AccountState> accounts;
    private final AvlTree<BlockchainAddress, ContractState> contracts;
    private final AvlTree<ChainPluginType, SerializedPlugin> plugins;
    private final AvlTree<ChainPluginType, ChainPlugin.LocalPlugin> localPluginState;
    private final AvlTree<String, String> features;
    private final AvlTree<ShardId, ShardNonces> shards;
    private final FixedList<String> activeShards;

    /** Version of governance running. */
    public final long governanceVersion;

    @SuppressWarnings({"unused"})
    SerializableChainState() {
      accounts = null;
      contracts = null;
      plugins = null;
      localPluginState = null;
      features = null;
      shards = null;
      activeShards = null;
      governanceVersion = 0;
    }

    SerializableChainState(
        AvlTree<BlockchainAddress, AccountState> accounts,
        AvlTree<BlockchainAddress, ContractState> contracts,
        AvlTree<ChainPluginType, SerializedPlugin> plugins,
        AvlTree<ChainPluginType, ChainPlugin.LocalPlugin> localPluginState,
        AvlTree<String, String> features,
        AvlTree<ShardId, ShardNonces> shards,
        FixedList<String> activeShards,
        long governanceVersion) {
      this.accounts = accounts;
      this.contracts = contracts;
      this.plugins = plugins;
      this.localPluginState = localPluginState;
      this.features = features;
      this.shards = shards;
      this.activeShards = activeShards;
      this.governanceVersion = governanceVersion;
    }
  }
}
