package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventProofTest {

  private final StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();

  @Test
  public void constructWithDifferentGovernance() {
    MutableChainState state = stateWithGovernance();
    createAndVerify(state, null);
    createAndVerify(state, "MySubChain");
  }

  @Test
  public void unableToVerify() {
    EventTransaction event =
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_FOUR,
            10L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("DestinationShard", 27),
            0,
            1);
    ExecutableEvent executableEvent = new ExecutableEvent("MySubChain", event);
    StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            stateWithGovernance(), "ChainId", "MySubChain", executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    EventProof eventProof =
        EventProof.constructEventProof(stateStorage, state.getHash(), executableEvent.identifier());
    Assertions.assertThatThrownBy(
            () -> eventProof.verify(TestObjects.EMPTY_HASH, event.identifier()))
        .hasMessageContaining("State hash do not match expected hash");

    Assertions.assertThatThrownBy(
            () ->
                eventProof.verify(
                    state.getHash(),
                    new EventTransaction(
                        TestObjects.EMPTY_HASH,
                        new ShardRoute("SomeWhere", 0),
                        0,
                        1,
                        0,
                        null,
                        new InnerSystemEvent.RemoveContract(TestObjects.CONTRACT_SYS))))
        .hasMessageContaining("Proof was for another event");
  }

  @Test
  public void mismatchStateAndTree() {
    EventTransaction event =
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_FOUR,
            10L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("DestinationShard", 27),
            0,
            1);
    ExecutableEvent executableEvent = new ExecutableEvent("MySubChain", event);
    EventTransaction secondEvent =
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_FOUR,
            11L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute("DestinationShard", 27),
            0,
            1);
    ExecutableEvent secondExecutableEvent = new ExecutableEvent("MySubChain", secondEvent);
    StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            stateWithGovernance(), "ChainId", "MySubChain", executableEvent.identifier());
    ImmutableChainState secondState =
        StateHelper.withEvent(
            stateWithGovernance(), "ChainId", "MySubChain", secondExecutableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    stateSerializer.write(secondState.getExecutedState());
    EventProof eventProof =
        EventProof.constructEventProof(stateStorage, state.getHash(), executableEvent.identifier());
    EventProof otherProof =
        EventProof.constructEventProof(
            stateStorage, secondState.getHash(), secondExecutableEvent.identifier());

    EventProof invalidProof = new EventProof(eventProof.getStateProof(), otherProof.getAvlProof());
    Assertions.assertThatThrownBy(
            () -> invalidProof.verify(state.getHash(), secondExecutableEvent.getEvent()))
        .hasMessageContaining("Tree root do not match expected hash.");
  }

  private MutableChainState stateWithGovernance() {
    return StateHelper.mutableFromPopulate(storage, FunctionUtility.noOpConsumer());
  }

  private void createAndVerify(MutableChainState mutableChainState, String subChainId) {
    ExecutableEvent executableEvent =
        new ExecutableEvent(
            subChainId,
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                TestObjects.ACCOUNT_FOUR,
                10L,
                InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
                new ShardRoute("DestinationShard", 27),
                0,
                1));
    StateStorageRaw stateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(stateStorage, true);
    ImmutableChainState state =
        StateHelper.withEvent(
            mutableChainState, "ChainId", subChainId, executableEvent.identifier());
    stateSerializer.write(state.getExecutedState());
    EventProof eventProof =
        EventProof.constructEventProof(stateStorage, state.getHash(), executableEvent.identifier());

    EventProof.ChainIds shardId = eventProof.verify(state.getHash(), executableEvent.identifier());
    Assertions.assertThat(shardId.chainId()).isEqualTo("ChainId");
    Assertions.assertThat(shardId.subChainId()).isEqualTo(subChainId);
  }
}
