package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger.BlockAndState;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;

/**
 * The complete result of executing a block. When a block is completely executed, this results in a
 * a final block, an update event (which contracts have changed) and the current, final state.
 */
public final class BlockAndStateWithParent {

  private final BlockAndState current;
  private final ImmutableChainState finalizedState;
  private final UpdateEvent contracts;

  BlockAndStateWithParent(
      BlockAndState current, ImmutableChainState finalizedState, UpdateEvent contracts) {
    this.current = current;
    this.finalizedState = finalizedState;
    this.contracts = contracts;
  }

  /**
   * Get the current final state.
   *
   * @return current final state
   */
  public ImmutableChainState finalState() {
    return finalizedState;
  }

  /**
   * Get the current block and state.
   *
   * @return current block and state
   */
  public BlockAndState current() {
    return current;
  }

  /**
   * Get the current state.
   *
   * @return current state
   */
  public ImmutableChainState currentState() {
    return current().getState();
  }

  /**
   * Get the current block.
   *
   * @return current block
   */
  public Block currentBlock() {
    return current.getBlock();
  }

  /**
   * Get the update event.
   *
   * @return update event
   */
  public UpdateEvent contracts() {
    return contracts;
  }
}
