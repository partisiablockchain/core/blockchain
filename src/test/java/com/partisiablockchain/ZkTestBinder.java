package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import java.util.function.Supplier;

/** Test. */
public final class ZkTestBinder implements ZkBinderContract<StateString> {

  private final ZkContract<StateString, StateVoid, ZkClosed<StateVoid>, Supplier<String>> contract;

  /**
   * Construct a new binder.
   *
   * @param contract the associated contract
   */
  public ZkTestBinder(
      ZkContract<StateString, StateVoid, ZkClosed<StateVoid>, Supplier<String>> contract) {
    this.contract = contract;
  }

  @Override
  public Class<StateString> getStateClass() {
    return StateString.class;
  }

  @Override
  public List<Class<?>> getStateClassTypeParameters() {
    return List.of();
  }

  @Override
  public ZkContract<?, ?, ?, ?> getContract() {
    return contract;
  }

  @Override
  public BinderResult<StateString, BinderInteraction> create(ZkBinderContext context, byte[] rpc) {
    return result(contract.onCreate(null, null, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<StateString, BinderInteraction> invoke(
      ZkBinderContext context, StateString state, byte[] rpc) {
    return result(
        contract.onOpenInput(null, null, state, SafeDataInputStream.createFromBytes(rpc)));
  }

  @Override
  public BinderResult<StateString, BinderInteraction> callback(
      ZkBinderContext context, StateString state, CallbackContext callbackContext, byte[] rpc) {
    return result(
        contract.onCallback(
            null, null, state, callbackContext, SafeDataInputStream.createFromBytes(rpc)));
  }

  private BinderResult<StateString, BinderInteraction> result(StateString state) {
    return new CommonBinderResult<>(state);
  }

  static final class CommonBinderResult<StateT> implements BinderResult<StateT, BinderInteraction> {

    private final StateT state;

    CommonBinderResult(StateT state) {
      this.state = state;
    }

    @Override
    public StateT getState() {
      return state;
    }

    @Override
    public List<BinderInteraction> getInvocations() {
      return List.of();
    }

    @Override
    public CallResult getCallResult() {
      return null;
    }
  }
}
