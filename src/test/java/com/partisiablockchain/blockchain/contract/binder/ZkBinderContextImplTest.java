package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test for {@link ZkBinderContextImpl}. */
public final class ZkBinderContextImplTest extends ContextTest<ZkBinderContextImpl> {

  /** Sets up the test. */
  @BeforeEach
  public void setUp() throws Exception {
    context = new ZkBinderContextImpl(contractContext, executionContext);
  }

  @Test
  public void payServiceFees() {
    long gas = 100_000L;
    FixedList<BlockchainAddress> targets =
        FixedList.create(
            List.of(TestObjects.ACCOUNT_ONE, TestObjects.ACCOUNT_THREE, TestObjects.ACCOUNT_FOUR));
    FixedList<Integer> weights = FixedList.create(List.of(45, 50, 5));

    context.payServiceFees(gas, targets, weights);

    List<PendingFee> pendingServiceFees = context.getExecutionContext().getPendingServiceFees();

    Assertions.assertThat(pendingServiceFees)
        .usingRecursiveComparison()
        .isEqualTo(
            List.of(
                PendingFee.create(TestObjects.ACCOUNT_ONE, (long) (gas * (45D / 100))),
                PendingFee.create(TestObjects.ACCOUNT_THREE, (long) (gas * (50D / 100))),
                PendingFee.create(TestObjects.ACCOUNT_FOUR, (long) (gas * (5D / 100)))));
  }

  @Test
  public void cpuFee() {
    Assertions.assertThat(feeCollector.sum()).isEqualTo(0);
    context.registerCpuFee(7);
    Assertions.assertThat(feeCollector.sum()).isEqualTo(7);
    Assertions.assertThat(context.availableGas()).isEqualTo(100);
  }
}
