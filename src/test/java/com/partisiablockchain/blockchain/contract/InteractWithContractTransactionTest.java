package com.partisiablockchain.blockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.ZkTestBinder;
import com.partisiablockchain.blockchain.ContractState;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.TestContracts;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransactionTest.TestSysContract.DummyState;
import com.partisiablockchain.blockchain.transaction.CallbackCreator;
import com.partisiablockchain.blockchain.transaction.EventCollector;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutionContextTransactionTest;
import com.partisiablockchain.blockchain.transaction.ExecutionEventManager;
import com.partisiablockchain.blockchain.transaction.FeeCollector;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class InteractWithContractTransactionTest {

  private static final long blockProductionTime = 112233L;

  private int nextState = 0;
  private final KeyPair producerKey = new KeyPair(BigInteger.TWO);
  private final BlockchainAddress contractId =
      BlockchainAddress.fromString("0201010101010101010101010101010101010101FF");
  private final BlockchainAddress sysContractId =
      BlockchainAddress.fromString("0101010101010101010101010101010101010101EE");
  private final BlockchainAddress taceContractId =
      BlockchainAddress.fromString("0301010101010101010101010101010101010101AB");
  private BlockchainAddress sender;
  private ExecutionEventManager eventManager;
  private MutableChainState state;

  /** Creating a test environment. */
  @BeforeEach
  public void setUp() {
    FeeCollector feeCollector = FeeCollector.create(null, 0L);
    eventManager =
        new ExecutionEventManager(
            () ->
                new EventCollector(
                    null, state, 1L, 1L, TestObjects.EMPTY_HASH, 0, null, feeCollector));
    sender = producerKey.getPublic().createAddress();
    state =
        StateHelper.initialWithAdditions(
            builder -> {
              CoreContractStateTest.createContract(
                  builder,
                  JarBuilder.buildJar(TestContractByInvocation.class),
                  contractId,
                  TestContracts.CONTRACT_BINDER_PUBLIC);
              CoreContractStateTest.createContract(
                  builder,
                  JarBuilder.buildJar(TestSysContract.class),
                  sysContractId,
                  TestContracts.CONTRACT_BINDER_SYSTEM);

              ContractState.CallbackInfo callbackInfo =
                  ContractState.CallbackInfo.create(List.of(), 0L, new LargeByteArray(new byte[0]));
              builder.addCallbacks(
                  sender, sysContractId, null, TestObjects.EMPTY_HASH, callbackInfo);
              CoreContractStateTest.createContract(
                  builder,
                  JarBuilder.buildJar(ZkTestContract.class),
                  taceContractId,
                  JarBuilder.buildJar(ZkTestBinder.class),
                  binder ->
                      binder
                          .create(
                              executionContext(),
                              null,
                              SafeDataOutputStream.serialize(
                                  rpc -> {
                                    producerKey.getPublic().write(rpc);
                                    rpc.writeString("test0");
                                    producerKey.getPublic().write(rpc);
                                    rpc.writeString("test1");
                                    producerKey.getPublic().write(rpc);
                                    rpc.writeString("test2");
                                  }))
                          .getState());
              builder.addCallbacks(
                  TestObjects.ACCOUNT_ONE,
                  taceContractId,
                  null,
                  TestObjects.EMPTY_HASH,
                  callbackInfo);
            });
  }

  @Test
  public void interactWithPub() {
    InteractWithContractTransaction tx = createTx();
    tx.execute(executionContext());
    StateSerializable contractState = state.getContractState(contractId);
    Assertions.assertThat(contractState).hasFieldOrPropertyWithValue("state", nextState);
  }

  @Test
  public void sysContractPaysValid() {
    InteractWithContractTransaction tx =
        InteractWithContractTransaction.create(sysContractId, new byte[0]);
    tx.execute(executionContext());
    StateSerializable contractState = state.getContractState(sysContractId);
    Assertions.assertThat(contractState).hasFieldOrPropertyWithValue("invoked", true);
  }

  @Test
  public void sysContractCallback() {
    CallbackToContract tx =
        new CallbackToContract(
            sysContractId,
            new CallbackCreator.Callback(
                TestObjects.EMPTY_HASH, sender, 5_000L, new LargeByteArray(new byte[0])));
    tx.execute(executionContext());
    StateSerializable contractState = state.getContractState(sysContractId);
    Assertions.assertThat(contractState).hasFieldOrPropertyWithValue("callback", true);
  }

  @Test
  public void sysContractUnusedRpc() {
    InteractWithContractTransaction tx =
        InteractWithContractTransaction.create(sysContractId, new byte[1]);

    Assertions.assertThatThrownBy(() -> tx.execute(executionContext()))
        .hasMessageContaining("Entire content of stream was not read");
  }

  @Test
  public void zkInteraction() {
    String updatedState = "Updated value";
    InteractWithContractTransaction tx =
        InteractWithContractTransaction.create(
            taceContractId,
            SafeDataOutputStream.serialize(stream -> stream.writeString(updatedState)));
    tx.execute(executionContext());
    StateString contractState = (StateString) state.getContractState(taceContractId);
    Assertions.assertThat(contractState.value()).isEqualTo(updatedState);
  }

  @Test
  public void zkCallback() {
    String updatedState = "Updated value in callback";
    CallbackToContract tx =
        new CallbackToContract(
            taceContractId,
            new CallbackCreator.Callback(
                TestObjects.EMPTY_HASH,
                sender,
                5_000L,
                new LargeByteArray(
                    SafeDataOutputStream.serialize(stream -> stream.writeString(updatedState)))));
    tx.execute(executionContext());
    StateString contractState = (StateString) state.getContractState(taceContractId);
    Assertions.assertThat(contractState.value()).isEqualTo(updatedState);
  }

  private ExecutionContextTransaction executionContext() {
    return ExecutionContextTransactionTest.create(
        "ChainId", state, sender, 1L, blockProductionTime, eventManager, 0);
  }

  @Test
  public void invalidTransactionWithMissingContract() {
    InteractWithContractTransaction transaction =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]);
    Assertions.assertThatThrownBy(() -> transaction.execute(executionContext()))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("Contract does not exist");
  }

  private InteractWithContractTransaction createTx() {
    nextState++;
    return InteractWithContractTransaction.create(
        contractId,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeInt(nextState);
              stream.writeLong(blockProductionTime);
            }));
  }

  /** Test. */
  public static final class TestSysContract extends SysContract<DummyState> {

    @Immutable
    static final class DummyState implements StateSerializable {

      final boolean invoked;
      final boolean callback;

      @SuppressWarnings("unused")
      DummyState() {
        invoked = false;
        callback = false;
      }

      DummyState(boolean invoked, boolean callback) {
        this.invoked = invoked;
        this.callback = callback;
      }
    }

    @Override
    public DummyState onCallback(
        SysContractContext context,
        DummyState state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      return new DummyState(false, true);
    }

    @Override
    public DummyState onInvoke(
        SysContractContext context, DummyState state, SafeDataInputStream rpc) {
      return new DummyState(true, false);
    }
  }
}
