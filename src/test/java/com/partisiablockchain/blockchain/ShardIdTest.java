package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardIdTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(ShardId.class).verify();
  }

  @Test
  public void getId() {
    Assertions.assertThat(new ShardId("IdString").getId()).isEqualTo("IdString");
    Assertions.assertThat(new ShardId(null).getId()).isNull();
  }

  @Test
  public void compareTo() {
    ShardId governanceShard = new ShardId(null);
    ShardId shard1 = new ShardId("1");
    ShardId shard2 = new ShardId("2");
    List<ShardId> shardIds =
        Arrays.asList(governanceShard, shard1, shard2, governanceShard, shard1);
    Collections.sort(shardIds);
    Assertions.assertThat(shardIds)
        .containsExactly(governanceShard, governanceShard, shard1, shard1, shard2);
  }
}
