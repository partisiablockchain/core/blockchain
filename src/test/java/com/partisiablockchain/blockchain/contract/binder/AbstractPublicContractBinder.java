package com.partisiablockchain.blockchain.contract.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.binder.pub.PubBinderContext;
import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.EventCreatorImpl;
import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.contract.EventManagerImpl;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.pub.PubContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Super class for public contract binder, shared for upgradable and non-upgradable public
 * contracts.
 */
public abstract class AbstractPublicContractBinder<OpenT extends StateSerializable>
    implements PubBinderContract<OpenT> {

  protected final PubContract<OpenT> contract;

  /**
   * Default constructor.
   *
   * @param contract public contract open state
   */
  public AbstractPublicContractBinder(PubContract<OpenT> contract) {
    this.contract = contract;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<OpenT> getStateClass() {
    Type genericSuperclass = contract.getClass().getGenericSuperclass();
    Type[] typeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
    return (Class<OpenT>) typeArguments[0];
  }

  private BinderResult<OpenT, BinderInteraction> execute(
      PubBinderContext context,
      byte[] rpc,
      BiFunction<PubContractContext, SafeDataInputStream, OpenT> function) {
    PubContext pubContext = new PubContext(context);
    OpenT openT = SafeDataInputStream.readFully(rpc, s -> function.apply(pubContext, s));
    long availableGas = context.availableGas();
    pubContext.distributeGas(availableGas);
    return createResult(openT, pubContext);
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> create(PubBinderContext context, byte[] rpc) {
    return execute(context, rpc, contract::onCreate);
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> invoke(
      PubBinderContext context, OpenT state, byte[] rpc) {
    return execute(
        context, rpc, (pubContext, stream) -> contract.onInvoke(pubContext, state, stream));
  }

  @Override
  public BinderResult<OpenT, BinderInteraction> callback(
      PubBinderContext context, OpenT state, CallbackContext callbackContext, byte[] rpc) {
    return execute(
        context,
        rpc,
        (pubContext, stream) -> contract.onCallback(pubContext, state, callbackContext, stream));
  }

  private BinderResult<OpenT, BinderInteraction> createResult(OpenT openT, PubContext events) {
    return new InnerBinderResult<>(events, openT);
  }

  /** Implements a public contract context for test purposes. */
  protected static final class PubContext implements PubContractContext {

    private final PubBinderContext context;
    private final List<ContractEvent> invocations;
    private ContractEventGroup remoteCalls;
    private byte[] result;

    public PubContext(PubBinderContext context) {
      this.context = context;
      this.invocations = new ArrayList<>();
    }

    @Override
    public BlockchainAddress getContractAddress() {
      return context.getContractAddress();
    }

    @Override
    public EventCreator getInvocationCreator() {
      EventCreatorImpl eventManager = new EventCreatorImpl(invocations::add);
      return eventManager;
    }

    @Override
    public EventManager getRemoteCallsCreator() {
      if (remoteCalls == null) {
        remoteCalls = new ContractEventGroup();
      }
      EventManagerImpl eventManager = new EventManagerImpl(remoteCalls);
      return eventManager;
    }

    @Override
    public void setResult(DataStreamSerializable result) {
      this.result = SafeDataOutputStream.serialize(result);
    }

    @Override
    public long getBlockTime() {
      return context.getBlockTime();
    }

    @Override
    public long getBlockProductionTime() {
      return context.getBlockProductionTime();
    }

    @Override
    public BlockchainAddress getFrom() {
      return context.getFrom();
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return context.getCurrentTransactionHash();
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return context.getOriginalTransactionHash();
    }

    void distributeGas(long remainingGas) {
      DistributionNeed distributionNeed = new DistributionNeed(remainingGas);
      findNeed(distributionNeed, remoteCalls);
      findNeed(distributionNeed, invocations);

      if (distributionNeed.remainingGas < 0) {
        throw new RuntimeException(
            "Cannot allocate gas for events: " + distributionNeed.remainingGas);
      }

      if (distributionNeed.count > 0) {
        long allowedCost = distributionNeed.remainingGas / distributionNeed.count;
        distribute(allowedCost, remoteCalls);
        distribute(allowedCost, invocations);
      }
    }

    public void distribute(long allowedCost, ContractEventGroup eventGroup) {
      if (eventGroup != null) {
        if (eventGroup.callbackCost == null) {
          eventGroup.callbackCost = allowedCost;
        }
        distribute(allowedCost, eventGroup.contractEvents);
      }
    }

    public void distribute(long allowedCost, List<ContractEvent> eventList) {
      for (ContractEvent contractEvent : eventList) {
        ContractEventInteraction event = (ContractEventInteraction) contractEvent;
        if (!event.costFromContract && event.allocatedCost == null) {
          event.allocatedCost = allowedCost;
        }
      }
    }

    void findNeed(DistributionNeed distributionNeed, ContractEventGroup eventGroup) {
      if (eventGroup != null) {
        if (eventGroup.callbackCost != null) {
          if (!eventGroup.callbackCostFromContract) {
            distributionNeed.remainingGas -= eventGroup.callbackCost;
          }
        } else {
          distributionNeed.count++;
        }
        findNeed(distributionNeed, eventGroup.contractEvents);
      }
    }

    void findNeed(DistributionNeed distributionNeed, List<ContractEvent> eventList) {
      for (ContractEvent contractEvent : eventList) {
        ContractEventInteraction event = (ContractEventInteraction) contractEvent;
        if (!event.costFromContract) {
          if (event.allocatedCost != null) {
            distributionNeed.remainingGas -= event.allocatedCost;
          } else {
            distributionNeed.count++;
          }
        }
      }
    }

    CallResult getRemoteCalls() {
      if (remoteCalls == null) {
        if (result != null) {
          return new CallReturnValue(result);
        } else {
          return null;
        }
      }
      List<BinderInteraction> binderInteractions =
          remoteCalls.contractEvents.stream().map(this::convert).toList();
      if (remoteCalls.callbackRpc != null) {
        return new BinderEventGroup<>(
            remoteCalls.callbackRpc,
            remoteCalls.callbackCost,
            remoteCalls.callbackCostFromContract,
            binderInteractions);
      } else {
        return null;
      }
    }

    List<BinderInteraction> getInteractions() {
      return invocations.stream().map(this::convert).toList();
    }

    public BinderInteraction convert(ContractEvent contractEvent) {
      ContractEventInteraction event = (ContractEventInteraction) contractEvent;
      return new BinderInteraction(
          event.contract,
          SafeDataOutputStream.serialize(event.rpc),
          event.originalSender,
          event.allocatedCost,
          event.costFromContract);
    }
  }

  static final class DistributionNeed {

    int count = 0;
    long remainingGas;

    public DistributionNeed(long remainingGas) {
      this.remainingGas = remainingGas;
    }
  }

  static final class InnerBinderResult<OpenT> implements BinderResult<OpenT, BinderInteraction> {

    private final OpenT openT;
    private final CallResult callResult;
    private final List<BinderInteraction> invocations;

    public InnerBinderResult(PubContext context, OpenT openT) {
      this.openT = openT;
      this.callResult = context.getRemoteCalls();
      this.invocations = context.getInteractions();
    }

    @Override
    public OpenT getState() {
      return openT;
    }

    @Override
    public List<BinderInteraction> getInvocations() {
      return invocations;
    }

    @Override
    public CallResult getCallResult() {
      return callResult;
    }
  }
}
