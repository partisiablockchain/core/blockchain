package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LivenessMonitorTest {

  private LivenessMonitor livenessMonitor;

  @Test
  public void shouldWaitAtLeastTimeoutBeforeFirstCall() throws InterruptedException {
    AtomicLong calledAt = new AtomicLong();
    long before = System.currentTimeMillis();
    livenessMonitor =
        new LivenessMonitor(
            () -> {
              if (livenessMonitor != null) {
                calledAt.set(System.currentTimeMillis());
                livenessMonitor.close();
              }
            },
            () -> 0,
            100);

    int i = 0;
    while (calledAt.get() == 0 && i++ < 20) {
      Thread.sleep(30);
    }

    Assertions.assertThat(calledAt.get()).isGreaterThanOrEqualTo(before + 100);
  }

  @Test
  public void shouldNotNotifyAfterSleep() throws InterruptedException {
    AtomicBoolean notified = new AtomicBoolean();
    AtomicInteger iterations = new AtomicInteger();
    livenessMonitor =
        new LivenessMonitor(
            () -> notified.set(true),
            () -> {
              iterations.getAndIncrement();
              return System.currentTimeMillis();
            },
            30);

    int i = 0;
    while (iterations.get() < 2 && i++ < 20) {
      Thread.sleep(30);
    }

    Assertions.assertThat(iterations).hasValueGreaterThanOrEqualTo(2);
    Assertions.assertThat(notified).isFalse();
    livenessMonitor.close();
  }

  @Test
  public void close() {
    LivenessMonitor livenessMonitor = new LivenessMonitor(() -> {}, () -> 0, 10);
    Assertions.assertThat(livenessMonitor.isRunning()).isTrue();
    livenessMonitor.close();
    Assertions.assertThat(livenessMonitor.isRunning()).isFalse();
  }

  @Test
  public void computeRemaining() {
    Assertions.assertThat(LivenessMonitor.computeRemaining(100, 10, 50)).isEqualTo(60);
    Assertions.assertThat(LivenessMonitor.computeRemaining(0, 10, 50)).isEqualTo(-40);
  }

  @Test
  public void calculateInitialNotify() {
    testInitialNotify(10, 10);
    testInitialNotify(10, 15);
    testInitialNotify(15, 10);
    testInitialNotify(700, 10);
  }

  private void testInitialNotify(int initialTimeout, int timeout) {
    long now = System.currentTimeMillis();
    long initialNotify = LivenessMonitor.calculateInitialNotify(now, initialTimeout, timeout);
    long nextNotify = initialNotify + timeout;
    Assertions.assertThat(nextNotify).isEqualTo(now + initialTimeout);
  }

  @Test
  public void sleep() throws InterruptedException {
    Assertions.assertThat(LivenessMonitor.sleep(1)).isTrue();

    Assertions.assertThat(LivenessMonitor.sleep(0)).isFalse();
    Assertions.assertThat(LivenessMonitor.sleep(-1)).isFalse();
  }

  @Test
  public void sleepShouldCallSleep() throws InterruptedException {
    long before = System.currentTimeMillis();
    LivenessMonitor.sleep(50);
    long after = System.currentTimeMillis();
    Assertions.assertThat(after - before).isGreaterThanOrEqualTo(50);
  }
}
