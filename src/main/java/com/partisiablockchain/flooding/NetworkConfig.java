package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.function.Supplier;

/** Configuration used to initialize the flooding network. */
public final class NetworkConfig {

  private final Address nodeAddress;
  private final Supplier<Address> knownNodes;
  private final Address persistentConnection;

  /**
   * Create a new configuration used to initialize the network.
   *
   * @param nodeAddress own address
   * @param knownNodes a supplier to get external nodes, e.g. kademlia
   * @param persistentConnection a remote node that should always be connected if possible
   */
  public NetworkConfig(
      Address nodeAddress, Supplier<Address> knownNodes, Address persistentConnection) {
    this.nodeAddress = nodeAddress;
    this.knownNodes = knownNodes;
    this.persistentConnection = persistentConnection;
  }

  Address getNodeAddress() {
    return nodeAddress;
  }

  Supplier<Address> getKnownNodes() {
    return knownNodes;
  }

  Address getPersistentConnection() {
    return persistentConnection;
  }
}
