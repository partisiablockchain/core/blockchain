package com.partisiablockchain.blockchain.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.contract.CallbackToContract;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction.InnerTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventTransactionTest {

  @Test
  public void toStringTest() {
    String shardId = "destinationShard";
    EventTransaction eventTransaction =
        EventTransaction.createStandalone(
            TestObjects.EMPTY_HASH,
            TestObjects.ACCOUNT_ONE,
            10_000L,
            InteractWithContractTransaction.create(TestObjects.CONTRACT_PUB1, new byte[0]),
            new ShardRoute(shardId, 27),
            0,
            1L);
    Assertions.assertThat(eventTransaction.toString())
        .contains(((InnerTransaction) eventTransaction.getInner()).getSender().toString())
        .contains(eventTransaction.identifier().toString())
        .contains(shardId)
        .contains("27")
        .contains(Long.toString(((InnerTransaction) eventTransaction.getInner()).getCost()));

    Assertions.assertThat(eventTransaction.identifier()).isEqualTo(Hash.create(eventTransaction));
  }

  @Test
  public void callback() {
    String shardId = "destinationShard";
    EventTransaction eventTransaction =
        EventTransaction.create(
            TestObjects.EMPTY_HASH,
            new CallbackToContract(
                TestObjects.CONTRACT_PUB1,
                new CallbackCreator.Callback(
                    TestObjects.EMPTY_HASH,
                    TestObjects.ACCOUNT_ONE,
                    5_000L,
                    new LargeByteArray(new byte[0]))),
            new ShardRoute(shardId, 27),
            0,
            1L,
            22);
    Assertions.assertThat(eventTransaction.toString())
        .contains(eventTransaction.identifier().toString())
        .contains(shardId)
        .contains("27")
        .contains(Long.toString(5_000))
        .contains("CallbackToContract")
        .contains(TestObjects.CONTRACT_PUB1.toString());

    Assertions.assertThat(eventTransaction.identifier()).isEqualTo(Hash.create(eventTransaction));
  }
}
