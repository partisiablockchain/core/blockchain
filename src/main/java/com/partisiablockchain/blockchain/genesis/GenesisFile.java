package com.partisiablockchain.blockchain.genesis;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/** Genesis file reading. */
public final class GenesisFile {
  /** Version 1 of genesis file. */
  public static final int VERSION_1 = 1;

  private static final List<Integer> KNOWN_VERSIONS = List.of(VERSION_1);

  private final SimpleZipFile zipFile;
  private final int versionNumber;

  /**
   * Initialize genesis file from file.
   *
   * @param file the file with genesis data.
   */
  public GenesisFile(File file) {
    this.zipFile = new SimpleZipFile(file);
    this.versionNumber = readVersionNumber();
  }

  /**
   * Read version number from zip file.
   *
   * @return the version number present in zipFile. 0 if no version number found.
   */
  private int readVersionNumber() {
    SimpleZipFile.Entry entry = zipFile.getEntry(GenesisStorage.GENESIS_VERSION_FILE);
    if (entry == null) {
      return 0;
    } else {
      int readVersion = entry.read(SafeDataInputStream::readInt, "Malformed version");
      if (!KNOWN_VERSIONS.contains(readVersion)) {
        throw new IllegalArgumentException("Unknown genesis version: " + readVersion);
      }

      return readVersion;
    }
  }

  /**
   * Returns the version of the genesis file.
   *
   * @return the version of the genesis file.
   */
  public int getVersion() {
    return versionNumber;
  }

  /**
   * Returns true if the genesis file supports transactions.
   *
   * @return true if genesis file supports transactions.
   */
  public boolean supportsTransactions() {
    return getVersion() >= 1;
  }

  /**
   * Write state from genesis file to state storage.
   *
   * @param stateStorage the state storage to write genesis state to.
   */
  public void populateStateStorage(StateStorage stateStorage) {
    List<SimpleZipFile.Entry> entries =
        zipFile.filterEntries(
            entry -> {
              String name = entry.getName();

              boolean isGenesisBlockFile = name.equals(GenesisStorage.GENESIS_BLOCK_FILE);
              boolean isVersionFile = name.equals(GenesisStorage.GENESIS_VERSION_FILE);
              boolean isInTransactionDir = name.startsWith(GenesisStorage.TRANSACTIONS_DIR);
              boolean isInEventsDir = name.startsWith(GenesisStorage.EVENTS_DIR);

              return !isGenesisBlockFile && !isVersionFile && !isInTransactionDir && !isInEventsDir;
            });

    for (SimpleZipFile.Entry entry : entries) {
      byte[] data = entry.readBytes();
      Hash identifier = Hash.fromString(entry.getName());
      stateStorage.write(identifier, s -> s.write(data));
    }
  }

  /**
   * Read genesis block from genesis file.
   *
   * @return genesis block from genesis file
   */
  public FinalBlock readGenesisBlockFromZip() {
    SimpleZipFile.Entry entry = zipFile.getEntry(GenesisStorage.GENESIS_BLOCK_FILE);
    return entry.read(FinalBlock::read, "Unable to read genesis block");
  }

  /**
   * Read transactions from genesis file that are in transactionHashes.
   *
   * @param chainId the id of the chain.
   * @param transactionHashes the events that should be read.
   * @return the transactions read from the genesis file.
   */
  public List<SignedTransaction> readTransactionsFromGenesisZip(
      String chainId, List<Hash> transactionHashes) {
    List<SignedTransaction> signedTransactions = new ArrayList<>();

    for (Hash transactionHash : transactionHashes) {
      String entryName = GenesisStorage.TRANSACTIONS_DIR + transactionHash;

      SimpleZipFile.Entry entry = zipFile.getEntry(entryName);
      SignedTransaction signedTransaction =
          entry.read(
              stream -> SignedTransaction.read(chainId, stream), "Unable to read transactions");

      signedTransactions.add(signedTransaction);
    }

    return signedTransactions;
  }

  /**
   * Read events from genesis file that are in eventTransactionHashes.
   *
   * @param eventTransactionHashes the events that should be read.
   * @return the events read from the genesis file.
   */
  public List<ExecutableEvent> readEventsFromGenesisZip(List<Hash> eventTransactionHashes) {
    List<ExecutableEvent> events = new ArrayList<>();
    for (Hash eventTransactionHash : eventTransactionHashes) {
      String entryName = GenesisStorage.EVENTS_DIR + eventTransactionHash;

      SimpleZipFile.Entry entry = zipFile.getEntry(entryName);
      events.add(entry.read(ExecutableEvent::read, "Could not read events"));
    }

    return events;
  }
}
