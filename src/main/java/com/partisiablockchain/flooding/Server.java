package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ContinuousExecution;
import com.secata.tools.thread.ExecutionFactory;
import java.io.Closeable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Internal server for the flooding network, monitors this node's server socket. */
public final class Server implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Server.class);
  static final String SERVER_SOCKET_THREAD_NAME = "Server";
  static final String FLOODING_SERVER_THREAD_NAME = "FloodingServer";

  private final ExecutorService executor;

  private final BiConsumer<Address, Socket> creator;
  private final ServerSocket serverSocket;
  private final ContinuousExecution serverLoop;

  Server(int port, BiConsumer<Address, Socket> creator, ExecutionFactory executionFactory) {
    // Intentional suppressed, TLS connection is established by Connection class.
    // It just doesn't use Java SSL sockets.
    // nosemgrep
    this(
        ExceptionConverter.call(() -> new ServerSocket(port), "Unable to bind"),
        creator,
        executionFactory);
  }

  Server(
      ServerSocket serverSocket,
      BiConsumer<Address, Socket> creator,
      ExecutionFactory executionFactory) {
    this.serverSocket = serverSocket;
    this.creator = creator;
    this.serverLoop =
        executionFactory
            .newContinuousExecution(
                SERVER_SOCKET_THREAD_NAME, () -> socketAccepted(serverSocket.accept()))
            .onException(this::close)
            .logger(logger::trace, "Error")
            .start();
    executor = executionFactory.newCachedThreadPool(FLOODING_SERVER_THREAD_NAME);
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void socketAccepted(final Socket accept) {
    executor.submit(
        () -> {
          Address address = new Address(accept.getInetAddress().getHostAddress(), accept.getPort());
          creator.accept(address, accept);
        });
  }

  @Override
  public void close() {
    if (serverLoop.isRunning()) {
      serverLoop.close();
      executor.shutdownNow();
      ExceptionLogger.handle(logger::debug, serverSocket::close, "Unable to close socket");
    }
  }

  boolean isClosed() {
    return !serverLoop.isRunning();
  }
}
