package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.jarutil.JarBuilder;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link JarClassLoader}. */
public final class JarClassLoaderTest {

  /** JAR files cannot be loaded without a file entry named {@code main}. */
  @Test
  public void unableToLoadWithoutMainFile() throws Exception {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    JarOutputStream jarOutputStream = new JarOutputStream(bytes);
    jarOutputStream.putNextEntry(new ZipEntry("not_main"));
    jarOutputStream.putNextEntry(new ZipEntry("totally_not_main"));
    jarOutputStream.closeEntry();
    jarOutputStream.close();

    Assertions.assertThatThrownBy(() -> JarClassLoader.builder(bytes.toByteArray()).build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            "The jar file should contain a \"main\" resource to specify the main class")
        .hasMessageContaining("totally_not_main")
        .hasMessageContaining("not_main");
  }

  /** The class name contained in {@code main} must exist in the JAR file. */
  @Test
  public void unableToLoadWithoutMainClass() throws Exception {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    JarOutputStream jarOutputStream = new JarOutputStream(bytes);
    jarOutputStream.putNextEntry(new ZipEntry("main"));
    jarOutputStream.write("com.partisiablockchain.Missing".getBytes(StandardCharsets.UTF_8));
    jarOutputStream.closeEntry();
    jarOutputStream.close();

    Assertions.assertThatThrownBy(() -> JarClassLoader.builder(bytes.toByteArray()).build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            "The jar file should contain the default main class:"
                + " com.partisiablockchain.Missing")
        .hasMessageContaining("Found: [main]");
  }

  /**
   * {@link JarClassLoader} caches the loaded classes, and returns identical {@link Class} objects
   * if the same class is requested multiple times.
   */
  @Test
  public void loadingMultipleTimesShouldReturnSameInstance() throws Exception {
    JarClassLoader loader = JarClassLoader.builder(TestContracts.CONTRACT_BINDER_PUBLIC).build();

    Class<?> loaded = loader.getMainClass();
    String handlerClassName = loaded.getCanonicalName();
    Assertions.assertThat(loader.loadClass(handlerClassName)).isSameAs(loaded);
    InputStream main = loader.getResourceAsStream("main");
    Assertions.assertThat(main).hasContent(loaded.getName());
  }

  /**
   * Some classes can be loaded through the fallback class loader, when they haven't been shaded
   * into the contract.
   */
  @Test
  public void loadThroughFallback() {
    JarClassLoader loader = JarClassLoader.builder(TestContracts.CONTRACT_BINDER_PUBLIC).build();

    Class<?> string = ExceptionConverter.call(() -> loader.loadClass("java.lang.String"), "Error");

    Assertions.assertThat(string).isSameAs(String.class);
  }

  /** {@link JarClassLoader} can load resource files from contract JARs. */
  @Test
  public void loadingResources() {
    JarClassLoader loader = JarClassLoader.builder(TestContracts.CONTRACT_BINDER_PUBLIC).build();

    Class<?> loaded = loader.getMainClass();
    Assertions.assertThat(loader.getResourceAsStream("main")).hasContent(loaded.getName());
    Assertions.assertThat(loader.getResourceAsStream("hellobananan")).isNull();
  }

  /**
   * {@link JarClassLoader} can be configured to always load classes through the fallback class
   * loader, even if the JAR contains a shaded version of the class.
   */
  @Test
  public void classesWithinProvidedNameSpacesAreLoadedByDefaultLoader() {
    JarClassLoader loader =
        JarClassLoader.builder(TEST_JAR)
            .defaultClassLoader(JarClassLoaderTest.class.getClassLoader())
            .addSystemClassLoadingNamespace("com.partisiablockchain.blockchain")
            .build();

    Assertions.assertThat(loader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");
    Assertions.assertThat(loader.getMainClass()).isEqualTo(TestLoadingClass.class);
  }

  /**
   * {@link JarClassLoader} falls back to loading from the JAR file, if a class covered by a
   * fallback-forced namespace is not found.
   */
  @Test
  public void whenDefaultClassLoaderThrowsFallbackClassloadingIsUsed() {
    JarClassLoader loader =
        JarClassLoader.builder(TEST_JAR)
            .defaultClassLoader(new DummyClassLoader())
            .addSystemClassLoadingNamespace("com.partisiablockchain.blockchain")
            .build();

    Assertions.assertThat(loader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");
    Class<?> mainClass = loader.getMainClass();
    Assertions.assertThat(mainClass).isNotEqualTo(TestLoadingClass.class);
    Assertions.assertThat(mainClass.getName()).isEqualTo(TestLoadingClass.class.getName());
  }

  private static final byte[] TEST_JAR =
      JarBuilder.buildJar(TestLoadingClass.class, JarClassLoaderTest.class);

  /** Utility class for testing {@link JarClassLoader}. */
  public static final class TestLoadingClass {}

  /** Utility class for testing {@link JarClassLoader}. */
  public static final class DummyClassLoader extends ClassLoader {

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
      if (TestLoadingClass.class.getName().equals(name)) {
        throw new ClassNotFoundException();
      } else {
        return JarClassLoader.class.getClassLoader().loadClass(name);
      }
    }
  }
}
