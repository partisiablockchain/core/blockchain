package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.ShardRoute;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.storage.BlockchainStorage.ValidBlock;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RejectedExecutionException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainStorageTest extends CloseableTest {

  private final KeyPair producer = new KeyPair(BigInteger.ONE);
  private final Map<MemoryStorage.PathAndHash, byte[]> memoryStorage = new ConcurrentHashMap<>();
  private RootDirectory rootDirectory;
  private String chainId;
  private BlockchainStorage blockchainStorage;

  @AfterEach
  void deleteStorage() {
    if (blockchainStorage != null) {
      blockchainStorage.close();
    }
  }

  /**
   * Clear all states from storage.
   *
   * @param rootDirectory the storage directory
   * @param memoryStorage the actual storage
   */
  public static void clearStates(
      RootDirectory rootDirectory, Map<MemoryStorage.PathAndHash, byte[]> memoryStorage) {
    clearDirectory(rootDirectory, "states", memoryStorage);
  }

  private static void clearDirectory(
      RootDirectory rootDirectory,
      String states,
      Map<MemoryStorage.PathAndHash, byte[]> memoryStorage) {
    String path = rootDirectory.createSub(states).getPath();
    List<MemoryStorage.PathAndHash> keys =
        memoryStorage.keySet().stream().filter(key -> key.getDataPath().equals(path)).toList();
    for (MemoryStorage.PathAndHash key : keys) {
      memoryStorage.remove(key);
    }
  }

  /**
   * Clear all transaction from storage.
   *
   * @param rootDirectory the storage directory
   * @param memoryStorage the actual storage
   */
  public static void clearTransactions(
      RootDirectory rootDirectory, Map<MemoryStorage.PathAndHash, byte[]> memoryStorage) {
    clearDirectory(rootDirectory, "transactions", memoryStorage);
  }

  /** Creates a test object. */
  @BeforeEach
  public void setUp() {
    File dataDirectory = temporaryFolder.toFile();
    rootDirectory = MemoryStorage.createRootDirectory(dataDirectory, memoryStorage);
    chainId = UUID.randomUUID().toString();
    blockchainStorage = new BlockchainStorage(rootDirectory, chainId);
  }

  @Test
  public void patchIndicesOnMissingLatestBlock() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());
    blockchainStorage.close();

    Assertions.assertThat(rootDirectory.createFile("latestBlockTime").delete()).isTrue();

    BlockchainStorage patchedStorage = new BlockchainStorage(rootDirectory, chainId);
    ValidBlock blockAndState = patchedStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block.getBlock());

    BlockTimeStorage latestBlockTime =
        new BlockTimeStorage(rootDirectory.createFile("latestBlockTime"), -1);
    Assertions.assertThat(latestBlockTime.getLatestBlockTime()).isEqualTo(1);
    latestBlockTime.close();
    patchedStorage.close();
  }

  @Test
  public void patchIndicesShouldHandleDuplicateBlocks() {
    ImmutableChainState initial = StateHelper.initial();
    registerDuplicateBlock(blockchainStorage);
    Hash parentHash = Hash.create(stream -> stream.writeInt(11));
    blockchainStorage.registerAcceptedBlock(
        createBlock(initial, 2L, parentHash, List.of()), List.of(), initial, Map.of());
    blockchainStorage.close();

    Assertions.assertThat(rootDirectory.createFile("latestBlockTime").delete()).isTrue();

    BlockchainStorage patchedStorage = new BlockchainStorage(rootDirectory, chainId);
    Assertions.assertThat(patchedStorage.getBlockHash(1L)).isEqualTo(parentHash);
    patchedStorage.close();
  }

  @Test
  public void patchIndicesShouldIgnoreDuplicateBlocksWithMissingParent() {
    registerDuplicateBlock(blockchainStorage);
    blockchainStorage.close();

    Assertions.assertThat(rootDirectory.createFile("latestBlockTime").delete()).isTrue();

    BlockchainStorage patchedStorage = new BlockchainStorage(rootDirectory, chainId);
    Assertions.assertThat(patchedStorage.getBlockHash(1L)).isNull();
    patchedStorage.close();
  }

  private void registerDuplicateBlock(BlockchainStorage blockchainStorage) {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());
    blockchainStorage.registerAcceptedBlock(
        createBlock(initial, 1L, Hash.create(stream -> stream.writeBoolean(true)), List.of()),
        List.of(),
        initial,
        Map.of());
  }

  @Test
  public void unableToWriteWhenClosed() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, TestObjects.EMPTY_HASH, List.of());

    blockchainStorage.close();

    Assertions.assertThatThrownBy(
            () -> blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of()))
        .hasStackTraceContaining("Stream Closed");
  }

  @Test
  public void patchIndicesOnMissingBlockTimeIndex() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());

    clearDirectory(rootDirectory, "blockTime", memoryStorage);

    Assertions.assertThat(blockchainStorage.loadLatestValidBlock()).isNull();

    checkMemoryStorage(rootDirectory, "blockTime", 0);

    blockchainStorage.close();

    BlockchainStorage patchedStorage = new BlockchainStorage(rootDirectory, chainId);

    checkMemoryStorage(rootDirectory, "blockTime", 1);

    ValidBlock blockAndState = patchedStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.isGenesis()).isTrue();
    Assertions.assertThat(blockAndState.getParentBlock()).isNull();
    Assertions.assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block.getBlock());
    patchedStorage.close();
  }

  @Test
  public void patchIndicesOnMissingStateIndex() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    // We need the next block to be able to patch the block to state index
    FinalBlock nextBlock = createBlock(initial, 2, block.getBlock().identifier(), List.of());
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());
    blockchainStorage.registerAcceptedBlock(nextBlock, List.of(), initial, Map.of());

    clearDirectory(rootDirectory, "blockStates", memoryStorage);

    Assertions.assertThat(blockchainStorage.loadLatestValidBlock()).isNull();

    checkMemoryStorage(rootDirectory, "blockStates", 0);

    blockchainStorage.close();

    BlockchainStorage patchedStorage = new BlockchainStorage(rootDirectory, chainId);

    checkMemoryStorage(rootDirectory, "blockStates", 2);

    ValidBlock blockAndState = patchedStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block.getBlock());
    patchedStorage.close();
  }

  @Test
  public void tooLargeValueInLatest() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());
    ValidBlock blockAndState = blockchainStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.getBlock().getBlock()).isEqualTo(block.getBlock());
  }

  @Test
  public void latestBlockWithMissingFinalizedStateShouldBeSkipped() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock genesis = createBlock(initial, 1L, Block.GENESIS_PARENT, List.of());
    blockchainStorage.registerAcceptedBlock(genesis, List.of(), initial, Map.of());
    FinalBlock block =
        createBlock(2L, genesis.getBlock().identifier(), List.of(), TestObjects.EMPTY_HASH);
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());
    ValidBlock blockAndState = blockchainStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.getBlock().getBlock()).isEqualTo(genesis.getBlock());
  }

  @Test
  public void latestBlockWithMissingParentBlockShouldBeSkipped() {

    ImmutableChainState initial = StateHelper.initial();
    FinalBlock genesis = createBlock(initial, 1L, TestObjects.EMPTY_HASH, List.of());
    blockchainStorage.registerAcceptedBlock(genesis, List.of(), initial, Map.of());
    ValidBlock blockAndState = blockchainStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNull();
  }

  @Test
  public void latestBlockNonGenesis() {

    ImmutableChainState initial = StateHelper.initial();
    FinalBlock genesis = createBlock(initial, 1L, TestObjects.EMPTY_HASH, List.of());
    blockchainStorage.registerAcceptedBlock(genesis, List.of(), initial, Map.of());
    FinalBlock next = createBlock(initial, 2L, genesis.getBlock().identifier(), List.of());
    blockchainStorage.registerAcceptedBlock(next, List.of(), initial, Map.of());
    ValidBlock blockAndState = blockchainStorage.loadLatestValidBlock();
    Assertions.assertThat(blockAndState).isNotNull();
    Assertions.assertThat(blockAndState.isGenesis()).isFalse();
    Assertions.assertThat(blockAndState.getParentBlock()).isNotNull();
  }

  @Test
  public void unableToLoadExecutableBlockWithMissingTransactions() {
    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block =
        createBlock(initial, 1L, TestObjects.EMPTY_HASH, List.of(TestObjects.EMPTY_HASH));
    blockchainStorage.registerAcceptedBlock(block, List.of(), initial, Map.of());

    Assertions.assertThat(blockchainStorage.loadExecutableBlock(1L)).isNull();
  }

  @Test
  public void eventTransactionStorage() {

    ExecutableEvent event =
        new ExecutableEvent(
            "originShard",
            EventTransaction.createStandalone(
                TestObjects.EMPTY_HASH,
                TestObjects.ACCOUNT_ONE,
                100_000L,
                InteractWithContractTransaction.create(TestObjects.CONTRACT_SYS, new byte[0]),
                new ShardRoute("destinationShard", 27),
                0,
                1L));

    blockchainStorage.storeEventTransactions(List.of(event));
    ExecutableEvent fromStorage = blockchainStorage.getEventTransaction(event.identifier());
    Assertions.assertThat(fromStorage).usingRecursiveComparison().isEqualTo(event);

    ImmutableChainState initial = StateHelper.initial();
    FinalBlock block =
        createBlock(initial, 1L, TestObjects.EMPTY_HASH, List.of(event.identifier()));

    ExecutedTransaction executedTransaction =
        ExecutedTransaction.create(block.getBlock().identifier(), event, true, FixedList.create());
    blockchainStorage.registerAcceptedBlock(block, List.of(executedTransaction), initial, Map.of());

    List<ExecutedTransaction> transactionsForBlock =
        blockchainStorage.getTransactionsForBlock(block.getBlock());
    Assertions.assertThat(transactionsForBlock).hasSize(1);
    ExecutableTransaction fromBlock = transactionsForBlock.get(0).getInner();
    Assertions.assertThat(fromBlock).usingRecursiveComparison().isEqualTo(event);

    ExecutableBlock executableBlock =
        blockchainStorage.loadExecutableBlock(block.getBlock().getBlockTime());
    Assertions.assertThat(executableBlock).isNotNull();
    Assertions.assertThat(executableBlock.toString()).contains(block.toString());
    Assertions.assertThat(executableBlock.getEventTransactions()).hasSize(1);
    ExecutableTransaction fromExecutable = executableBlock.getEventTransactions().get(0);
    Assertions.assertThat(fromExecutable).usingRecursiveComparison().isEqualTo(event);
  }

  private FinalBlock createBlock(
      ImmutableChainState initial, long blockTime, Hash parentBlock, List<Hash> emptyHash2) {
    return createBlock(blockTime, parentBlock, emptyHash2, initial.getHash());
  }

  private FinalBlock createBlock(
      long blockTime, Hash parentBlock, List<Hash> emptyHash2, Hash stateHash) {
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockTime,
            blockTime,
            parentBlock,
            stateHash,
            List.of(),
            emptyHash2);
    return new FinalBlock(block, SafeDataOutputStream.serialize(producer.sign(block.identifier())));
  }

  private void checkMemoryStorage(RootDirectory rootDirectory, String states, long expected) {
    String path = rootDirectory.createSub(states).getPath();
    long count =
        memoryStorage.keySet().stream().filter(key -> key.getDataPath().equals(path)).count();
    Assertions.assertThat(count).isEqualTo(expected);
  }

  @Test
  public void nonExistingBlock() {
    Assertions.assertThat(blockchainStorage.getBlock(TestObjects.EMPTY_HASH)).isNull();
    Assertions.assertThat(blockchainStorage.getBlock(77777L)).isNull();
  }

  @Test
  public void closeStateDir() {
    StateStorage stateStorage = blockchainStorage.getStateStorage();
    blockchainStorage.close();

    Assertions.assertThatThrownBy(
            () -> stateStorage.write(TestObjects.EMPTY_HASH, FunctionUtility.noOpConsumer()))
        .isInstanceOf(RejectedExecutionException.class);
  }
}
