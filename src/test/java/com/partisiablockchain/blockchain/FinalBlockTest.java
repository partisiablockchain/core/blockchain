package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FinalBlockTest {

  @Test
  public void testToString() {
    long blockTime = 117L;
    Hash parentHash = TestObjects.hashNumber(blockTime);
    Block block =
        new Block(
            System.currentTimeMillis(),
            blockTime,
            blockTime,
            parentHash,
            TestObjects.EMPTY_HASH,
            List.of(),
            List.of(TestObjects.hashNumber(111L)));

    FinalBlock finalBlock = new FinalBlock(block, new byte[0]);

    assertThat(finalBlock.toString())
        .contains("blockTime=" + blockTime)
        .contains("transactions=1")
        .contains("hash=" + block.identifier().toString());
  }
}
