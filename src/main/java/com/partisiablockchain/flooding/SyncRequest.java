package com.partisiablockchain.flooding;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;

/**
 * A sync request in the flooding layer. Results in sending every relevant block to the sender of
 * the sync request.
 */
public final class SyncRequest implements DataStreamSerializable {

  static final SyncRequest INSTANCE = new SyncRequest();

  /** Sync packet. */
  public static final Packet<SyncRequest> PACKET = new Packet<>(Packet.Type.SYNC, INSTANCE);

  private SyncRequest() {}

  static SyncRequest read() {
    return INSTANCE;
  }

  @Override
  public void write(SafeDataOutputStream stream) {}
}
