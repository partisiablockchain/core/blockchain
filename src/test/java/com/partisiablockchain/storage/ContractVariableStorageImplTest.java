package com.partisiablockchain.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractVariableStorageImplTest extends CloseableTest {

  private ContractVariableStorage storage;

  /** Setup the storage. */
  @BeforeEach
  public void setUp() {
    RootDirectory root = MemoryStorage.createRootDirectory(temporaryFolder.toFile());
    this.storage = root.createVariableStorage("test-variables");
  }

  @Test
  public void shouldAllowOverwrite() {
    final byte[] first = {1};
    Assertions.assertThat(storage.containsVariable(TestObjects.CONTRACT_SYS, 1)).isFalse();
    storage.write(TestObjects.CONTRACT_SYS, 1, first);
    Assertions.assertThat(storage.containsVariable(TestObjects.CONTRACT_SYS, 1)).isTrue();

    Assertions.assertThat(storage.read(TestObjects.CONTRACT_SYS, 1)).isEqualTo(first);

    final byte[] second = {2};
    storage.write(TestObjects.CONTRACT_SYS, 1, second);
    Assertions.assertThat(storage.read(TestObjects.CONTRACT_SYS, 1)).isEqualTo(second);
    Assertions.assertThat(storage.containsVariable(TestObjects.CONTRACT_SYS, 1)).isTrue();
  }

  @Test
  public void shouldAllowRemove() {
    final byte[] value = {1};
    storage.write(TestObjects.CONTRACT_PUB1, 1, value);

    Assertions.assertThat(storage.containsVariable(TestObjects.CONTRACT_PUB1, 1)).isTrue();

    storage.remove(TestObjects.CONTRACT_PUB1, 1);

    Assertions.assertThat(storage.containsVariable(TestObjects.CONTRACT_PUB1, 1)).isFalse();
  }

  @Test
  public void key() {
    Hash key = ContractVariableStorageImpl.key(TestObjects.CONTRACT_SYS, 1);
    Assertions.assertThat(key.toString())
        .isEqualTo("7f1ea6ddefa16a9053ea90baed5cb79200c8d9485155165632dbe80942039ff4");
  }
}
