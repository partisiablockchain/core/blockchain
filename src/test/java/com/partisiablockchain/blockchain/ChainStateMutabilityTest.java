package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.ImmutableChainState.SerializableChainState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.fee.FeePluginHelper;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.ObjectCreator;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ChainStateMutabilityTest extends CloseableTest {

  private static final int ACCOUNT_COUNT = 50;
  private static final int CONTRACT_COUNT = 50;
  private Random random;

  private AvlTree<BlockchainAddress, AccountState> accounts;
  private AvlTree<BlockchainAddress, ContractState> contracts;
  private BlockchainAddress feeAccount;
  private BlockchainAddress zkFeeAccount;
  private final String chainId = UUID.randomUUID().toString();

  private BlockchainAddress randomBlockchainAddress() {
    BigInteger integer = new BigInteger(32, random);
    KeyPair pair = new KeyPair(integer);
    return pair.getPublic().createAddress();
  }

  private ContractState randomContractState() {
    return ContractStateTest.create(new byte[0]);
  }

  private AccountState randomAccountState() {
    long l = random.nextLong();
    if (l < 0) {
      return randomAccountState();
    } else {
      return AccountState.create();
    }
  }

  /** Creates a new test object. */
  @BeforeEach
  public void setUp() {
    random = new Random(6784678934798567843L);
    accounts = AvlTree.create();
    for (int i = 0; i < ACCOUNT_COUNT; i++) {
      BlockchainAddress account = randomBlockchainAddress();
      accounts = accounts.set(account, randomAccountState());
    }

    contracts = AvlTree.create();
    for (int i = 0; i < CONTRACT_COUNT; i++) {
      contracts = contracts.set(randomBlockchainAddress(), randomContractState());
    }

    feeAccount = randomBlockchainAddress();
    zkFeeAccount = randomBlockchainAddress();
    accounts = accounts.set(feeAccount, AccountState.create());
    accounts = accounts.set(zkFeeAccount, AccountState.create());
  }

  private void test(Consumer<MutableChainState> mutation, boolean equal) {
    ImmutableChainState initial = createInitial();

    MutableChainState mutable = initial.asMutable();
    mutation.accept(mutable);
    ImmutableChainState modified =
        mutable.asImmutable(chainId, null, AvlTree.create(), AvlTree.create());
    if (equal) {
      Assertions.assertThat(initial.getHash()).isEqualTo(modified.getHash());
    } else {
      Assertions.assertThat(initial.getHash()).isNotEqualTo(modified.getHash());
    }
  }

  private ImmutableChainState createInitial() {
    StateStorage memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState initial =
        new ImmutableChainState(
            new ChainStateCache(memoryStateStorage),
            new ExecutedState(
                new SerializableChainState(
                    accounts,
                    contracts,
                    AvlTree.create(),
                    AvlTree.create(),
                    AvlTree.create(),
                    AvlTree.create(),
                    FixedList.create(),
                    0),
                chainId,
                null,
                AvlTree.create(),
                AvlTree.create()));
    MutableChainState mutable = initial.asMutable();
    byte[] rpc = new byte[0];
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        FeePluginHelper.createJar(),
        rpc);
    FeePluginHelper.setFeeAccount(mutable, feeAccount);
    FeePluginHelper.setZkFeeAccount(mutable, zkFeeAccount);
    return mutable.asImmutable(chainId, null, AvlTree.create(), AvlTree.create());
  }

  private void unchanged(Consumer<MutableChainState> mutation) {
    test(mutation, true);
  }

  private void changed(Consumer<MutableChainState> mutation) {
    test(mutation, false);
  }

  @Test
  public void noneUnchanged() {
    unchanged(mutableChainState -> {});
  }

  @Test
  public void unableToCreateTwice() {
    changed(
        mutableChainState -> {
          BlockchainAddress address = randomBlockchainAddress();
          mutableChainState.createAccount(address);
          Assertions.assertThatThrownBy(() -> mutableChainState.createAccount(address))
              .hasMessageContaining("Unable to create existing account " + address);
        });
  }

  @Test
  public void bumpGovernanceVersion() {
    changed(MutableChainState::bumpGovernanceVersion);
  }

  @Test
  public void setContractChanged() {
    changed(
        mutableChainState ->
            mutableChainState.setContract(randomBlockchainAddress(), randomContractState()));
  }

  @Test
  public void setFeeAccountChanged() {
    changed(
        mutableChainState ->
            FeePluginHelper.setFeeAccount(mutableChainState, randomBlockchainAddress()));
  }

  @Test
  public void setZkFeeAccountChanged() {
    changed(
        mutableChainState ->
            FeePluginHelper.setZkFeeAccount(mutableChainState, randomBlockchainAddress()));
  }
}
